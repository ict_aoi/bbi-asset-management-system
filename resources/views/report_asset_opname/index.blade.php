@extends('layouts.app', ['active' => 'report-asset-opname'])

@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Report Asset Opname</span></h4>
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
				<li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
				<li>Report</li>
				<li class="active">Asset Opname</li>
			</ul>
		</div>
	</div>
@endsection

@section('page-content')
<div class="panel panel-default border-grey">
		<div class="panel-body">
			<div class="row {{ (auth::user()->is_super_admin)? '':'hidden' }}">
				@include('form.select', [
					'field'         => 'factory',
					'label'         => 'Factory',
					'default'       => $factory_id,
					'div_class'     => (auth::user()->is_super_admin)? '':'hidden',
					'options'       => [
						''          => '-- Select Factory --',
					]+$factories,
					'class'         => 'select-search',
					'attributes'    => [
						'id'        => 'select_factory'
					]
				])
			</div>
			<div class="row  {{ (auth::user()->is_super_admin)? '':'hidden' }}">
				@include('form.select', [
					'field'         => 'sub_department',
					'label'         => 'Subdepartment',
					'default'       => $sub_department_id,
					'div_class'     => (auth::user()->is_super_admin)? '':'hidden',
					'options'       => [
						''          => '-- Select Subdepartment --',
					]+$sub_departments,
					'class'         => 'select-search',
					'attributes'    => [
						'id'        => 'select_sub_department'
					]
				])
			</div>
			<div class="text-right">
				<button type="btn" class="btn btn-blue-success legitRipple col-xs-12" id="btn_export">Export <i class="icon-file-excel position-right"></i></button>
			</div>

			{!!
				Form::open([
					'role' 		=> 'form',
					'url' 		=> route('reportAssetOpname.export'),
					'method' 	=> 'get',
					'enctype' 	=> 'multipart/form-data',
					'id'		=> 'form',
					'target'	=> '_blank'
				])
			!!}
				{!! Form::hidden('_sub_department',$sub_department_id, array('id' => '_sub_department')) !!}
				{!! Form::hidden('_factory',$factory_id, array('id' => '_factory')) !!}
			{!! Form::close() !!}
		</div>
	</div>
	{!! Form::hidden('page','index', array('id' => 'page')) !!}
@endsection

@section('page-js')
	<script src="{{ mix('js/report_asset_opname.js') }}"></script>
@endsection



