<ul class="icons-list">
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-menu9"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-right">
            @if (isset($approve))
                <li><a href="#" onclick="approve('{!! $approve !!}')"><i class="icon-checkmark3"></i> Approved</a></li>
            @endif

            @if (isset($cancel))
                <li><a href="#" onclick="cancel('{!! $cancel !!}')"><i class="icon-cross3"></i> Cancel</a></li>
            @endif

        </ul>
    </li>
</ul>
