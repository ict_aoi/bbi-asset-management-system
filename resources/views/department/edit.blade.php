@extends('layouts.app',['active' => 'department'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Department</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Dashboard</a></li>
            <li>Organization</li>
            <li><a href="{{ route('department.index') }}">Department</a></li>
            <li class="active">Edit</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat">
    <div class="panel-body">
        {!!
            Form::open([
                'role'      => 'form',
                'url'       => ($department ? route('department.update',$department->id) : route('department.store') ),
                'method'    => 'post',
                'class'     => 'form-horizontal',
                'id'        => 'form'
            ])
        !!}
            @include('form.select', [
                'field'     => 'oprational_cost_types_erp',
                'label'     => 'Oprational Cost Types ERP',
                'default'   => ($department ? strtoupper($department->erp_oprational_cost_type) : null),
                'mandatory' => '*Required',
                'label_col' => 'col-md-2 col-lg-2 col-sm-12',
                'form_col'  => 'col-md-10 col-lg-10 col-sm-12',
                'options'   => [
                    '' => '-- Select Oprational Cost Type ERP --',
                ]+$oprational_cost_types_erp,
                'class'      => 'select-search',
                'attributes' => [
                    'id' => 'select_oprational_cost_types_erp'
                ]
            ])

            @include('form.select', [
                'field'     => 'department_erp',
                'label'     => 'Department ERP',
                'default'   => ($department ? $department->department_erp_id : null),
                'mandatory' => '*Required',
                'label_col' => 'col-md-2 col-lg-2 col-sm-12',
                'form_col'  => 'col-md-10 col-lg-10 col-sm-12',
                'options'   => [
                    '' => '-- Select Department ERP ID --',
                ]+$departments_erp,
                'class'      => 'select-search',
                'attributes' => [
                    'id' => 'select_department_erp'
                ]
            ])

            @include('form.text', [
                'field'         => 'name',
                'label'         => 'Nama',
                'default'       => ucwords(strtolower($department_absensi->department_name)),
                'mandatory'     => '*Required',
                'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                'attributes'    => [
                    'id'        => 'name',
                    'readonly'  => 'readonly'
                ]
            ])

            @include('form.textarea', [
                'field'         => 'description',
                'label'         => 'Description',
                'default'       => ($department ? ucwords($department->description) : null),
                'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                'attributes'    => [
                    'id'        => 'description',
                    'rows'      => 5,
                    'style'     => 'resize: none;'
                ]
            ])

            <div class="text-right">
                <button type="submit" class="btn btn-primary legitRipple col-xs-12">Save <i class="icon-floppy-disk position-right"></i></button>
            </div>
            {!! Form::hidden('department_absensi_id', $id, array('id' => 'department_absensi_id')) !!}
        {!! Form::close() !!}
    </div>
</div>

@endsection

@section('page-js')
<script src="{{ mix('js/department.js') }}"></script>
@endsection