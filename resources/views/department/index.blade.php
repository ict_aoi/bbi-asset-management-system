@extends('layouts.app',['active' => 'department'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Department</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Dashboard</a></li>
            <li>Organization</li>
            <li class="active">Department</li>
        </ul>
    </div>
</div>
@endsection
@section('page-content')
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="departmentTable">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Erp Oprational Cost Type</th>
                        <th>Erp Accouting Locator</th>
                        <th>Erp Department Id</th>
                        <th>Department</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
{!! Form::hidden('msg', $msg, array('id' => 'msg')) !!}
@endsection

@section('page-js')
<script src="{{ mix('js/department.js') }}"></script>
@endsection