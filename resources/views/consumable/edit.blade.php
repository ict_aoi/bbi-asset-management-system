@extends('layouts.app',['active' => 'consumable'])

@section('page-css')

@endsection

@section('page-header')
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Consumable</span></h4>
            </div>
        </div>
        <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
            <ul class="breadcrumb">
                <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Dashboard</a></li>
                <li ><a href="{{ route('consumable.index') }}">Consumable</a></li>
                <li class="active">Edit</li>
            </ul>
        </div>
    </div>
@endsection

@section('page-content')
    <div class="panel panel-flat">
        <div class="panel-body">
            {!!
                Form::open([
                    'role'          => 'form',
                    'url'           => route('consumable.update',$consumable_asset->id),
                    'method'        => 'post',
                    'class'         => 'form-horizontal',
                    'enctype'       => 'multipart/form-data',
                    'id'            => 'form'
                ])
            !!}
            <div class="row">
                <div class="col-md-6">
                    @include('form.select', [
                        'field'         => 'company',
                        'label'         => 'Company',
                        'default'       => $consumable_asset->company_id,
                        'mandatory'     => '*Required',
                        'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                        'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                        'div_class'     => (auth::user()->is_super_admin)? '':'hidden',
                        'options'       => [
                            ''          => '-- Select Company --',
                        ]+$companies,
                        'class'         => 'select-search',
                        'attributes'    => [
                            'id'        => 'select_company',
                            'disabled'  => 'disabled'
                        ]
                    ])

                    @include('form.select', [
                        'field'         => 'sub_department',
                        'label'         => 'Sub Departement',
                        'default'       => $consumable_asset->sub_department_id,
                        'mandatory'     => '*Required',
                        'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                        'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                        'div_class'     => (auth::user()->is_super_admin)? '':'hidden',
                        'options'       => [
                            ''          => '-- Select Sub Departement --',
                        ]+$sub_departments,
                        'class'         => 'select-search',
                        'attributes'    => [
                            'id'        => 'select_sub_department',
                            'disabled'  => 'disabled'
                        ]
                    ])

                    @include('form.select', [
                        'field'         => 'asset_type',
                        'label'         => 'Asset Type',
                        'default'       => $consumable_asset->asset_type_id,
                        'mandatory'     => '*Required',
                        'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                        'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                        'options'       => [
                            ''          => '-- Select Asset Type --',
                        ],
                        'class'         => 'select-search',
                        'attributes'    => [
                            'id'        => 'select_asset_type',
                            'disabled'  => 'disabled'
                        ]
                    ])
                    
                    @include('form.picklist', [
                        'field'         => 'no_asset',
                        'label'         => 'No. Asset',
                        'name'          => 'no_asset',
                        'readonly'      => 'readonly',
                        'placeholder'   => 'Scan no asset here',
                        'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                        'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                        'attributes'    => [
                            'id'        => 'no_asset',
                        ]
                    ])
                    
                </div>

                <div class="col-md-6">
                    @include('form.select', [
                        'field'         => 'manufacture',
                        'label'         => 'Manufacture',
                        'default'       => $consumable_asset->manufacture_id,
                        'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                        'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                        'options'       => [
                            ''          => '-- Select Manufacture --',
                        ],
                        'class'         => 'select-search',
                        'attributes'    => [
                            'id'        => 'select_manufacture'
                        ]
                    ])


                    @include('form.text', [
                        'field'         => 'model',
                        'label'         => 'Model',
                        'default'       => ucwords($consumable_asset->model),
                        'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                        'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                        'mandatory'     => '*Required',
                        'attributes'    => [
                            'id'        => 'model'
                        ]
                    ])

                    @include('form.text', [
                        'field'         => 'serial_number',
                        'label'         => 'Serial Number',
                        'default'       => $consumable_asset->serial_number,
                        'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                        'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                        'attributes'    => [
                            'id'        => 'serial_number'
                        ]
                    ])

                    <div class="form-group col-lg-12">
                        <label class="control-label col-lg-2 text-semibold">Total Received</label>
                        <div class="col-lg-10">
                            <div class="row">
                                <div class="col-xs-7">
                                    <input type="text" class="form-control" name="total_current" id="total_current">
                                    <span class="help-block">Total Receive for this moment</span>
                                </div>

                                <div class="col-xs-5">
                                    <input type="hidden" class="form-control" name="_total_all" id="_total_all" value="{{ $consumable_asset->total }}" readonly="readonly">
                                    <input type="text" class="form-control" name="total_all" id="total_all" value="{{ $consumable_asset->total }}" readonly="readonly">
                                    <span class="help-block">Total Overall</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="draw"></div>
                </div>
            </div>

            {!! Form::hidden('url_store_detail_consumable_asset',route('consumable.storeDetail',$consumable_asset->id), array('id' => 'url_store_detail_consumable_asset')) !!}
            {!! Form::hidden('url_data_detail_consumable_asset',route('consumable.dataDetailConsumable',$consumable_asset->id), array('id' => 'url_data_detail_consumable_asset')) !!}
            {!! Form::hidden('form_status', 'edit', array('id' => 'form_status')) !!}
            {!! Form::hidden('consumable_asset_id', $consumable_asset->id, array('id' => 'consumable_asset_id')) !!}
            {!! Form::hidden('manufacture_id', $consumable_asset->manufacture_id, array('id' => 'manufacture_id')) !!}
            {!! Form::hidden('asset_type_id', $consumable_asset->asset_type_id, array('id' => 'asset_type_id')) !!}
            {!! Form::hidden('details', json_encode($details), array('id' => 'details')) !!}
            {!! Form::hidden('custom_fields', '[]', array('id' => 'custom_fields')) !!}
            {!! Form::hidden('company_id', $consumable_asset->company_id, array('id' => 'company_id')) !!}
            {!! Form::hidden('sub_department_id', $consumable_asset->sub_department_id, array('id' => 'sub_department_id')) !!}
                <div class="text-right">
                    <button type="submit" class="btn btn-primary legitRipple col-xs-12">Save <i class="icon-floppy-disk position-right"></i></button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>

    <div class="panel panel-flat">
        <div class="panel-body">
            <div class="panel-heading">
                <h6 class="panel-title"> Detail <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
                <div class="heading-elements">
                    <button id="add_detail_consumable" class="btn btn-default space"><i class="icon-plus2 position-left"></i>Create New Detail</button>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-basic table-condensed" id="consumableDetailTable">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Value</th>
                                <th>No. Asset</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-modal')
    @include('consumable._detail_edit_modal')
    @include('consumable._detail_table')
    @include('consumable._fields')
    @include('consumable._modal_confirmation_asset', [
        'name' => 'no_asset',
        'title' => 'List No. Asset',
        'placeholder' => 'Cari berdasarkan No. Inventori / Nomor Serial',
    ])
@endsection

@section('page-js')
    <script src="{{ mix('js/datepicker.js') }}"></script>
    <script src="{{ mix('js/consumable.js') }}"></script>
@endsection
