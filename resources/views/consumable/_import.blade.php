<script type="x-tmpl-mustache" id="upload_asset">
	{% #item %}
		<tr>
			<td>{% no %}</td>
			<td>
				{% department_name %}
			</td>
			<td>
				{% asset_type_name %}
			</td>
			<td>
				{% manufacture_name %}
			</td>
			<td>
				{% model %}
			</td>
			<td>
				{% serial_number %}
			</td>
			<td>
				{% no_asset %}
			</td>
			<td>
				{% no_inventory_manual %}
			</td>
			<td>
				{% purchase_number %}
			</td>
			<td>
				{% supplier_name %}
			</td>
			<td>
				{% system_log %}
			</td>
		</tr>
	{%/item%}
</script>