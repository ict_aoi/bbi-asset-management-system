@extends('layouts.app',['active' => 'consumable'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Consumable</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Dashboard</a></li>
            <li class="active">Consumable</li>
        </ul>

        <ul class="breadcrumb-elements">
			<li class="dropdown">
				<a href="#" class="dropdown-toggle legitRipple breadcrumb-dropdown" data-toggle="dropdown">
					<i class="icon-three-bars position-left"></i>
					Actions
					<span class="caret"></span>
				</a>
				
				<ul class="dropdown-menu dropdown-menu-right">
					<!--<li><a href="{{ route('consumable.import') }}"><i class="icon-drawer-out pull-right"></i> Import</a></li>-->
					<li><a href="{{ route('consumable.create') }}"><i class="icon-plus2 pull-right"></i> Create</a></li>
				</ul>
			</li>
		</ul>
    </div>
</div>
@endsection
@section('page-content')
    <div class="panel panel-flat panel-collapsed">
        <div class="panel-heading">
            <h5 class="panel-title">Filters   <a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            <div class="col-md-4 col-lg-4 col-sm-12">
                @include('form.select', [
                    'field'             => 'company',
                    'label'             => 'Company',
                    'default'           => $company_id,
                    'label_col'         => 'col-md-12 col-lg-12 col-sm-12',
                    'form_col'          => 'col-md-12 col-lg-12 col-sm-12',
                    'div_class'         => (auth::user()->is_super_admin)? '':'hidden',
                    'options'           => $companies,
                    'class'             => 'select-search',
                    'attributes'        => [
                        'id'            => 'select_company'
                    ]
                ])
            </div>
            <div class="col-md-4 col-lg-4 col-sm-12">
                @include('form.select', [
                    'field'         => 'sub_department',
                    'label'         => 'Sub Departemen',
                    'default'       => $sub_department_id,
                    'label_col'     => 'col-md-12 col-lg-12 col-sm-12',
                    'form_col'      => 'col-md-12 col-lg-12 col-sm-12',
                    'div_class'     => (auth::user()->is_super_admin)? '':'hidden',
                    'options'       => [
                        ''          => '-- Select Sub Departemen --',
                    ]+$sub_departments,
                    'class'         => 'select-search',
                    'attributes'    => [
                        'id'        => 'select_sub_department'
                    ]
                ])
            </div>
            <div class="col-md-4 col-lg-4 col-sm-12">
                @include('form.select', [
                    'field'         => 'asset_type',
                    'label'         => 'Asset Type',
                    'label_col'     => 'col-md-12 col-lg-12 col-sm-12',
                    'form_col'      => 'col-md-12 col-lg-12 col-sm-12',
                    'options'       => [
                        ''          => '-- Select Asset Type --',
                    ],
                    'class'         => 'select-search',
                    'attributes'    => [
                        'id'        => 'select_asset_type'
                    ]
                ])
            </div>
        </div>
    </div>

    <div class="panel panel-flat">
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-basic table-condensed" id="consumableTable">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Id</th>
                            <th>custom_field_1</th>
                            <th>custom_field_2</th>
                            <th>custom_field_3</th>
                            <th>custom_field_4</th>
                            <th>custom_field_5</th>
                            <th>custom_field_6</th>
                            <th>custom_field_7</th>
                            <th>custom_field_8</th>
                            <th>custom_field_9</th>
                            <th>custom_field_10</th>
                            <th>custom_field_11</th>
                            <th>custom_field_12</th>
                            <th>custom_field_13</th>
                            <th>custom_field_14</th>
                            <th>custom_field_15</th>
                            <th>custom_field_16</th>
                            <th>custom_field_17</th>
                            <th>custom_field_18</th>
                            <th>custom_field_19</th>
                            <th>custom_field_20</th>
                            @if(auth::user()->is_super_admin)
                                <th>Asset Company</th>
                                <th>Asset Departement</th>
                            @endif
                            <th>Manufacture</th>
                            <th>No Asset (Accounting)</th>
                            <th>Type Asset</th>
                            <th>Model</th>
                            <th>Serial Number</th>
                            <th>Total All</th>
                            <th>Total Used</th>
                            <th>Total Available</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    {!! Form::hidden('is_super_admin', (auth::user()->is_super_admin)? '1':'0', array('id' => 'is_super_admin')) !!}
    {!! Form::hidden('form_status', 'index', array('id' => 'form_status')) !!}
    {!! Form::hidden('msg', $msg, array('id' => 'msg')) !!}
    {!! Form::hidden('custom_fields', '[]', array('id' => 'custom_fields')) !!}
    {!! Form::hidden('details', '[]', array('id' => 'details')) !!}
@endsection

@section('page-js')
    <script src="{{ mix('js/consumable.js') }}"></script>
@endsection
