@extends('layouts.app',['active' => 'check_in'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Check-In</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
            <li ><a href="{{ route('consumable.index') }}">Consumable</a></li>
            <li class="active">Check-In</li>
        </ul>
    </div>
</div>
@endsection
@section('page-content')
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="checkinTable">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Date</th>
                        <th>Destination</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
{!! Form::hidden('consumable_asset_id',$consumable_asset_id , array('id' => 'consumable_asset_id')) !!}
@endsection


@section('page-js')
<script src="{{ mix('js/consumable_checkin.js') }}"></script>
@endsection