@extends('layouts.app',['active' => 'consumable'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Consumable</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Dashboard</a></li>
            <li ><a href="{{ route('consumable.index') }}">Consumable</a></li>
            <li class="active">History</li>
        </ul>
    </div>
</div>
@endsection


@section('page-content')
<div class="panel panel-flat">
    <div class="panel-body">
			<div class="row">
                <div class="col-md-6">
                    <p>No. Asset (accounting) :  <b>{{$consumable_asset->no_asset}}</b></p>
                    <p>Asset Company :  <b>{{ ucwords($consumable_asset->company->name) }}</b></p>
                    <p>Asset Department :  <b>{{ ucwords($consumable_asset->department_name) }}</b></p>
                    <p>Model :  <b>{{ ucwords($consumable_asset->model) }}</b></p>
					<p>No. Serial : <b>{{ $consumable_asset->serial_number}}</b></p> 
            	</div>
				<div class="col-md-6">
                     <p>Total All : <b>{{$consumable_asset->total}}</b></p> 
                     <p>Total Used : <b>{{$consumable_asset->qty_used}}</b></p> 
                     <p>Total Available : <b>{{$consumable_asset->qty_available}}</b></p> 
				</div>
			</div>
		</div>
</div>
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="historyTable">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Date</th>
                        <th>Status</th>
                        <th>From</th>
                        <th>Destination</th>
                        <th>Returned</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
{!! Form::hidden('consumable_asset_id', $consumable_asset->id, array('id' => 'consumable_asset_id')) !!}
@endsection

@section('page-js')
<script>
    $(document).ready( function ()
    {
        var consumable_asset_id = $('#consumable_asset_id').val();
        var historyTable = $('#historyTable').DataTable({
            dom: 'Bfrtip',
            processing: true,
            serverSide: true,
            pageLength:100,
            deferRender:true,
            ajax: {
                type: 'GET',
                url: '/consumable/history/'+consumable_asset_id+'/data',
            },
            fnCreatedRow: function (row, data, index) {
                var info = historyTable.page.info();
                var value = index+1+info.start;
                $('td', row).eq(0).html(value);
            },
            columns: [
                {data: null, sortable: false, orderable: false, searchable: false},
                {data: 'movement_date', name: 'movement_date',searchable:true,visible:true,orderable:false},
                {data: 'status', name: 'status',searchable:true,visible:true,orderable:false},
                {data: 'from', name: 'from',searchable:false,visible:true,orderable:false},
                {data: 'to', name: 'to',searchable:false,visible:true,orderable:false},
                {data: 'is_return', name: 'is_return',searchable:false,visible:true,orderable:false}
                    
            ]
        });

        var dtable = $('#historyTable').dataTable().api();
        $(".dataTables_filter input")
            .unbind() // Unbind previous default bindings
            .bind("keyup", function (e) { // Bind our desired behavior
                // If the user pressed ENTER, search
                if (e.keyCode == 13) {
                    // Call the API search function
                    dtable.search(this.value).draw();
                }
                // Ensure we clear the search if they backspace far enough
                if (this.value == "") {
                    dtable.search("").draw();
                }
                return;
        });
        dtable.draw();
    });   
</script>
@endsection

