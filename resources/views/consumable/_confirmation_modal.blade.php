<div id="confirmationModal" data-backdrop="static" data-keyboard="false" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
			</div>
				{!!
					Form::open([
						'role' 		=> 'form',
						'url' 		=> route('asset.exportFormImport'),
						'method' 	=> 'get',
						'class' 	=> 'form-horizontal',
					])
				!!}
				<div class="modal-body">
					@include('form.select', [
						'field' 		=> 'company',
						'label' 		=> 'Perusahaan',
						'default' 		=> $company_id,
						'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
						'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
						'div_class' 	=> (auth::user()->is_super_admin)? '':'hidden',
						'options' 		=> [
							'' 			=> '-- Pilih Perusahaan --',
						]+$companies,
						'class' 		=> 'select-search',
						'attributes' 	=> [
							'id' 		=> 'select_company'
						]
					])

					@include('form.select', [
						'field' 		=> 'factory',
						'label' 		=> 'Pabrik',
						'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
						'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
						'default' 		=> $factory_id,
						'div_class' 	=> (auth::user()->is_super_admin)? '':'hidden',
						'options' => [
							'' 			=> '-- Pilih Pabrik --',
						]+$factories,
						'class' 		=> 'select-search',
						'attributes' 	=> [
							'id' 		=> 'select_factory'
						]
					])

					@include('form.select', [
						'field' 		=> 'department',
						'label' 		=> 'Departemen',
						'default' 		=> $department_id,
						'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
						'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
						'div_class' 	=> (auth::user()->is_super_admin)? '':'hidden',
						'options' 		=> [
							'' 			=> '-- Pilih Departemen --',
						]+$departments,
						'class' 		=> 'select-search',
						'attributes' 	=> [
							'id' 		=> 'select_department'
						]
					])

					@include('form.select', [
						'field' 		=> 'asset_type',
						'label' 		=> 'Jenis Asset',
						'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
						'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
						'options' 		=> [
							'' 			=> '-- Pilih Jenis Asset --',
						],
						'class' 		=> 'select-search',
						'attributes' 	=> [
							'id' 		=> 'select_asset_type'
						]
					])
					
				</div>
				
				{!! Form::hidden('factory_id', $factory_id, array('id' => 'factory_id')) !!}
				{!! Form::hidden('department_id', $department_id, array('id' => 'department_id')) !!}
				<div class="modal-footer">
					<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Get form upload</button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
