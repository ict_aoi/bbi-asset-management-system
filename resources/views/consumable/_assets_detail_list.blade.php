<table class="table">
	<thead>
	  <tr>
		<th>Value</th>
		<th>No. Asset</th>
		<th>Action</th>
	  </tr>
	</thead>

	<tbody>
		@foreach ($lists as $key => $list)
			<tr>
				<td>
					{{ strtoupper($list->value) }}
				</td>
				<td>
					{{ strtoupper($list->no_asset) }}
				</td>
					{!! '<span class="label label-info">'.ucwords($list->status).'</span>' !!}
				</td>
				<td>
					<button data-dismiss="modal" class="btn btn-info btn-xs btn-choose"
						type="button" data-id="{{ $list->id  }}" data-value="{{ $list->value  }}" data-no_asset="{{ $list->no_asset  }}">Select
					</button>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>

{!! $lists->appends(Request::except('page'))->render() !!}
