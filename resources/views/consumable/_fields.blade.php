<script type="x-tmpl-mustache" id="fields">
	{% #item %}
		{% #is_text %}
			<div class="form-group col-lg-12">
				<label for="{%label%}" class="text-semibold control-label col-md-2 col-lg-2 col-sm-12">
					{%label%}
				</label>
				<div class="control-input col-md-10 col-lg-10 col-sm-12">
					<input class="form-control custom-text" placeholder="" id="{%_id%}" type="text" data-id="{%_id%}" value="{%value%}">
					{% #is_required %}
						<span class="help-block text-danger">*Wajib diisi</span>
					{% /is_required %}
				</div>
			</div>
		{% /is_text %}

		{% #is_option %}
			<div class="form-group col-lg-12">
				<label for="{%label%}" class="control-label col-md-2 col-lg-2 col-sm-12 text-semibold">
				{% label %}
				</label>
				<div class="col-md-10 col-lg-10 col-sm-12">
					<select id="{%_id%}" class="select-search-2 custom-select" data-id="{%_id%}">
						<option value="">-- Pilih Jenis {%label%} --</option>
						{% #default_value %}
							<option value="{% id %}" {% #selected %}selected{% /selected %}>{% name %}</option>
						{% /default_value %}
					</select>
					{% #is_required %}
						<span class="help-block text-danger">*Wajib diisi</span>
					{% /is_required %}
				</div>
				
			</div>
		{% /is_option %}

		{% #is_number %}
			<div class="form-group col-lg-12">
				<label for="{%label%}" class="text-semibold control-label col-md-2 col-lg-2 col-sm-12">
					{%label%}
				</label>
				<div class="control-input col-md-10 col-lg-10 col-sm-12">
					<input class="form-control custom-text input-number" placeholder="" id="{%_id%}" type="text" data-id="{%_id%}" value="{%value%}">
					{% #is_required %}
						<span class="help-block text-danger">*Wajib diisi</span>
					{% /is_required %}
				</div>
			</div>
		{% /is_number %}
		
	{%/item%}
</script>