<div id="{{ $name }}Modal" data-backdrop="static" data-keyboard="false" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
			</div>
				<div class="modal-body">
					@include('form.date', [
						'field' => 'receiving_date',
						'label' => 'Tanggal Penerimaan',
						'label_col' => 'col-md-2 col-lg-2 col-sm-12',
						'form_col' => 'col-md-10 col-lg-10 col-sm-12',
						'class' => 'daterange-single',
						'placeholder' => 'dd/mm/yyyy',
						'attributes' => [
							'id' => 'receiving_date'
						]
					])
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
				</div>
		</div>
	</div>
</div>
