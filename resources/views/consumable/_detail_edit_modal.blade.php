<div id="editDetailModal" data-backdrop="static" data-keyboard="false" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
			</div>
				<div class="modal-body">
				{!!
					Form::open([
						'role' 		=> 'form',
						'url' 		=> '#',
						'method' 	=> 'post',
						'class' 	=> 'form-horizontal',
						'id' 		=> 'detail_form'
					])
				!!}
					@include('form.text', [
						'field' => 'value',
						'label' => 'Nilai',
						'label_col' => 'col-md-2 col-lg-2 col-sm-12',
						'form_col' => 'col-md-10 col-lg-10 col-sm-12',
						'attributes' => [
							'id' => 'detail_insert_value'
						]
					])

					@include('form.text', [
						'field' => 'no_asset',
						'label' => 'No. asset',
						'label_col' => 'col-md-2 col-lg-2 col-sm-12',
						'form_col' => 'col-md-10 col-lg-10 col-sm-12',
						'attributes' => [
							'id' => 'detail_insert_no_asset'
						]
					])
				</div>
				
				{!! Form::hidden('_detail_consumable_asset_id',null, array('id' => '_detail_consumable_asset_id')) !!}
				<div class="modal-footer">
					<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Simpan <i class="icon-arrow-right14 position-right"></i></button>
				</div>
				{!! Form::close() !!}
		</div>
	</div>
</div>
