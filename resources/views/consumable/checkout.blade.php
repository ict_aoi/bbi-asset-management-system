@extends('layouts.app',['active' => 'consumable'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Check-Out</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Beranda</a></li>
            <li ><a href="{{ route('consumable.index') }}">Consumable</a></li>
            <li class="active">Check-Out</li>
        </ul>
    </div>
</div>
@endsection
@section('page-content')
<div class="panel panel-flat">
    <div class="panel-body">
			<div class="row">
				<div class="col-md-6">
					<p>No. Asset (accounting) :  <b>{{$consumable_asset->no_asset}}</b></p>
                    <p>Asset Company :  <b>{{ $consumable_asset->company->name }}</b></p>
					<p>Asset Subdepartment :  <b>{{ $consumable_asset->subDepartment->name }}</b></p>
                    <p>Model :  <b>{{ $consumable_asset->model }}</b></p>
					<p>No. Serial : <b>{{ $consumable_asset->serial_number}}</b></p> 
            	</div>
				<div class="col-md-6">
                     <p>Total : <b>{{$consumable_asset->total}}</b></p> 
                     <p>Total Used : <b>{{$consumable_asset->qty_used}}</b></p> 
                     <p>Total Available : <b>{{$consumable_asset->qty_available}}</b></p> 
				</div>
			</div>
		</div>
</div>

<div class="panel panel-flat">
    <div class="panel-body">
        {!!
            Form::open([
                'role' => 'form',
                'url' => route('consumable.storeCheckout',$consumable_asset->id),
                'method' => 'post',
                'class' => 'form-horizontal',
                'id'=> 'form'
            ])
        !!}
        <div class="row">
            <div class="col-md-6">
                @include('form.text', [
                    'field'      => 'company',
                    'label'      => 'Company',
                    'default'    => $consumable_asset->company->name,
                    'label_col'  => 'col-md-3 col-lg-3 col-sm-12',
                    'form_col'   => 'col-md-9 col-lg-9 col-sm-12',
                    'attributes' => [
                        'id'       => 'company',
                        'readonly' => 'readonly',
                    ]
                ]) 

                @include('form.text', [
                    'field'      => 'sub_department',
                    'label'      => 'Sub Departement',
                    'default'    => $consumable_asset->subDepartment->name,
                    'label_col'  => 'col-md-3 col-lg-3 col-sm-12',
                    'form_col'   => 'col-md-9 col-lg-9 col-sm-12',
                    'attributes' => [
                        'id'       => 'sub_department',
                        'readonly' => 'readonly',
                    ]
                ]) 
                @include('form.picklist', [
                    'field'       => 'detail',
                    'label'       => 'Detail',
                    'name'        => 'detail',
                    'placeholder' => 'Pilih Detail',
                    'div_class'   => (!$consumable_asset_detail)? 'hidden': '',
                    'label_col'   => 'col-md-3 col-lg-3 col-sm-12',
                    'form_col'    => 'col-md-9 col-lg-9 col-sm-12',
                    'attributes' => [
                        'id' => 'detail',
                    ]
                ])
                
                <div class="form-group col-lg-12">
                    <label for="nik" class="text-semibold control-label col-md-3 col-lg-3 col-sm-12">
                        Check-Out To
                    </label>
                    <div class="control-input col-md-9 col-lg-9 col-sm-12">
                        <div class="btn-group" data-toggle="buttons">
                            <button type="button" class="btn bg-slate-700 btn-raised legitRipple" id="btn-user" data-popup="tooltip" title="User" data-placement="bottom" data-original-title="user"><i class=" icon-users position-left active"></i></button>
                            <button type="button" class="btn btn-danger btn-raised legitRipple" id="btn-asset" data-popup="tooltip" title="Asset" data-placement="bottom" data-original-title="asset"><i class=" icon-cabinet position-right"></i></button>
                        </div>
                    </div>
                </div>

                @include('form.date', [
                    'field'             => 'checkout_date',
                    'label'             => 'Checkout Date',
                    'label_col'         => 'col-md-3 col-lg-3 col-sm-12',
                    'form_col'          => 'col-md-9 col-lg-9 col-sm-12',
                    'placeholder'       => 'dd/mm/yyyy',
                    'attributes'        => [
                        'id'            => 'checkout_date',
                        'readonly'      => 'readonly'
                    ]
                ])
                
             
                <div class="form-group col-lg-12" id="nik">
                    <label for="nik" class="text-semibold control-label col-md-3 col-lg-3 col-sm-12">
                        NIK
                    </label>
                    <div class="control-input col-md-9 col-lg-9 col-sm-12">
                        <div class="form-group has-feedback has-feedback-right">
                            <input type="text" class="form-control" id="employee_nik" name="employee_nik">
                            <div class="form-control-feedback">
                                <i class="icon-search4 text-size-base"></i>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="information hidden">
                    @include('form.text', [
                        'field'      => 'employee_name',
                        'label'      => 'Nama',
                        'label_col'  => 'col-md-3 col-lg-3 col-sm-12',
                        'form_col'   => 'col-md-9 col-lg-9 col-sm-12',
                        'attributes' => [
                            'id'       => 'employee_name',
                            'readonly' => 'readonly'
                        ]
                    ])

                    @include('form.text', [
                        'field'      => 'employee_sub_department',
                        'label'      => 'Subdepartment',
                        'label_col'  => 'col-md-3 col-lg-3 col-sm-12',
                        'form_col'   => 'col-md-9 col-lg-9 col-sm-12',
                        'attributes' => [
                            'id'       => 'employee_sub_department',
                            'readonly' => 'readonly'
                        ]
                    ]) 
                </div>

                <div class="row">
                    <div class="form-group col-lg-12 hidden" id="asset">
                        @include('form.picklist', [
                            'field'       => 'barcode',
                            'label'       => 'Barcode',
                            'name'        => 'barcode',
                            'placeholder' => 'Scan Barcode here',
                            'label_col'   => 'col-md-3 col-lg-3 col-sm-12',
                            'form_col'    => 'col-md-9 col-lg-9 col-sm-12',
                            'attributes' => [
                                'id' => 'barcode',
                            ]
                        ])

                        @include('form.text', [
                            'field'      => 'no_inv',
                            'label'      => 'No. Inventory',
                            'label_col'  => 'col-md-3 col-lg-3 col-sm-12',
                            'form_col'   => 'col-md-9 col-lg-9 col-sm-12',
                            'attributes' => [
                                'id'       => 'no_inv',
                                'readonly' => 'readonly'
                            ]
                        ])
                        @include('form.text', [
                            'field'      => 'no_asset',
                            'label'      => 'No. Asset',
                            'label_col'  => 'col-md-3 col-lg-3 col-sm-12',
                            'form_col'   => 'col-md-9 col-lg-9 col-sm-12',
                            'attributes' => [
                                'id'       => 'no_asset',
                                'readonly' => 'readonly'
                            ]
                        ]) 
                    </div>
                </div>
                
            </div>
            <div class="col-md-6">
                <div id="draw"></div>
            </div>
        </div>
        
        <br/>

        {!! Form::hidden('checkout_to_status',null, array('id' => 'checkout_to_status')) !!}
        {!! Form::hidden('flag_detail',$consumable_asset_detail, array('id' => 'flag_detail')) !!}
        {!! Form::hidden('detail_id',null, array('id' => 'detail_id')) !!}
        {!! Form::hidden('code_factory',null, array('id' => 'code_factory')) !!}
        {!! Form::hidden('asset_id', null, array('id' => 'asset_id')) !!}
        {!! Form::hidden('consumable_asset_id',$consumable_asset->id , array('id' => 'consumable_asset_id')) !!}
        {!! Form::hidden('sub_department_id', $consumable_asset->sub_department_id, array('id' => 'sub_department_id')) !!}
        {!! Form::hidden('company_id', $consumable_asset->company_id, array('id' => 'company_id')) !!}
            <div class="text-right">
            <button type="submit" class="btn btn-primary legitRipple col-xs-12">Save <i class="icon-floppy-disk position-right"></i></button>
            </div>
        {!! Form::close() !!}

        {!! Form::hidden('custom_fields', json_encode($array), array('id' => 'custom_fields')) !!}
        {!! Form::hidden('auto_completes', '[]', array('id' => 'auto_completes')) !!}
        
    </div>
</div>

@endsection

@section('page-modal')
    @include('form.modal_picklist', [
        'name' => 'barcode',
        'title' => 'Select Asset',
        'placeholder' => 'Search based on no. inventory /no. inventory manual/no asset/ nomor serial',
    ])
    @include('form.modal_picklist', [
        'name' => 'detail',
        'title' => 'List Asset',
        'placeholder' => 'Search based on serial number/no asset',
    ])
    @include('consumable._custom_fields')
@endsection

@section('page-js')
<script src="{{ mix('js/datepicker.js') }}"></script>
<script src="{{ mix('js/consumable_checkout.js') }}"></script>
@endsection