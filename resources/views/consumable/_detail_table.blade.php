<script type="x-tmpl-mustache" id="detail_table">
	{% #item %}
		<tr>
			<td style="text-align:center;">{% no %}</td>
			<td>
				{% detail_value %}
			</td>
			<td>
				{% detail_no_asset %}
			</td>
			<td>
				<ul class="icons-list">
					<li><a href="#" id="edit_{% _id %}" data-id="{% _id %}" class="btn-edit-item"><i class="icon-pencil7"></i></a></li>
					<li><a href="#" id="delete_{% _id %}" data-id="{% _id %}" class="btn-delete-item"><i class="icon-trash"></i></a></li>
				</ul>

			</td>
		</tr>
	{%/item%}
</script>