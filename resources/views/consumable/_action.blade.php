
<ul class="icons-list">
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-menu9"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-left">
            <li><a href="{!! $edit !!}" ><i class="icon-pencil6"></i> Edit</a></li>
            <li><a href="{!! $checkin !!}" ><i class="icon-database-insert"></i> Check-in</a></li>
            <li><a href="{!! $checkout !!}" ><i class="icon-database-export"></i> Check-out</a></li>
            <li><a href="{!! $history !!}" ><i class="icon-history"></i> History</a></li>
            <li><a href="#" onclick="hapus('{!! $delete !!}')"><i class="icon-trash"></i> Delete</a></li>
        </ul>
    </li>
</ul>
