<table class="table table-striped table-hover table-responsive">
	<thead>
	  <tr>
			<th>No</th>
			<th>Tanggal Penerimaan</th>
			<th>Erp No Inventori</th>
			<th>No Po Supplier</th>
			<th>Nama Supplier</th>
			<th>Kode Item</th>
			<th>Deskripsi Item</th>
			<th>Erp Group Asset</th>
			<th>Aksi</th>
		</tr>
	</thead>

	<tbody>
		@foreach ($lists as $key => $list)
			<tr>
				<td>
					{{ $key+1 }}
				</td>
				<td>
					{{ \Carbon\Carbon::createFromFormat('Y-m-d', $list->mr_date)->format('d/M/Y') }}
				</td>
				<td>
					{{ $list->inventoryno }}
				</td>
				<td>
					{{ $list->po_supplier }}
				</td>
				<td>
					{{ $list->supplier_name }}
				</td>
				<td>
					{{ $list->item_code }}
				</td>
				<td>
					{{ $list->item_desc }}
				</td>
				<td>
					{{ $list->asset_group }}
				</td>
				<td>
					<button data-dismiss="modal" class="btn btn-info btn-xs btn-choose" 
						type="button" 
						data-id="{{ $list->a_asset_id }}"
						data-name="{{ $list->inventoryno }}" 
						data-itemid="{{ $list->m_product_id }}"
						data-itemcode="{{ $list->item_code }}"
						data-assetgroupid="{{ $list->a_asset_group_id }}"
						data-assetgroupname="{{ $list->asset_group }}"
						data-receivingdate="{{ $list->mr_date}}"
						data-posupplier="{{ $list->po_supplier }}"
						data-itemdesc="{{ $list->item_desc }}"
						data-suppliername="{{ $list->supplier_name }}"
					>
							Select
					</button>
				</td>
				
			</tr>
		@endforeach
	</tbody>
</table>

{!! $lists->appends(Request::except('page'))->render() !!}
