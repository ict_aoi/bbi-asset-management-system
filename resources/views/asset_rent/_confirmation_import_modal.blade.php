<div id="confirmationModal" data-backdrop="static" data-keyboard="false" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
			</div>
				{!!
					Form::open([
						'role' 			=> 'form',
						'url' 			=> route('assetRent.exportFormImport'),
						'method' 		=> 'get',
						'class' 		=> 'form-horizontal',
					])
				!!}
				<div class="modal-body">
					@include('form.select', [
						'field' 		=> 'company',
						'label' 		=> 'Company',
						'default' 		=> $company_id,
						'label_col'		=> 'col-md-2 col-lg-2 col-sm-12',
						'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
						'div_class' 	=> (auth::user()->is_super_admin)? '':'hidden',
						'options' 		=> [
							'' 			=> '-- Select Company --',
						]+$companies,
						'class' 		=> 'select-search',
						'attributes'	=> [
							'id' 		=> 'select_company'
						]
					])

					@include('form.select', [
						'field' 		=> 'factory',
						'label' 		=> 'Factory',
						'default' 		=> $factory_id,
						'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
						'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
						'div_class'		=> (auth::user()->is_super_admin)? '':'hidden',
						'options' 		=> [
							'' 			=> '-- Select Factory --',
						]+$factories,
						'class' 		=> 'select-search',
						'attributes' 	=> [
							'id' 		=> 'select_factory'
						]
					])

					@include('form.select', [
						'field' 		=> 'department',
						'label' 		=> 'Department',
						'default' 		=> $department_id,
						'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
						'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
						'div_class' 	=> (auth::user()->is_super_admin)? '':'hidden',
						'options' 		=> [
							'' 			=> '-- Select Department --',
						]+$departments,
						'class'			=> 'select-search',
						'attributes' 	=> [
							'id' 		=> 'select_department'
						]
					])

					@include('form.select', [
						'field' 		=> 'asset_type',
						'label' 		=> 'Asset Type',
						'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
						'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
						'options' 		=> [
							'' 			=> '-- Select Asset Type --',
						],
						'class' 		=> 'select-search',
						'attributes' 	=> [
							'id' 		=> 'select_asset_type'
						]
					])
					
				</div>
				
				{!! Form::hidden('factory_id', $factory_id, array('id' => 'factory_id')) !!}
				{!! Form::hidden('department_id', $department_id, array('id' => 'department_id')) !!}
				<div class="modal-footer">
					<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Get form upload</button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
