@extends('layouts.app',['active' => 'asset_rent'])

@section('page-css')
    
@endsection

@section('page-header')
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Asset Rent</span></h4>
            </div>
        </div>
        <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
            <ul class="breadcrumb">
                <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Dashboard</a></li>
                <li class="active">Asset Rent</li>
            </ul>

            <ul class="breadcrumb-elements">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle legitRipple breadcrumb-dropdown" data-toggle="dropdown">
                        <i class="icon-three-bars position-left"></i>
                        Actions
                        <span class="caret"></span>
                    </a>
                    
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="{{ route('assetRent.create') }}"><i class="icon-plus2 pull-right"></i> Create</a></li>
                        <li><a href="#" data-toggle="modal" data-target="#confirmationModal"><i class="icon-barcode2 pull-right"></i> Print Barcode</a></li>
                        <li><a href="{{ route('assetRent.import') }}"><i class="icon-drawer-out pull-right"></i> Import</a></li>
                        @if(auth::user()->is_super_admin)
                            <li><a href="#" data-toggle="modal" data-target="#exportModal"><i class="icon-file-excel pull-right"></i> Export</a></li>
                        @else
                            <li><a href="#" id="export_report_button"><i class="icon-file-excel pull-right"></i> Export</a></li>
                           
                            {!!
                                Form::open([
                                    'url' 		=> route('assetRent.exportReport'),
                                    'method' 	=> 'get',
                                    'class'     => 'hidden',
                                    'id'        => 'download_export_report_excel',
                                ])
                            !!}
                                {!! Form::hidden('sub_department_id', auth::user()->sub_department_id, array('id' => 'sub_department_id')) !!}
                            {!! Form::close() !!}
                        @endif
                        
                        
                    </ul>
                </li>
            </ul>
        </div>
    </div>
@endsection

@section('page-content')
    <div class="panel panel-flat panel-collapsed">
        <div class="panel-heading">
            <h5 class="panel-title">Filters   <a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            
                @include('form.select', [
                    'field'         => 'company',
                    'label'         => 'Company',
                    'default'       => $company_id,
                    'mandatory'     => '*Required',
                    'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                    'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                    'div_class'     => (auth::user()->is_super_admin)? '':'hidden',
                    'options' => [
                        ''          => '-- Select Company --',
                    ]+$companies,
                    'class'         => 'select-search',
                    'attributes'    => [
                        'id'        => 'select_company'
                    ]
                ])

                @include('form.select', [
                    'field'         => 'factory',
                    'label'         => 'Factory',
                    'mandatory'     => '*Required',
                    'default'       => $factory_id,
                    'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                    'div_class'     => (auth::user()->is_super_admin)? '':'hidden',
                    'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                    'options'       => [
                        ''          => '-- Select Factory --',
                    ]+$factories,
                    'class'         => 'select-search',
                    'attributes'    => [
                        'id'        => 'select_factory'
                    ]
                ])

                @include('form.select', [
                    'field'         => 'sub_department',
                    'label'         => 'Subdepartment',
                    'default'       => $sub_department_id,
                    'mandatory'     => '*Required',
                    'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                    'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                    'div_class'     => (auth::user()->is_super_admin)? '':'hidden',
                    'options'       => [
                        ''          => '-- Select Subdepartment --',
                    ]+$sub_departments,
                    'class'         => 'select-search',
                    'attributes'    => [
                        'id'        => 'select_sub_department'
                    ]
                ])

                @include('form.select', [
                    'field'             => 'asset_type',
                    'label'             => 'Asset Type',
                    'mandatory'         => '*Required',
                    'label_col'         => 'col-md-2 col-lg-2 col-sm-12',
                    'form_col'          => 'col-md-10 col-lg-10 col-sm-12',
                    'options'           => [
                        ''              => '-- Select Asset Type --',
                    ],
                    'class'             => 'select-search',
                    'attributes'        => [
                        'id'            => 'select_asset_type'
                    ]
                ])

                @include('form.select', [
                    'field'         => 'status',
                    'label'         => 'Status',
                    'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                    'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                    'options'       => [
                        ''          => '-- Select Status Asset --',
                    ],
                    'class'         => 'select-search',
                    'attributes'    => [
                        'id'        => 'select_status_asset'
                    ]
                ])
        </div>
    </div>

    <div class="panel panel-flat">
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-basic table-condensed" id="assetRentTable">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Id</th>
                            <th>Budget</th>
                            <th>Arrival Date</th>
                            <th>No. KK Rent</th>
                            <th>Total Rent Price</th>
                            <th>Supplier Name</th>
                            <th>Asset Type</th>
                            <th>Model</th>
                            <th>Serial Number</th>
                            <th>Start Date Rent</th>
                            <th>End Date Rent</th>
                            <th>Return Date Rent</th>
                            <th>Remark Rent</th>
                            <th>Status</th>
                            <th>Last Company Location</th>
                            <th>Last Factory Location</th>
                            <th>Last Department Location</th>
                            <th>Last Subdepartment Location</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    {!! Form::hidden('form_status', 'index', array('id' => 'form_status')) !!}
    {!! Form::hidden('is_super_admin', (auth::user()->is_super_admin)? '1':'0', array('id' => 'is_super_admin')) !!}
    {!! Form::hidden('msg', $msg, array('id' => 'msg')) !!}
    {!! Form::hidden('custom_fields', '[]', array('id' => 'custom_fields')) !!}
@endsection

@section('page-modal')
    @include('asset_rent._confirmation_barcode_modal')
    @include('asset_rent._export_modal')
@endsection

@section('page-js')
    <script src="{{ mix('js/datepicker.js') }}"></script>
    <script src="{{ mix('js/asset_rent.js') }}"></script>
@endsection
