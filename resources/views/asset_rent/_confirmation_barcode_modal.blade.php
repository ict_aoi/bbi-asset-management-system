<div id="confirmationModal" data-backdrop="static" data-keyboard="false" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
			</div>
				{!!
					Form::open([
						'role' 			=> 'form',
						'url' 			=> route('asset.printBarcode'),
						'method' 		=> 'post',
						'target' 		=> '_blank',
						'class' 		=> 'form-horizontal',
					])
				!!}
				<div class="modal-body">
					@include('form.select', [
						'field' 		=> 'company',
						'label' 		=> 'Company',
						'default' 		=> $company_id,
						'label_col'		=> 'col-md-2 col-lg-2 col-sm-12',
						'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
						'div_class' 	=> (auth::user()->is_super_admin)? '':'hidden',
						'options' 		=> [
							'' 			=> '-- Select Company --',
						]+$companies,
						'class' 		=> 'select-search',
						'attributes'	=> [
							'id' 		=> 'select_barcode_company'
						]
					])

					@include('form.select', [
						'field' 		=> 'factory',
						'label' 		=> 'Factory',
						'default' 		=> $factory_id,
						'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
						'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
						'div_class'		=> (auth::user()->is_super_admin)? '':'hidden',
						'options' 		=> [
							'' 			=> '-- Select Factory --',
						]+$factories,
						'class' 		=> 'select-search',
						'attributes' 	=> [
							'id' 		=> 'select_barcode_factory'
						]
					])

					@include('form.select', [
						'field' 		=> 'sub_department',
						'label' 		=> 'Sub Departement',
						'default' 		=> $sub_department_id,
						'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
						'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
						'div_class' 	=> (auth::user()->is_super_admin)? '':'hidden',
						'options' 		=> [
							'' 			=> '-- Select Sub Departement --',
						]+$sub_departments,
						'class'			=> 'select-search',
						'attributes' 	=> [
							'id' 		=> 'select_barcode_department'
						]
					])

					@include('form.select', [
						'field' 		=> 'asset_type',
						'label' 		=> 'Asset Type',
						'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
						'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
						'options' 		=> [
							'' 			=> '-- Select Asset Type --',
						],
						'class' 		=> 'select-search',
						'attributes' 	=> [
							'id' 		=> 'select_barcode_asset_type'
						]
					])
					
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Print</button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
