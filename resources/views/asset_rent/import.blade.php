@extends('layouts.app',['active' => 'asset_rent'])

@section('page-css')

@endsection

@section('page-header')
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Asset Rent</span></h4>
            </div>
        </div>
        <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
            <ul class="breadcrumb">
                <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Dashboard</a></li>
                <li ><a href="{{ route('assetRent.index') }}">Asset Rent</a></li>
                <li class="active">Import</li>
            </ul>
        </div>
    </div>
@endsection

@section('page-content')

    <div class="panel panel-flat">
        <div class="panel-heading">
                <h6 class="panel-title">&nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
                <div class="heading-elements">
                    <a href="#" class="btn btn-primary btn-icon"  data-toggle="modal" data-target="#confirmationModal" data-popup="tooltip" title="download form import" data-placement="bottom" data-original-title="download form import"><i class="icon-download"></i></a>
                    <button type="button" class="btn btn-success btn-icon" id="upload_button" data-popup="tooltip" title="upload form import" data-placement="bottom" data-original-title="upload form import"><i class="icon-upload"></i></button>

                    {!!
                        Form::open([
                            'role'      => 'form',
                            'url'       => route('assetRent.uploadFormImport'),
                            'method'    => 'POST',
                            'id'        => 'upload_file_allocation',
                            'enctype'   => 'multipart/form-data'
                        ])
                    !!}
                        {!! Form::hidden('current_time',null, array('id' => 'current_time')) !!}
                        <input type="file" class="hidden" id="upload_file" name="upload_file" accept=".csv, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
                        <button type="submit" class="btn btn-success hidden btn-lg" id="submit_button" >Submit</button>
                    {!! Form::close() !!}

                </div>
            </div>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table table-striped">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Budget</th>
                            <th>Arrival Date</th>
                            <th>No KK Rent</th>
                            <th>Start Date Rent</th>
                            <th>End Date Rent</th>
                            <th>Total Rent Price</th>
                            <th>Rent Duration (in day)</th>
                            <th>Supplier Name</th>
                            <th>Asset Type</th>
                            <th>Model</th>
                            <th>Serial Number</th>
                            <th>System log</th>
                        </tr>
                    </thead>
                    <tbody id="draw"></tbody>
                </table>
            </div>
        </div>
    </div>
    {!! Form::hidden('result', '[]', array('id' => 'result')) !!}

    {!!
        Form::open([
            'role'      => 'form',
            'url'       => route('asset.printBarcode'),
            'method'    => 'post',
            'class'     => 'form-horizontal',
            'id'        => 'printBarcode',
            'target'    => '_blank'
        ])
    !!}
        {!! Form::hidden('list_barcodes','[]' , array('id' => 'list_barcodes')) !!}
        <div class="form-group text-right" style="margin-top: 10px;">
            <button type="submit" class="btn hidden">Print <i class="icon-printer position-left"></i></button>
        </div>
    {!! Form::close() !!}

@endsection

@section('page-modal')
    @include('asset_rent._confirmation_import_modal')
    @include('asset_rent._import')
@endsection

@section('page-js')
    <script src="{{ mix('js/import_asset_rent.js') }}"></script>
@endsection
