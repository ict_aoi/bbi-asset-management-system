<!DOCTYPE html>
    <html>
    <head>
        <title>Barcode :: Asset</title>
        <link rel="stylesheet" type="text/css" href="{{ asset(elixir('css/barcode_asset.css')) }}">
    </head>
    <body>
        @foreach($assets  as $key => $asset)
            <div class="outer">
                <span class="text" >
                    <span style="font-size: 10px">{{ $asset->no_inventory }}</span>
                </span><br/>
                <span class="text" >
                    <span style="font-size: 10px">{{ $asset->assetType->name }}</span>
                </span>
                <span style="padding-left: 10px">
                    <img style="width: 5.5cm;margin-top:5px" src="data:image/png;base64,{{ DNS1D::getBarcodePNG("$asset->barcode", 'C128') }}" alt="barcode"   />
                </span>
                <span style="padding-left: 15px">
                <span style="font-size: 10px">{{ $asset->barcode }} | <span style="font-size: 10px">{{ ($asset->serial_number ? $asset->serial_number : '-')  }}</span>
                </span>
            </div>
        @endforeach
    </body>
</html>