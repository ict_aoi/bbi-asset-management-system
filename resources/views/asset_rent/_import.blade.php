<script type="x-tmpl-mustache" id="upload_asset">
	{% #item %}
		<tr {% #is_error %} style="background-color:#FF5722;color:#fff" {% /is_error %}>
			<td>{% no %}</td>
			<td>
				{% budget %}
			</td>
			<td>
				{% receiving_date %}
			</td>
			<td>
				{% no_rent %}
			</td>
			<td>
				{% start_rent_date %}
			</td>
			<td>
				{% end_rent_date %}
			</td>
			<td>
				{% price_rent %}
			</td>
			<td>
				{% duration_rent_in_days %}
			</td>
			<td>
				{% supplier_name %}
			</td>
			<td>
				{% asset_type_name %}
			</td>
			<td>
				{% model %}
			</td>
			<td>
				{% serial_number %}
			</td>
			<td>
				{% system_log %}
			</td>
		</tr>
	{%/item%}
</script>