@extends('layouts.app',['active' => 'asset_rent'])

@section('page-header')
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Asset Rent</span></h4>
            </div>
        </div>
        <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
            <ul class="breadcrumb">
                <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Dashboard</a></li>
                <li ><a href="{{ route('assetRent.index') }}">Asset Rent</a></li>
                <li class="active">Edit</li>
            </ul>
        </div>
    </div>
@endsection

@section('page-content')
    <div class="panel panel-flat">
        <div class="panel-body">
            {!!
                Form::open([
                    'role'      => 'form',
                    'url'       => route('assetRent.update',$asset->id),
                    'method'    => 'post',
                    'class'     => 'form-horizontal',
                    'enctype'   => 'multipart/form-data',
                    'id'        => 'form'
                ])
            !!}
            <div class="row">
                <div class="col-md-6">
                    @include('form.select', [
                        'field'         => 'company',
                        'label'         => 'Company',
                        'default'       => $asset->origin_company_location_id,
                        'mandatory'     => '*Required',
                        'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                        'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                        'div_class'     => (auth::user()->is_super_admin)? '':'hidden',
                        'options'       => [
                            ''          => '-- Select Company --',
                        ]+$companies,
                        'class'         => 'select-search',
                        'attributes'    => [
                            'id'        => 'select_company',
                            'disabled'  => 'disabled'
                        ]
                    ])

                    @include('form.select', [
                        'field'         => 'factory',
                        'label'         => 'Factory',
                        'mandatory'     => '*Required',
                        'default'       => $asset->origin_factory_location_id,
                        'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                        'div_class'     => (auth::user()->is_super_admin)? '':'hidden',
                        'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                        'options'       => [
                            ''          => '-- Select Factory --',
                        ]+$factories,
                        'class'         => 'select-search',
                        'attributes'    => [
                            'id'        => 'select_factory',
                            'disabled'  => 'disabled'
                        ]
                    ])

                    @include('form.select', [
                        'field'         => 'sub_department',
                        'label'         => 'Subdepartment',
                        'default'       => $asset->origin_sub_department_location_id,
                        'mandatory'     => '*Required',
                        'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                        'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                        'div_class'     => (auth::user()->is_super_admin)? '':'hidden',
                        'options'       => [
                            ''          => '-- Select Subdepartment --',
                        ]+$sub_departments,
                        'class'         => 'select-search',
                        'attributes'    => [
                            'id'        => 'select_sub_department',
                            'disabled'  => 'disabled'
                        ]
                    ])

                    @include('form.select', [
                        'field'         => 'asset_type',
                        'label'         => 'Asset Type',
                        'mandatory'     => '*Required',
                        'default'       => $asset->asset_type_id,
                        'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                        'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                        'options'       => [
                            ''          => '-- Select Asset Type --',
                        ]+$asset_types,
                        'class'         => 'select-search',
                        'attributes'    => [
                            'id'        => 'select_asset_type',
                            'disabled'  => 'disabled'
                        ]
                    ])

                    @include('form.select', [
                        'field'         => 'budget',
                        'label'         => 'Budget',
                        'mandatory'     => '*Required',
                        'default'       => $asset->budget_id,
                        'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                        'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                        'options'       => [
                            ''          => '-- Select Budget --',
                        ],
                        'class'         => 'select-search',
                        'attributes'    => [
                            'id'        => 'select_budget'
                        ]
                    ])

                    @include('form.text', [
                        'field'         => 'no_kk_rent',
                        'label'         => 'No. KK Rent',
                        'default'       => $asset->no_rent,
                        'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                        'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                        'attributes'    => [
                            'id'        => 'no_kk_rent',
                            'readonly'  => 'readonly'
                        ]
                    ])

                    @include('form.select', [
                        'field'         => 'duration',
                        'label'         => 'Rent Duration',
                        'mandatory'     => '*Required',
                        'default'       => $asset->duration_rent_in_days,
                        'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                        'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                        'options'       => [
                            ''            => '-- Select Duration --',
                            '14'          => '2 Week',
                            '30'          => '1 Month',
                        ],
                        'class'         => 'select-search',
                        'attributes'    => [
                            'id'        => 'select_durationt'
                        ]
                    ])

                    @include('form.text', [
                        'field'         => 'price_rent',
                        'label'         => 'Price Rent',
                        'mandatory'     => '*Required',
                        'default'       => $asset->price,
                        'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                        'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                        'attributes'    => [
                            'id'        => 'price_rent'
                        ]
                    ])

                    @include('form.date', [
                        'field'             => 'arrival_date',
                        'label'             => 'Arrival Date',
                        'default'           => ($asset->receiving_date ? $asset->receiving_date->format('d/m/Y') : null ),
                        'label_col'         => 'col-md-3 col-lg-3 col-sm-12',
                        'form_col'          => 'col-md-9 col-lg-9 col-sm-12',
                        'class'             => 'daterange-single',
                        'placeholder'       => 'dd/mm/yyyy',
                        'attributes'        => [
                            'id'            => 'arrival_date',
                            'readonly'      => 'readonly'
                        ]
                    ])

                    @include('form.date', [
                        'field'             => 'start_rent_date',
                        'label'             => 'Start Rent Date',
                        'default'           => ($asset->start_rent_date ? $asset->start_rent_date->format('d/m/Y') : null),
                        'label_col'         => 'col-md-3 col-lg-3 col-sm-12',
                        'form_col'          => 'col-md-9 col-lg-9 col-sm-12',
                        'class'             => 'daterange-single',
                        'placeholder'       => 'dd/mm/yyyy',
                        'attributes'        => [
                            'id'            => 'start_rent_date',
                            'readonly'      => 'readonly'
                        ]
                    ])

                    
                    @include('form.date', [
                        'field'             => 'end_rent_date',
                        'label'             => 'End Rent Date',
                        'default'           => ($asset->end_rent_date ? $asset->end_rent_date->format('d/m/Y') : null),
                        'label_col'         => 'col-md-3 col-lg-3 col-sm-12',
                        'form_col'          => 'col-md-9 col-lg-9 col-sm-12',
                        'class'             => 'daterange-single',
                        'placeholder'       => 'dd/mm/yyyy',
                        'attributes'        => [
                            'id'            => 'end_rent_date',
                            'readonly'      => 'readonly'
                        ]
                    ])


                    @include('form.text', [
                        'field'         => 'supplier_name',
                        'label'         => 'Suplier Name',
                        'default'       => $asset->supplier_name,
                        'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                        'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                        'attributes'    => [
                            'id'        => 'supplier_name'
                        ]
                    ])
                    
                </div>

                <div class="col-md-6">
                    @include('form.select', [
                        'field'         => 'manufacture',
                        'label'         => 'Pabrikan',
                        'default'       => $asset->manufacture_id,
                        'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                        'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                        'options'       => [
                            ''          => '-- Pilih Pabrikan --',
                        ]+$manufactures,
                        'class'         => 'select-search',
                        'attributes'    => [
                            'id'        => 'select_manufacture'
                        ]
                    ])

                    @include('form.text', [
                        'field'         => 'model',
                        'label'         => 'Model',
                        'default'       => $asset->model,
                        'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                        'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                        'mandatory'     => '*Required',
                        'attributes'    => [
                            'id'        => 'model'
                        ]
                    ])

                    @include('form.text', [
                        'field'         => 'serial_number',
                        'label'         => 'Serial Number',
                        'default'       => $asset->serial_number,
                        'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                        'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                        'mandatory'     => '*Required',
                        'attributes'    => [
                            'id'        => 'serial_number'
                        ]
                    ])

                    <div id="draw"></div>
                    
                    @include('form.file', [
                        'field'         => 'photo',
                        'label'         => 'Upload Image',
                        'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                        'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                        'help'          => 'Accepted formats: gif, png, jpg. Max file size 2Mb',
                        'attributes'    => [
                            'id'        => 'photo',
                            'accept'    => 'image/*'
                        ]
                    ])

                    
                </div>
            </div>
            {!! Form::hidden('form_status', 'edit', array('id' => 'form_status')) !!}
            {!! Form::hidden('current_time',null, array('id' => 'current_time')) !!}
            {!! Form::hidden('asset_id', $asset->id, array('id' => 'asset_id')) !!}
            {!! Form::hidden('budget_id', $asset->budget_id, array('id' => 'budget_id')) !!}
            {!! Form::hidden('asset_group_id', $asset->erp_asset_group_id, array('id' => 'asset_group_id')) !!}
            {!! Form::hidden('asset_group_name', $asset->erp_asset_group_name, array('id' => 'asset_group_name')) !!}
            {!! Form::hidden('manufacture_id', $asset->manufacture_id, array('id' => 'manufacture_id')) !!}
            {!! Form::hidden('asset_type_id', $asset->asset_type_id, array('id' => 'asset_type_id')) !!}
            {!! Form::hidden('custom_fields', '[]', array('id' => 'custom_fields')) !!}
            {!! Form::hidden('company_id', $asset->origin_company_location_id, array('id' => 'company_id')) !!}
            {!! Form::hidden('factory_id', $asset->factory_id, array('id' => 'factory_id')) !!}
            {!! Form::hidden('sub_department_id', $asset->sub_department_id, array('id' => 'sub_department_id')) !!}
                <div class="text-right">
                    <button type="submit" class="btn btn-primary legitRipple col-xs-12">Save <i class="icon-floppy-disk position-right"></i></button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('page-modal')
    @include('asset_rent._erp_asset_modal')
@endsection

@section('page-js')
    @include('asset_rent._fields')
    <script src="{{ mix('js/datepicker.js') }}"></script>
    <script src="{{ mix('js/asset_rent.js') }}"></script>
@endsection
