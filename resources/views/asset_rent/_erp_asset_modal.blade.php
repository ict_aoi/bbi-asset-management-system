<div id="erp_no_assetModal" data-backdrop="static" data-keyboard="false" class="modal fade">
	<div class="modal-dialog modal-full">
		<div class="modal-content">
				<div class="modal-header bg-indigo">
					<h5 class="modal-title"><p>List Asset Erp</h5>
				</div>
				<div class="modal-body">
					<div class="row">
						@include('form.date', [
							'field'             => 'receiving_date',
							'label'             => 'Tanggal Penerimaan',
							'label_col'         => 'col-xs-12',
							'default'			=> \Carbon\Carbon::now()->format('d/m/Y'),
							'form_col'          => 'col-xs-12',
							'class'             => 'daterange-single',
							'placeholder'       => 'dd/mm/yyyy',
							'attributes'        => [
								'id'            => 'receiving_date',
								'readonly'      => 'readonly'
							]
						])
					</div>

					<div class="row">
						<fieldset>
							<div class="form-group">
								<div class="col-xs-12">
									<div class="input-group">
										<input type="text" id="erp_no_assetSearch"  class="form-control" placeholder="Masukan kata kunci..." autofocus="true">
										<span class="input-group-btn">
											<button class="btn bg-teal legitRipple" type="button" id="erp_no_assetButtonSrc"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
										</span>
									</div>
									<span class="help-block">Silahkan cari berdasarkan nomor po supplier</span>
										
								</div>
							</div>
						</fieldset>
						<br/>
						<div class="shade-screen" style="text-align: center;">
							<div class="shade-screen hidden">
								<img src="/images/ajax-loader.gif">
							</div>
						</div>
						<div class="table-responsive" id="erp_no_assetTable"></div>
					</div>
					
					
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
				</div>
		</div>
	</div>
</div>
