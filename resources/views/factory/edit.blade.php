@extends('layouts.app',['active' => 'factory'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Factory</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Dashboard</a></li>
            <li>Organization</li>
            <li><a href="{{ route('factory.index') }}">Factory</a></li>
            <li class="active">Edit</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat">
    <div class="panel-body">
        {!!
            Form::open([
                'role'          => 'form',
                'url'           => route('factory.update',$factory->id),
                'method'        => 'post',
                'class'         => 'form-horizontal',
                'id'            => 'form'
            ])
        !!}
            @include('form.select', [
                'field'             => 'company',
                'label'             => 'Company',
                'default'           => $factory->company_id,
                'options'           => [
                    ''              => '-- Select Company --',
                ]+$companies,
                'mandatory'         => '*Required',
                'class'             => 'select-search',
                'attributes'        => [
                    'id'            => 'select_company'
                ]
            ])

            @if($factory->erp_accounting_location_id)
                @include('form.select', [
                'field'             => 'erp_accounting_location',
                'label'             => 'Accounting Location ERP',
                'default'           => $factory->erp_accounting_location_id,
                'mandatory'         => '*Required',
                'options'           => [
                    ''              => '-- Select Accounting Location ERP --',
                ]+$erp_accounting_locations,
                'class' => 'select-search',
                'attributes' => [
                    'id'        => 'select_erp_accounting_location',
                    'disabled'  => 'disabled'
                ]
            ])
            @else
                @include('form.select', [
                    'field'             => 'erp_accounting_location',
                    'label'             => 'Accounting Location ERP',
                    'mandatory'         => '*Required',
                    'options'           => [
                        ''              => '-- Select Accounting Location ERP --',
                    ]+$erp_accounting_locations,
                    'class' => 'select-search',
                    'attributes' => [
                        'id' => 'select_erp_accounting_location'
                    ]
                ])
            @endif

            
            @include('form.text', [
                'field'             => 'code',
                'label'             => 'Code',
                'default'           => strtoupper($factory->code),
                'mandatory'         => '*Required',
                'label_col'         => 'col-md-2 col-lg-2 col-sm-12',
                'form_col'          => 'col-md-10 col-lg-10 col-sm-12',
                'attributes'        => [
                    'id'            => 'code',
                    'readonly'      => 'readonly'
                ]
            ])

            @include('form.text', [
                'field'             => 'name',
                'label'             => 'Nama',
                'default'           => ucwords($factory->name),
                'mandatory'         => '*Required',
                'label_col'         => 'col-md-2 col-lg-2 col-sm-12',
                'form_col'          => 'col-md-10 col-lg-10 col-sm-12',
                'attributes'        => [
                    'id'            => 'name',
                    'readonly'      => 'readonly'
                ]
            ])

            @include('form.textarea', [
                'field'             => 'description',
                'label'             => 'Description',
                'default'           => ucwords($factory->description),
                'label_col'         => 'col-md-2 col-lg-2 col-sm-12',
                'form_col'          => 'col-md-10 col-lg-10 col-sm-12',
                'attributes'        => [
                    'id'            => 'description',
                    'rows'          => 5,
                    'style'         => 'resize: none;'
                ]
            ])

            <div class="text-right">
                <button type="submit" class="btn btn-primary legitRipple col-xs-12">Save <i class="icon-floppy-disk position-right"></i></button>
            </div>
        {!! Form::close() !!}
    </div>
</div>

@endsection

@section('page-js')
<script src="{{ mix('js/factory.js') }}"></script>
@endsection