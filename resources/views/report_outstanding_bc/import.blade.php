@extends('layouts.app',['active' => 'report-outstanding-bc'])

@section('page-css')

@endsection

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Report Outstanding BC</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
         <ul class="breadcrumb">
				<li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Dashboard</a></li>
            <li>Report</li>
            <li ><a href="{{ route('reportOutstandingBC.index') }}">Outstanding BC</a></li>
            <li class="active">Import</li>
        </ul>
    </div>
</div>
@endsection
@section('page-content')

    <div class="panel panel-flat">
    <div class="panel panel-flat">
        <div class="panel-heading">
                <h6 class="panel-title">&nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
                <div class="heading-elements">
                    @if(auth::user()->is_super_admin) <a href="#" class="btn btn-primary btn-icon"  data-toggle="modal" data-target="#confirmationImportModal" data-popup="tooltip" title="download form import" data-placement="bottom" data-original-title="download form import"><i class="icon-download"></i></a>
                    @else <a href="{{ route('reportOutstandingBC.downloadFormImport')}}" class="btn btn-primary btn-icon"  data-popup="tooltip" title="download form import" data-placement="bottom" data-original-title="download form import"><i class="icon-download"></i></a>
                    @endif

                    <button type="button" class="btn btn-success btn-icon" id="upload_button" data-popup="tooltip" title="upload form import" data-placement="bottom" data-original-title="upload form import"><i class="icon-upload"></i></button>

                    {!!
                        Form::open([
                            'role'      => 'form',
                            'url'       => route('reportOutstandingBC.uploadFormImport'),
                            'method'    => 'POST',
                            'id'        => 'form_upload_file',
                            'enctype'   => 'multipart/form-data'
                        ])
                    !!}
                        {!! Form::hidden('current_time', null, array('id' => 'current_time')) !!}
                        <input type="file" class="hidden" id="upload_file" name="upload_file" accept=".csv, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
                        <button type="submit" class="btn btn-success hidden btn-lg" id="submit_button" >Submit</button>
                    {!! Form::close() !!}

                </div>
            </div>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table table-striped">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>No KK</th>
                            <th>No BC</th>
                            <th>System log</th>
                        </tr>
                    </thead>
                    <tbody id="draw"></tbody>
                </table>
            </div>
        </div>
    </div>
    {!! Form::hidden('result', '[]', array('id' => 'result')) !!}
    {!! Form::hidden('page','import', array('id' => 'page')) !!}
@endsection

@section('page-modal')
    @include('report_outstanding_bc._import_confirmation_modal')
@endsection

@section('page-js')
    @include('report_outstanding_bc._import')
    <script src="{{ mix('js/report_outstanding_bc.js') }}"></script>
@endsection
