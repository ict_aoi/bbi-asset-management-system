<div id="confirmationImportModal" data-backdrop="static" data-keyboard="false" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Confirmation Modal</h5>
			</div>
				{!!
					Form::open([
						'role' 			=> 'form',
						'url' 			=> route('reportOutstandingBC.downloadFormImport'),
						'method' 		=> 'get',
						'class' 		=> 'form-horizontal',
					])
				!!}
				<div class="modal-body">
					@include('form.select', [
						'field' 		=> 'factory',
						'label' 		=> 'Factory',
						'default' 		=> $factory_id,
						'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
						'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
						'options' 		=> [
							'' 			=> '-- Select Factory --',
						]+$factories,
						'class' 		=> 'select-search',
						'attributes' 	=> [
							'id' 		=> 'select_factory'
						]
					])
				</div>
				{!! Form::hidden('current_time',null, array('id' => 'current_time')) !!}
				{!! Form::hidden('id',null, array('id' => '_id')) !!}
				
				<div class="modal-footer">
					<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Get form upload</button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
