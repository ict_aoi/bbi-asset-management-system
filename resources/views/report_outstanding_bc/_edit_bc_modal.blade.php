<div id="editBCModal" data-backdrop="static" data-keyboard="false" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Edit BC <span id="txt_bc_type"></span> For <span id="txt_no_kk"></span></h5>
			</div>
				{!!
					Form::open([
						'role' 			=> 'form',
						'url' 			=> route('reportOutstandingBC.update'),
						'method' 		=> 'post',
						'class' 		=> 'form-horizontal',
						'id' 			=> 'form',
					])
				!!}
				<div class="modal-body">
					@include('form.text', [
						'field'         => 'no_registration',
						'label'         => 'No. SJ',
						'mandatory'     => '*Required',
						'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
						'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
						'attributes'    => [
							'id'        => 'no_registration'
						]
					])

					@include('form.date', [
						'field'             => 'date_registration',
						'label'             => 'BC Registration',
						'mandatory'     	=> '*Required',
						'label_col'         => 'col-md-2 col-lg-2 col-sm-12',
						'form_col'          => 'col-md-10 col-lg-10 col-sm-12',
						'class'             => 'daterange-single',
						'mandatory'         => '*Required',
						'placeholder'       => 'dd/mm/yyyy',
						'attributes'        => [
							'id'            => 'date_registration',
							'readonly'      => 'readonly'
						]
					])

					@include('form.text', [
						'field'         => 'no_aju',
						'label'         => 'No. AJU',
						'mandatory'     => '*Required',
						'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
						'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
						'attributes'    => [
							'id'        => 'no_aju'
						]
					])

					@include('form.text', [
						'field'         => 'no_bc',
						'label'         => 'No. BC',
						'mandatory'     => '*Required',
						'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
						'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
						'attributes'    => [
							'id'        => 'no_bc'
						]
					])

					@include('form.select', [
						'field'         => 'bc_type',
						'label'         => 'BC Type',
						'mandatory'     => '*Required',
						'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
						'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
						'options'       => [
							''          => '-- Select BC Type --',
						]+$bc_types,
						'class'         => 'select-search',
						'attributes'    => [
							'id'        => 'select_bc_type'
						]
					])
				</div>
				{!! Form::hidden('status',null, array('id' => 'status')) !!}
				{!! Form::hidden('current_time',null, array('id' => 'current_time')) !!}
				{!! Form::hidden('id',null, array('id' => '_id')) !!}
				
				<div class="modal-footer">
					<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Save <i class="icon-floppy-disk position-right"></i></button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
