<ul class="icons-list">
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-menu9"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-left">
            @if (isset($detail))
                <li><a href="{{ $detail }}"><i class="icon-search4"></i> Detail</a></li>
            @endif
            @if (isset($edit))
                <li><a href="#" onclick="edit('{!! $edit !!}','{!! $status !!}')"><i class="icon-pencil6"></i> Edit BC</a></li>
            @endif

        </ul>
    </li>
</ul>
