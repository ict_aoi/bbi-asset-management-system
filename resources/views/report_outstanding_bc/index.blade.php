@extends('layouts.app', ['active' => 'report-outstanding-bc'])

@section('page-css')
    <style>
       .datepicker{z-index:9999 !important}
    </style>
@endsection

@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Report Outstanding BC</span></h4>
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
				<li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Dashboard</a></li>
				<li>Report</li>
				<li class="active">Outstanding BC</li>
			</ul>

            <ul class="breadcrumb-elements">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle legitRipple breadcrumb-dropdown" data-toggle="dropdown">
                        <i class="icon-three-bars position-left"></i>
                        Actions
                        <span class="caret"></span>
                    </a>
                    
                    <ul class="dropdown-menu dropdown-menu-right">
                        <!--li><a href="{{ route('reportOutstandingBC.import') }}"> Import <i class="icon-drawer-out pull-right"></i></a></li-->
                        <li><a href="{{ route('reportOutstandingBC.downloadReport') }}"> Download Report <i class="icon-file-excel pull-right"></i></a></li>
                    </ul>
                </li>
            </ul>
		</div>
	</div>
@endsection

@section('page-content')
<div class="panel panel-flat panel-collapsed">
        <div class="panel-heading">
            <h5 class="panel-title">Filters   <a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            <div class="col-lg-4 col-md-4 col-sm-12">
                @include('form.select', [
                    'field'         => 'factory',
                    'label'         => 'Factory',
                    'mandatory'     => '*Required',
                    'default'       => auth::user()->factory_id,
                    'label_col'     => 'col-md-12 col-lg-12 col-sm-12',
                    'div_class'     => (auth::user()->is_super_admin)? '':'hidden',
                    'form_col'      => 'col-md-12 col-lg-12 col-sm-12',
                    'options'       => [
                        ''          => '-- Select Factory --',
                    ]+$factories,
                    'class'         => 'select-search',
                    'attributes'    => [
                        'id'        => 'select_factory'
                    ]
                ])
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12">
                @include('form.select', [
                    'field'         => 'type',
                    'label'         => 'Type',
                    'default'       => '',
                    'label_col'     => 'col-md-12 col-lg-12 col-sm-12',
                    'form_col'      => 'col-md-12 col-lg-12 col-sm-12',
                    'options'       => [
                        ''          => '-- Select Type --',
                        '1'          => 'Rent from external',
                        '2'          => 'Rent from internal',
                        '3'          => 'Handover',
                        '4'          => 'Obsolete',
                        '5'          => 'Return',
                    ],
                    'class'         => 'select-search',
                    'attributes'    => [
                        'id'        => 'select_type'
                    ]
                ])
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12">
                @include('form.select', [
                    'field'         => 'status',
                    'label'         => 'Status',
                    'default'       => '1',
                    'label_col'     => 'col-md-12 col-lg-12 col-sm-12',
                    'form_col'      => 'col-md-12 col-lg-12 col-sm-12',
                    'options'       => [
                        '1'          => 'Oustanding BC In',
                        '2'          => 'Oustanding BC Out',
                        '3'          => 'Complete',
                    ],
                    'class'         => 'select-search',
                    'attributes'    => [
                        'id'        => 'select_status'
                    ]
                ])
            </div>
            
        </div>
    </div>

    <div class="panel panel-flat">
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-basic table-condensed" id="reportOutstandingBCTable">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Type</th>
                            <th>Origin Factory</th>
                            <th>Destination Factory</th>
                            <th>Department</th>
                            <th>No KK</th>
                            <th>No KK Parent</th>
                            <th>BC Type In</th>
                            <th>BC Type Out</th>
                            <th>Start Agreement</th>
                            <th>End Agreement</th>
                          
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

	{!! Form::hidden('page','index', array('id' => 'page')) !!}
@endsection

@section('page-modal')
    @include('report_outstanding_bc._edit_bc_modal')
@endsection

@section('page-js')
    <script src="{{ mix('js/datepicker.js') }}"></script>
	<script src="{{ mix('js/report_outstanding_bc.js') }}"></script>
@endsection



