@extends('layouts.app',['active' => 'rental_agreement'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Rental Agreement</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Dashboard</a></li>
            <li><a href="{{ route('reportOutstandingBC.index') }}">Rental Agreement</a></li>
            <li class="active">Detail</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Header   <a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-md-4 col-lg-4 col-sm-12">
                    <p>From Factory 		:  <b>{{$data->factory_name}}</b></p>
                    <p>Subdepartment       :  <b>{{$data->sub_department_name}}</b></p>
					<p>No SJ Out            :  <b>{{$data->no_registration_out}}</b></p>
					<p>BC Registration Out  :  <b>{{$data->date_registration_out}}</b></p>
					<p>No Aju Out			:  <b>{{$data->no_aju_out}}</b></p>
					<p>No BC Out            :  <b>{{$data->no_bc_out}}</b></p>
					<p>BC Type Out          :  <b>{{$data->bc_out_type}}</b></p>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-12">
                    <p>To Factory           :  <b>{{$data->destination_factory_name}}</b></p>
					<p>Subdepartment       :  <b>{{$data->sub_department_name}}</b></p>
					<p>No SJ In             :  <b>{{$data->no_registration_in}}</b></p>
					<p>BC Registration In   :  <b>{{$data->date_registration_in}}</b></p>
					<p>No Aju In			:  <b>{{$data->no_aju_in}}</b></p>
					<p>No BC In             :  <b>{{$data->no_bc_in}}</b></p>
					<p>BC Type In           :  <b>{{$data->bc_in_type}}</b></p>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-12">
                    <p>No KK Parent         :  <b>{{$data->parent_no_agreement}}</b></p>
                    <p>No KK 			    :  <b>{{$data->no_agreement}}</b></p>
                    @if($data->is_return_to_source)
                        <p>Type             :  <b></span><span class="label label-info">KK RETURN</span></b></p>
                    @else 
                        
                        @if($data->is_agreement_for_rental)
                            <p>Start Agreement  :  <b>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $data->start_date)->format('d/M/Y H:i:s') }}</b></p>
                            <p>End Agreement    :  <b>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $data->end_date)->format('d/M/Y H:i:s') }}</b></p>
                            <p>Type             :  <b></span><span class="label label-info">KK RENT</span></b></p>
                        @else
                            <p>Start Agreement  :  <b>-</b></p>
                            <p>End Agreement    :  <b>-</b></p>
                            <p>Type             :  <b></span><span class="label label-default">KK HANDOVER</span></b></p>
                        @endif
                    @endif
                    
                </div>
            </div>
                

        </div>
    </div>

    <div class="panel panel-flat">
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-basic table-condensed" id="detailRentalAgreementTable">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Asset Type</th>
                            <th>No Serial Number</th>
                            <th>Model</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    {!! Form::hidden('url', route('reportOutstandingBC.detailData',$data->id), array('id' => 'url_detail_data')) !!}
    {!! Form::hidden('page', 'detail', array('id' => 'page')) !!}
@endsection

@section('page-js')
    <script src="{{ mix('js/report_outstanding_bc.js') }}"></script>
@endsection
