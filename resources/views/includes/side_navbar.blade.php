<div class="sidebar sidebar-main sidebar-default">
    <div class="sidebar-content">

        <!-- User menu -->
        <div class="sidebar-user-material">
            <div class="category-content">
                <div class="sidebar-user-material-content">
                    @if(auth::user()->photo)
                        <img src="{{ route('accountSetting.showAvatar', auth::user()->photo) }}" class="img-circle" alt="profile_photo" id="avatar_image">
                    @else
                        @if(strtoupper(auth::user()->sex) == 'LAKI')
                            <a href="#"><img src="{{ asset('images/male_avatar.png') }}" class="img-circle" alt="avatar_male"></a>
                        @else
                            <a href="#"><img src="{{ asset('images/female_avatar.png') }}" class="img-circle" alt="avatar_female"></a>
                        @endif
                    @endif
                    <h6>{{ Auth::user()->name }}</h6>
                    <span class="text-size-small">{{ Auth::user()->email }}</span>
                </div>

                <div class="sidebar-user-material-menu">
                    <a href="#user-nav" data-toggle="collapse"><span>My Account</span> <i class="caret"></i></a>
                </div>
            </div>

            <div class="navigation-wrapper collapse" id="user-nav">
                <ul class="navigation">
                    <li class="divider"></li>
                    <li class="{{ $active == 'account_setting' ? 'active' : '' }}"><a href="{{ route('accountSetting') }}"><i class="icon-cog5"></i> <span>Account Setting</span></a></li>
                    <li><a href="{{ route('logout') }}"
							onclick="event.preventDefault();
									document.getElementById('logout-form').submit();">
							<i class="icon-switch2"></i> Logout</a>
							<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
								{{ csrf_field() }}
                            </form>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /user menu -->


        <!-- Main navigation -->
        <div class="sidebar-category sidebar-category-visible">
            <div class="category-content no-padding">
                <ul class="navigation navigation-main navigation-accordion">

                    <li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
                    <li class="{{ $active == 'dashboard' ? 'active' : '' }}"><a href="{{ route('home') }}"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
                    
                    <!-- START OF TRANSAKSI -->
                    @permission(['menu-check-in','menu-asset-opname','menu-check-out','menu-maintenance','menu-bapb'])
                    <li class="navigation-header"><span>Transaction</span> <i class="icon-menu" title="Forms"></i></li>
                    @endpermission
                    @permission(['menu-check-in'])
                    <li class="{{ $active == 'check_in' ? 'active' : '' }}"><a href="{{ route('checkin.index') }}"><i class="icon-database-insert"></i> <span>Check In</span></a></li>
                    @endpermission
                    @permission(['menu-check-out'])
                    <li class="{{ $active == 'check_out' ? 'active' : '' }}"><a href="{{ route('checkout.index') }}"><i class="icon-database-export"></i> <span>Check Out</span></a></li>
                    @endpermission
                    @permission(['menu-asset-opname'])
                    <li class="{{ $active == 'menu_asset_opname' ? 'active' : '' }}"><a href="{{ route('assetOpname.index') }}"><i class="icon-calculator3"></i> <span>Opname</span></a></li>
                    @endpermission
                    @permission(['menu-maintenance'])
                    <li class="{{ $active == 'maintenance' ? 'active' : '' }}"><a href="{{ route('maintenance.index') }}"><i class="icon-hammer-wrench"></i> <span>Maintenance</span></a></li>
                    @endpermission
                    @permission(['menu-bapb'])
                    <li class="{{ $active == 'bapb' ? 'active' : '' }}"><a href="{{ route('bapb.index') }}"><i class="icon-trash"></i> <span>BAPB</span></a></li>
                    @endpermission
                    <!-- END OF TRANSAKSI -->
                    
                     <!-- START OF REPORT -->
                     @permission('menu-report-asset-opname','menu-report-outstanding-bc')
                    <li class="navigation-header"><span>Report</span> <i class="icon-archive" title="Reports"></i></li>
                    <!-- END OF REPORT -->
                    @endpermission
                    @permission('menu-report-asset-opname')
                    <li class="{{ $active == 'report-asset-opname' ? 'active' : '' }}"><a href="{{ route('reportAssetOpname.index') }}" class="legitRipple"><i class="icon-archive"></i>  <span>Asset Opname</span></a></li>
                    @endpermission
                    @permission('menu-report-outstanding-bc')
                    <li class="{{ $active == 'report-outstanding-bc' ? 'active' : '' }}"><a href="{{ route('reportOutstandingBC.index') }}" class="legitRipple"><i class="icon-archive"></i>  <span>Outstanding BC</span></a></li>
                    @endpermission

                    <!-- START OF MASTER DATA -->
                    @permission(['menu-company','menu-calendar','menu-bc-type','menu-rental-agreement','menu-asset-rent','menu-budget','menu-area','menu-factory','menu-asset-type','menu-asset','menu-user','menu-role','menu-permission','menu-department','menu-consumable'])
                    <li class="navigation-header"><span>Master Data</span> <i class="icon-menu" title="Forms"></i></li>
                    @endpermission
                    @permission('menu-asset')
                    <li class="{{ $active == 'asset' ? 'active' : '' }}"><a href="{{ route('asset.index') }}" class="legitRipple"><i class="icon-cabinet"></i>  <span>Asset</span></a></li>
                    @endpermission
                    @permission('menu-asset-rent')
                    <li class="{{ $active == 'asset_rent' ? 'active' : '' }}"><a href="{{ route('assetRent.index') }}" class="legitRipple"><i class="icon-cabinet"></i>  <span>Asset Rent</span></a></li>
                    @endpermission
                    @permission('menu-consumable')
                    <li class="{{ $active == 'consumable' ? 'active' : '' }}"><a href="{{ route('consumable.index') }}"><i class="icon-pie-chart2"></i>  <span>Consumable</span></a></li>
                    @endpermission
                    @permission('menu-rental-agreement')
                    <li class="{{ $active == 'rental_agreement' ? 'active' : '' }}"><a href="{{ route('rentalAgreement.index') }}" class="legitRipple"><i class="icon-certificate"></i>  <span>Rental Agreement</span></a></li>
                    @endpermission
                    @permission('menu-budget')
                    <li class="{{ $active == 'budget' ? 'active' : '' }}"><a href="{{ route('budget.index') }}"><i class="icon-coins"></i>  <span>Budget</span></a></li>
                    @endpermission
                    @permission('menu-area')
                    <li class="{{ $active == 'area' ? 'active' : '' }}"><a href="{{ route('area.index') }}"><i class="icon-map"></i> <span>Area</span></a></li>
                    @endpermission
                    @permission('menu-calendar')
                    <li class="{{ $active == 'calendar' ? 'active' : '' }}"><a href="{{ route('calendar.index') }}"><i class=" icon-calendar3"></i> <span>Calendar</span></a></li>
                    @endpermission
                    @permission('menu-bc-type')
                    <li class="{{ $active == 'bc_type' ? 'active' : '' }}"><a href="{{ route('bcType.index') }}"><i class="icon-file-text"></i> <span>BC Type</span></a></li>
                    @endpermission
                    @permission(['menu-company','menu-factory','menu-department','menu-sub-department'])
                    <li class="{{ (in_array($active,['company','factory'])) ? 'active' : '' }}">
                        <a href="#" class="has-ul legitRipple"><i class="icon-lan2"></i> <span>Organization</span></a>
                        <ul>
                            @permission('menu-company')
                                <li class="{{ $active == 'company' ? 'active' : '' }}"><a href="{{ route('company.index') }}" class="legitRipple">Company</a></li>
                            @endpermission
                            @permission('menu-factory')
                                <li class="{{ $active == 'factory' ? 'active' : '' }}"><a href="{{ route('factory.index') }}" class="legitRipple">Factory</a></li>
                            @endpermission
                            @permission('menu-department')
                                <li class="{{ $active == 'department' ? 'active' : '' }}"><a href="{{ route('department.index') }}" class="legitRipple">Department</a></li>
                            @endpermission
                            @permission('menu-sub-department')
                                <li class="{{ $active == 'sub_department' ? 'active' : '' }}"><a href="{{ route('subDepartment.index') }}" class="legitRipple">Subdepartment</a></li>
                            @endpermission
                        </ul>
                    </li>
                    @endpermission
                    @permission(['menu-asset-type','menu-manufacture'])
                    <li class="{{ (in_array($active,['category','asset_type'])) ? 'active' : '' }}">
                        <a href="#" class="has-ul legitRipple"><i class="icon-gear"></i> <span>Setting</span></a>
                        <ul>
                            @permission(['menu-manufacture'])
                            <li class="{{ $active == 'manufacture' ? 'active' : '' }}"><a href="{{ route('manufacture.index') }}"><span>Manufacture</span></a></li>
                            @endpermission
                           
                            @permission('menu-asset-type')
                                <li class="{{ $active == 'asset_type' ? 'active' : '' }}"><a href="{{ route('assetType.index') }}" class="legitRipple">Asset Type</a></li>
                            @endpermission
                        </ul>
                    </li>
                    @endpermission
                    @permission(['menu-user','menu-role','menu-permission'])
                    <li class="{{ (in_array($active,['permission','role','user'])) ? 'active' : '' }}">
                        <a href="#" class="has-ul legitRipple"><i class="icon-users"></i> <span>Account Management</span></a>
                        <ul>
                            @permission('menu-permission')
                                <li class="{{ $active == 'permission' ? 'active' : '' }}"><a href="{{ route('permission.index') }}" class="legitRipple">Permission</a></li>
                            @endpermission
                            @permission('menu-role')
                                <li class="{{ $active == 'role' ? 'active' : '' }}"><a href="{{ route('role.index') }}" class="legitRipple">Role</a></li>
                            @endpermission
                            @permission('menu-user')
                                <li class="{{ $active == 'user' ? 'active' : '' }}"><a href="{{ route('user.index') }}" class="legitRipple">User</a></li>
                            @endpermission
                        </ul>
                    </li>
                    @endpermission
                    <!-- END OF MASTER DATA -->
                </ul>
            </div>
        </div>
        <!-- /main navigation -->

    </div>
</div>
