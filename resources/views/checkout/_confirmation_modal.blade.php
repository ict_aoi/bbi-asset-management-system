<div id="confirmationInsertModal" data-backdrop="static" data-keyboard="false" class="modal fade">
	<div class="modal-dialog modal-lg ui-front">
		<div class="modal-content">
				{!!
					Form::open([
						'role' 		=> 'form',
						'url' 		=> route('checkout.store'),
						'method' 	=> 'store',
						'class' 	=> 'form-horizontal',
						'id'		=>	'form'
					])
				!!}
				<div class="modal-header bg-indigo">
					<h5 class="modal-title"><p>Confirmation Checkout Asset</h5>
				</div>
				<div class="modal-body">
					<div class="form-group col-lg-12">
                        <label for="nik" class="text-semibold control-label col-md-3 col-lg-3 col-sm-12">
                            Check-Out To
                        </label>
                        <div class="control-input col-md-9 col-lg-9 col-sm-12">
                            <div class="btn-group" data-toggle="buttons">
                                <button type="button" class="btn bg-slate-700 btn-raised legitRipple" id="btn_user" data-popup="tooltip" title="user" data-placement="bottom" data-original-title="user"><i class=" icon-users position-left active"></i></button>
                                <button type="button" class="btn btn-danger btn-raised legitRipple" id="btn_location" data-popup="tooltip" title="location" data-placement="bottom" data-original-title="location"><i class=" icon-location4 position-right"></i></button>
                            </div>
                        </div>
                    </div>

                  	
					<div class="form-group col-lg-12" id="nik">
                        <label for="nik" class="text-semibold control-label col-md-3 col-lg-3 col-sm-12">
                            NIK
                        </label>
                        <div class="control-input col-md-9 col-lg-9 col-sm-12">
                            <div class="form-group has-feedback has-feedback-right">
                                <input type="text" class="form-control" id="txt_nik" name="nik">
                                <div class="form-control-feedback">
                                    <i class="icon-search4 text-size-base"></i>
                                </div>
                            </div>
                        </div>
                    </div>

					<div class="information hidden">
                        @include('form.text', [
                            'field'      => 'employee_name',
                            'label'      => 'Nama',
                            'label_col'  => 'col-md-3 col-lg-3 col-sm-12',
                            'form_col'   => 'col-md-9 col-lg-9 col-sm-12',
                            'attributes' => [
                                'id'       => 'txt_name',
                                'readonly' => 'readonly'
                            ]
                        ])

                        @include('form.text', [
                            'field'      => 'employee_department',
                            'label'      => 'Department',
                            'label_col'  => 'col-md-3 col-lg-3 col-sm-12',
                            'form_col'   => 'col-md-9 col-lg-9 col-sm-12',
                            'attributes' => [
                                'id'       => 'txt_department',
                                'readonly' => 'readonly'
                            ]
                        ]) 

                        @include('form.text', [
                            'field'      => 'employee_sub_department',
                            'label'      => 'Subdepartment',
                            'label_col'  => 'col-md-3 col-lg-3 col-sm-12',
                            'form_col'   => 'col-md-9 col-lg-9 col-sm-12',
                            'attributes' => [
                                'id'       => 'txt_sub_department',
                                'readonly' => 'readonly'
                            ]
                        ]) 
                    </div>
					
					<div class="row">
                        <div class="form-group col-lg-12 col-md-6 col-sm-6 hidden" id="location">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                @include('form.select', [
                                    'field'                 => 'status',
                                    'label'                 => 'Status',
                                    'mandatory'             => '*Required',
                                    'options'               => [
                                        ''                  => '-- Select Status --',
                                        'digunakan'         => 'Used',
                                        'obsolete'          => 'Obsolete/BAPB',
                                        'dipinjam'          => 'Rent',
                                        'dikembalikan'      => 'Return',
                                        'rusak'             => 'Maintenance',
                                        'dipindah tangan'   => 'Handover'
                                    ],
                                    'class'                 => 'select-search',
                                    'label_col'             => 'col-md-3 col-lg-3 col-sm-12',
                                    'form_col'              => 'col-md-9 col-lg-9 col-sm-12',
                                    'attributes'            => [
                                        'id'                => 'select_status',
                                    ]
                                ])
                               
                               @include('form.select', [
                                    'field'         => 'destination_factory',
                                    'label'         => 'Destination Factory',
                                    'mandatory'     => '*Required',
                                    'label_col'     => 'col-md-3 col-lg-3 col-sm-12',
                                    'form_col'      => 'col-md-9 col-lg-9 col-sm-12',
                                    'options'       => [
                                        ''          => '-- Select Destination Factory --',
                                    ],
                                    'class'         => 'select-search',
                                    'attributes'    => [
                                        'id'        => 'select_destination_factory'
                                    ]
                                ])

                                @include('form.text', [
                                    'field'         => 'new_oth_destination',
                                    'label'         => 'Other Destination',
                                    'mandatory'     => '*Required',
                                    'label_col'     => 'col-md-3 col-lg-3 col-sm-12',
                                    'form_col'      => 'col-md-9 col-lg-9 col-sm-12',
                                    'attributes'    => [
                                        'id'        => 'new_oth_destination',
                                    ]
                                ]) 


                                @include('form.select', [
                                    'field'     => 'destination_sub_department',
                                    'label'     => 'Destination Subdepartment',
                                    'mandatory' => '*Required',
                                    'label_col' => 'col-md-3 col-lg-3 col-sm-12',
                                    'form_col'  => 'col-md-9 col-lg-9 col-sm-12',
                                    'options'   => [
                                        '' => '-- Select Destination Subdepartment --',
                                    ],
                                    'class'      => 'select-search',
                                    'attributes' => [
                                        'id' => 'select_destination_subdepartment'
                                    ]
                                ])

                                @include('form.select', [
                                    'field'     => 'destination_area',
                                    'label'     => 'Destination Area',
                                    'label_col' => 'col-md-3 col-lg-3 col-sm-12',
                                    'form_col'  => 'col-md-9 col-lg-9 col-sm-12',
                                    'options'   => [
                                        '' => '-- Select Destination Area --',
                                    ],
                                    'class'      => 'select-search',
                                    'attributes' => [
                                        'id' => 'select_destination_area'
                                    ]
                                ])
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div id="kk_panel">
                                    @include('form.select', [
                                        'field'     => 'no_kk',
                                        'label'     => 'No KK',
                                        'mandatory' => '*Required',
                                        'label_col' => 'col-md-3 col-lg-3 col-sm-12',
                                        'form_col'  => 'col-md-9 col-lg-9 col-sm-12',
                                        'options'   => [
                                            ''          => '-- Select No KK --',
                                            '-1'        => '- Entry New -'
                                        ],
                                        'class'      => 'select-search',
                                        'attributes' => [
                                            'id' => 'select_no_kk'
                                        ]
                                    ])
    
                                    @include('form.text', [
                                        'field'      => 'new_no_kk',
                                        'label'      => 'Entry New No. KK',
                                        'mandatory' => '*Required when status is borrowed / handover',
                                        'label_col'  => 'col-md-3 col-lg-3 col-sm-12',
                                        'form_col'   => 'col-md-9 col-lg-9 col-sm-12',
                                        'attributes' => [
                                            'id'       => 'new_no_kk',
                                            'readonly' => 'readonly'
                                        ]
                                    ]) 
                                </div>
                               

                                <div id="panel_borrow">
                                    @include('form.date', [
                                        'field'             => 'start_rent_date',
                                        'label'             => 'Start Rent Date',
                                        'mandatory'         => '*Required',
                                        'label_col'         => 'col-md-3 col-lg-3 col-sm-12',
                                        'form_col'          => 'col-md-9 col-lg-9 col-sm-12',
                                        'class'             => 'daterange-single',
                                        'placeholder'       => 'dd/mm/yyyy',
                                        'attributes'        => [
                                            'id'            => 'start_rent_date',
                                            'readonly'      => 'readonly'
                                        ]
                                    ])

                                    @include('form.date', [
                                        'field'             => 'end_rent_date',
                                        'label'             => 'End Rent Date',
                                        'mandatory'         => '*Required',
                                        'label_col'         => 'col-md-3 col-lg-3 col-sm-12',
                                        'form_col'          => 'col-md-9 col-lg-9 col-sm-12',
                                        'class'             => 'daterange-single',
                                        'placeholder'       => 'dd/mm/yyyy',
                                        'attributes'        => [
                                            'id'            => 'end_rent_date',
                                            'readonly'      => 'readonly'
                                        ]
                                    ])
                                </div>
                            </div>
                            
                        </div>
                    </div>

				    {!! Form::hidden('selected_last_factory_id', '', array('id' => 'selected_last_factory_id')) !!}
                    {!! Form::hidden('selected_origin_subdepartment_id', '', array('id' => 'selected_origin_subdepartment_id')) !!}
                    {!! Form::hidden('checkout_to_status',null, array('id' => 'checkout_to_status')) !!}
					{!! Form::hidden('assets','[]' , array('id' => 'assets')) !!}
                    {!! Form::hidden('current_time',null, array('id' => 'current_time')) !!}
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-link" onClick="dismisModal()">Close</button>
					<button type="submit" class="btn btn-primary legitRipple">Save <i class="icon-floppy-disk position-right"></i></button>
				</div>
            {!! Form::close() !!}
            
            {!! Form::hidden('check_out_subdepartment_id', '', array('id' => 'check_out_subdepartment_id')) !!}
            {!! Form::hidden('area_factory_id', '', array('id' => 'area_factory_id')) !!}
            {!! Form::hidden('area_name', '', array('id' => 'area_name')) !!}
            {!! Form::hidden('auto_completes', '[]', array('id' => 'auto_completes')) !!}
            {!! Form::hidden('is_handover',null, array('id' => 'is_handover')) !!}
            {!! Form::hidden('factories_out', json_encode($factories_out), array('id' => 'factories_out')) !!}
		</div>
	</div>
</div>
