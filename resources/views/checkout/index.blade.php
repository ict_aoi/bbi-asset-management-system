@extends('layouts.app',['active' => 'check_out'])

@section('page-css')
<style>
    .ui-autocomplete { z-index:2147483647; }
    .ui-front {
        z-index: 9999;
    }
</style>
@endsection

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Check Out</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Dashboard</a></li>
            <li class="active">Check Out</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-default border-grey">
		<div class="panel-body">
			<div class="row">
				<div id="draw"></div>
			</div>
		</div>
	</div>

    <ul class="fab-menu fab-menu-fixed fab-menu-bottom-right" data-fab-toggle="click">
		<li>
			<a class="fab-menu-btn btn btn-success btn-float btn-rounded btn-icon">
				<i class="fab-icon-open icon-cog52""></i>
				<i class="fab-icon-close icon-cross2"></i>
			</a>

			<ul class="fab-menu-inner">
				<li>
					<div data-fab-label="Save">
                        <button type="btn" id="btn_confirmation" class="btn btn-default btn-rounded btn-icon btn-float" data-popup="tooltip" title="save" data-placement="bottom" data-original-title="save"><i class="icon-floppy-disk position-left"></i></button>
					</div>
				</li>
				<li>
					<div data-fab-label="List Asset">
						<button id="asset_button_lookup" class="btn btn-default btn-rounded btn-icon btn-float">
							<i class="glyphicon glyphicon-th-list"></i>
						</button>
					</div>
				</li>
                <li class ="{{ (auth::user()->is_super_admin ? '': 'hidden') }}">
					<div data-fab-label="Filter">
						<a data-toggle="modal" data-target="#filterModal"class="btn btn-default btn-rounded btn-icon btn-float">
							<i class="icon-filter3"></i>
						</a>
					</div>
				</li>
			</ul>
		</li>
	</ul>

	{!! Form::hidden('is_delete','', array('id' => 'is_delete')) !!}
	{!! Form::hidden('barcode_value','', array('id' => '_barcode_value')) !!}
    {!! Form::hidden('url_get_asset', route('checkout.getAsset'), ['id' => 'url_get_asset']) !!}  
@endsection

@section('page-modal')
    @include('form.modal_picklist', [
        'name'          => 'barcode',
        'title'         => 'List Asset',
        'placeholder'   => 'Search Based on No. Inventori /No. Inventori Manual/No Asset/ Serial Number',
    ])
    @include('checkout._detail_information_asset_modal')
    @include('checkout._confirmation_modal')
    @include('checkout._filter_modal')
@endsection

@section('page-js')
    @include('checkout._item')
    @include('checkout._item_information')
    <script src="{{ mix('js/datepicker.js') }}"></script>
    <script src="{{ mix('js/checkout.js') }}"></script>
@endsection