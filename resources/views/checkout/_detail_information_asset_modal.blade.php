<div id="detailInformationModal" data-backdrop="static" data-keyboard="false" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
				<div class="modal-header bg-indigo">
					<h5 class="modal-title"><p>Detail Informasi Asset</h5>
				</div>
				<div class="modal-body">
                    <div class="table-responsive">
                            <table class="table table table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Name</th>
                                        <th>Value</th>
                                    </tr>
                                </thead>
                                <tbody id="tbody_detail_asset_information"></tbody>
                            </table>
                        </div>
                        {!! Form::hidden('detail_information','[]' , array('id' => 'detail_information')) !!}
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
				</div>
		</div>
	</div>
</div>
