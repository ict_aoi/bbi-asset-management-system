@extends('layouts.app',['active' => 'calendar'])

@section('page-header')
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Calendar</span></h4>
            </div>
        </div>
        <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
            <ul class="breadcrumb">
                <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Dashboard</a></li>
                <li><a href="{{ route('calendar.index') }}">Calendar</a></li>
                <li class="active">Create</li>
            </ul>
        </div>
    </div>
@endsection

@section('page-content')
    <div class="panel panel-flat">
        <div class="panel-body">
            {!!
                Form::open([
                    'role'      => 'form',
                    'url'       => route('calendar.store'),
                    'method'    => 'post',
                    'class'     => 'form-horizontal',
                    'id'        => 'form'
                ])
            !!}
                @include('form.text', [
                    'field'         => 'name',
                    'label'         => 'Event Name',
                    'mandatory'     => '*Required',
                    'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                    'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                    'attributes'    => [
                        'id'        => 'name'
                    ]
                ])

                @include('form.date', [
                    'field'             => 'event_start',
                    'label'             => 'Event Start',
                    'label_col'         => 'col-md-2 col-lg-2 col-sm-12',
                    'form_col'          => 'col-md-10 col-lg-10 col-sm-12',
                    'class'             => 'daterange-single',
                    'mandatory'         => '*Required',
                    'placeholder'       => 'dd/mm/yyyy',
                    'attributes'        => [
                        'id'            => 'event_start',
                        'readonly'      => 'readonly'
                    ]
                ])

                @include('form.date', [
                    'field'             => 'event_end',
                    'label'             => 'Event End',
                    'label_col'         => 'col-md-2 col-lg-2 col-sm-12',
                    'form_col'          => 'col-md-10 col-lg-10 col-sm-12',
                    'class'             => 'daterange-single',
                    'placeholder'       => 'dd/mm/yyyy',
                    'mandatory'         => '*Required',
                    'attributes'        => [
                        'id'            => 'event_end',
                        'readonly'      => 'readonly'
                    ]
                ])

                <div class="text-right">
                    {!! Form::hidden('current_time', null, array('id' => 'current_time')) !!}
                    <button type="submit" class="btn btn-primary legitRipple col-xs-12">Save <i class="icon-floppy-disk position-right"></i></button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>

@endsection

@section('page-js')
    <script src="{{ mix('js/datepicker.js') }}"></script>                  
    <script src="{{ mix('js/calendar.js') }}"></script>
@endsection