<div id="confirmationModal" data-backdrop="static" data-keyboard="false" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
			</div>
				{!!
					Form::open([
						'role' 		=> 'form',
						'url' 		=> route('manufacture.exportFormImport'),
						'method' 	=> 'get',
						'class' 	=> 'form-horizontal',
					])
				!!}
				<div class="modal-body">
					@include('form.select', [
						'field' 		=> 'company',
						'label' 		=> 'Company',
						'default' 		=> $company_id,
						'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
						'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
						'div_class' 	=> (auth::user()->is_super_admin)? '':'hidden',
						'options' 		=> [
							'' 			=> '-- Select Company --',
						]+$companies,
						'class' 		=> 'select-search',
						'attributes' 	=> [
							'id' 		=> 'select_company'
						]
					])

					@include('form.select', [
						'field' 		=> 'sub_department',
						'label' 		=> 'Sub Departement',
						'default' 		=> $sub_department_id,
						'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
						'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
						'div_class' 	=> (auth::user()->is_super_admin)? '':'hidden',
						'options' 		=> [
							'' 			=> '-- Select Sub Departement --',
						]+$sub_departments,
						'class' 		=> 'select-search',
						'attributes' 	=> [
							'id' 		=> 'select_sub_department'
						]
					])

					
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Get form upload</button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
