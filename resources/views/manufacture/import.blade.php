@extends('layouts.app',['active' => 'manufacture'])

@section('page-css')

@endsection

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Manufacture</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Dashboard</a></li>
            <li >Setting</li>
            <li ><a href="{{ route('manufacture.index') }}">Manufacture</a></li>
            <li class="active">Import</li>
        </ul>
    </div>
</div>
@endsection
@section('page-content')
<div class="panel panel-flat">
    <div class="panel-heading">
			<h6 class="panel-title">&nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			<div class="heading-elements">
                @if(auth::user()->is_super_admin)
                    <a href="#" class="btn btn-primary btn-icon"  data-toggle="modal" data-target="#confirmationModal" data-popup="tooltip" title="download form import" data-placement="bottom" data-original-title="download form import"><i class="icon-download"></i></a>
                @else
                    <button type="button" class="btn btn-primary btn-icon" id="import_button" data-popup="tooltip" title="download form import" data-placement="bottom" data-original-title="download form import"><i class="icon-download"></i></button>
                
                    {!!
                        Form::open([
                            'url' 		=> route('manufacture.exportFormImport'),
                            'method' 	=> 'get',
                            'class'     => 'hidden',
                            'id'        => 'download_file_allocation',
                        ])
                    !!}
                        {!! Form::hidden('company', $company_id, array('id' => 'company')) !!}
                        {!! Form::hidden('sub_department', $sub_department_id, array('id' => 'sub_department_id')) !!}
                    {!! Form::close() !!}
                @endif
                <button type="button" class="btn btn-success btn-icon" id="upload_button" data-popup="tooltip" title="upload form import" data-placement="bottom" data-original-title="upload form import"><i class="icon-upload"></i></button>

				{!!
					Form::open([
						'role'      => 'form',
						'url'       => route('manufacture.uploadFormImport'),
						'method'    => 'POST',
						'id'        => 'upload_file_allocation',
						'enctype'   => 'multipart/form-data'
					])
				!!}
                    {!! Form::hidden('current_time','', array('id' => 'current_time')) !!}
					<input type="file" class="hidden" id="upload_file" name="upload_file" accept=".csv, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
					<button type="submit" class="btn btn-success hidden btn-lg" id="submit_button" >Submit</button>
				{!! Form::close() !!}

			</div>
		</div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table table-striped">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Company</th>
                        <th>Sub Departement</th>
                        <th>Manufacture</th>
                        <th>Description</th>
                        <th>System Log</th>
                    </tr>
                </thead>
                <tbody id="draw"></tbody>
            </table>
        </div>
    </div>
</div>
{!! Form::hidden('result', '[]', array('id' => 'result')) !!}
@endsection

@section('page-modal')
    @include('manufacture._confirmation_modal')
    @include('manufacture._import')
@endsection

@section('page-js')
    <script src="{{ mix('js/import_manufacture.js') }}"></script>
@endsection
