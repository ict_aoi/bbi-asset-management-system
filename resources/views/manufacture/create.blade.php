@extends('layouts.app',['active' => 'manufacture'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Manufacture</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Dashboard</a></li>
            <li>Setting</li>
            <li><a href="{{ route('manufacture.index') }}">Manufacture</a></li>
            <li class="active">Create</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat">
    {!! 
        Form::open([
            'role'      => 'form',
            'url'       => route('manufacture.store'),
            'method'    => 'post',
            'class'     => 'form-horizontal',
            'id'        => 'form'
        ])
    !!}
    <div class="panel-body">
        @include('form.select', [
            'field'         => 'sub_department',
            'label'         => 'Sub Departement',
            'default'       => auth::user()->sub_department_id,
            'mandatory'     => '*Required',
            'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
            'div_class'     => (auth::user()->is_super_admin)? '':'hidden',
            'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
            'options'       => [
                ''          => '-- Select Sub Departement --',
            ]+$sub_departments,
            'class'         => 'select-search',
            'attributes'    => [
                'id'        => 'select_sub_department'
            ]
        ])

        @include('form.text', [
            'field'         => 'name',
            'label'         => 'Name',
            'mandatory'     => '*Required',
            'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
            'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
            'attributes'        => [
                'id'        => 'name'
            ]
        ])

        @include('form.textarea', [
            'field'         => 'description',
            'label'         => 'Description',
            'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
            'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
            'attributes'    => [
                'id'        => 'description',
                'rows'      => 5,
                'style'     => 'resize: none;'
            ]
        ])

        {!! Form::hidden('current_time','', array('id' => 'current_time')) !!}
        <div class="text-right">
            <button type="submit" class="btn btn-primary legitRipple col-xs-12">Save <i class="icon-floppy-disk position-right"></i></button>
        </div>
        
    </div>
    {!! Form::close() !!}
</div>
@endsection

@section('page-js')
    <script src="{{ mix('js/manufacture.js') }}"></script>
@endsection
