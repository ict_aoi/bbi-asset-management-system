@extends('layouts.app',['active' => 'manufacture'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Manufacture</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Dashboard</a></li>
            <li>Setting</li>
            <li class="active">Manufacture</li>
        </ul>

        <ul class="breadcrumb-elements">
			<li class="dropdown">
				<a href="#" class="dropdown-toggle legitRipple breadcrumb-dropdown" data-toggle="dropdown">
					<i class="icon-three-bars position-left"></i>
					Actions
					<span class="caret"></span>
				</a>
				
				<ul class="dropdown-menu dropdown-menu-right">
					<li><a href="{{ route('manufacture.import') }}"><i class="icon-drawer-out pull-right"></i> Import</a></li>
					<li><a href="{{ route('manufacture.create') }}"><i class="icon-plus2 pull-right"></i> Create</a></li>
				</ul>
			</li>
		</ul>
    </div>
</div>
@endsection

@section('page-content')
    <div class="panel panel-flat panel-collapsed {{ (auth::user()->is_super_admin ? '': 'hidden') }}">
        <div class="panel-heading">
            <h5 class="panel-title">Filters   <a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
                @include('form.select', [
                    'field'         => 'sub_departments',
                    'label'         => 'Sub Departement',
                    'default'       => auth::user()->sub_department_id,
                    'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                    'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                    #'div_class'     => (auth::user()->is_super_admin)? '':'hidden',
                    'options'       => [
                        ''          => '-- Select Sub Departement --',
                    ]+$sub_departments,
                    'class'         => 'select-search',
                    'attributes'    => [
                        'id'        => 'select_sub_department'
                    ]
                ])
        </div>
    </div>

    <div class="panel panel-flat">
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-basic table-condensed" id="manufactureTable">
                    <thead>
                        <tr>
                            <th>No</th>
                            @if(auth::user()->is_super_admin)
                                <th>Subdepartement</th>
                            @endif
                            <th>Manufacture</th>
                            <th>Description</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

{!! Form::hidden('msg', $msg, array('id' => 'msg')) !!}
{!! Form::hidden('is_super_admin', (auth::user()->is_super_admin)? '1':'0', array('id' => 'is_super_admin')) !!}
{!! Form::hidden('form_status', 'index', array('id' => 'form_status')) !!}

@endsection


@section('page-js')
<script src="{{ mix('js/manufacture.js') }}"></script>
@endsection
