<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1,viewport-fit=cover">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="description" content="BBI Digital Asset System App">
        <meta name="author" content="ICT">
        <title>DIAS | Digital Assets </title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <!-- Global stylesheets -->
        <link rel="shortcut icon" href="{{ asset('images/favicon.png') }}">

	    <link href="{{ asset('css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
	    <link href="{{ mix('css/backend.css') }}" rel="stylesheet" type="text/css">
       
    </head>
    <body class="login-container login-cover">
        <div class="page-container">
            <div class="page-content">
                <div class="content-wrapper">
                    <div class="content">
                        <form  method="POST" action="{{ route('login') }}">
                            {{ csrf_field() }}
                            <div class="panel panel-body login-form">
                                <div class="text-center">
                                    <img src="{{ asset('images/logo_transparent.png') }}" alt="dias_logo" style="width:50%">
                                    <h5 class="content-group-lg">Login to your account <small class="display-block">Enter your credentials</small></h5>
                                </div>

                                <div class="form-group has-feedback has-feedback-left">
                                    <input type="text" class="form-control {{ $errors->has('nik') || $errors->has('email') ? 'has-error' : '' }}" value="{{ old('nik') ?: old('email') }}" placeholder="NIK / E-mail" name="login" required>
                                    <div class="form-control-feedback">
                                        <i class="icon-user text-muted"></i>
                                    </div>
                                </div>

                                <div class="form-group has-feedback has-feedback-left">
                                    <div class="input-group">
                                       <input type="password" class="form-control" placeholder="Password" name="password" id="password" required>
                                       <span class="input-group-btn">
                                           <button class="btn btn-default legitRipple" type="button" onClick="showPassword()"><i class="icon-eye-blocked" id="iconEye"></i></button>
                                       </span>
                                       <div class="form-control-feedback">
                                           <a><i class="icon-lock2 text-muted"></i></a>
                                       </div>
                                   </div>
                               </div>

                                @if ($errors->has('employee_code'))
                                    <span class="help-block text-danger">
                                        <strong>{{ $errors->first('employee_code') }}</strong>
                                    </span>
                                @elseif ($errors->has('email'))
                                    <span class="help-block text-danger">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @elseif (session()->has('flash_notification.message'))
                                    <span class="help-block text-danger">
                                        <strong>{!! session()->get('flash_notification.message') !!}</strong>
                                    </span>   
                                @endif
                                <div class="form-group">
                                    <button type="submit" class="btn bg-pink-400 btn-block">Login <i class="icon-circle-right2 position-right"></i></button>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <script>
            function showPassword()
            {
                console.log('asd');
                var text_field  = document.getElementById("password");
                var icon        = document.getElementById("iconEye");
                
                if (text_field.type === "password") text_field.type = "text";
                else text_field.type = "password";
                
                if(icon.className=="icon-eye-blocked") icon.className = "icon-eye";
                else icon.className = "icon-eye-blocked";
            }
        </script>
	</body>
</html>
