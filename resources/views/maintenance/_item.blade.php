<script type="x-tmpl-mustache" id="asset_maintenance_table">
	{% #item %}	
		<tr id="panel_{% _id %}" {% #is_error %} style="background-color:#fab1b1" {% /is_error %}>
			<td>{% no %}</td>
			<td>{% barcode %}</td>
			<td>{% no_inventory %}</td>
			<td>{% erp_no_asset %}</td>
			<td>{% asset_type %}</td>
			<td>{% serial_number %}</td>
			<td>{% model %}</td>
			<td>
				<button type="button" id="show_detail_{% _id %}" data-id="{% _id %}" class="btn btn-default btn-detail-item"><i class="fa icon-search4"></i></button>
				<button type="button" id="delete_{% _id %}" data-id="{% _id %}" class="btn btn-default btn-delete-item"><i class="fa icon-trash"></i></button>
			</td>
		</tr>
	{%/item%}
	<tr>
		<td colspan="9">
			<div class="input-group col-xs-12">
				<input type="text" id="barcode_value" name="barcode_value" class="form-control input-new barcode_value" placeholder="Please scan your barcode here" autocomplete="off"></input>
				<span class="input-group-btn">
					<button class="btn btn-default legitRipple" type="button" id="asset_button_lookup">
						<span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
					</button>
				</span>
			</div>
		</td>
	</tr>
</script>