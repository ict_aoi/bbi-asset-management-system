
<ul class="icons-list">
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-menu9"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-left">
            @if (isset($editStatus))
                <li><a onclick="editStatus('{{ $editStatus }}')"><i class="icon-pencil6"></i> Edit Status</a></li>
            @endif
        </ul>
    </li>
</ul>
