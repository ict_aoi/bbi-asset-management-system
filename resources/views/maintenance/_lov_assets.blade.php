<table class="table">
	<thead>
	  <tr>
		<th>Tipe Asset</th>
		<th>Model</th>
		<th>No. Inventory</th>
		<th>No. Inventory Manual</th>
		<th>ERP No. Asset</th>
		<th>Serial Number</th>
		<th>Status</th>
		<th>Action</th>
	  </tr>
	</thead>

	<tbody>
		@foreach ($lists as $key => $list)
			<tr>
				<td>
					{{ strtoupper($list->assetType->name) }}
				</td>
				<td>
					{{ strtoupper($list->model) }}
				</td>
				<td>
					{{ strtoupper($list->no_inventory) }}
				</td>
				<td>
					{{ strtoupper($list->no_inventory_manual) }}
				</td>
				<td>
					{{ strtoupper($list->erp_no_asset) }}
				</td>
				<td>
					{{ strtoupper($list->serial_number) }}
				</td>
				<td>
					@if($list->status == 'siap digunakan')
						{!! '<span class="label label-info">'.ucwords($list->status).'</span>' !!}
					@else
						{!! '<span class="label label-danger">'.ucwords($list->status).'</span>' !!}
					@endif
				</td>
				<td>
					<button data-dismiss="modal" class="btn btn-info btn-xs btn-choose"
						type="button" data-id="{{ $list->barcode }}">Select
					</button>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>

{!! $lists->appends(Request::except('page'))->render() !!}
