@extends('layouts.app',['active' => 'maintenance'])

@section('page-css')
    <style>
        .ui-autocomplete { z-index:2147483647; }
        .ui-front {
            z-index: 9999;
        }
    </style>
@endsection

@section('page-header')
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Maintenance</span></h4>
            </div>
        </div>
        <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
            <ul class="breadcrumb">
                <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Dashboard</a></li>
                <li class="active">Maintenance</li>
            </ul>
        </div>
    </div>
@endsection

@section('page-content')

    <div class="panel panel-flat panel-collapsed {{ (auth::user()->is_super_admin ? '': 'hidden') }}">
        <div class="panel-heading">
            <h5 class="panel-title">Filters   <a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">

            <div class="col-lg-4 col-md-4 col-sm-12">
                @include('form.select', [
                    'field'         => 'factory',
                    'label'         => 'Factory',
                    'mandatory'     => '*Required',
                    'default'       => $factory_id,
                    'label_col'     => 'col-md-12 col-lg-12 col-sm-12',
                    'div_class'     => (auth::user()->is_super_admin)? '':'hidden',
                    'form_col'      => 'col-md-12 col-lg-12 col-sm-12',
                    'options'       => [
                        ''          => '-- Select Factory --',
                    ]+$factories,
                    'class'         => 'select-search',
                    'attributes'    => [
                        'id'        => 'select_factory'
                    ]
                ])
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12">
                @include('form.select', [
                    'field'         => 'sub_department',
                    'label'         => 'Subdepartment',
                    'mandatory'     => '*Required',
                    'default'       => $sub_department_id,
                    'label_col'     => 'col-md-12 col-lg-12 col-sm-12',
                    'form_col'      => 'col-md-12 col-lg-12 col-sm-12',
                    'div_class'     => (auth::user()->is_super_admin)? '':'hidden',
                    'options'       => [
                        '' => '-- Select Subdepartment --',
                    ]+$sub_departments,
                    'class'         => 'select-search',
                    'attributes'    => [
                        'id'        => 'select_sub_department'
                    ]
                ])
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12">
                @include('form.select', [
                    'field'         => 'asset_type',
                    'label'         => 'Asset Type',
                    'mandatory'     => '*Required',
                    'label_col'     => 'col-md-12 col-lg-12 col-sm-12',
                    'form_col'      => 'col-md-12 col-lg-12 col-sm-12',
                    'options'       => [
                        ''          => '-- Select Asset Type --',
                    ],
                    'class' => 'select-search',
                    'attributes' => [
                        'id' => 'select_asset_type'
                    ]
                ])
            </div>
                
        </div>
    </div>

    <div class="panel panel-default border-grey">
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-basic table-condensed" id="maintenanceTable">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Id</th>
                            <th>last_company_location_id</th>
                            <th>last_factory_location_id</th>
                            <th>last_area_location</th>
                            <th>custom_field_1</th>
                            <th>custom_field_2</th>
                            <th>custom_field_3</th>
                            <th>custom_field_4</th>
                            <th>custom_field_5</th>
                            <th>custom_field_6</th>
                            <th>custom_field_7</th>
                            <th>custom_field_8</th>
                            <th>custom_field_9</th>
                            <th>custom_field_10</th>
                            <th>custom_field_11</th>
                            <th>custom_field_12</th>
                            <th>custom_field_13</th>
                            <th>custom_field_14</th>
                            <th>custom_field_15</th>
                            <th>custom_field_16</th>
                            <th>custom_field_17</th>
                            <th>custom_field_18</th>
                            <th>custom_field_19</th>
                            <th>custom_field_20</th>
                            <th>last_used_by</th>
                            <th>last_department_location_id</th>
                            @if(auth::user()->is_super_admin)
                                <th>Asset Factory</th>
                                <th>Asset Subdepartemen</th>
                            @endif
                            <th>No Inventori</th>
                            <th>No Inventori Manual</th>
                            <th>ERP No Asset (Accounting)</th>
                            <th>Asset Type</th>
                            <th>Model</th>
                            <th>Serial Number</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    {!! Form::hidden('is_super_admin', (auth::user()->is_super_admin)? '1':'0', array('id' => 'is_super_admin')) !!}

@endsection

@section('page-modal')
    @include('form.modal_picklist', [
        'name'          => 'barcode',
        'title'         => 'List Asset',
        'placeholder'   => 'Search based on No. Inventori / Nomor Serial',
    ])
    @include('maintenance._detail_information_asset_modal')
    @include('maintenance._confirmation_modal')
@endsection

@section('page-js')
    @include('maintenance._item')
    @include('maintenance._item_information')
    <script src="{{ mix('js/datepicker.js') }}"></script>
    <script src="{{ mix('js/maintenance.js') }}"></script>
@endsection