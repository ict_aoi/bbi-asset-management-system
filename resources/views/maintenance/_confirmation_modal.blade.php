<div id="confirmationInsertModal" data-backdrop="static" data-keyboard="false" class="modal fade">
	<div class="modal-dialog modal-lg ui-front">
		<div class="modal-content">
				{!!
					Form::open([
						'role' 		=> 'form',
						'url' 		=> route('maintenance.store'),
						'method' 	=> 'store',
						'class' 	=> 'form-horizontal',
						'id'		=>	'form'
					])
				!!}
				<div class="modal-header bg-indigo">
					<h5 class="modal-title"><p>Confirmation Maintenance Asset</h5>
				</div>
				<div class="modal-body">
                    @include('form.select', [
                        'field'     => 'status',
                        'label'     => 'Status',
                        'mandatory' => '*Required',
                        'default'   => 'diperbaiki',
                        'options'   => [
                            'diperbaiki'    => 'Repaired',
                        ],
                        'class'      => 'select-search',
                        'label_col'  => 'col-md-3 col-lg-3 col-sm-12',
                        'form_col'   => 'col-md-9 col-lg-9 col-sm-12',
                        'attributes' => [
                            'id'       => 'select_status',
                        ]
                        
                    ])
                    @include('form.text', [
                        'field'     => 'teknisi',
                        'label'     => 'Technician Name',
                        'mandatory' => '*Required when status is repaired',
                        'label_col' => 'col-md-3 col-lg-3 col-sm-12',
                        'form_col'  => 'col-md-9 col-lg-9 col-sm-12',
                        'attributes' => [
                            'id'       => 'teknisi',
                        ]
                    ])

                    @include('form.textarea', [
                        'field'         => 'root_cause',
                        'label'         => 'Root Cause',
                        'label_col'     => 'col-md-3 col-lg-3 col-sm-12',
                        'form_col'      => 'col-md-9 col-lg-9 col-sm-12',
                        'mandatory'     => '*Required when status is repaired',
                        'attributes'    => [
                            'id'        => 'root_cause',
                            'rows'      => 5,
                            'style'     => 'resize: none;'
                        ]
                    ])

					{!! Form::hidden('asset_id', '', array('id' => 'asset_id')) !!}
					{!! Form::hidden('checkout_to_status', '', array('id' => 'checkout_to_status')) !!}
					{!! Form::hidden('factory_id', '', array('id' => 'factory_id')) !!}
					{!! Form::hidden('sub_department_id', '', array('id' => 'sub_department_id')) !!}
					{!! Form::hidden('assets','[]' , array('id' => 'assets')) !!}
                    {!! Form::hidden('current_time',null, array('id' => 'current_time')) !!}
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary legitRipple">Save <i class="icon-floppy-disk position-right"></i></button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
