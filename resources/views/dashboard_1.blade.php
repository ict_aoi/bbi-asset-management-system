@extends(($dashboard_eis ?  'layouts.dashboard' : 'layouts.app'),['active' => 'dashboard'])


@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Dashboard</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ ($dashboard_eis ? route('dashboardEis') : route('home') ) }}">Dashboard</a></li>
            <li class="active">Detail {{ $detail }}</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
    <div class="row">
        <div id ="draw_summary"></div>
    </div>
    </br/>
    <div class="row">
        <div class="col-col-6 col-md-6 col-sm-12">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title">Total Mechanic {{ $detail }}</h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a href="{{ ($dashboard_eis ? route('dashboardEis',['detail' => $detail,'sub_department' => 'mechanic'])  : route('home',['detail' => $detail,'sub_department' => 'mechanic']) )}}"><i class="icon-search4"></i></a></li>
                        </ul>
                    </div>
                </div>
                

                <div class="panel-body">
                    <div class="row">
                        <div class="chart-container">
                            <div class="chart has-fixed-height" id="connect_pie_mechanic"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-col-6 col-md-6 col-sm-12">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title">Total Electric {{ $detail }}</h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a href="{{ ($dashboard_eis ? route('dashboardEis',['detail' => $detail,'sub_department' => 'electric'])  : route('home',['detail' => $detail,'sub_department' => 'electric']) )}}"><i class="icon-search4"></i></a></li>
                        </ul>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="row">
                        <div class="chart-container">
                            <div class="chart has-fixed-height" id="connect_pie_electric"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-col-6 col-md-6 col-sm-12">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title">Total GA {{ $detail }}</h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a href="{{ ($dashboard_eis ? route('dashboardEis',['detail' => $detail,'sub_department' => 'ga'])  : route('home',['detail' => $detail,'sub_department' => 'ga']) )}}"><i class="icon-search4"></i></a></li>
                        </ul>
                    </div>
                </div>
                

                <div class="panel-body">
                    <div class="row">
                        <div class="chart-container">
                            <div class="chart has-fixed-height" id="connect_pie_ga"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-col-6 col-md-6 col-sm-12">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title">Total ICT {{ $detail }}</h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a href="{{ ($dashboard_eis ? route('dashboardEis',['detail' => $detail,'sub_department' => 'ict'])  : route('home',['detail' => $detail,'sub_department' => 'ict']) )}}"><i class="icon-search4"></i></a></li>
                        </ul>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="row">
                        <div class="chart-container">
                            <div class="chart has-fixed-height" id="connect_pie_ict"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
   
    {!! Form::hidden('page', 'detail_1', ['id' => 'page']) !!}
    {!! Form::hidden('factory', $detail, ['id' => 'factory']) !!}
    {!! Form::hidden('summary_data', '[]', ['id' => 'summary_data']) !!}
    {!! Form::hidden('url_data_summary', route('home.dashboardDataSummary',['detail' => $detail]), ['id' => 'url_data_summary']) !!}
    {!! Form::hidden('url_data_pie_department', route('home.dashboardDataPieDepartmentSummary'), ['id' => 'url_data_pie_department']) !!}
   
    {!! Form::hidden('asset_status_legend_mechanical', '[]', ['id' => 'asset_status_legend_mechanical']) !!}
    {!! Form::hidden('total_asset_mechanical','[]', ['id' => 'total_asset_mechanical']) !!}
    {!! Form::hidden('asset_status_legend_electric', '[]', ['id' => 'asset_status_legend_electric']) !!}
    {!! Form::hidden('total_asset_electric','[]', ['id' => 'total_asset_electric']) !!}
    {!! Form::hidden('asset_status_legend_ga', '[]', ['id' => 'asset_status_legend_ga']) !!}
    {!! Form::hidden('total_asset_ga','[]', ['id' => 'total_asset_ga']) !!}
    {!! Form::hidden('asset_status_legend_ict', '[]', ['id' => 'asset_status_legend_ict']) !!}
    {!! Form::hidden('total_asset_ict','[]', ['id' => 'total_asset_ict']) !!}
@endsection


@section('page-js')
    @include('_summary_dashboard')
    <script src="{{ mix('js/echarts.js') }}"></script>
    <script src="{{ mix('js/dashboard.js') }}"></script>
@endsection