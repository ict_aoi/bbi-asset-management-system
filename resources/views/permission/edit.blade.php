@extends('layouts.app',['active' => 'permission'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Permission</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Dashboard</a></li>
            <li>Account Management</li>
            <li><a href="{{ route('permission.index') }}">Permission</a></li>
            <li class="active">Edit</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat">
    <div class="panel-body">
    {!!
            Form::open([
                'role'      => 'form',
                'url'       => route('permission.update',$permission->id),
                'method'    => 'post',
                'class'     => 'form-horizontal',
                'id'        => 'form'
            ])
        !!}
            @include('form.text', [
                'field'         => 'name',
                'default'       => $permission->name,
                'label'         => 'Name',
                'mandatory'     => '*Required',
                'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                'attributes'    => [
                    'id'        => 'name',
                    'readonly'  => 'readonly'
                ]
            ])

            @include('form.textarea', [
                'field'         => 'description',
                'label'         => 'Description',
                'default'       => $permission->description,
                'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                'attributes'    => [
                    'id'        => 'description',
                    'rows'      => 5,
                    'style'     => 'resize: none;'
                ]
            ])

            <div class="text-right">
                {!! Form::hidden('current_time', null, array('id' => 'current_time')) !!}
                <button type="submit" class="btn btn-primary legitRipple col-xs-12">Save <i class="icon-floppy-disk position-right"></i></button>
            </div>
        {!! Form::close() !!}
    </div>
</div>

@endsection

@section('page-js')
    <script src="{{ mix('js/permission.js') }}"></script>
@endsection