@extends('layouts.app',['active' => 'rental_agreement'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Rental Agreement</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Dashboard</a></li>
            <li><a href="{{ route('rentalAgreement.index') }}">Rental Agreement</a></li>
            <li class="active">Edit</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
    <div class="panel panel-flat">
        {!! 
            Form::open([
                'role'      => 'form',
                'url'       => route('rentalAgreement.update',$rental_agreement->id),
                'method'    => 'post',
                'class'     => 'form-horizontal',
                'id'        => 'form'
            ])
        !!}
        <div class="panel-body">
            @include('form.select', [
                'field'         => 'factory',
                'label'         => 'Factory',
                'default' 		=> $rental_agreement->factory_id,
                'mandatory'     => '*Required',
                'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                'div_class'     => (auth::user()->is_super_admin)? '':'hidden',
                'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                'options'       => [
                    ''          => '-- Select Factory --',
                ]+$factories,
                'class'         => 'select-search',
                'attributes'    => [
                    'id'        => 'select_factory',
                    'disabled'  => 'disabled'
                ]
            ])

            @include('form.select', [
                'field'         => 'sub_department',
                'label'         => 'Subdepartment',
                'mandatory'     => '*Required',
                'default' 		=> $rental_agreement->sub_department_id,
                'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                'div_class'     => (auth::user()->is_super_admin)? '':'hidden',
                'options'       => [
                    ''          => '-- Select Subdepartment --',
                ]+$sub_departments,
                'class'         => 'select-search',
                'attributes'    => [
                    'id'        => 'select_sub_department',
                    'disabled'  => 'disabled'
                ]
            ])
            @include('form.select', [
                'field'         => 'budget',
                'label'         => 'Budget',
                'mandatory'     => '*Required',
                'default' 		=> $rental_agreement->budget_id,
                'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                'options'       => [
                    ''          => '-- Select Budget --',
                ]+$budgets,
                'class'         => 'select-search',
                'attributes'    => [
                    'id'        => 'select_budget'
                ]
            ])

            @include('form.text', [
                'field'         => 'no_agreement',
                'label'         => 'No. KK',
                'default' 		=> $rental_agreement->no_agreement,
                'mandatory'     => '*Required',
                'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                'attributes'    => [
                    'id'        => 'no_agreement'
                ]
            ])

            @include('form.text', [
                'field'         => 'price_rent',
                'default' 		=> $rental_agreement->price,
                'label'         => 'Price Rent',
                'mandatory'     => '*Required',
                'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                'attributes'    => [
                    'id'        => 'price_rent'
                ]
            ])

            @include('form.date', [
                'field'             => 'start_rent_date',
                'label'             => 'Start Rent Date',
                'default' 		    => $rental_agreement->start_date->format('d/m/Y'),
                'mandatory'         => '*Required',
                'label_col'         => 'col-md-2 col-lg-2 col-sm-12',
                'form_col'          => 'col-md-10 col-lg-10 col-sm-12',
                'class'             => 'daterange-single',
                'placeholder'       => 'dd/mm/yyyy',
                'attributes'        => [
                    'id'            => 'start_rent_date',
                    'readonly'      => 'readonly'
                ]
            ])

            @include('form.date', [
                'field'             => 'end_rent_date',
                'label'             => 'End Rent Date',
                'default' 		    => $rental_agreement->end_date->format('d/m/Y'),
                'mandatory'         => '*Required',
                'label_col'         => 'col-md-2 col-lg-2 col-sm-12',
                'form_col'          => 'col-md-10 col-lg-10 col-sm-12',
                'class'             => 'daterange-single',
                'placeholder'       => 'dd/mm/yyyy',
                'attributes'        => [
                    'id'            => 'end_rent_date',
                    'readonly'      => 'readonly'
                ]
            ])

            @include('form.textarea', [
                'field'         => 'description',
                'default' 		=> $rental_agreement->description,
                'label'         => 'Description',
                'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                'attributes'    => [
                    'id'        => 'description',
                    'style'     => 'resize:none',
                    'rows'      => '5', 
                    'cols'      => '10'
                ]
            ])

            <div class="text-right">
                <button type="submit" id="btn_save" class="btn btn-primary legitRipple col-xs-12">Save <i class="icon-floppy-disk position-right"></i></button>
            </div>
            {!! Form::hidden('budget_id',$rental_agreement->budget_id, array('id' => 'budget_id')) !!}
            {!! Form::hidden('current_time',null, array('id' => 'current_time')) !!}
        {!! Form::close() !!}
        {!! 
            Form::open([
                'role'      => 'form',
                'url'       => route('rentalAgreement.extend',$rental_agreement->id),
                'method'    => 'post',
                'class'     => 'form-horizontal',
                'id'        => 'form-extend'
            ])
        !!}
            
            {!! Form::hidden('old_no_agreement',$rental_agreement->no_agreement, array('id' => 'old_no_agreement')) !!}
            {!! Form::hidden('old_start_date',$rental_agreement->start_date->format('d/m/Y'), array('id' => 'old_start_date')) !!}
            {!! Form::hidden('old_end_date',$rental_agreement->end_date->format('d/m/Y'), array('id' => 'old_end_date')) !!}
            {!! Form::hidden('budget',null, array('id' => 'extend_budget')) !!}
            {!! Form::hidden('no_agreement',null, array('id' => 'extend_no_agreement')) !!}
            {!! Form::hidden('price_rent',null, array('id' => 'extend_price_rent')) !!}
            {!! Form::hidden('start_rent_date',null, array('id' => 'extend_start_rent_date')) !!}
            {!! Form::hidden('end_rent_date',null, array('id' => 'extend_end_rent_date')) !!}
            {!! Form::hidden('description',null, array('id' => 'extend_description')) !!}
            {!! Form::hidden('current_time',null, array('id' => 'extend_current_time')) !!}
            <div class="text-right">
                <button type="submit" id="btn_extend" class="btn btn-warning legitRipple col-xs-12 hidden">Extend <i class="icon-split position-right"></i></button>
            </div>
        {!! Form::close() !!}
        </div>
    </div>

    {!! Form::hidden('page', 'edit', array('id' => 'page')) !!}
@endsection

@section('page-js')
    <script src="{{ mix('js/datepicker.js') }}"></script>
    <script src="{{ mix('js/rental_agreement.js') }}"></script>
@endsection
