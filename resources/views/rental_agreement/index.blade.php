@extends('layouts.app',['active' => 'rental_agreement'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Rental Agreement</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Dashboard</a></li>
            <li class="active">Rental Agreement</li>
        </ul>

        <ul class="breadcrumb-elements">
			<li class="dropdown">
				<a href="#" class="dropdown-toggle legitRipple breadcrumb-dropdown" data-toggle="dropdown">
					<i class="icon-three-bars position-left"></i>
					Actions
					<span class="caret"></span>
				</a>
				
				<ul class="dropdown-menu dropdown-menu-right">
                <li><a href="{{ route('rentalAgreement.create') }}"><i class="icon-plus2 pull-right"></i> Create</a></li>
				</ul>
			</li>
		</ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat panel-collapsed">
        <div class="panel-heading">
            <h5 class="panel-title">Filters   <a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-md-3 col-lg-3 col-sm-12">
                    @include('form.select', [
                        'field'         => 'factory',
                        'label'         => 'Factory',
                        'mandatory'     => '*Required',
                        'label_col'     => 'col-md-12 col-lg-12 col-sm-12',
                        'form_col'      => 'col-md-12 col-lg-12 col-sm-12',
                        'default'       => auth::user()->factory_id,
                        'div_class'     => (auth::user()->is_super_admin)? '':'hidden',
                        'options'       => [
                            ''          => '-- Select Factory --',
                        ]+$factories,
                        'class'         => 'select-search',
                        'attributes'    => [
                            'id'        => 'select_factory'
                        ]
                    ])
                </div>
                <div class="col-md-3 col-lg-3 col-sm-12">
                    @include('form.select', [
                        'field'         => 'sub_department',
                        'label'         => 'Subdepartment',
                        'label_col'     => 'col-md-12 col-lg-12 col-sm-12',
                        'form_col'      => 'col-md-12 col-lg-12 col-sm-12',
                        'default'       => auth::user()->sub_department_id,
                        'mandatory'     => '*Required',
                        'div_class'     => (auth::user()->is_super_admin)? '':'hidden',
                        'options'       => [
                            ''          => '-- Select Subdepartment --',
                        ]+$sub_departments,
                        'class'         => 'select-search',
                        'attributes'    => [
                            'id'        => 'select_sub_department'
                        ]
                    ])
                </div>
                <div class="col-md-3 col-lg-3 col-sm-12">
                    @include('form.select', [
                        'field'         => 'budget',
                        'label'         => 'Budget',
                        'label_col'     => 'col-md-12 col-lg-12 col-sm-12',
                        'form_col'      => 'col-md-12 col-lg-12 col-sm-12',
                        'options'       => [
                            ''          => '-- Select Budget --',
                        ],
                        'class'         => 'select-search',
                        'attributes'    => [
                            'id'        => 'select_budget'
                        ]
                    ])
                </div>
                <div class="col-md-3 col-lg-3 col-sm-12">
                    @include('form.select', [
                        'field'         => 'status',
                        'label'         => 'Status',
                        'label_col'     => 'col-md-12 col-lg-12 col-sm-12',
                        'form_col'      => 'col-md-12 col-lg-12 col-sm-12',
                        'default'       => '1',
                        'options'       => [
                            ''          => '-- Select Status --',
                            '1'          => 'Active',
                            '2'          => 'Not Active',
                            '3'          => 'Assets Returned',
                        ],
                        'class'         => 'select-search',
                        'attributes'    => [
                            'id'        => 'select_status_agreement'
                        ]
                    ])
                </div>
            </div>
                

        </div>
    </div>

    <div class="panel panel-flat">
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-basic table-condensed" id="rentalAgreementTable">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>id</th>
                            <th>Factory</th>
                            <th>Subdepartment</th>
                            <th>Budget</th>
                            <th>No KK</th>
                            <th>No KK Parent</th>
                            <th>Duration In Day</th>
                            <th>Price</th>
                            <th>Start Rent</th>
                            <th>End Rent</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    {!! Form::hidden('page', 'index', array('id' => 'page')) !!}
    {!! Form::hidden('msg', $msg, array('id' => 'msg')) !!}
@endsection

@section('page-js')
    <script src="{{ mix('js/rental_agreement.js') }}"></script>
@endsection
