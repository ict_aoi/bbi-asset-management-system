<script type="x-tmpl-mustache" id="upload">
	{% #item %}
		<tr {% #is_error %} style="background-color:#FF5722;color:#fff" {% /is_error %}>
			<td>{% no %}</td>
			<td>
				{% factory_name %}
			</td>
			<td>
				{% name %}
			</td>
			<td>
				{% description %}
			</td>
			<td>
				{% system_log %}
			</td>
		</tr>
	{%/item%}
</script>