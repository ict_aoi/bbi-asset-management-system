<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>DIAS | Digital Assets </title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <!-- Global stylesheets -->

	    <link href="{{ asset('css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
	    <link href="{{ mix('css/backend.css') }}" rel="stylesheet" type="text/css">
       
    </head>
    <body>
        <div class="page-container">
            <div class="page-content">
                <div class="content-wrapper">
                    @yield('page-header')
                    <div class="content">
                        
                        @yield('page-content')
                        <script src="{{ mix('js/backend.js') }}"></script>
                        <script src="{{ mix('js/notification.js') }}"></script>
                        @yield('page-js')
                    </div>
                </div>
            </div>
        </div>
	</body>
</html>
