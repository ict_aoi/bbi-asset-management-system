<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1,viewport-fit=cover">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>DIAS | Digital Assets</title>
        <link href="{{ asset('css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
        <link rel="shortcut icon" href="{{ asset('images/favicon.png') }}">
        <link href="{{ mix('css/backend.css') }}" rel="stylesheet" type="text/css">
        <meta name="description" content="BBI Digital Asset System App">
        <meta name="author" content="ICT">
        @yield('page-css')
        
    </head>
    <body>
        @include('includes.main_navbar')
        @include('includes.top_notif')
        <div class="page-container">
            <div class="page-content">
                @include('includes.side_navbar')
                <div class="content-wrapper">
                    @yield('page-header')
                    <div class="content">
                            @yield('page-content')
                            @yield('page-modal')
                            <script src="{{ mix('js/backend.js') }}"></script>
                            <script src="{{ mix('js/floating_button.js') }}"></script>
                            <script src="{{ mix('js/notification.js') }}"></script>
                            @yield('page-js')
                            <script type="text/javascript">
                                var timestamp = {{ time() }}
                                function updateTime(){
                                    time=new Date();
                                    second=time.getSeconds();
                                    minute=time.getMinutes();
                                    hour=time.getHours();
                                    if(second<10){
                                        second='0'+second;
                                    }
                                    if(minute<10){
                                        minute='0'+minute;
                                    }
                                    if(hour<10){
                                        hour='0'+hour;
                                    }

                                    var _month = parseInt(time.getMonth())+1;
                                    if(_month < 10) _month = '0'+_month;
                                    else _month;

                                    var _date = time.getDate();
                                    if(_date < 10) _date = '0'+_date;
                                    else _date;

                                    $('#time').html(time.getFullYear()+'-'+_month+'-'+_date+' '+hour+':'+minute+':'+second);
                                    timestamp++;
                                    
                                }
                                $(function(){
                                    setInterval(updateTime, 1000);
                                });
                            </script>
                        <div class="footer text-muted">
                            &copy; 2019. <a href="route('home')">DIAS | Digital Assets</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
