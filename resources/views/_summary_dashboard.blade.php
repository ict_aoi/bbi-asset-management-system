<script type="x-tmpl-mustache" id="summary_dashboard">
	{% #item %}	
		<div class="col-lg-3">
				<div style="background-color: #018BCD;border-color: #018BCD;color: #00000;">
					<div class="panel-body text-center">
						<h1 class="no-margin">{% total_asset_all %}</h1>
						Total Asset BBIS
					</div>
				</div>
			</div>
			<div class="col-lg-2">
				<div style="background-color: #58D2D3;border-color: #58D2D3;color: #00000;">
					<div class="panel-body text-center">
						<h1 class="no-margin">{% total_asset_in_used %}</h1>
						Total Asset In Used
					</div>
				</div>
			</div>
			<div class="col-lg-2">
				<div style="background-color: #C4B4E4;border-color: #C4B4E4;color: #00000;">
					<div class="panel-body text-center">
						<h1 class="no-margin">{% total_asset_in_stock %}</h1>
						Total Asset In Stock
					</div>
				</div>
			</div>
			<div class="col-lg-3">
				<div style="background-color: #7BC0F2;border-color: #7BC0F2;color: #00000;">
					<div class="panel-body text-center">
						<h1 class="no-margin">{% total_asset_broken %}</h1>
						Total Asset Broken
					</div>
				</div>
			</div>
			<div class="col-lg-2">
				<div style="background-color: #FFC799;border-color: #FFC799;color: #00000;">
					<div class="panel-body text-center">
						<h1 class="no-margin">{% total_asset_obsolete %}</h1>
						Total Asset Obsolete
					</div>
				</div>
			</div>
	{%/item%}
</script>