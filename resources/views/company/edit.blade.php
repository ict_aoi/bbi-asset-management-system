@extends('layouts.app',['active' => 'company'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Company</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Dashboard</a></li>
            <li>Organisasi</li>
            <li><a href="{{ route('company.index') }}">Company</a></li>
            <li class="active">Edit</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat">
    <div class="panel-body">
        {!!
            Form::open([
                'role' => 'form',
                'url' => route('company.update',$company->id),
                'method' => 'post',
                'class' => 'form-horizontal',
                'id'=> 'form'
            ])
        !!}
            @include('form.text', [
                'field' => 'code',
                'label' => 'Code',
                'default' => strtoupper($company->code),
                'mandatory' => '*Required',
                'label_col' => 'col-md-2 col-lg-2 col-sm-12',
                'form_col' => 'col-md-10 col-lg-10 col-sm-12',
                'attributes' => [
                    'id' => 'code',
                    'readonly' => 'readonly'
                ]
            ])

            @include('form.text', [
                'field' => 'name',
                'label' => 'Name',
                'default' => ucwords($company->name),
                'mandatory' => '*Required',
                'label_col' => 'col-md-2 col-lg-2 col-sm-12',
                'form_col' => 'col-md-10 col-lg-10 col-sm-12',
                'attributes' => [
                    'id' => 'name',
                    'readonly' => 'readonly'
                ]
            ])

            @include('form.textarea', [
                'field' => 'description',
                'label' => 'Description',
                'default' => ucwords($company->description),
                'label_col' => 'col-md-2 col-lg-2 col-sm-12',
                'form_col' => 'col-md-10 col-lg-10 col-sm-12',
                'attributes' => [
                    'id' => 'description',
                    'rows' => 5,
                    'style' => 'resize: none;'
                ]
            ])

            <div class="text-right">
                <button type="submit" class="btn btn-primary legitRipple col-xs-12">Save <i class="icon-floppy-disk position-right"></i></button>
            </div>
        
        {!! Form::close() !!}
    </div>
</div>

@endsection

@section('page-js')
<script src="{{ mix('js/company.js') }}"></script>
@endsection