@extends('layouts.app',['active' => 'company'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Company</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Dashboard</a></li>
            <li>Organization</li>
            <li class="active">Company</li>
        </ul>

        <ul class="breadcrumb-elements">
			<li class="dropdown">
				<a href="#" class="dropdown-toggle legitRipple breadcrumb-dropdown" data-toggle="dropdown">
					<i class="icon-three-bars position-left"></i>
					Actions
					<span class="caret"></span>
				</a>
				
				<ul class="dropdown-menu dropdown-menu-right">
					<li><a href="{{ route('company.create') }}"><i class="icon-plus2 pull-right"></i> Create</a></li>
				</ul>
			</li>
		</ul>
    </div>
</div>
@endsection
@section('page-content')
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="companyTable">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Company Code</th>
                        <th>Company</th>
                        <th>Description</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
{!! Form::hidden('msg', $msg, array('id' => 'msg')) !!}
@endsection

@section('page-js')
<script src="{{ mix('js/company.js') }}"></script>
@endsection