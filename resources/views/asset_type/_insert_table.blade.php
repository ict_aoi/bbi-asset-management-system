<script type="x-tmpl-mustache" id="asset_field_custom_table">
	{% #item %}
		<tr {% ^is_custom %} style="background-color:#666666;color:#fff" {% /is_custom %}>
			<td style="text-align:center;">{% no %}</td>
			<td>
				{% custom_field_id %}
			</td>
			<td>
				{% custom_label %}
			</td>
			<td>
				{% custom_type_name %}
			</td>
			<td>
				{% custom_default_value %}
			</td>
			<td>
				{% #is_required %}
					<div class="checker border-info-600 text-info-800">
						<span class="checked"><input type="checkbox" class="control-info" checked="checked"></span>
					</div>
				{% /is_required %}
			</td>
			<td>
				{% #is_custom %}
				<ul class="icons-list">
					<li><button type="button" id="edit_{% _id %}" data-id="{% _id %}" class="btn btn-default btn-edit-item" style="color: black;"><i class="icon-pencil7"></i></button></li>
					{% #is_edited %}
						<li><button type="button" id="delete_{% _id %}" data-id="{% _id %}" class="btn btn-default" style="color: black;" disabled><i class="icon-trash"></i></button></li>
					{% /is_edited %}
					{% ^is_edited %}
						<li><button type="button" id="delete_{% _id %}" data-id="{% _id %}" class="btn btn-default btn-delete-item" style="color: black;"><i class="icon-trash"></i></button></li>
					{% /is_edited %}
				</ul>
				{% /is_custom %}
				
			</td>
		</tr>
	{%/item%}
</script>