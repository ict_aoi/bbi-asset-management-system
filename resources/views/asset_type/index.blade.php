@extends('layouts.app',['active' => 'asset_type'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Asset Type</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Dashboard</a></li>
            <li>Setting</li>
            <li class="active">Asset Type</li>
        </ul>

        <ul class="breadcrumb-elements">
			<li class="dropdown">
				<a href="#" class="dropdown-toggle legitRipple breadcrumb-dropdown" data-toggle="dropdown">
					<i class="icon-three-bars position-left"></i>
					Actions
					<span class="caret"></span>
				</a>
				
				<ul class="dropdown-menu dropdown-menu-right">
					<li><a href="{{ route('assetType.create') }}"><i class="icon-plus2 pull-right"></i> Create</a></li>
				</ul>
			</li>
		</ul>
    </div>
</div>
@endsection
@section('page-content')
    <div class="panel panel-flat panel-collapsed">
        <div class="panel-heading">
            <h5 class="panel-title">Filters   <a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
                @include('form.select', [
                    'field'         => 'sub_department',
                    'label'         => 'Sub Departement',
                    'default'       => $sub_department_id,
                    'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                    'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                    'div_class'     => (auth::user()->is_super_admin)? '':'hidden',
                    'options'       => [
                        ''          => '-- Select Sub Departement --',
                    ]+$sub_departments,
                    'class'         => 'select-search',
                    'attributes'    => [
                        'id'        => 'select_sub_department'
                    ]
                ])

                @include('form.select', [
                    'field'         => 'is_consumable',
                    'label'         => 'Consumable',
                    'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                    'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                    'options'       => [
                        ''          => '-- Select Asset Type --',
                        '1'         => 'Consumbale',
                        '2'         => 'Asset',
                    ],
                    'class'         => 'select-search',
                    'attributes'    => [
                        'id'        => 'select_is_consumable'
                    ]
                ])
        </div>
    </div>

    <div class="panel panel-flat">
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-basic table-condensed" id="assetTypeTable">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Sub Departement</th>
                            <th>ERP Category Name</th>
                            <th>ERP Group Name</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Consumable</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    {!! Form::hidden('msg', $msg, array('id' => 'msg')) !!}
    {!! Form::hidden('is_super_admin', (auth::user()->is_super_admin)? '1':'0', array('id' => 'is_super_admin')) !!}
    {!! Form::hidden('form_status', 'index', array('id' => 'form_status')) !!}
    {!! Form::hidden('mappings', '[]', array('id' => 'mappings')) !!}
@endsection

@section('page-js')
    <script src="{{ mix('js/switch.js') }}"></script>
    <script src="{{ mix('js/asset_type.js') }}"></script>
@endsection
