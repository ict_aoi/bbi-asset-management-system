<table class="table">
	<thead>
	  <tr>
		<th>Asset Type Name</th>
		<th>Action</th>
	  </tr>
	</thead>

	<tbody>
		@foreach ($lists as $key => $list)
			<tr>
				<td>
					{{ ucwords($list->name) }}
				</td>
				<td>
					<button data-dismiss="modal" class="btn btn-info btn-xs btn-choose"
						type="button" data-id="{{ $list->id }}" data-name="{{ $list->name }}">Select
					</button>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>

{!! $lists->appends(Request::except('page'))->render() !!}
