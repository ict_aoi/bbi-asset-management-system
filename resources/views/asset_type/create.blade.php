@extends('layouts.app',['active' => 'asset_type'])

@section('page-css')

@endsection

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Asset Type</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Dashboard</a></li>
            <li>Setting</li>
            <li ><a href="{{ route('assetType.index') }}">Asset Type</a></li>
            <li class="active">Creae</li>
        </ul>
    </div>
</div>
@endsection
@section('page-content')
    <div class="panel panel-flat">
        <div class="panel-body">
            {!!
                Form::open([
                    'role'      => 'form',
                    'url'       => route('assetType.store'),
                    'method'    => 'post',
                    'class'     => 'form-horizontal',
                    'enctype'   => 'multipart/form-data',
                    'id'        => 'form'
                ])
            !!}
            <div class="row">
                <div class="col-md-6">
                    @include('form.select', [
                        'field'             => 'sub_department',
                        'label'             => 'Sub Departement',
                        'mandatory'         => '*Required',
                        'default'           => auth::user()->sub_department_id,
                        'label_col'         => 'col-md-3 col-lg-3 col-sm-12',
                        'form_col'          => 'col-md-9 col-lg-9 col-sm-12',
                        'div_class'         => (auth::user()->is_super_admin)? '':'hidden',
                        'options'           => [
                            ''              => '-- Select Sub Departement --',
                        ]+$subdepartments,
                        'class' => 'select-search',
                        'attributes' => [
                            'id' => 'select_sub_department'
                        ]
                    ])

                    @include('form.select', [
                        'field'             => 'erp_catogories',
                        'label'             => 'ERP Category Product',
                        'mandatory'         => '*Required',
                        'label_col'         => 'col-md-3 col-lg-3 col-sm-12',
                        'form_col'          => 'col-md-9 col-lg-9 col-sm-12',
                        'options'           => [
                            ''              => '-- Select ERP Category Product --',
                        ]+$erp_catogories,
                        'class' => 'select-search',
                        'attributes' => [
                            'id' => 'select_erp_catogories'
                        ]
                    ])

                    @include('form.text', [
                        'field'         => 'code',
                        'label'         => 'Asset Code',
                        'mandatory'     => '*Required',
                        'label_col'     => 'col-md-3 col-lg-3 col-sm-12',
                        'form_col'      => 'col-md-9 col-lg-9 col-sm-12',
                        'attributes'    => [
                            'id'        => 'code'
                        ]
                    ])

                    @include('form.text', [
                        'field'         => 'name',
                        'label'         => 'Name',
                        'mandatory'     => '*Required',
                        'label_col'     => 'col-md-3 col-lg-3 col-sm-12',
                        'form_col'      => 'col-md-9 col-lg-9 col-sm-12',
                        'attributes'    => [
                            'id'        => 'name'
                        ]
                    ])

                    @include('form.checkbox', [
                        'field'             => 'is_consumable',
                        'label'             => 'Consumable',
                        'label_col'         => 'col-md-3 col-lg-3 col-sm-12',
                        'form_col'          => 'col-md-9 col-lg-9 col-sm-12',
                        'style_checkbox'    => 'checkbox checkbox-switchery',
                        'class'             => 'switchery',
                        'attributes'        => [
                            'id'            => 'checkbox_is_consumable'
                        ]
                    ])

                    @include('form.textarea', [
                        'field'             => 'description',
                        'label'             => 'Description',
                        'label_col'         => 'col-md-3 col-lg-3 col-sm-12',
                        'form_col'          => 'col-md-9 col-lg-9 col-sm-12',
                        'attributes'        => [
                            'id'            => 'description',
                            'rows'          => 5,
                            'style'         => 'resize: none;'
                        ]
                    ])
                </div>

                <div class="col-md-6">
                    @include('form.select', [
                        'field'             => 'custom_field',
                        'label'             => 'Custom Field',
                        'label_col'         => 'col-md-3 col-lg-3 col-sm-12',
                        'form_col'          => 'col-md-9 col-lg-9 col-sm-12',
                        'options'           => [
                            ''              => '-- Select Custom Field --',
                        ]+$custom_fields,
                        'class'             => 'select-search',
                        'attributes'        => [
                            'id'            => 'select_custom_field'
                        ]
                    ])

                    @include('form.text', [
                        'field'             => 'label',
                        'label'             => 'Custom Label',
                        'label_col'         => 'col-md-3 col-lg-3 col-sm-12',
                        'form_col'          => 'col-md-9 col-lg-9 col-sm-12',
                        'attributes'        => [
                            'id'            => 'custom_label'
                        ]
                    ])

                    @include('form.select', [
                        'field'             => 'custom_type',
                        'label'             => 'Custom Type',
                        'label_col'         => 'col-md-3 col-lg-3 col-sm-12',
                        'form_col'          => 'col-md-9 col-lg-9 col-sm-12',
                        'options'           => [
                            ''              => '-- Select Custom Type --',
                            'text'          => 'Text',
                            'option'        => 'Option',
                            'number'        => 'Number',
                            'lookup'        => 'Lookup',
                        ],
                        'class'             => 'select-search',
                        'attributes'        => [
                            'id'            => 'select_custom_type'
                        ]
                    ])

                    @include('form.tag', [
                        'field'             => 'default_value',
                        'label'             => 'Custom Default Value',
                        'id_div'            => 'div_default_value',
                        'div_class'         => 'hidden',
                        'label_col'         => 'col-md-3 col-lg-3 col-sm-12',
                        'form_col'          => 'col-md-9 col-lg-9 col-sm-12',
                        'attributes'        => [
                            'id'            => 'custom_default_value',
                        ]
                    ])

                    @include('form.picklist', [
                        'field'             => 'custom_lookup',
                        'label'             => 'Custom Consumable Lookup',
                        'name'              => 'custom_lookup',
                        'readonly'          => 'readonly',
                        'div_id'            => 'div_custom_lookup',
                        'div_class'         => 'hidden',
                        'placeholder'       => null,
                        'label_col'         => 'col-md-3 col-lg-3 col-sm-12',
                        'form_col'          => 'col-md-9 col-lg-9 col-sm-12',
                        'attributes'        => [
                            'id'            => 'custom_lookup',
                        ]
                    ])
                    
                    @include('form.checkbox', [
                        'field'             => 'is_required',
                        'label'             => 'Custom Required',
                        'label_col'         => 'col-md-3 col-lg-3 col-sm-12',
                        'form_col'          => 'col-md-9 col-lg-9 col-sm-12',
                        'style_checkbox'    => 'checkbox checkbox-switchery',
                        'class'             => 'switchery',
                        'attributes'        => [
                            'id'            => 'is_required'
                        ]
                    ])
                    <button type="button" class="btn btn-primary legitRipple" style="width:100%" id="btn_save_custom_field">Save Custom Field <i class="icon-floppy-disk position-right"></i></button>
                </div>
            </div>
            
            <br/>

            <div class="row">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h6 class="panel-title text-semibold">Asset Fields</h6>
                    </div>

                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-basic table-condensed">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Label</th>
                                        <th>Type</th>
                                        <th>Default Value</th>
                                        <th>Reqired</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody id="asset_custom_field"></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::hidden('_custom_field_id', null, array('id' => '_custom_field_id')) !!}
            {!! Form::hidden('form_status', 'create', array('id' => 'form_status')) !!}
            {!! Form::hidden('mappings', json_encode($mappings), array('id' => 'mappings')) !!}
            {!! Form::hidden('current_time',null, array('id' => 'current_time')) !!}
            <div class="text-right">
                <button type="submit" class="btn btn-primary legitRipple col-xs-12">Save <i class="icon-floppy-disk position-right"></i></button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('page-modal')
    @include('asset_type._insert_table')
    @include('form.modal_picklist', [
        'name' => 'custom_lookup',
        'title' => 'Daftar List Tipe Consumable Asset',
        'placeholder' => 'Cari berdasarkan Nama Asset',
    ])
@endsection

@section('page-js')
    <script src="{{ mix('js/tags.js') }}"></script>
    <script src="{{ mix('js/switch.js') }}"></script>
    <script src="{{ mix('js/asset_type.js') }}"></script>
@endsection
