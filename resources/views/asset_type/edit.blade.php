@extends('layouts.app',['active' => 'asset_type'])

@section('page-css')

@endsection

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Asset Type</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Dashboard</a></li>
            <li>Setting</li>
            <li ><a href="{{ route('assetType.index') }}">Asset Type</a></li>
            <li class="active">Edit</li>
        </ul>
    </div>
</div>
@endsection
@section('page-content')
<div class="panel panel-flat">
    <div class="panel-body">
        {!!
            Form::open([
                'role' => 'form',
                'url' => route('assetType.update',$asset_type->id),
                'method' => 'post',
                'class' => 'form-horizontal',
                'enctype' => 'multipart/form-data',
                'id'=> 'form'
            ])
        !!}
        <div class="row">
            <div class="col-md-6">
                @if($asset_type->erp_category_id)
                    @include('form.text', [
                        'field'         => 'erp_catogories',
                        'label'         => 'ERP Category Product',
                        'mandatory'     => '*Required',
                        'default'       => strtoupper($asset_type->erp_category_name),
                        'label_col'     => 'col-md-3 col-lg-3 col-sm-12',
                        'form_col'      => 'col-md-9 col-lg-9 col-sm-12',
                        'attributes'    => [
                            'id'        => 'select_erp_catogories',
                            'disabled'  => 'disabled'
                        ]
                    ])
                @else
                    @include('form.select', [
                        'field'             => 'erp_catogories',
                        'label'             => 'ERP Category Product',
                        'mandatory'         => '*Required',
                        'default'           => $asset_type->erp_category_id,
                        'label_col'         => 'col-md-3 col-lg-3 col-sm-12',
                        'form_col'          => 'col-md-9 col-lg-9 col-sm-12',
                        'options'           => [
                            ''              => '-- Select ERP Category Product --',
                        ]+$erp_catogories,
                        'class' => 'select-search',
                        'attributes' => [
                            'id' => 'select_erp_catogories'
                        ]
                    ])
                @endif
                
                @include('form.text', [
                    'field'         => 'code',
                    'label'         => 'Asset Code',
                    'mandatory'     => '*Required',
                    'default'       => strtoupper($asset_type->code),
                    'label_col'     => 'col-md-3 col-lg-3 col-sm-12',
                    'form_col'      => 'col-md-9 col-lg-9 col-sm-12',
                    'attributes'    => [
                        'id'        => 'code',
                        'readonly'  => 'readonly'
                    ]
                ])
                
                @include('form.text', [
                    'field'         => 'name',
                    'label'         => 'Name',
                    'mandatory'     => '*Required',
                    'default'       => ucwords($asset_type->name),
                    'label_col'     => 'col-md-3 col-lg-3 col-sm-12',
                    'form_col'      => 'col-md-9 col-lg-9 col-sm-12',
                    'attributes'    => [
                        'id'        => 'name'
                    ]
                ])

                @include('form.textarea', [
                    'field'             => 'description',
                    'label'             => 'Description',
                    'default'           => $asset_type->description,
                    'label_col'         => 'col-md-3 col-lg-3 col-sm-12',
                    'form_col'          => 'col-md-9 col-lg-9 col-sm-12',
                    'attributes'        => [
                        'id'            => 'description',
                        'rows'          => 5,
                        'style'         => 'resize: none;'
                    ]
                ])
            </div>
            {!! Form::hidden('current_time',null, array('id' => 'current_time')) !!}
            {!! Form::close() !!}

            <div class="col-md-6">
                @include('form.select', [
                    'field'             => 'custom_field',
                    'label'             => 'Custom Field',
                    'label_col'         => 'col-md-3 col-lg-3 col-sm-12',
                    'form_col'          => 'col-md-9 col-lg-9 col-sm-12',
                    'options'           => [
                        ''              => '-- Select Custom Field --',
                    ]+$custom_fields,
                    'class'             => 'select-search',
                    'attributes'        => [
                        'id'            => 'select_custom_field'
                    ]
                ])

                @include('form.text', [
                    'field'             => 'label',
                    'label'             => 'Custom Label',
                    'label_col'         => 'col-md-3 col-lg-3 col-sm-12',
                    'form_col'          => 'col-md-9 col-lg-9 col-sm-12',
                    'attributes'        => [
                        'id'            => 'custom_label'
                    ]
                ])


                @include('form.select', [
                    'field'             => 'custom_type',
                    'label'             => 'Custom Type',
                    'label_col'         => 'col-md-3 col-lg-3 col-sm-12',
                    'form_col'          => 'col-md-9 col-lg-9 col-sm-12',
                    'options'           => [
                        ''              => '-- Select Custom Type --',
                        'text'          => 'Text',
                        'option'        => 'Option',
                        'number'        => 'Number',
                        'lookup'        => 'Lookup',
                    ],
                    'class'             => 'select-search',
                    'attributes'        => [
                        'id'            => 'select_custom_type'
                    ]
                ])

                @include('form.tag', [
                    'field'             => 'default_value',
                    'label'             => 'Custom Default Value',
                    'id_div'            => 'div_default_value',
                    'div_class'         => 'hidden',
                    'label_col'         => 'col-md-3 col-lg-3 col-sm-12',
                    'form_col'          => 'col-md-9 col-lg-9 col-sm-12',
                    'attributes'        => [
                        'id'            => 'custom_default_value',
                    ]
                ])

                @include('form.picklist', [
                    'field'             => 'custom_lookup',
                    'label'             => 'Custom Consumable Lookup',
                    'name'              => 'custom_lookup',
                    'readonly'          => 'readonly',
                    'div_id'            => 'div_custom_lookup',
                    'div_class'         => 'hidden',
                    'placeholder'       => null,
                    'label_col'         => 'col-md-3 col-lg-3 col-sm-12',
                    'form_col'          => 'col-md-9 col-lg-9 col-sm-12',
                    'attributes'        => [
                        'id'            => 'custom_lookup',
                    ]
                ])
                
                @include('form.checkbox', [
                    'field'             => 'is_required',
                    'label'             => 'Custom Required',
                    'label_col'         => 'col-md-3 col-lg-3 col-sm-12',
                    'form_col'          => 'col-md-9 col-lg-9 col-sm-12',
                    'style_checkbox'    => 'checkbox checkbox-switchery',
                    'class'             => 'switchery',
                    'attributes'        => [
                        'id'            => 'is_required'
                    ]
                ])

                {!! Form::hidden('_custom_field_id', null, array('id' => '_custom_field_id')) !!}
                {!! Form::hidden('department', $asset_type->department_id, ['id' => 'department_id']) !!}
                {!! Form::hidden('sub_department', $asset_type->sub_department_id, ['id' => 'select_sub_department']) !!}
                {!! Form::hidden('asset_type_id', $asset_type->id, array('id' => 'asset_type_id')) !!}
                {!! Form::hidden('is_comsumable_chacked', $asset_type->is_consumable, array('id' => 'is_comsumable_chacked')) !!}
                {!! Form::hidden('url_data_custom_field', route('assetType.dataField',$asset_type->id), array('id' => 'url_data_custom_field')) !!}
                
                {!! Form::hidden('form_status', 'edit', array('id' => 'form_status')) !!}
                {!! Form::hidden('url_update_custom_field', null, array('id' => 'url_update_custom_field')) !!}
                {!! Form::hidden('mappings', json_encode($mappings), array('id' => 'mappings')) !!}
                <button type="button" class="btn btn-primary legitRipple" style="width:100%" id="btn_save_custom_field">Save Custom Field <i class="icon-floppy-disk position-right"></i></button>
                
            </div>
        </div>

        <br/>
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h6 class="panel-title text-semibold">Asset Fields</h6>
                </div>

                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-basic table-condensed" id="customFieldTable">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Id</th>
                                    <th>Name</th>
                                    <th>Label</th>
                                    <th>Type</th>
                                    <th>Default Value</th>
                                    <th>Required</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="text-right">
            <button type="submit" class="btn btn-primary legitRipple col-xs-12" id="submit_button">Save <i class="icon-floppy-disk position-right"></i></button>
        </div>
    </div>
</div>
@endsection

@section('page-modal')
    @include('form.modal_picklist', [
        'name' => 'custom_lookup',
        'title' => 'List Consumable Asset',
        'placeholder' => 'Search based on name',
    ])
@endsection

@section('page-js')
    <script src="{{ mix('js/tags.js') }}"></script>
    <script src="{{ mix('js/switch.js') }}"></script>
    <script src="{{ mix('js/asset_type.js') }}"></script>
@endsection
