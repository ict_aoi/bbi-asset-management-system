@extends('layouts.app',['active' => 'role'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Role</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Dashboard</a></li>
            <li> Account Management</li>
            <li ><a href="{{ route('role.index') }}">Role </a></li>
            <li class="active">Edit</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="row">
    <div class="col-md-6">
        <div class="panel panel-flat">
            <div class="panel-body">
                {!!
                    Form::open([
                        'role'      => 'form',
                        'url'       => route('role.update',$role->id),
                        'method'    => 'post',
                        'class'     => 'form-horizontal',
                        'enctype'   => 'multipart/form-data',
                        'id'        => 'form'
                    ])
                !!}
                    @include('form.text', [
                        'field'         => 'name',
                        'label'         => 'Name',
                        'default'       => $role->name,
                        'mandatory'     => '*Required',
                        'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                        'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                        'attributes'    => [
                            'id'        => 'name',
                            'readonly'  => 'readonly'
                        ]
                    ])

                    @include('form.textarea', [
                        'field'         => 'description',
                        'label'         => 'Description',
                        'default'       => $role->description,
                        'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                        'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                        'attributes'    => [
                            'id'        => 'description',
                            'rows'      => 5,
                            'style'     => 'resize: none;'
                        ]
                    ])

                    @include('form.select', [
                        'field'         => 'permission',
                        'label'         => 'Permission',
                        'options'       => [
                            ''          => '-- Select Permission --',
                        ]+$permissions,
                        'class'         => 'select-search',
                        'attributes'    => [
                            'id'        => 'select_permission'
                        ]
                    ])
                {!! Form::hidden('role_id', $role->id, array('id' => 'role_id')) !!}
                {!! Form::hidden('current_time', null, array('id' => 'current_time')) !!}
                {!! Form::hidden('form_status', 'edit', array('id' => 'form_status')) !!}
                {!! Form::hidden('mappings', json_encode($role->permissions()->get()), array('id' => 'mappings')) !!}
                <div class="text-right">
                    <button type="submit" class="btn btn-primary legitRipple col-xs-12">Save <i class="icon-floppy-disk position-right"></i></button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h6 class="panel-title text-semibold">Role - Permission </h6>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-basic table-condensed" id="permissionTable">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Permission</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
{!! Form::hidden('url_data_permission', route('role.dataPermission',$role->id), array('id' => 'url_data_permission')) !!}
                
@endsection

@section('page-modal')
	@include('role._table')
@endsection

@section('page-js')
    <script src="{{ mix('js/role.js') }}"></script>
@endsection
