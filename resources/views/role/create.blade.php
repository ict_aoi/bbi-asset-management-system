@extends('layouts.app',['active' => 'role'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Role</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Dashboard</a></li>
            <li> Account Management</li>
            <li ><a href="{{ route('role.index') }}">Role </a></li>
            <li class="active">Create</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="row">
    <div class="col-md-6">
        <div class="panel panel-flat">
            <div class="panel-body">
                {!!
                    Form::open([
                        'role'      => 'form',
                        'url'       => route('role.store'),
                        'method'    => 'post',
                        'class'     => 'form-horizontal',
                        'enctype'   => 'multipart/form-data',
                        'id'        => 'form'
                    ])
                !!}
                    @include('form.text', [
                        'field'         => 'name',
                        'label'         => 'Name',
                        'mandatory'     => '*Required',
                        'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                        'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                        'attributes'    => [
                            'id'        => 'name'
                        ]
                    ])

                    @include('form.textarea', [
                        'field'         => 'description',
                        'label'         => 'Description',
                        'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                        'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                        'attributes'    => [
                            'id'        => 'description',
                            'rows'      => 5,
                            'style'     => 'resize: none;'
                        ]
                    ])

                    @include('form.select', [
                        'field'         => 'permission',
                        'label'         => 'Permission',
                        'options'       => [
                            ''          => '-- Select Permission --',
                        ]+$permissions,
                        'class'         => 'select-search',
                        'attributes'    => [
                            'id'        => 'select_permission'
                        ]
                    ])
                {!! Form::hidden('form_status', 'create', array('id' => 'form_status')) !!}
                {!! Form::hidden('mappings', '[]', array('id' => 'mappings')) !!}
                {!! Form::hidden('current_time', null, array('id' => 'current_time')) !!}
                <div class="text-right">
                    <button type="submit" class="btn btn-primary legitRipple col-xs-12">Save <i class="icon-floppy-disk position-right"></i></button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h6 class="panel-title text-semibold">Role - Permission </h6>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-basic table-condensed">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Permission</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody id="role_permission"></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-modal')
	@include('role._table')
@endsection

@section('page-js')
<script src="{{ mix('js/role.js') }}"></script>
@endsection
