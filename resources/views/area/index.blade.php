@extends('layouts.app',['active' => 'area'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Area</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Dashboard</a></li>
            <li class="active">Area</li>
        </ul>

        <ul class="breadcrumb-elements">
			<li class="dropdown">
				<a href="#" class="dropdown-toggle legitRipple breadcrumb-dropdown" data-toggle="dropdown">
					<i class="icon-three-bars position-left"></i>
					Actions
					<span class="caret"></span>
				</a>
				
				<ul class="dropdown-menu dropdown-menu-right">
					<li><a href="{{ route('area.import') }}"><i class="icon-drawer-out pull-right"></i> Import</a></li>
					<li><a href="{{ route('area.create') }}"><i class="icon-plus2 pull-right"></i> Create</a></li>
                    <li><a href="{{ route('area.printAllBarcode') }}" target="_blank"><i class="icon-barcode2 pull-right"></i> Barcode</a></li>
				</ul>
			</li>
		</ul>
    </div>
</div>
@endsection

@section('page-content')
    <div class="panel panel-flat panel-collapsed">
        <div class="panel-heading">
            <h5 class="panel-title">Filters   <a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            
                @include('form.select', [
                    'field'         => 'area_location',
                    'label'         => 'Area Location',
                    'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                    'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                    'options'       => [
                        ''          => 'All',
                        '1'          => 'Stock',
                        '2'          => 'Non Stock',
                    ],
                    'class'         => 'select-search',
                    'attributes'    => [
                        'id'        => 'select_area_location'
                    ]
                ])
        </div>
    </div>

    <div class="panel panel-flat">
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-basic table-condensed" id="areaTable">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Factory</th>
                            <th>Area</th>
                            <th>Description</th>
                            <th>Is Stock Area</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    {!! Form::hidden('msg', $msg, array('id' => 'msg')) !!}
@endsection


@section('page-js')
<script src="{{ mix('js/area.js') }}"></script>
@endsection
