@extends('layouts.app',['active' => 'area'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Area</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Dashboard</a></li>
            <li><a href="{{ route('area.index') }}">Area</a></li>
            <li class="active">Edit</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat">
    {!! 
        Form::open([
            'role'      => 'form',
            'url'       => route('area.update',$area->id),
            'method'    => 'post',
            'class'     => 'form-horizontal',
            'id'        => 'form'
        ])
    !!}
    <div class="panel-body">
        @include('form.text', [
            'field'         => 'factory',
            'label'         => 'Factory',
            'default'       => $area->factory->name,
            'mandatory'     => '*Required',
            'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
            'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
            'attributes'    => [
                'id'        => 'select_factory',
                'readonly'  => 'readonly' 
            ]
        ])

        @include('form.text', [
            'field'         => 'name',
            'label'         => 'Name',
            'default'       => $area->name,
            'mandatory'     => '*Required',
            'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
            'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
            'attributes'    => [
                'id'        => 'name'
            ]
        ])

        @include('form.textarea', [
            'field'         => 'description',
            'label'         => 'Description',
            'default'       => $area->description,
            'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
            'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
            'attributes'    => [
                'id'        => 'description',
                'rows'      => 5,
                'style'     => 'resize: none;'
            ]
        ])

        @include('form.checkbox', [
            'field'             => 'is_area_stock',
            'label'             => 'Is Area Stock',
            'label_col'         => 'col-md-2 col-lg-2 col-sm-12',
            'form_col'          => 'col-md-10 col-lg-10 col-sm-12',
            'style_checkbox'    => 'checkbox checkbox-switchery',
            'class'             => 'switchery',
            'attributes'        => [
                'id'            => 'checkbox_is_area_stock'
            ]
        ])

        {!! Form::hidden('current_time','', array('id' => 'current_time')) !!}
        {!! Form::hidden('is_stock_area_chacked', $area->is_area_stock, array('id' => 'is_stock_area_chacked')) !!}
        <div class="text-right">
            <button type="submit" class="btn btn-primary col-xs-12  legitRipple">Save <i class="icon-floppy-disk position-right"></i></button>
        </div>
        
    </div>
    {!! Form::close() !!}
</div>
@endsection

@section('page-js')
<script src="{{ mix('js/switch.js') }}"></script>
<script src="{{ mix('js/area.js') }}"></script>
@endsection
