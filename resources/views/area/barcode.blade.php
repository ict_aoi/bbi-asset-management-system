<!DOCTYPE html>
    <html>
    <head>
        <title>Barcode :: Area</title>
        <link rel="stylesheet" type="text/css" href="{{ asset(elixir('css/barcode_asset.css')) }}">
    </head>
    <body>
        @foreach($areas  as $key => $area)
            <div class="outer">
                <span class="text" >
                    <span style="font-size: 10px">{{ $area->factory->name }}</span>
                </span><br/>
                <span class="text" >
                    <span style="font-size: 10px">{{ $area->name }}</span>
                </span>
                <span style="padding-left: 10px">
                    <img style="width: 5.5cm;margin-top:5px" src="data:image/png;base64,{{ DNS1D::getBarcodePNG("$area->code", 'C128') }}" alt="barcode"   />
                </span>
                <span style="padding-left: 15px">
                <span style="font-size: 10px">{{ $area->code }}</span>
                </span>
            </div>
        @endforeach
    </body>
</html>