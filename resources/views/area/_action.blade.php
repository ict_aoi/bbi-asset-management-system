
<ul class="icons-list">
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-menu9"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-right">
            <li><a href="{!! $edit !!}"><i class="icon-pencil6"></i> Edit</a></li>
            <li><a href="{!! $barcode !!}" target="_blank"><i class="icon-barcode2"></i> Barcode</a></li>
            <li><a href="#" onclick="hapus('{!! $delete !!}')"><i class="icon-trash"></i> Delete</a></li>
        </ul>
    </li>
</ul>
