<div id="confirmationModal" data-backdrop="static" data-keyboard="false" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
				{!!
					Form::open([
						'role' 		=> 'form',
						'url' 		=> route('area.exportFormImport'),
						'method' 	=> 'get',
						'class' 	=> 'form-horizontal',
					])
				!!}
				<div class="modal-body">
					@include('form.select', [
						'field' 		=> 'factory',
						'label' 		=> 'Factory',
						'default' 		=> $factory_id,
						'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
						'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
						'div_class' 	=> (auth::user()->is_super_admin)? '':'hidden',
						'options' 		=> [
							'' 			=> '-- Select Factory --',
						]+$factories,
						'class' 		=> 'select-search',
						'attributes' 	=> [
							'id' 		=> 'select_factory'
						]
					])
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Get form upload</button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
