@extends(($dashboard_eis ?  'layouts.dashboard' : 'layouts.app'),['active' => 'dashboard'])


@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Dashboard</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li class="active">Dashboard</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
    <div class="row">
        <div id ="draw_summary"></div>
    </div>
    </br/>
    <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">Total Asset BBI</h5>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a href="{{ ($dashboard_eis ? route('dashboardEis',['detail' => 'bbis'])  : route('home',['detail' => 'bbis']) )}}"><i class="icon-search4"></i></a></li>
                    </ul>
                </div>
            </div>
            

            <div class="panel-body">
                <div class="row">
                    <div class="chart-container">
                        <div class="chart has-fixed-height" id="connect_pie_bbi"></div>
                    </div>
                </div>
            </div>
        </div>
   
    {!! Form::hidden('page', 'home', ['id' => 'page']) !!}
    {!! Form::hidden('url_data_summary', route('home.dashboardDataSummary'), ['id' => 'url_data_summary']) !!}
    {!! Form::hidden('url_data_pie', route('home.dashboardDataPie'), ['id' => 'url_data_pie']) !!}
    {!! Form::hidden('summary_data', '[]', ['id' => 'summary_data']) !!}
    {!! Form::hidden('asset_status_legend_bbi', '[]', ['id' => 'asset_status_legend_bbi']) !!}
    {!! Form::hidden('total_asset_bbi','[]', ['id' => 'total_asset_bbi']) !!}
@endsection


@section('page-js')
    @include('_summary_dashboard')
    <script src="{{ mix('js/echarts.js') }}"></script>
    <script src="{{ mix('js/dashboard.js') }}"></script>
@endsection