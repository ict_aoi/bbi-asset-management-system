<script type="x-tmpl-mustache" id="asset_in_table">
	<input type="text" id="barcode_value" name="barcode_value" class="barcode_value form-control input-new" placeholder="Please scan your barcode here"></input>
	<div class="tabbable nav-tabs-vertical nav-tabs-left tab-content-bordered">
		<ul class="nav nav-tabs nav-tabs-highlight" style="width: 150px;">
			<li class="dropdown">
				<a href="#" class="dropdown-toggle legitRipple" data-toggle="dropdown" aria-expanded="false"><span class="badge badge-success pull-right" id="total_barcode"></span> Barcodes</a>
				<ul class="dropdown-menu scrollable-menu">
					{% #item %}
						<li class="{% _active %} btn-active" data-barcode="{% barcode %}"><a href="#{% barcode %}" data-toggle="tab" {% #is_error %} style="background-color: #fab1b1;" {% /is_error %} >{% barcode %}</a></li>
					{%/item%}
				</ul>
			</li>
		</ul>

		<div class="tab-content" style="padding-left: unset">
			{% #item %}
				<div class="tab-pane {% _active %}" id="{% barcode %}">
					<div class="panel border-left-lg border-left-white">
						<div class="panel-body">
							<ul class="list list-unstyled">
								<li>
									<div class="row">
										<label class="col-lg-3">Barcode</label>
										<div class="col-lg-9">
											<input type="text" class="form-control" readonly="readonly" value="{% barcode %}" >
										</div>
									</div>
								</li>
								<li>
									<div class="row">
										<label class="col-lg-3">No Inventory</label>
										<div class="col-lg-9">
											<input type="text" class="form-control" readonly="readonly" value="{% no_inventory %}" >
										</div>
									</div>
								</li>
								<li>
									<div class="row">
										<label class="col-lg-3">Erp No Asset</label>
										<div class="col-lg-9">
											<input type="text" class="form-control" readonly="readonly" value="{% erp_no_asset %}" >
										</div>
									</div>
								</li>
								<li>
									<div class="row">
										<label class="col-lg-3">Asset Condition</label>
										<div class="col-lg-9">
											<select class="form-control status_item" id="status_option_{% _id %}" data-id="{% _id %}">
												{% #status_options %}
													<option value="{% id %}" {% selected %}>{% name %}</option>
												{% /status_options %}
											</select>
										</div>
									</div>
								</li>
							</ul>
						</div>
					</div>
					<button type="button" id="show_detail_{% _id %}" data-id="{% _id %}" class="btn col-xs-12 btn-default btn-detail-item"><i class="fa icon-search4"></i></button>
					<button id="delete_{% _id %}" data-id="{% _id %}" class="btn btn-danger btn-lg col-xs-12 btn-delete-item">Remove <i class="fa icon-trash position-left"></i></button>
				</div>
			{%/item%}
		</div>
	</div>
	
</script>