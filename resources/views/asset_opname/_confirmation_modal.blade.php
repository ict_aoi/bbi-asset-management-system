<div id="confirmationInsertModal" data-backdrop="static" data-keyboard="false" class="modal fade">
	<div class="modal-dialog modal-lg ui-front">
		<div class="modal-content">
				{!!
					Form::open([
						'role' 		=> 'form',
						'url' 		=> route('assetOpname.store'),
						'method' 	=> 'store',
						'class' 	=> 'form-horizontal',
						'id'		=>	'form'
					])
				!!}
				<div class="modal-header bg-indigo">
					<h5 class="modal-title"><p>Confirmation Asset Opname</h5>
				</div>
				<div class="modal-body">
                    <div class="hidden">
                        @include('form.date', [
                            'field'             => 'checkin_date',
                            'label'             => 'Checkin Date',
                            'label_col'         => 'col-md-3 col-lg-3 col-sm-12',
                            'form_col'          => 'col-md-9 col-lg-9 col-sm-12',
                            'class'             => 'daterange-single',
                            'placeholder'       => 'dd/mm/yyyy',
                            'attributes'        => [
                                'id'            => 'checkin_date',
                                'readonly'      => 'readonly'
                            ]
                        ])
                    </div>
					
                    @include('form.select', [
                        'field'     => 'opname_area_location',
                        'label'     => 'Area',
                        'required'  => '*Required',
                        'label_col' => 'col-md-3 col-lg-3 col-sm-12',
                        'form_col'  => 'col-md-9 col-lg-9 col-sm-12',
                        'options'   => [
                            '' => '-- Select Area --',
                        ],
                        'class'      => 'select-search',
                        'attributes' => [
                            'id' => 'select_opname_area_location'
                        ]
                    ])
                    
                    {!! Form::hidden('area_name', '', array('id' => 'area_name')) !!}
					{!! Form::hidden('factory_id', '', array('id' => 'factory_id')) !!}
					{!! Form::hidden('department_id', '', array('id' => 'department_id')) !!}
					{!! Form::hidden('assets','[]' , array('id' => 'assets')) !!}
                    {!! Form::hidden('current_time',null, array('id' => 'current_time')) !!}
                    {!! Form::hidden('calendar_id',($calendar ? $calendar->id :null), array('id' => 'calendar_id')) !!}
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary legitRipple">Save <i class="icon-floppy-disk position-right"></i></button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
