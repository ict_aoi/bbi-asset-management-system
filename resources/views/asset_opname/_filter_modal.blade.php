<div id="filterModal" data-backdrop="static" data-keyboard="false" class="modal fade">
	<div class="modal-dialog modal-lg ui-front">
		<div class="modal-content">
				<div class="modal-header bg-indigo">
					<h5 class="modal-title"><p>Filters</h5>
				</div>
				<div class="modal-body">
                    @include('form.select', [
                        'field'         => 'factory',
                        'label'         => 'Factory',
                        'mandatory'     => '*Required',
                        'default'       => $factory_id,
                        'options'       => [
                            ''          => '-- Select Factory --',
                        ]+$factories,
                        'class'         => 'select-search',
                        'label_col'     => 'col-md-3 col-lg-3 col-sm-12',
                        'form_col'      => 'col-md-9 col-lg-9 col-sm-12',
                        'attributes'    => [
                            'id'        => 'select_factory'
                        ]
                    ])
				</div>
				
				<div class="modal-footer">
				    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
