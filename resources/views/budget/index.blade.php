@extends('layouts.app',['active' => 'budget'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Budget</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Dashboard</a></li>
            <li class="active">Budget</li>
        </ul>

        <ul class="breadcrumb-elements">
			<li class="dropdown">
				<a href="#" class="dropdown-toggle legitRipple breadcrumb-dropdown" data-toggle="dropdown">
					<i class="icon-three-bars position-left"></i>
					Actions
					<span class="caret"></span>
				</a>
				
				<ul class="dropdown-menu dropdown-menu-right">
					<!--li><a href="{{ route('area.import') }}"><i class="icon-drawer-out pull-right"></i> Import</a></li-->
					<li><a href="#" data-toggle="modal" data-target="#confirmationInsertModal"><i class="icon-plus2 pull-right"></i> Create</a></li>
				</ul>
			</li>
		</ul>
    </div>
</div>
@endsection

@section('page-content')
    <div class="panel panel-flat">
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-basic table-condensed" id="budgetTable">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Factory</th>
                            <th>Department</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Periode</th>
                            <th>Total Budget</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    {!! Form::hidden('page', 'index', array('id' => 'page')) !!}
    {!! Form::hidden('msg', $msg, array('id' => 'msg')) !!}
@endsection

@section('page-modal')
    @include('budget._confirmation_insert_modal')
@endsection


@section('page-js')
    <script src="{{ mix('js/budget.js') }}"></script>
@endsection
