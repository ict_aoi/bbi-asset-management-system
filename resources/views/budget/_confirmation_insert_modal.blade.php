<div id="confirmationInsertModal" data-backdrop="static" data-keyboard="false" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
				{!!
					Form::open([
						'role' 		=> 'form',
						'url' 		=> route('budget.store'),
						'method' 	=> 'post',
						'class' 	=> 'form-horizontal',
						'id' 		=> 'form',
					])
				!!}
				<div class="modal-header bg-indigo">
					<h5 class="modal-title"><p>Create Budget</h5>
				</div>
				<div class="modal-body">
					@include('form.select', [
						'field' 		=> 'factory',
						'label' 		=> 'Factory',
						'default' 		=> auth::user()->factory_id,
						'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
						'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
						'div_class' 	=> (auth::user()->is_super_admin)? '':'hidden',
						'options' 		=> [
							'' 			=> '-- Select Factory --',
						]+$factories,
						'class' 		=> 'select-search',
						'attributes' 	=> [
							'id' 		=> 'select_create_factory'
						]
					])

					@include('form.select', [
						'field' 		=> 'sub_department',
						'label' 		=> 'Subdepartment',
						'default' 		=> auth::user()->sub_department_id,
						'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
						'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
						'div_class' 	=> (auth::user()->is_super_admin)? '':'hidden',
						'options' 		=> [
							'' 			=> '-- Select Subdepartment --',
						]+$sub_departments,
						'class' 		=> 'select-search',
						'attributes' 	=> [
							'id' 		=> 'select_create_sub_department'
						]
					])

					@include('form.text', [
						'field'         => 'name',
						'label'         => 'Name',
						'mandatory'     => '*Required',
						'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
						'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
						'attributes'    => [
							'id'        => 'name'
						]
					])

					@include('form.select', [
						'field' 		=> 'year',
						'label' 		=> 'Periode of Year',
						'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
						'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
						'options' 		=> [
							'' 			=> '-- Select Year --',
						]+$years,
						'class' 		=> 'select-search',
						'attributes' 	=> [
							'id' 		=> 'select_create_year'
						]
					])

					@include('form.textarea', [
						'field'         => 'description',
						'label'         => 'Description',
						'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
						'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
						'attributes'    => [
							'id'        => 'description',
							'rows'      => 5,
							'style'     => 'resize: none;'
						]
					])
				</div>
				
				{!! Form::hidden('unique_id',null, array('id' => 'unique_id')) !!}
				{!! Form::hidden('current_time',null, array('id' => 'current_time')) !!}
				<div class="modal-footer">
					<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary legitRipple">Save <i class="icon-floppy-disk position-right"></i></button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
