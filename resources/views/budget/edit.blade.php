@extends('layouts.app',['active' => 'budget'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Budget</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Dashboard</a></li>
            <li><a href="{{ route('budget.index') }}">Budget</a></li>
            <li class="active">Edit</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
    <div class="panel panel-flat">
        {!! 
            Form::open([
                'role'      => 'form',
                'url'       => route('budget.update',$budget->id),
                'method'    => 'post',
                'class'     => 'form-horizontal',
                'id'        => 'form_edit'
            ])
        !!}
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-8 col-sm-6"> <p style="text-align: center;">Budget Name <b>{{ $budget->name }}</b></p></div>
                <div class="col-xs-4 col-sm-6"> <p style="text-align: center;">Year Periode <b>{{ $budget->periode_year }}</b></p></div>
            </div>
            
            <legend class="text-bold"></legend>
            <div class="row">
                <div class="table-responsive">
                    <table class="table table-basic table-condensed" id="budgetTable">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Month of Periode</th>
                                <th>Budget of Periode</th>
                            </tr>
                        </thead>
                        <tbody id="tbody_budget">
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="text-right">
                <button type="submit" class="btn btn-primary legitRipple col-xs-12">Save <i class="icon-floppy-disk position-right"></i></button>
            </div>
            
        </div>

        {!! Form::hidden('budgets', $detail_budgets, array('id' => 'budgets')) !!}
        {!! Form::hidden('current_time',null, array('id' => 'current_time')) !!}
        {!! Form::close() !!}
    </div>

    {!! Form::hidden('page', 'edit', array('id' => 'page')) !!}
@endsection

@section('page-js')
    @include('budget._item')
    <script src="{{ mix('js/budget.js') }}"></script>
@endsection
