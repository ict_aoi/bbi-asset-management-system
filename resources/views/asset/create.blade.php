@extends('layouts.app',['active' => 'asset'])

@section('page-css')

@endsection

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Asset</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Dashboard</a></li>
            <li ><a href="{{ route('asset.index') }}">Asset</a></li>
            <li class="active">Create</li>
        </ul>
    </div>
</div>
@endsection
@section('page-content')
<div class="panel panel-flat">
    <div class="panel-body">
        {!!
            Form::open([
                'role'      => 'form',
                'url'       => route('asset.store'),
                'method'    => 'post',
                'class'     => 'form-horizontal',
                'enctype'   => 'multipart/form-data',
                'id'        => 'form'
            ])
        !!}
        <div class="row">
            <div class="col-md-6">
                @include('form.select', [
                    'field'         => 'factory',
                    'label'         => 'Factory',
                    'mandatory'     => '*Required',
                    'default'       => $factory_id,
                    'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                    'div_class'     => (auth::user()->is_super_admin)? '':'hidden',
                    'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                    'options'       => [
                        ''          => '-- Select Factory --',
                    ]+$factories,
                    'class'         => 'select-search',
                    'attributes'    => [
                        'id'        => 'select_factory'
                    ]
                ])

                @include('form.select', [
                    'field'         => 'sub_department',
                    'label'         => 'Sub Departement',
                    'default'       => $sub_department_id,
                    'mandatory'     => '*Required',
                    'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                    'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                    'div_class'     => (auth::user()->is_super_admin)? '':'hidden',
                    'options'       => [
                        ''          => '-- Select Sub Departement --',
                    ]+$sub_departments,
                    'class'         => 'select-search',
                    'attributes'    => [
                        'id'        => 'select_sub_department'
                    ]
                ])

                @include('form.select', [
                    'field'         => 'no_rent',
                    'label'         => 'Rent Agreement (No KK Rent)',
                    'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                    'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                    'options'       => [
                        ''          => '-- Select Rent Agreement --',
                    ],
                    'class'         => 'select-search',
                    'attributes'    => [
                        'id'        => 'select_rent_agreement'
                    ]
                ])

                @include('form.date', [
                    'field'             => 'receiving_date',
                    'label'             => 'Receiving Date',
                    'label_col'         => 'col-md-2 col-lg-2 col-sm-12',
                    'form_col'          => 'col-md-10 col-lg-10 col-sm-12',
                    'class'             => 'daterange-single',
                    'placeholder'       => 'dd/mm/yyyy',
                    'attributes'        => [
                        'id'            => 'receiving_date',
                        'readonly'      => 'readonly'
                    ]
                ])

                @include('form.select', [
                    'field'         => 'asset_type',
                    'label'         => 'Asset Type',
                    'mandatory'     => '*Required',
                    'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                    'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                    'options'       => [
                        ''          => '-- Select Asset Type --',
                    ]+$asset_types,
                    'class'         => 'select-search',
                    'attributes'    => [
                        'id'        => 'select_asset_type'
                    ]
                ])

                @include('form.text', [
                    'field'         => 'no_inventory_manual',
                    'label'         => 'No. Inventory Manual',
                    'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                    'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                    'attributes'    => [
                        'id'        => 'no_inventory_manual'
                    ]
                ])

                @include('form.picklist', [
                    'field'         => 'erp_no_asset',
                    'label'         => 'Erp No. Aset',
                    'name'          => 'erp_no_asset',
                    'delete'        => true,
                    'readonly'      => 'readonly',
                    'placeholder'   => 'Select No Asset ERP here',
                    'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                    'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                    'attributes'    => [
                        'id'        => 'erp_no_asset',
                    ]
                ])

                @include('form.text', [
                    'field'         => 'supplier_name',
                    'label'         => 'Suplier Name',
                    'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                    'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                    'attributes'    => [
                        'id'        => 'supplier_name',
                        'readonly'  => 'readonly'
                    ]
                ])

                @include('form.text', [
                    'field'         => 'item_code',
                    'label'         => 'Item Code',
                    'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                    'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                    'attributes'    => [
                        'id'        => 'item_code',
                        'readonly'  => 'readonly'
                    ]
                ])

                @include('form.text', [
                    'field'         => 'purchase_number',
                    'label'         => 'Purchase Number',
                    'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                    'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                    'attributes'    => [
                        'id'        => 'purchase_number',
                        'readonly'  => 'readonly'
                    ]
                ])

                @include('form.text', [
                    'field'         => 'price',
                    'label'         => 'Price',
                    'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                    'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                    'attributes'    => [
                        'id'        => 'price',
                    ]
                ])
                
                @include('form.select', [
                    'field'         => 'erp_asset_group',
                    'label'         => 'Erp Asset Group',
                    'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                    'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                    'options'       => [
                        ''          => '-- Select Asset Group --',
                    ]+$asset_groups,
                    'class'         => 'select-search',
                    'attributes'    => [
                        'id'        => 'select_erp_asset_group'
                    ]
                ])

                @include('form.textarea', [
                    'field'         => 'item_desc',
                    'label'         => 'Asset Description',
                    'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                    'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                    'attributes'    => [
                        'id'        => 'item_desc',
                        'style'     => 'resize:none',
                        'rows'      => '5', 
                        'cols'      => '10'
                    ]
                ])
                
            </div>

            <div class="col-md-6">
                @include('form.select', [
                    'field'         => 'manufacture',
                    'label'         => 'Manufacture',
                    'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                    'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                    'options'       => [
                        ''          => '-- Select Manufacture --',
                    ]+$manufactures,
                    'class'         => 'select-search',
                    'attributes'    => [
                        'id'        => 'select_manufacture'
                    ]
                ])

                @include('form.text', [
                    'field'         => 'model',
                    'label'         => 'Model',
                    'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                    'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                    'mandatory'     => '*Required',
                    'attributes'    => [
                        'id'        => 'model'
                    ]
                ])

                @include('form.text', [
                    'field'         => 'serial_number',
                    'label'         => 'Serial Number',
                    'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                    'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                    'mandatory'     => '*Required',
                    'attributes'    => [
                        'id'        => 'serial_number'
                    ]
                ])

                <div id="draw"></div>
                
                @include('form.file', [
                    'field'         => 'photo',
                    'label'         => 'Upload Image',
                    'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                    'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                    'help'          => 'Accepted formats: gif, png, jpg. Max file size 2Mb',
                    'attributes'    => [
                        'id'        => 'photo',
                        'accept'    => 'image/*'
                    ]
                ])

                
            </div>
        </div>
        
        {!! Form::hidden('form_status', 'create', array('id' => 'form_status')) !!}
        {!! Form::hidden('item_id', null, array('id' => 'item_id')) !!}
        {!! Form::hidden('asset_id', '', array('id' => 'asset_id')) !!}
        {!! Form::hidden('asset_group_id', null, array('id' => 'asset_group_id')) !!}
        {!! Form::hidden('asset_group_name', null, array('id' => 'asset_group_name')) !!}
        {!! Form::hidden('no_asset', null, array('id' => 'no_asset')) !!}
        {!! Form::hidden('manufacture_id', null, array('id' => 'manufacture_id')) !!}
        {!! Form::hidden('asset_type_id', null, array('id' => 'asset_type_id')) !!}
        {!! Form::hidden('custom_fields', '[]', array('id' => 'custom_fields')) !!}
        {!! Form::hidden('factory_id', $factory_id, array('id' => 'factory_id')) !!}
        {!! Form::hidden('department_id', $department_id, array('id' => 'department_id')) !!}
        {!! Form::hidden('sub_department_id', $sub_department_id, array('id' => 'sub_department_id')) !!}
        {!! Form::hidden('rent_agreement_id', '', array('id' => 'rent_agreement_id')) !!}
        {!! Form::hidden('current_time', '', array('id' => 'current_time')) !!}
            <div class="text-right">
                <button type="submit" class="btn btn-primary legitRipple col-xs-12">Save <i class="icon-floppy-disk position-right"></i></button>
            </div>
        {!! Form::close() !!}


        
    </div>
</div>

{!!
    Form::open([
        'role'      => 'form',
        'url'       => route('asset.printBarcode'),
        'method'    => 'post',
        'class'     => 'form-horizontal',
        'id'        => 'printBarcode',
        'target'    => '_blank'
    ])
!!}
    {!! Form::hidden('list_barcodes','[]' , array('id' => 'list_barcodes')) !!}
    <div class="form-group text-right" style="margin-top: 10px;">
        <button type="submit" class="btn hidden">Print <i class="icon-printer position-left"></i></button>
    </div>
{!! Form::close() !!}
@endsection

@section('page-modal')
    @include('asset._erp_asset_modal')
@endsection

@section('page-js')
    @include('asset._fields')
    <script src="{{ mix('js/datepicker.js') }}"></script>
    <script src="{{ mix('js/asset.js') }}"></script>
@endsection
