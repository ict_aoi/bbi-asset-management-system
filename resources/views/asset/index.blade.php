@extends('layouts.app',['active' => 'asset'])

@section('page-css')
    
@endsection

@section('page-header')
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Asset</span></h4>
            </div>
        </div>
        <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
            <ul class="breadcrumb">
                <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Dashboard</a></li>
                <li class="active">Asset</li>
            </ul>

            <ul class="breadcrumb-elements">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle legitRipple breadcrumb-dropdown" data-toggle="dropdown">
                        <i class="icon-three-bars position-left"></i>
                        Actions
                        <span class="caret"></span>
                    </a>
                    
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="{{ route('asset.create') }}"><i class="icon-plus2 pull-right"></i> Create</a></li>
                        <li><a href="{{ route('asset.import') }}"><i class="icon-drawer-out pull-right"></i> Import</a></li>
                        <li><a href="#" data-toggle="modal" data-target="#confirmationModal"><i class="icon-barcode2 pull-right"></i> Print Barcode</a></li>
                        @if(auth::user()->is_super_admin)
                            <li><a href="#" data-toggle="modal" data-target="#exportModal"><i class="icon-file-excel pull-right"></i> Export</a></li>
                        @else
                            <li><a href="#" id="export_report_button"><i class="icon-file-excel pull-right"></i> Export</a></li>
                           
                            {!!
                                Form::open([
                                    'url' 		=> route('asset.exportReport'),
                                    'method' 	=> 'get',
                                    'class'     => 'hidden',
                                    'id'        => 'download_export_report_excel',
                                ])
                            !!}
                                {!! Form::hidden('department', auth::user()->sub_department_id, array('id' => 'department_id')) !!}
                            {!! Form::close() !!}
                        @endif
                    </ul>
                </li>
            </ul>
        </div>
    </div>
@endsection

@section('page-content')
    <div class="panel panel-flat panel-collapsed">
        <div class="panel-heading">
            <h5 class="panel-title">Filters   <a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
                <div class="col-md-4 col-lg-4 col-sm-12">
                    @include('form.select', [
                        'field'         => 'company',
                        'label'         => 'Company',
                        'default'       => $company_id,
                        'label_col'     => 'col-md-12 col-lg-12 col-sm-12',
                        'form_col'      => 'col-md-12 col-lg-12 col-sm-12',
                        'div_class'     => (auth::user()->is_super_admin)? '':'hidden',
                        'options' => [
                            ''          => '-- Select Company --',
                        ]+$companies,
                        'class'         => 'select-search',
                        'attributes'    => [
                            'id'        => 'select_company'
                        ]
                    ])

                    @include('form.select', [
                        'field'         => 'asset_type',
                        'label'         => 'Asset Type',
                        'label_col'     => 'col-md-12 col-lg-12 col-sm-12',
                        'form_col'      => 'col-md-12 col-lg-12 col-sm-12',
                        'options'       => [
                            ''          => '-- Select Asset Type --',
                        ]+$asset_types,
                        'class'         => 'select-search',
                        'attributes'    => [
                            'id'        => 'select_asset_type'
                        ]
                    ])
                </div>
                <div class="col-md-4 col-lg-4 col-sm-12">
                    @include('form.select', [
                        'field'         => 'factory',
                        'label'         => 'Factory',
                        'default'       => $factory_id,
                        'label_col'     => 'col-md-12 col-lg-12 col-sm-12',
                        'div_class'     => (auth::user()->is_super_admin)? '':'hidden',
                        'form_col'      => 'col-md-12 col-lg-12 col-sm-12',
                        'options'       => [
                            ''          => '-- Select Factory --',
                        ]+$factories,
                        'class'         => 'select-search',
                        'attributes'    => [
                            'id'        => 'select_factory'
                        ]
                    ])

                    @include('form.select', [
                        'field'         => 'status',
                        'label'         => 'Status',
                        'label_col'     => 'col-md-12 col-lg-12 col-sm-12',
                        'form_col'      => 'col-md-12 col-lg-12 col-sm-12',
                        'options'       => [
                            ''          => '-- Select Asset Status --',
                        ]+$status,
                        'class'         => 'select-search',
                        'attributes'    => [
                            'id'        => 'select_status_asset'
                        ]
                    ])
                </div>
                <div class="col-md-4 col-lg-4 col-sm-12">
                    @include('form.select', [
                        'field'         => 'sub_department',
                        'label'         => 'Sub Departement',
                        'default'       => $sub_department_id,
                        'label_col'     => 'col-md-12 col-lg-12 col-sm-12',
                        'form_col'      => 'col-md-12 col-lg-12 col-sm-12',
                        'div_class'     => (auth::user()->is_super_admin)? '':'hidden',
                        'options'       => [
                            ''          => '-- Select Sub Departement --',
                        ]+$sub_departments,
                        'class'         => 'select-search',
                        'attributes'    => [
                            'id'        => 'select_sub_department'
                        ]
                    ])

                    @include('form.select', [
                        'field'         => 'is_rent',
                        'label'         => 'Is Rent',
                        'label_col'     => 'col-md-12 col-lg-12 col-sm-12',
                        'form_col'      => 'col-md-12 col-lg-12 col-sm-12',
                        'options'       => [
                            ''          => '-- Select Rent Status --',
                            '1'          => 'Rent',
                            '2'          => 'No Rent',
                        ],
                        'class'         => 'select-search',
                        'attributes'    => [
                            'id'        => 'select_is_rent'
                        ]
                    ])
                </div>
          </div>
    </div>

    <div class="panel panel-flat">
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-basic table-condensed" id="assetTable">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Id</th>
                            <th>last_company_location_id</th>
                            <th>last_factory_location_id</th>
                            <th>last_area_location</th>
                            <th>custom_field_1</th>
                            <th>custom_field_2</th>
                            <th>custom_field_3</th>
                            <th>custom_field_4</th>
                            <th>custom_field_5</th>
                            <th>custom_field_6</th>
                            <th>custom_field_7</th>
                            <th>custom_field_8</th>
                            <th>custom_field_9</th>
                            <th>custom_field_10</th>
                            <th>custom_field_11</th>
                            <th>custom_field_12</th>
                            <th>custom_field_13</th>
                            <th>custom_field_14</th>
                            <th>custom_field_15</th>
                            <th>custom_field_16</th>
                            <th>custom_field_17</th>
                            <th>custom_field_18</th>
                            <th>custom_field_19</th>
                            <th>custom_field_20</th>
                            <th>last_used_by</th>
                            <th>last_department_location_id</th>
                            <th>last_sub_department_location_id</th>
                            @if(auth::user()->is_super_admin)
                                <th>Asset Factory</th>
                                <th>Asset Departement</th>
                                <th>Asset Sub Departement</th>
                            @endif
                            <th>No Inventori</th>
                            <th>No Inventori Manual</th>
                            <th>Erp No Asset (Accounting)</th>
                            <th>Asset Type</th>
                            <th>Model</th>
                            <th>Nomor Serial</th>
                            <th>Status</th>
                            <th>No KK Rent</th>
                            <th>Last Company Location</th>
                            <th>Last Factory Location</th>
                            <th>Last Department Location</th>
                            <th>Last Subdepartment Location</th>
                            <th>Last Used By</th>
                            <th>Last Area Location</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    {!! Form::hidden('form_status', 'index', array('id' => 'form_status')) !!}
    {!! Form::hidden('is_super_admin', (auth::user()->is_super_admin)? '1':'0', array('id' => 'is_super_admin')) !!}
    {!! Form::hidden('msg', $msg, array('id' => 'msg')) !!}
    {!! Form::hidden('custom_fields', '[]', array('id' => 'custom_fields')) !!}
@endsection

@section('page-modal')
    @include('asset._confirmation_barcode_modal')
    @include('asset._export_modal')
@endsection

@section('page-js')
    <script src="{{ mix('js/datepicker.js') }}"></script>
    <script src="{{ mix('js/asset.js') }}"></script>
@endsection
