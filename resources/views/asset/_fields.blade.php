<script type="x-tmpl-mustache" id="fields">
	{% #item %}
		{% #is_text %}
			<div class="form-group col-lg-12">
				<label for="{%label%}" class="text-semibold control-label col-md-2 col-lg-2 col-sm-12">
					{%label%}
				</label>
				<div class="control-input col-md-10 col-lg-10 col-sm-12">
					<input class="form-control custom-text" placeholder="" id="{%_id%}" type="text" data-id="{%_id%}" value="{%value%}">
					{% #is_required %}
						<span class="help-block text-danger">*Required</span>
					{% /is_required %}
				</div>
			</div>
		{% /is_text %}

		{% #is_option %}
			<div class="form-group col-lg-12">
				<label for="{%label%}" class="control-label col-md-2 col-lg-2 col-sm-12 text-semibold">
				{% label %}
				</label>
				<div class="col-md-10 col-lg-10 col-sm-12">
					<select id="{%_id%}" class="select-search-2 custom-select" data-id="{%_id%}">
						<option value="">-- Select {%label%} --</option>
						{% #default_value %}
							<option value="{% id %}" {% #selected %}selected{% /selected %}>{% name %}</option>
						{% /default_value %}
					</select>
					{% #is_required %}
						<span class="help-block text-danger">*Required</span>
					{% /is_required %}
				</div>
				
			</div>
		{% /is_option %}

		{% #is_number %}
			<div class="form-group col-lg-12">
				<label for="{%label%}" class="text-semibold control-label col-md-2 col-lg-2 col-sm-12">
					{%label%}
				</label>
				<div class="control-input col-md-10 col-lg-10 col-sm-12">
					<input class="form-control custom-text input-number" placeholder="" id="{%_id%}" type="text" data-id="{%_id%}" value="{%value%}">
					{% #is_required %}
						<span class="help-block text-danger">*Required</span>
					{% /is_required %}
				</div>
			</div>
		{% /is_number %}
		
		{% #is_lookup %}
			<div class="form-group col-lg-12" id="div_lookup_%_id%}" data-id="{%_id%}">
				<label class="control-label text-semibold col-md-3 col-lg-3 col-sm-12">{%label%}</label>

				<div id="lookup_error_{%_id%}" data-id="{%_id%}" class="input-group  col-md-9 col-lg-9 col-sm-12">
					
					<input id="lookup_name_{%_id%}" data-id="{%_id%}" class="form-control lookup-value custom-lookup-name" readonly="readonly" placeholder="" autocomplete="off" type="text" data-id="{%_id%}" value="{%value%}">
					
						<span class="input-group-btn">
						<button class="btn btn-default legitRipple lookup-value" type="button" id="lookupButtonLookup_{%_id%}" data-id="{%_id%}">
							<span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
						</button>
					</span>
				</div>
				
				<input id="lookup_id_{%_id%}"  value="{%lookup_id%}" class="custom-lookup" data-id="{%_id%}" type="hidden">
				<input id="lookup_detail_id_{%_id%}" value="{%detail_lookup_id%}" class="custom-lookup-detail"  data-id="{%_id%}" type="hidden">
				<input id="custom_lookup_table_id_{%_id%}" data-id="{%_id%}" value="{%custom_lookup_id%}" type="hidden">
			</div>

			<div id="lookup_modal_{%_id%}" data-id="{%_id%}" class="modal fade" style="color:black !important">
				<div class="modal-dialog modal-full">
					<div class="modal-content">
						<div class="modal-header bg-primary">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h5 class="modal-title">Lookup {%label%}</h5>
						</div>

						<div class="modal-body">
							<fieldset>
								<div class="form-group">
									<div class="col-xs-12">
										<div class="input-group">
											<input type="text" id="lookup_search_{%_id%}" id="{%_id%}" class="form-control custom-text" placeholder="Masukan kata kunci..." autofocus="true">
											<span class="input-group-btn">
												<button class="btn bg-teal legitRipple" type="button" id="lookup_button_src_{%_id%}" data-id="{%_id%}"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
											</span>
										</div>
											
									</div>
								</div>
							</fieldset>
							<br/>
							<div class="shade-screen" style="text-align: center;">
								<div class="shade-screen hidden">
									<img src="/images/ajax-loader.gif">
								</div>
							</div>
							<div class="table-responsive" id="lookup_table_{%_id%}" data-id="{%_id%}"></div>
						</div>

						<div class="modal-footer">
							<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
						</div>
					</div>
				</div>
			</div>

		{% /is_lookup %}

	{%/item%}
</script>