@extends('layouts.app',['active' => 'asset'])

@section('page-css')

@endsection

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Asset</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li ><a href="{{ route('asset.index') }}">Asset</a></li>
            <li class="active">History</li>
        </ul>
    </div>
</div>
@endsection
@section('page-content')
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="row">
            <div class="col-md-6">
                <p>Barcode : <b> {{$asset->barcode}}</b></p>
                <p>No. Inventory : <b> {{$asset->no_inventory}}</b></p>
                <p>No. Asset (accounting) :  <b>{{$asset->no_asset}}</b></p>
                <p>Asset Company :  <b>{{$asset->originCompany->name}}</b></p>
                <p>Asset Factory :  <b>{{$asset->originFactory->name}}</b></p>
                <p>Asset Department :  <b>{{$asset->originDepartment->name}}</b></p>
                <p>Asset Sub Department :  <b>{{$asset->originSubDepartment->name}}</b></p>
                <p>Model :  <b>{{$asset->model}}</b></p>
                <p>No. Serial : <b>{{$asset->serial_number}}</b></p> 
            </div>
            <div class="col-md-6">
                <p>Last Status:  <b>{{ ucwords($asset->status) }}</b></p>
                <p>Last Movement Date:  <b>{{ ($asset->last_movement_date ? $asset->last_movement_date->format('d/m/Y H:i:s') : $asset->created_at->format('d/m/Y H:i:s')) }}</b></p>
                <p>Last Company Location:  <b>{{$asset->lastCompany->name}}</b></p>
                <p>Last Factory Location:  <b>{{$asset->lastFactory->name}}</b></p>
                <p>Last Department Location :  <b>@if($asset->last_department_location_id) {{ ucwords($asset->lastDepartment->name) }} @else {{ ucwords($asset->last_department_location) }} @endif</b></p>
                <p>Last Sub Departement Location :  <b>@if($asset->last_sub_department_location_id) {{ ucwords($asset->lastSubDepartment->name) }} @else - @endif</b></p>
                <p>Last Area Location :  <b>@if($asset->last_area_location){{ ucwords($asset->last_area_location) }}  @else - @endif</b></p>
                <p>Last User Movement :  <b>@if($asset->last_used_by){{ ucwords($asset->last_used_by) }}  @else - @endif</b></p>
                <p>Last No Agreemeent :  <b>@if($asset->no_rent){{ strtoupper($asset->rentalAgreement->no_agreement) }}  @else - @endif</b></p>
                <p>Last No BC :  <b>@if($asset->no_rent){{ strtoupper($asset->rentalAgreement->no_bc) }}  @else - @endif</b></p>
            </div>
        </div>
    </div>
</div>
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="historyTable">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Movement Date</th>
                        <th>Status</th>
                        <th>From</th>
                        <th>To</th>
                        <th>No KK</th>
                        <th>No Bea Cukai</th>
                        <th>Note</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
{!! Form::hidden('asset_id', $asset->id, array('id' => 'asset_id')) !!}
@endsection

@section('page-js')
<script>
    $(document).ready( function ()
    {
        var asset_id = $('#asset_id').val();
        var historyTable = $('#historyTable').DataTable({
            dom: 'Bfrtip',
            processing: true,
            serverSide: true,
            pageLength:100,
            deferRender:true,
            ajax: {
                type: 'GET',
                url: '/asset/history/'+asset_id+'/data',
            },
            fnCreatedRow: function (row, data, index) {
                var info = historyTable.page.info();
                var value = index+1+info.start;
                $('td', row).eq(0).html(value);
            },
            columns: [
                {data: null, sortable: false, orderable: false, searchable: false},
                {data: 'movement_date', name: 'movement_date',searchable:true,visible:true,orderable:false},
                {data: 'status', name: 'status',searchable:true,visible:true,orderable:false},
                {data: 'from', name: 'from',searchable:false,visible:true,orderable:false},
                {data: 'to', name: 'to',searchable:false,visible:true,orderable:false},
                {data: 'no_kk', name: 'no_kk',searchable:true,visible:true,orderable:false},
                {data: 'no_bea_cukai', name: 'no_bea_cukai',searchable:true,visible:true,orderable:false},
                {data: 'note', name: 'note',searchable:true,visible:true,orderable:false},
                
            ]
        });

        var dtable = $('#historyTable').dataTable().api();
        $("#historyTable.dataTables_filter input")
            .unbind() // Unbind previous default bindings
            .bind("keyup", function (e) { // Bind our desired behavior
                // If the user pressed ENTER, search
                if (e.keyCode == 13) {
                    // Call the API search function
                    dtable.search(this.value).draw();
                }
                // Ensure we clear the search if they backspace far enough
                if (this.value == "") {
                    dtable.search("").draw();
                }
                return;
        });
        dtable.draw();
    });   
</script>
@endsection

