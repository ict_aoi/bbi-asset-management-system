<script type="x-tmpl-mustache" id="upload_asset">
	{% #item %}
		<tr {% #is_error %} style="background-color:#FF5722;color:#fff" {% /is_error %}>
			<td>{% no %}</td>
			<td>
				{% erp_asset_id %}
			</td>
			<td>
				{% erp_asset_group_name %}
			</td>
			<td>
				{% asset_type_name %}
			</td>
			<td>
				{% manufacture_name %}
			</td>
			<td>
				{% model %}
			</td>
			<td>
				{% serial_number %}
			</td>
			<td>
				{% no_asset %}
			</td>
			<td>
				{% no_inventory %}
			</td>
			<td>
				{% no_inventory_manual %}
			</td>
			<td>
				{% purchase_number %}
			</td>
			<td>
				{% supplier_name %}
			</td>
			<td>
				{% system_log %}
			</td>
		</tr>
	{%/item%}
</script>