<table class="table">
	<thead>
	  <tr>
		<th>Model</th>
		<th>Serial number</th>
		<th>Detail value</th>
		<th>Action</th>
	  </tr>
	</thead>

	<tbody>
		@foreach ($lists as $key => $list)
			<tr>
				<td>
					{{ strtoupper($list->consumable_asset_model) }}
				</td>
				<td>
					{{ strtoupper($list->consumable_asset_serial_number) }}
				</td>
				<td>
					{{ strtoupper($list->consumable_asset_detail_value) }}
				</td>
				<td>
					@php
						$name = $list->consumable_asset_model;
						if($list->consumable_asset_serial_number) $name .= ':'.$list->consumable_asset_serial_number;
						if($list->consumable_asset_detail_value) $name 	.= ':'.$list->consumable_asset_detail_value;
						
					@endphp
					<button data-dismiss="modal" class="btn btn-info btn-xs btn-choose"
						type="button" data-id="{{ $list->consumable_asset_id }}"
						data-detailId="{{ $list->consumable_asset_detail_id }}"
						data-name="{{ $name }}"
					>Select
					</button>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>

{!! $lists->appends(Request::except('page'))->render() !!}
