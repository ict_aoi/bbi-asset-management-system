@extends('layouts.app',['active' => 'asset'])

@section('page-header')
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Asset</span></h4>
            </div>
        </div>
        <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
            <ul class="breadcrumb">
                <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Dashboard</a></li>
                <li ><a href="{{ route('asset.index') }}">Asset</a></li>
                <li class="active">Edit</li>
            </ul>
        </div>
    </div>
@endsection

@section('page-content')
    <div class="panel panel-flat">
        <div class="panel-body">
            {!!
                Form::open([
                    'role'      => 'form',
                    'url'       => route('asset.update',$asset->id),
                    'method'    => 'post',
                    'class'     => 'form-horizontal',
                    'enctype'   => 'multipart/form-data',
                    'id'        => 'form'
                ])
            !!}
            <div class="row">
                <div class="col-md-6">
                    @include('form.select', [
                        'field'         => 'company',
                        'label'         => 'Company',
                        'default'       => $asset->origin_company_location_id,
                        'mandatory'     => '*Required',
                        'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                        'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                        'div_class'     => (auth::user()->is_super_admin)? '':'hidden',
                        'options'       => [
                            ''          => '-- Select Company --',
                        ]+$companies,
                        'class'         => 'select-search',
                        'attributes'    => [
                            'id'        => 'select_company',
                            'disabled'  => 'disabled'
                        ]
                    ])

                    @include('form.select', [
                        'field'         => 'factory',
                        'label'         => 'Factory',
                        'mandatory'     => '*Required',
                        'default'       => $asset->origin_factory_location_id,
                        'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                        'div_class'     => (auth::user()->is_super_admin)? '':'hidden',
                        'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                        'options'       => [
                            ''          => '-- Select Factory --',
                        ]+$factories,
                        'class'         => 'select-search',
                        'attributes'    => [
                            'id'        => 'select_factory',
                            'disabled'  => 'disabled'
                        ]
                    ])

                    @include('form.select', [
                        'field'         => 'sub_department',
                        'label'         => 'Sub Departement',
                        'default'       => $asset->origin_sub_department_location_id,
                        'mandatory'     => '*Required',
                        'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                        'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                        'div_class'     => (auth::user()->is_super_admin)? '':'hidden',
                        'options'       => [
                            ''          => '-- Select Sub Departemen --',
                        ]+$sub_departments,
                        'class'         => 'select-search',
                        'attributes'    => [
                            'id'        => 'select_sub_department',
                            'disabled'  => 'disabled'
                        ]
                    ])

                    @if($asset->no_rent)
                        @if($asset->rentalAgreement->destination_factory_id)
                            @include('form.text', [
                                'field'         => 'no_rent',
                                'label'         => 'Rent Agreement (No KK Rent)',
                                'default'       => $asset->rentalAgreement->no_agreement,
                                'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                                'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                                'attributes'    => [
                                    'readonly'  => 'readonly'
                                ]
                            ])
                        @else
                            @include('form.select', [
                                'field'         => 'no_rent',
                                'default'       => $asset->no_rent,
                                'label'         => 'Rent Agreement (No KK Rent)',
                                'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                                'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                                'options'       => [
                                    ''          => '-- Select Rent Agreement --',
                                ],
                                'class'         => 'select-search',
                                'attributes'    => [
                                    'id'        => 'select_rent_agreement'
                                ]
                            ])
                        @endif
                    @else 
                        @include('form.select', [
                            'field'         => 'no_rent',
                            'default'       => $asset->no_rent,
                            'label'         => 'Rent Agreement (No KK Rent)',
                            'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                            'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                            'options'       => [
                                ''          => '-- Select Rent Agreement --',
                            ],
                            'class'         => 'select-search',
                            'attributes'    => [
                                'id'        => 'select_rent_agreement'
                            ]
                        ])
                    @endif
                    

                    @include('form.date', [
                        'field'             => 'receiving_date',
                        'label'             => 'Receiving Date',
                        'label_col'         => 'col-md-2 col-lg-2 col-sm-12',
                        'form_col'          => 'col-md-10 col-lg-10 col-sm-12',
                        'default'			=> ($asset->receiving_date ? $asset->receiving_date->format('d/m/Y') : null),
                        'class'             => 'daterange-single',
                        'placeholder'       => 'dd/mm/yyyy',
                        'attributes'        => [
                            'id'            => 'receiving_date',
                            'readonly'      => 'readonly'
                        ]
                    ])

                    @include('form.select', [
                        'field'         => 'asset_type',
                        'label'         => 'Asset Type',
                        'mandatory'     => '*Required',
                        'default'       => $asset->asset_type_id,
                        'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                        'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                        'options'       => [
                            ''          => '-- Select Asset Type --',
                        ]+$asset_types,
                        'class'         => 'select-search',
                        'attributes'    => [
                            'id'        => 'select_asset_type',
                            'disabled'  => 'disabled'
                        ]
                    ])

                    @include('form.text', [
                        'field'         => 'no_inventory_manual',
                        'label'         => 'No. Inventory Manual',
                        'default'       => $asset->no_inventory_manual,
                        'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                        'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                        'attributes'    => [
                            'id'        => 'no_inventory_manual'
                        ]
                    ])
                
                    @if($asset->erp_no_asset)
                        @include('form.text', [
                            'field'         => 'erp_no_asset',
                            'default'       => $asset->erp_no_asset,
                            'label'         => 'Erp No. Aset',
                            'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                            'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                            'attributes'    => [
                                'id'        => 'erp_no_asset',
                                'readonly'  => 'readonly'
                            ]
                        ])

                        {!! Form::hidden('erp_no_asset_id', $asset->erp_no_asset_id, array('id' => 'erp_no_assetId')) !!}
                    @else
                        @include('form.picklist', [
                            'field'         => 'erp_no_asset',
                            'label'         => 'Erp No. Aset',
                            'name'          => 'erp_no_asset',
                            'delete'        => true,
                            'readonly'      => 'readonly',
                            'placeholder'   => 'Scan pilih no asset disini',
                            'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                            'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                            'attributes'    => [
                                'id'        => 'erp_no_asset',
                            ]
                        ])
                    @endif
                   

                    @include('form.text', [
                        'field'         => 'supplier_name',
                        'default'       => $asset->supplier_name,
                        'label'         => 'Suplier Name',
                        'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                        'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                        'attributes'    => [
                            'id'        => 'supplier_name',
                            'readonly'  => 'readonly'
                        ]
                    ])

                    @include('form.text', [
                        'field'         => 'item_code',
                        'label'         => 'Item Code',
                        'default'       => $asset->erp_item_code,
                        'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                        'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                        'attributes'    => [
                            'id'        => 'item_code',
                            'readonly'  => 'readonly'
                        ]
                    ])

                    @include('form.text', [
                        'field'         => 'purchase_number',
                        'default'       => $asset->purchase_number,
                        'label'         => 'Purchase Order',
                        'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                        'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                        'attributes'    => [
                            'id'        => 'purchase_number',
                            'readonly'  => 'readonly'
                        ]
                    ])

                    @include('form.text', [
                    'field'         => 'price',
                    'label'         => 'Price',
                    'default'       => $asset->price,
                    'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                    'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                    'attributes'    => [
                        'id'        => 'price',
                    ]
                ])

                    @include('form.select', [
                        'field'         => 'erp_asset_group',
                        'default'       => $asset->erp_asset_group_id,
                        'label'         => 'Erp Group Asset',
                        'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                        'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                        'options'       => [
                            ''          => '-- Select Erp Group Asset --',
                        ]+$asset_groups,
                        'class'         => 'select-search',
                        'attributes'    => [
                            'id'        => 'select_erp_asset_group'
                        ]
                    ])

                    @include('form.textarea', [
                        'field'         => 'item_desc',
                        'default'       => $asset->description,
                        'label'         => 'Asset Description',
                        'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                        'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                        'attributes'    => [
                            'id'        => 'item_desc',
                            'style'     => 'resize:none',
                            'rows'      => '5', 
                            'cols'      => '10'
                            
                        ]
                    ])
                    
                </div>

                <div class="col-md-6">
                    @include('form.select', [
                        'field'         => 'manufacture',
                        'label'         => 'Manufacture',
                        'default'       => $asset->manufacture_id,
                        'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                        'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                        'options'       => [
                            ''          => '-- Select Manufacture --',
                        ]+$manufactures,
                        'class'         => 'select-search',
                        'attributes'    => [
                            'id'        => 'select_manufacture'
                        ]
                    ])

                    @include('form.text', [
                        'field'         => 'model',
                        'label'         => 'Model',
                        'default'       => $asset->model,
                        'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                        'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                        'mandatory'     => '*Required',
                        'attributes'    => [
                            'id'        => 'model'
                        ]
                    ])

                    @include('form.text', [
                        'field'         => 'serial_number',
                        'label'         => 'Serial Number',
                        'default'       => $asset->serial_number,
                        'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                        'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                        'mandatory'     => '*Required',
                        'attributes'    => [
                            'id'        => 'serial_number'
                        ]
                    ])

                    <div id="draw"></div>
                    
                    @include('form.file', [
                        'field'         => 'photo',
                        'label'         => 'Upload Image',
                        'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                        'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                        'help'          => 'Accepted formats: gif, png, jpg. Max file size 2Mb',
                        'attributes'    => [
                            'id'        => 'photo',
                            'accept'    => 'image/*'
                        ]
                    ])

                    
                </div>
            </div>
            {!! Form::hidden('form_status', 'edit', array('id' => 'form_status')) !!}
            {!! Form::hidden('item_id', $asset->erp_item_id, array('id' => 'item_id')) !!}
            {!! Form::hidden('is_asset_group', $asset->id, array('id' => 'asset_id')) !!}
            {!! Form::hidden('asset_id', $asset->id, array('id' => 'asset_id')) !!}
            {!! Form::hidden('asset_group_id', $asset->erp_asset_group_id, array('id' => 'asset_group_id')) !!}
            {!! Form::hidden('asset_group_name', $asset->erp_asset_group_name, array('id' => 'asset_group_name')) !!}
            {!! Form::hidden('manufacture_id', $asset->manufacture_id, array('id' => 'manufacture_id')) !!}
            {!! Form::hidden('asset_type_id', $asset->asset_type_id, array('id' => 'asset_type_id')) !!}
            {!! Form::hidden('custom_fields', '[]', array('id' => 'custom_fields')) !!}
            {!! Form::hidden('factory_id', $asset->origin_factory_location_id, array('id' => 'factory_id')) !!}
            {!! Form::hidden('department_id', $asset->origin_department_location_id, array('id' => 'department_id')) !!}
            {!! Form::hidden('rent_agreement_id', $asset->no_rent, array('id' => 'rent_agreement_id')) !!}
            {!! Form::hidden('sub_department_id', $asset->origin_sub_department_location_id, array('id' => 'sub_department_id')) !!}
            {!! Form::hidden('current_time', '', array('id' => 'current_time')) !!}
                @if($asset->origin_factory_location_id != $asset->last_factory_location_id)
                    <div class="alert bg-danger alert-styled-right">
                        <span class="text-semibold">Oh snap!</span> Assets cannot be edited.
                    </div>
               @else
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary legitRipple col-xs-12">Save <i class="icon-floppy-disk position-right"></i></button>
                    </div>
                @endif
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('page-modal')
    @include('asset._erp_asset_modal')
@endsection

@section('page-js')
    @include('asset._fields')
    <script src="{{ mix('js/datepicker.js') }}"></script>
    <script src="{{ mix('js/asset.js') }}"></script>
@endsection
