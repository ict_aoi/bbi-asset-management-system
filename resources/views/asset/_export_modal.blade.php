<div id="exportModal" data-backdrop="static" data-keyboard="false" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
			</div>
				{!!
					Form::open([
						'role' 			=> 'form',
						'url' 			=> route('asset.exportReport'),
						'method' 		=> 'get',
						'class' 		=> 'form-horizontal',
					])
				!!}
				<div class="modal-body">
					@include('form.select', [
						'field' 		=> 'company',
						'label' 		=> 'Perusahaan',
						'default' 		=> $company_id,
						'label_col'		=> 'col-md-2 col-lg-2 col-sm-12',
						'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
						'options' 		=> [
							'' 			=> '-- Pilih Perusahaan --',
						]+$companies,
						'class' 		=> 'select-search',
						'attributes'	=> [
							'id' 		=> 'select_export_company'
						]
					])

					@include('form.select', [
						'field' 		=> 'department',
						'label' 		=> 'Departemen',
						'default' 		=> $sub_department_id,
						'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
						'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
						'options' 		=> [
							'' 			=> '-- Pilih Sub Departemen --',
						]+$sub_departments,
						'class'			=> 'select-search',
						'attributes' 	=> [
							'id' 		=> 'select_export_department'
						]
					])

				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Export</button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
