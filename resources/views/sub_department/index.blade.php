@extends('layouts.app',['active' => 'sub_department'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Sub Departement</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Dashboard</a></li>
            <li>Organization</li>
            <li class="active">Sub Departement</li>
        </ul>
    </div>
</div>
@endsection
@section('page-content')
<div class="panel panel-flat">
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-basic table-condensed" id="subDepartmentTable">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Code</th>
                        <th>Departement</th>
                        <th>Sub Departement</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
{!! Form::hidden('msg', $msg, array('id' => 'msg')) !!}
@endsection

@section('page-js')
<script src="{{ mix('js/sub_department.js') }}"></script>
@endsection