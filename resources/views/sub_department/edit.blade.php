@extends('layouts.app',['active' => 'sub_department'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Sub Departement</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Dashboard</a></li>
            <li>Organisasi</li>
            <li><a href="{{ route('subDepartment.index') }}">Sub Departement</a></li>
            <li class="active">Edit</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-flat">
    <div class="panel-body">
        {!!
            Form::open([
                'role'      => 'form',
                'url'       => ($sub_department ? route('subDepartment.update',$sub_department->id) : route('subDepartment.store') ),
                'method'    => 'post',
                'class'     => 'form-horizontal',
                'id'        => 'form'
            ])
        !!}
            

            @include('form.select', [
                'field'     => 'department',
                'label'     => 'Department',
                'default'   => ($sub_department ? $sub_department->department_id : null),
                'mandatory' => '*Required',
                'label_col' => 'col-md-2 col-lg-2 col-sm-12',
                'form_col'  => 'col-md-10 col-lg-10 col-sm-12',
                'options'   => [
                    '' => '-- Select Department --',
                ]+$departments,
                'class'      => 'select-search',
                'attributes' => [
                    'id' => 'select_department'
                ]
            ])

            @include('form.text', [
                'field'         => 'code',
                'label'         => 'Code',
                'default'       => ($sub_department ? strtoupper($sub_department->code) : null),
                #'mandatory'     => '*Required',
                'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                'attributes'    => [
                    'id'        => 'code',
                ]
            ])

            @include('form.text', [
                'field'         => 'name',
                'label'         => 'Name',
                'default'       => ucwords(strtolower($sub_department_absensi->subdept_name)),
                #'mandatory'     => '*Required',
                'label_col'     => 'col-md-2 col-lg-2 col-sm-12',
                'form_col'      => 'col-md-10 col-lg-10 col-sm-12',
                'attributes'    => [
                    'id'        => 'name',
                    'readonly'  => 'readonly'
                ]
            ])

            <div class="text-right">
                <button type="submit" class="btn btn-primary legitRipple col-xs-12">Save <i class="icon-floppy-disk position-right"></i></button>
            </div>
            {!! Form::hidden('sub_department_absensi_id', $id, array('id' => 'sub_department_absensi_id')) !!}
        {!! Form::close() !!}
    </div>
</div>

@endsection

@section('page-js')
    <script src="{{ mix('js/sub_department.js') }}"></script>
@endsection