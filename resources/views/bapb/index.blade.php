@extends('layouts.app',['active' => 'bapb'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">BAPB</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">BAPB</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
    <div class="panel panel-flat panel-collapsed border-grey">
        <div class="panel-heading">
            <h5 class="panel-title">Filters   <a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                </ul>
            </div>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12">
                    @include('form.select', [
                        'field'         => 'factory',
                        'label'         => 'Factory',
                        'mandatory'     => '*Required',
                        'default'       => auth::user()->factory_id,
                        'options'       => [
                            ''          => '-- Select Factory --',
                        ]+$factories,
                        'class'         => 'select-search',
                        'label_col'     => 'col-md-12 col-lg-12 col-sm-12',
                        'form_col'      => 'col-md-12 col-lg-12 col-sm-12',
                        'attributes'    => [
                            'id'        => 'select_factory'
                        ]
                    ])
                </div>

                <div class="col-lg-4 col-md-4 col-sm-12">
                    @include('form.select', [
                        'field'         => 'department',
                        'label'         => 'Department',
                        'mandatory'     => '*Required',
                        'default'       => auth::user()->sub_department_id,
                        'label_col'     => 'col-md-12 col-lg-12 col-sm-12',
                        'form_col'      => 'col-md-12 col-lg-12 col-sm-12',
                        'options'       => [
                            '' => '-- Select Subdepartment --',
                        ]+$sub_departments,
                        'class'         => 'select-search',
                        'attributes'    => [
                            'id'        => 'select_subdepartment'
                        ]
                    ])
                </div>

                <div class="col-lg-4 col-md-4 col-sm-12">
                    @include('form.select', [
                        'field'         => 'asset_type',
                        'label'         => 'Asset Type',
                        'mandatory'     => '*Required',
                        'label_col'     => 'col-md-12 col-lg-12 col-sm-12',
                        'form_col'      => 'col-md-12 col-lg-12 col-sm-12',
                        'options'       => [
                            ''          => '-- Select Asset Type --',
                        ],
                        'class' => 'select-search',
                        'attributes' => [
                            'id' => 'select_asset_type'
                        ]
                    ])
                </div>
            </div>
        </div>
    </div>

    <div class="panel panel-default border-grey">
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-basic table-condensed" id="bapbTable">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Id</th>
                            <th>last_company_location_id</th>
                            <th>last_factory_location_id</th>
                            <th>last_area_location</th>
                            <th>custom_field_1</th>
                            <th>custom_field_2</th>
                            <th>custom_field_3</th>
                            <th>custom_field_4</th>
                            <th>custom_field_5</th>
                            <th>custom_field_6</th>
                            <th>custom_field_7</th>
                            <th>custom_field_8</th>
                            <th>custom_field_9</th>
                            <th>custom_field_10</th>
                            <th>custom_field_11</th>
                            <th>custom_field_12</th>
                            <th>custom_field_13</th>
                            <th>custom_field_14</th>
                            <th>custom_field_15</th>
                            <th>custom_field_16</th>
                            <th>custom_field_17</th>
                            <th>custom_field_18</th>
                            <th>custom_field_19</th>
                            <th>custom_field_20</th>
                            <th>last_used_by</th>
                            <th>last_department_location_id</th>
                            <th>Asset Factory</th>
                            <th>Asset Department</th>
                            <th>No Inventori</th>
                            <th>No Inventori Manual</th>
                            <th>ERP No Asset (Accounting)</th>
                            <th>Asset Type</th>
                            <th>Model</th>
                            <th>Nomor Serial</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    <<ul class="fab-menu fab-menu-fixed fab-menu-bottom-right" data-fab-toggle="click">
		<li>
			<a class="fab-menu-btn btn btn-success btn-float btn-rounded btn-icon">
				<i class="fab-icon-open icon-cog52""></i>
				<i class="fab-icon-close icon-cross2"></i>
			</a>

			<ul class="fab-menu-inner">
				<li class ="{{ (auth::user()->is_super_admin ? '': 'hidden') }}">
					<div data-fab-label="Create Packing List">
						<a data-toggle="modal" data-target="#confirmationBapbModal"class="btn btn-default btn-rounded btn-icon btn-float">
							<i class="icon-cart2"></i>
                        </a>
                        <span class="badge bg-primary-400 hidden" id="total_cart"></span>
					</div>
				</li>
			</ul>
		</li>
    </ul>
    
    {!! Form::hidden('url_add_to_cart',route('bapb.addToCart'),array('id' => 'url_add_to_cart')) !!}
@endsection

@section('page-modal')
    @include('bapb._confirmation_modal')
@endsection

@section('page-js')
    @include('bapb._table')
    <script src="{{ mix('js/bapb.js') }}"></script>
@endsection
