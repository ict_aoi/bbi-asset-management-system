
<ul class="icons-list">
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-menu9"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-left">
            @if (isset($approve))
                <li><a href="javascript:void(0)" onclick="addToCart('{{ $approve }}')"><i class=" icon-cart-add2"></i> Add To Cart</a></li>
            @endif
            @if (isset($remove))
                <li><a href="javascript:void(0)" onclick="removeFromCart('{{ $remove }}')"><i class=" icon-cart-remove"></i> Remove from Cart</a></li>
            @endif
            @if (isset($cancel))
                <li><a href="javascript:void(0)" onclick="cancelObsolete('{{ $cancel }}')"><i class=" icon-cross2"></i> Cancel</a></li>
            @endif
        </ul>
    </li>
</ul>
