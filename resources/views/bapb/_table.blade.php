<script type="x-tmpl-mustache" id="table_cart">
	{% #item %}
		<tr>
			<td style="text-align:center;">{% no %}</td>
			<td>{% last_factory_name %}</td>
			<td>{% last_department_name %}</td>
			<td>{% barcode %}</td>
			<td>{% no_inventory %}</td>
			<td>{% model %}</td>
			<td>{% serial_number %}</td>
			<td>
				<button type="button" id="delete_{% _id %}" data-id="{% _id %}" class="btn btn-danger btn-delete-cart"><i class="icon-trash"></i></button>
			</td>
		</tr>
	{%/item%}
</script>