<div id="confirmationBapbModal" data-backdrop="static" data-keyboard="false" class="modal fade">
	<div class="modal-dialog modal-lg ui-front">
		<div class="modal-content">
				{!!
					Form::open([
						'role' 		=> 'form',
						'url' 		=> route('bapb.store'),
						'method' 	=> 'store',
						'class' 	=> 'form-horizontal',
						'id'		=> 'formApprove'
					])
				!!}
				<div class="modal-header bg-indigo">
					<h5 class="modal-title"><p>Confirmation Bapb Asset</h5>
				</div>
				<div class="modal-body">
                    <div class="row">
                        <div class="col-md-6 col-lg-6 col-sm-12">
                            @include('form.select', [
                                'field'         => 'bapb_factory',
                                'label'         => 'Bapb Factory',
                                'mandatory'     => '*Required',
                                'options'       => [
                                    ''          => '-- Select Factory --',
                                ]+$factories,
                                'class'         => 'select-search',
                                'label_col'     => 'col-md-12 col-lg-12 col-sm-12',
                                'form_col'      => 'col-md-12 col-lg-12 col-sm-12',
                                'attributes'    => [
                                    'id'        => 'select_bapb_factory'
                                ]
                            ])
                            
                            @include('form.select', [
                                'field'         => 'bapb_subdepartment',
                                'label'         => 'Bapb Subdepartment',
                                'mandatory'     => '*Required',
                                'label_col'     => 'col-md-12 col-lg-12 col-sm-12',
                                'form_col'      => 'col-md-12 col-lg-12 col-sm-12',
                                'options'       => [
                                    '' => '-- Select Subdepartment --',
                                ]+$sub_departments,
                                'class'         => 'select-search',
                                'attributes'    => [
                                    'id'        => 'select_bapb_subdepartment'
                                ]
                            ])
    
                            @include('form.text', [
                                'field'         => 'new_no_ba',
                                'label'         => 'No. BA',
                                'mandatory'     => '*Required',
                                'label_col'     => 'col-md-12 col-lg-12 col-sm-12',
                                'form_col'      => 'col-md-12 col-lg-12 col-sm-12',
                                'attributes'    => [
                                    'id'        => 'new_no_ba',
                                ]
                            ]) 
                        </div>
                        <div class="col-md-6 col-lg-6 col-sm-12">
                            @include('form.select', [
                                'field'     => 'status',
                                'label'     => 'Status',
                                'mandatory' => '*Required',
                                'default'   => '',
                                'options'   => [
                                    ''              => '-- Please Select Status --',
                                    'dijual'        => 'Sell',
                                    'scrap'         => 'Scrap',
                                ],
                                'class'      => 'select-search',
                                'label_col'  => 'col-md-12 col-lg-12 col-sm-12',
                                'form_col'   => 'col-md-12 col-lg-12 col-sm-12',
                                'attributes' => [
                                    'id'     => 'select_status',
                                ]
                                
                            ])
                            
                            @include('form.text', [
                                'field'         => 'buyer_name',
                                'label'         => 'Buyer Name',
                                'mandatory'     => '*Required when status is sell',
                                'label_col'     => 'col-md-12 col-lg-12 col-sm-12',
                                'form_col'      => 'col-md-12 col-lg-12 col-sm-12',
                                'attributes'    => [
                                    'id'        => 'buyer_name',
                                    'readonly'  => 'readonly'
                                ]
                            ])

                            @include('form.text', [
                                'field'     => 'cost_of_goods',
                                'label'     => 'Price',
                                'mandatory' => '*Required when status is sell',
                                'label_col'     => 'col-md-12 col-lg-12 col-sm-12',
                                'form_col'      => 'col-md-12 col-lg-12 col-sm-12',
                                'attributes' => [
                                    'id'       => 'cost_of_goods',
                                    'readonly' => 'readonly'
                                ]
                            ])
                        </div>
                    </div>

                    <div class="row">
                        <div class="table-responsive">
                            <table class="table table table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Factory</th>
                                        <th>Deparment</th>
                                        <th>Barcode</th>
                                        <th>No Inventory</th>
                                        <th>Model</th>
                                        <th>Serial</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody id="list_bapb"></tbody>
                            </table>
                        </div>
                    </div>

                    {!! Form::hidden('list_selected_data','[]',array('id' => 'list_selected_data')) !!}
			        {!! Form::hidden('current_time',null, array('id' => 'current_time')) !!}
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary legitRipple">Save <i class="icon-floppy-disk position-right"></i></button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
