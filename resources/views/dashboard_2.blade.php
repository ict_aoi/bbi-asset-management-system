@extends(($dashboard_eis ?  'layouts.dashboard' : 'layouts.app'),['active' => 'dashboard'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Dashboard</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ ($dashboard_eis ? route('dashboardEis') : route('home') ) }}">Dashboard</a></li>
            <li><a href="{{ ($dashboard_eis ? route('dashboardEis',['detail' => $detail])  : route('home',['detail' => $detail]) )}}">Detail {{ $detail }}</a></li>
            <li class="active">{{ $sub_department }}</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
    <div class="row">
        <div id ="draw_summary"></div>
    </div>
    </br/>

    <div class="panel panel-flat">
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-basic table-condensed" id="dashboardTable">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Id</th>
                            <th>Asset Type</th>
                            <th>Total</th>
                            <th>Total Digunakan</th>
                            <th>Total Dipinjam</th>
                            <th>Total Siap Digunakan</th>
                            <th>Total Rusak</th>
                            <th>Total Diperbaiki</th>
                            <th>Total Obsolete</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
   
    {!! Form::hidden('page', 'detail_2', ['id' => 'page']) !!}
    {!! Form::hidden('factory', $detail, ['id' => 'factory']) !!}
    {!! Form::hidden('sub_department', $sub_department, ['id' => 'sub_department']) !!}
    {!! Form::hidden('summary_data', '[]', ['id' => 'summary_data']) !!}
    {!! Form::hidden('url_data_summary', route('home.dashboardDataSummary',['detail' => $detail,'sub_department' => $sub_department]), ['id' => 'url_data_summary']) !!}
    {!! Form::hidden('url_detail_table_dashboard', route('home.dashboardDataTableSummary',['detail' => $detail,'sub_department' => $sub_department]), ['id' => 'url_detail_table_dashboard']) !!}
@endsection


@section('page-js')
    @include('_summary_dashboard')
    <script src="{{ mix('js/echarts.js') }}"></script>
    <script src="{{ mix('js/dashboard.js') }}"></script>
@endsection