summary = JSON.parse($('#summary_data').val());

$(document).ready( function ()
{
    var page = $('#page').val();

    if(page == 'home')
    {
        getSummaryDashboard();
        getSummaryDataPie('bbis');
    }else if(page == 'detail_1')
    {
        getSummaryDashboard();
        getSummaryDataPieDepartment('mechanical');
        getSummaryDataPieDepartment('electric');
        getSummaryDataPieDepartment('ga');
        getSummaryDataPieDepartment('ict');
    }else if(page == 'detail_2')
    {
        getSummaryDashboard();

        var url = $('#url_detail_table_dashboard').val();
        var userTable = $('#dashboardTable').DataTable({
            dom: 'Bfrtip',
            processing: true,
            serverSide: true,
            pageLength:-1,
            deferRender:true,
            ajax: {
                type: 'GET',
                url: url,
            },
            fnCreatedRow: function (row, data, index) {
                var info = userTable.page.info();
                var value = index+1+info.start;
                $('td', row).eq(0).html(value);
            },
            footerCallback: function ( row, data, start, end, display ) {
                var api = this.api(), data;
    
                // converting to interger to find total
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };
    
                // computing column Total of the complete result
                var total_all = api
                    .column( 3 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                var total_all_used = api
                .column( 4 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

                var total_all_dipinjam = api
                .column( 5 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

                var total_all_siap_digunakan = api
                .column( 6 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

                var total_all_rusak = api
                .column( 7 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

                var total_all_diperbaiki = api
                .column( 8 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

                var total_all_obsolete = api
                .column( 9 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
    
                // Update footer by showing the total with the reference of the column index
                $( api.column( 2 ).footer() ).html('Total');
                $( api.column( 3 ).footer() ).html(total_all);
                $( api.column( 4 ).footer() ).html(total_all_used);
                $( api.column( 5 ).footer() ).html(total_all_dipinjam);
                $( api.column( 6 ).footer() ).html(total_all_siap_digunakan);
                $( api.column( 7 ).footer() ).html(total_all_rusak);
                $( api.column( 8 ).footer() ).html(total_all_diperbaiki);
                $( api.column( 9 ).footer() ).html(total_all_obsolete);
    
            },
            columns: [
                {data: null, sortable: false, orderable: false, searchable: false},
                {data: 'id', name: 'id',searchable:true,visible:false,orderable:false},
                {data: 'name', name: 'name',searchable:true,orderable:true},
                {data: 'total_all', name: 'total_all',searchable:true,orderable:true},
                {data: 'total_used', name: 'total_used',searchable:true,orderable:true},
                {data: 'total_dipinjam', name: 'total_dipinjam',searchable:true,orderable:true},
                {data: 'total_siap_digunakan', name: 'total_siap_digunakan',searchable:true,orderable:true},
                {data: 'total_rusak', name: 'total_rusak',searchable:true,orderable:true},
                {data: 'total_diperbaiki', name: 'total_diperbaiki',searchable:true,orderable:true},
                {data: 'total_obsolete', name: 'total_obsolete',searchable:true,visible:true,orderable:true},
            ]
        });
    
        var dtable = $('#dashboardTable').dataTable().api();
        $(".dataTables_filter input")
            .unbind() // Unbind previous default bindings
            .bind("keyup", function (e) { // Bind our desired behavior
                // If the user pressed ENTER, search
                if (e.keyCode == 13) {
                    // Call the API search function
                    dtable.search(this.value).draw();
                }
                // Ensure we clear the search if they backspace far enough
                if (this.value == "") {
                    dtable.search("").draw();
                }
                return;
        });
        dtable.draw();
    }
   
});

function getSummaryDashboard()
{
    var url = $('#url_data_summary').val();
    $.ajax({
        type: 'get',
        url: url
    })
    .done(function(response)
    {
        summary = response;
        render();
    });   
}

function getSummaryDataPieDepartment(sub_department)
{
    var factory = $('#factory').val();
    var url     = $('#url_data_pie_department').val();
    
    $.ajax({
        type: 'get',
        url: url,
        data:{
            sub_department  :sub_department,
            factory         :factory
        }
    })
    .done(function(response)
    {
        
        if(response.sub_department == 'mechanical')
        {
            $('#asset_status_legend_mechanical').val(JSON.stringify(response.asset_status_legend));
            $('#total_asset_mechanical').val(JSON.stringify(response.total_asset));
    
        }else if(response.sub_department == 'electric')
        {
            $('#asset_status_legend_electric').val(JSON.stringify(response.asset_status_legend));
            $('#total_asset_electric').val(JSON.stringify(response.total_asset));
        }else if(response.sub_department == 'ga')
        {
            $('#asset_status_legend_ga').val(JSON.stringify(response.asset_status_legend));
            $('#total_asset_ga').val(JSON.stringify(response.total_asset));
        }else if(response.sub_department == 'ict')
        {
            $('#asset_status_legend_ict').val(JSON.stringify(response.asset_status_legend));
            $('#total_asset_ict').val(JSON.stringify(response.total_asset));
        }

        renderSubDepartment(response.sub_department);
    });  
}

function getSummaryDataPie(factory)
{
    var url = $('#url_data_pie').val();
    $.ajax({
        type: 'get',
        url: url,
        data:{
            factory:factory
        }
    })
    .done(function(response)
    {
        $('#asset_status_legend_bbi').val(JSON.stringify(response.asset_status_legend));
        $('#total_asset_bbi').val(JSON.stringify(response.total_asset));
        
        renderPie(response.factory);
    });  
}

function renderSubDepartment(sub_department)
{
    var factory = $('#factory').val();
    require.config({
        paths: {
            echarts: 'js/visualization/echarts'
        }
    });

    require(
        [
            'echarts',
            'echarts/theme/limitless',
            'echarts/chart/pie',
        ],

        function (ec, limitless) 
        {

            if(sub_department == 'mechanical')
            {
                var asset_status_legend     = JSON.parse($('#asset_status_legend_mechanical').val());
                var total_asset             = JSON.parse($('#total_asset_mechanical').val());
                var connect_pie             = ec.init(document.getElementById('connect_pie_mechanic'), limitless);
            }else if(sub_department == 'electric')
            {
                var asset_status_legend     = JSON.parse($('#asset_status_legend_electric').val());
                var total_asset             = JSON.parse($('#total_asset_electric').val());
                var connect_pie             = ec.init(document.getElementById('connect_pie_electric'), limitless);
            }else if(sub_department == 'ga')
            {
                var asset_status_legend     = JSON.parse($('#asset_status_legend_ga').val());
                var total_asset             = JSON.parse($('#total_asset_ga').val());
                var connect_pie             = ec.init(document.getElementById('connect_pie_ga'), limitless);
            }else if(sub_department == 'ict')
            {
                var asset_status_legend     = JSON.parse($('#asset_status_legend_ict').val());
                var total_asset             = JSON.parse($('#total_asset_ict').val());
                var connect_pie             = ec.init(document.getElementById('connect_pie_ict'), limitless);
            }

            connect_pie_options     = 
            {
                // Add title
                title: 
                {
                    text: 'Total Asset '+sub_department,
                    subtext: 'Data '+factory,
                    x: 'center'
                },

                // Add tooltip
                tooltip: 
                {
                    trigger: 'item',
                    formatter: "{a} <br/>{b}: {c} ({d}%)"
                },

                // Add legend
                legend: 
                {
                    orient: 'vertical',
                    x: 'left',
                    data: asset_status_legend
                },

                // Enable drag recalculate
                calculable: false,

                // Add series
                series: [{
                    name: 'Status Asset',
                    type: 'pie',
                    radius: '75%',
                    center: ['50%', '57.5%'],
                    data: total_asset
                }]
            };

            connect_pie.setOption(connect_pie_options);
            connect_pie.on('click', function (params) 
            {
                //alert('bung nge klik status ' + params.data.name)
            });   
        }
    );
}

function renderPie(factory)
{
    require.config({
        paths: {
            echarts: 'js/visualization/echarts'
        }
    });

    require(
        [
            'echarts',
            'echarts/theme/limitless',
            'echarts/chart/pie',
        ],

        function (ec, limitless) 
        {

            var asset_status_legend = JSON.parse($('#asset_status_legend_bbi').val());
            var total_asset         = JSON.parse($('#total_asset_bbi').val());
            var connect_pie         = ec.init(document.getElementById('connect_pie_bbi'), limitless);
       
            
           
            connect_pie_options     = 
            {
                // Add title
                title: 
                {
                    text: 'Total Asset '+factory,
                    subtext: 'Data '+factory,
                    x: 'center'
                },

                // Add tooltip
                tooltip: 
                {
                    trigger: 'item',
                    formatter: "{a} <br/>{b}: {c} ({d}%)"
                },

                // Add legend
                legend: 
                {
                    orient: 'vertical',
                    x: 'left',
                    data: asset_status_legend
                },

                // Enable drag recalculate
                calculable: false,

                // Add series
                series: [{
                    name: 'Status Asset',
                    type: 'pie',
                    radius: '75%',
                    center: ['50%', '57.5%'],
                    data: total_asset
                }]
            };

            connect_pie.setOption(connect_pie_options);
            connect_pie.on('click', function (params) 
            {
                //alert('bung nge klik status ' + params.data.name)
            });   
        }
    );
}

function render() 
{
    $('#summary_data').val(JSON.stringify(summary));
    var tmpl = $('#summary_dashboard').html();
    Mustache.parse(tmpl);
    var data = { item: summary };
    var html = Mustache.render(tmpl, data);
    $('#draw_summary').html(html);
}
