$(document).ready( function ()
{ 
    var msg = $('#msg').val();
    
    $('input.input-date').datepicker({
        format: "dd/mm/yyyy",
        autoclose: true,
        todayHighlight: true
    });

    if (msg == 'success') $("#alert_success").trigger("click", 'Data successfully saved.');
    else if (msg == 'success_2') $("#alert_success").trigger("click", 'Data successfully updated.');
    
    var calendarTable = $('#calendarTable').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:10,
        scrollY:250,
        paging: true, 
        scrollX:true,
        deferRender:true,
        ajax: {
            type: 'GET',
            url: '/calendar/data',
        },
        fnCreatedRow: function (row, data, index) {
            var info = calendarTable.page.info();
            var value = index+1+info.start+data.action;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'id', name: 'id',searchable:true,visible:false,orderable:false},
            {data: 'event_name', name: 'event_name',searchable:true,orderable:true},
            {data: 'event_start', name: 'event_start',searchable:true,orderable:true},
            {data: 'event_end', name: 'event_end',searchable:true,orderable:true},
        ]
    });

    var dtable = $('#calendarTable').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
    });
    dtable.draw();

    $('#form').submit(function (event){
        event.preventDefault();
        var name            = $('#name').val();
        var event_start     = $('#event_start').val();
        var event_end       = $('#event_end').val();
        var current_time    = $('#time').text();
        
        $('#current_time').val(current_time);
        
        if(!name)
        {
            $("#alert_warning").trigger("click", 'Please type name first');
            return false;
        }
        if(!event_start)
        {
            $("#alert_warning").trigger("click", 'Please select event start first');
            return false;
        }
        if(!event_end)
        {
            $("#alert_warning").trigger("click", 'Please select event end first');
            return false;
        }

        if(event_start && event_end)
        {
            var d1 = event_start.split("/");
            var d2 = event_end.split("/");

            var from = new Date(d1[2], parseInt(d1[1])-1, d1[0]);  // -1 because months are from 0 to 11
            var to   = new Date(d2[2], parseInt(d2[1])-1, d2[0]);

            if(from > to)
            {
                $("#alert_warning").trigger("click", 'event end date cannot be small than event start');
                return false
            }
           
        }
        
        bootbox.confirm("Are you sure want to save this data ?.", function (result) {
            if(result){
                $.ajax({
                    type: "POST",
                    url: $('#form').attr('action'),
                    data: $('#form').serialize(),
                    beforeSend: function () {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function () {
                        document.location.href = '/calendar';
                    },
                    error: function (response) {
                        $.unblockUI();
                        
                        if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON.message);
                    }
                });
            }
        });
    });   
});


function hapus(url)
{
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    $.ajax({
        type: "delete",
        url: url,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function () {
            $.unblockUI();
        },
        error: function () {
            $.unblockUI();
        }
    }).done(function ($result) {
        $('#permissionTable').DataTable().ajax.reload();
        $("#alert_success").trigger("click", 'Data successfully deleted');
    });
}