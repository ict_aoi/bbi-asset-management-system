$(document).ready(function() 
{
    var is_super_admin  = $('#is_super_admin').val();
    $('input.input-date').datepicker({
        format: "dd/mm/yyyy",
        autoclose: true,
        todayHighlight: true
    });
    
    list_assets = JSON.parse($('#assets').val());
    
    $('#form').on('keyup keypress', function(e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode == 13) {
            e.preventDefault();
            return false;
        }
    });

    $('#form').submit(function(event) 
    {
        event.preventDefault();
        var sub_department_id       = $('#sub_department_id').val();
        var factory_id              = $('#factory_id').val();
        var teknisi                 = $('#teknisi').val();
        var root_cause              = $('#root_cause').val();
        var current_time            = $('#time').text();
        
        $('#current_time').val(current_time);

        if(!factory_id)
        {
            $("#alert_warning").trigger("click", 'Please select factory first');
            return false;
        }

        if(!sub_department_id)
        {
            $("#alert_warning").trigger("click", 'Please select subdepartment first');
            return false;
        }


        if(teknisi == '')
        {
            $("#alert_warning").trigger("click", 'Please type technician name first');
            return false;
        }

        if(root_cause == '')
        {
            $("#alert_warning").trigger("click", 'Please type root cause first');
            return false;
        }
        
        $('#confirmationInsertModal').modal('hide');
        bootbox.confirm("Are you sure want to save this data ?.", function(result) {
            if (result) {
                $.ajax({
                    type: 'post',
                    url: $('#form').attr('action'),
                    data: $('#form').serialize(),
                    beforeSend: function() {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    complete: function() {
                        $.unblockUI();
                    },
                    success: function() {
                        $("#alert_success").trigger("click", 'Maintenance successfully');
                        $('#maintenanceTable').DataTable().ajax.reload();

                        $('#form').trigger('reset');
                    },
                    error: function(response) {
                        $.unblockUI();
                        if (response.status == 422) $("#alert_warning").trigger("click", response.responseJSON.message);

                    }
                });
            }
        });
    });

    if(is_super_admin == 1)
    {
        var maintenanceTable = $('#maintenanceTable').DataTable({
            dom: 'Bfrtip',
            processing: true,
            serverSide: true,
            pageLength:100,
            deferRender:true,
            scrollY:250,
            paging: true, 
            scrollX:true,
            ajax: {
                type: 'GET',
                url: '/maintenance/data',
                data: function(d) {
                     return $.extend({}, d, {
                         "company"         : $('#select_company').val(),
                         "sub_department"  : $('#select_sub_department').val(),
                         "asset_type"      : $('#select_asset_type').val(),
                         "factory"         : $('#select_factory').val(),
                     });
                }
            },
            fnCreatedRow: function (row, data, index) {
                var info = maintenanceTable.page.info();
                var value = index+1+info.start+data.action;
                $('td', row).eq(0).html(value);
            },
            columns: [
                {data: null, sortable: false, orderable: false, searchable: false},
                {data: 'id', name: 'id',searchable:true,visible:false,orderable:false},
                {data: 'last_company_location_id', name: 'last_company_location_id',searchable:true,visible:false,orderable:false},
                {data: 'last_factory_location_id', name: 'last_factory_location_id',searchable:true,visible:false,orderable:false},
                {data: 'last_area_location', name: 'last_area_location',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_1', name: 'custom_field_1',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_2', name: 'custom_field_2',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_3', name: 'custom_field_3',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_4', name: 'custom_field_4',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_5', name: 'custom_field_5',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_6', name: 'custom_field_6',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_7', name: 'custom_field_7',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_8', name: 'custom_field_8',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_9', name: 'custom_field_9',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_10', name: 'custom_field_10',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_11', name: 'custom_field_11',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_12', name: 'custom_field_12',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_13', name: 'custom_field_13',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_14', name: 'custom_field_14',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_15', name: 'custom_field_15',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_16', name: 'custom_field_16',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_17', name: 'custom_field_17',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_18', name: 'custom_field_18',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_19', name: 'custom_field_19',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_20', name: 'custom_field_20',searchable:true,visible:false,orderable:false},
                {data: 'last_used_by', name: 'last_used_by',searchable:true,visible:false,orderable:false},
                {data: 'last_department_location_id', name: 'last_department_location_id',searchable:true,visible:false,orderable:false},
                {data: 'origin_factory_name', name: 'origin_factory_name',searchable:true,orderable:true},
                {data: 'origin_department_name', name: 'origin_department_name',searchable:true,orderable:true},
                {data: 'no_inventory', name: 'no_inventory',searchable:true,orderable:true},
                {data: 'no_inventory_manual', name: 'no_inventory_manual',searchable:true,orderable:true},
                {data: 'erp_no_asset', name: 'erp_no_asset',searchable:true,orderable:true},
                {data: 'asset_type', name: 'asset_type',searchable:true,orderable:true},
                {data: 'model', name: 'model',searchable:true,orderable:true},
                {data: 'serial_number', name: 'serial_number',searchable:true,orderable:true}
            ]
        });
    }else
    {
        var maintenanceTable = $('#maintenanceTable').DataTable({
            dom: 'Bfrtip',
            processing: true,
            serverSide: true,
            pageLength:100,
            deferRender:true,
            scrollY:250,
            paging: true, 
            scrollX:true,
            ajax: {
                type: 'GET',
                url: '/maintenance/data',
                data: function(d) {
                     return $.extend({}, d, {
                         "company"         : $('#select_company').val(),
                         "sub_department"  : $('#select_sub_department').val(),
                         "asset_type"      : $('#select_asset_type').val(),
                         "factory"         : $('#select_factory').val(),
                     });
                }
            },
            fnCreatedRow: function (row, data, index) {
                var info = maintenanceTable.page.info();
                var value = index+1+info.start+data.action;
                $('td', row).eq(0).html(value);
            },
            columns: [
                {data: null, sortable: false, orderable: false, searchable: false},
                {data: 'id', name: 'id',searchable:true,visible:false,orderable:false},
                {data: 'last_company_location_id', name: 'last_company_location_id',searchable:true,visible:false,orderable:false},
                {data: 'last_factory_location_id', name: 'last_factory_location_id',searchable:true,visible:false,orderable:false},
                {data: 'last_area_location', name: 'last_area_location',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_1', name: 'custom_field_1',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_2', name: 'custom_field_2',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_3', name: 'custom_field_3',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_4', name: 'custom_field_4',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_5', name: 'custom_field_5',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_6', name: 'custom_field_6',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_7', name: 'custom_field_7',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_8', name: 'custom_field_8',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_9', name: 'custom_field_9',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_10', name: 'custom_field_10',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_11', name: 'custom_field_11',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_12', name: 'custom_field_12',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_13', name: 'custom_field_13',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_14', name: 'custom_field_14',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_15', name: 'custom_field_15',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_16', name: 'custom_field_16',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_17', name: 'custom_field_17',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_18', name: 'custom_field_18',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_19', name: 'custom_field_19',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_20', name: 'custom_field_20',searchable:true,visible:false,orderable:false},
                {data: 'last_used_by', name: 'last_used_by',searchable:true,visible:false,orderable:false},
                {data: 'last_department_location_id', name: 'last_department_location_id',searchable:true,visible:false,orderable:false},
                {data: 'no_inventory', name: 'no_inventory',searchable:true,orderable:true},
                {data: 'no_inventory_manual', name: 'no_inventory_manual',searchable:true,orderable:true},
                {data: 'erp_no_asset', name: 'erp_no_asset',searchable:true,orderable:true},
                {data: 'asset_type', name: 'asset_type',searchable:true,orderable:true},
                {data: 'model', name: 'model',searchable:true,orderable:true},
                {data: 'serial_number', name: 'serial_number',searchable:true,orderable:true},
            ]
        });
    }

    var dtable = $('#maintenanceTable').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
    });
    dtable.draw();
    
    $('#select_company').on('change',function()
    {
        dtable.draw();
    });

    $('#select_factory').on('change',function()
    {
        dtable.draw();
    });

    $('#select_sub_department').on('change',function()
    {
        dtable.draw();
    });

    $('#select_asset_type').on('change',function()
    {
        dtable.draw();
    });

    $('#select_status_asset').on('change',function()
    {
        dtable.draw();
    });
    
    $('#select_factory').trigger('change');
    $('#select_sub_department').trigger('change');
});


$('#select_factory').on('change',function (event,arg1) 
{
    var value = $(this).val();    
    $('#factory_id').val(value);
});

$('#select_sub_department').on('change',function (event,arg1) 
{
    var asset_type_id   = '';    
    var sub_department_id = $(this).val();    
    $('#sub_department_id').val(sub_department_id);

    $.ajax({
        type: 'get',
        url: '/maintenance/asset-type?sub_department_id='+sub_department_id+'&asset_type_id='+asset_type_id+'&flag=asset_type'
    })
    .done(function(response)
    {
        var  asset_types    = response.asset_types;
        var  asset_type_id  = response.asset_type_id;
       
        $("#select_asset_type").empty();
        $("#select_asset_type").append('<option value="">-- Select Asset Type --</option>');

        $.each(asset_types,function(id,name){
            if(asset_type_id == id) var selected = 'selected';
            else var selected = null;

            $("#select_asset_type").append('<option value="'+id+'"'+selected+'>'+name+'</option>');
        });

        if(asset_type_id) $('#select_asset_type').trigger('change');
        custom_fields = [];
        render();
    });
});

$('#btn_confirmation').on('click',function() 
{
    $('#select_status').val('diperbaiki').trigger('change');
    $('#teknisi').val('');
    $('#root_cause').val('');
    $('#confirmationInsertModal').modal();
});

function editStatus(id)
{
    $('#select_status').val('diperbaiki').trigger('change');
    $('#confirmationInsertModal').modal();
    $('#asset_id').val(id);
}

