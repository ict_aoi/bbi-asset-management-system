$(document).ready( function ()
{
    
    var msg = $('#msg').val();
    var is_super_admin = $('#is_super_admin').val();
    if (msg == 'success') $("#alert_success").trigger("click", 'Data successfully saved');
    else if (msg == 'success_2') $("#alert_success").trigger("click", 'Data successfully updated');

    if(is_super_admin == 1)
    {
        var assetTypetable = $('#assetTypeTable').DataTable({
            dom: 'Bfrtip',
            processing: true,
            serverSide: true,
            pageLength:10,
            scrollY:250,
            paging: true, 
            scrollX:true,
            deferRender:true,
            ajax: {
                type: 'GET',
                url: '/setting/asset-type/data',
                data: function(d) {
                    return $.extend({}, d, {
                        "sub_department"  : $('#select_sub_department').val(),
                        "is_consumable"   : $('#select_is_consumable').val(),
                    });
               }
            },
            fnCreatedRow: function (row, data, index) {
                var info = assetTypetable.page.info();
                var value = index+1+info.start+data.action;
                $('td', row).eq(0).html(value);
            },
            columns: [
                {data: null, sortable: false, orderable: false, searchable: false},
                {data: 'sub_department_name', name: 'sub_department_name',searchable:true,orderable:true},
                {data: 'erp_category_name', name: 'erp_category_name',searchable:true,orderable:true},
                {data: 'erp_asset_group_name', name: 'erp_asset_group_name',searchable:true,orderable:true},
                {data: 'name', name: 'name',searchable:true,orderable:true},
                {data: 'description', name: 'description',searchable:true,orderable:false},
                {data: 'is_consumable', name: 'is_consumable',searchable:false,orderable:true},
            ]
        });
    }else
    {
        var assetTypetable = $('#assetTypeTable').DataTable({
            dom: 'Bfrtip',
            processing: true,
            serverSide: true,
            pageLength:10,
            scrollY:250,
            paging: true, 
            scrollX:true,
            deferRender:true,
            ajax: {
                type: 'GET',
                url: '/setting/asset-type/data',
                data: function(d) {
                    return $.extend({}, d, {
                        "sub_department"  : $('#select_sub_department').val(),
                        "is_consumable"   : $('#select_is_consumable').val(),
                    });
               }
            },
            fnCreatedRow: function (row, data, index) {
                var info = assetTypetable.page.info();
                var value = index+1+info.start+data.action;
                $('td', row).eq(0).html(value);
            },
            columns: [
                {data: null, sortable: false, orderable: false, searchable: false},
                {data: 'sub_department_name', name: 'sub_department_name',searchable:true,orderable:true},
                {data: 'name', name: 'name',searchable:true,orderable:true},
                {data: 'erp_category_name', name: 'erp_category_name',searchable:true,orderable:true},
                {data: 'erp_asset_group_name', name: 'erp_asset_group_name',searchable:true,orderable:true},
                {data: 'description', name: 'description',searchable:true,orderable:false},
                {data: 'is_consumable', name: 'is_consumable',searchable:false,orderable:true},
            ]
        });
    }

    var dtable = $('#assetTypeTable').dataTable().api();
    $("#assetTypeTable.dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
    });
    dtable.draw();


    mappings = JSON.parse($('#mappings').val());
    var form_status = $('#form_status').val();
    var department_name = $('#department_name').val();

    $('#form').submit(function (event)
    {
        event.preventDefault();
        var erp_category    = $('#select_erp_catogories').val();
        var sub_department  = $('#select_sub_department').val();
        var name            = $('#name').val();
        var code            = $('#code').val();
       
        var current_time    = $('#time').text();
        $('#current_time').val(current_time);
        
        if(sub_department == '')
        {
            $("#alert_warning").trigger("click", 'Please select sub departemen first');
            return false;
        }

        if(erp_category == '')
        {
            $("#alert_warning").trigger("click", 'Please select erp category first');
            return false;
        }


        if(code == '')
        {
            $("#alert_warning").trigger("click", 'Please type code first');
            return false;
        }

        if(!name)
        {
            $("#alert_warning").trigger("click", 'Please type name first');
            return false
        }
       
        bootbox.confirm("Are you sure want to save this data ?.", function (result) {
            if(result){
                $.ajax({
                    type: "POST",
                    url: $('#form').attr('action'),
                    data: $('#form').serialize(),
                    beforeSend: function () {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function () {
                        document.location.href = '/setting/asset-type';
                    },
                    error: function (response) 
                    {
                        $.unblockUI();
                        
                        if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON.message);
                    }
                });
            }
        });
    });

    $('#select_company').trigger('change',department_name);

    if(form_status == 'create')
    {
        render();
    }else
    {
        if(form_status == 'index')
        {
            $('#select_sub_department').on('change',function()
            {
                dtable.draw();
            });

            $('#select_is_consumable').on('change',function()
            {
                dtable.draw();
            });
        }else
        {
            var url_data_custom_field = $('#url_data_custom_field').val();
            var is_comsumable_chacked = $('#is_comsumable_chacked').val();
            
            if(is_comsumable_chacked) $("#checkbox_is_consumable").parent().find(".switchery").prop('checked', true).trigger("click");
            else $("#checkbox_is_consumable").parent().find(".switchery").prop('checked', false).trigger("click");
    
            $('#customFieldTable').DataTable().destroy();
            $('#customFieldTable tbody').empty();
            var table = $('#customFieldTable').DataTable({
                dom: 'Bfrtip',
                processing: true,
                serverSide: true,
                pageLength:-1,
                scrollY:700,
                scroller:true,
                destroy:true,
                deferRender:true,
                bFilter:true,
                ajax: 
                {
                    type: 'GET',
                    url: url_data_custom_field,
                },
                fnCreatedRow: function (row, data, index) 
                {
                    var info = table.page.info();
                    var value = index+1+info.start;
                    $('td', row).eq(0).html(value);
                },
                columns: [
                    {data: null, sortable: false, orderable: false, searchable: false},
                    {data: 'id', name: 'id',searchable:true,visible:false,orderable:false},
                    {data: 'name', name: 'name',searchable:true,orderable:true},
                    {data: 'label', name: 'label',searchable:true,orderable:true},
                    {data: 'type', name: 'type',searchable:true,orderable:true},
                    {data: 'value', name: 'value',searchable:true,orderable:true},
                    {data: 'required', name: 'required',searchable:true,orderable:true},
                    {data: 'action', name: 'action',searchable:false,orderable:false},
                ]
            });
    
            var dtable2 = $('#customFieldTable').dataTable().api();
            $("#customFieldTable.dataTables_filter input")
                .unbind() // Unbind previous default bindings
                .bind("keyup", function (e) { // Bind our desired behavior
                    if (e.keyCode == 13) {
                        // Call the API search function
                        dtable2.search(this.value).draw();
                    }
                    if (this.value == "") {
                        dtable2.search("").draw();
                    }
                    return;
            });
            dtable2.draw();
    
            render();
        }
        
    }

    var code = $('#code').val();
    var name = $('#name').val();
    
    if(code) $('#code').attr('readonly','readonly');
    //if(name) $('#name').attr('readonly','readonly');
});

function hapus(url)
{
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    $.ajax({
        type: "put",
        url: url,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function () {
            $.unblockUI();
        },
        error: function () {
            $.unblockUI();
        }
    })
    .done(function () {
        $('#assetTypeTable').DataTable().ajax.reload();
        $("#alert_success").trigger("click", 'Data successfully deleted');
        location.reload();
    });
}

$('#submit_button').on('click',function()
{
    $('#form').trigger('submit');
});

$('#select_custom_field').on('change',function()
{
    var value               = $(this).val();    
    var _custom_field_id    = $('#_custom_field_id').val();    
    var name                = $(this).select2('data')[0].text;

    if(value)
    {
        if(_custom_field_id == null || _custom_field_id == '')
        {
            var diff = checkItem(value);
            if (!diff) 
            {
                $("#alert_warning").trigger("click", name+' already selected');
                $(this).val('').trigger('change');
                return false;
            }
        }
        
    }
    
});

$('#select_custom_type').on('change',function()
{
    var value = $(this).val();  
    if(value == 'option')
    {
        $('#div_default_value').removeClass("hidden");
    }else
    {
        $('#div_default_value').addClass("hidden");
        $("#custom_default_value").tagsinput('removeAll');
    }

    if(value == 'lookup')
    {
        $('#div_custom_lookup').removeClass("hidden");
    }else
    {
        $('#custom_lookupId').val('');
        $('#custom_lookupName').val('');
        $('#div_custom_lookup').addClass("hidden");
    }
    
});

$('#checkbox_is_consumable').on('click',function()
{
    var is_consumable  = $('#checkbox_is_consumable').is(":checked");
    var form_status   = $('#form_status').val();

    if(form_status == 'create')
    {
        $('#select_custom_field').val('').trigger('change').removeAttr('disabled');
        $('#select_custom_type').val('').trigger('change');
        $('#_custom_field_id').val('');
        $('#custom_lookupName').val('');
        $('#custom_lookupId').val('');
        $('#custom_label').val('');
        $("#custom_default_value").tagsinput('removeAll');
        $("#is_required").parent().find(".switchery").prop('checked', false).trigger("click");
        
        $.ajax({
            type: "get",
            url: '/setting/asset-type/list-available-columns',
            data:{
             'is_consumable' : is_consumable   
            }
        })
        .done(function (response) 
        {
            mappings = [];
            for (idx in response) 
            {
                var data = response[idx];
               
                var input = 
                {
                    'custom_field_id'       : data.custom_field_id,
                    'custom_label'          : data.custom_label,
                    'custom_type_id'        : data.custom_type_id,
                    'custom_type_name'      : data.custom_type_name,
                    'custom_lookup_id'      : data.custom_lookup_id,
                    'custom_default_value'  : data.custom_default_value,
                    'is_required'           : data.is_required,
                    'is_edited'             : data.is_edited,
                    'is_custom'             : data.is_custom,
                };
        
                mappings.push(input);
            }

            render();
        });
    }
    
});

$('#btn_save_custom_field').on('click',function()
{
    var url_update_custom_field         = $('#url_update_custom_field').val(); 
    var form_status                     = $('#form_status').val(); 
    var _custom_field_id                = $('#_custom_field_id').val();  
    var custom_field_id                 = $('#select_custom_field').val();  
    var custom_label                    = $('#custom_label').val(); 
    var custom_type_id                  = $('#select_custom_type').val();  
    var custom_type_name                = $('#select_custom_type').select2('data')[0].text; 
    var _custom_default_value           = $('#custom_default_value').val(); 
    var custom_lookup_name              = $('#custom_lookupName').val(); 
    var custom_lookup_id                = $('#custom_lookupId').val(); 
    var is_required                     = $('#is_required').is(":checked");
    var checkbox_is_consumable          = $('#checkbox_is_consumable').is(":checked");

    if(custom_type_id == 'option') custom_default_value = _custom_default_value;
    else if(custom_type_id == 'lookup') custom_default_value = custom_lookup_name;
    else custom_default_value = null;

    if(!custom_field_id)
    {
        $("#alert_warning").trigger("click",'Please select custom field first');
        return false;
    }

    if(!custom_label)
    {
        $("#alert_warning").trigger("click",'Please type custom label first');
        return false;
    }

    if(!custom_type_id)
    {
        $("#alert_warning").trigger("click",'Please select custom type first');
        return false;
    }

    if(checkbox_is_consumable)
    {
        if(custom_type_id == 'lookup')
        {
            $("#alert_warning").trigger("click",'Custom lookup only available when asset type is non consumable');
            return false;
        }
    }else
    {
        if(custom_type_id == 'lookup')
        {
            
            if(!custom_lookup_id)
            {
                $("#alert_warning").trigger("click",'Please select lookup value first');
                return false;
            }else
            {
                var check_lookup_exists = checkLookupExists(custom_lookup_id);
                if(!_custom_field_id)
                {
                    if(check_lookup_exists > 0)
                    {
                        $("#alert_warning").trigger("click",'Lookup id is already exists');
                        return false;
                    }
                }
                
            }
        }
        
    }
    
    if(form_status == 'edit')
    {
        var asset_type_id = $('#asset_type_id').val(); 
        
        if(!url_update_custom_field)
        {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'post',
                url: '/setting/asset-type/store/asset-information-custom-field',
                data: 
                {
                    'custom_field_id'       : custom_field_id,
                    'custom_label'          : custom_label,
                    'custom_type_id'        : custom_type_id,
                    'is_required'           : is_required,
                    'custom_default_value'  : custom_default_value,
                    'custom_lookup_id'      : custom_lookup_id,
                    asset_type_id           : asset_type_id
                }
            })
            .done(function () 
            {
                $('#customFieldTable').DataTable().ajax.reload();
                var input = 
                {
                    'custom_field_id'       : custom_field_id,
                    'custom_label'          : custom_label,
                    'custom_type_id'        : custom_type_id,
                    'custom_type_name'      : custom_type_name,
                    'custom_lookup_id'      : custom_lookup_id,
                    'custom_default_value'  : custom_default_value,
                    'is_required'           : is_required,
                    'is_edited'             : false,
                    'is_custom'             : true,
                };
        
                mappings.push(input);
                render();
            });
        }else
        {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            
            $.ajax({
                type: "put",
                url: url_update_custom_field,
                data : 
                {
                    'custom_field_id'       : custom_field_id,
                    'custom_label'          : custom_label,
                    'custom_type_id'        : custom_type_id,
                    'is_required'           : is_required,
                    'custom_default_value'  : custom_default_value,
                    'custom_lookup_id'      : custom_lookup_id,
                }
            })
            .done(function () 
            {
                $('#customFieldTable').DataTable().ajax.reload();
                var input = 
                {
                    'custom_field_id'       : custom_field_id,
                    'custom_label'          : custom_label,
                    'custom_type_id'        : custom_type_id,
                    'custom_type_name'      : custom_type_name,
                    'custom_lookup_id'      : custom_lookup_id,
                    'custom_default_value'  : custom_default_value,
                    'is_required'           : is_required,
                    'is_edited'             : false,
                    'is_custom'             : true,
                };
        
                mappings.push(input);
                render();
            });
            
        }
    }else
    {
        if(!_custom_field_id)
        {
            var input = 
            {
                'custom_field_id'       : custom_field_id,
                'custom_label'          : custom_label,
                'custom_type_id'        : custom_type_id,
                'custom_type_name'      : custom_type_name,
                'custom_lookup_id'      : custom_lookup_id,
                'custom_default_value'  : custom_default_value,
                'is_required'           : is_required,
                'is_edited'             : false,
                'is_custom'             : true,
            };

            mappings.push(input);
            
        }else
        {
            var data                    = mappings[_custom_field_id];
            data.custom_label           = custom_label;
            data.custom_type_id         = custom_type_id;
            data.custom_type_id         = custom_type_id;
            data.custom_type_name       = custom_type_name;
            data.custom_lookup_id       = custom_lookup_id;
            data.custom_default_value   = custom_default_value;
            data.is_required            = is_required;
            enableAllDeleteButton();
            
        }
    }
    
    
    render();
    $('#select_custom_field').val('').trigger('change').removeAttr('disabled');
    $('#select_custom_type').val('').trigger('change');
    $('#_custom_field_id').val('');
    $('#custom_lookupName').val('');
    $('#custom_lookupId').val('');
    $('#custom_label').val('');
    $("#custom_default_value").tagsinput('removeAll');
    $("#is_required").parent().find(".switchery").prop('checked', false).trigger("click");
})

function editModal(url)
{
    $.ajax({
        type: "get",
        url: url
    })
    .done(function (response) {
        $("#url_update_custom_field").val(response.url_update);
        $('#_custom_field_id').val(response.id);
        $("#select_custom_field").val(response.name).trigger('change').attr('disabled', 'disabled');
        $("#select_custom_type").val(response.type).trigger('change');
        $('#custom_label').val(response.label);

        if(response.type == 'lookup')
        {
            $('#custom_lookupName').val(response.value);
            $('#custom_lookupId').val(response.lookup_id);
        }else
        {
            $('#custom_default_value').tagsinput('removeAll');
            $('#custom_default_value').tagsinput('add', response.value);
        }
        

        
        if(response.required) $("#is_required").parent().find(".switchery").prop('checked', true).trigger("click");
        else $("#is_required").parent().find(".switchery").prop('checked', false).trigger("click");
    });
}

function hapusModal(url)
{
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    $.ajax({
        type: "post",
        url: url,
        beforeSend: function () {
            mappings = [];
        },
        success: function (response) {
            var permissions = response;
            for (idx in permissions) 
            {
                var data    = permissions[idx];
                var input   = 
                {
                    'id'    : data.id,
                    'name'  : data.display_name
                };
                mappings.push(input);
            }
            
            
        }
    })
    .done(function () {
        $('#roleTable').DataTable().ajax.reload();
        render();
    });
}

function render() 
{
    getIndex();
    $('#mappings').val(JSON.stringify(mappings));

    var tmpl = $('#asset_field_custom_table').html();
    Mustache.parse(tmpl);
    var data = { item: mappings };
    var html = Mustache.render(tmpl, data);
     $('#asset_custom_field').html(html);
    bind();
}

function bind() 
{
    $('.btn-edit-item').on('click', editItem);
    $('.btn-delete-item').on('click', deleteItem);
}

function getIndex() 
{
    for (idx in mappings) 
    {
        mappings[idx]['_id']    = idx;
        mappings[idx]['no']     = parseInt(idx) + 1;
    }
}

function deleteItem() 
{
    var i = $(this).data('id');

    mappings.splice(i, 1);
    render();
}

function editItem() 
{
    var i = $(this).data('id');
    var data = mappings[i];
    $('#_custom_field_id').val(i);
    $('#select_custom_field').val(data.custom_field_id).trigger('change').attr('disabled', 'disabled');
    $('#select_custom_type').val(data.custom_type_id).trigger('change');
    $('#custom_label').val(data.custom_label);
    $('#custom_default_value').tagsinput('removeAll');
    $('#custom_default_value').tagsinput('add', data.custom_default_value);
    $('#custom_lookupName').val(data.custom_default_value);
    $('#custom_lookupId').val(data.custom_lookup_id);

    if(data.is_required) $("#is_required").parent().find(".switchery").prop('checked', true).trigger("click");
    else $("#is_required").parent().find(".switchery").prop('checked', false).trigger("click");
    
    disabledAllDeleteButton()
    render();
}

function disabledAllDeleteButton()
{
    for (var i in mappings) 
    {
        var data = mappings[i];
        data.is_edited = true;
    }
}

function enableAllDeleteButton()
{
    for (var i in mappings) 
    {
        var data = mappings[i];
        data.is_edited = false;
    }
}

function checkItem(id) 
{
    for (var i in mappings) 
    {
        var data = mappings[i];
        if (data.custom_field_id == id)
            return false;
    }

    return true;
}

$('#custom_lookupButtonLookup').on('click',function()
{
    var select_custom_type  = $('#select_custom_type').val();
    var sub_department      = $('#select_sub_department').val();
    var asset_type_id       = $('#asset_type_id').val();
    var is_consumable       = $('#checkbox_is_consumable').is(":checked");

    if(!sub_department)
    {
        $("#alert_warning").trigger("click", 'Please select subdepartment first');
        return false;
    }

    if(is_consumable == true)
    {
        $("#alert_warning").trigger("click", 'Function is available only when asset type is non consumable');
        return false;
    }

    if(select_custom_type != 'lookup')
    {
        $("#alert_warning").trigger("click", 'Function is available only when custom type is lookup');
        return false;
    }

    $('#custom_lookupModal').modal();
    lov('custom_lookup',department,asset_type_id,'/setting/asset-type/list-of-consumable-asset-type?');
});

function lov(name,arg1,arg2, url) 
{
    var search      = '#' + name + 'Search';
    var item_id     = '#' + name + 'Id';
    var item_name   = '#' + name + 'Name';
    var modal       = '#' + name + 'Modal';
    var table       = '#' + name + 'Table';
    var buttonSrc   = '#' + name + 'ButtonSrc';
    var buttonDel   = '#' + name + 'ButtonDel';

    function itemAjax()
    {
        var q = $(search).val();
        $(table).addClass('hidden');
        $(modal).find('.shade-screen').removeClass('hidden');
        $(modal).find('.form-search').addClass('hidden');

        $.ajax({
            url: url + '&q=' + q+ '&department_id=' + arg1+'&asset_type_id='+arg2
        })
        .done(function (data) 
        {
            $(table).html(data);
            pagination(name);
            $(search).focus();
            $(table).removeClass('hidden');
            $(modal).find('.shade-screen').addClass('hidden');
            $(modal).find('.form-search').removeClass('hidden');

            $(table).find('.btn-choose').on('click', chooseItem);
        });
    }

    function chooseItem()
    {
        var id    = $(this).data('id');
        var name  = $(this).data('name');
        var obj   = $(this).data('obj');
       
        $(item_id).val(id);
        $(item_name).val(name);

        if (!obj || obj.length == 0) obj = [{}];
    }

    function pagination()
    {
        $(modal).find('.pagination a').on('click', function (e) 
        {
            var params = $(this).attr('href').split('?')[1];
            url = $(this).attr('href') + (params == undefined ? '?' : '');

            e.preventDefault();
            itemAjax();
        });
    }

    $(buttonSrc).unbind();
    $(search).unbind();

    $(buttonSrc).on('click', itemAjax);

    $(search).on('keypress', function (e) 
    {
        if (e.keyCode == 13)
            itemAjax();
    });

    $(buttonDel).on('click', function () 
    {
        $(item_id).val('');
        $(item_name).val('');

    });

    itemAjax();
}

function checkLookupExists(custom_lookup_id)
{
    var flag = 0;
    for (idx in mappings) 
    {
        var data = mappings[idx];
       
        if(data.custom_lookup_id == custom_lookup_id) flag++;
    }

    return flag;
}