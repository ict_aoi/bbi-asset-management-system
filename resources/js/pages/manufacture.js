$(document).ready( function ()
{
    var msg = $('#msg').val();
    var is_super_admin = $('#is_super_admin').val();

    if (msg == 'success') $("#alert_success").trigger("click", 'Data successfully saved');
    else if (msg == 'success_2') $("#alert_success").trigger("click", 'Data successfully updated');

    if(is_super_admin == '1')
    {
        var manufactureTable = $('#manufactureTable').DataTable({
            dom: 'Bfrtip',
            processing: true,
            serverSide: true,
            pageLength:10,
            scrollY:250,
            paging: true, 
            scrollX:true,
            deferRender:true,
            ajax: {
                type: 'GET',
                url: '/setting/manufacture/data',
                data: function(d) {
                    return $.extend({}, d, {
                        "sub_department"      : $('#select_sub_department').val()
                    });
               }
            },
            fnCreatedRow: function (row, data, index) {
                var info = manufactureTable.page.info();
                var value = index+1+info.start+data.action;
                $('td', row).eq(0).html(value);
            },
            columns: [
                {data: null, sortable: false, orderable: false, searchable: false},
                {data: 'sub_department_name', name: 'sub_department_name',searchable:true,visible:true,orderable:true},
                {data: 'name', name: 'name',searchable:true,visible:true,orderable:true},
                {data: 'description', name: 'description',searchable:true,orderable:true},
            ],
    
        });
    }else
    {
        var manufactureTable = $('#manufactureTable').DataTable({
            dom: 'Bfrtip',
            processing: true,
            serverSide: true,
            pageLength:10,
            scrollY:250,
            paging: true, 
            scrollX:true,
            deferRender:true,
            ajax: {
                type: 'GET',
                url: '/setting/manufacture/data',
                data: function(d) {
                    return $.extend({}, d, {
                        "sub_department"      : $('#select_sub_department').val()
                    });
               }
            },
            fnCreatedRow: function (row, data, index) {
                var info = manufactureTable.page.info();
                var value = index+1+info.start+data.action;
                $('td', row).eq(0).html(value);
            },
            columns: [
                {data: null, sortable: false, orderable: false, searchable: false},
                {data: 'name', name: 'name',searchable:true,visible:true,orderable:true},
                {data: 'description', name: 'description',searchable:true,orderable:true},
            ],
    
        });
    }

    var dtable = $('#manufactureTable').dataTable().api();
    $("#manufactureTable.dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
    });
    dtable.draw();

    $('#select_sub_department').trigger('change');
    $('#select_sub_department').on('change',function()
    {
        dtable.draw();
    });
    
    $('#form').submit(function (event)
    {
        event.preventDefault();
        var sub_department  = $('#select_sub_department').val();
        var name            = $('#name').val();

        var current_time    = $('#time').text();
        $('#current_time').val(current_time);

        if (!sub_department) 
        {
            $("#alert_warning").trigger("click", 'Please select sub departement first');
            return false
        }

        if (!name) 
        {
            $("#alert_warning").trigger("click", 'Please type name first');
            return false
        }

        bootbox.confirm("Are you sure want to save this data ?.", function (result) {
            if(result){
                $.ajax({
                    type: "POST",
                    url: $('#form').attr('action'),
                    data: $('#form').serialize(),
                    beforeSend: function () {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function () {
                        document.location.href = '/setting/manufacture';
                    },
                    error: function (response) {
                        $.unblockUI();

                        if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON.message);
                    }
                });
            }
        });
    });

});

function hapus(url)
{
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: "put",
        url: url,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function (response) {
            $.unblockUI();
        },
        error: function (response) {
            $.unblockUI();
        }
    }).done(function ($result) {
        $('#manufactureTable').DataTable().ajax.reload();
        $("#alert_success").trigger("click", 'Data successfully deleted');
    });
}