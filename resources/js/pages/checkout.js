$(document).ready(function()
{
    $('input.input-date').datepicker({
        format: "dd/mm/yyyy",
        autoclose: true,
        todayHighlight: true
    });
    
    list_assets         = JSON.parse($('#assets').val());
    list_factorys_out   = JSON.parse($('#factories_out').val());
    
    $('#form').on('keyup keypress', function(e) 
    {
        var keyCode = e.keyCode || e.which;
        if (keyCode == 13) 
        { 
            e.preventDefault();
            return false;
        }
    });
    
    $('#form').submit(function (event)
    {
        event.preventDefault();

        var last_factory                = $('#select_last_factory').val();
        var origin_subdepartment_id     = $('#select_origin_subdepartment').val();
        var nik                         = $('#txt_nik').val();
        var employee_name               = $('#txt_name').val();
        var employee_department         = $('#txt_department').val();
        var employee_subdepartment      = $('#txt_sub_department').val();
        var checkout_to_status          = $('#checkout_to_status').val();
        var status                      = $('#select_status').val();
        var new_oth_destination         = $('#new_oth_destination').val();
        var destination_factory         = $('#select_destination_factory').val();
        var destination_factory_name    = $('#select_destination_factory').select2('data')[0].text; 
        var destination_sub_department  = $('#select_destination_subdepartment').val();
        var destination_area            = $('#destination_area').val();
        var is_handover                 = $('#is_handover').val();
        var select_no_kk                = $('#select_no_kk').val();
        var new_no_kk                   = $('#new_no_kk').val();
        var current_time                = $('#time').text();
        
        $('#current_time').val(current_time);

        if(last_factory == '')
        {
            $("#alert_warning").trigger("click", 'Please select factory first');
            setFocusToTextBox();
            return false;
        }

        if(origin_subdepartment_id == '')
        {
            $("#alert_warning").trigger("click", 'Please select subdepartment first');
            setFocusToTextBox();
            return false;
        }

        if(list_assets.length == '0')
        {
            $("#alert_warning").trigger("click", 'Please scan / select asset first');
            setFocusToTextBox();
            return false;
        }

        if(checkout_to_status == 1)
        {
            if(nik == '')
            {
                $("#alert_warning").trigger("click", 'Please type NIK first');
                return false;
            }

            if(employee_name == '')
            {
                $("#alert_warning").trigger("click", 'Employe name not found');
                return false;
            }

            if(employee_department == '')
            {
                $("#alert_warning").trigger("click", 'Destination department not found');
                return false;
            }

            if(employee_subdepartment == '')
            {
                $("#alert_warning").trigger("click", 'Destination Subdepartment not found');
                return false;
            }
        }else
        {
            if(destination_factory == '')
            {
                $("#alert_warning").trigger("click", 'Please select destination factory first');
                return false;
            }

            if(destination_factory_name.toLowerCase() == 'other' && new_oth_destination == '')
            {
                $("#alert_warning").trigger("click", 'Please input other destination first');
                return false;
            }

            if(destination_sub_department == '')
            {
                $("#alert_warning").trigger("click", 'Please select destination subdepartment first');
                return false;
            }

            if(destination_area == '' && status == 'digunakan')
            {
                $("#alert_warning").trigger("click", 'Please select destination area first');
                return false;
            }

            if(status == '')
            {
                $("#alert_warning").trigger("click", 'Please select status first');
                return false;
            }

            if(is_handover == 1 && status != 'dikembalikan')
            {
                if(select_no_kk == '-1')
                {
                    if(!new_no_kk)
                    {
                        $("#alert_warning").trigger("click", 'Please type New No KK first');
                        return false;
                    }
                    
                }else
                {
                    if(!select_no_kk)
                    {
                        $("#alert_warning").trigger("click", 'Please select no kk first');
                        return false;
                    }
                   
                }

                if(status =='dipinjam' && select_no_kk == '-1')
                {
                    var start_rent_date = $('#start_rent_date').val();
                    var end_rent_date   = $('#end_rent_date').val();

                    if(!start_rent_date)
                    {
                        $("#alert_warning").trigger("click", 'Please select start rent first');
                        return false;
                    }

                    if(!end_rent_date)
                    {
                        $("#alert_warning").trigger("click", 'Please select end rent first');
                        return false;
                    }

                    if(start_rent_date && end_rent_date)
                    {
                        var d1 = start_rent_date.split("/");
                        var d2 = end_rent_date.split("/");
            
                        var from = new Date(d1[2], parseInt(d1[1])-1, d1[0]);  // -1 because months are from 0 to 11
                        var to   = new Date(d2[2], parseInt(d2[1])-1, d2[0]);
            
                        if(from > to)
                        {
                            $("#alert_warning").trigger("click", 'end rent date cannot be small than start rent date');
                            return false
                        }
                       
                    }
                }
            }
        }
        
        $('#confirmationInsertModal').modal('hide');
        bootbox.confirm("Are you sure want to save this data ?.", function (result) {
            if(result){
                $.ajax({
                    type: 'post',
                    url: $('#form').attr('action'),
                    data: $('#form').serialize(),
                    beforeSend: function () {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function () {
                        $("#alert_success").trigger("click",'Checkout successfully');
                        document.location.href = '/check-out';
                    },
                    error: function (response) {
                        $.unblockUI();
                        if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON.message);
                    
                    }
                });
            }
        });
    });

    $('#btn_user').trigger('click');
    $('#select_last_factory').trigger('change'); // filter buat get asset
    $('#select_origin_subdepartment').trigger('change'); // filter buat get asset
    $('#select_status').val('').trigger('change');  // filter buat get asset
    render();
});

$('#btn_confirmation').on('click',function() 
{
    var department_id   = $('#select_origin_subdepartment').val();
    var factory_id      = $('#select_last_factory').val();

    if(department_id && factory_id) $('#confirmationInsertModal').modal();
    else 
    {
        $("#alert_warning").trigger("click", 'Please select factory and subdepartment first');
        return false;
    }
});

$('#btn_user').on('click',function() 
{
    $('#nik').removeClass("hidden");
    $('#location').addClass("hidden");
    $('#location').trigger("reset");
    $('#checkout_to_status').val('1').trigger('change');;
});

$('#btn_location').on('click',function() 
{
    $('#nik').addClass("hidden");
    $('.information').addClass("hidden");
    $('#location').removeClass("hidden");
    $('#txt_name').val('');
    $('#txt_department').val('');
    $('#txt_sub_department').val('');
    $('#txt_nik').val('');
    $('#checkout_to_status').val('2').trigger('change');
});

$('#checkout_to_status').on('change',function()
{
    var value = $(this).val();
    if(value == 1)
    {
        $('#select_destination_factory').val('').trigger('change');
        $('#select_destination_area').val('').trigger('change');
    }
    if(value == 2)
    {
        var area_factory_id = $('#area_factory_id').val();
        var area_name         = $('#area_name').val();

        $('#select_destination_factory').val(area_factory_id).trigger('change');
        $('#select_destination_area').val(area_name).trigger('change');
    }
});

$('#area_name').on('change',function()
{
    var value = $('#checkout_to_status').val();
    if(value == 1)
    {
        $('#select_destination_factory').val('').trigger('change');
        $('#select_destination_area').val('').trigger('change');
    }
    if(value == 2)
    {
        var area_factory_id = $('#area_factory_id').val();
        var area_name         = $(this).val();

        $('#select_destination_factory').val(area_factory_id).trigger('change');
        $('#select_destination_area').val(area_name).trigger('change');
    }
});

$('#select_no_kk').on('change',function () 
{
    var value   = $(this).val();    
    if(value == '-1')
    {
        $('#new_no_kk').attr('readonly',false);
    }else
    {
        $('#new_no_kk').attr('readonly',true);
        $('#new_no_kk').val('');
    }
});

$('#select_status').on('change',function () 
{
    var status                  = $(this).val();  
    var last_factory_id         = $('#select_last_factory').val();   
    
    if(status) 
    {
        if(status == 'dipinjam') $('#panel_borrow').removeClass('hidden');
        else $('#panel_borrow').addClass('hidden');
        
        if(status ==  'dikembalikan' || status ==  'digunakan' || status ==  'rusak' || status ==  'obsolete') $('#kk_panel').addClass('hidden');
        else $('#kk_panel').removeClass('hidden');
    
        if(status == 'digunakan' || status == 'obsolete' || status == 'rusak')
        {
            $("#select_destination_factory").empty();
            $("#select_destination_subdepartment").empty();
            $.each(list_factorys_out,function(id,name)
            {
                if(id == last_factory_id) $("#select_destination_factory").append('<option value="'+id+'" selected>'+name+'</option>');
            });
           
            $('#select_destination_factory').trigger('change');

            if(status == 'rusak') $('#select_destination_subdepartment').trigger('change');
            $("#select_no_kk").empty();
            $('#is_handover').val('');

        }else
        {
            $("#select_destination_factory").empty();
            $("#select_destination_subdepartment").empty();
            $("#select_destination_factory").append('<option value="">-- Select Destination Factory --</option>');
            $.each(list_factorys_out,function(id,name)
            {
                if(id != last_factory_id) $("#select_destination_factory").append('<option value="'+id+'">'+name+'</option>');
            });

            $('#is_handover').val('1');

            if(status == 'dipindah tangan' || status == 'dipinjam')
            {
                $('#panel_kk').removeClass('hidden');
                $("#select_no_kk").empty();
                $('#select_destination_subdepartment').trigger('change');

            }else 
            {
                $('#panel_kk').addClass('hidden');
                $("#select_no_kk").empty();
                $("#new_no_kk").val('');
                $("#start_rent_date").val('');
                $("#end_rent_date").val('');
            }
        }
    }else
    {
        $("#select_no_kk").empty();
        $("#select_destination_factory").empty();
        $("#select_destination_subdepartment").empty();
        $("#select_destination_area").empty();

        $("#select_destination_factory").append('<option value="">-- Select Destination Factory --</option>');
        $("#select_destination_subdepartment").append('<option value="">-- Select Destination Subdepartment --</option>');
        $("#select_destination_area").append('<option value="">-- Select Destination Area --</option>');

        $('#panel_kk').addClass('hidden');
        $('#panel_borrow').addClass('hidden');
        $('#new_oth_destinationDiv').addClass('hidden');
       
        $("#new_oth_destination").val('');
        $("#new_no_kk").val('');
        $("#start_rent_date").val('');
        $("#end_rent_date").val('');
    } 
});


$('#select_last_factory').on('change',function () 
{
    var last_factory_id         = $(this).val();    
    var origin_subdepartment_id = $('#select_origin_subdepartment').val();    
    
    if(last_factory_id != '')
    {
        $.ajax({
            type: 'get',
            url: '/check-out/absence?factory_id='+last_factory_id
        })
        .done(function(response){
            $('#auto_completes').val(JSON.stringify(response)).trigger('change');
        })

        $('#select_origin_subdepartment').trigger('change');
        $('#selected_last_factory_id').val(last_factory_id);
        $('#selected_origin_subdepartment_id').val(origin_subdepartment_id);
  
    }else
    {
        $("#auto_completes").val(JSON.stringify([])).trigger('change');
    }

    list_assets = [];
    render();
});

$('#select_origin_subdepartment').on('change',function () 
{
    var last_factory_id         = $('#select_last_factory').val();  
    var origin_subdepartment_id = $(this).val();  

    $('#selected_origin_subdepartment_id').val(origin_subdepartment_id);
    $('#selected_last_factory_id').val(last_factory_id);

    list_assets = [];
    render();
});

$('#select_destination_factory').on('change',function () 
{
    var destination_factory_id      = $(this).val();    
    var origin_subdepartment_id     = $('#select_origin_subdepartment').val();    
    var status                      = $('#select_status').val();    
    var area_name                   = $('#area_name').val();    
    var check_out_subdepartment_id  = $('#check_out_subdepartment_id').val();    
    
    
    if(destination_factory_id)
    {
        var destination_factory_name    = $(this).select2('data')[0].text;  

        if(destination_factory_name.toLowerCase() == 'other') $('#new_oth_destinationDiv').removeClass('hidden');
        else $('#new_oth_destinationDiv').addClass('hidden');

        $.ajax({
            type: 'get',
            url: '/check-out/destination-subdepartment',
            data:{
                factory_id              : destination_factory_id,
                origin_subdepartment_id : origin_subdepartment_id,
                status                  : status,
            }
        })
        .done(function(response){
            $("#select_destination_subdepartment").empty();

            if(status == 'digunakan')
            {
               
                $("#select_destination_subdepartment").append('<option value="">-- Select Destination Subdepartment --</option>');
            
                $.each(response,function(id,name)
                {
                    if(check_out_subdepartment_id == id) var selected = 'selected';
                    else  var selected = null;
    
                    $("#select_destination_subdepartment").append('<option value="'+id+'"'+selected+'>'+name+'</option>');
                });
            }else
            {
                $.each(response,function(id,name)
                {
                    var selected = 'selected';
                    $("#select_destination_subdepartment").append('<option value="'+id+'"'+selected+'>'+name+'</option>');
                });
            }

            $('#select_destination_subdepartment').trigger('change');
            
        });
        
        if(status == 'digunakan')
        {
            $.ajax({
                type: 'get',
                url: '/check-out/destination-area',
                data:
                {
                    factory_id  : destination_factory_id,
                    status      : status,
                }
            })
            .done(function(response)
            {
                $("#select_destination_area").empty();
                $("#select_destination_area").append('<option value="">-- Select Destination Area --</option>');
                
                $.each(response,function(id,name)
                {
                    if(area_name == id) var selected = 'selected';
                    else var selected = null;
                    
                    $("#select_destination_area").append('<option value="'+id+'"'+selected+'>'+name+'</option>');
                });
            });
        }else 
        {
            $("#select_destination_area").empty();
        }
    }
});

$('#select_destination_subdepartment').on('change',function () 
{
    var status                          = $('#select_status').val();    
    var last_factory_id                 = $('#select_last_factory').val();    
    var destination_factory_id          = $('#select_destination_factory').val();    
    var origin_subdepartment_id         = $('#select_origin_subdepartment').val();    
    var destination_factory_name        = $('#select_destination_factory').select2('data')[0].text;  
    var destination_subdepartment_id    = $(this).val();    
   
    if(destination_factory_id)
    {
        if(destination_factory_name.toLowerCase() == 'other') $('#new_oth_destinationDiv').removeClass('hidden');
        else $('#new_oth_destinationDiv').addClass('hidden');

    }

    if(destination_factory_id && destination_subdepartment_id && origin_subdepartment_id)
    {
        if(status != 'digunakan' || status != 'dikembalikan')
        {
            $.ajax({
                type: 'get',
                url: '/check-out/list-of-kk?',
                data: {
                    subdepartment_id        : origin_subdepartment_id,
                    factory_id              : last_factory_id,
                    destination_factory_id  : destination_factory_id,
                    status                  : status,
                }
            })
            .done(function(response)
            {
                var rent_agreement = response;
                
                $("#select_no_kk").empty();
                $("#select_no_kk").append('<option value="">-- Select No KK --</option>');
                $("#select_no_kk").append('<option value="-1">- Entry New -</option>');
            
                $.each(rent_agreement,function(id,name){
                    $("#select_no_kk").append('<option value="'+id+'">'+name+'</option>');
                });
            });
        }
    }
});

$('#auto_completes').on('change',function()
{
    available_nik = JSON.parse($('#auto_completes').val());
    $("#txt_nik").autocomplete({
        source: available_nik
    });
});

$('#txt_nik').on('change',function()
{
    var select_factory      = $('#select_last_factory').val();
       
    if(!select_factory)
    {
        $("#alert_warning").trigger("click", 'Please select factory first');
        $(this).val('');
        return false;
    }

    var value     = $(this).val().split(':');
    if(value != '')
    {
        $('.information').removeClass("hidden");
        $('#txt_nik').val(value[0]);
        $('#txt_name').val(value[1]);
        $('#txt_department').val(value[2]);
        $('#txt_sub_department').val(value[3]);
    }else
    {
        $("#alert_warning").trigger("click", 'Data not found');
        $('.information').addClass("hidden");
        $(this).val('');
        $('#txt_nik').val('');
        $('#txt_name').val('');
        $('#txt_department').val('');
        $('#txt_sub_department').val('');
        return false;
    }

    
    
});

function setFocusToTextBox() 
{
	$('.barcode_value').focus();
}

function render() 
{
    getIndex();
    $('#assets').val(JSON.stringify(list_assets));
    var tmpl = $('#asset_out_table').html();
    Mustache.parse(tmpl);
    var data = { item: list_assets };
    var html = Mustache.render(tmpl, data);
    $('#draw').html(html);
    bind();
    setFocusToTextBox();
    $('#total_barcode').text(list_assets.length)
}

function getIndex() 
{
    var is_delete   = $('#is_delete').val();
    var _barcode    = $('#_barcode_value').val();
    var total	    = list_assets.length;

    for (id in list_assets) 
    {
        if(_barcode == list_assets[id]['barcode']) list_assets[id]['_active'] = 'active';
		else
		{
        	if(is_delete == 1)
            {
                if(total == (parseInt(id)+1)) list_assets[id]['_active'] = 'active';
                else list_assets[id]['_active'] = null;
            }
            else list_assets[id]['_active'] = null;
            
		}
        list_assets[id]['_id'] = id;
        list_assets[id]['no'] = parseInt(id) + 1;

        for(idx in list_assets[id].detail_informations)
        {
            list_assets[id].detail_informations[idx]['_idx'] 	= idx;
            list_assets[id].detail_informations[idx]['nox']	    = parseInt(idx) + 1;
        }
    }
}

function bind() 
{
	$('.btn-delete-item').on('click', deleteItem);
	$('.btn-detail-item').on('click', showDetail);
	$('#barcode_value').on('change', addItem);
    $('#asset_button_lookup').on('click', lookupAsset);
    $('.btn-active').on('click', setActive);
}

function setActive()
{
    var barcode       = $(this).data('barcode');
    $('#_barcode_value').val(barcode);
    render();
}

function deleteItem()
{
	var i = $(this).data('id');
    list_assets.splice(i, 1);
    $('#is_delete').val('1');
    $('#_barcode_value').val('');
	render();
}

function showDetail()
{
    var i       = $(this).data('id');
    var asset   = list_assets[i];

    for (var id in asset.detail_informations) 
    {
        var detail = asset.detail_informations[id];
        if(asset.id == detail.id) detail.is_hidden = false;
    }

    $('#detail_information').val(JSON.stringify(asset));
    var tmpl = $('#detail_asset_information_table').html();
    Mustache.parse(tmpl);
    var data = { item: asset };
    var html = Mustache.render(tmpl, data);
    $('#tbody_detail_asset_information').html(html);
	$('#detailInformationModal').modal();
}

function checkExists(barcode) 
{
	for (var i in list_assets) 
	{
        var data = list_assets[i];
        
        if(data.barcode == barcode) return true;
    }
    
    return false;
	
}

function addItem()
{
    var factory 	        = $('#select_last_factory').val();
    var sub_department_name = $('#select_origin_subdepartment').select2('data')[0].text;
	var sub_department_id   = $('#select_origin_subdepartment').val();
	var barcode 	        = $('#barcode_value').val();
	var url 	            = $('#url_get_asset').val();
	var isExists            = checkExists(barcode);

    if(!factory)
    {
        $('#barcode_value').val('');
        $("#alert_warning").trigger("click", 'Please select factory first');
        render();
		return false;
    }

    if(!sub_department_id)
    {
        $('#barcode_value').val('');
	    $("#alert_warning").trigger("click", 'Please select subdepartment first');
        render();
		return false;
    }

	if (isExists) 
	{
        $('#barcode_value').val('');
        $('#_barcode_value').val(barcode);
		$("#alert_warning").trigger("click", 'Barcode already scan');
        render();
		return false;
	}

	$.ajax({
		type: "GET",
		url: url,
		data: {
			barcode             : barcode,
			sub_department_id   : sub_department_id,
			factory             : factory,
		},
		beforeSend: function () {
			$.blockUI({
				message: '<i class="icon-spinner4 spinner"></i>',
				overlayCSS: {
					backgroundColor: '#fff',
					opacity: 0.8,
					cursor: 'wait'
				},
				css: {
					border: 0,
					padding: 0,
					backgroundColor: 'transparent'
				}
			});
		},
		complete: function () {
			$.unblockUI();
		},
		success: function (response) 
		{
            var area    = response.area;
            var asset   = response.asset;

            if(asset)
            {
                list_assets.push(asset);
                $('#_barcode_value').val(asset.barcode);
                $('#is_delete').val('0');
            }
            else 
            {
                $("#alert_success").trigger('click','Area '+area.name+' has been selected');
                $('#area_factory_id').val(area.factory_id);
                $('#area_name').val(area.name.toLowerCase()).trigger('change');

                if(sub_department_name == 'Mekanik')
                {
                    $('#check_out_subdepartment_id').val(sub_department);
                    $('#btn_location').trigger('click');
                }else
                {
                    $('#check_out_subdepartment_id').val('');
                }
            }
		},
		error: function (response) 
		{
			$.unblockUI();
			$('#barcode_value').val('');
			
            if (response['status'] == 500) $("#alert_error").trigger('click','Please contact ICT');
            if (response['status'] == 422) $("#alert_info").trigger('click', response.responseJSON.message);
            
            render();
		}
	})
	.done(function (response)
	{
        render();
        $('#barcode_value').val('');

	});
}

function lookupAsset()
{
    var origin_subdepartment_id = $('#select_origin_subdepartment').val();
    var last_factory_id         = $('#select_last_factory').val();

    if(!last_factory_id)
    {
        $('#barcode_value').val('');
        $("#alert_warning").trigger("click", 'Please select factory first');
        render();
        return;
    }

    if(!origin_subdepartment_id)
    {
        $('#barcode_value').val('');
	    $("#alert_warning").trigger("click", 'Please select subdepartment first');
        render();
		return;
    }

    $('#barcodeModal').modal();
    lov('barcode',origin_subdepartment_id,last_factory_id,'/check-out/list-of-asset?');
}

function lov(name,arg1,arg2, url) 
{
    var search      = '#' + name + 'Search';
    var item_id     = '#' + name + 'Id';
    var item_name   = '#' + name + 'Name';
    var modal       = '#' + name + 'Modal';
    var table       = '#' + name + 'Table';
    var buttonSrc   = '#' + name + 'ButtonSrc';
    var buttonDel   = '#' + name + 'ButtonDel';

    function itemAjax()
    {
        var q = $(search).val();
        $(table).addClass('hidden');
        $(modal).find('.shade-screen').removeClass('hidden');
        $(modal).find('.form-search').addClass('hidden');

        $.ajax({
            url: url + '&q=' + q+ '&sub_department_id=' + arg1+ '&factory_id=' + arg2
        })
        .done(function (data) {
            $(table).html(data);
            pagination(name);
            $(search).focus();
            $(table).removeClass('hidden');
            $(modal).find('.shade-screen').addClass('hidden');
            $(modal).find('.form-search').removeClass('hidden');

            $(table).find('.btn-choose').on('click', chooseItem);
        });
    }

    function chooseItem()
    {
        var id      = $(this).data('id');
        $('#barcode_value').val(id).trigger('change');
    }

    function pagination()
    {
        $(modal).find('.pagination a').on('click', function (e) 
        {
            var params = $(this).attr('href').split('?')[1];
            url = $(this).attr('href') + (params == undefined ? '?' : '');

            e.preventDefault();
            itemAjax();
        });
    }

    $(buttonSrc).unbind();
    $(search).unbind();

    $(buttonSrc).on('click', itemAjax);

    $(search).on('keypress', function (e) 
    {
        if (e.keyCode == 13)
            itemAjax();
    });

    $(buttonDel).on('click', function () 
    {
        $(item_id).val('');
        $(item_name).val('');

    });

    itemAjax();
}


function dismisModal() 
{
	$('#confirmationInsertModal').modal('toggle'); 
	$("#select_status").val('').trigger('change');
	$(".modal-backdrop").remove();
	setFocusToTextBox();
}