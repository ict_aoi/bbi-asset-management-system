list_selected_data 	    = JSON.parse($('#list_selected_data').val());
    
$(function()
{
    var maintenanceTable = $('#bapbTable').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:100,
        deferRender:true,
        scrollY:250,
        paging: true, 
        scrollX:true,
        ajax: {
            type: 'GET',
            url: '/bapb/data',
            data: function(d) {
                 return $.extend({}, d, {
                     "subdepartment"   : $('#select_subdepartment').val(),
                     "asset_type"      : $('#select_asset_type').val(),
                     "factory"         : $('#select_factory').val(),
                 });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = maintenanceTable.page.info();
            var value = index+1+info.start+data.action;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'id', name: 'id',searchable:true,visible:false,orderable:false},
            {data: 'last_company_location_id', name: 'last_company_location_id',searchable:true,visible:false,orderable:false},
            {data: 'last_factory_location_id', name: 'last_factory_location_id',searchable:true,visible:false,orderable:false},
            {data: 'last_area_location', name: 'last_area_location',searchable:true,visible:false,orderable:false},
            {data: 'custom_field_1', name: 'custom_field_1',searchable:true,visible:false,orderable:false},
            {data: 'custom_field_2', name: 'custom_field_2',searchable:true,visible:false,orderable:false},
            {data: 'custom_field_3', name: 'custom_field_3',searchable:true,visible:false,orderable:false},
            {data: 'custom_field_4', name: 'custom_field_4',searchable:true,visible:false,orderable:false},
            {data: 'custom_field_5', name: 'custom_field_5',searchable:true,visible:false,orderable:false},
            {data: 'custom_field_6', name: 'custom_field_6',searchable:true,visible:false,orderable:false},
            {data: 'custom_field_7', name: 'custom_field_7',searchable:true,visible:false,orderable:false},
            {data: 'custom_field_8', name: 'custom_field_8',searchable:true,visible:false,orderable:false},
            {data: 'custom_field_9', name: 'custom_field_9',searchable:true,visible:false,orderable:false},
            {data: 'custom_field_10', name: 'custom_field_10',searchable:true,visible:false,orderable:false},
            {data: 'custom_field_11', name: 'custom_field_11',searchable:true,visible:false,orderable:false},
            {data: 'custom_field_12', name: 'custom_field_12',searchable:true,visible:false,orderable:false},
            {data: 'custom_field_13', name: 'custom_field_13',searchable:true,visible:false,orderable:false},
            {data: 'custom_field_14', name: 'custom_field_14',searchable:true,visible:false,orderable:false},
            {data: 'custom_field_15', name: 'custom_field_15',searchable:true,visible:false,orderable:false},
            {data: 'custom_field_16', name: 'custom_field_16',searchable:true,visible:false,orderable:false},
            {data: 'custom_field_17', name: 'custom_field_17',searchable:true,visible:false,orderable:false},
            {data: 'custom_field_18', name: 'custom_field_18',searchable:true,visible:false,orderable:false},
            {data: 'custom_field_19', name: 'custom_field_19',searchable:true,visible:false,orderable:false},
            {data: 'custom_field_20', name: 'custom_field_20',searchable:true,visible:false,orderable:false},
            {data: 'last_used_by', name: 'last_used_by',searchable:true,visible:false,orderable:false},
            {data: 'last_department_location_id', name: 'last_department_location_id',searchable:true,visible:false,orderable:false},
            {data: 'origin_factory_name', name: 'origin_factory_name',searchable:true,orderable:true},
            {data: 'origin_department_name', name: 'origin_department_name',searchable:true,orderable:true},
            {data: 'no_inventory', name: 'no_inventory',searchable:true,orderable:true},
            {data: 'no_inventory_manual', name: 'no_inventory_manual',searchable:true,orderable:true},
            {data: 'erp_no_asset', name: 'erp_no_asset',searchable:true,orderable:true},
            {data: 'asset_type', name: 'asset_type',searchable:true,orderable:true},
            {data: 'model', name: 'model',searchable:true,orderable:true},
            {data: 'serial_number', name: 'serial_number',searchable:true,orderable:true},
        ]
    });

    var dtable = $('#bapbTable').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
    });
    dtable.draw();

    $('#select_subdepartment').on('change',function()
    {
        dtable.draw();
    });

    $('#select_asset_type').on('change',function()
    {
        dtable.draw();
    });

    $('#select_factory').on('change',function()
    {
        dtable.draw();
    });

    $('#select_factory').trigger('change');
    $('#select_subdepartment').trigger('change');
    render();



    $('#formApprove').submit(function (event) 
    {
        event.preventDefault();
        var bapb_factory        = $('#select_bapb_factory').val();
        var bapb_subdepartment  = $('#select_bapb_subdepartment').val();
        var new_no_ba           = $('#new_no_ba').val();
        var status              = $('#select_status').val();
        var buyer_name          = $('#buyer_name').val();
        var cost_of_goods       = $('#cost_of_goods').val();

        if(!bapb_factory)
        {
            $("#alert_warning").trigger("click", 'Please select bapb factory first');
            return false;
        }

        if(!bapb_subdepartment)
        {
            $("#alert_warning").trigger("click", 'Please select bapb subdepartment first');
            return false;
        }

        if(!new_no_ba)
        {
            $("#alert_warning").trigger("click", 'Please input no ba first');
            return false;
        }
        
        if(status == 'dijual')
        {
            if(!buyer_name)
            {
                $("#alert_warning").trigger("click", 'Please input buyer name first');
                return false;
            }   

            if(!cost_of_goods)
            {
                $("#alert_warning").trigger("click", 'Please input price first');
                return false;
            }   
        }

        $('#confirmationBapbModal').modal('hide');
        bootbox.confirm("Are you sure want to approve this data ?.", function (result) 
        {
            if(result)
            {
                $.ajax({
                    type: 'post',
                    url: $('#formApprove').attr('action'),
                    data: $('#formApprove').serialize(),
                    beforeSend: function () {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function () {
                        $("#alert_success").trigger("click",'Bapb successfully');
                        $('#select_bapb_factory').val('').trigger('change');
                        $('#select_bapb_subdepartment').val('').trigger('change');
                        $('#select_status').val('').trigger('change');
                        $('#formApprove').trigger('reset');
                    },
                    error: function (response) {
                        $.unblockUI();
                        if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON.message);
                        if (response.status == 500) $("#alert_error").trigger("click",'Please contact ICT');

                        $('#confirmationBapbModal').modal();
                    
                    }
                })
                .done(function()
                {
                    $('#bapbTable').DataTable().ajax.reload();
                    list_selected_data = [];
                    render();
                });
            }
        });

    });
});

$('#select_factory').on('change',function () 
{
    var sub_department_id   = $('#select_subdepartment').val();   
    var factory_id          = $(this).val();   

    $.ajax({
        type: 'get',
        url: '/bapb/get-asset-type',
        data: {
            sub_department_id   : sub_department_id,
            factory_id          : factory_id
        },
    })
    .done(function(response)
    {
        var  asset_types    = response.asset_types;
        $("#select_asset_type").empty();
        $("#select_asset_type").append('<option value="">-- Select Asset Type --</option>');

        $.each(asset_types,function(id,name){
            $("#select_asset_type").append('<option value="'+id+'">'+name+'</option>');
        });
    });
});

$('#select_department').on('change',function () 
{
    var factory_id          = $('#select_factory').val();   
    var sub_department_id   = $(this).val();   

    $.ajax({
        type: 'get',
        url: '/bapb/get-asset-type',
        data: {
            sub_department_id   : sub_department_id,
            factory_id          : factory_id
        },
    })
    .done(function(response)
    {
        var  asset_types    = response.asset_types;
        $("#select_asset_type").empty();
        $("#select_asset_type").append('<option value="">-- Select Asset Type --</option>');

        $.each(asset_types,function(id,name){
            $("#select_asset_type").append('<option value="'+id+'">'+name+'</option>');
        });
    });
});

$('#select_status').on('change', function() 
{
    var value = $(this).val();
    if (value) 
    {
        if (value == 'dijual') 
        {
            $('#buyer_name').removeAttr('readonly');
            $('#cost_of_goods').removeAttr('readonly');
        } else 
        {
            $('#buyer_name').attr('readonly', 'readonly');
            $('#cost_of_goods').attr('readonly', 'readonly');
        }
    }
});

function addToCart(id)
{
    var url = $('#url_add_to_cart').val();

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: "post",
        url: url,
        data: {
            id	    : id,
            status	: 'add',
        },
        beforeSend: function () 
        {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function () 
        {
            $.unblockUI();
        },
        error: function (response) 
        {
            $.unblockUI();
            if (response['status'] == 500) $("#alert_error").trigger("click", 'Please contact ICT');
            if (response['status'] == 422) $("#alert_warning").trigger("click", response.responseJSON.message);
            if (response['status'] == 419 || response['status'] == 401) 
            {
                $("#alert_info_2").trigger("click", 'Session expired. You\'ll be take to the login page');
                location.href = "/login"; 
            }
        }
    })
    .done(function (response) 
    {
        if(response)
        {
            $("#alert_success").trigger("click", 'Success to add cart');
            $('#bapbTable').DataTable().ajax.reload();
            var input = {
                'id'                        : response.id,
                'no_inventory'              : response.no_inventory,
                'model'                     : response.model,
                'barcode'                   : response.barcode,
                'serial_number'             : response.serial_number,
                'last_factory_id'           : response.last_factory_id,
                'last_factory_name'         : response.last_factory_name,
                'last_sub_department_id'    : response.last_sub_department_id,
                'last_sub_department_name'  : response.last_sub_department_name,
            };
            list_selected_data.push(input);
            render();

        }
    });
}

function render()
{
    getIndex();
    $('#list_selected_data').val(JSON.stringify(list_selected_data));
    var tmpl = $('#table_cart').html();
    Mustache.parse(tmpl);
    var data = { item: list_selected_data };
    var html = Mustache.render(tmpl, data);
    $('#list_bapb').html(html);
    bind();
    
    if(list_selected_data.length > 0) $('#total_cart').removeClass('hidden');
    else $('#total_cart').addClass('hidden');

    $('#total_cart').text(list_selected_data.length);
}


function getIndex() 
{
    for (id in list_selected_data) 
    {
        list_selected_data[id]['_id'] = id;
        list_selected_data[id]['no'] = parseInt(id) + 1;
    }
}

function searchIndex(_id)
{
    for (id in list_selected_data) 
    {
        var data = list_selected_data[id];
        if(data.id == _id) return data._id;
    }
}
function bind()
{
    $('.btn-delete-cart').on('click', deleteCart);
}

function removeFromCart(id)
{
    var url = $('#url_add_to_cart').val();
    var i   = searchIndex(id);

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: "post",
        url: url,
        data: {
            id	    : id,
            status	: 'remove',
        },
        beforeSend: function () 
        {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function () 
        {
            $.unblockUI();
        },
        error: function (response) 
        {
            $.unblockUI();
            if (response['status'] == 500) $("#alert_error").trigger("click", 'Please contact ICT');
            if (response['status'] == 422) $("#alert_warning").trigger("click", response.responseJSON.message);
            if (response['status'] == 419 || response['status'] == 401) 
            {
                $("#alert_info_2").trigger("click", 'Session expired. You\'ll be take to the login page');
                location.href = "/login"; 
            }
        }
    })
    .done(function () {

        $("#alert_success").trigger("click", 'Success remove from cart');
        $('#bapbTable').DataTable().ajax.reload();
        list_selected_data.splice(i, 1);
        render();
    });
}

function deleteCart()
{
    var i = $(this).data('id');
    var data = list_selected_data[i];

    var url = $('#url_add_to_cart').val();

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: "post",
        url: url,
        data: {
            id	    : data.id,
            status	: 'remove',
        },
        beforeSend: function () 
        {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function () 
        {
            $.unblockUI();
        },
        error: function (response) 
        {
            $.unblockUI();
            if (response['status'] == 500) $("#alert_error").trigger("click", 'Please contact ICT');
            if (response['status'] == 422) $("#alert_warning").trigger("click", response.responseJSON.message);
            if (response['status'] == 419 || response['status'] == 401) 
            {
                $("#alert_info_2").trigger("click", 'Session expired. You\'ll be take to the login page');
                location.href = "/login"; 
            }
        }
    })
    .done(function () {

        $("#alert_success").trigger("click", 'Success remove from cart');
        $('#bapbTable').DataTable().ajax.reload();
        list_selected_data.splice(i, 1);
        render();
    });
    
}

function cancelObsolete(url)
{

	swal({
		title: "Are you sure want to cancel this data ?",
		text: "Reason:",
		type: "input",
		showCancelButton: true,
		confirmButtonColor: "#2196F3",
		closeOnConfirm: false,
		animation: "slide-from-top",
		inputPlaceholder: "Please type reason first"
	},

	function (inputValue) 
	{
		if (inputValue === false) return false;
		if (inputValue === "") {
			swal.showInputError("Please type reason first");
			return false
		}
		
		
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});

		$.ajax({
			type: "put",
			url: url,
			data: {
				cancel_reason	: inputValue,
				current_time	: $('#time').text()
			},
			beforeSend: function () {
				swal.close();
				$.blockUI({
					message: '<i class="icon-spinner4 spinner"></i>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: 0,
						backgroundColor: 'transparent'
					}
				});
			},
            success: function () 
            {
				$.unblockUI();
			},
			error: function (response) {
				$.unblockUI();
				if (response['status'] == 500) $("#alert_error").trigger("click", 'Please contact ICT');
				if (response['status'] == 422) $("#alert_warning").trigger("click", response.responseJSON.message);
				if (response['status'] == 419 || response['status'] == 401) 
				{
					$("#alert_info_2").trigger("click", 'Session expired. You\'ll be take to the login page');
					location.href = "/login"; 
				}
				$("#select_filter_status").trigger('change');
			}
		})
		.done(function () {
			$("#alert_success").trigger("click", 'Cancel has successfully');
			$('#bapbTable').DataTable().ajax.reload();
		});
	});
}

