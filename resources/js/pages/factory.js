$(document).ready( function ()
{ 
    var msg = $('#msg').val();
    
    if (msg == 'success') $("#alert_success").trigger("click", 'Data successfully saved');
    else if (msg == 'success_2') $("#alert_success").trigger("click", 'Data successfully updated');
    
    var factoryTable = $('#factoryTable').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:100,
        deferRender:true,
        ajax: {
            type: 'GET',
            url: '/organization/factory/data',
        },
        fnCreatedRow: function (row, data, index) {
            var info = factoryTable.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'erp_accounting_location_id', name: 'erp_accounting_location_id',searchable:true,visible:true,orderable:true},
            {data: 'company_name', name: 'company_name',searchable:true,visible:true,orderable:true},
            {data: 'code', name: 'code',searchable:true,visible:true,orderable:true},
            {data: 'name', name: 'name',searchable:true,visible:true,orderable:true},
            {data: 'description', name: 'description',searchable:true,orderable:true},
            {data: 'action', name: 'action',searchable:false,orderable:false},
        ],

    });
    
    var dtable = $('#factoryTable').dataTable().api();
    $("#factoryTable.dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
    });
    dtable.draw();

    $('#form').submit(function (event){
        event.preventDefault();
        var code                            = $('#code').val();
        var name                            = $('#name').val();
        var select_company                  = $('#select_company').val();
        var select_erp_accounting_location  = $('#select_erp_accounting_location').val();
        
        if(!select_company)
        {
            $("#alert_warning").trigger("click", 'Please select factory first');
            return false;
        }

        if(!select_erp_accounting_location)
        {
            $("#alert_warning").trigger("click", 'Please select accounting location erp first');
            return false;
        }

        if(!code)
        {
            $("#alert_warning").trigger("click", 'Please type code first');
            return false;
        }

        if(!name)
        {
            $("#alert_warning").trigger("click", 'Please type name first');
            return false;
        }
        
        
        bootbox.confirm("Are you sure want to save this data ?.", function (result) {
            if(result){
                $.ajax({
                    type: "POST",
                    url: $('#form').attr('action'),
                    data: $('#form').serialize(),
                    beforeSend: function () {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function () {
                        document.location.href = '/organization/factory';
                    },
                    error: function (response) {
                        $.unblockUI();
                        
                        if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON.message);
                    }
                });
            }
        });
    });   
});

function hapus(url)
{
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    $.ajax({
        type: "put",
        url: url,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function (response) {
            $.unblockUI();
        },
        error: function (response) {
            $.unblockUI();
        }
    }).done(function ($result) {
        $('#factoryTable').DataTable().ajax.reload();
        $("#alert_success").trigger("click", 'Data successfully deleted');
    });
}