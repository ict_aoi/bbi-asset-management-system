$(document).ready( function ()
{ 
    var msg = $('#msg').val();
    if (msg == 'success') $("#alert_success").trigger("click", 'Data successfully saved.');
    else if (msg == 'success_2') $("#alert_success").trigger("click", 'Data successfully updated.');

    var areaTable = $('#areaTable').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:10,
        scrollY:250,
        paging: true, 
        scrollX:true,
        deferRender:true,
        ajax: {
            type: 'GET',
            url: '/area/data',
            data: function(d) {
                return $.extend({}, d, {
                    "area_location"         : $('#select_area_location').val(),
                });
            }
        },
        fnCreatedRow: function (row, data, index) {
            var info = areaTable.page.info();
            var value = index+1+info.start+data.action;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'factory_name', name: 'factory_name',searchable:true,visible:true,orderable:true},
            {data: 'name', name: 'name',searchable:true,visible:true,orderable:true},
            {data: 'description', name: 'description',searchable:true,orderable:true},
            {data: 'is_area_stock', name: 'is_area_stock',searchable:false,orderable:false},
        ],

    });

    var dtable = $('#areaTable').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
    });
    dtable.draw();

    $('#select_area_location').on('change',function()
    {
        dtable.draw();
    });

    $('#form').submit(function (event){
        event.preventDefault();
        var name           = $('#name').val();
        var select_factory = $('#select_factory').val();
        
        if(!select_factory)
        {
            $("#alert_warning").trigger("click", 'Factory Name wajib diisi');
            return false;
        }
       

        if(!name)
        {
            $("#alert_warning").trigger("click", 'Nama wajib diisi');
            return false;
        }
        
        bootbox.confirm("Apakah anda yakin akan menyimpan data ini ?.", function (result) {
            if(result){
                $.ajax({
                    type: "POST",
                    url: $('#form').attr('action'),
                    data: $('#form').serialize(),
                    beforeSend: function () {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function () {
                        document.location.href = '/area';
                    },
                    error: function (response) {
                        $.unblockUI();
                        
                        if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON.message);
                        
                    }
                });
            }
        });
    });

    var is_stock_area_chacked = $('#is_stock_area_chacked').val();
    if(is_stock_area_chacked) $("#checkbox_is_area_stock").parent().find(".switchery").prop('checked', true).trigger("click");
    else $("#checkbox_is_area_stock").parent().find(".switchery").prop('checked', false).trigger("click");
});

function hapus(url)
{
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    $.ajax({
        type: "put",
        url: url,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function () {
            $.unblockUI();
        },
        error: function () {
            $.unblockUI();
        }
    }).done(function () {
        $('#areaTable').DataTable().ajax.reload();
        $("#alert_success").trigger("click", 'Data successfully deleted');
    });
}