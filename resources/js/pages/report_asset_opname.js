$(function()
{
    $('input.input-date').datepicker({
        format: "dd/mm/yyyy",
        autoclose: true,
		todayHighlight: true
    });
    
    var page = $('#page').val();
    if(page == 'index')
    {
        $('#btn_export').on('click',function()
        {
            var sub_department  = $('#select_sub_department').val();
            var factory         = $('#select_factory').val();

            if(!factory)
            {
                $("#alert_warning").trigger("click", 'Please select factory first');
                return false;
            }

            if(!sub_department)
            {
                $("#alert_warning").trigger("click", 'Please select subdepartment first');
                return false;
            }

            $('#_sub_department').val(sub_department);
            $('#_factory').val(factory);
            $('#form').trigger('submit')
        });
    }
});