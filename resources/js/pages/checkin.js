$(document).ready(function()
{
    $('input.input-date').datepicker({
        format: "dd/mm/yyyy",
        autoclose: true,
        todayHighlight: true
    });
    
    list_assets = JSON.parse($('#assets').val());
    
    $('#form').on('keyup keypress', function(e) 
    {
        var keyCode = e.keyCode || e.which;
        if (keyCode == 13) 
        { 
            e.preventDefault();
            return false;
        }
    });

    $('#form').submit(function (event)
    {
        event.preventDefault();

        var factory         = $('#select_factory').val();
        var sub_department  = $('#select_sub_department').val();
         var current_time    = $('#time').text();
        $('#current_time').val(current_time);
        
        if(factory == '')
        {
            $("#alert_warning").trigger("click", 'Please select factory first');
            setFocusToTextBox();
            return false;
        }

        if(sub_department == '')
        {
            $("#alert_warning").trigger("click", 'Please select subdepartment first');
            setFocusToTextBox();
            return false;
        }

        if(list_assets.length == '')
        {
            $("#alert_warning").trigger("click", 'Please scan asset first');
            setFocusToTextBox();
            return false;
        }

        $('#confirmationInsertModal').modal('hide');
        bootbox.confirm("Are you sure want to save this data ?.", function (result) 
        {
            if(result)
            {
                $.ajax({
                    type: 'post',
                    url: $('#form').attr('action'),
                    data: $('#form').serialize(),
                    beforeSend: function () {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function () {
                        $("#alert_success").trigger("click",'Checkin successfully');
                        document.location.href = '/check-in';
                    },
                    error: function (response) {
                        $.unblockUI();
                        if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON.message);
                    
                    }
                });
            }
        });
    });

    $('#select_factory').trigger('change');
    $('#select_sub_department').trigger('change');
    render();
});


$('#select_factory').on('change',function (event,arg1) 
{
    var value       = $(this).val();
    var area_name   = $('#area_name').val(); 

    $.ajax({
        type: 'get',
        url: '/check-in/area-stock?factory_id='+value
    })
    .done(function(response)
    {
        $("#select_checkin_area_location").empty();
        $("#select_checkin_area_location").append('<option value="">-- Select Stock Area --</option>');
        
        $.each(response,function(id,name)
        {
            if(area_name == id) var selected = 'selected';
            else var selected = null;
            
            $("#select_checkin_area_location").append('<option value="'+id+'"'+selected+'>'+name+'</option>');
        });
    });

    $('#factory_id').val(value);
});


$('#select_sub_department').on('change',function (event,arg1) 
{
    var value = $(this).val();    
    $('#sub_department_id').val(value);
});

$('#area_name').on('change',function()
{
    var area_name         = $(this).val();
    $('#select_checkin_area_location').val(area_name).trigger('change');
});

$('#btn_confirmation').on('click',function() 
{
    $('#confirmationInsertModal').modal();
});

function render() 
{
    getIndex();
    $('#assets').val(JSON.stringify(list_assets));
    var tmpl = $('#asset_in_table').html();
    Mustache.parse(tmpl);
    var data = { item: list_assets };
    var html = Mustache.render(tmpl, data);
    $('#draw').html(html);
    bind();
    setFocusToTextBox();
    $('#total_barcode').text(list_assets.length);
}

function setFocusToTextBox() 
{
	$('#barcode_value').focus();
}


function getIndex() 
{
    var is_delete   = $('#is_delete').val();
    var _barcode    = $('#_barcode_value').val();
    var total	    = list_assets.length;

    for (id in list_assets) 
    {
        if(_barcode == list_assets[id]['barcode']) list_assets[id]['_active'] = 'active';
		else
		{
        	if(is_delete == 1)
            {
                if(total == (parseInt(id)+1)) list_assets[id]['_active'] = 'active';
                else list_assets[id]['_active'] = null;
            }
            else list_assets[id]['_active'] = null;
            
		}
        list_assets[id]['_id'] = id;
        list_assets[id]['no'] = parseInt(id) + 1;

        for(idx in list_assets[id].detail_informations)
        {
            list_assets[id].detail_informations[idx]['_idx'] 	= idx;
            list_assets[id].detail_informations[idx]['nox']	    = parseInt(idx) + 1;
        }
    }
}

function bind() 
{
	$('.btn-delete-item').on('click', deleteItem);
	$('.btn-detail-item').on('click', showDetail);
	$('#barcode_value').on('change', addItem);
    $('#asset_button_lookup').on('click', lookupAsset);
    $('.btn-active').on('click', setActive);
}

function setActive()
{
    var barcode       = $(this).data('barcode');
    $('#_barcode_value').val(barcode);
    render();
}

function deleteItem()
{
	var i = $(this).data('id');
	list_assets.splice(i, 1);
    $('#is_delete').val('1');
    $('#_barcode_value').val('');
	render();
}

function showDetail()
{
    var i       = $(this).data('id');
    var asset   = list_assets[i];

    if(asset.flag == 1)
    {
        $("#alert_warning").trigger("click", 'Please receive this asset first');
        return false;
    }else
    {
        for (var id in asset.detail_informations) 
        {
            var detail = asset.detail_informations[id];
            if(asset.id == detail.id) detail.is_hidden = false;
        }
    
        $('#detail_information').val(JSON.stringify(asset));
        var tmpl = $('#detail_asset_information_table').html();
        Mustache.parse(tmpl);
        var data = { item: asset };
        var html = Mustache.render(tmpl, data);
        $('#tbody_detail_asset_information').html(html);
        $('#detailInformationModal').modal();
    }
}

function checkExists(barcode) 
{
	for (var i in list_assets) 
	{
        var data = list_assets[i];
        
        if(data.barcode == barcode) return true;
    }
    
    return false;
	
}

function addItem()
{
	var factory 	        = $('#select_factory').val();
	var sub_department 	    = $('#select_sub_department').val();
	var barcode 	        = $('#barcode_value').val();
	var url_asset_data 	    = $('#url_get_asset').val();
	var isExists            = checkExists(barcode);

    if(!factory)
    {
        $('#barcode_value').val('');
		$("#alert_warning").trigger("click", 'Please select factory first');
        render();
		return;
    }

    if(!sub_department)
    {
        $('#barcode_value').val('');
        $("#alert_warning").trigger("click", 'Please select subdepartment first');
        render();
		return;
    }

	if (isExists) 
	{
        $('#barcode_value').val('');
        $('#_barcode_value').val(barcode);
        $("#alert_warning").trigger("click", 'Barcode already scan');
        render();
		return;
	}

	$.ajax({
		type: "GET",
		url: url_asset_data,
		data: {
			barcode         : barcode,
			sub_department  : sub_department,
			factory         : factory,
		},
		beforeSend: function () {
			$.blockUI({
				message: '<i class="icon-spinner4 spinner"></i>',
				overlayCSS: {
					backgroundColor: '#fff',
					opacity: 0.8,
					cursor: 'wait'
				},
				css: {
					border: 0,
					padding: 0,
					backgroundColor: 'transparent'
				}
			});
		},
		complete: function () {
			$.unblockUI();
		},
		success: function (response) 
		{
			var area    = response.area;
            var asset   = response.asset;

            if(asset)
            {
                list_assets.push(asset);
                $('#_barcode_value').val(asset.barcode);
                $('#is_delete').val('0');
            } 
            else 
            {
                $("#alert_success").trigger('click','Area '+area.name+' has been selected');
                $('#area_name').val(area.name.toLowerCase()).trigger('change');
            }
		},
		error: function (response) 
		{
			$.unblockUI();
			$('#barcode_value').val('');

            if (response['status'] == 500) $("#alert_error").trigger('click','Please contact ICT');
			if (response['status'] == 422) $("#alert_info").trigger('click', response.responseJSON.message);
		}
	})
	.done(function ()
	{
        render();
        $('#barcode_value').val('');
		$('.barcode_value').focus();

	});
}

function lookupAsset()
{
    var select_sub_department   = $('#select_sub_department').val();
    var select_factory          = $('#select_factory').val();

    if(!select_factory)
    {
        $('#barcode_value').val('');
        $("#alert_warning").trigger("click", 'Please select factory first');
        render();
        return;
    }

    if(!select_sub_department)
    {
        $("#alert_warning").trigger("click", 'Please select subdepartment first');
        render();
        return false;
    }

    $('#barcodeModal').modal();
    lov('barcode',select_sub_department,select_factory,'/check-in/list-of-asset?');
}

function lov(name,arg1,arg2, url) 
{
    var search          = '#' + name + 'Search';
    var item_id         = '#' + name + 'Id';
    var item_name       = '#' + name + 'Name';
    var modal           = '#' + name + 'Modal';
    var table           = '#' + name + 'Table';
    var buttonSrc       = '#' + name + 'ButtonSrc';
    var buttonDel       = '#' + name + 'ButtonDel';

    function itemAjax()
    {
        var q = $(search).val();
        $(table).addClass('hidden');
        $(modal).find('.shade-screen').removeClass('hidden');
        $(modal).find('.form-search').addClass('hidden');

        $.ajax({
            url: url + '&q=' + q+ '&sub_department_id=' + arg1+ '&factory_id=' + arg2
        })
        .done(function (data) 
        {
            $(table).html(data);
            pagination(name);
            $(search).focus();
            $(table).removeClass('hidden');
            $(modal).find('.shade-screen').addClass('hidden');
            $(modal).find('.form-search').removeClass('hidden');

            $(table).find('.btn-choose').on('click', chooseItem);
        });
    }

    function chooseItem()
    {
        var id    = $(this).data('id');
        $('#barcode_value').val(id).trigger('change');
    }

    function pagination()
    {
        $(modal).find('.pagination a').on('click', function (e) 
        {
            var params = $(this).attr('href').split('?')[1];
            url = $(this).attr('href') + (params == undefined ? '?' : '');

            e.preventDefault();
            itemAjax();
        });
    }

    $(buttonSrc).unbind();
    $(search).unbind();

    $(buttonSrc).on('click', itemAjax);

    $(search).on('keypress', function (e) {
        if (e.keyCode == 13)
            itemAjax();
    });

    $(buttonDel).on('click', function () {
        $(item_id).val('');
        $(item_name).val('');

    });

    itemAjax();
}