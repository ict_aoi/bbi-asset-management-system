$(document).ready( function ()
{ 
    var msg = $('#msg').val();
    if (msg == 'success') $("#alert_success").trigger("click", 'Data successfully saved.');
    else if (msg == 'success_2') $("#alert_success").trigger("click", 'Data successfully updated.');

    $('input.input-date').datepicker({
        format: "dd/mm/yyyy",
        autoclose: true,
        todayHighlight: true
    });
    

    $('#form').submit(function (event){

        event.preventDefault();
        var no_registration     = $('#no_registration').val();
        var date_registration   = $('#date_registration').val();
        var no_aju              = $('#no_aju').val();
        var select_bc_type      = $('#select_bc_type').val();
        var no_bc               = $('#no_bc').val();
        var current_time        = $('#time').text();

        $('#current_time').val(current_time);
        
        if(!no_registration)
        {
            $("#alert_warning").trigger("click", 'Please type no registration first');
            return false;
        }

        if(!date_registration)
        {
            $("#alert_warning").trigger("click", 'Please select date registration first');
            return false;
        }

        if(!no_aju)
        {
            $("#alert_warning").trigger("click", 'Please type no aju first');
            return false;
        }
        
        if(!no_bc)
        {
            $("#alert_warning").trigger("click", 'Please type no bc first');
            return false;
        }

        if(!select_bc_type)
        {
            $("#alert_warning").trigger("click", 'Please select bc type first');
            return false;
        }
        
        $('#editBCModal').modal('hide');
        bootbox.confirm("Are you sure want to save this data ?.", function (result) 
        {
            if(result)
            {
                $.ajax({
                    type: "POST",
                    url: $('#form').attr('action'),
                    data: $('#form').serialize(),
                    beforeSend: function () {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function (url) 
                    {
                        $('#form').trigger('reset');
                        $('#reportOutstandingBCTable').DataTable().ajax.reload(null,false);
                        $("#alert_success").trigger("click", 'Data successfully updated.');
                    },
                    error: function (response) 
                    {
                        $.unblockUI();
                        $('#editBCModal').modal();
                        if (response.status == 500) $("#alert_error").trigger("click",'Please contact ICT!');
                        if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON.message);
                        
                    }
                });
            }
        });
    });

    $('#select_factory').trigger('change');
    $('#select_department').trigger('change');
    
    var page = $('#page').val();
    if(page == 'index')
    {
        var report = $('#reportOutstandingBCTable').DataTable({
            dom: 'Bfrtip',
            processing: true,
            serverSide: true,
            pageLength:10,
            scrollY:250,
            paging: true, 
            scrollX:true,
            deferRender:true,
            ajax: {
                type: 'GET',
                url: '/report/outsanding-bc/data',
                data: function(d) {
                    return $.extend({}, d, {
                        "factory"           : $('#select_factory').val(),
                        "type"              : $('#select_type').val(),
                        "status"            : $('#select_status').val(),
                    });
               }
            },
            fnCreatedRow: function (row, data, index) {
                var info = report.page.info();
                var value = index+1+info.start+data.action;
                $('td', row).eq(0).html(value);
            },
            columns: [
                {data: null, sortable: false, orderable: false, searchable: false},
                {data: 'type', name: 'type',searchable:true,orderable:true},
                {data: 'factory_name', name: 'factory_name',searchable:true,visible:true,orderable:true},
                {data: 'destination_factory_name', name: 'destination_factory_name',searchable:true,visible:true,orderable:true},
                {data: 'sub_department_name', name: 'sub_department_name',searchable:true,visible:true,orderable:true},
                {data: 'no_agreement', name: 'no_agreement',searchable:true,visible:true,orderable:true},
                {data: 'parent_no_agreement', name: 'parent_no_agreement',searchable:true,visible:true,orderable:true},
                {data: 'bc_in_type', name: 'bc_in_type',searchable:true,visible:true,orderable:true},
                {data: 'bc_out_type', name: 'bc_out_type',searchable:true,visible:true,orderable:true},
                {data: 'start_date', name: 'start_date',searchable:true,orderable:true},
                {data: 'end_date', name: 'end_date',searchable:true,orderable:true},
                
            ],
    
        });
    
        var dtable = $('#reportOutstandingBCTable').dataTable().api();
        $(".dataTables_filter input")
            .unbind() // Unbind previous default bindings
            .bind("keyup", function (e) { // Bind our desired behavior
                // If the user pressed ENTER, search
                if (e.keyCode == 13) {
                    // Call the API search function
                    dtable.search(this.value).draw();
                }
                // Ensure we clear the search if they backspace far enough
                if (this.value == "") {
                    dtable.search("").draw();
                }
                return;
        });
        dtable.draw();

        $('#select_factory').on('change',function()
        {
            dtable.draw();
        });

        $('#select_type').on('change',function()
        {
            dtable.draw();
        });

        $('#select_status').on('change',function()
        {
            dtable.draw();
        });
    }else if(page =='import')
    {
        result = JSON.parse($('#result').val());
        render();
    }else if(page =='detail')
    {
        var url_detail_data = $('#url_detail_data').val();
        var report = $('#detailRentalAgreementTable').DataTable({
            dom: 'Bfrtip',
            processing: true,
            serverSide: true,
            pageLength:10,
            deferRender:true,
            ajax: {
                type: 'GET',
                url: url_detail_data
            },
            fnCreatedRow: function (row, data, index) {
                var info = report.page.info();
                var value = index+1+info.start;
                $('td', row).eq(0).html(value);
            },
            columns: [
                {data: null, sortable: false, orderable: false, searchable: false},
                {data: 'asset_type', name: 'asset_type',searchable:true,visible:true,orderable:true},
                {data: 'serial_number', name: 'serial_number',searchable:true,visible:true,orderable:true},
                {data: 'model', name: 'model',searchable:true,visible:true,orderable:true},
            ],
    
        });
    
        var dtable = $('#detailRentalAgreementTable').dataTable().api();
        $(".dataTables_filter input")
            .unbind() // Unbind previous default bindings
            .bind("keyup", function (e) { // Bind our desired behavior
                // If the user pressed ENTER, search
                if (e.keyCode == 13) {
                    // Call the API search function
                    dtable.search(this.value).draw();
                }
                // Ensure we clear the search if they backspace far enough
                if (this.value == "") {
                    dtable.search("").draw();
                }
                return;
        });
        dtable.draw();
    }

});

$('#upload_button').on('click', function () 
{
    $('#upload_file').trigger('click');
});

$('#upload_file').on('change', function () 
{
    //$('#form_upload_file').submit();
    var current_time    = $('#time').text();
    $('#current_time').val(current_time);
    $.ajax({
        type: "post",
        url: $('#form_upload_file').attr('action'),
        data: new FormData(document.getElementById("form_upload_file")),
        processData: false,
        contentType: false,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
            result = [];
            render();
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            $("#alert_info").trigger("click", 'Import Success.');
            var data        = response;

            for(idx in data)
            {
                var input = data[idx];
                result.push(input);
            }

            
        },
        error: function (response) {
            $.unblockUI();
            $('#form_upload_file').trigger('reset');

            if (response['status'] == 500) $("#alert_error").trigger("click", 'Please Contact ICT.');
            if (response['status'] == 422) $("#alert_error").trigger("click", response.responseJSON.message);

        }
    })
    .done(function () {
        $('#form_upload_file').trigger('reset');
        render();
    });

})

function render() 
{
    getIndex();
    $('#result').val(JSON.stringify(result));
    var tmpl = $('#upload_asset').html();
    Mustache.parse(tmpl);
    var data = { item: result };
    var html = Mustache.render(tmpl, data);
    $('#draw').html(html);
}

function getIndex() 
{
    for (idx in result) 
    {
        result[idx]['_id'] = idx;
        result[idx]['no'] = parseInt(idx) + 1;
    }
}

function edit(url,status)
{

	$.ajax({
        url: url,
        data: {
            status : status
        },
		beforeSend: function () {
			$.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
			});
			
			$('#form').trigger('reset');
			$('#txt_txt_no_kk').text('');
			$('#txt_bc_type').text('');
            
		},
		success: function () 
		{
			$.unblockUI();
		},
		error: function (response) {
			$.unblockUI();
			if (response['status'] == 422) $("#alert_warning").trigger("click", response.responseJSON.message);
			if (response['status'] == 500) $("#alert_error").trigger("click", 'Please contact ict');
			if (response['status'] == 419 || response['status'] == 401) 
			{
				$("#alert_info_2").trigger("click", 'Session expired. You\'ll be take to the login page');
				location.href = "/login"; 
			}
		}
	})
	.done(function (data) 
	{
        if(data.status == '1') $("#txt_bc_type").text('In');
        else $("#txt_bc_type").text('Out');

		$("#status").val(data.status);
		$("#_id").val(data.id);
		$("#no_registration").val(data.no_registration);
		$("#date_registration").val(data.date_registration);
		$("#no_aju").val(data.no_aju);
		$("#no_bc").val(data.no_bc);
		$("#select_bc_type").val(data.bc_type_id).trigger('change');
		$("#txt_no_kk").text(data.no_agreement);
		$('#editBCModal').modal();
	});
}
