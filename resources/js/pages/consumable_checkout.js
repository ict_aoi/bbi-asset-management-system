$(document).ready(function() 
{
    $('input.input-date').datepicker({
        format: "dd/mm/yyyy",
        autoclose: true,
        todayHighlight: true
    });

    $('#btn-user').trigger('click');
    $('#form').submit(function(event) 
    {
        event.preventDefault();
        var employee_nik            = $('#employee_nik').val();
        var employee_name           = $('#employee_name').val();
        var employee_sub_department = $('#employee_sub_department').val();
        var checkout_to_status      = $('#checkout_to_status').val();
        var asset_id                = $('#asset_id').val();
        var no_inv                  = $('#no_inv').val();
        var flag_detail             = $('#flag_detail').val();
        var detail_id               = $('#detailId').val();

        if (flag_detail == '1') 
        {
            if (detail_id == '') 
            {
                $("#alert_warning").trigger("click", 'Please select detail first');
                return false;
            }
        }

        if (checkout_to_status == 1) 
        {
            if (employee_nik == '') 
            {
                $("#alert_warning").trigger("click", 'Please input nik first');
                return false;
            }

            if (employee_name == '') 
            {
                $("#alert_warning").trigger("click", 'Employee name doest not exists');
                return false;
            }

            if (employee_sub_department == '') 
            {
                $("#alert_warning").trigger("click", 'Destination Subdepartment doest not exists');
                return false;
            }
        }else 
        {
            if (no_inv == '') 
            {
                $("#alert_warning").trigger("click", 'Please scan barcode / select asset first');
                return false;
            }
            if (asset_id == '') 
            {
                $("#alert_warning").trigger("click", 'Please scan barcode / select asset first');
                return false;
            }
        }


        bootbox.confirm("Are you sure want to save this data ?.", function(result) 
        {
            if (result) 
            {
                $.ajax({
                    type: 'post',
                    url: $('#form').attr('action'),
                    data: $('#form').serialize(),
                    beforeSend: function() {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    complete: function() {
                        $.unblockUI();
                    },
                    success: function() {
                        $("#alert_success").trigger("click", 'Checkout sonsumable asset successfully');
                        document.location.href = '/consumable';
                    },
                    error: function(response) {
                        $.unblockUI();
                        if (response.status == 422) $("#alert_warning").trigger("click", response.responseJSON.message);

                    }
                });
            }
        });
    });

    getAbsence();
});

$('#auto_completes').on('change', function() 
{
    available_nik = JSON.parse($('#auto_completes').val());
    $("#employee_nik").autocomplete({
        source: available_nik
    });
});

$('#form').on('keyup keypress', function(e) 
{
    var keyCode = e.keyCode || e.which;
    if (keyCode == 13) {
        e.preventDefault();
        return false;
    }
});

$('#employee_nik').keypress(function(e) 
{
    if (e.keyCode == 13 || e.keyCode == 9) 
    {
        var value         = $(this).val().split(':'); 
        if(value)
        {
            $('.information').removeClass("hidden");
            $('#employee_nik').val(value[0]);
            $('#employee_name').val(value[1]);
            $('#employee_sub_department').val(value[2]);
            $('#code_factory').val(value[3]);
        }else
        {
            $("#alert_warning").trigger("click", 'Data not found');
            $('.information').addClass("hidden");
            $('#employee_nik').val('');
            $('#employee_name').val('');
            $('#employee_sub_department').val('');
            $('#code_factory').val('');
            $(this).val('');
            return false;
        }
    }


});

$('#btn-user').on('click', function() 
{
    $('#nik').removeClass("hidden");
    $('#asset').addClass("hidden");
    $('#no_asset').val("");
    $('#asset_id').val("");
    $('#code_factory').val("");
    $('#no_inv').val("");
    $('#checkout_to_status').val('1');
});

$('#btn-asset').on('click', function() 
{
    $('#asset').removeClass("hidden");
    $('#nik').addClass("hidden");
    $('.information').addClass("hidden");
    $('#nik').trigger("reset");
    $('#employee_name').val('');
    $('#employee_sub_department').val('');
    $('#employee_sub_department').val('');
    $('#code_factory').val("");
    $('#asset_id').val("");
    $('#code_factory').val("");
    $('#checkout_to_status').val('2');
});

$('#barcodeName').on('keypress', function(e) 
{

    if (e.keyCode == 13) 
    {
        var sub_department_id   = $('#sub_department_id').val();
        var company_id          = $('#company_id').val();
        var barcode             = $(this).val();
        var flag_detail         = $('#flag_detail').val();
        var detail_id           = $('#detailId').val();

        if (flag_detail == '1') 
        {
            if (detail_id == '') 
            {
                $("#alert_warning").trigger("click", 'Please select detail first');
                return false;
            }
        }

        if (barcode) 
        {
            $.ajax({
                    type: 'get',
                    url: '/consumable/asset?barcode=' + barcode + '&sub_department_id=' + sub_department_id + '&company_id=' + company_id,
                    error: function(response) {
                        if (response.status == 422) {
                            $("#alert_warning").trigger("click", response.responseJSON.message);
                        }
                    }
                })
                .done(function(response) 
                {
                    $('#no_inv').val(response.assets.no_inventory);
                    $('#no_asset').val(response.assets.erp_no_asset);
                    $('#asset_id').val(response.assets.id);
                });

            $(this).val('');
        }
    }
});

$('#barcodeButtonLookup').on('click', function() 
{
    var flag_detail         = $('#flag_detail').val();
    var detail_id           = $('#detailId').val();
    var sub_department_id   = $('#sub_department_id').val();
    var company_id          = $('#company_id').val();

    if (flag_detail == '1') 
    {
        if (detail_id == '') 
        {
            $("#alert_warning").trigger("click", 'Please select detail first');
            return false;
        }
    }

    $('#barcodeModal').modal();
    lov('barcode', sub_department_id, company_id, '/consumable/list-of-asset?');
});


function getAbsence() 
{
    var value = $('#company_id').val();
    $.ajax({
            type: 'get',
            url: '/consumable/absence'
        })
        .done(function(response) {
            $('#auto_completes').val(JSON.stringify(response)).trigger('change');
        })
}

function lov(name, arg1, arg2, url) 
{
    var search    = '#' + name + 'Search';
    var item_id   = '#' + name + 'Id';
    var item_name = '#' + name + 'Name';
    var modal     = '#' + name + 'Modal';
    var table     = '#' + name + 'Table';
    var buttonSrc = '#' + name + 'ButtonSrc';
    var buttonDel = '#' + name + 'ButtonDel';

    function itemAjax() {
        var q = $(search).val();
        $(table).addClass('hidden');
        $(modal).find('.shade-screen').removeClass('hidden');
        $(modal).find('.form-search').addClass('hidden');

        $.ajax({
                url: url + '&q=' + q + '&sub_department_id=' + arg1 + '&company_id=' + arg2
            })
            .done(function(data) {
                $(table).html(data);
                pagination(name);
                $(search).focus();
                $(search).val('');
                $(table).removeClass('hidden');
                $(modal).find('.shade-screen').addClass('hidden');
                $(modal).find('.form-search').removeClass('hidden');

                $(table).find('.btn-choose').on('click', chooseItem);
            });
    }

    function chooseItem() {
        var id      = $(this).data('id');
        var inv     = $(this).data('noinv');
        var asset   = $(this).data('noasset');



        $('#asset_id').val(id);
        $('#no_inv').val(inv);
        $('#no_asset').val(asset);
    }

    function pagination() {
        $(modal).find('.pagination a').on('click', function(e) {
            var params = $(this).attr('href').split('?')[1];
            url = $(this).attr('href') + (params == undefined ? '?' : '');

            e.preventDefault();
            itemAjax();
        });
    }

    $(search).val("");
    $(buttonSrc).unbind();
    $(search).unbind();

    $(buttonSrc).on('click', itemAjax);

    $(search).on('keypress', function(e) {
        if (e.keyCode == 13)
            itemAjax();
    });

    $(buttonDel).on('click', function() {
        $(item_id).val('');
        $(item_name).val('');

    });

    itemAjax();
}