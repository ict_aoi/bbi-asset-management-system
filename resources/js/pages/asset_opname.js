setInterval(function(){ setFocusToTextBox(); }, 10000);

$(document).ready(function()
{
    var calendar_id = $('#calendar_id').val();

    if(!calendar_id)
    {
        $("#alert_warning").trigger("click", 'You dont have sto event, please contact admin to create new event');
        setFocusToTextBox();
    }

    $('input.input-date').datepicker({
        format: "dd/mm/yyyy",
        autoclose: true,
        todayHighlight: true
    });
    
    list_assets = JSON.parse($('#assets').val());
    
    $('#form').on('keyup keypress', function(e) 
    {
        var keyCode = e.keyCode || e.which;
        if (keyCode == 13) 
        { 
            e.preventDefault();
            return false;
        }
    });

    $('#form').submit(function (event)
    {
        event.preventDefault();

        var factory             = $('#select_factory').val();
        var area_opname         = $('#select_opname_area_location').val();
        var check_validation    = checkValidation();
        var current_time        = $('#time').text();
        
        $('#current_time').val(current_time);

        if(factory == '')
        {
            $("#alert_warning").trigger("click", 'Please select factory first');
            setFocusToTextBox();
            return false;
        }

        /*if(area_opname == '')
        {
            $("#alert_warning").trigger("click", 'Please select area first');
            setFocusToTextBox();
            return false;
        }*/

        if (check_validation) 
		{ 
            render();
            $("#alert_warning").trigger("click", check_validation);
            return false;
        }

        if(list_assets.length == '')
        {
            $("#alert_warning").trigger("click", 'Please scan / select asset first');
            setFocusToTextBox();
            return false;
        }

        $('#confirmationInsertModal').modal('hide');
        bootbox.confirm("Are you sure want to save this data ?.", function (result) 
        {
            if(result)
            {
                $.ajax({
                    type: 'post',
                    url: $('#form').attr('action'),
                    data: $('#form').serialize(),
                    beforeSend: function () {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function () {
                        $("#alert_success").trigger("click",'Asset opname successfully');
                        document.location.href = '/asset-opname';
                    },
                    error: function (response) {
                        $.unblockUI();
                        if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON.message);
                    
                    }
                });
            }
        });
    });

    $('#select_factory').trigger('change');
    render();
});

$('#select_factory').on('change',function (event,arg1) 
{
    var value       = $(this).val();
    var area_name   = $('#area_name').val(); 

    $.ajax({
        type: 'get',
        url: '/asset-opname/area?factory_id='+value
    })
    .done(function(response)
    {
        $("#select_opname_area_location").empty();
        $("#select_opname_area_location").append('<option value="">-- Select Area --</option>');
        
        $.each(response,function(id,name)
        {
            if(area_name == id) var selected = 'selected';
            else var selected = null;
            
            $("#select_opname_area_location").append('<option value="'+id+'"'+selected+'>'+name+'</option>');
        });
    });

    $('#factory_id').val(value);
});

$('#btn_confirmation').on('click',function() 
{
    $('#confirmationInsertModal').modal();
});

$('#area_name').on('change',function()
{
    var area_name         = $(this).val();
    $('#select_opname_area_location').val(area_name).trigger('change');
});

$('#asset_button_lookup').on('click', function(){
    var select_factory    = $('#select_factory').val();

    if(!select_factory)
    {
        $('#barcode_value').val('');
        $("#alert_warning").trigger("click", 'Please select factory first');
        render();
        return false;
    }

    $('#barcodeModal').modal();
    lov('barcode',select_factory,'/asset-opname/list-of-asset?');
});

function checkValidation() 
{
	for (var i in list_assets) 
	{
        console.log(list_assets[i].status);
        if(list_assets[i].status == '' || list_assets[i].status == null)
        {
            list_assets[i].is_error = true;
            return 'Please select status aasset for barcode number ' + list_assets[i].barcode + '.';
        }

        list_assets[i].is_error = false;
	}
	return false;
}

function setFocusToTextBox() 
{
	$('#barcode_value').focus();
}

function render() 
{
    getIndex();
    $('#assets').val(JSON.stringify(list_assets));
    var tmpl = $('#asset_in_table').html();
    Mustache.parse(tmpl);
    var data = { item: list_assets };
    var html = Mustache.render(tmpl, data);
    $('#draw').html(html);
    bind();
    console.log(data);
    setFocusToTextBox();
    $('#total_barcode').text(list_assets.length)
}

function getIndex() 
{
    var is_delete   = $('#is_delete').val();
    var _barcode    = $('#_barcode_value').val();
    var total	    = list_assets.length;

    for (id in list_assets) 
    {
        if(_barcode == list_assets[id]['barcode']) list_assets[id]['_active'] = 'active';
		else
		{
        	if(is_delete == 1)
            {
                if(total == (parseInt(id)+1)) list_assets[id]['_active'] = 'active';
                else list_assets[id]['_active'] = null;
            }
            else list_assets[id]['_active'] = null;
            
		}
        list_assets[id]['_id'] = id;
        list_assets[id]['no'] = parseInt(id) + 1;

        for(idx in list_assets[id].detail_informations)
        {
            list_assets[id].detail_informations[idx]['_idx'] 	= idx;
            list_assets[id].detail_informations[idx]['nox']	    = parseInt(idx) + 1;
        }
    }
}

function bind() 
{
	$('.btn-delete-item').on('click', deleteItem);
	$('.btn-detail-item').on('click', showDetail);
    $('#barcode_value').on('change', addItem);
    $('.status_item').on('change', changeStatus);
    $('.btn-active').on('click', setActive);
}

function deleteItem()
{
	var i = $(this).data('id');
    list_assets.splice(i, 1);
    $('#is_delete').val('1');
    $('#_barcode_value').val('');
	render();
}

function setActive()
{
    var barcode       = $(this).data('barcode');
    $('#_barcode_value').val(barcode);
    render();
}

function changeStatus()
{
    var i       = $(this).data('id');
    var data    = list_assets[i];

    var status = $('#status_option_'+i).val();
    data.status = status;

    for (var id in data.status_options) 
    {
        var data_options = data.status_options[id];
        
        if (status == data_options.id) data_options.selected = 'selected';
        else data_options.selected= '';
    }

    $('#_barcode_value').val(data.barcode);
    render();
}

function showDetail()
{
    var i       = $(this).data('id');
    var asset   = list_assets[i];

    for (var id in asset.detail_informations) 
    {
        var detail = asset.detail_informations[id];
        if(asset.id == detail.id) detail.is_hidden = false;
    }

    $('#detail_information').val(JSON.stringify(asset));
    var tmpl = $('#detail_asset_information_table').html();
    Mustache.parse(tmpl);
    var data = { item: asset };
    var html = Mustache.render(tmpl, data);
    $('#tbody_detail_asset_information').html(html);
	$('#detailInformationModal').modal();
}

function checkExists(barcode) 
{
	for (var i in list_assets) 
	{
        var data = list_assets[i];
        
        if(data.barcode == barcode) return true;
    }
    
    return false;
	
}

function addItem()
{
	var factory 	    = $('#select_factory').val();
	var barcode 	    = $('#barcode_value').val();
	var url 	        = $('#url_get_asset').val();
	var isExists        = checkExists(barcode);
    var calendar_id     = $('#calendar_id').val();

    if(!calendar_id)
    {
        $("#alert_warning").trigger("click", 'You dont have sto event, please contact admin to create new event');
        setFocusToTextBox();
        return false;
    }

    if(!factory)
    {
        $('#barcode_value').val('');
        $("#alert_warning").trigger("click", 'Please select factory first');
        render();
		return;
    }

  	if (isExists) 
	{
       
        $('#_barcode_value').val(barcode);
		$('#barcode_value').val('');
        $("#alert_warning").trigger("click", 'Barcode already scan');
        render();
    	return false;
	}

	$.ajax({
		type: "GET",
		url: url,
		data: {
			barcode     : barcode,
			factory     : factory,
		},
		beforeSend: function () {
			$.blockUI({
				message: '<i class="icon-spinner4 spinner"></i>',
				overlayCSS: {
					backgroundColor: '#fff',
					opacity: 0.8,
					cursor: 'wait'
				},
				css: {
					border: 0,
					padding: 0,
					backgroundColor: 'transparent'
				}
			});
		},
		complete: function () {
			$.unblockUI();
		},
		success: function (response) 
		{
            var area    = response.area;
            var asset   = response.asset;

            if(asset)
            {
                list_assets.push(asset);
                $('#_barcode_value').val(asset.barcode);
                $('#is_delete').val('0');
            }
            else 
            {
                $("#alert_success").trigger('click','Area '+area.name+' has been selected');
                $('#areaName').text(area.name.toLowerCase());
                $('#area_name').val(area.name.toLowerCase()).trigger('change');
            }

		},
		error: function (response) 
		{
			$.unblockUI();
			$('#barcode_value').val('');
			$('.barcode_value').focus();

            if (response['status'] == 500) $("#alert_error").trigger('click','Please contact ICT');
			if (response['status'] == 422) $("#alert_info").trigger('click', response.responseJSON.message);
		}
	})
	.done(function ()
	{
        render();
        $('#barcode_value').val('');
		$('.barcode_value').focus();

	});
}

function lov(name,arg1, url) 
{
    var search          = '#' + name + 'Search';
    var item_id         = '#' + name + 'Id';
    var item_name       = '#' + name + 'Name';
    var modal           = '#' + name + 'Modal';
    var table           = '#' + name + 'Table';
    var buttonSrc       = '#' + name + 'ButtonSrc';
    var buttonDel       = '#' + name + 'ButtonDel';

    function itemAjax()
    {
        var q = $(search).val();
        $(table).addClass('hidden');
        $(modal).find('.shade-screen').removeClass('hidden');
        $(modal).find('.form-search').addClass('hidden');

        $.ajax({
            url: url + '&q=' + q+ '&factory_id=' + arg1
        })
        .done(function (data) 
        {
            $(table).html(data);
            pagination(name);
            $(search).focus();
            $(table).removeClass('hidden');
            $(modal).find('.shade-screen').addClass('hidden');
            $(modal).find('.form-search').removeClass('hidden');

            $(table).find('.btn-choose').on('click', chooseItem);
        });
    }

    function chooseItem()
    {
        var id    = $(this).data('id');
        console.log(id);
        $('#_barcode_value').val(id);
        $('#barcode_value').val(id).trigger('change');
    }

    function pagination()
    {
        $(modal).find('.pagination a').on('click', function (e) 
        {
            var params = $(this).attr('href').split('?')[1];
            url = $(this).attr('href') + (params == undefined ? '?' : '');

            e.preventDefault();
            itemAjax();
        });
    }

    $(buttonSrc).unbind();
    $(search).unbind();

    $(buttonSrc).on('click', itemAjax);

    $(search).on('keypress', function (e) {
        if (e.keyCode == 13)
            itemAjax();
    });

    $(buttonDel).on('click', function () {
        $(item_id).val('');
        $(item_name).val('');

    });

    itemAjax();
}