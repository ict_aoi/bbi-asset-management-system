$(document).ready( function ()
{ 
    var msg = $('#msg').val();
    if (msg == 'success') $("#alert_success").trigger("click", 'Data successfully saved.');
    else if (msg == 'success_2') $("#alert_success").trigger("click", 'Data successfully updated.');

    $('#form').submit(function (event){
        event.preventDefault();
        var year                    = $('#select_create_year').val();
        var name                    = $('#name').val();
        var select_factory          = $('#select_create_factory').val();
        var select_sub_department   = $('#select_create_sub_department').val();
        var current_time            = $('#time').text();
        var unique_id               = uuidv4();

        $('#unique_id').val(unique_id);
        $('#current_time').val(current_time);
        
        if(!select_factory)
        {
            $("#alert_warning").trigger("click", 'Please select factory first');
            return false;
        }
        
        if(!select_sub_department)
        {
            $("#alert_warning").trigger("click", 'Please select subdepartment first');
            return false;
        }
       

        if(!name)
        {
            $("#alert_warning").trigger("click", 'Please type name first');
            return false;
        }

        if(!year)
        {
            $("#alert_warning").trigger("click", 'Please type year of periode');
            return false;
        }
        
        $('#confirmationInsertModal').modal('hide');
        bootbox.confirm("Are you sure want to save this data ?.", function (result) {
            if(result)
            {
                $.ajax({
                    type: "POST",
                    url: $('#form').attr('action'),
                    data: $('#form').serialize(),
                    beforeSend: function () {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function (url) {
                        document.location.href = url;
                    },
                    error: function (response) {
                        $.unblockUI();
                        
                        $('#confirmationInsertModal').modal('show');
                        if (response.status == 500) $("#alert_error").trigger("click",'Please contact ICT!');
                        if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON.message);
                        
                    }
                });
            }
        });
    });

    $('#form_edit').submit(function (event){
        event.preventDefault();
        var current_time        = $('#time').text();
    
        $('#current_time').val(current_time);
        
        bootbox.confirm("Are you sure want to save this data ?.", function (result) 
        {
            if(result)
            {
                $.ajax({
                    type: "POST",
                    url: $('#form_edit').attr('action'),
                    data: $('#form_edit').serialize(),
                    beforeSend: function () {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function (url) {
                        document.location.href = '/budget';
                    },
                    error: function (response) {
                        $.unblockUI();
                        
                        if (response.status == 500) $("#alert_error").trigger("click",'Please contact ICT!');
                        if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON.message);
                        
                    }
                });
            }
        });
    });

    $('#select_factory').trigger('change');
    $('#select_sub_department').trigger('change');
    $('#select_create_factory').trigger('change');
    $('#select_create_department').trigger('change');

    var page = $('#page').val();
    if(page == 'edit')
    {
        list_budgets = JSON.parse($('#budgets').val());
        render();
    }else
    {
        var budgetTable = $('#budgetTable').DataTable({
            dom: 'Bfrtip',
            processing: true,
            serverSide: true,
            pageLength:10,
            scrollY:250,
            paging: true, 
            scrollX:true,
            deferRender:true,
            ajax: {
                type: 'GET',
                url: '/budget/data',
            },
            fnCreatedRow: function (row, data, index) {
                var info = budgetTable.page.info();
                var value = index+1+info.start+data.action;
                $('td', row).eq(0).html(value);
            },
            columns: [
                {data: null, sortable: false, orderable: false, searchable: false},
                {data: 'factory_name', name: 'factory_name',searchable:true,visible:true,orderable:true},
                {data: 'department_name', name: 'department_name',searchable:true,visible:true,orderable:true},
                {data: 'budget_name', name: 'budget_name',searchable:true,visible:true,orderable:true},
                {data: 'budget_description', name: 'budget_description',searchable:true,visible:true,orderable:true},
                {data: 'periode_year', name: 'periode_year',searchable:true,orderable:true},
                {data: 'total_budget', name: 'total_budget',searchable:true,orderable:true},
            ],
    
        });
    
        var dtable = $('#budgetTable').dataTable().api();
        $(".dataTables_filter input")
            .unbind() // Unbind previous default bindings
            .bind("keyup", function (e) { // Bind our desired behavior
                // If the user pressed ENTER, search
                if (e.keyCode == 13) {
                    // Call the API search function
                    dtable.search(this.value).draw();
                }
                // Ensure we clear the search if they backspace far enough
                if (this.value == "") {
                    dtable.search("").draw();
                }
                return;
        });
        dtable.draw();
    }

});

function render() 
{
	getIndex();
	$('#budgets').val(JSON.stringify(list_budgets));
	var tmpl 		= $('#budget_table').html();
	Mustache.parse(tmpl);
	var data 		= { list: list_budgets };
	var html 		= Mustache.render(tmpl, data);
    $('#tbody_budget').html(html);
    console.log(data);
	bind();
}

function getIndex() 
{
	for (idx in list_budgets) 
	{
		list_budgets[idx]['_id'] 	= idx;
		list_budgets[idx]['no'] 	= parseInt(idx) + 1;
	}
}

function bind() 
{
    $('.txt-input-budget').on('change', changeBudget);
    
    $('.input-number').keypress(function (e) 
	{

		if (e.keyCode > 31 && (e.keyCode < 46 || e.keyCode > 57)) {
			return false;
		}
		return true;
	});
}

function changeBudget()
{
    var i           = $(this).data('id');
    var total       = $('#budget_of_periode_'+i).val();
    console.log(total);
    var data        = list_budgets[i];
    data.total      = total;

    render();
}

function uuidv4() 
{
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

function hapus(url)
{
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    $.ajax({
        type: "put",
        url: url,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function () {
            $.unblockUI();
        },
        error: function () {
            $.unblockUI();
        }
    }).done(function () {
        $('#budgetTable').DataTable().ajax.reload();
        $("#alert_success").trigger("click", 'Data successfully deleted');
    });
}