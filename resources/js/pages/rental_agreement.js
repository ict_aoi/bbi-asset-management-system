$(document).ready( function ()
{ 
    $('input.input-date').datepicker({
        format: "dd/mm/yyyy",
        autoclose: true,
        todayHighlight: true
    });

    var msg = $('#msg').val();
    if (msg == 'success') $("#alert_success").trigger("click", 'Data successfully saved.');
    else if (msg == 'success_2') $("#alert_success").trigger("click", 'Data successfully updated.');

    $('#form').submit(function (event){

        event.preventDefault();
        var select_factory          = $('#select_factory').val();
        var select_sub_department   = $('#select_sub_department').val();
        var select_budget           = $('#select_budget').val();
        var no_agreement            = $('#no_agreement').val();
        var price_rent              = $('#price_rent').val();
        var start_rent_date         = $('#start_rent_date').val();
        var end_rent_date           = $('#end_rent_date').val();
        var current_time            = $('#time').text();

        $('#current_time').val(current_time);
        
        if(!select_factory)
        {
            $("#alert_warning").trigger("click", 'Please select factory first');
            return false;
        }
        
        if(!select_sub_department)
        {
            $("#alert_warning").trigger("click", 'Please select subdepartment first');
            return false;
        }

        if(!select_budget)
        {
            $("#alert_warning").trigger("click", 'Please select budget first');
            return false;
        }
       

        if(!no_agreement)
        {
            $("#alert_warning").trigger("click", 'Please type no kk first');
            return false;
        }

        if(!price_rent)
        {
            $("#alert_warning").trigger("click", 'Please type price rent first');
            return false;
        }

        if(!start_rent_date)
        {
            $("#alert_warning").trigger("click", 'Please select start rent first');
            return false;
        }

        if(!end_rent_date)
        {
            $("#alert_warning").trigger("click", 'Please select end rent first');
            return false;
        }

        if(start_rent_date && end_rent_date)
        {
            var d1 = start_rent_date.split("/");
            var d2 = end_rent_date.split("/");

            var from = new Date(d1[2], parseInt(d1[1])-1, d1[0]);  // -1 because months are from 0 to 11
            var to   = new Date(d2[2], parseInt(d2[1])-1, d2[0]);

            if(from > to)
            {
                $("#alert_warning").trigger("click", 'end rent date cannot be small than start rent date');
                return false
            }
           
        }
       
        
        bootbox.confirm("Are you sure want to save this data ?.", function (result) 
        {
            if(result)
            {
                $.ajax({
                    type: "POST",
                    url: $('#form').attr('action'),
                    data: $('#form').serialize(),
                    beforeSend: function () {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function (url) 
                    {
                        document.location.href = '/rental-agreement';
                    },
                    error: function (response) 
                    {
                        $.unblockUI();
                        
                        if (response.status == 500) $("#alert_error").trigger("click",'Please contact ICT!');
                        if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON.message);
                        
                    }
                });
            }
        });
    });

    $('#form-extend').submit(function (event){

        event.preventDefault();
        var select_budget       = $('#select_budget').val();
        var no_agreement        = $('#no_agreement').val();
        var price_rent          = $('#price_rent').val();
        var start_rent_date     = $('#start_rent_date').val();
        var end_rent_date       = $('#end_rent_date').val();
        var description         = $('#description').val();
        var old_no_agreement    = $('#old_no_agreement').val();
        var old_end_date        = $('#old_end_date').val();
        var old_start_date      = $('#old_start_date').val();
        var current_time        = $('#time').text();

        $('#extend_description').val(description);
        $('#extend_end_rent_date').val(end_rent_date);
        $('#extend_start_rent_date').val(start_rent_date);
        $('#extend_budget').val(select_budget);
        $('#extend_price_rent').val(price_rent);
        $('#extend_no_agreement').val(no_agreement);
        $('#extend_current_time').val(current_time);
        
        if(!select_factory)
        {
            $("#alert_warning").trigger("click", 'Please select factory first');
            return false;
        }
        
        if(!select_budget)
        {
            $("#alert_warning").trigger("click", 'Please select budget first');
            return false;
        }
       

        if(!no_agreement)
        {
            $("#alert_warning").trigger("click", 'Please type no kk first');
            return false;
        }

        //if( old_no_agreement.toUpperCase() == no_agreement.toUpperCase())
        //{
          //  $("#alert_warning").trigger("click", 'no kk must diferrent than before');
            //return false;
        //}

        if(!price_rent)
        {
            $("#alert_warning").trigger("click", 'Please type price rent first');
            return false;
        }

        if(!start_rent_date)
        {
            $("#alert_warning").trigger("click", 'Please select start rent first');
            return false;
        }

        if(!end_rent_date)
        {
            $("#alert_warning").trigger("click", 'Please select end rent first');
            return false;
        }
        

        if( old_start_date == start_rent_date )
        {
            $("#alert_warning").trigger("click", 'start rent must diferrent than before');
            return false;
        }

        if( old_end_date == end_rent_date )
        {
            $("#alert_warning").trigger("click", 'end rent must diferrent than before');
            return false;
        }

        if(start_rent_date && end_rent_date)
        {
            var d1 = start_rent_date.split("/");
            var d2 = end_rent_date.split("/");

            var from = new Date(d1[2], parseInt(d1[1])-1, d1[0]);  // -1 because months are from 0 to 11
            var to   = new Date(d2[2], parseInt(d2[1])-1, d2[0]);

            if(from > to)
            {
                $("#alert_warning").trigger("click", 'end rent date cannot be small than start rent date');
                return false
            }
           
        }

        if(start_rent_date && old_end_date)
        {
            var d1 = old_end_date.split("/");
            var d2 = start_rent_date.split("/");

            var from = new Date(d1[2], parseInt(d1[1])-1, d1[0]);  // -1 because months are from 0 to 11
            var to   = new Date(d2[2], parseInt(d2[1])-1, d2[0]);

            if(from > to)
            {
                $("#alert_warning").trigger("click", 'start rent date cannot be small than end rent date before');
                return false
            }
           
        }
       
        
        bootbox.confirm("Are you sure want to save this data ?.", function (result) 
        {
            if(result)
            {
                $.ajax({
                    type: "POST",
                    url: $('#form-extend').attr('action'),
                    data: $('#form-extend').serialize(),
                    beforeSend: function () {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function (url) 
                    {
                        document.location.href = '/rental-agreement';
                    },
                    error: function (response) 
                    {
                        $.unblockUI();
                        
                        if (response.status == 500) $("#alert_error").trigger("click",'Please contact ICT!');
                        if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON.message);
                        
                    }
                });
            }
        });
    });

    $('#select_factory').trigger('change');
    $('#select_sub_department').trigger('change');
    
    var page = $('#page').val();
    if(page == 'index')
    {
        var budgetTable = $('#rentalAgreementTable').DataTable({
            dom: 'Bfrtip',
            processing: true,
            serverSide: true,
            pageLength:10,
            scrollY:250,
            paging: true, 
            scrollX:true,
            deferRender:true,
            ajax: {
                type: 'GET',
                url: '/rental-agreement/data',
                data: function(d) {
                    return $.extend({}, d, {
                        "company"           : $('#select_company').val(),
                        "sub_department"    : $('#select_sub_department').val(),
                        "budget"            : $('#select_budget').val(),
                        "status_agreement"  : $('#select_status_agreement').val(),
                    });
               }
            },
            fnCreatedRow: function (row, data, index) {
                var info = budgetTable.page.info();
                var value = index+1+info.start+data.action;
                $('td', row).eq(0).html(value);
            },
            columns: [
                {data: null, sortable: false, orderable: false, searchable: false},
                {data: 'id', name: 'id',searchable:true,visible:false,orderable:true},
                {data: 'factory_name', name: 'factory_name',searchable:true,visible:true,orderable:true},
                {data: 'sub_department_name', name: 'sub_department_name',searchable:true,visible:true,orderable:true},
                {data: 'budget_name', name: 'budget_name',searchable:true,visible:true,orderable:true},
                {data: 'no_agreement', name: 'no_agreement',searchable:true,visible:true,orderable:true},
                {data: 'parent_no_agreement', name: 'parent_no_agreement',searchable:true,visible:true,orderable:true},
                {data: 'duration_in_day', name: 'duration_in_day',searchable:true,orderable:true},
                {data: 'price', name: 'price',searchable:true,orderable:true},
                {data: 'start_date', name: 'start_date',searchable:true,orderable:true},
                {data: 'end_date', name: 'end_date',searchable:true,orderable:true},
                {data: 'status', name: 'status',searchable:true,orderable:true},
            ],
    
        });
    
        var dtable = $('#rentalAgreementTable').dataTable().api();
        $(".dataTables_filter input")
            .unbind() // Unbind previous default bindings
            .bind("keyup", function (e) { // Bind our desired behavior
                // If the user pressed ENTER, search
                if (e.keyCode == 13) {
                    // Call the API search function
                    dtable.search(this.value).draw();
                }
                // Ensure we clear the search if they backspace far enough
                if (this.value == "") {
                    dtable.search("").draw();
                }
                return;
        });
        dtable.draw();

        $('#select_budget').on('change',function()
        {
            dtable.draw();
        });

        $('#select_sub_department').on('change',function()
        {
            dtable.draw();
        });

        $('#select_factory').on('change',function()
        {
            dtable.draw();
        });
        
        $('#select_status_agreement').on('change',function()
        {
            dtable.draw();
        });
    }else if(page =='edit')
    {
        var end_rent_date   = $('#end_rent_date').val();
        var now             = new Date();
        var _month          = parseInt(now.getMonth())+1;
        var _date           = now.getDate();

        if(_month < 10) _month = '0'+_month;
        else _month;

        if(_date < 10) _date = '0'+_date;
        else _date;
        
        var current_time    = _date+'/'+_month+'/'+now.getFullYear();
        
        if(end_rent_date && current_time)
        {
            var d1 = end_rent_date.split("/");
            var d2 = current_time.split("/");

            var from = new Date(d1[2], parseInt(d1[1])-1, d1[0]);  // -1 because months are from 0 to 11
            var to   = new Date(d2[2], parseInt(d2[1])-1, d2[0]);

            if(from <= to)
            {
                $('#btn_save').addClass('hidden');
                $('#btn_extend').removeClass('hidden');
            }
           
        }
    }

});


$('#price_rent').keypress(function (e) 
{
    var charCode = (e.which) ? e.which : e.keyCode;
    if ((charCode >= 46 && charCode <= 57) || (charCode >= 37 && charCode <= 40) || charCode == 9 || charCode == 8 || charCode == 46) {
        return true;
    }
    return false;
});

$('#select_sub_department').on('change',function(event,arg1)
{
    var value           = $(this).val();  
    var page            = $('#page').val();

    if(value)
    {
        $.ajax({
            type: 'get',
            url: '/rental-agreement/get-budget',
            data : {
                sub_department_id : value
            }
        })
        .done(function(response)
        {
            $("#select_budget").empty();
            $("#select_budget").append('<option value="">-- Select Budget --</option>');


            $.each(response,function(id,name)
            {
                if(page == 'edit')
                {
                    var budget_id =  $('#budget_id').val();
                    if(budget_id == id) var selected = 'selected';
                    else var selected = null;
                }else var selected = null;

                $("#select_budget").append('<option value="'+id+'"'+selected+'>'+name+'</option>');
            });

        });
    }else
    {
        $("#select_budget").empty();
        $("#select_budget").append('<option value="">-- Select Budget --</option>');
    }
});

$('#select_budget').on('change',function(event,arg1)
{
    var value           = $(this).val();  
    var page            = $('#page').val();

    if(page == 'edit')
    {
        if(value) $('#extend_budget').val(value); 
    }
});

$('#no_agreement').on('change',function(event,arg1)
{
    var value           = $(this).val();  
    var page            = $('#page').val();

    if(page == 'edit')
    {
        if(value) $('#extend_no_agreement').val(value); 
    }
});

$('#price_rent').on('change',function(event,arg1)
{
    var value           = $(this).val();  
    var page            = $('#page').val();

    if(page == 'edit')
    {
        if(value) $('#extend_price_rent').val(value); 
    }
});

$('#start_rent_date').on('change',function(event,arg1)
{
    var value           = $(this).val();  
    var page            = $('#page').val();

    if(page == 'edit')
    {
        if(value) $('#extend_start_rent_date').val(value); 
    }
});

$('#end_rent_date').on('change',function(event,arg1)
{
    var value           = $(this).val();  
    var page            = $('#page').val();

    if(page == 'edit')
    {
        if(value) $('#extend_end_rent_date').val(value); 
    }
});

$('#description').on('change',function(event,arg1)
{
    var value           = $(this).val();  
    var page            = $('#page').val();

    if(page == 'edit')
    {
        if(value) $('#extend_description').val(value); 
    }
});

function returnAsset(url)
{
    bootbox.confirm("Are you sure want to return this data ?.", function (result) 
    {
        if(result)
        {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            
            $.ajax({
                type: "post",
                url: url,
                data:
                {
                    current_time : $('#time').text()
                },
                beforeSend: function () {
                    $.blockUI({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                success: function () {
                    $.unblockUI();
                },
                error: function (response) {
                    $.unblockUI();
                    if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON.message);
                }
            })
            .done(function () {
                $('#rentalAgreementTable').DataTable().ajax.reload();
                $("#alert_success").trigger("click", 'Data successfully return');
            });
        }
    });

    
}

function hapus(url)
{
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    $.ajax({
        type: "put",
        url: url,
        data:
        {
            current_time : $('#time').text()
        },
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function () {
            $.unblockUI();
        },
        error: function (response) {
            $.unblockUI();
            if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON.message);
        }
    })
    .done(function () {
        $('#rentalAgreementTable').DataTable().ajax.reload();
        $("#alert_success").trigger("click", 'Data successfully deleted');
    });
}