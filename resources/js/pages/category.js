$(document).ready(function() 
{
    var msg = $('#msg').val();
    var is_super_admin = $('#is_super_admin').val();

    if (msg == 'success') $("#alert_success").trigger("click", 'Data berhasil disimpan.');
    else if (msg == 'success_2') $("#alert_success").trigger("click", 'Data berhasil diubah.');

    if (is_super_admin == 1) 
    {
        var categoryTable = $('#categoryTable').DataTable({
            dom: 'Bfrtip',
            processing: true,
            serverSide: true,
            pageLength: 100,
            deferRender: true,
            ajax: {
                type: 'GET',
                url: '/pengaturan-asset/kategori/data',
            },
            fnCreatedRow: function(row, data, index) {
                var info = categoryTable.page.info();
                var value = index + 1 + info.start;
                $('td', row).eq(0).html(value);
            },
            columns: [
                {data: null, sortable: false, orderable: false, searchable: false},
                {data: 'company_name', name: 'company_name',searchable:true,visible:true,orderable:true},
                {data: 'department_name', name: 'department',searchable:true,visible:true,orderable:true},
                {data: 'name', name: 'name',searchable:true,visible:true,orderable:true},
                {data: 'description', name: 'description',searchable:true,orderable:true},
                {data: 'action', name: 'action',searchable:false,orderable:false},
            ]
        });
    } else 
    {
        var categoryTable = $('#categoryTable').DataTable({
            dom: 'Bfrtip',
            processing: true,
            serverSide: true,
            pageLength: 100,
            deferRender: true,
            ajax: {
                type: 'GET',
                url: '/pengaturan-asset/kategori/data',
            },
            fnCreatedRow: function(row, data, index) {
                var info = categoryTable.page.info();
                var value = index + 1 + info.start;
                $('td', row).eq(0).html(value);
            },
            columns: [
                {data: null, sortable: false, orderable: false, searchable: false},
                {data: 'name', name: 'name',searchable:true,visible:true,orderable:true},
                {data: 'description', name: 'description',searchable:true,orderable:true},
                {data: 'action', name: 'action',searchable:false,orderable:false},
            ]
        });
    }

    var dtable = $('#categoryTable').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function(e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
        });
    dtable.draw();

    $('#form').submit(function(event) 
    {
        event.preventDefault();
        var company = $('#select_company').val();
        var department = $('#select_department').val();
        var name = $('#name').val();

        if (!company) 
        {
            $("#alert_warning").trigger("click", 'Perusahaan wajib diisi');
            return false
        }

        if (!department) 
        {
            $("#alert_warning").trigger("click", 'Departemen wajib diisi');
            return false
        }

        if (!name) 
        {
            $("#alert_warning").trigger("click", 'Nama wajib diisi');
            return false
        }

        bootbox.confirm("Apakah anda yakin akan menyimpan data ini ?.", function(result) {
            if (result) {
                $.ajax({
                    type: "POST",
                    url: $('#form').attr('action'),
                    data: $('#form').serialize(),
                    beforeSend: function() {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    complete: function() {
                        $.unblockUI();
                    },
                    success: function() {
                        document.location.href = '/pengaturan-asset/kategori';
                    },
                    error: function(response) {
                        $.unblockUI();
                        if (response.status == 422) $("#alert_warning").trigger("click", response.responseJSON.message);
                    }
                });
            }
        });
    });

    var department_name = $('#department_name').val();
    $('#select_company').trigger('change',department_name);
});

$('#select_company').on('change',function(event,arg1)
{
    var value = $(this).val();    
    var select_department = $('#select_department').val();    
    var department_name = (select_department)? select_department : arg1;
   
    if(value)
    {
        $.ajax({
            type: 'get',
            url: '/pengaturan-asset/kategori/department?company_id='+value+'&department_name='+department_name
        })
        .done(function(response){
            var departments = response.departments;
            var department_name = response.department_name;
           
            $("#select_department").empty();
            $("#select_department").append('<option value="">-- Pilih Departemen --</option>');
           
            $.each(departments,function(id,name){
                if(department_name == id) var selected = 'selected';
                else var selected = null;

                $("#select_department").append('<option value="'+id+'"'+selected+'>'+name+'</option>');
            });

            if(department_name) $("#select_department").trigger('change');
        })
    }else
    {
        $("#select_department").empty();
        $("#select_department").append('<option value="">-- Pilih Departemen --</option>');
    }
});


function hapus(url) 
{
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: "put",
        url: url,
        beforeSend: function() {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function(response) {
            $.unblockUI();
        },
        error: function(response) {
            $.unblockUI();
        }
    }).done(function($result) {
        $('#categoryTable').DataTable().ajax.reload();
        $("#alert_success").trigger("click", 'Data Berhasil hapus');
    });
}

