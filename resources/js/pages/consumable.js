$(document).ready( function ()
{
    var msg             = $('#msg').val();
    var is_super_admin  = $('#is_super_admin').val();
    if (msg == 'success') $("#alert_success").trigger("click", 'Data berhasil disimpan.');
    else if (msg == 'success_2') $("#alert_success").trigger("click", 'Data berhasil diubah.');
    
    $('input.input-date').datepicker({
        format: "dd/mm/yyyy",
        autoclose: true,
        todayHighlight: true
    });

    if(is_super_admin == 1)
    {
        var consumableTable = $('#consumableTable').DataTable({
            dom: 'Bfrtip',
            processing: true,
            serverSide: true,
            pageLength:10,
            scrollY:250,
            paging: true, 
            scrollX:true,
            deferRender:true,
            ajax: {
                type: 'GET',
                url: '/consumable/data',
                data: function(d) {
                     return $.extend({}, d, {
                         "company"          : $('#select_company').val(),
                         "sub_department"   : $('#select_sub_department').val(),
                         "asset_type"       : $('#select_asset_type').val(),
                     });
                }
            },
            fnCreatedRow: function (row, data, index) {
                var info = consumableTable.page.info();
                var value = index+1+info.start+data.action;
                $('td', row).eq(0).html(value);
            },
            columns: [
                {data: null, sortable: false, orderable: false, searchable: false},
                {data: 'id', name: 'id',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_1', name: 'custom_field_1',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_2', name: 'custom_field_2',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_3', name: 'custom_field_3',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_4', name: 'custom_field_4',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_5', name: 'custom_field_5',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_6', name: 'custom_field_6',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_7', name: 'custom_field_7',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_8', name: 'custom_field_8',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_9', name: 'custom_field_9',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_10', name: 'custom_field_10',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_11', name: 'custom_field_11',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_12', name: 'custom_field_12',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_13', name: 'custom_field_13',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_14', name: 'custom_field_14',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_15', name: 'custom_field_15',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_16', name: 'custom_field_16',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_17', name: 'custom_field_17',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_18', name: 'custom_field_18',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_19', name: 'custom_field_19',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_20', name: 'custom_field_20',searchable:true,visible:false,orderable:false},
                {data: 'company', name: 'company',searchable:true,orderable:true},
                {data: 'sub_department_name', name: 'sub_department_name',searchable:true,orderable:true},
                {data: 'manufacture', name: 'manufacture',searchable:true,orderable:true},
                {data: 'no_asset', name: 'no_asset',searchable:true,orderable:true},
                {data: 'asset_type', name: 'asset_type',searchable:true,orderable:true},
                {data: 'model', name: 'model',searchable:true,orderable:true},
                {data: 'serial_number', name: 'serial_number',searchable:false,orderable:true},
                {data: 'total', name: 'total',searchable:false,orderable:true},
                {data: 'qty_used', name: 'qty_used',searchable:false,orderable:true},
                {data: 'qty_available', name: 'qty_available',searchable:false,orderable:true}
            ]
        });
    }else
    {
        var consumableTable = $('#consumableTable').DataTable({
            dom: 'Bfrtip',
            processing: true,
            serverSide: true,
            pageLength:10,
            scrollY:250,
            paging: true, 
            scrollX:true,
            deferRender:true,
            ajax: {
                type: 'GET',
                url: '/consumable/data',
                data: function(d) {
                     return $.extend({}, d, {
                        "company"          : $('#select_company').val(),
                        "sub_department"   : $('#select_sub_department').val(),
                        "asset_type"       : $('#select_asset_type').val(),
                     });
                }
            },
            fnCreatedRow: function (row, data, index) {
                var info = consumableTable.page.info();
                var value = index+1+info.start+data.action;
                $('td', row).eq(0).html(value);
            },
            columns: [
                {data: null, sortable: false, orderable: false, searchable: false},
                {data: 'id', name: 'id',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_1', name: 'custom_field_1',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_2', name: 'custom_field_2',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_3', name: 'custom_field_3',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_4', name: 'custom_field_4',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_5', name: 'custom_field_5',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_6', name: 'custom_field_6',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_7', name: 'custom_field_7',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_8', name: 'custom_field_8',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_9', name: 'custom_field_9',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_10', name: 'custom_field_10',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_11', name: 'custom_field_11',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_12', name: 'custom_field_12',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_13', name: 'custom_field_13',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_14', name: 'custom_field_14',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_15', name: 'custom_field_15',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_16', name: 'custom_field_16',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_17', name: 'custom_field_17',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_18', name: 'custom_field_18',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_19', name: 'custom_field_19',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_20', name: 'custom_field_20',searchable:true,visible:false,orderable:false},
                {data: 'manufacture', name: 'manufacture',searchable:true,orderable:true},
                {data: 'no_asset', name: 'no_asset',searchable:true,orderable:true},
                {data: 'asset_type', name: 'asset_type',searchable:true,orderable:true},
                {data: 'model', name: 'model',searchable:true,orderable:true},
                {data: 'serial_number', name: 'serial_number',searchable:false,orderable:true},
                {data: 'total', name: 'total',searchable:false,orderable:true},
                {data: 'qty_used', name: 'qty_used',searchable:false,orderable:true},
                {data: 'qty_available', name: 'qty_available',searchable:false,orderable:true},
            ]
        });
    }

    var dtable = $('#consumableTable').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                console.log('asd');
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
    });

    $('#select_company').on('change',function()
    {
        dtable.draw();
    });

    $('#select_sub_department').on('change',function()
    {
        dtable.draw();
    });

    $('#select_asset_type').on('change',function()
    {
        dtable.draw();
    });
    // dtable.draw();

    
    
    $('#select_company').trigger('change');
    $('#select_sub_department').trigger('change');
    var factory_id = $('#factory_id').val();
    custom_fields = JSON.parse($('#custom_fields').val());
    details = JSON.parse($('#details').val());
    
    $('#form').submit(function (event)
    {
        event.preventDefault();
        var company         = $('#select_company').val();
        var department      = $('#select_department').val();
        var asset_type      = $('#select_asset_type').val();
        var model           = $('#model').val();
        var check_required  = checkRequired();

        if(company == '')
        {
            $("#alert_warning").trigger("click", 'Perusahaan wajib dipilih');
            return false;
        }

        if(department == '')
        {
            $("#alert_warning").trigger("click", 'Departemen wajib dipilih');
            return false;
        }

        if(asset_type == '')
        {
            $("#alert_warning").trigger("click", 'Tipe aset wajib dipilih');
            return false;
        }

        if(model == '')
        {
            $("#alert_warning").trigger("click", 'Model wajib diisi');
            return false;
        }

        if(check_required > 0) return false;
        
        bootbox.confirm("Apakah anda yakin akan menyimpan data ini ?.", function (result) {
            if(result){
                $.ajax({
                    type: 'post',
                    url: $('#form').attr('action'),
					data:new FormData($("#form")[0]),
					processData: false,
					contentType: false,
                    beforeSend: function () {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function () {
                        document.location.href = '/consumable';
                    },
                    error: function (response) {
                        $.unblockUI();
                        if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON.message);
                       
                    }
                });
            }
        });
    });

    $('#detail_form').submit(function (event)
    {
        event.preventDefault();
        var value = $('#detail_insert_value').val();
       
        if(value == '')
        {
            $("#alert_warning").trigger("click", 'Silahkan isi nilai terlebih dahulu');
            return false;
        }

        $('#editDetailModal').modal('hide');
        bootbox.confirm("Apakah anda yakin akan menyimpan data ini ?.", function (result) {
            if(result){
                $.ajax({
                    type: "POST",
                    url: $('#detail_form').attr('action'),
                    data: $('#detail_form').serialize(),
                    beforeSend: function () {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                        details = [];
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function (response) {
                       if(response.message == 'success_1') $("#alert_success").trigger("click",'data berhasil disimpan');
                       else if(response.message == 'success_2') $("#alert_success").trigger("click",'data berhasil diubah');

                       $('#consumableDetailTable').DataTable().ajax.reload();
                       $('#detail_insert_value').val('');
                       $('#detail_insert_no_asset').val('');

                       for (idx in response.details) 
                       {
                           var data = response.details[idx];
                           var input = {
                               'id': data.id,
                               'detail_value': data.detail_value,
                               'detail_no_asset': data.detail_no_asset
                           };
                           details.push(input);
                       }

                       detailRender();
                    },
                    error: function (response) {
                        $.unblockUI();
                        if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON.message);
                       
                    }
                });
            }
        });
    });

    var form_status = $('#form_status').val();
    
    if(form_status == 'create')
    {
        detailRender();
    }else if(form_status == 'edit')
    {
        var url_data_detail_consumable_asset = $('#url_data_detail_consumable_asset').val();
        
        $('#consumableDetailTable').DataTable().destroy();
        $('#consumableDetailTable tbody').empty();
        var consumableDetailTable = $('#consumableDetailTable').DataTable({
            dom: 'Bfrtip',
            processing: true,
            serverSide: true,
            pageLength:10,
            scrollY:250,
            scroller:true,
            destroy:true,
            deferRender:true,
            bFilter:true,
            ajax: {
                type: 'GET',
                url: url_data_detail_consumable_asset,
            },
            fnCreatedRow: function (row, data, index) {
                var info = consumableDetailTable.page.info();
                var value = index+1+info.start;
                $('td', row).eq(0).html(value);
            },
            columns: [
                {data: null, sortable: false, orderable: false, searchable: false},
                {data: 'value', name: 'value',searchable:true,orderable:true},
                {data: 'no_asset', name: 'no_asset',searchable:true,orderable:true},
                {data: 'action', name: 'action',searchable:false,orderable:false},
            ]
        });

        var dtable2 = $('#consumableDetailTable').dataTable().api();
        $(".dataTables_filter input")
            .unbind() // Unbind previous default bindings
            .bind("keyup", function (e) { // Bind our desired behavior
                if (e.keyCode == 13) {
                    // Call the API search function
                    dtable2.search(this.value).draw();
                }
                if (this.value == "") {
                    dtable2.search("").draw();
                }
                return;
        });
        dtable2.draw();

        detailRender();
    }

});

function hapus(url)
{
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    $.ajax({
        type: "put",
        url: url,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function () {
            $.unblockUI();
        },
        error: function () {
            $.unblockUI();
        }
    })
    .done(function () {
        $('#assetTable').DataTable().ajax.reload();
        $("#alert_success").trigger("click", 'Data Berhasil hapus');
    });
}

$('#select_sub_department').on('change',function(event,arg1)
{
    var value               = $(this).val();  
    var company_id          = $('#select_company').val();  
    var asset_type_id       = $('#asset_type_id').val();  
    var manufacture_id      = $('#manufacture_id').val();  

    if(value)
    {
        $.ajax({
            type: 'get',
            url: '/consumable/asset-type?sub_department_id='+value+'&company_id='+company_id+'&asset_type_id='+asset_type_id+'&flag=asset_type'
        })
        .done(function(response){
            var  asset_types    = response.asset_types;
            var  asset_type_id  = response.asset_type_id;
           
            $("#select_asset_type").empty();
            $("#select_asset_type").append('<option value="">-- Pilih Jenis Asset --</option>');

            $.each(asset_types,function(id,name){
                if(asset_type_id == id) var selected = 'selected';
                else var selected = null;

                $("#select_asset_type").append('<option value="'+id+'"'+selected+'>'+name+'</option>');
            });

            if(asset_type_id) $('#select_asset_type').trigger('change');
            custom_fields = [];
            render();

        });

        $.ajax({
            type: 'get',
            url: '/consumable/manufacture?sub_department_id='+value+'&company_id='+company_id+'&flag=manufacture'+'&manufacture_id='+manufacture_id
        })
        .done(function(response){
            var  manufactures   = response.manufactures;
            var  manufacture_id = response.manufacture_id;

            $.each(manufactures,function(id,name){
                if(manufacture_id == id) var selected = 'selected';
                else var selected = null;

                $("#select_manufacture").append('<option value="'+id+'"'+selected+'>'+name+'</option>');
            });
        });
    }else
    {
        $("#select_asset_type").empty();
        $("#select_asset_type").append('<option value="">-- Pilih Jenis Asset --</option>');

        $("#select_manufacture").empty();
        $("#select_manufacture").append('<option value="">-- Pilih Pabrikan --</option>');
    }
});

$('#select_asset_type').on('change',function()
{
    var value = $(this).val();    
    var consumable_asset_id = $('#consumable_asset_id').val();  

    if(value)
    {
        $.ajax({
            type: 'get',
            url: '/consumable/custom-field?asset_type_id='+value+'&consumable_asset_id='+consumable_asset_id
        })
        .done(function(response){
            custom_fields = [];
            for (idx in response) 
            {
                var data = response[idx];
                var input = {
                    'id': data.id,
                    'asset_type': data.asset_type,
                    'name': data.name,
                    'label': data.label,
                    'default_value': data.default_value,
                    'value': data.value,
                    'is_required': data.is_required,
                    'is_text': data.is_text,
                    'is_option': data.is_option,
                    'is_number': data.is_number
                };
                custom_fields.push(input);
            }
            render();
        })
    }else{
        custom_fields = [];
        render();
    }
});

$('#no_assetButtonLookup').on('click',function()
{
    var select_sub_department   = $('#select_sub_department').val();
    var select_company          = $('#select_company').val();

    if(!select_sub_department || !select_company)
    {
        $("#alert_warning").trigger("click", 'Perusahaan dan sub departemen wajib di pilih');
        $(this).val('');
        return false;
    }

    $('#no_assetModal').modal();
    //lov('barcode',select_department,select_company,'/consumable/list-of-no-asset?');
});

$('#total_current').keypress(function (e) 
{
    if (e.keyCode > 31 && (e.keyCode < 46 || e.keyCode > 57)) {
        return false;
    }
    return true;
});

$('#total_current').on('keyup',function()
{
    var total_current   = $('#total_current').val();
   
    if(total_current == '') total_current = '0';
    else total_current = total_current;
    
    var _total_all      = $('#_total_all').val();
    var total_all       = parseFloat(total_current)+parseFloat(_total_all);
    
    $('#total_all').val(total_all);
});

$('#add_detail_consumable').on('click',function()
{
    var url_store_detail_consumable_asset = $('#url_store_detail_consumable_asset').val();
    $('#detail_form').attr('action',url_store_detail_consumable_asset);
    $('#editDetailModal').modal();
});

function render() 
{
    getIndex();
    $('#custom_fields').val(JSON.stringify(custom_fields));

    var tmpl = $('#fields').html();
    Mustache.parse(tmpl);
    var data = { item: custom_fields };
    var html = Mustache.render(tmpl, data);
    $('#draw').html(html);
    bind();
    $(".select-search-2").select2();
}

function bind() 
{
    $('.custom-text').on('change', updateText);
    $('.custom-select').on('change', updateOption);
    $('.input-number').keypress(function (e) {
        if (e.keyCode > 31 && (e.keyCode < 46 || e.keyCode > 57)) {
            return false;
        }
        return true;
    });
}

function getIndex() 
{
    for (idx in custom_fields) 
    {
        custom_fields[idx]['_id'] = idx;
        custom_fields[idx]['no'] = parseInt(idx) + 1;
    }
}

function updateText()
{
    var i = $(this).data('id');
    var value = $('#'+i).val();
    var data = custom_fields[i];
    data.value = value;
    render();
}

function updateOption()
{
    var i = $(this).data('id');
    var value = $('#'+i).val();
    var data = custom_fields[i];
    data.value = value;
    var default_values = data.default_value;

    for (idx in default_values) 
    {
        var default_value = default_values[idx];
        if(value == default_value.id)
        {
            default_value.selected = true;
        }else
        {
            default_value.selected = false;
        }
    }

    render();
}

function checkRequired()
{
    var is_exists = 0;
    for (idx in custom_fields) 
    {
        var custom_field = custom_fields[idx];
        if(custom_field.is_required)
        {
            if(custom_field.value == '' || custom_field.value == null)
            {
                if(custom_field.is_option)
                {
                    $("#alert_warning").trigger("click", custom_field.label+'  wajib di pilih');
                    is_exists++;
                }else
                {
                    $("#alert_warning").trigger("click", custom_field.label+'  wajib di isi');
                    is_exists++;
                }
                
            }
        }
    }
    return is_exists++;;   
}

function detailRender() 
{
    getDetailIndex();
    $('#details').val(JSON.stringify(details));

    var tmpl    = $('#detail_table').html();
    Mustache.parse(tmpl);
    var data    = { item: details };
    var html    = Mustache.render(tmpl, data);
    $('#detail_body').html(html);
    detailbind();
}

function detailbind() 
{
    $('#save_detail_insert').off('click', addDetail).on('click', addDetail);
    $('.btn-delete-item').off('click', deleteDetail).on('click', deleteDetail);
    $('.btn-edit-item').off('click', editDetail).on('click', editDetail);
}

function addDetail()
{
    var detail_insert_value     = $('#detail_insert_value').val();
    var detail_insert_no_asset  = $('#detail_insert_no_asset').val();
    var _detail_consumable_asset_id  = $('#_detail_consumable_asset_id').val();

    if(!detail_insert_value)
    {
        $("#alert_warning").trigger("click",'silahkan isi nilainya terlebih dahulu.');
        return false;
    }

    if(_detail_consumable_asset_id)
    {
        var data                = details[_detail_consumable_asset_id];
        data.detail_value       = detail_insert_value;
        data.detail_no_asset    = detail_insert_no_asset;
    }else
    {
        var input = {
            'detail_value': detail_insert_value,
            'detail_no_asset': detail_insert_no_asset
        };
    
        if (detail_insert_value) details.push(input);
    }
    
    $('#detail_insert_value').val('');
    $('#detail_insert_no_asset').val('');
    $('#_detail_consumable_asset_id').val('');
    detailRender();
    
}

function deleteDetail()
{
    var i = $(this).data('id');

    details.splice(i, 1);
    detailRender();
    
}

function editDetail()
{
    var i       = $(this).data('id');
    var data    = details[i];

    $('#detail_insert_value').val(data.detail_value);
    $('#detail_insert_no_asset').val(data.detail_no_asset);
    $('#_detail_consumable_asset_id').val(i);
    $('#addDetailModal').modal();
}

function getDetailIndex() 
{
    for (idx in details) 
    {
        details[idx]['_id'] = idx;
        details[idx]['no'] = parseInt(idx) + 1;
    }
}

function updateModal(url)
{
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    $.ajax({
        type: "get",
        url: url,
        success: function (response) {
            var consumable_asset_detail = response.consumable_asset_detail;
            $('#detail_insert_value').val(consumable_asset_detail.value);
            $('#detail_insert_no_asset').val(consumable_asset_detail.no_asset);
            $('#_detail_consumable_asset_id').val(consumable_asset_detail.id);
            $('#detail_form').attr('action',response.url_update_detail);
        }
    })
    .done(function () {
        $('#editDetailModal').modal();
    });
}

function hapusModal(url)
{
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    $.ajax({
        type: "put",
        url: url,
        beforeSend: function () {
            details = [];
        },
        success: function (response) {
            for (idx in response) 
            {
                var data = response[idx];
                var input = {
                    'id'                : data.id,
                    'detail_value'      : data.detail_value,
                    'detail_no_asset'   : data.detail_no_asset
                };
                details.push(input);
            }
        }
    })
    .done(function () {
        $('#consumableDetailTable').DataTable().ajax.reload();
        detailRender();
    });
}

/*function lov(name,arg1,arg2, url) 
{
    var search = '#' + name + 'Search';
    var item_id = '#' + name + 'Id';
    var item_name = '#' + name + 'Name';
    var modal = '#' + name + 'Modal';
    var table = '#' + name + 'Table';
    var buttonSrc = '#' + name + 'ButtonSrc';
    var buttonDel = '#' + name + 'ButtonDel';

    function itemAjax()
    {
        var q = $(search).val();
        $(table).addClass('hidden');
        $(modal).find('.shade-screen').removeClass('hidden');
        $(modal).find('.form-search').addClass('hidden');

        $.ajax({
            url: url + '&q=' + q+ '&department_name=' + arg1+ '&factory_id=' + arg2
        })
        .done(function (data) {
            $(table).html(data);
            pagination(name);
            $(search).focus();
            $(search).val('');
            $(table).removeClass('hidden');
            $(modal).find('.shade-screen').addClass('hidden');
            $(modal).find('.form-search').removeClass('hidden');

            $(table).find('.btn-choose').on('click', chooseItem);
        });
    }

    function chooseItem()
    {
        var id = $(this).data('id');
        var name = $(this).data('name');
        var obj = $(this).data('obj');
        var e = jQuery.Event("keypress");
        e.keyCode = $.ui.keyCode.ENTER
        
        $(item_id).val(id);
        $(item_name).val(id).trigger(e);
        if (!obj || obj.length == 0)
            obj = [{}];
    }

    function pagination()
    {
        $(modal).find('.pagination a').on('click', function (e) 
        {
            var params = $(this).attr('href').split('?')[1];
            console.log(params);
            url = $(this).attr('href') + (params == undefined ? '?' : '');

            e.preventDefault();
            itemAjax();
        });
    }

    $(search).val("");
    $(buttonSrc).unbind();
    $(search).unbind();

    $(buttonSrc).on('click', itemAjax);

    $(search).on('keypress', function (e) {
        if (e.keyCode == 13)
            itemAjax();
    });

    $(buttonDel).on('click', function () {
        $(item_id).val('');
        $(item_name).val('');

    });

    //itemAjax();
}*/