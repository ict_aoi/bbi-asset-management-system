$(document).ready( function ()
{
    result = JSON.parse($('#result').val());
    $('#select_company').trigger('change');
});

$('#select_sub_department').on('change',function(event)
{
    var value           = $(this).val();  
    var company_id      = $('#select_company').val();  
    
    if(value)
    {
        $.ajax({
            type: 'get',
            url: '/asset/asset-type?sub_department_id='+value+'&company_id='+company_id+'&asset_type_id=null&flag=asset_type'
        })
        .done(function(response){
            var  asset_types = response.asset_types;
            var  asset_type_id = response.asset_type_id;

            $("#select_asset_type").empty();
            $("#select_asset_type").append('<option value="">-- Select Asset Type --</option>');

            $.each(asset_types,function(id,name){
                if(asset_type_id == id) var selected = 'selected';
                else var selected = null;

                $("#select_asset_type").append('<option value="'+id+'"'+selected+'>'+name+'</option>');
            });

            if(asset_type_id) $('#select_asset_type').trigger('change');

        })
    }else
    {
        $("#select_asset_type").empty();
        $("#select_asset_type").append('<option value="">-- Select Asset Type --</option>');
    }
});

$('#upload_button').on('click', function () 
{
    $('#upload_file').trigger('click');
});

$('#upload_file').on('change', function () 
{
    //$('#upload_file_allocation').submit();
    var current_time    = $('#time').text();
    $('#current_time').val(current_time);

    $.ajax({
        type: "post",
        url: $('#upload_file_allocation').attr('action'),
        data: new FormData(document.getElementById("upload_file_allocation")),
        processData: false,
        contentType: false,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            $("#alert_info").trigger("click", 'Upload successfully');
            result = [];
            var data        = response.data;
            var barcodes    = response.barcodes;


            for(idx in data)
            {
                var input = data[idx];
                result.push(input);
            }

            $('#list_barcodes').val(JSON.stringify(barcodes));
            $('#printBarcode').submit();
            $('#list_barcodes').val('');
            
        },
        error: function (response) {
            $.unblockUI();
            $('#upload_file_allocation').trigger('reset');
            if (response['status'] == 500)
                $("#alert_error").trigger("click", 'Please Contact ICT.');

            if (response['status'] == 422)
                $("#alert_error").trigger("click", response.responseJSON.message);

        }
    })
    .done(function () {
        $('#upload_file_allocation').trigger('reset');
        render();
    });

})

function render() 
{
    getIndex();
    $('#result').val(JSON.stringify(result));
    var tmpl = $('#upload_asset').html();
    Mustache.parse(tmpl);
    var data = { item: result };
    var html = Mustache.render(tmpl, data);
    $('#draw').html(html);
}

function getIndex() 
{
    for (idx in result) 
    {
        result[idx]['_id'] = idx;
        result[idx]['no'] = parseInt(idx) + 1;
    }
}

