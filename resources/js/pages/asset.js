
$(document).ready( function ()
{
    var msg             = $('#msg').val();
    var is_super_admin  = $('#is_super_admin').val();
    var form_status     = $('#form_status').val();

    if (msg == 'success') $("#alert_success").trigger("click", 'Data successfully saved.');
    else if (msg == 'success_2') $("#alert_success").trigger("click", 'Data successfully updated.');
    
    $('input.input-date').datepicker({
        format: "dd/mm/yyyy",
        autoclose: true,
        todayHighlight: true
    });
    
    $(".file-styled").uniform({
        fileButtonClass: 'action btn bg-warning'
    });

    if(is_super_admin == 1)
    {
        var assetTable = $('#assetTable').DataTable({
            dom: 'Bfrtip',
            processing: true,
            serverSide: true,
            pageLength:10,
            scrollY:250,
            paging: true, 
            scrollX:true,
            deferRender:true,
            ajax: {
                type: 'GET',
                url: '/asset/data',
                data: function(d) {
                     return $.extend({}, d, {
                         "company"         : $('#select_company').val(),
                         "sub_department"  : $('#select_sub_department').val(),
                         "asset_type"      : $('#select_asset_type').val(),
                         "factory"         : $('#select_factory').val(),
                         "status"          : $('#select_status_asset').val(),
                         "is_rent"         : $('#select_is_rent').val(),
                     });
                }
            },
            fnCreatedRow: function (row, data, index) 
            {
                var info = assetTable.page.info();
                var value = index+1+info.start+data.action;
                $('td', row).eq(0).html(value);
            },
            columns: [
                {data: null, sortable: false, orderable: false, searchable: false},
                {data: 'id', name: 'id',searchable:true,visible:false,orderable:false},
                {data: 'last_company_location_id', name: 'last_company_location_id',searchable:true,visible:false,orderable:false},
                {data: 'last_factory_location_id', name: 'last_factory_location_id',searchable:true,visible:false,orderable:false},
                {data: 'last_area_location', name: 'last_area_location',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_1', name: 'custom_field_1',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_2', name: 'custom_field_2',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_3', name: 'custom_field_3',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_4', name: 'custom_field_4',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_5', name: 'custom_field_5',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_6', name: 'custom_field_6',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_7', name: 'custom_field_7',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_8', name: 'custom_field_8',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_9', name: 'custom_field_9',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_10', name: 'custom_field_10',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_11', name: 'custom_field_11',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_12', name: 'custom_field_12',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_13', name: 'custom_field_13',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_14', name: 'custom_field_14',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_15', name: 'custom_field_15',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_16', name: 'custom_field_16',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_17', name: 'custom_field_17',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_18', name: 'custom_field_18',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_19', name: 'custom_field_19',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_20', name: 'custom_field_20',searchable:true,visible:false,orderable:false},
                {data: 'last_used_by', name: 'last_used_by',searchable:true,visible:false,orderable:false},
                {data: 'last_department_location_id', name: 'last_department_location_id',searchable:true,visible:false,orderable:false},
                {data: 'last_sub_department_location_id', name: 'last_sub_department_location_id',searchable:true,visible:false,orderable:false},
                {data: 'origin_factory_name', name: 'origin_factory_name',searchable:true,orderable:true},
                {data: 'origin_department_name', name: 'origin_department_name',searchable:true,orderable:true},
                {data: 'origin_sub_department_name', name: 'origin_sub_department_name',searchable:true,orderable:true},
                {data: 'no_inventory', name: 'no_inventory',searchable:true,orderable:true},
                {data: 'no_inventory_manual', name: 'no_inventory_manual',searchable:true,orderable:true},
                {data: 'erp_no_asset', name: 'erp_no_asset',searchable:true,orderable:true},
                {data: 'asset_type', name: 'asset_type',searchable:true,orderable:true},
                {data: 'model', name: 'model',searchable:true,orderable:true},
                {data: 'serial_number', name: 'serial_number',searchable:true,orderable:true},
                {data: 'status', name: 'status',searchable:false,orderable:true},
                {data: 'no_agreement', name: 'no_agreement',searchable:false,orderable:true},
                {data: 'last_company_name', name: 'last_company_name',searchable:false,orderable:true},
                {data: 'last_factory_name', name: 'last_factory_name',searchable:false,orderable:true},
                {data: 'last_department_name', name: 'last_department_name',searchable:false,orderable:true},
                {data: 'last_sub_department_name', name: 'last_sub_department_name',searchable:false,orderable:true},
                {data: 'last_used_by', name: 'last_used_by',searchable:false,orderable:true},
                {data: 'last_area_location', name: 'last_area_location',searchable:false,orderable:true},
            ]
        });
    }else
    {
        var assetTable = $('#assetTable').DataTable({
            dom: 'Bfrtip',
            processing: true,
            serverSide: true,
            pageLength:10,
            scrollY:250,
            paging: true, 
            scrollX:true,
            deferRender:true,
            ajax: {
                type: 'GET',
                url: '/asset/data',
                data: function(d) {
                     return $.extend({}, d, {
                         "company"         : $('#select_company').val(),
                         "sub_department"  : $('#select_sub_department').val(),
                         "asset_type"      : $('#select_asset_type').val(),
                         "factory"         : $('#select_factory').val(),
                         "status"          : $('#select_status_asset').val(),
                         "is_rent"         : $('#select_is_rent').val(),
                     });
                }
            },
            fnCreatedRow: function (row, data, index) {
                var info = assetTable.page.info();
                var value = index+1+info.start+data.action;
                $('td', row).eq(0).html(value);
            },
            columns: [
                {data: null, sortable: false, orderable: false, searchable: false},
                {data: 'id', name: 'id',searchable:true,visible:false,orderable:false},
                {data: 'last_company_location_id', name: 'last_company_location_id',searchable:true,visible:false,orderable:false},
                {data: 'last_factory_location_id', name: 'last_factory_location_id',searchable:true,visible:false,orderable:false},
                {data: 'last_area_location', name: 'last_area_location',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_1', name: 'custom_field_1',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_2', name: 'custom_field_2',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_3', name: 'custom_field_3',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_4', name: 'custom_field_4',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_5', name: 'custom_field_5',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_6', name: 'custom_field_6',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_7', name: 'custom_field_7',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_8', name: 'custom_field_8',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_9', name: 'custom_field_9',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_10', name: 'custom_field_10',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_11', name: 'custom_field_11',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_12', name: 'custom_field_12',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_13', name: 'custom_field_13',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_14', name: 'custom_field_14',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_15', name: 'custom_field_15',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_16', name: 'custom_field_16',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_17', name: 'custom_field_17',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_18', name: 'custom_field_18',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_19', name: 'custom_field_19',searchable:true,visible:false,orderable:false},
                {data: 'custom_field_20', name: 'custom_field_20',searchable:true,visible:false,orderable:false},
                {data: 'last_used_by', name: 'last_used_by',searchable:true,visible:false,orderable:false},
                {data: 'last_department_location_id', name: 'last_department_location_id',searchable:true,visible:false,orderable:false},
                {data: 'last_sub_department_location_id', name: 'last_sub_department_location_id',searchable:true,visible:false,orderable:false},
                {data: 'no_inventory', name: 'no_inventory',searchable:true,orderable:true},
                {data: 'no_inventory_manual', name: 'no_inventory_manual',searchable:true,orderable:true},
                {data: 'erp_no_asset', name: 'erp_no_asset',searchable:true,orderable:true},
                {data: 'asset_type', name: 'asset_type',searchable:true,orderable:true},
                {data: 'model', name: 'model',searchable:true,orderable:true},
                {data: 'serial_number', name: 'serial_number',searchable:true,orderable:true},
                {data: 'status', name: 'status',searchable:false,orderable:true},
                {data: 'no_agreement', name: 'no_agreement',searchable:false,orderable:true},
                {data: 'last_company_name', name: 'last_company_name',searchable:false,orderable:true},
                {data: 'last_factory_name', name: 'last_factory_name',searchable:false,orderable:true},
                {data: 'last_department_name', name: 'last_department_name',searchable:false,orderable:true},
                {data: 'last_sub_department_name', name: 'last_sub_department_name',searchable:false,orderable:true},
                {data: 'last_used_by', name: 'last_used_by',searchable:false,orderable:true},
                {data: 'last_area_location', name: 'last_area_location',searchable:false,orderable:true},
            ]
        });
    }

    var dtable = $('#assetTable').dataTable().api();
    $("#assetTable.dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
    });
    dtable.draw();

    custom_fields = JSON.parse($('#custom_fields').val());
    
    if(form_status == 'index')
    {
        $('#select_company').on('change',function()
        {
            dtable.draw();
        });

        $('#select_factory').on('change',function()
        {
            dtable.draw();
        });

        $('#select_sub_department').on('change',function()
        {
            dtable.draw();
        });

        $('#select_asset_type').on('change',function()
        {
            dtable.draw();
        });

        $('#select_status_asset').on('change',function()
        {
            dtable.draw();
        });

        $('#select_is_rent').on('change',function()
        {
            dtable.draw();
        });

        $('#select_company').trigger('change');
        $('#select_factory').trigger('change');
        $('#select_sub_department').trigger('change');

        $('#select_export_company').trigger('change');
        $('#select_export_factory').trigger('change');
        $('#select_export_department').trigger('change');
        $('#select_barcode_department').trigger('change');
    }else if(form_status == 'create')
    {
        $('#select_factory').trigger('change');
        $('#select_company').trigger('change');
        $('#select_sub_department').trigger('change');
        lovEpAsset('erp_no_asset','/asset/erp-asset-picklist?');
    }else if(form_status == 'edit')
    {
        $('#select_factory').trigger('change');
        $('#select_company').trigger('change');
        $('#select_sub_department').trigger('change');
        $('#select_asset_type').trigger('change');
        var asset_group_id = $('#asset_group_id').val();
        if(asset_group_id) $('#select_erp_asset_group').val(asset_group_id).trigger('change');
        $('#select_erp_asset_group').prop('disabled', true);
        lovEpAsset('erp_no_asset','/asset/erp-asset-picklist?');
    }
    

    $('#form').submit(function (event)
    {
        event.preventDefault();
        var factory         = $('#select_factory').val();
        var sub_department  = $('#select_sub_department').val();
        var asset_type      = $('#select_asset_type').val();
        var erp_no_asset    = $('#erp_no_asset').val();
        var model           = $('#model').val();
        var serial_number   = $('#serial_number').val();
        var check_required  = checkRequired();

        var current_time    = $('#time').text();
        $('#current_time').val(current_time);

       
        if(factory == '')
        {
            $("#alert_warning").trigger("click", 'Please select factory first');
            return false;
        }

        if(sub_department == '')
        {
            $("#alert_warning").trigger("click", 'Please select subdepartment first');
            return false;
        }

        if(asset_type == '')
        {
            $("#alert_warning").trigger("click", 'Please select asset type first');
            return false;
        }

        /*if(no_asset == '')
        {
            $("#alert_warning").trigger("click", 'Nomor asset wajib diisi');
            return false;
        }*/

        if(model == '')
        {
            $("#alert_warning").trigger("click", 'Please type modal first');
            return false;
        }

        if(serial_number == '')
        {
            $("#alert_warning").trigger("click", 'Please type serial number first');
            return false;
        }
        
        if(check_required > 0) return false;
        
        bootbox.confirm("Are you sure want to save this data ?.", function (result) 
        {
            if(result){
                $.ajax({
                    type: 'post',
                    url: $('#form').attr('action'),
					data:new FormData($("#form")[0]),
					processData: false,
					contentType: false,
                    beforeSend: function () {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function (response) {
                        $('#list_barcodes').val(JSON.stringify(response));
                        $('#printBarcode').submit();
                        $('#list_barcodes').val('');
                        document.location.href = '/asset';
                    },
                    error: function (response) {
                        $.unblockUI();
                        if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON.message);
                       
                    }
                });
            }
        });
    });
});

function hapus(url)
{
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    $.ajax({
        type: "put",
        url: url,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function () {
            $.unblockUI();
        },
        error: function () {
            $.unblockUI();
        }
    })
    .done(function () {
        $('#assetTable').DataTable().ajax.reload();
        $("#alert_success").trigger("click", 'Data Berhasil hapus');
    });
}

$('#submit_button').on('click',function()
{
    $('#form').trigger('submit');
});

$('#select_erp_asset_group').on('change',function()
{
    var asset_group_id      = $(this).val();
    var asset_group_name    = $(this).select2('data')[0].text;
    $('#asset_group_id').val(asset_group_id);
    $('#asset_group_name').val(asset_group_name);
});

$('#receiving_date').on('change',function()
{
    lovEpAsset('erp_no_asset','/asset/erp-asset-picklist?');
});

$('#erp_no_assetButtonLookup').on('click',function()
{
    $('#erp_no_assetModal').modal();
});

$('#erp_no_assetName').on('click',function()
{
    $('#erp_no_assetModal').modal();
});

$('#select_factory').on('change',function(event,arg1)
{
    var value           = $(this).val();  
    
    if(value)
    {
        $.ajax({
            type: 'get',
            url: '/asset/status?sub_department_id='+null+'&factory_id='+value
        })
        .done(function(response){
            var status = response.status;
            

            $("#select_status_asset").empty();
            $("#select_status_asset").append('<option value="">-- Pilih Status Asset --</option>');

            $.each(status,function(id,name)
            {
                if(status == id) var selected = 'selected';
                else var selected = null;

                $("#select_status_asset").append('<option value="'+id+'"'+selected+'>'+name+'</option>');
            });
        })
    }else
    {
        $("#select_status_asset").empty();
        $("#select_status_asset").append('<option value="">-- Pilih Status Asset --</option>');
    }
});


$('#price').keypress(function (e) {

    if (e.keyCode > 31 && (e.keyCode < 46 || e.keyCode > 57)) {
        return false;
    }
    return true;
});


$('#select_sub_department').on('change',function(event,arg1)
{
    var value               = $(this).val();  
    var rent_agreement_id   = $('#rent_agreement_id').val();  
    var asset_type_id       = $('#asset_type_id').val();  
    var manufacture_id      = $('#manufacture_id').val();  
    var factory_id          = $('#select_factory').val();  
    var sub_department_id   = (value)? value:arg1;

    if(sub_department_id)
    {
        $.ajax({
            type: 'get',
            url: '/asset/asset-type?sub_department_id='+sub_department_id+'&asset_type_id='+asset_type_id+'&flag=asset_type'
        })
        .done(function(response){
            var  asset_types = response.asset_types;
            var  asset_type_id = response.asset_type_id;
           
            $("#select_asset_type").empty();
            $("#select_asset_type").append('<option value="">-- Select Asset Type --</option>');

            $.each(asset_types,function(id,name){
                if(asset_type_id == id) var selected = 'selected';
                else var selected = null;

                $("#select_asset_type").append('<option value="'+id+'"'+selected+'>'+name+'</option>');
            });

            if(asset_type_id) $('#select_asset_type').trigger('change');
            custom_fields = [];
            render();

        });

        $.ajax({
            type: 'get',
            url: '/asset/manufacture?sub_department_id='+sub_department_id+'&flag=manufacture'+'&manufacture_id='+manufacture_id
        })
        .done(function(response){
            var  manufactures = response.manufactures;
            var  manufacture_id = response.manufacture_id;

            $.each(manufactures,function(id,name){
                if(manufacture_id == id) var selected = 'selected';
                else var selected = null;

                $("#select_manufacture").append('<option value="'+id+'"'+selected+'>'+name+'</option>');
            });
        });

        $.ajax({
            type: 'get',
            url: '/asset/status?sub_department_id='+sub_department_id+'&factory_id='+factory_id
        })
        .done(function(response){
            var status = response.status;
            

            $("#select_status_asset").empty();
            $("#select_status_asset").append('<option value="">-- Select Asset Status --</option>');

            $.each(status,function(id,name){
                if(status == id) var selected = 'selected';
                else var selected = null;

                $("#select_status_asset").append('<option value="'+id+'"'+selected+'>'+name+'</option>');
            });
        });


        $.ajax({
            type: 'get',
            url: '/asset/rent-agreement?sub_department_id='+sub_department_id+'&factory_id='+factory_id
        })
        .done(function(response)
        {
            var rent_agreement = response.rent_agreement;
            
            $("#select_rent_agreement").empty();
            $("#select_rent_agreement").append('<option value="">-- Select Rent Agreement --</option>');

            $.each(rent_agreement,function(id,name){
                if(rent_agreement_id == id) var selected = 'selected';
                else var selected = null;

                $("#select_rent_agreement").append('<option value="'+id+'"'+selected+'>'+name+'</option>');
            
                $('#select_rent_agreement').trigger('change');
            });
        });
    }else
    {
        $("#select_asset_type").empty();
        $("#select_asset_type").append('<option value="">-- Select Asset Type --</option>');

        $("#select_manufacture").empty();
        $("#select_manufacture").append('<option value="">-- Select Manufacture --</option>');

        $("#select_rent_agreement").empty();
        $("#select_rent_agreement").append('<option value="">-- Select Rent Agreement --</option>');
    }
});


$('#select_rent_agreement').on('change',function(event,arg1)
{
    var value           = $(this).val();  
    
    if(value)
    {
        $("#supplier_name").attr("readonly", false); 
    }else
    {
        $("#supplier_name").attr("readonly", true); 
    }
});

$('#select_asset_type').on('change',function()
{
    var value       = $(this).val();    
    var asset_id    = $('#asset_id').val();    
    
    if(value)
    {
        $.ajax({
            type: 'get',
            url: '/asset/custom-field?asset_type_id='+value+'&asset_id='+asset_id
        })
        .done(function(response){
            custom_fields = [];
            for (idx in response) 
            {
                var data = response[idx];
                var input = 
                {
                    'id'                : data.id,
                    'asset_type'        : data.asset_type,
                    'name'              : data.name,
                    'label'             : data.label,
                    'default_value'     : data.default_value,
                    'value'             : data.value,
                    'is_required'       : data.is_required,
                    'is_text'           : data.is_text,
                    'is_option'         : data.is_option,
                    'is_number'         : data.is_number,
                    'is_lookup'         : data.is_lookup,
                    'custom_lookup_id'  : data.custom_lookup_id,
                    'lookup_id'         : data.lookup_id,
                    'detail_lookup_id'  : data.detail_lookup_id
                };
                custom_fields.push(input);
            }
            render();
        })
    }else{
        custom_fields = [];
        render();
    }
});

$('#select_barcode_department').on('change',function(event,arg1)
{
    var value           = $(this).val();  
    
    if(value)
    {
        $.ajax({
            type: 'get',
            url: '/asset/asset-type?sub_department_id='+value+'&asset_type_id=-1&flag=asset_type'
        })
        .done(function(response){
            var  asset_types = response.asset_types;
            
            $("#select_barcode_asset_type").empty();
            $("#select_barcode_asset_type").append('<option value="">-- Select Asset Type --</option>');

            $.each(asset_types,function(id,name){
                var selected = null;
                $("#select_barcode_asset_type").append('<option value="'+id+'"'+selected+'>'+name+'</option>');
            });


        });

       
    }else
    {
        $("#select_barcode_asset_type").empty();
        $("#select_barcode_asset_type").append('<option value="">-- Select Asset Type --</option>');
    }
});

$('#export_report_button').on('click', function () 
{
    $('#download_export_report_excel').submit();
});

$('#select_export_department').on('change',function(event,arg1)
{
    var asset_type_id   = $('#select_export_asset_type').val();  
    var department_id   = $(this).val();  

    if(department_id)
    {
        $.ajax({
            type: 'get',
            url: '/asset/asset-type?sub_department_id='+department_id+'&asset_type_id='+asset_type_id+'&flag=asset_type'
        })
        .done(function(response){
            var  asset_types    = response.asset_types;
            var  asset_type_id  = response.asset_type_id;
           
            $("#select_export_asset_type").empty();
            $("#select_export_asset_type").append('<option value="">-- Select Asset Type --</option>');

            $.each(asset_types,function(id,name){
                if(asset_type_id == id) var selected = 'selected';
                else var selected = null;

                $("#select_export_asset_type").append('<option value="'+id+'"'+selected+'>'+name+'</option>');
            });
        });
    }else
    {
        $("#select_export_asset_type").empty();
        $("#select_export_asset_type").append('<option value="">-- Select Asset Type --</option>');
    }
});

function render() 
{
    getIndex();
    $('#custom_fields').val(JSON.stringify(custom_fields));

    var tmpl = $('#fields').html();
    Mustache.parse(tmpl);
    var data = { item: custom_fields };
    var html = Mustache.render(tmpl, data);
    $('#draw').html(html);
    bind();
    $(".select-search-2").select2();
}

function bind() 
{
    $('.lookup-value').on('click', lookupValue);
    $('.custom-text').on('change', updateText);
    $('.custom-lookup-name').on('change', updateLookupText);
    $('.custom-lookup').on('change', updateLookup);
    $('.custom-lookup-detail').on('change', updateLookupDetail);
    $('.custom-select').on('change', updateOption);
    $('.input-number').keypress(function (e) {
        if (e.keyCode > 31 && (e.keyCode < 46 || e.keyCode > 57)) {
            return false;
        }
        return true;
    });
}

function getIndex() 
{
    for (idx in custom_fields) 
    {
        custom_fields[idx]['_id'] = idx;
        custom_fields[idx]['no'] = parseInt(idx) + 1;
    }
}

function updateText()
{
    var i = $(this).data('id');
    var value = $('#'+i).val();
    var data = custom_fields[i];
    data.value = value;
    render();
}


function updateLookupText()
{
    var i = $(this).data('id');
    var value = $('#lookup_name_'+i).val();
    var data = custom_fields[i];
    data.value = value;
    $('#custom_fields').val(JSON.stringify(custom_fields));
}

function updateLookup()
{
    var i = $(this).data('id');
    var value = $('#lookup_id_'+i).val();
    var data = custom_fields[i];
    data.lookup_id = value;
    $('#custom_fields').val(JSON.stringify(custom_fields));
}

function updateLookupDetail()
{
    var i = $(this).data('id');
    var value = $('#lookup_detail_id_'+i).val();
    var data = custom_fields[i];
    data.detail_lookup_id = value;
    $('#custom_fields').val(JSON.stringify(custom_fields));
}

function updateOption()
{
    var i = $(this).data('id');
    var value = $('#'+i).val();
    var data = custom_fields[i];
    data.value = value;
    var default_values = data.default_value;

    for (idx in default_values) 
    {
        var default_value = default_values[idx];
        if(value == default_value.id)
        {
            default_value.selected = true;
        }else
        {
            default_value.selected = false;
        }
    }

    render();
}

function lookupValue()
{
    var i                   = $(this).data('id');
    var custom_lookup_id    = $('#custom_lookup_table_id_'+i).val();

    $('#lookup_modal_'+i).modal();
    lov(i,custom_lookup_id,'/asset/lookup?')
}

function checkRequired()
{
    var is_exists = 0;
    for (idx in custom_fields) 
    {
        var custom_field = custom_fields[idx];
        if(custom_field.is_required)
        {
            if(custom_field.value == '' || custom_field.value == null)
            {
                if(custom_field.is_option)
                {
                    $("#alert_warning").trigger("click", custom_field.label+'  is required');
                    is_exists++;
                }else
                {
                    $("#alert_warning").trigger("click", custom_field.label+'  is required');
                    is_exists++;
                }
                
            }
        }
    }
    return is_exists++;;   
}

function lov(_id,custom_lookup_id, url) 
{
    var search = '#lookup_search_'+_id;
    var lookup_id = '#lookup_id_'+_id;
    var lookup_detail_id = '#lookup_detail_id_'+_id;
    var item_name = '#lookup_name_'+_id;
    var modal = '#lookup_modal_'+_id;
    var table = '#lookup_table_'+_id;
    var buttonSrc = '#lookup_button_src_'+_id;
    var buttonDel = '#' + name + 'ButtonDel';

    function itemAjax()
    {
        var q = $(search).val();
        $(table).addClass('hidden');
        $(modal).find('.shade-screen').removeClass('hidden');
        $(modal).find('.form-search').addClass('hidden');

        $.ajax({
            url: url + '&q=' + q+ '&custom_lookup_id=' + custom_lookup_id
        })
        .done(function (data) {
            $(table).html(data);
            pagination(name);
            $(search).focus();
            $(search).val('');
            $(table).removeClass('hidden');
            $(modal).find('.shade-screen').addClass('hidden');
            $(modal).find('.form-search').removeClass('hidden');

            $(table).find('.btn-choose').on('click', chooseItem);
        });
    }

    function chooseItem()
    {
        var id          = $(this).data('id');
        var detail_id   = $(this).data('detailid');
        var name        = $(this).data('name');
       
        $(lookup_id).val(id).trigger('change');
        $(lookup_detail_id).val(detail_id).trigger('change');
        $(item_name).val(name).trigger('change');
    }

    function pagination()
    {
        $(modal).find('.pagination a').on('click', function (e) 
        {
            var params = $(this).attr('href').split('?')[1];
            url = $(this).attr('href') + (params == undefined ? '?' : '');

            e.preventDefault();
            itemAjax();
        });
    }

    $(search).val("");
    $(buttonSrc).unbind();
    $(search).unbind();

    $(buttonSrc).on('click', itemAjax);

    $(search).on('keypress', function (e) 
    {
        if (e.keyCode == 13)
            itemAjax();
    });

    $(buttonDel).on('click', function () {
        $(item_id).val('');
        $(item_name).val('');

    });

    itemAjax();
}

function lovEpAsset(name, url) 
{
    var search          = '#' + name + 'Search';
    var item_id         = '#' + name + 'Id';
    var item_name       = '#' + name + 'Name';
    var modal           = '#' + name + 'Modal';
    var table           = '#' + name + 'Table';
    var buttonSrc       = '#' + name + 'ButtonSrc';
    var buttonDel       = '#' + name + 'ButtonDel';
    var receiving_date  = $('#receiving_date').val();
    
    function itemAjax()
    {
        var q = $(search).val();
        $(table).addClass('hidden');
        $(modal).find('.shade-screen').removeClass('hidden');
        $(modal).find('.form-search').addClass('hidden');

        $.ajax({
            url: url + 'q=' + q+ '&receiving_date=' + receiving_date
        })
        .done(function (data) 
        {
            $(table).html(data);
            pagination(name);
            $(search).focus();
            $(table).removeClass('hidden');
            $(modal).find('.shade-screen').addClass('hidden');
            $(modal).find('.form-search').removeClass('hidden');

            $(table).find('.btn-choose').on('click', chooseItem);
        });
    }

    function chooseItem()
    {
        var id                  = $(this).data('id');
        var name                = $(this).data('name');
        var erp_item_id         = $(this).data('itemid');
        var item_code           = $(this).data('itemcode');
        var asset_group_id      = $(this).data('assetgroupid');
        var receiving_date      = $(this).data('receivingdate');
        var document_no         = $(this).data('posupplier');
        var item_desc           = $(this).data('itemdesc');
        var supplier_name       = $(this).data('suppliername');
       
        $(item_id).val(id);
        $(item_name).val(name);
        $('#item_code').val(item_code);
        $('#item_id').val(erp_item_id);
        $('#supplier_name').val(supplier_name);
        $('#_receiving_date').val(receiving_date);
        $('#document_no').val(document_no);
        $('#item_desc').val(item_desc);
        $('#select_erp_asset_group').val(asset_group_id).trigger('change');
        $('#select_erp_asset_group').prop('disabled', true);
    }

    function pagination()
    {
        $(modal).find('.pagination a').on('click', function (e) 
        {
            var params = $(this).attr('href').split('?')[1];
            url = $(this).attr('href') + (params == undefined ? '?' : '');

            e.preventDefault();
            itemAjax();
        });
    }

    $(buttonSrc).unbind();
    $(search).unbind();

    $(buttonSrc).on('click', itemAjax);

    $(search).on('keypress', function (e) 
    {
        if (e.keyCode == 13)
            itemAjax();
    });

    $(buttonDel).on('click', function () 
    {
        $(item_id).val('');
        $(item_name).val('');
        $('#item_code').val('');
        $('#item_id').val('');
        $('#asset_group_id').val('');
        $('#_receiving_date').val('');
        $('#asset_group_name').val('');
        $('#supplier_name').val('');
        $('#document_no').val('');
        $('#item_desc').val('');
        $('#select_erp_asset_group').val('').trigger('change');
        $('#select_erp_asset_group').prop('disabled', false)

    });

    itemAjax();
}
