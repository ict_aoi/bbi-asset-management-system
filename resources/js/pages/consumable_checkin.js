 $(document).ready(function() 
 {
    $('input.input-date').datepicker({
        format: "dd/mm/yyyy",
        autoclose: true,
        todayHighlight: true
    });
    
     var checkinTable = $('#checkinTable').DataTable({
         dom: 'Bfrtip',
         processing: true,
         serverSide: true,
         pageLength: 100,
         deferRender: true,
         ajax: {
             type: 'GET',
             url: '/consumable/check-in-data',
             data: function(d) {
                 return $.extend({}, d, {
                     "consumable_asset_id": $('#consumable_asset_id').val()
                 });
             }
         },

         fnCreatedRow: function(row, data, index) {
             var info = checkinTable.page.info();
             var value = index + 1 + info.start;
             $('td', row).eq(0).html(value);
         },
         columns: [{
             data: null,
             sortable: false,
             orderable: false,
             searchable: false
         }, 
         {
            data: 'movement_date',
            name: 'movement_date',
            searchable: false,
            orderable: false
        },
         {
             data: 'to',
             name: 'to',
             searchable: false,
             visible: true,
             orderable: true
         }, {
             data: 'action',
             name: 'action',
             searchable: false,
             orderable: false
         }, ],

     });

     var dtable = $('#checkinTable').dataTable().api();
     $(".dataTables_filter input")
         .unbind() // Unbind previous default bindings
         .bind("keyup", function(e) { // Bind our desired behavior
             // If the user pressed ENTER, search
             if (e.keyCode == 13) {
                 // Call the API search function
                 dtable.search(this.value).draw();
             }
             // Ensure we clear the search if they backspace far enough
             if (this.value == "") {
                 dtable.search("").draw();
             }
             return false;
         });
 });

 function backTo(url) {
     $.ajaxSetup({
         headers: {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
     });

     $.ajax({
         type: "post",
         url: url,
         beforeSend: function() {
             $.blockUI({
                 message: '<i class="icon-spinner4 spinner"></i>',
                 overlayCSS: {
                     backgroundColor: '#fff',
                     opacity: 0.8,
                     cursor: 'wait'
                 },
                 css: {
                     border: 0,
                     padding: 0,
                     backgroundColor: 'transparent'
                 }
             });
         },
         success: function() {
             $.unblockUI();
         },
         error: function() {
             $.unblockUI();
         }
     }).done(function() {
         $('#checkinTable').DataTable().ajax.reload();
         $("#alert_success").trigger("click", 'Data Berhasil dikembalikan');
     });
 }