<?php

return [
    'avatar' => storage_path() . '/app/avatar',
    'asset' => storage_path() . '/app/asset',
    'excel_asset'   => storage_path() . '/app/report/asset',
];
