<?php namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class Asset extends Model
{
    use Uuids;
    public $timestamps      = false;
    public $incrementing    = false;
    protected $guarded      = ['id'];
    protected $dates        = ['created_at','last_sto_date','updated_at','start_rent_date','end_rent_date','return_date','receiving_date','last_movement_date'];
    protected $fillable     = ['status'
    ,'price'
    ,'is_rent'
    ,'no_rent'
    ,'start_rent_date'
    ,'last_user_movement_id'
    ,'end_rent_date'
    ,'return_date'
    ,'remark_rent'
    ,'duration_rent_in_days'
    ,'budget_id'
    ,'last_asset_movement_id'
    ,'created_at'
    ,'updated_at'
    ,'last_movement_date'
    ,'origin_company_location_id'
    ,'origin_factory_location_id'
    ,'origin_department_location_id'
    ,'origin_sub_department_location_id'
    ,'department_id'
    ,'sub_department_id'
    ,'last_used_by'
    ,'last_company_location_id'
    ,'last_factory_location_id'
    ,'last_department_location_id'
    ,'last_sub_department_location_id'
    ,'source_company_location_id'
    ,'source_factory_location_id'
    ,'source_department_location_id'
    ,'source_sub_department_location_id'
    ,'last_area_location'
    ,'asset_type_id'
    ,'company_id'
    ,'factory_id'
    ,'area_id'
    ,'department'
    ,'manufacture_id'
    ,'referral_code'
    ,'sequence'
    ,'erp_no_asset'
    ,'erp_asset_id'
    ,'erp_asset_group_id'
    ,'erp_asset_group_name'
    ,'erp_item_id'
    ,'erp_item_code'
    ,'is_from_sto_date'
    ,'model'
    ,'description'
    ,'purchase_number'
    ,'supplier_name'
    ,'receiving_date'
    ,'total'
    ,'qty_used'
    ,'qty_available'
    ,'image'
    ,'last_used_by'
    ,'last_no_kk'
    ,'last_no_bea_cukai'
    ,'no_bea_cukai'
    ,'custom_field_1','custom_field_2','custom_field_3','custom_field_4','custom_field_5','custom_field_6','custom_field_7','custom_field_8','custom_field_9','custom_field_10'
    ,'custom_field_11','custom_field_12','custom_field_13','custom_field_14','custom_field_15','custom_field_16','custom_field_17','custom_field_18','custom_field_19','custom_field_20'
    ,'barcode'
    ,'serial_number'
    ,'no_inventory_manual'
    ,'no_inventory'
    ,'create_user_id'
    ,'last_sto_date'
    ,'last_sto_date'
    ,'delete_at'
    ,'sto_user_id'
    ,'barcode_handover'
    ,'mapping_id'
    ,'delete_user_id'];
    
    public function getSupplierNameAttribute($value)
    {
        return ucwords($value);
    }

    public function getErpItemCodeAttribute($value)
    {
        return strtoupper($value);
    }

    public function getErpNoAssetAttribute($value)
    {
        return strtoupper($value);
    }

    public function getPurchaseNumberAttribute($value)
    {
        return strtoupper($value);
    }

    public function assetType()
    {
        return $this->belongsTo('App\Models\AssetType');
    }

    public function company()
    {
        return $this->belongsTo('App\Models\Company','company_id');
    }

    public function factory()
    {
        return $this->belongsTo('App\Models\Factory','factory_id');
    }

    public function department()
    {
        return $this->belongsTo('App\Models\Department','department_id');
    }

    public function subDepartment()
    {
        return $this->belongsTo('App\Models\SubDepartment','sub_department_id');
    }

    public function area()
    {
        return $this->belongsTo('App\Models\Area','last_area_location');
    }

    public function rentalAgreement()
    {
        return $this->belongsTo('App\Models\RentalAgreement','no_rent');
    }

    public function lastCompany()
    {
        return $this->belongsTo('App\Models\Company','last_company_location_id');
    }

    public function lastFactory()
    {
        return $this->belongsTo('App\Models\Factory','last_factory_location_id');
    }

    public function lastDepartment()
    {
        return $this->belongsTo('App\Models\Department','last_department_location_id');
    }

    public function lastSubDepartment()
    {
        return $this->belongsTo('App\Models\SubDepartment','last_sub_department_location_id');
    }
    
    public function originCompany()
    {
        return $this->belongsTo('App\Models\Company','origin_company_location_id');
    }

    public function originFactory()
    {
        return $this->belongsTo('App\Models\Factory','origin_factory_location_id');
    }

    public function originDepartment()
    {
        return $this->belongsTo('App\Models\Department','origin_department_location_id');
    }

    public function originSubDepartment()
    {
        return $this->belongsTo('App\Models\SubDepartment','origin_sub_department_location_id');
    }

    public function assetMovement()
    {
        return $this->hasMany('App\Models\AssetMovement','asset_id');
    }
    
    public function sourceCompany()
    {
        return $this->belongsTo('App\Models\Company','source_company_location_id');
    }

    public function sourceFactory()
    {
        return $this->belongsTo('App\Models\Factory','source_factory_location_id');
    }

    public function sourceDepartment()
    {
        return $this->belongsTo('App\Models\Department','source_department_location_id');
    }

    public function sourceSubDepartment()
    {
        return $this->belongsTo('App\Models\Department','source_sub_department_location_id');
    }

    public function manufacture()
    {
        return $this->belongsTo('App\Models\Manufacture');
    }
    
    public function stoUser()
    {
        return $this->belongsTo('App\Models\User','sto_user_id');
    }

    public function createUser()
    {
        return $this->belongsTo('App\Models\User','create_user_id');
    }

    public function deleteUser()
    {
        return $this->belongsTo('App\Models\User','delete_user_id');
    }
}
