<?php namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class RentalAgreement extends Model
{
    use Uuids;
	public $timestamps      = false;
	public $incrementing    = false;
    protected $guarded      = ['id'];
    protected $dates        = ['created_at','accounting_confirmation_date','updated_at','date_registration_in','date_registration','deleted_at','start_date','end_date'];
    protected $fillable     = [ 'factory_id',
        'destination_factory_id',
        'sub_department_id',
        'budget_id',
        'parent_rental_agreement_id',
        'no_agreement',
        'duration_in_day',
        'price',
        'start_date',
        'end_date',
        'description',
        'created_at',
        'updated_at',
        'created_user_id',
        'updated_user_id',
        'deleted_user_id',
        'is_agreement_for_rental',
        'is_extended',
        'is_internal',
        'bc_type_id', // out
        'bc_type_in_id',
        'no_bc_in',
        'no_bc', // out
        'no_aju_in',
        'no_aju', // out
        'no_registration', // out
        'no_registration_in',
        'date_registration', // out
        'date_registration_in',
        'return_user_id',
        'return_date',
        'receive_date',
        'updated_user_exim_id',
        'updated_user_exim_in_id',
        'updated_bc_in_date',
        'updated_bc_date',
        'destination_other',
        'mapping_id',
        'is_return_to_source',
        'status_obsolete',
        'buyer_name',
        'cost_of_goods',
        'accounting_confirmation_date',
        'accounting_user_id',
    ];

    public function budget()
	{
        return $this->belongsTo('App\Models\Budget','budget_id');
    }

    public function subDepartment()
	{
        return $this->belongsTo('App\Models\SubDepartment','sub_department_id');
    }

    public function factory()
	{
        return $this->belongsTo('App\Models\Factory','factory_id');
    }

    public function getNoAgreementAttribute($value)
	{
	    return strtoupper($value);
    }

    public function getNoBCAttribute($value)
	{
	    return strtoupper($value);
    }

    public function accountingUser()
	{
        return $this->belongsTo('App\Models\User','accounting_user_id');
    }

    public function assetMovement()
    {
        return $this->hasMany('App\Models\AssetMovement','rental_agreement_id');
    }

    static function getItem($id)
    {
        return AssetMovement::whereNotNull('rental_agreement_id')
        ->whereNull('cancel_obsolete_date')
        ->whereNull('delete_user_id')
        ->where('rental_agreement_id',$id)
        ->get();
    }
}
