<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;


class Barcode extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $guarded = ['id'];
    protected $fillable = ['barcode','referral_code','sequence','fixing'];
}
