<?php namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class AssetMovement extends Model
{
    use Uuids;
    public $timestamps      = false;
    public $incrementing    = false;
    protected $guarded      = ['id'];
    protected $fillable     = ['asset_id'
        ,'status'
        ,'from_company_location'
        ,'from_factory_location'
        ,'from_department_location'
        ,'from_sub_department_location'
        ,'from_employee_nik'
        ,'from_employee_name'
        ,'to_company_location'
        ,'to_factory_location'
        ,'to_department_location'
        ,'to_sub_department_location'
        ,'to_area_location'
        ,'to_employee_nik'
        ,'to_employee_name'
        ,'no_kk'
        ,'no_bea_cukai'
        ,'movement_date'
        ,'created_at'
        ,'updated_at'
        ,'note'
        ,'create_user_id'
        ,'technician_name'
        ,'repaired_date'
        ,'rental_agreement_id'
        ,'is_handover_asset'
        ,'delete_user_id'
        ,'cancel_obsolete_date'
        ,'cancel_obsolete_user_id'
        ,'cancel_obsolete_note'    
    ];

    protected $dates    = ['created_at','movement_date'];
    
    public function asset()
    {
        return $this->belongsTo('App\Models\Asset');
    }

    public function calendar()
    {
        return $this->belongsTo('App\Models\Calendar');
    }
    
    public function rentalAgreement()
    {
        return $this->belongsTo('App\Models\RentalAgreement');
    }
}
