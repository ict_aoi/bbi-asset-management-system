<?php namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $guarded = ['id'];
	protected $fillable = ['name','description','company_id','department_name','is_check_out','create_user_id','delete_at','delete_user_id'];
	protected $dates    = ['created_at'];
}
