<?php namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;


class DesignTable extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $guarded = ['id'];
	protected $fillable = ['table_name','order','column_name','alias','type_data','length','not_null','is_primary_key','is_foreign_key','description','is_column_custom'];
	protected $dates    = ['created_at'];
}
