<?php namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
	use Uuids;
	public $incrementing = false;
	protected $guarded   = ['id'];
	protected $fillable  = ['code','name','description','create_user_id','delete_at','delete_user_id','mapping_id'];
	protected $dates     = ['created_at'];
	
	public function getNameAttribute($value)
	{
	    return ucwords($value);
	}

	public function factory()
    {
        return $this->hasMany('App\Models\Factory');
    }
	
}
