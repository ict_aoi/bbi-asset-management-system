<?php namespace App\Models;

use DB;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class AssetType extends Model
{
    use Uuids;
    public $timestamps      = false;
    public $incrementing    = false;
    protected $guarded      = ['id'];
    protected $dates        = ['created_at'];
    protected $fillable     = ['company_id'
        ,'department_id'
        ,'sub_department_id'
        ,'code'
        ,'name'
        ,'description'
        ,'is_consumable'
        ,'create_user_id'
        ,'delete_at'
        ,'delete_user_id'
        ,'erp_category_id'
        ,'erp_category_name'
        ,'erp_asset_group_id'
        ,'erp_asset_group_name'
        ,'mapping_id'
    ];

    
    
    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }
    
    public function department()
    {
        return $this->belongsTo('App\Models\Department','department_id');
    }

    public function assetInformationFields($asset_type_id,$is_consumable)
    {
        $data = db::table('asset_type_information_v')
        ->where('asset_type_id',$asset_type_id)
        ->whereNotIn('name',[
            'department_name',
            'no_inventory',
            'last_sub_department_location_id',
            'source_sub_department_location_id',
            'barcode',
            'budget_id',
            'is_rent',
            'last_asset_movement_id',
            'last_user_movement_id',
            'last_sto_date',
            'sto_user_id',
            'start_rent_date',
            'end_rent_date',
            'return_date',
            'remark_rent',
            'duration_rent_in_days',
            'area_id',
            'total',
            'qty_used',
            'is_from_sto_date',
            'erp_item_id',
            'image',
            'qty_available',
            'referral_code',
            'sequence',
            'last_used_by',
            'source_company_location_id',
            'status',
            'source_department_location_id',
            'source_factory_location_id',
            'last_department_location_id',
            'created_at',
            'updated_at',
            'delete_at',
            'create_user_id',
            'delete_user_id',
            'id',
            'erp_asset_id',
            'erp_asset_group_id',
            'last_movement_date',
            'last_company_location_id',
            'last_factory_location_id',
            'last_department_location',
            'last_area_location',
            'last_no_kk',
            'last_no_bea_cukai',
        ]);
        if($is_consumable) $data = $data->where('table_name','consumable_assets');
        else $data = $data->where('table_name','assets');

        $data = $data->get();

        return $data;
    }

    public function assetRentInformationFields($asset_type_id,$is_consumable)
    {
        $data = db::table('asset_type_information_v')
        ->where('asset_type_id',$asset_type_id)
        ->whereNotIn('name',[
            'department_name',
            'no_inventory',
            'barcode',
            'area_id',
            'total',
            'qty_used',
            'is_from_sto_date',
            'erp_item_id',
            'image',
            'qty_available',
            'referral_code',
            'sequence',
            'last_used_by',
            'erp_asset_group_name',
            'no_inventory_manual',
            'erp_no_asset',
            'no_bea_cukai',
            'purchase_number',
            'erp_item_code',
            'is_rent',
            'last_asset_movement_id',
            'source_company_location_id',
            'status',
            'source_department_location_id',
            'source_factory_location_id',
            'last_department_location_id',
            'created_at',
            'updated_at',
            'delete_at',
            'create_user_id',
            'delete_user_id',
            'id',
            'erp_asset_id',
            'erp_asset_group_id',
            'last_movement_date',
            'last_company_location_id',
            'last_factory_location_id',
            'last_department_location',
            'last_area_location',
            'last_no_kk',
            'last_no_bea_cukai',
        ]);
        if($is_consumable) $data = $data->where('table_name','consumable_assets');
        else $data = $data->where('table_name','assets');

        $data = $data->get();

        return $data;
        //return $this->hasMany('App\Models\AssetInformationField');
    }

    public function getNameAttribute($value)
	{
	    return ucwords($value);
	}
}
