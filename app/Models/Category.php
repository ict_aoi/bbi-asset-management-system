<?php namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $guarded = ['id'];
	protected $fillable = ['name','description','company_id','department_name','create_user_id','delete_at','delete_user_id'];
    protected $dates    = ['created_at'];
    
    public function assetTypes()
    {
        return $this->hasMany('App\Models\AssetType');
    }
    public function factory()
	{
        return $this->belongsTo('App\Models\Factory');
    }

    public function getNameAttribute($value)
    {
        return ucwords($value);
    }

}
