<?php namespace App\Models\Erp;

use Illuminate\Database\Eloquent\Model;

class DepartmentDev extends Model
{
    protected $connection   = 'erp_dev';
    protected $guarded      = ['id'];
    protected $table        = 'jz_department';
}
