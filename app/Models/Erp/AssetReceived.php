<?php namespace App\Models\Erp;

use Illuminate\Database\Eloquent\Model;

class AssetReceived extends Model
{
    protected $connection   = 'erp_live';
    //protected $connection   = 'erp_dev';
    protected $guarded      = ['id'];
    protected $table        = 'jz_dias_asset_received';
}
