<?php namespace App\Models\Erp;

use Illuminate\Database\Eloquent\Model;

class DepartmentLive extends Model
{
    protected $connection   = 'erp_live';
    protected $guarded      = ['id'];
    protected $table        = 'jz_department';
}
