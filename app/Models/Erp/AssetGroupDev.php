<?php namespace App\Models\Erp;

use Illuminate\Database\Eloquent\Model;

class AssetGroupDev extends Model
{
    protected $connection   = 'erp_live';
    protected $guarded      = ['id'];
    protected $table        = 'jz_asset_group';
}
