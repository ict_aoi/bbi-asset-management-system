<?php namespace App\Models\Erp;

use Illuminate\Database\Eloquent\Model;

class CategoryAsset extends Model
{
    protected $connection   = 'erp_live';
    protected $guarded      = ['id'];
    protected $table        = 'jz_product_category_asset';
}
