<?php namespace App\Models\Erp;

use Illuminate\Database\Eloquent\Model;

class AccountingFactoryLocation extends Model
{
    protected $connection   = 'erp_live';
    protected $guarded      = ['id'];
    protected $table        = 'jz_accounting_location';
}
