<?php namespace App\Models\Erp;

use Illuminate\Database\Eloquent\Model;

class AssetDev extends Model
{
    protected $connection   = 'erp_dev';
    protected $guarded      = ['id'];
    protected $table        = 'jz_dias_master_asset';
}
