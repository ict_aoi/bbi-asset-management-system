<?php namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class Temporary extends Model
{
    use Uuids;
    public $timestamp       = false;
    public $incrementing    = false;
    protected $guarded      = ['id'];
	protected $fillable     = ['column_1','column_2','column_3','column_4','column_5'];
	protected $dates        = ['created_at'];
}
