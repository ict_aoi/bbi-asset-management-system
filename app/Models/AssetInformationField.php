<?php namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class AssetInformationField extends Model
{
    use Uuids;
    public $timestamps      = false;
    public $incrementing    = false;
    protected $guarded      = ['id'];
	protected $fillable     = ['created_at','updated_at','asset_type_id','is_custom','ordering','type','name','label','value','required','lookup_id','create_user_id','delete_at','delete_user_id'];
    protected $dates        = ['created_at'];
    
    public function assetType()
    {
        return $this->belongsTo('App\Models\AssetType');
    }
}
