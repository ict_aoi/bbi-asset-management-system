<?php namespace App\Models\Absensi;

use Illuminate\Database\Eloquent\Model;

class DepartmentBbi extends Model
{
    protected $connection   = 'absence_bbi';
    protected $guarded      = ['id'];
    protected $table        = 'm_department';
}
