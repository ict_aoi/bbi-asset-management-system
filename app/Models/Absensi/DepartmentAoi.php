<?php namespace App\Models\Absensi;

use Illuminate\Database\Eloquent\Model;

class DepartmentAoi extends Model
{
    protected $connection   = 'absence_aoi';
    protected $guarded      = ['id'];
    protected $table        = 'm_department';
}
