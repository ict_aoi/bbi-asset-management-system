<?php namespace App\Models\Absensi;

use Illuminate\Database\Eloquent\Model;

class AbsensiAoi extends Model
{
    protected $connection   = 'absence_aoi';
    protected $guarded      = ['id'];
    protected $table        = 'get_employee';
}
