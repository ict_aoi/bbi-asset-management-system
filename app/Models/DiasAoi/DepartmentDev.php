<?php namespace App\Models\DiasAoi;

use Illuminate\Database\Eloquent\Model;

class DepartmentDev extends Model
{
    protected $connection   = 'dias_aoi_dev';
    protected $guarded      = ['id'];
    protected $table        = 'departments';

    public function getIdAttribute($value)
	{
	    return $value;
    }
}
