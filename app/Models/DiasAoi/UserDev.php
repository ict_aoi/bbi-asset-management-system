<?php namespace App\Models\DiasAoi;

use Illuminate\Database\Eloquent\Model;

class UserDev extends Model
{
    protected $connection   = 'dias_aoi_dev';
    protected $guarded      = ['id'];
    protected $table        = 'users';

    public function getIdAttribute($value)
	{
	    return $value;
    }
}
