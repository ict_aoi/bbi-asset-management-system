<?php namespace App\Models\DiasAoi;

use Illuminate\Database\Eloquent\Model;

class AssetInformationFieldLive extends Model
{
    protected $connection   = 'dias_aoi_live';
    protected $guarded      = ['id'];
    protected $table        = 'asset_information_fields';

    public function getIdAttribute($value)
	{
	    return $value;
    }
}
