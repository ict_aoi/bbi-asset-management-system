<?php namespace App\Models\DiasAoi;

use Illuminate\Database\Eloquent\Model;

class FactoryLive extends Model
{
    protected $connection   = 'dias_aoi_live';
    protected $guarded      = ['id'];
    protected $table        = 'factories';

    public function getIdAttribute($value)
	{
	    return $value;
    }

    public function getCompanyIdAttribute($value)
	{
	    return $value;
    }
}
