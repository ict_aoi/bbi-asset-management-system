<?php namespace App\Models\DiasAoi;

use Illuminate\Database\Eloquent\Model;

class AssetTypeLive extends Model
{
    protected $connection   = 'dias_aoi_live';
    protected $guarded      = ['id'];
    protected $table        = 'asset_types';

    public function getIdAttribute($value)
	{
	    return $value;
    }
}
