<?php namespace App\Models\DiasAoi;

use Illuminate\Database\Eloquent\Model;

class AssetDev extends Model
{
    protected $connection   = 'dias_aoi_dev';
    protected $guarded      = ['id'];
    protected $table        = 'assets';

    public function getIdAttribute($value)
	{
	    return $value;
    }
}
