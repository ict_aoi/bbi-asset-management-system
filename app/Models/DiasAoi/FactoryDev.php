<?php namespace App\Models\DiasAoi;

use Illuminate\Database\Eloquent\Model;

class FactoryDev extends Model
{
    protected $connection   = 'dias_aoi_dev';
    protected $guarded      = ['id'];
    protected $table        = 'factories';

    public function getIdAttribute($value)
	{
	    return $value;
    }

    public function getCompanyIdAttribute($value)
	{
	    return $value;
    }
}
