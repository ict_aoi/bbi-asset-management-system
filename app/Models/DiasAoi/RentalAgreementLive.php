<?php namespace App\Models\DiasAoi;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class RentalAgreementLive extends Model
{
    use Uuids;
    public $timestamps      = false;
	public $incrementing    = false;
    protected $connection   = 'dias_aoi_live';
    protected $guarded      = ['id'];
    protected $table        = 'rental_agreements';
    protected $fillable     = [ 'factory_id',
        'department_id',
        'budget_id',
        'parent_rental_agreement_id',
        'no_agreement',
        'duration_in_day',
        'price',
        'start_date',
        'end_date',
        'description',
        'created_at',
        'updated_at',
        'created_user_id',
        'updated_user_id',
        'deleted_user_id',
        'is_agreement_for_rental',
        'is_internal',
        'is_extended',
        'no_bc',
        'no_aju',
        'no_registration',
        'date_registration',
        'bc_type_id',
        'updated_user_exim_id',
        'destination_factory_id',
        'bc_type_in_id',
        'return_user_id',
        'return_date',
        'updated_bc_date',
        'no_bc_in',
        'no_aju_in',
        'no_registration_in',
        'date_registration_in',
        'updated_user_exim_in_id',
        'updated_bc_in_date',
        'receive_date',
        'mapping_id',
        'is_return_to_source',
    ];

    public function getIdAttribute($value)
	{
	    return $value;
    }
}
