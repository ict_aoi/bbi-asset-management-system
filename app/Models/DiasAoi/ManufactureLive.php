<?php namespace App\Models\DiasAoi;

use Illuminate\Database\Eloquent\Model;

class ManufactureLive extends Model
{
    protected $connection   = 'dias_aoi_live';
    protected $guarded      = ['id'];
    protected $table        = 'manufactures';

    public function getIdAttribute($value)
	{
	    return $value;
    }
}
