<?php namespace App\Models\DiasAoi;

use Illuminate\Database\Eloquent\Model;

class CompanyLive extends Model
{
    protected $connection   = 'dias_aoi_live';
    protected $guarded      = ['id'];
    protected $table        = 'companies';

    public function getIdAttribute($value)
	{
	    return $value;
    }
}
