<?php namespace App\Models\DiasAoi;

use Illuminate\Database\Eloquent\Model;

class AssetMovementLive extends Model
{
    protected $connection   = 'dias_aoi_live';
    protected $guarded      = ['id'];
    protected $table        = 'asset_movements';

    public function getIdAttribute($value)
	{
	    return $value;
    }
}
