<?php namespace App\Models\DiasAoi;

use Illuminate\Database\Eloquent\Model;

class UserLive extends Model
{
    protected $connection   = 'dias_aoi_live';
    protected $guarded      = ['id'];
    protected $table        = 'users';

    public function getIdAttribute($value)
	{
	    return $value;
    }
}
