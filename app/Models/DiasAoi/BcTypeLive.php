<?php namespace App\Models\DiasAoi;

use Illuminate\Database\Eloquent\Model;

class BcTypeLive extends Model
{
    protected $connection   = 'dias_aoi_live';
    protected $guarded      = ['id'];
    protected $table        = 'bc_types';

    public function getIdAttribute($value)
	{
	    return $value;
    }
}
