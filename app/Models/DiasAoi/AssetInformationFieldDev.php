<?php namespace App\Models\DiasAoi;

use Illuminate\Database\Eloquent\Model;

class AssetInformationFieldDev extends Model
{
    protected $connection   = 'dias_aoi_dev';
    protected $guarded      = ['id'];
    protected $table        = 'asset_information_fields';

    public function getIdAttribute($value)
	{
	    return $value;
    }
}
