<?php namespace App\Models\DiasAoi;

use Illuminate\Database\Eloquent\Model;

class CompanyDev extends Model
{
    protected $connection   = 'dias_aoi_dev';
    protected $guarded      = ['id'];
    protected $table        = 'companies';

    public function getIdAttribute($value)
	{
	    return $value;
    }
}
