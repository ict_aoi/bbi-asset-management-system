<?php namespace App\Models\DiasAoi;

use Illuminate\Database\Eloquent\Model;

class DepartmentLive extends Model
{
    protected $connection   = 'dias_aoi_live';
    protected $guarded      = ['id'];
    protected $table        = 'departments';

    public function getIdAttribute($value)
	{
	    return $value;
    }
}
