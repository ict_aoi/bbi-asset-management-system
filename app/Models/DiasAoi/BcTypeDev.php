<?php namespace App\Models\DiasAoi;

use Illuminate\Database\Eloquent\Model;

class BcTypeDev extends Model
{
    protected $connection   = 'dias_aoi_dev';
    protected $guarded      = ['id'];
    protected $table        = 'bc_types';

    public function getIdAttribute($value)
	{
	    return $value;
    }
}
