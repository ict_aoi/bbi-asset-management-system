<?php namespace App\Models\DiasAoi;

use Illuminate\Database\Eloquent\Model;

class AssetLive extends Model
{
    protected $connection   = 'dias_aoi_live';
    protected $guarded      = ['id'];
    protected $table        = 'assets';

    public function getIdAttribute($value)
	{
	    return $value;
    }
}
