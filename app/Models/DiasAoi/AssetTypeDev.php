<?php namespace App\Models\DiasAoi;

use Illuminate\Database\Eloquent\Model;

class AssetTypeDev extends Model
{
    protected $connection   = 'dias_aoi_dev';
    protected $guarded      = ['id'];
    protected $table        = 'asset_types';

    public function getIdAttribute($value)
	{
	    return $value;
    }
}
