<?php namespace App\Models\DiasAoi;

use Illuminate\Database\Eloquent\Model;

class ManufactureDev extends Model
{
    protected $connection   = 'dias_aoi_dev';
    protected $guarded      = ['id'];
    protected $table        = 'manufactures';

    public function getIdAttribute($value)
	{
	    return $value;
    }
}
