<?php namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class ConsumableAsset extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $guarded = ['id'];
    protected $dates    = ['created_at','receiving_date'];
    protected $fillable = ['barcode'
    ,'company_id'
    ,'department_name'
    ,'asset_type_id'
    ,'manufacture_id'
    ,'no_asset'
    ,'no_inventory'
    ,'no_inventory_manual'
    ,'serial_number'
    ,'sub_department_id'
    ,'model'
    ,'description'
    ,'purchase_number'
    ,'supplier_name'
    ,'receiving_date'
    ,'total'
    ,'qty_used'
    ,'qty_available'
    ,'custom_field_1','custom_field_2','custom_field_3','custom_field_4','custom_field_5','custom_field_6','custom_field_7','custom_field_8','custom_field_9','custom_field_10'
    ,'custom_field_11','custom_field_12','custom_field_13','custom_field_14','custom_field_15','custom_field_16','custom_field_17','custom_field_18','custom_field_19','custom_field_20'
    ,'referral_code'
    ,'sequence'
    ,'create_user_id','delete_at','delete_user_id'];
    
   
    public function getModelAttribute($value)
    {
        return strtoupper($value);
    }

    public function getSerialNumberAttribute($value)
    {
        return strtoupper($value);
    }

    public function assetType()
    {
        return $this->belongsTo('App\Models\AssetType');
    }

    public function company()
    {
        return $this->belongsTo('App\Models\Company');
    }
    public function manufacture()
    {
        return $this->belongsTo('App\Models\Manufacture');
    }

    public function consumableAssetDetail()
    {
        return $this->hasMany('App\Models\ConsumableAssetDetail')->whereNull('delete_at');
    }

    public function consumableAssetMovement()
    {
        return $this->hasMany('App\Models\ConsumableAssetMovement')->whereNull('delete_at');
    }

    public function subDepartment()
    {
        return $this->belongsTo('App\Models\SubDepartment','sub_department_id');
    }
}
