<?php namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class ConsumableAssetMovement extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $guarded = ['id'];
    protected $dates    = ['created_at'];
    protected $fillable = ['consumable_asset_id'
    ,'consumable_asset_detail_id'
    ,'asset_id'
    ,'status'
    ,'from_company_location'
    ,'from_factory_location'
    ,'from_department_location'
    ,'from_employee_nik'
    ,'from_employee_name'
    ,'to_company_location'
    ,'to_factory_location'
    ,'to_department_location'
    ,'to_area_location'
    ,'to_employee_nik'
    ,'to_employee_name'
    ,'movement_date'
    ,'delete_at'
    ,'delete_user_id'
    ,'is_return'
    ,'is_using_in_custom_field'
    ,'system_note'
    ];
    
    public function consumableAsset()
    {
        return $this->belongsTo('App\Models\ConsumableAsset');
    }

    public function consumableAssetDetail()
    {
        return $this->belongsTo('App\Models\ConsumableAssetDetail');
    }

    public function asset()
    {
        return $this->belongsTo('App\Models\Asset');
    }
}
