<?php namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class Calendar extends Model
{
    use Uuids;
    public $timestamps      = false;
    public $incrementing    = false;
    protected $guarded      = ['id'];
    protected $dates        = ['created_at','event_start','event_end'];
    protected $fillable     = ['event_name'
        ,'event_start'
        ,'event_end'
        ,'created_user_id'
        ,'updated_user_id'
        ,'deleted_user_id'
        ,'created_at'
        ,'updated_at'
        ,'deleted_at'
    ];
}
