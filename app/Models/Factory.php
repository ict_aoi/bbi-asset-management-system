<?php namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class Factory extends Model
{
	use Uuids;
	public $incrementing = false;
	protected $guarded   = ['id'];
	protected $fillable  = ['company_id','code','name','description','delete_at','erp_accounting_location_id','mapping_id'];
	protected $dates     = ['created_at'];


	public function company()
	{
        return $this->belongsTo('App\Models\Company');
    }

    public function getNameAttribute($value)
	{
	    return ucwords($value);
	}

	

	
}
