<?php namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class SubDepartment extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $dates     = ['created_at'];
	protected $guarded   = ['id'];
	protected $fillable  = ['department_id'
		,'sub_department_absensi_id'
		,'code'
		,'name'
		,'description'
        ,'delete_at'
        ,'mapping_id'
        ,'is_other'
    ];
        
    public function department()
    {
        return $this->belongsTo('App\Models\Department','department_id');
    }

    public function getNameAttribute($value)
    {
        return ucwords($value);
    }
}
