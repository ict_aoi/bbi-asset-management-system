<?php namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;


class User extends Authenticatable
{
    use Notifiable;
    use EntrustUserTrait;

    protected $fillable = [
        'name'
        ,'email'
        ,'password'
        ,'photo'
        ,'sex'
        ,'nik'
        ,'photo'
        ,'email_verified_at'
        ,'department_id'
        ,'sub_department_id'
        ,'company_id'
        ,'factory_id'
        ,'is_super_admin'
        ,'create_user_id'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function roles()
    {
        return $this->belongsToMany('App\Models\Role','role_user','user_id','role_id');
    }

    public function getNameAttribute($value)
    {
        return ucwords($value);
    }

    public function department()
	{
        return $this->belongsTo('App\Models\Department','department_id');
    }

    public function subDepartment()
	{
        return $this->belongsTo('App\Models\SubDepartment','sub_department_id');
    }

    public function factory()
	{
        return $this->belongsTo('App\Models\Factory','factory_id');
    }

}
