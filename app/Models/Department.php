<?php namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $dates     = ['created_at'];
	protected $guarded   = ['id'];
	protected $fillable  = ['factory_id'
		,'department_absensi_id'
		,'code'
		,'name'
		,'description'
		,'erp_account_locator_id'
		,'erp_account_locator_name'
		,'erp_oprational_cost_type'
		,'department_erp_id'
		,'delete_at'
		,'mapping_id'
		,'is_other'
	];

   
    
    public function factory()
	{
        return $this->belongsTo('App\Models\Factory','factory_id');
    }

    public function getNameAttribute($value)
	{
	    return ucwords($value);
	}
}
