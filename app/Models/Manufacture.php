<?php namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class Manufacture extends Model
{
    use Uuids;
    public $timestamps      = false;
    public $incrementing    = false;
    protected $dates        = ['created_at'];
    protected $guarded      = ['id'];
    protected $fillable     = ['logo',
        'name',
        'company_id',
        'department_id',
        'sub_department_id',
        'description',
        'create_user_id',
        'created_at',
        'updated_at',
        'delete_at',
        'delete_user_id',
        'mapping_id'
    ];

    public function department()
    {
        return $this->belongsTo('App\Models\Department','department_id');
    }

    public function subDepartment()
    {
        return $this->belongsTo('App\Models\SubDepartment','sub_department_id');
    }
	
}
