<?php namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class Budget extends Model
{
    use Uuids;
	public $timestamps      = false;
	public $incrementing    = false;
    protected $guarded      = ['id'];
    protected $dates        = ['created_at','updated_at','deleted_at'];
    protected $fillable     = ['factory_id',
        'sub_department_id',
        'unique_id',
        'name',
        'description',
        'periode_year',
        'deleted_at',
        'created_at',
        'updated_at',
        'created_user_id',
        'updated_user_id',
        'deleted_user_id'
    ];

    public function factory()
	{
        return $this->belongsTo('App\Models\Factory','factory_id');
    }

    public function subDepartment()
	{
        return $this->belongsTo('App\Models\SubDepartment','sub_deparment_id');
    }

    public function getNameAttribute($value)
	{
	    return ucwords($value);
    }
    
    public function getPeriodeMonthAttribute($value)
	{
	    return ucwords($value);
    }
    
    public function detailBudget()
    {
        return $this->hasMany('App\Models\DetailBudget','budget_id');
    }
}


