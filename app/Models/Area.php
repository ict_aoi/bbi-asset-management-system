<?php namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    use Uuids;
	public $timestamps 	 = false;
	public $incrementing = false;
	protected $guarded   = ['id'];
	protected $dates     = ['created_at'];
	protected $fillable  = ['factory_id'
		,'code'
		,'name'
		,'description'
		,'delete_at'
		,'create_user_id'
		,'is_area_stock'
		,'delete_user_id'];
    
    
    public function factory()
	{
        return $this->belongsTo('App\Models\Factory');
    }

    public function getNameAttribute($value)
	{
	    return ucwords($value);
	}
}
