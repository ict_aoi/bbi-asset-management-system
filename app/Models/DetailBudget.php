<?php namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class DetailBudget extends Model
{
    use Uuids;
	public $timestamps      = false;
	public $incrementing    = false;
    protected $guarded      = ['id'];
    protected $dates        = ['created_at','updated_at','deleted_at'];
    protected $fillable     = [ 'budget_id',
        'periode_month',
        'periode_by_date_start',
        'periode_by_date_end',
        'total',
        'created_at',
        'updated_at',
        'created_user_id',
        'updated_user_id',
        'deleted_user_id'
    ];

    public function getPeriodeMonthAttribute($value)
	{
	    return ucwords($value);
    }
    

    public function budget()
	{
        return $this->belongsTo('App\Models\Budget','budget_id');
    }

}
