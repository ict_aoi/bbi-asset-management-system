<?php namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class BcType extends Model
{
    use Uuids;
	public $timestamps      = false;
	public $incrementing    = false;
    protected $guarded      = ['id'];
    protected $dates        = ['created_at','updated_at','deleted_at'];
    protected $fillable     = ['code',
        'description',
        'created_at',
        'updated_at',
        'deleted_at',
        'created_user_id',
        'updated_user_id',
        'deleted_user_id'
    ];

    public function getCodeAttribute($value)
	{
	    return strtoupper($value);
	}
}
