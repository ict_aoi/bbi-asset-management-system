<?php namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class ConsumableAssetDetail extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $guarded = ['id'];
    protected $dates    = ['created_at'];
    protected $fillable = ['consumable_asset_id'
    ,'value'
    ,'no_asset'
    ,'create_user_id'
    ,'delete_at'
    ,'delete_user_id'
    ];
    
    public function consumableAsset()
    {
        return $this->belongsTo('App\Models\ConsumableAsset');
    }

    public function consumableAssetMovement()
    {
        return $this->hasMany('App\Models\ConsumableAssetMovement');
    }
}
