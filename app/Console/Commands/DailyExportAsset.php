<?php namespace App\Console\Commands;

use DB;
use File;
use Excel;
use Config;
use Carbon\Carbon;
use Illuminate\Console\Command;

use App\Models\Asset;
use App\Models\AssetType;
use App\Models\Temporary;
use App\Models\Scheduler;
use App\Models\SubDepartment;
use App\Models\ConsumableAsset;
use App\Models\ConsumableAssetDetail;
use App\Models\AssetInformationField;

class DailyExportAsset extends Command
{
    protected $signature = 'exportAsset:daily';
    protected $description = 'Export Asset Daily';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $data  = Temporary::where('column_2','sync_export_asset_per_department')->get();
        $location = Config::get('storage.excel_asset');
        if (!File::exists($location)) File::makeDirectory($location, 0777, true);

        $alphabets = array(
            0 => 'A',
            1 => 'B',
            2 => 'C',
            3 => 'D',
            4 => 'E',
            5 => 'F',
            6 => 'G',
            7 => 'H',
            8 => 'I',
            9 => 'J',
            10 => 'K',
            11 => 'L',
            12 => 'M',
            13 => 'N',
            14 => 'O',
            15 => 'P',
            16 => 'Q',
            17 => 'R',
            18 => 'S',
            19 => 'T',
            20 => 'U',
            21 => 'V',
            22 => 'W',
            23 => 'X',
            24 => 'Y',
            25 => 'Z',
            26 => 'AA',
            27 => 'AB',
            28 => 'AC',
            29 => 'AD',
            30 => 'AE',
            31 => 'AF',
            32 => 'AG',
            33 => 'AH',
            34 => 'AI',
            35 => 'AJ',
            36 => 'AK',
            37 => 'AL',
            38 => 'AM',
            39 => 'AN',
            40 => 'AO',
            41 => 'AP',
            42 => 'AQ',
            43 => 'AR',
            44 => 'AS',
            45 => 'AT',
            46 => 'AU',
            47 => 'AV',
            48 => 'AW',
            49 => 'AX',
            50 => 'AY',
            51 => 'AZ',
            52 => 'BA',
            53 => 'BB',
            54 => 'BC',
            55 => 'BD',
            56 => 'BE',
            57 => 'BF',
            58 => 'BG',
            59 => 'BH',
            60 => 'BI',
            61 => 'BJ',
            62 => 'BK',
            63 => 'BL',
            64 => 'BM',
            65 => 'BN',
            66 => 'BO',
            67 => 'BP',
            68 => 'BQ',
            69 => 'BR',
            70 => 'BS',
            71 => 'BT',
            72 => 'BU',
            73 => 'BV',
            74 => 'BW',
            75 => 'BX',
            76 => 'BY',
            77 => 'BZ',
        );

        foreach ($data as $key => $value) 
        {
            $sub_department_id = $value->column_1;
            $is_schedule_on_going = Scheduler::where('job','daily_export_'.$sub_department_id)
            ->where('status','ongoing')
            ->exists();

            if(!$is_schedule_on_going)
            {
                $new_scheduler = Scheduler::FirstOrCreate([
                    'job'       => 'daily_export_'.$sub_department_id,
                    'status'    => 'ongoing'
                ]);

                $this->info('START DAILY EXPORT ASSET AT '.carbon::now());
                $this->setStartJob($new_scheduler);
                $this->dailyJob($value,$location,$alphabets);
                $this->setStatus($new_scheduler,'done');
                $this->setEndJob($new_scheduler); 
                $this->info('DONE DAILY EXPORT ASSET AT '.carbon::now());

                $value->delete();
            }else
            {
                $this->info('SYNC DAILY EXPORT ASSET SEDANG BERJALAN');
            }

        }
    }

    private function dailyJob($value,$location,$alphabets)
    {
        $department     = SubDepartment::find($value->column_1);
        $asset_types    = AssetType::where([
            ['sub_department_id',$value->column_1],
            ['is_consumable',false],
        ])
        ->whereNull('delete_at')
        ->get();

        if(count($asset_types) > 0)
        {
            return Excel::create('asset_'.str_slug($department->name),function ($excel) use($asset_types,$alphabets) 
            {
                foreach ($asset_types as $key => $asset_type) 
                {
                    $asset_type_informations = AssetInformationField::where([
                        ['asset_type_id', $asset_type->id],
                    ])
                    ->whereNotIn('name',[
                        'department_name',
                        'last_department_location',
                        'created_at',
                        'updated_at',
                        'referral_code',
                        'sequence',
                        'image',
                        'delete_at',
                        'create_user_id',
                        'source_company_location_id',
                        'source_factory_location_id',
                        'source_department_location_id',
                        'delete_user_id',
                        'is_from_sto_date',
                        'qty_available',
                        'qty_used',
                        'total',
                        'erp_item_id',
                        'erp_asset_group_id',
                        
                    ])
                    ->whereNull('delete_at')
                    ->orderby('label','asc')
                    ->get();

                    if (strlen($asset_type->name) > 29) 
                    {
                        $sheet_name = str_slug(substr_replace($asset_type->name, '..', 30));
                    }else
                    {
                        $sheet_name = str_slug($asset_type->name);
                    }
                    $excel->sheet($sheet_name, function($sheet) use ($asset_type_informations,$alphabets)
                    {
                        foreach ($asset_type_informations as $key => $value) 
                        {
                            $sheet->setCellValue($alphabets[$key].'1',$value->label);
                            $j = 2;

                            $assets = Asset::whereNull('delete_at')
                            ->where('asset_type_id',$value->asset_type_id)
                            ->orderby('created_at','asc')
                            ->get();
                            
                            foreach ($assets as $key_2 => $asset) 
                            {
                                $column_name        = $value->name;
                                $column_type        = $value->type;
                                $curr_value         = ($asset)? $asset->$column_name:null;

                                if($column_name == 'asset_type_id') $curr_value                     = $asset->assetType->name;
                                if($column_name == 'company_id') $curr_value                        = $asset->company->name;
                                if($column_name == 'factory_id') $curr_value                        = $asset->factory->name;
                                if($column_name == 'department_id') $curr_value                     = $asset->department->name;
                                if($column_name == 'sub_department_id') $curr_value                 = $asset->subDepartment->name;
                                if($column_name == 'manufacture_id') $curr_value                    = ($asset->manufacture_id)? $asset->manufacture->name : null;
                                if($column_name == 'last_company_location_id') $curr_value          = $asset->lastCompany->name;
                                if($column_name == 'last_factory_location_id') $curr_value          = $asset->lastFactory->name;
                                if($column_name == 'last_department_location_id') $curr_value       = $asset->lastDepartment->name;
                                if($column_name == 'last_sub_department_location_id') $curr_value   = $asset->lastSubDepartment->name;
                                if($column_name == 'last_movement_date') $curr_value                = ($asset->last_movement_date ? $asset->last_movement_date->format('d/m/Y H:i:s') : null);
                                
                                
                                if($asset->assetType->is_consumable == false)
                                {
                                    if($column_name == 'total') $curr_value = 1;
                                }
                    
                                if($column_type == 'lookup')
                                {
                                    $splits                     = explode('|',$curr_value);
                                    if($curr_value)
                                    {
                                        $custom_lookup_id           = $splits[0];
                                        $lookup_id                  = $splits[1];
                                        $detail_lookup_id           = $splits[2];
                    
                                        $consumable_asset           = ConsumableAsset::where([
                                            ['asset_type_id',$custom_lookup_id],
                                            ['id',$lookup_id],
                                        ])
                                        ->first();
                        
                                        $consumable_asset_detail    = ConsumableAssetDetail::where([
                                            ['consumable_asset_id',$lookup_id],
                                            ['id',$detail_lookup_id],
                                        ])
                                        ->first();
                        
                                        $model_header               = ($consumable_asset ? $consumable_asset->model : null);
                                        $serial_number_header       = ($consumable_asset ? $consumable_asset->serial_number : null);
                                        $detail_value               = ($consumable_asset_detail) ? $consumable_asset_detail->value : null;
                                        $lookup_value               = $model_header;
                        
                                        if($serial_number_header)   $lookup_value .= ':'.strtoupper($serial_number_header);
                                        if($detail_value)           $lookup_value .= ':'.strtoupper($detail_value);
                                        
                                        $curr_value                 = $lookup_value;
                                    }else $curr_value               = $curr_value;
                                }
                                
                                $sheet->setCellValue($alphabets[$key].$j,ucwords($curr_value));

                                $j++; 
                            }
                        }
                        

                    
                    });

                    $excel->setActiveSheetIndex(0);
                }

            
            })
            ->save('xlsx', $location);
        }
        
    }


    private function setStatus($scheduler,$status)
    {
        $scheduler->update([
            'status'=> $status
        ]);
    }

    private function setStartJob($scheduler)
    {
        $scheduler->update([
            'start_job'=> Carbon::now()
        ]);
    }

    private function setEndJob($scheduler)
    {
        $scheduler->update([
            'end_job'=> Carbon::now()
        ]);
    }
}
