<?php namespace App\Console\Commands;

use DB;
use Carbon\Carbon;
use Illuminate\Console\Command;

use App\Models\Asset;
use App\Models\Temporary;
use App\Models\Scheduler;
use App\Models\Erp\Asset as ERPAsset;
use App\Models\Erp\AssetDev as ERPAssetDev;

class dailySyncAssetIdErp extends Command
{
    protected $signature = 'syncAssetIDErp:daily';
    protected $description = 'Daily sync asset id erp';
    
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {

        $is_schedule_on_going = Scheduler::where('job','DAILY_SYNC_ASSET_ID_ERP')
        ->where('status','ongoing')
        ->exists();

        if(!$is_schedule_on_going)
        {
            $new_scheduler = Scheduler::create([
                'job' => 'DAILY_SYNC_ASSET_ID_ERP',
                'status' => 'ongoing'
            ]);
            $this->info('START DAILY SYNC ASSET ID ERP AT '.carbon::now());
            $this->setStartJob($new_scheduler);
            $this->dailyJob();
            $this->setStatus($new_scheduler,'done');
            $this->setEndJob($new_scheduler); 
            $this->info('DONE DAILY SYNC ASSET AT '.carbon::now());
        }else
        {
            $this->info('SYNC ASSET ID ERP SEDANG BERJALAN');
        }
    }

    private function dailyJob()
    {
        $data  = Temporary::where('column_2','sync_asset_id_erp')->get();

        try 
        {
            DB::beginTransaction();

            foreach ($data as $key => $value) 
            {
                $asset          = Asset::find($value->column_1);
                $erp_no_asset   = $asset->erp_no_asset;
                $erp_asset      = ERPAssetDev::where(db::raw('lower(asset_accounting)'),$erp_no_asset)->first();

                if($erp_asset)
                {
                    $asset->erp_asset_id = $erp_asset->a_asset_id;
                    $asset->save();
                    $value->delete();
                }
                
            }

            DB::commit();

        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }


    private function setStatus($scheduler,$status)
    {
        $scheduler->update([
            'status'=> $status
        ]);
    }

    private function setStartJob($scheduler)
    {
        $scheduler->update([
            'start_job'=> Carbon::now()
        ]);
    }

    private function setEndJob($scheduler)
    {
        $scheduler->update([
            'end_job'=> Carbon::now()
        ]);
    }
}
