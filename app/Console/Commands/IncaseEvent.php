<?php namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;

use App\Models\Asset;
use App\Models\AssetType;
use App\Models\SubDepartment;

use App\Http\Controllers\AssetController;

class IncaseEvent extends Command
{
    protected $signature    = 'incaseEvent:update';
    protected $description  = 'update status incase';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        
        $this->info('START EVENT AT '.carbon::now());
        
        $this->updateNoInventory();
        //$this->GenerateBarcode();
        
        $this->info('END EVENT AT '.carbon::now());
    }

    static function updateNoInventory()
    {
        $data = Asset::get();

        foreach ($data as $key => $value) 
        {
           $asset_type      = AssetType::find($value->asset_type_id);
           $sub_department  = SubDepartment::find($value->origin_sub_department_location_id);
           $referral_code   = explode('.',$value->referral_code);

           $asset_group     = $asset_type->erp_asset_group_name;
           $department_code = strtoupper($sub_department->code);
           $factory         = strtoupper($value->originFactory->code);
           $asset_type      = strtoupper($asset_type->code);

           $new_referral_code   = $asset_group.'.'.$department_code.'.'.$factory.'.'.$asset_type;
           $value->referral_code = $new_referral_code;
           $value->save();

           //$no_inventory    = explode('.',$value->no_inventory);
           $month           = ($value->receiving_date ? $value->receiving_date->format('m') : $value->created_at->format('m'));
           $year            = ($value->receiving_date ? $value->receiving_date->format('y') : $value->created_at->format('y'));
           $sequence        = $value->sequence;

           if($sequence)
           {
                if($sequence < 10) $sequence_format = '00'.$sequence;
                else if($sequence < 100) $sequence_format = '0'.$sequence;
                else $sequence_format = $sequence;
                
                $value->no_inventory = $value->referral_code.'.'.$month.'.'.$year.'.'.$sequence_format;
                $value->save();
           }
           

           
        }
    }

    static function GenerateBarcode()
    {
        $data = Asset::where('barcode','BELUM DIPRINT')->get();
        foreach ($data as $key => $value) 
        {
           $value->barcode =  AssetController::randomCode();;
           $value->save();
        }
    }
}
