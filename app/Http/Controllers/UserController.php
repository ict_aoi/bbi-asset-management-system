<?php namespace App\Http\Controllers;

use DB;
use File;
use Auth;
use Config;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Intervention\Image\ImageManagerStatic as Image;

use App\Models\User;
use App\Models\Role;
use App\Models\Factory;
use App\Models\Department;
use App\Models\SubDepartment;

use App\Models\Absensi\AbsensiBbi;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $msg = $request->session()->get('message');
        return view('user.index',compact('msg'));
    }

    public function data()
    {
        if(request()->ajax()) 
        {
            if(auth::user()->is_super_admin)
            {
                $data = DB::table('user_v')->where('company_id',auth::user()->company_id);
            }else
            {
                $data = DB::table('user_v')->where([
                    ['company_id',auth::user()->company_id],
                    ['nik','!=','11111111'],
                ]);
            } 

            return datatables()->of($data)
            ->editColumn('name',function($data)
            {
            	return ucwords($data->name);
            })
            ->editColumn('department_name',function($data)
            {
            	return ucwords($data->department_name);
            })
            ->editColumn('factory_name',function($data)
            {
            	return ucwords($data->factory_name);
            })
            ->editColumn('company_name',function($data)
            {
            	return ucwords($data->company_name);
            })
            ->editColumn('sub_department_name',function($data)
            {
            	return ucwords($data->sub_department_name);
            })
            ->editColumn('sex',function($data)
            {
            	return ucwords($data->sex);
            })
            ->addColumn('action', function($data) 
            {
                return view('user._action', [
                    'model'     => $data,
                    'edit'      => route('user.edit',$data->id),
                    'delete'    => route('user.destroy',$data->id),
                    'reset'     => route('user.resetPassword',$data->id),
                ]);
            })
            ->make(true);
        }
    }

    public function create(Request $request)
    {
        if($request->session()->has('message')) $request->session()->forget('message');

        $is_super_admin = Auth::user()->is_super_admin;
        if($is_super_admin)
        {
            $roles = Role::pluck('display_name', 'id')->all();
            $factory_id = null;
        }else
        {
            $roles = Role::where('name','!=','super-admin')->pluck('display_name', 'id')->all();
            $factory_id = Auth::user()->factory_id;
        } 

        $factories = Factory::where('company_id',auth::user()->company_id)->pluck('name', 'id')->all();
        return view('user.create',compact('roles','factories','factory_id'));
    }

    public function store(Request $request)
    {
        $avatar = Config::get('storage.avatar');
        if (!File::exists($avatar)) File::makeDirectory($avatar, 0777, true);

        $this->validate($request, [
            'name'          => 'required|min:3',
            'department'    => 'required',
            'sub_department'=> 'required',
            'sex'           => 'required',
        ]);

        if($request->nik)
        {
            if(User::where('nik',$request->nik)->exists()) 
                return response()->json('Nik sudah ada.', 422);
        }

        $department     = Department::where('name',strtolower($request->department))->first();
        $sub_department = SubDepartment::where('name',strtolower($request->sub_department))->first();

        if(!$department) return response()->json('Departemen belum ada, silahkan sync terlebih dahulu.', 422);
        if(!$sub_department) return response()->json('Sub Departemen belum ada, silahkan sync terlebih dahulu.', 422);

        $image = null;
        if ($request->hasFile('photo')) 
        {
            if ($request->file('photo')->isValid()) 
            {
                $file           = $request->file('photo');
                $now            = Carbon::now()->format('u');
                $filename       = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                $image          = $filename . '_' . $now . '.' . $file->getClientOriginalExtension();

                //Resize Function
                $image_resize   = Image::make($file->getRealPath());
                $height         = $image_resize->height();
                $width          = $image_resize->width();

                $newWidth       = 130;
                $newHeight      = 130;
                $image_resize->resize($newWidth, $newHeight);
                $image_resize->save($avatar.'/'.$image);
            }
        }

        $factory = factory::find($request->factory);
        
        try
        {
            DB::beginTransaction();
            
            $user = User::firstorCreate([
                'name'              => strtolower($request->name),
                'nik'               => $request->nik,
                'sex'               => $request->sex,
                'company_id'        => $factory->company->id,
                'factory_id'        => $request->factory,
                'department_id'     => $department->id,
                'sub_department_id' => $sub_department->id,
                'photo'             => $image,
                'email'             => $request->nik.'.dummy@'.$factory->code.'.co.id',
                'email_verified_at' => carbon::now(),
                'password'          => bcrypt('password1'),
                'is_super_admin'    => ($request->has('is_super_admin'))? true : false,
                'create_user_id'    => auth::user()->id
            ]);

            $mappings   =  json_decode($request->mappings);
            $array      = array();

            foreach ($mappings as $key => $mapping) $array [] = [ 'id' => $mapping->id ];
            
            if($user->save()) $user->attachRoles($array);
            
            DB::commit();
            

        } catch (Exception $e)
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }

        $request->session()->flash('message', 'success');
        return response()->json('success',200);
    }

    public function storeRole(Request $request)
    {
        $user = User::find($request->user_id);
        $user->attachRoles([$request->role_id]);
        return response()->json(200);
    }

    public function edit(Request $request,$id)
    {
        if($request->session()->has('message')) $request->session()->forget('message');

        $is_super_admin = Auth::user()->is_super_admin;
        if($is_super_admin)
        {
            $roles      = Role::pluck('display_name', 'id')->all();
            $factory_id = null;
        }else
        {
            $roles      = Role::where('name','!=','super-admin')->pluck('display_name', 'id')->all();
            $factory_id = Auth::user()->factory_id;
        } 

        $factories      = Factory::pluck('name', 'id')->all();
        $user           = User::find($id);
        $user_roles     = $user->roles()->get();
        $mappings       = array();

        foreach ($user_roles as $key => $user_role) 
        {
            $obj           = new stdClass;
            $obj->id       = $user_role->id;
            $obj->name     = $user_role->name;
            $mappings []   = $obj;
        }

        return view('user.edit',compact('roles','factories','factory_id','user','mappings'));
    }

    public function dataRole(Request $request,$id)
    {
        if(request()->ajax()) 
        {
            $data = db::select(db::raw("select roles.id as id
            ,roles.display_name
            from roles 
            join role_user on role_user.role_id = roles.id 
            where role_user.user_id = '".$id."'
            "));

            return datatables()->of($data)
            ->addColumn('action', function($data)use($id){
                return view('role._action_modal', [
                    'model' => $data,
                    'delete' => route('user.destroyRoleUser',[$id,($data)?$data->id : null]),
                ]);
            })
            ->make(true);
        }
        
    }

    public function update(Request $request, $id)
    {
        $avatar = Config::get('storage.avatar');
        if (!File::exists($avatar)) File::makeDirectory($avatar, 0777, true);

        $this->validate($request, [
            'name'          => 'required',
            'department'    => 'required',
            'sex'           => 'required',
        ]);
        
        if($request->nik)
        {
            if(User::where([
                ['nik',$request->nik],
                ['id','!=',$id],
            ])->exists()) 
                return response()->json('Nik sudah ada.', 422);
        }

        $user = User::find($id);
        $image = null;
        if ($request->hasFile('photo')) 
        {
            if ($request->file('photo')->isValid()) 
            {
                $old_file = $avatar . '/' . $user->photo;
                if(File::exists($avatar)) File::delete($old_file);

                $file           = $request->file('photo');
                $now            = Carbon::now()->format('u');
                $filename       = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                $image          = $filename . '_' . $now . '.' . $file->getClientOriginalExtension();
                $image_resize   = Image::make($file->getRealPath());
                $height         = $image_resize->height();
                $width          = $image_resize->width();
                $newWidth       = 130;
                $newHeight      = 130;

                $image_resize->resize($newWidth, $newHeight);
                $image_resize->save($avatar.'/'.$image);
            }
        }

        $factory = factory::find($request->factory);

        $user->sex = $request->sex;
        if ($image) $user->photo = $image;
        $user->save();

        $request->session()->flash('message', 'success_2');
        return response()->json(200);
    }

    public function destroy($id)
    {
        $user = User::findorFail($id);
        $user->delete_at = carbon::now();
        $user->delete_user_id = auth::user()->id;
        $user->save();
        return response()->json(200);
    }

    public function resetPassword($id)
    {
        $user = User::findorFail($id);
        $user->password = bcrypt('password1');
        $user->save();
        return response()->json(200);
    }

    public function destroyRoleUser($user_id,$role_id)
    {
        $user = User::find($user_id);
        $roles = $user->roles()->where('role_id','!=',$role_id)->get();
        $array = array();
        foreach ($roles as $key => $role) {
            $array [] = $role->id;
        }
        
        $user->roles()->sync([]);
        $user->attachRoles($array);

        return response()->json($roles);
    }

    public function getAbsence(Request $request)
    {
        $absences = AbsensiBbi::select('nik','name','department_name','subdept_name')
        ->where(function($query){
            $query->whereNotNull('department_name')
            ->whereNotNull('subdept_name');
        })
        ->get();

        $array = [];
        foreach ($absences as $key => $value) 
        {
            $array [] = $value->nik.':'.ucwords(strtolower(trim($value->name))).':'.ucwords(strtolower(trim($value->department_name))).':'.ucwords(strtolower(trim($value->subdept_name)));
        }
        
        return response()->json($array,200);
        
    }

    public function getInformationUser(Request $request)
    {
        $nik                = $request->nik;
        $user_information   = AbsensiBbi::where('nik',$nik)->first();

        return response()->json($user_information,200);
    }
}
