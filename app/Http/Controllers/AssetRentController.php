<?php namespace App\Http\Controllers;

use DB;
use Auth;
use File;
use Excel;
use Config;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Session;
use Intervention\Image\ImageManagerStatic as Image;

use App\Models\Area;
use App\Models\User;
use App\Models\Asset;
use App\Models\Status;
use App\Models\Budget;
use App\Models\Company;
use App\Models\Factory;
use App\Models\Barcode;
use App\Models\AssetType;
use App\Models\Temporary;
use App\Models\Department;
use App\Models\SubDepartment;
use App\Models\Manufacture;
use App\Models\AssetMovement;
use App\Models\ConsumableAsset;
use App\Models\ConsumableAssetDetail;
use App\Models\AssetInformationField;
use App\Models\ConsumableAssetMovement;

use App\Models\Erp\AssetGroupLive;
use App\Models\Erp\ItemIndirect;
use App\Models\Erp\AssetReceived;
use App\Models\Erp\Asset as ERPAsset;

class AssetRentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $msg                = $request->session()->get('message');
        $companies          = Company::where('id',auth::user()->company_id)->whereNull('delete_at')->pluck('name', 'id')->all();
        $factories          = Factory::where('company_id',auth::user()->company_id)->whereNull('delete_at')->pluck('name', 'id')->all();
        $sub_departments    = SubDepartment::whereNull('delete_at')->pluck('name', 'id')->all();
        $budgets            = Budget::whereNull('deleted_at')->groupby('name', 'unique_id')->pluck('name', 'unique_id')->all();
        
        $company_id         = auth::user()->company_id;
        $factory_id         = auth::user()->factory_id;
        $sub_department_id  = auth::user()->sub_department_id;

        return view('asset_rent.index',compact('msg','companies','budgets','company_id','factory_id','sub_department_id','factories','sub_departments'));
    }

    public function data(Request $request)
    {
        if(request()->ajax()) 
        {
            $company_id         = $request->company;
            $factory_id         = $request->factory;
            $sub_department_id  = $request->sub_department;
            $asset_type_id      = $request->asset_type;
            $status             = $request->status;
            
            $data = DB::table('asset_rent_v')
            ->where(function($query) use($company_id,$factory_id,$sub_department_id,$asset_type_id,$status)
            {
                $query = $query->Where('origin_company_id','LIKE',"%$company_id%")
                ->Where('origin_factory_id','LIKE',"%$factory_id%")
                ->Where('origin_sub_department_id','LIKE',"%$sub_department_id%")
                ->Where('asset_type_id','LIKE',"%$asset_type_id%");

                if($status)
                {
                    $query = $query->Where('status',"$status");
                }
            });

            return datatables()->of($data)
            ->editColumn('status',function($data){
                if(strtolower($data->status) == 'siap digunakan') return '<span class="label label-info">'.$data->status.'</span>';
                else return '<span class="label label-default">'.$data->status.'</span>';
            })
            ->editColumn('arrival_date',function($data)
            {
                if($data->arrival_date) return  Carbon::createFromFormat('Y-m-d', $data->arrival_date)->format('d/M/Y');
                else return null;
            })
            ->editColumn('start_rent_date',function($data)
            {
                if($data->start_rent_date) return  Carbon::createFromFormat('Y-m-d H:i:s', $data->start_rent_date)->format('d/M/Y');
                else return null;
            })
            ->editColumn('end_rent_date',function($data)
            {
                if($data->end_rent_date) return  Carbon::createFromFormat('Y-m-d H:i:s', $data->end_rent_date)->format('d/M/Y');
                else return null;
            })
            ->editColumn('return_date',function($data)
            {
                if($data->return_date) return  Carbon::createFromFormat('Y-m-d H:i:s', $data->return_date)->format('d/M/Y');
                else return null;
            })
            ->editColumn('total_price_rent',function($data)
            {
                if($data->total_price_rent) return number_format($data->total_price_rent, 4, '.', ',').' (per '.$data->duration_rent_in_days.' days)';
                else return null;
            })
            ->addColumn('action', function($data) 
            {
                return view('asset_rent._action', [
                    'model'    => $data,
                    'edit'     => route('assetRent.edit',$data->asset_id),
                    'printout' => route('assetRent.barcode',$data->asset_id),
                    'delete'   => route('assetRent.destroy',$data->asset_id),
                    'history'  => route('assetRent.history',$data->asset_id),
                ]);
            })
            ->rawColumns(['action','status'])
            ->make(true);
        }
    }

    public function create(Request $request)
    {
        if($request->session()->has('message')) $request->session()->forget('message');
        
        $companies          = Company::where('id',auth::user()->company_id)->whereNull('delete_at')->pluck('name', 'id')->all();
        $factories          = Factory::whereNull('delete_at')->where('company_id', auth::user()->company_id)->pluck('name', 'id')->all();
        $sub_departments    = SubDepartment::whereNull('delete_at')->pluck('name', 'id')->all();
       
        $company_id         = auth::user()->company_id;
        $factory_id         = auth::user()->factory_id;
        $sub_department_id  = auth::user()->sub_department_id;


        return view('asset_rent.create',compact('companies','company_id','factory_id','sub_department_id','factories','sub_departments'));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
    		'factory'       =>'required',
    		'asset_type'    =>'required',
    		'serial_number' =>'required',
        ]);

        $asset_store    = Config::get('storage.asset');
            if (!File::exists($asset_store)) File::makeDirectory($asset_store, 0777, true);
        
        $asset_type             = AssetType::find($request->asset_type);
        $factory                = Factory::find($request->factory);
        $_sub_department        = SubDepartment::find($request->sub_department);
        $manufacture            = Manufacture::find($request->manufacture);
        $no_kk_rent             = trim(strtolower($request->no_kk_rent));
        $supplier_name          = trim(strtolower($request->supplier_name));
        $model                  = trim(strtolower($request->model));
        $serial_number          = trim(strtolower($request->serial_number));
        $inserted_at            = $request->current_time;

        if($asset_type->delete_at) return response()->json(['message' => 'Asset type is not active'],422);
        if($factory->delete_at) return response()->json(['message' => 'Factory is not active'],422);
        if($_sub_department->delete_at) return response()->json(['message' => 'Sub Department is not active'],422);
        if(!$_sub_department) return response()->json(['message' => 'Department code is empty. Please fill code first'],422);
        
        $is_exists      = Asset::where([
            ['company_id',$factory->company_id],
            ['factory_id',$factory->id],
            ['sub_department_id',$_sub_department->id],
            ['asset_type_id',$asset_type->id],
            ['asset_type_id',$asset_type->id],
            ['is_rent',true],
            ['no_rent',$no_kk_rent],
        ])
        ->whereNull('return_date')
        ->whereNull('delete_at');

        if($model) $is_exists                  = $is_exists->where('model',$model);
        if($serial_number) $is_exists          = $is_exists->where('serial_number',$serial_number);
        if($manufacture) $is_exists            = $is_exists->where('manufacture_id',$manufacture->id);

        $is_exists = $is_exists->exists();

        if($is_exists) return response()->json(['message' => 'Data already exists'],422);

        $image = null;
        if ($request->hasFile('photo')) 
        {
            if ($request->file('photo')->isValid()) 
            {
                $file           = $request->file('photo');
                $now            = Carbon::now()->format('u');
                $filename       = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                $image          = $filename . '_' . $now . '.' . $file->getClientOriginalExtension();

                //Resize Function
                $image_resize   = Image::make($file->getRealPath());
                $height         = $image_resize->height();
                $width          = $image_resize->width();

                $newWidth       = 200;
                $newHeight      = 200;
                $image_resize->resize($newWidth, $newHeight);
            }
        }

        $month              = ($request->arrival_date)? Carbon::createFromFormat('d/m/Y', $request->arrival_date)->format('m') : Carbon::now()->format('m');
        $year               = ($request->arrival_date)? Carbon::createFromFormat('d/m/Y', $request->arrival_date)->format('y') : Carbon::now()->format('y');
        
        $_asset_group_id    = $asset_type->erp_asset_group_id;
        $_asset_group_name  = $asset_type->erp_asset_group_name;
        $_referral_code     = $_asset_group_name.'.'.strtoupper($_sub_department->code).'.'.strtoupper($factory->code).'.'.strtoupper($asset_type->code);
        $_no_inventory      = $_asset_group_name.'.'.strtoupper($_sub_department->code).'.'.strtoupper($factory->code).'.'.strtoupper($asset_type->code).'.'.$month.'.'.$year;
        $sequence           = Asset::where('referral_code',$_referral_code)->max('sequence');

        if(!$_asset_group_name)
        {
            return response()->json(['message' => 'Code asset group is empthy, Please fill code first'],422);
        }

        if ($sequence == null) $sequence = 1;
        else $sequence                  += 1;

        if($sequence < 10) $sequence_format         = '00'.$sequence;
        else if($sequence < 100) $sequence_format   = '0'.$sequence;
        else $sequence_format                       = $sequence;

        $no_inventory                               = $_no_inventory.'.'.$sequence_format;
        
        try 
        {
            DB::beginTransaction();
            $asset = Asset::firstorCreate([
                'asset_type_id'                     => $asset_type->id,
                'company_id'                        => $factory->company_id,
				'factory_id'                        => $factory->id,
                'department_id'                     => $_sub_department->department_id,
                'sub_department_id'                 => $_sub_department->id,
                'origin_company_location_id'        => $factory->company_id,
				'origin_factory_location_id'        => $factory->id,
                'origin_department_location_id'     => $_sub_department->department_id,
                'origin_sub_department_location_id' => $_sub_department->id,
                'manufacture_id'                    => $manufacture->id,
                'budget_id'                         => $request->budget,
				'barcode'                           => 'BELUM DIPRINT',
                'serial_number'                     => $serial_number,
                'no_inventory'                      => $no_inventory,
                'no_inventory_manual'               => '-',
                'description'                       => '-',
                'erp_no_asset'                      => '-',
                'erp_asset_id'                      => '-',
                'model'                             => $model,
                'purchase_number'                   => '-',
                'supplier_name'                     => $supplier_name,
				'receiving_date'                    => ($request->arrival_date)? Carbon::createFromFormat('d/m/Y', $request->arrival_date)->format('Y-m-d') : null,
                'image'                             => $image,
                'erp_asset_group_id'                => $_asset_group_id,
                'erp_asset_group_name'              => $_asset_group_name,
                'erp_item_id'                       => '-',
                'erp_item_code'                     => '-',
                'referral_code'                     => $_referral_code,
				'sequence'                          => $sequence,
                'status'                            => 'siap digunakan',
                'last_company_location_id'          => $factory->company_id,
				'last_factory_location_id'          => $factory->id,
				'last_department_location_id'       => $_sub_department->department_id,
				'last_sub_department_location_id'   => $_sub_department->id,
                'last_user_movement_id'             => Auth::user()->id,
                'create_user_id'                    => Auth::user()->id,
                'delete_at'                         => null,
                'is_rent'                           => true,
                'duration_rent_in_days'             => $request->duration,
                'no_rent'                           => $no_kk_rent,
                'price'                             => $request->price_rent,
                'start_rent_date'                   => ($request->start_rent_date)? Carbon::createFromFormat('d/m/Y', $request->start_rent_date)->format('Y-m-d') : null,
                'end_rent_date'                     => ($request->end_rent_date)? Carbon::createFromFormat('d/m/Y', $request->end_rent_date)->format('Y-m-d') : null,
                'return_date'                       => null,
                'created_at'                        => $inserted_at,
                'updated_at'                        => $inserted_at,
                'last_movement_date'                => $inserted_at,
            ]);

            if($asset->erp_no_asset)
            {
                Temporary::firstorCreate([
                    'column_1'  => $asset->id,
                    'column_2'  => 'sync_asset_id_erp',
                ]);
            }

            $asset_movement = AssetMovement::firstorCreate([
                'asset_id'                          => $asset->id,
                'status'                            => 'siap digunakan',
                'from_company_location'             => $asset->company_id,
                'from_factory_location'             => $asset->factory_id,
                'from_department_location'          => $asset->department_id,
                'from_employee_nik'                 => auth::user()->nik,
                'from_employee_name'                => auth::user()->name,
                'movement_date'                     => $inserted_at,
                'note'                              => 'asset sewa baru',
            ]);

            $asset->last_user_movement_id   = auth::user()->id;
            $asset->last_asset_movement_id  = $asset_movement->id;
            $asset->save();
            

            $custom_fields  = json_decode($request->custom_fields);
            $array          = array();
            foreach ($custom_fields as $key => $custom_field) 
            {
                $custom_field_name          = $custom_field->name;
                $is_lookup                  = $custom_field->is_lookup;
                $custom_lookup_id           = $custom_field->custom_lookup_id;
                $lookup_id                  = $custom_field->lookup_id;
                $detail_lookup_id           = ($custom_field->detail_lookup_id)? $custom_field->detail_lookup_id : '-1';
                $value                      = ($is_lookup)? $custom_lookup_id.'|'.$lookup_id.'|'.$detail_lookup_id : $custom_field->value;
                $asset->$custom_field_name  = strtolower($value);

                if($is_lookup)
                {
                    if($custom_field->lookup_id)
                    {  
                        $movement_date          = carbon::now();
                        ConsumableAssetMovement::firstorCreate([
                            'asset_id'                          => $asset->id,
                            'consumable_asset_id'               => $custom_field->lookup_id,
                            'consumable_asset_detail_id'        => ($custom_field->detail_lookup_id != '')? $custom_field->detail_lookup_id : null,
                            'status'                            => 'digunakan',
                            'from_company_location'             => $asset->company_id,
                            'from_factory_location'             => $asset->factory_id,
                            'from_department_location'          => $asset->department_id,
                            'from_employee_nik'                 => auth::user()->nik,
                            'from_employee_name'                => auth::user()->name,
                            'to_company_location'               => $asset->company_id,
                            'to_factory_location'               => $asset->factory_id,
                            'to_department_location'            => $asset->department_id,
                            'movement_date'                     => $movement_date,
                            'is_using_in_custom_field'          => true,
                            'system_note'                       => 'digunakan untuk asset, dengan nomor inventori '.$asset->no_inventory.'. dikeluarkan pada tanggal '.$movement_date->format('d/M/Y').' oleh '.auth::user()->name,
                        ]);
    
                        $array [] = $custom_field->lookup_id;
                    }
                   
                }
                
            }

            if(isset($array))
            {
                $detail_consumable_assets = ConsumableAssetMovement::select('consumable_asset_id',db::raw('count(0) as total_qty_used'))
                ->whereIn('consumable_asset_id',$array)
                ->where([
                    ['status','digunakan'],
                    ['is_return',false]
                ])
                ->groupby('consumable_asset_id')
                ->get();

                foreach ($detail_consumable_assets as $key => $detail_consumable_asset) 
                {
                    $consumable_asset                   = ConsumableAsset::find($detail_consumable_asset->consumable_asset_id);
                    $total                              = $consumable_asset->total;
                    $new_available_qty                  = $total - $detail_consumable_asset->total_qty_used;

                    $consumable_asset->qty_used         = $detail_consumable_asset->total_qty_used;
                    $consumable_asset->qty_available    = $new_available_qty;
                    $consumable_asset->save();
                }
            }
            

            if($asset->save())
            {
                if ($request->hasFile('photo')) $image_resize->save($asset_store.'/'.$image);
            } 

            Temporary::firstorCreate([
                'column_1'  => $asset->origin_department_location_id,
                'column_2'  => 'sync_export_asset_per_department',
            ]);

            DB::commit();

            $request->session()->flash('message', 'success');
            $return_barcode [] = $asset->id;
            return response()->json($return_barcode,200);
            
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    public function edit(Request $request,$id)
    {
        if($request->session()->has('message')) $request->session()->forget('message');
        
        $asset              = Asset::find($id);
        $companies          = Company::whereNull('delete_at')->pluck('name', 'id')->all();
        $factories          = Factory::whereNull('delete_at')->pluck('name', 'id')->all();
        $sub_departments    = SubDepartment::whereNull('delete_at')->pluck('name', 'id')->all();
        $manufactures       = Manufacture::whereNull('delete_at')->pluck('name', 'id')->all();
        $asset_types        = AssetType::whereNull('delete_at')->pluck('name', 'id')->all();
      
        return view('asset_rent.edit',compact('asset','companies','factories','sub_departments','manufactures','asset_types'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
    		'serial_number'=>'required',
        ]);
        
        $asset_store = Config::get('storage.asset');
        if (!File::exists($asset_store)) File::makeDirectory($asset_store, 0777, true);
        
        $manufacture            = Manufacture::find($request->manufacture);
        $no_kk_rent             = trim(strtolower($request->no_kk_rent));
        $supplier_name          = trim(strtolower($request->supplier_name));
        $model                  = trim(strtolower($request->model));
        $serial_number          = trim(strtolower($request->serial_number));
        $inserted_at            = $request->current_time;

        $is_exists      = Asset::where([
            ['id','!=',$id],
            ['origin_company_location_id',$request->company_id],
            ['origin_factory_location_id',$request->factory_id],
            ['origin_department_location_id',$request->department_id],
            ['asset_type_id',$request->asset_type_id],
            ['is_rent',true],
            ['no_rent',$no_kk_rent],
        ])
        ->whereNull('return_date')
        ->whereNull('delete_at');
    
        if($model) $is_exists                  = $is_exists->where('model',$model);
        if($serial_number) $is_exists          = $is_exists->where('serial_number',$serial_number);
        if($manufacture) $is_exists            = $is_exists->where('manufacture_id',$manufacture->id);

        $is_exists = $is_exists->exists();

        if($is_exists) return response()->json(['message' => 'Data already exists'],422);

        $asset      = Asset::find($id);
        $image      = $asset->image;

        if ($request->hasFile('photo')) 
        {
            if ($request->file('photo')->isValid()) 
            {
                $file           = $request->file('photo');
                $now            = Carbon::now()->format('u');
                $filename       = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                $image          = $filename . '_' . $now . '.' . $file->getClientOriginalExtension();

                //Resize Function
                $image_resize   = Image::make($file->getRealPath());
                $height         = $image_resize->height();
                $width          = $image_resize->width();

                $newWidth       = 200;
                $newHeight      = 200;
                $image_resize->resize($newWidth, $newHeight);

                $old_file = $asset_store . '/' . $asset->image;
                if(File::exists($old_file)) File::delete($old_file);
            }
        }

        try 
        {
            DB::beginTransaction();
           
            $asset->manufacture_id          = $manufacture->id;
            $asset->serial_number           = $serial_number;
            $asset->model                   = $model;
            $asset->image                   = $image;
            $asset->supplier_name           = $supplier_name;
            $asset->no_rent                 = $no_kk_rent;
            $asset->price                   = $request->price_rent;
            $asset->duration_rent_in_days   = $request->duration;
            $asset->receiving_date          = ($request->arrival_date)? Carbon::createFromFormat('d/m/Y', $request->arrival_date)->format('Y-m-d') : null;
            $asset->start_rent_date         = ($request->start_rent_date)? Carbon::createFromFormat('d/m/Y', $request->start_rent_date)->format('Y-m-d') : null;
            $asset->end_rent_date           = ($request->end_rent_date)? Carbon::createFromFormat('d/m/Y', $request->end_rent_date)->format('Y-m-d') : null;
            $asset->updated_at              = $inserted_at;
           
            $custom_fields = json_decode($request->custom_fields);
            foreach ($custom_fields as $key => $custom_field) 
            {
                $custom_field_name          = $custom_field->name;
                $is_lookup                  = $custom_field->is_lookup;
                $custom_lookup_id           = $custom_field->custom_lookup_id;
                $lookup_id                  = $custom_field->lookup_id;
                $detail_lookup_id           = ($custom_field->detail_lookup_id)? $custom_field->detail_lookup_id : '-1';
                $value                      = ($is_lookup)? $custom_lookup_id.'|'.$lookup_id.'|'.$detail_lookup_id : $custom_field->value;
                $asset->$custom_field_name  = strtolower($value);

                if($is_lookup)
                {
                    
                    $is_exists              = ConsumableAssetMovement::where([
                        ['asset_id',$asset->id],
                        ['is_using_in_custom_field',true],
                    ])
                    ->whereIn('consumable_asset_id',function($query) use($custom_lookup_id)
                    {
                        $query->select('id')
                        ->from('consumable_assets')
                        ->where('asset_type_id',$custom_lookup_id);
                    })
                    ->first();

                    if($is_exists)
                    {
                        if($is_exists->consumable_asset_detail_id)
                        {
                            if($is_exists->consumable_asset_id != $custom_field->lookup_id
                            || $is_exists->consumable_asset_detail_id != $custom_field->detail_lookup_id)
                            {
                                ConsumableAssetMovement::firstorCreate([
                                    'asset_id'                          => $asset->id,
                                    'consumable_asset_id'               => $is_exists->consumable_asset_id,
                                    'consumable_asset_detail_id'        => $is_exists->consumable_asset_detail_id,
                                    'status'                            => 'dikembalikan',
                                    'from_company_location'             => strtolower($is_exists->from_company_location),
                                    'from_factory_location'             => strtolower($is_exists->from_factory_location),
                                    'from_department_location'          => strtolower($is_exists->from_department_location),
                                    'from_employee_nik'                 => strtolower($is_exists->from_employee_nik),
                                    'from_employee_name'                => strtolower($is_exists->from_employee_name),
                                    'movement_date'                     => $inserted_at,
                                    'is_return'                         => true,
                                    'is_using_in_custom_field'          => true,
                                    'system_note'                       => 'dikembalikan dari asset, dengan nomor inventori '.$asset->no_inventory.'. dikembalikan pada tanggal '.$movement_date->format('d/M/Y').' oleh '.auth::user()->name,
                                ]);

                                ConsumableAssetMovement::firstorCreate([
                                    'asset_id'                          => $asset->id,
                                    'consumable_asset_id'               => $custom_field->lookup_id,
                                    'consumable_asset_detail_id'        => ($custom_field->detail_lookup_id != '')? $custom_field->detail_lookup_id : null,
                                    'status'                            => 'digunakan',
                                    'from_company_location'             => strtolower($factory->company->name),
                                    'from_factory_location'             => strtolower($factory->name),
                                    'from_department_location'          => strtolower( $asset->department_name),
                                    'from_employee_nik'                 => strtolower(auth::user()->nik),
                                    'from_employee_name'                => strtolower(auth::user()->name),
                                    'movement_date'                     => $inserted_at,
                                    'is_using_in_custom_field'          => true,
                                    'system_note'                       => 'digunakan untuk asset, dengan nomor inventori '.$asset->no_inventory.'. dikeluarkan pada tanggal '.$movement_date->format('d/M/Y').' oleh '.auth::user()->name,
                                ]);
                            }
                        }else
                        {
                            if($is_exists->consumable_asset_id != $custom_field->lookup_id)
                            {
                                ConsumableAssetMovement::firstorCreate([
                                    'asset_id'                          => $asset->id,
                                    'consumable_asset_id'               => $is_exists->consumable_asset_id,
                                    'consumable_asset_detail_id'        => $is_exists->consumable_asset_detail_id,
                                    'status'                            => 'dikembalikan',
                                    'from_company_location'             => strtolower($is_exists->from_company_location),
                                    'from_factory_location'             => strtolower($is_exists->from_factory_location),
                                    'from_department_location'          => strtolower($is_exists->from_department_location),
                                    'from_employee_nik'                 => strtolower($is_exists->from_employee_nik),
                                    'from_employee_name'                => strtolower($is_exists->from_employee_name),
                                    'to_company_location'               => strtolower($is_exists->from_company_location),
                                    'to_factory_location'               => strtolower($is_exists->from_factory_location),
                                    'to_department_location'            => strtolower($is_exists->from_department_location),
                                    'movement_date'                     => $inserted_at,
                                    'is_using_in_custom_field'          => true,
                                    'system_note'                       => 'dikembalikan dari asset, dengan nomor inventori '.$asset->no_inventory.'. dikembalikan pada tanggal '.$movement_date->format('d/M/Y').' oleh '.auth::user()->name,
                                ]);

                                ConsumableAssetMovement::firstorCreate([
                                    'asset_id'                          => $asset->id,
                                    'consumable_asset_id'               => $custom_field->lookup_id,
                                    'consumable_asset_detail_id'        => ($custom_field->detail_lookup_id != '')? $custom_field->detail_lookup_id : null,
                                    'status'                            => 'digunakan',
                                    'from_company_location'             => strtolower($factory->company->name),
                                    'from_factory_location'             => strtolower($factory->name),
                                    'from_department_location'          => strtolower($asset->department_name),
                                    'from_employee_nik'                 => strtolower(auth::user()->nik),
                                    'from_employee_name'                => strtolower(auth::user()->name),
                                    'to_company_location'               => strtolower($is_exists->from_company_location),
                                    'to_factory_location'               => strtolower($is_exists->from_factory_location),
                                    'to_department_location'            => strtolower($is_exists->from_department_location),
                                    'movement_date'                     => $inserted_at,
                                    'is_using_in_custom_field'          => true,
                                    'system_note'                       => 'digunakan untuk asset, dengan nomor inventori '.$asset->no_inventory.'. dikeluarkan pada tanggal '.$movement_date->format('d/M/Y').' oleh '.auth::user()->name,
                                ]);
                            }
                        }
                        
                        
                    }

                    $array [] = $custom_field->lookup_id;
                }
            }

            if(isset($array))
            {
                $detail_consumable_assets = ConsumableAssetMovement::select('consumable_asset_id',db::raw('count(0) as total_qty_used'))
                ->whereIn('consumable_asset_id',$array)
                ->where([
                    ['status','digunakan'],
                    ['is_return',false]
                ])
                ->groupby('consumable_asset_id')
                ->get();

                foreach ($detail_consumable_assets as $key => $detail_consumable_asset) 
                {
                    $consumable_asset                   = ConsumableAsset::find($detail_consumable_asset->consumable_asset_id);
                    $total                              = $consumable_asset->total;
                    $new_available_qty                  = $total - $detail_consumable_asset->total_qty_used;

                    $consumable_asset->qty_used         = $detail_consumable_asset->total_qty_used;
                    $consumable_asset->qty_available    = $new_available_qty;
                    $consumable_asset->save();
                }
            }
            

            if($asset->save())
            {
                if($request->hasFile('photo')) $image_resize->save($asset_store.'/'.$image);
            } 


            $asset_movement = AssetMovement::firstorCreate([
                'asset_id'                          => $asset->id,
                'status'                            => 'siap digunakan',
                'from_company_location'             => $asset->company_id,
                'from_factory_location'             => $asset->factory_id,
                'from_department_location'          => $asset->department_id,
                'from_employee_nik'                 => auth::user()->nik,
                'from_employee_name'                => auth::user()->name,
                'movement_date'                     => $inserted_at,
                'note'                              => 'asset sewa diedit',
            ]);

            $asset->last_asset_movement_id = $asset_movement->id;
            $asset->save();

            Temporary::firstorCreate([
                'column_1'  => $asset->origin_department_location_id,
                'column_2'  => 'sync_export_asset_per_department',
            ]);

            DB::commit();

            $request->session()->flash('message', 'success_2');
            return response()->json('success',200);
            
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    public function printBarcode(Request $request)
    {
        if($request->list_barcodes)
        {
            $list_barcodes  = json_decode($request->list_barcodes);
            $assets         = Asset::whereIn('id',$list_barcodes)
            ->orderby('sequence','asc')
            ->get();
    
            foreach ($assets as $key => $asset) 
            {
                if($asset->barcode == 'BELUM DIPRINT')
                {
                    $asset->barcode = $this->randomCode();
                    $asset->save();
                }
            }

           
        }else
        {
            $assets         = Asset::where([
                ['origin_factory_location_id',$request->factory],
                ['asset_type_id',$request->asset_type],
            ])
            ->orderby('sequence','asc')
            ->get();
    
            foreach ($assets as $key => $asset) 
            {
                if($asset->barcode == 'BELUM DIPRINT')
                {
                    $asset->barcode = $this->randomCode();
                    $asset->save();
                }
            }
        }
        

        return view('asset.barcode',compact('assets'));
        
    }

    public function barcode($id)
    {   
        $assets = Asset::where('id',$id)->get();

        foreach ($assets as $key => $asset) 
        {
            if($asset->barcode == 'BELUM DIPRINT')
            {
                $asset->barcode = $this->randomCode();
                $asset->save();
            }
        }
        
        return view('asset.barcode',compact('assets'));
    }

    public function erpAssetPicklist(Request $request)
    {
        $receiving_date = ($request->receiving_date) ? Carbon::createFromFormat('d/m/Y', $request->receiving_date)->format('Y-m-d') : Carbon::now()->format('Y-m-d');
        $q              = trim(strtolower($request->q));

        $erp_asset_ids  = Asset::select('erp_asset_id')
        ->groupby('erp_asset_id')
        ->whereNotNull('erp_asset_id')
        ->get()
        ->toArray();

        $lists          = AssetReceived::whereBetween('mr_date', [$receiving_date, $receiving_date])
        ->whereNotIn('a_asset_id',$erp_asset_ids)
        ->where(function($query) use($q)
        {
            $query->where(db::raw('lower(inventoryno)'),'like',"%$q%")
            ->orwhere(db::raw('lower(item_code)'),$q,'like',"%$q%")
            ->orwhere(db::raw('lower(item_desc)'),$q,'like',"%$q%")
            ->orwhere(db::raw('lower(po_supplier)'),$q,'like',"%$q%");
        })
        ->paginate(20);

        return view('asset._erp_picklist',compact('lists'));
    }

    public function destroy($id)
    {
        $asset = Asset::find($id);
        $asset->delete_at = carbon::now();
        $asset->delete_user_id = auth::user()->id;
        $asset->save();
    }

    public function exportReport(Request $request)
    {
        $sub_department = SubDepartment::find($request->sub_department_id);
        $file = Config::get('storage.excel_asset') . '/' . 'asset_'.str_slug($sub_department->name).'.xlsx';

        if(!file_exists($file)) return 'file yang anda cari tidak ditemukan';

        $resp = response()->download($file);
        $resp->headers->set('Pragma', 'no-cache');
        $resp->headers->set('Cache-Control', 'nocache, no-store, max-age=0, must-revalidate');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');
        return $resp;
    }

    public function import(Request $request)
    {
        if($request->session()->has('message')) $request->session()->forget('message');
        
        $companies          = Company::where('id',auth::user()->company_id)->whereNull('delete_at')->pluck('name', 'id')->all();
        $factories          = Factory::where('company_id',auth::user()->company_id)->whereNull('delete_at')->pluck('name', 'id')->all();
        $sub_departments    = SubDepartment::whereNull('delete_at')->pluck('name', 'id')->all();

        if(auth::user()->is_super_admin)
        {
            $company_id     = null;
            $factory_id     = null;
            $sub_department_id  = null;
        }else
        {
            $company_id         = auth::user()->factory->company->id;
            $factory_id         = auth::user()->factory_id;
            $sub_department_id  = auth::user()->sub_department_id;
        } 


        return view('asset_rent.import',compact('companies','company_id','factory_id','sub_department_id','factories','sub_departments'));
    }

    public function exportFormImport(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company'       => 'required',
            'factory'       => 'required',
            'department'    => 'required',
            'asset_type'    => 'required',
        ]);

        $company_id         = $request->company;
        $factory_id         = $request->factory;
        $sub_department_id  = $request->sub_department;
        $asset_type_id      = $request->asset_type;
       
        $asset_type     = AssetType::find($asset_type_id);
        
        if($asset_type)
        {
            if($asset_type->delete_at == null)
            {
                $asset_type_informations = $asset_type->assetRentInformationFields($asset_type->id,$asset_type->is_consumable);
    
                $alphabets = array(
                    0 => 'A',
                    1 => 'B',
                    2 => 'C',
                    3 => 'D',
                    4 => 'E',
                    5 => 'F',
                    6 => 'G',
                    7 => 'H',
                    8 => 'I',
                    9 => 'J',
                    10 => 'K',
                    11 => 'L',
                    12 => 'M',
                    13 => 'N',
                    14 => 'O',
                    15 => 'P',
                    16 => 'Q',
                    17 => 'R',
                    18 => 'S',
                    19 => 'T',
                    20 => 'U',
                    21 => 'V',
                    22 => 'W',
                    23 => 'X',
                    24 => 'Y',
                    25 => 'Z',
                    26 => 'AA',
                    27 => 'AB',
                    28 => 'AC',
                    29 => 'AD',
                    30 => 'AE',
                    31 => 'AF',
                    32 => 'AG',
                    33 => 'AH',
                    34 => 'AI',
                    35 => 'AJ',
                    36 => 'AK',
                    37 => 'AL',
                    38 => 'AM',
                    39 => 'AN',
                    40 => 'AO',
                    41 => 'AP',
                    42 => 'AQ',
                    43 => 'AR',
                    44 => 'AS',
                    45 => 'AT',
                    46 => 'AU',
                    47 => 'AV',
                    48 => 'AW',
                    49 => 'AX',
                    50 => 'AY',
                    51 => 'AZ',
                    52 => 'BA',
                    53 => 'BB',
                    54 => 'BC',
                    55 => 'BD',
                    56 => 'BE',
                    57 => 'BF',
                    58 => 'BG',
                    59 => 'BH',
                    60 => 'BI',
                    61 => 'BJ',
                    62 => 'BK',
                    63 => 'BL',
                    64 => 'BM',
                    65 => 'BN',
                    66 => 'BO',
                    67 => 'BP',
                    68 => 'BQ',
                    69 => 'BR',
                    70 => 'BS',
                    71 => 'BT',
                    72 => 'BU',
                    73 => 'BV',
                    74 => 'BW',
                    75 => 'BX',
                    76 => 'BY',
                    77 => 'BZ',
                );
        
                //return response()->json($array);
        
                return Excel::create('upload_asset_rent_'.str_slug($asset_type->name),function ($excel) use($company_id,$factory_id,$sub_department_id,$asset_type_id,$asset_type_informations,$alphabets) 
                {
                    $excel->sheet('active', function($sheet) use($company_id,$factory_id,$sub_department_id,$asset_type_id,$asset_type_informations,$alphabets) 
                    {
                        foreach ($asset_type_informations as $key => $asset_type_information) 
                        {
                            if($asset_type_information->is_custom) $column_name = $asset_type_information->name.'_'.str_slug($asset_type_information->label,'_');
                            else $column_name = $asset_type_information->name;
        
                            $sheet->setCellValue($alphabets[$key].'1',$column_name);
                            $j = 2;
        
                            if(in_array(strtoupper($asset_type_information->name),['COMPANY_ID','ASSET_TYPE_ID','FACTORY_ID','SUB_DEPARTMENT_ID','MANUFACTURE_ID','BUDGET_ID','SERIAL_NUMBER','MODEL','NO_RENT','PRICE_RENT','START_RENT_DATE','END_RENT_DATE','DURATION_RENT_IN_DAYS'])
                                || ($asset_type_information->is_custom && $asset_type_information->type == 'option')
                                || ($asset_type_information->is_custom && $asset_type_information->type == 'lookup'))
                            {
                                if(strtoupper($asset_type_information->name) == 'COMPANY_ID') $value = $company_id;
                                elseif(strtoupper($asset_type_information->name) == 'FACTORY_ID') $value = $factory_id;
                                elseif(strtoupper($asset_type_information->name) == 'ASSET_TYPE_ID') $value = $asset_type_id;
                                elseif(strtoupper($asset_type_information->name) == 'SUB_DEPARTMENT_ID') $value = $sub_department_id;
        
                                if(in_array(strtoupper($asset_type_information->name),['COMPANY_ID','ASSET_TYPE_ID','FACTORY_ID','SUB_DEPARTMENT_ID']))
                                {
                                    $sheet->cell($alphabets[$key].'1', function($cell) 
                                    {
                                        $cell->setBackground('#000000');
                                        $cell->setFontColor('#ffffff');
                                    });
                                }else if(in_array(strtoupper($asset_type_information->name),['SERIAL_NUMBER','MODEL','RECEIVING_DATE','BUDGET_ID','NO_RENT','PRICE_RENT','START_RENT_DATE','END_RENT_DATE','DURATION_RENT_IN_DAYS']))
                                {
                                    $sheet->cell($alphabets[$key].'1', function($cell) 
                                    {
                                        $cell->setBackground('#ff4d4d');
                                        $cell->setFontColor('#ffffff');
                                    });
                                }
                                
        
                                for ($i=0; $i < 100; $i++) 
                                { 
                                    if(in_array(strtoupper($asset_type_information->name),['COMPANY_ID','ASSET_TYPE_ID','FACTORY_ID','DEPARTMENT_ID']))
                                    {
                                        $sheet->setCellValue($alphabets[$key].$j,$value);
                                        $sheet->cell($alphabets[$key].$j, function($cell) 
                                        {
                                            $cell->setBackground('#000000');
                                            $cell->setFontColor('#ffffff');
                                        });
                                    }else if(in_array(strtoupper($asset_type_information->name),['SERIAL_NUMBER','MODEL','BUDGET_ID','NO_RENT','PRICE_RENT','START_RENT_DATE','END_RENT_DATE','DURATION_RENT_IN_DAYS']))
                                    {
                                        $sheet->cell($alphabets[$key].$j, function($cell) 
                                        {
                                            $cell->setBackground('#ff4d4d');
                                            $cell->setFontColor('#ffffff');
                                        });

                                        if(strtoupper($asset_type_information->name) == 'BUDGET_ID')
                                        {
                                           
                                            $objValidation = $sheet->getCell($alphabets[$key].$j)->getDataValidation();
                                            $objValidation->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
                                            $objValidation->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
                                            $objValidation->setAllowBlank(false);
                                            $objValidation->setShowInputMessage(true);
                                            $objValidation->setShowErrorMessage(true);
                                            $objValidation->setShowDropDown(true);
                                            $objValidation->setErrorTitle('Input error');
                                            $objValidation->setError('Nilai tidak ada dalam list.');
                                            $objValidation->setPromptTitle('Pilih Budget');
                                            $objValidation->setPrompt('Silahkan input sesuai dengan list.');
                                            $objValidation->setFormula1('budget_list');
                                        }

                                        if(strtoupper($asset_type_information->name) == 'DURATION_RENT_IN_DAYS')
                                        {
                                            $objValidation = $sheet->getCell($alphabets[$key].$j)->getDataValidation();
                                            $objValidation->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
                                            $objValidation->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
                                            $objValidation->setAllowBlank(false);
                                            $objValidation->setShowInputMessage(true);
                                            $objValidation->setShowErrorMessage(true);
                                            $objValidation->setShowDropDown(true);
                                            $objValidation->setErrorTitle('Input error');
                                            $objValidation->setError('Nilai tidak ada dalam list.');
                                            $objValidation->setPromptTitle('Pilih Durasi');
                                            $objValidation->setPrompt('Silahkan input sesuai dengan list.');
                                            $objValidation->setFormula1('duration_rent_list');
                                        }
                                    }
                                    else 
                                    {
                                        if(in_array(strtoupper($asset_type_information->name),['MODEL','SERIAL_NUMBER','BUDGET_ID','NO_RENT','PRICE_RENT','START_RENT_DATE','END_RENT_DATE','DURATION_RENT_IN_DAYS']))
                                        {
                                            $sheet->cell($alphabets[$key].$j, function($cell) 
                                            {
                                                $cell->setBackground('#ff4d4d');
                                                $cell->setFontColor('#ffffff');
                                            });
                                        }
        
                                        if($asset_type_information->is_custom && ($asset_type_information->type == 'option' || $asset_type_information->type == 'lookup'))
                                        {
                                            $objValidation = $sheet->getCell($alphabets[$key].$j)->getDataValidation();
                                            $objValidation->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
                                            $objValidation->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
                                            $objValidation->setAllowBlank(false);
                                            $objValidation->setShowInputMessage(true);
                                            $objValidation->setShowErrorMessage(true);
                                            $objValidation->setShowDropDown(true);
                                            $objValidation->setErrorTitle('Input error');
                                            $objValidation->setError('Nilai tidak ada dalam list.');
                                            $objValidation->setPromptTitle('Pilih Nilai');
                                            $objValidation->setPrompt('Silahkan input sesuai dengan list.');
                                            $objValidation->setFormula1($asset_type_information->name);
                                        }else
                                        {
                                            if(strtoupper($asset_type_information->name) == 'MANUFACTURE_ID')
                                            {
                                                $objValidation = $sheet->getCell($alphabets[$key].$j)->getDataValidation();
                                                $objValidation->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
                                                $objValidation->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
                                                $objValidation->setAllowBlank(false);
                                                $objValidation->setShowInputMessage(true);
                                                $objValidation->setShowErrorMessage(true);
                                                $objValidation->setShowDropDown(true);
                                                $objValidation->setErrorTitle('Input error');
                                                $objValidation->setError('Nilai tidak ada dalam list.');
                                                $objValidation->setPromptTitle('Pilih Pabrikan');
                                                $objValidation->setPrompt('Silahkan input sesuai dengan list.');
                                                $objValidation->setFormula1('manufacture_list');
                                            }
                                        }
                                    }
                                   
                                    $j++;
                                }
                            }
        
                            $sheet->setColumnFormat(array(
                                $alphabets[$key] => '@'
                            ));
                        }
                    });
        
                    $excel->sheet('list_data',function($sheet) use ($company_id,$factory_id,$department,$asset_type_informations,$alphabets)
                    {
                        $variantsSheet = $sheet->_parent->getSheet(1); // Variants Sheet , "0" is the Index of Variants Sheet
        
                        $manufactures = Manufacture::select('name')
                        ->whereNull('delete_at')
                        ->where([
                            ['department_id',$department],
                            ['company_id',$company_id],
                        ])
                        ->groupby('name')
                        ->get();
                        $total_manufacture = count($manufactures)+1;
                        $sheet->SetCellValue("A1", "LIST_PABRIKAN");
                        $j = 2;
                        foreach ($manufactures as $key => $manufacture) 
                        {
                            $sheet->SetCellValue("A".$j, $manufacture->name);
                            $j++;
                        }
        
                        $budgets       = Budget::select('unique_id','name')
                        ->whereNull('deleted_at')
                        ->where([
                            ['department_id',$department],
                            ['factory_id',$factory_id],
                        ])
                        ->groupby('name', 'unique_id')
                        ->get();

                        $total_budgets = count($budgets)+1;
                    
                        $sheet->SetCellValue("B1", "LIST_BUDGET");
                        $l = 2;
                        foreach ($budgets as $key => $budget) 
                        {
                            $sheet->SetCellValue("B".$l, $budget->unique_id.'_'.$budget->name);
                            $l++;
                        }

                        $sheet->SetCellValue("C1", "LIST_DURATION");
                        $sheet->SetCellValue("C2", '14_TWO WEEKS');
                        $sheet->SetCellValue("C3", '30_ONE_MONTHS');
                       
        
                        foreach ($asset_type_informations as $key => $asset_type_information) 
                        {
                            if($asset_type_information->is_custom && $asset_type_information->type == 'option')
                            {
                                $column_name = $asset_type_information->name.'_'.str_slug($asset_type_information->label,'_');
        
                                $sheet->setCellValue($alphabets[$key].'1',$column_name);
                                $has_coma = strpos($asset_type_information->value, ",");
                                $is_need_expolde = ($has_coma) ? true : false;
                               
                                if($is_need_expolde)
                                {
                                    $splits = explode(',',$asset_type_information->value);
                                    $k = 2;
                                    foreach ($splits as $key_1 => $split) 
                                    {
                                        $sheet->SetCellValue($alphabets[$key].$k, $split);
                                        $k++;
                                    }
                                }
        
                                $sheet->_parent->addNamedRange(
                                    new \PHPExcel_NamedRange(
                                        $asset_type_information->name, $variantsSheet, $alphabets[$key].'2:'.$alphabets[$key].($k-1) // You can also replace 'A2:A7' with $variantsSheet ->calculateWorksheetDimension()
                                    )
                                );
                            }else if($asset_type_information->is_custom && $asset_type_information->type == 'lookup')
                            {
                                $column_name        = $asset_type_information->name.'_'.str_slug($asset_type_information->label,'_');
                                $custom_lookup_id   = $asset_type_information->lookup_id;
                                $lists              = ConsumableAsset::select('consumable_assets.id as consumable_asset_id'
                                ,'consumable_asset_details.id as consumable_asset_detail_id'
                                ,'consumable_assets.model as consumable_asset_model'
                                ,'consumable_assets.serial_number as consumable_asset_serial_number'
                                ,'consumable_asset_details.value as consumable_asset_detail_value')
                                ->where('asset_type_id',$custom_lookup_id)
                                ->leftjoin('consumable_asset_details','consumable_asset_details.consumable_asset_id','consumable_assets.id')
                                ->get();
                                
                                $sheet->setCellValue($alphabets[$key].'1',$column_name);
                                
                                if(count($lists) > 0)
                                {
                                    $k = 2;
                                    foreach ($lists as $key_1 => $list) 
                                    {
                                        $lookup_id                  = $list->consumable_asset_id;
                                        $detail_lookup_id           = ($list->consumable_asset_detail_id)? $list->consumable_asset_detail_id : '-1';
                                        $model_header               = $list->consumable_asset_model;
                                        $serial_number_header       = $list->consumable_asset_serial_number;
                                        $detail_value               = ($list->consumable_asset_detail_value) ? $list->consumable_asset_detail_value : null;
                                        $lookup_value               = $custom_lookup_id.'|'.$lookup_id.'|'.$detail_lookup_id.';'.$model_header;
        
                                        if($serial_number_header)   $lookup_value .= ':'.strtoupper($serial_number_header);
                                        if($detail_value)           $lookup_value .= ':'.strtoupper($detail_value);
                                        
                                        $sheet->SetCellValue($alphabets[$key].$k, $lookup_value);
                                        $k++;
                                    }
                                }
        
                                $sheet->_parent->addNamedRange(
                                    new \PHPExcel_NamedRange(
                                        $asset_type_information->name, $variantsSheet, $alphabets[$key].'2:'.$alphabets[$key].($k-1) // You can also replace 'A2:A7' with $variantsSheet ->calculateWorksheetDimension()
                                    )
                                );
                            }
        
                        }
        
                        $sheet->_parent->addNamedRange(
                            new \PHPExcel_NamedRange(
                                'manufacture_list', $variantsSheet, 'A2:A'.$total_manufacture // You can also replace 'A2:A7' with $variantsSheet ->calculateWorksheetDimension()
                            )
                        );
        
                        $sheet->_parent->addNamedRange(
                            new \PHPExcel_NamedRange(
                                'budget_list', $variantsSheet, 'B2:B'.$total_budgets // You can also replace 'A2:A7' with $variantsSheet ->calculateWorksheetDimension()
                            )
                        );
                        
                        $sheet->_parent->addNamedRange(
                            new \PHPExcel_NamedRange(
                                'duration_rent_list', $variantsSheet, 'C2:C3' // You can also replace 'A2:A7' with $variantsSheet ->calculateWorksheetDimension()
                            )
                        );
        
                        
                    });
        
                   
        
                    $excel->setActiveSheetIndex(0);
        
        
        
                })->export('xlsx');
            }else
            {
                echo 'tipe asset '.$asset_type->name.' sudah di hapus';
            }
        }else
        {
            return redirect()->action('AssetController@import'); 
        }
        
        
    }
    
    public function uploadFormImport(Request $request)
    {  
        $return_barcode     = array();
        $array              = array();
        $update_consumables = array();

        if($request->hasFile('upload_file'))
        {
            $validator = Validator::make($request->all(), [
                'upload_file' => 'required|mimes:xls'
            ]);

            $path           = $request->file('upload_file')->getRealPath();
            $data           = Excel::selectSheets('active')->load($path,function($render){})
            ->setDateFormat('Y-m-d')
            ->get();

            $movement_date  = $request->current_time;

            if(!empty($data) && $data->count())
            {
                try 
                {
                    DB::beginTransaction();
                    foreach ($data as $key => $value) 
                    {
                        $asset_type_id          = $value->asset_type_id;
                        $company_id             = $value->company_id;
                        $factory_id             = $value->factory_id;
                        $department_id          = $value->department_id;
                        $_budget_id             = $value->budget_id;
                        $price_rent             = $value->price_rent;
                        $_duration_rent_in_days = $value->duration_rent_in_days;
                        $manufacture_name       = strtolower($value->manufacture_id);
                        $serial_number          = strtolower(trim($value->serial_number));
                        $model                  = strtolower(trim($value->model));
                        $supplier_name          = strtolower(trim($value->supplier_name));
                        $description            = strtolower(trim($value->description));
                        $no_rent                = strtolower(trim($value->no_rent));
                        $remark_rent            = strtolower(trim($value->remark_rent));
                        $receiving_date         = ($value->receiving_date != '') ? Carbon::createFromFormat('Y-m-d', $value->receiving_date)->format('Y-m-d') : null;
                        $start_rent_date        = ($value->start_rent_date != '') ? Carbon::createFromFormat('Y-m-d', $value->start_rent_date)->format('Y-m-d') : null;
                        $end_rent_date          = ($value->end_rent_date != '') ? Carbon::createFromFormat('Y-m-d', $value->end_rent_date)->format('Y-m-d') : null;
                        $no_inventory_manual    = null;
                        $erp_no_asset           = null;
                        $purchase_number        = null;
                        
                        if($_budget_id)
                        {
                            $has_underscore = strpos($_budget_id, "_");
                            if($has_underscore)
                            {
                                $split_budget   = explode('_',$_budget_id);
                                $budget_id      = $split_budget[0];
                            }else
                            {
                                $budget_id = null;
                            }
                        }else
                        {
                            $budget_id = null;
                        }
                        
                        if($_duration_rent_in_days)
                        {
                            $has_underscore = strpos($_duration_rent_in_days, "_");
                            if($has_underscore)
                            {
                                $split_duration   = explode('_',$_duration_rent_in_days);
                                $duration_rent_in_days      = $split_duration[0];
                            }else
                            {
                                $duration_rent_in_days = null;
                            }
                        }else
                        {
                            $duration_rent_in_days = null;
                        }
                        
                        if($asset_type_id && $company_id && $factory_id && $department_id && $serial_number && $model && $budget_id && $no_rent && $price_rent && $duration_rent_in_days && $receiving_date && $start_rent_date && $end_rent_date)
                        {
                            $budget                     = Budget::where('unique_id',$budget_id)->first();
                            $company                    = Company::find($company_id);
                            $asset_type                 = AssetType::find($asset_type_id);
                            $factory                    = Factory::find($factory_id);
                            $asset_information_fields   = AssetInformationField::whereNull('delete_at')
                            ->where([
                                ['asset_type_id',$asset_type_id],
                                ['is_custom',true],
                            ])
                            ->orderby('created_at','asc')
                            ->get();

                            $_department                = Department::find($department_id);

                            $_manufacture               = Manufacture::whereNull('delete_at')
                            ->where([
                                [db::raw('lower(name)'),$manufacture_name],
                                ['company_id',$company_id],
                                ['department_id',$department_id],
                            ])
                            ->first();

                            if(!$company)
                            {
                                $obj                            = new stdClass();
                                $obj->asset_type_name           = ($asset_type ? $asset_type->name : null);
                                $obj->department_name           = ($_department ? $_department->name : null);
                                $obj->manufacture_name          = ($_manufacture) ? $_manufacture->name : null;
                                $obj->supplier_name             = $supplier_name;
                                $obj->serial_number             = $serial_number;
                                $obj->no_inventory              = null;
                                $obj->no_inventory_manual       = $no_inventory_manual;
                                $obj->no_asset                  = $erp_no_asset;
                                $obj->model                     = $model;
                                $obj->purchase_number           = $purchase_number;
                                $obj->receiving_date            = $receiving_date;
                                $obj->is_error                  = true;
                                $obj->start_rent_date           = $start_rent_date;
                                $obj->end_rent_date             = $end_rent_date;
                                $obj->price_rent                = $price_rent;
                                $obj->duration_rent_in_days     = $duration_rent_in_days;
                                $obj->budget                    = $budget->name;

                                foreach ($asset_information_fields as $key_2 => $asset_information_field) 
                                {

                                    $custom_field_name           = $asset_information_field->name;
                                    $_custom_field_name          = $asset_information_field->name.'_'.str_slug($asset_information_field->label,'_');
                                    $obj->$custom_field_name     = strtolower($value->$_custom_field_name);
                                }
                            
                                $obj->system_log                = 'FAILED, Company not found';
                                $array []   = $obj;
                            }else
                            {
                                if(!$factory)
                                {
                                    $obj                            = new stdClass();
                                    $obj->asset_type_name           = ($asset_type ? $asset_type->name : null);
                                    $obj->department_name           = ($_department ? $_department->name : null);
                                    $obj->manufacture_name          = ($_manufacture) ? $_manufacture->name : null;
                                    $obj->supplier_name             = $supplier_name;
                                    $obj->serial_number             = $serial_number;
                                    $obj->no_inventory              = null;
                                    $obj->no_inventory_manual       = $no_inventory_manual;
                                    $obj->no_asset                  = $erp_no_asset;
                                    $obj->model                     = $model;
                                    $obj->purchase_number           = $purchase_number;
                                    $obj->receiving_date            = $receiving_date;
                                    $obj->is_error                  = true;
                                    $obj->start_rent_date           = $start_rent_date;
                                    $obj->end_rent_date             = $end_rent_date;
                                    $obj->price_rent                = $price_rent;
                                    $obj->duration_rent_in_days     = $duration_rent_in_days;
                                    $obj->budget                    = $budget->name;

                                    foreach ($asset_information_fields as $key_2 => $asset_information_field) 
                                    {

                                        $custom_field_name           = $asset_information_field->name;
                                        $_custom_field_name          = $asset_information_field->name.'_'.str_slug($asset_information_field->label,'_');
                                        $obj->$custom_field_name     = strtolower($value->$_custom_field_name);
                                    }
                                
                                    $obj->system_log                = 'Failed, Factory not found';
                                    $array []   = $obj;
                                }else
                                {
                                    if(!$asset_type)
                                    {
                                        $obj                            = new stdClass();
                                        $obj->asset_type_name           = ($asset_type ? $asset_type->name : null);
                                        $obj->department_name           = ($_department ? $_department->name : null);
                                        $obj->manufacture_name          = ($_manufacture) ? $_manufacture->name : null; 
                                        $obj->supplier_name             = $supplier_name;
                                        $obj->serial_number             = $serial_number;
                                        $obj->no_inventory              = null;
                                        $obj->no_inventory_manual       = $no_inventory_manual;
                                        $obj->no_asset                  = $erp_no_asset;
                                        $obj->model                     = $model;
                                        $obj->purchase_number           = $purchase_number;
                                        $obj->receiving_date            = $receiving_date;
                                        $obj->is_error                  = true;
                                        $obj->start_rent_date           = $start_rent_date;
                                        $obj->end_rent_date             = $end_rent_date;
                                        $obj->price_rent                = $price_rent;
                                        $obj->duration_rent_in_days     = $duration_rent_in_days;
                                        $obj->budget                    = $budget->name;

                                        foreach ($asset_information_fields as $key_2 => $asset_information_field) 
                                        {

                                            $custom_field_name           = $asset_information_field->name;
                                            $_custom_field_name          = $asset_information_field->name.'_'.str_slug($asset_information_field->label,'_');
                                            $obj->$custom_field_name     = strtolower($value->$_custom_field_name);
                                        }
                                    
                                        $obj->system_log                = 'FAILED, Asset Type not found';
                                        $array []   = $obj;
                                    }else
                                    {
                                        if(!$_department)
                                        {
                                            $obj                            = new stdClass();
                                            $obj->asset_type_name           = ($asset_type ? $asset_type->name : null);
                                            $obj->department_name           = ($_department ? $_department->name : null);
                                            $obj->manufacture_name          = ($_manufacture) ? $_manufacture->name : null; 
                                            $obj->supplier_name             = $supplier_name;
                                            $obj->serial_number             = $serial_number;
                                            $obj->no_inventory              = null;
                                            $obj->no_inventory_manual       = $no_inventory_manual;
                                            $obj->no_asset                  = $erp_no_asset;
                                            $obj->model                     = $model;
                                            $obj->purchase_number           = $purchase_number;
                                            $obj->receiving_date            = $receiving_date;
                                            $obj->is_error                  = true;
                                            $obj->no_rent                   = $no_rent;
                                            $obj->start_rent_date           = $start_rent_date;
                                            $obj->end_rent_date             = $end_rent_date;
                                            $obj->price_rent                = $price_rent;
                                            $obj->duration_rent_in_days     = $duration_rent_in_days;
                                            $obj->budget                    = $budget->name;

                                            foreach ($asset_information_fields as $key_2 => $asset_information_field) 
                                            {

                                                $custom_field_name           = $asset_information_field->name;
                                                $_custom_field_name          = $asset_information_field->name.'_'.str_slug($asset_information_field->label,'_');
                                                $obj->$custom_field_name     = strtolower($value->$_custom_field_name);
                                            }
                                        
                                            $obj->system_log                = 'FAILED,Departement code is empty. Please fill it first';
                                            $array []   = $obj;
                                        }else
                                        {
                                            if($asset_type->delete_at)
                                            {
                                                $obj                            = new stdClass();
                                                $obj->asset_type_name           = ($asset_type ? $asset_type->name : null);
                                                $obj->department_name           = ($_department ? $_department->name : null);
                                                $obj->manufacture_name          = ($_manufacture) ? $_manufacture->name : null; 
                                                $obj->supplier_name             = $supplier_name;
                                                $obj->serial_number             = $serial_number;
                                                $obj->no_inventory              = null;
                                                $obj->no_inventory_manual       = $no_inventory_manual;
                                                $obj->no_asset                  = $erp_no_asset;
                                                $obj->model                     = $model;
                                                $obj->purchase_number           = $purchase_number;
                                                $obj->receiving_date            = $receiving_date;
                                                $obj->is_error                  = true;
                                                $obj->no_rent                   = $no_rent;
                                                $obj->start_rent_date           = $start_rent_date;
                                                $obj->end_rent_date             = $end_rent_date;
                                                $obj->price_rent                = $price_rent;
                                                $obj->duration_rent_in_days     = $duration_rent_in_days;
                                                $obj->budget                    = $budget->name;

                                                foreach ($asset_information_fields as $key_2 => $asset_information_field) 
                                                {
        
                                                    $custom_field_name           = $asset_information_field->name;
                                                    $_custom_field_name          = $asset_information_field->name.'_'.str_slug($asset_information_field->label,'_');
                                                    $obj->$custom_field_name     = strtolower($value->$_custom_field_name);
                                                }
                                            
                                                $obj->system_log                = 'FAILED, Asset type is deleted';
                                                $array []   = $obj;
                                            }else
                                            {
                                                $is_exists      = Asset::where([
                                                    ['company_id',$company_id],
                                                    ['factory_id',$factory_id],
                                                    ['department_id',$department_id],
                                                    ['origin_company_location_id',$company_id],
                                                    ['origin_factory_location_id',$factory_id],
                                                    ['origin_department_location_id',$department_id],
                                                    ['asset_type_id',$asset_type_id],
                                                    ['no_rent',$no_rent],
                                                    ['is_rent',true],
                                                ])
                                                ->whereNull('return_date')
                                                ->whereNull('delete_at');
                                        
                                                if($model) $is_exists                   = $is_exists->where('model',$model);
                                                if($serial_number) $is_exists           = $is_exists->where('serial_number',$serial_number);
                                                if($_manufacture) $is_exists            = $is_exists->where('manufacture_id',($_manufacture)? $_manufacture->id : null);
                                               
                                                $is_exists = $is_exists->exists();
    
                                                if($is_exists)
                                                {
                                                    $obj                            = new stdClass();
                                                    $obj->asset_type_name           = ($asset_type ? $asset_type->name : null);
                                                    $obj->department_name           = ($_department ? $_department->name : null);
                                                    $obj->manufacture_name          = ($_manufacture) ? $_manufacture->name : null;
                                                    $obj->supplier_name             = $supplier_name;
                                                    $obj->serial_number             = $serial_number;
                                                    $obj->no_inventory              = null;
                                                    $obj->no_inventory_manual       = $no_inventory_manual;
                                                    $obj->no_asset                  = $erp_no_asset;
                                                    $obj->model                     = $model;
                                                    $obj->purchase_number           = $purchase_number;
                                                    $obj->receiving_date            = $receiving_date;
                                                    $obj->is_error                  = true;
                                                    $obj->no_rent                   = $no_rent;
                                                    $obj->start_rent_date           = $start_rent_date;
                                                    $obj->end_rent_date             = $end_rent_date;
                                                    $obj->price_rent                = $price_rent;
                                                    $obj->duration_rent_in_days     = $duration_rent_in_days;
                                                    $obj->budget                    = $budget->name;

                                                    foreach ($asset_information_fields as $key_2 => $asset_information_field) 
                                                    {
    
                                                        $custom_field_name           = $asset_information_field->name;
                                                        $_custom_field_name          = $asset_information_field->name.'_'.str_slug($asset_information_field->label,'_');
                                                        $obj->$custom_field_name     = strtolower($value->$_custom_field_name);
                                                    }
                                                
                                                    $obj->system_log                = 'FAILED,Data already exists';
                                                    $array []   = $obj;
                                                }else
                                                {
                                                    $month                          = ($value->receiving_date)? Carbon::createFromFormat('Y-m-d', $value->receiving_date)->format('m') : Carbon::now()->format('m');
                                                    $year                           = ($value->receiving_date)? Carbon::createFromFormat('Y-m-d', $value->receiving_date)->format('y') : Carbon::now()->format('y');
                                                    $_erp_asset_group_name          = $asset_type->erp_asset_group_name;
                                                    $_erp_asset_group_id            = $asset_type->erp_asset_group_id;
                                                    $_referral_code                 = $_erp_asset_group_name.'.'.strtoupper($_department->code).'.'.strtoupper($factory->code).'.'.strtoupper($asset_type->code);
                                                    $_no_inventory                  = $_erp_asset_group_name.'.'.strtoupper($_department->code).'.'.strtoupper($factory->code).'.'.strtoupper($asset_type->code).'.'.$month.'.'.$year;
                                                    $sequence                       = Asset::where('referral_code',$_referral_code)->max('sequence');

                                                    if ($sequence == null) $sequence = 1;
                                                    else $sequence                  += 1;

                                                    if($sequence < 10) $sequence_format = '00'.$sequence;
                                                    else if($sequence < 100) $sequence_format = '0'.$sequence;
                                                    else $sequence_format = $sequence;

                                                    $no_inventory                   = $_no_inventory.'.'.$sequence_format;

                                                    
                                                    $asset = Asset::firstorCreate([
                                                        'asset_type_id'                     => $asset_type_id,
                                                        'company_id'                        => $company_id,
                                                        'factory_id'                        => $factory_id,
                                                        'department_id'                     => $_department->id,
                                                        'origin_company_location_id'        => $company_id,
                                                        'origin_factory_location_id'        => $factory_id,
                                                        'origin_department_location_id'     => $_department->id,
                                                        'manufacture_id'                    => ($_manufacture ? $_manufacture->id : null),
                                                        'budget_id'                         => $budget->unique_id,
                                                        'barcode'                           => 'BELUM DIPRINT',
                                                        'serial_number'                     => $serial_number,
                                                        'no_inventory'                      => $no_inventory,
                                                        'no_inventory_manual'               => '-',
                                                        'description'                       => $description,
                                                        'erp_no_asset'                      => '-',
                                                        'erp_asset_id'                      => '-',
                                                        'model'                             => $model,
                                                        'purchase_number'                   => '-',
                                                        'supplier_name'                     => $supplier_name,
                                                        'receiving_date'                    => $receiving_date,
                                                        'erp_asset_group_id'                => $_erp_asset_group_id,
                                                        'erp_asset_group_name'              => $_erp_asset_group_name,
                                                        'erp_item_id'                       => '-',
                                                        'erp_item_code'                     => '-',
                                                        'referral_code'                     => $_referral_code,
                                                        'sequence'                          => $sequence,
                                                        'status'                            => 'siap digunakan',
                                                        'last_company_location_id'          => $company_id,
                                                        'last_factory_location_id'          => $factory_id,
                                                        'last_department_location_id'       => $_department->id,
                                                        'last_user_movement_id'             => Auth::user()->id,
                                                        'create_user_id'                    => Auth::user()->id,
                                                        'delete_at'                         => null,
                                                        'is_rent'                           => true,
                                                        'duration_rent_in_days'             => $duration_rent_in_days,
                                                        'no_rent'                           => $no_rent,
                                                        'price_rent'                        => $price_rent,
                                                        'start_rent_date'                   => $start_rent_date,
                                                        'end_rent_date'                     => $end_rent_date,
                                                        'return_date'                       => null,
                                                        'created_at'                        => $movement_date,
                                                        'updated_at'                        => $movement_date,
                                                        'last_movement_date'                => $movement_date,    
                                                    ]);

                                                    if($asset->erp_no_asset)
                                                    {
                                                        Temporary::firstorCreate([
                                                            'column_1'  => $asset->id,
                                                            'column_2'  => 'sync_asset_id_erp',
                                                        ]);
                                                    }
                                            
                                                    $asset_movement = AssetMovement::firstorCreate([
                                                        'asset_id'                          => $asset->id,
                                                        'status'                            => 'siap digunakan',
                                                        'from_company_location'             => $asset->company_id,
                                                        'from_factory_location'             => $asset->factory_id,
                                                        'from_department_location'          => $asset->department_id,
                                                        'from_employee_nik'                 => auth::user()->nik,
                                                        'from_employee_name'                => auth::user()->name,
                                                        'movement_date'                     => $movement_date,
                                                        'note'                              => 'asset baru',
                                                    ]);

                                                    $asset->last_asset_movement_id = $asset_movement->id;
                                                    $asset->save();
                                                    
                                                    foreach ($asset_information_fields as $key_2 => $asset_information_field) 
                                                    {

                                                        $custom_field_name           = $asset_information_field->name;
                                                        $is_lookup                   = ($asset_information_field->type == 'lookup') ? true :false;
                                                        $_custom_field_name          = $asset_information_field->name.'_'.str_slug($asset_information_field->label,'_');
                                                        
                                                        if($is_lookup)
                                                        {
                                                            $lookup_value            = $value->$_custom_field_name;
                                                            $has_simicolon           = strpos($lookup_value, ";");
                                                            $is_need_lookup          = ($has_simicolon) ? true : false;

                                                            if($is_need_lookup)
                                                            {
                                                                $split_simicolons           = explode(';',$lookup_value);
                                                                $custom_lookup_value        = $split_simicolons[0];
                                                                $has_pipe                   = strpos($custom_lookup_value, "|");
                                                                $_is_need_lookup            = ($has_pipe) ? true : false;
                                                                
                                                                if($_is_need_lookup)
                                                                {
                                                                    $split_pipelines                = explode('|',$custom_lookup_value);
                                                                    $custom_lookup_id               = $split_pipelines[0];
                                                                    $lookup_id                      = $split_pipelines[1];
                                                                    $lookup_detail_id               = $split_pipelines[2];

                                                                    $is_consumable_header_exists    = ConsumableAsset::where('id',$lookup_id)->exists();
                                                                    
                                                                    if($is_consumable_header_exists)
                                                                    {
                                                                        ConsumableAssetMovement::firstorCreate([
                                                                            'asset_id'                          => $asset->id,
                                                                            'consumable_asset_id'               => $lookup_id,
                                                                            'consumable_asset_detail_id'        => ($lookup_detail_id != '-1'? $lookup_detail_id : null),
                                                                            'status'                            => 'digunakan',
                                                                            'from_company_location'             => $asset->company_id,
                                                                            'from_factory_location'             => $asset->factory_id,
                                                                            'from_department_location'          => $asset->deparment_id,
                                                                            'from_employee_nik'                 => auth::user()->nik,
                                                                            'from_employee_name'                => auth::user()->name,
                                                                            'to_company_location'               => $asset->company_id,
                                                                            'to_factory_location'               => $asset->factory_id,
                                                                            'to_department_location'            => $asset->deparment_id,
                                                                            'movement_date'                     => $movement_date,
                                                                            'is_using_in_custom_field'          => true,
                                                                            'system_note'                       => 'digunakan untuk asset, dengan nomor inventori '.$asset->no_inventory.'. dikeluarkan pada tanggal '.$movement_date->format('d/M/Y').' oleh '.auth::user()->name,
                                                                        ]);
                                                    
                                                                        $update_consumables [] = $lookup_id;
                                                                    }
                                                                }
                                                                
                                                                $asset->$custom_field_name   = $custom_lookup_value;
                                                            }
                                                        }else
                                                        {
                                                            $asset->$custom_field_name   = strtolower($value->$_custom_field_name);
                                                        }
                                                        $asset->save();
                                                    }
    
                                                    $obj                            = new stdClass();
                                                    $obj->asset_type_name           = ($asset_type ? $asset_type->name : null);
                                                    $obj->department_name           = ($_department ? $_department->name : null);
                                                    $obj->manufacture_name          = ($_manufacture) ? $_manufacture->name : null;
                                                    $obj->serial_number             = $serial_number;
                                                    $obj->no_inventory              = $no_inventory;
                                                    $obj->supplier_name             = $supplier_name;
                                                    $obj->no_inventory_manual       = $no_inventory_manual;
                                                    $obj->no_asset                  = $erp_no_asset;
                                                    $obj->model                     = $model;
                                                    $obj->purchase_number           = $purchase_number;
                                                    $obj->receiving_date            = $receiving_date;
                                                    $obj->no_rent                   = $no_rent;
                                                    $obj->start_rent_date           = $start_rent_date;
                                                    $obj->end_rent_date             = $end_rent_date;
                                                    $obj->price_rent                = $price_rent;
                                                    $obj->duration_rent_in_days     = $duration_rent_in_days;
                                                    $obj->is_error                  = false;
                                                    $obj->budget                    = $budget->name;

                                                    foreach ($asset_information_fields as $key_2 => $asset_information_field) 
                                                    {

                                                        $custom_field_name           = $asset_information_field->name;
                                                        $_custom_field_name          = $asset_information_field->name.'_'.str_slug($asset_information_field->label,'_');
                                                        $obj->$custom_field_name     = strtolower($value->$_custom_field_name);
                                                    }
                                                
                                                    $obj->system_log                = 'success';
                                                    $array []   = $obj;
                                                    $return_barcode [] = $asset->id;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                    }  

                    $detail_consumable_assets = ConsumableAssetMovement::select('consumable_asset_id',db::raw('count(0) as total_qty_used'))
                    ->whereIn('consumable_asset_id',$update_consumables)
                    ->where([
                        ['status','digunakan'],
                        ['is_return',false]
                    ])
                    ->groupby('consumable_asset_id')
                    ->get();

                    foreach ($detail_consumable_assets as $key => $detail_consumable_asset) 
                    {
                        $consumable_asset                   = ConsumableAsset::find($detail_consumable_asset->consumable_asset_id);
                        $total                              = $consumable_asset->total;
                        $new_available_qty                  = $total - $detail_consumable_asset->total_qty_used;

                        $consumable_asset->qty_used         = $detail_consumable_asset->total_qty_used;
                        $consumable_asset->qty_available    = $new_available_qty;
                        $consumable_asset->save();
                    }

                    DB::commit();   
                } catch (Exception $e) 
                {
                    DB::rollBack();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }

                return response()->json([
                    'data'      => $array,
                    'barcodes'  => $return_barcode,
                ],200);
            }else
            {
                return response()->json(['message' => 'import gagal, silahkan cek file anda'],422);
            }


        }
    }

    public function history($id)
    {
        $asset = Asset::find($id);
        return view('asset_rent.history',compact('asset'));
    }

    public function historyData(Request $request, $id)
    {
        if(request()->ajax()) 
        {
            $data = db::select(db::raw("select status
                ,movement_date
                ,from_company.name as from_company_name
                ,from_factory.name as from_factory_name
                ,from_department.name from_department_location
                ,from_employee_nik
                ,from_employee_name
                ,to_company.name as to_company_name
                ,to_factory.name as to_factory_name
                ,to_department.name as to_department_location
                ,to_area_location
                ,to_employee_nik
                ,to_employee_name
                ,no_kk
                ,no_bea_cukai
                ,note
                ,is_handover_asset
                from asset_movements 
                left join companies as from_company on asset_movements.from_company_location = from_company.id 
                left join factories as from_factory on asset_movements.from_factory_location = from_factory.id
                left join departments as from_department on asset_movements.from_department_location = from_department.id 
                left join companies as to_company on asset_movements.to_company_location = to_company.id 
                left join factories as to_factory on asset_movements.to_factory_location = to_factory.id 
                left join departments as to_department on asset_movements.to_department_location = to_department.id 
                where asset_movements.asset_id = '".$id."'
                order by movement_date desc"));

            return datatables()->of($data)
            ->editColumn('movement_date',function($data)
            {
                return  Carbon::createFromFormat('Y-m-d H:i:s', $data->movement_date)->format('d/M/Y H:i:s');
            })
            ->editColumn('status',function($data)
            {
                if($data->is_handover_asset) return 'Penerimaan asset dari pindah tangan';
                else return ucwords($data->status);
            })
            ->editColumn('from',function($data)
            {
                if($data->from_company_name && $data->from_factory_name && $data->from_department_location && $data->from_employee_nik)
                {
                    return '<b>Perusahaan</b><br/>'.ucwords($data->from_company_name).
                    '<br><b>Pabik</b><br/>'.ucwords($data->from_factory_name).
                    '<br><b>Departemen</b><br/>'.ucwords($data->from_department_location).
                    '<br><b>Nik</b><br/>'.ucwords($data->from_employee_nik).
                    '<br><b>Nama</b><br/>'.ucwords($data->from_employee_name);
                }else
                {
                    if($data->status != 'diterima') return '<br><b>Nama Teknisi</b><br/>'.ucwords($data->from_employee_name);
                }
                 
            })
            ->editColumn('to',function($data)
            {
                if($data->note != 'asset baru')
                {
                    if($data->status == 'dikeluarkan' || $data->status == 'dipindah tangan' )
                    {
                        return '<b>Perusahaan</b><br/>'.ucwords($data->to_company_name).
                        '<br><b>Pabik</b><br/>'.ucwords($data->to_factory_name).
                        '<br><b>Departemen</b><br/>'.ucwords($data->to_department_location).
                        '<br><b>Area</b><br/>'.ucwords($data->to_area_location);
                    }
                    else if($data->status =='dipinjam' || $data->status == 'dipindah tangan')
                    {
    
                        return '<b>Perusahaan</b><br/>'.ucwords($data->to_company_name).
                        '<br><b>Pabik</b><br/>'.ucwords($data->to_factory_name).
                        '<br><b>Departemen</b><br/>'.ucwords($data->to_department_location);
    
                    }else if($data->status == 'diperbaiki')
                    {
                        return '<br><b>Nama Teknisi</b><br/>'.ucwords($data->to_employee_name);
                    }else if ($data->status == 'obsolete') 
                    {
                        return '<b>Siap di BAPB</b><br/>';
                    }else
                    {
                        return '<b>Perusahaan</b><br/>'.ucwords($data->to_company_name).
                        '<br><b>Pabik</b><br/>'.ucwords($data->to_factory_name).
                        '<br><b>Departemen</b><br/>'.ucwords($data->to_department_location).
                        '<br><b>Nik</b><br/>'.ucwords($data->to_employee_nik).
                        '<br><b>Nama</b><br/>'.ucwords($data->to_employee_name);
                    }
                }
            })
            ->editColumn('no_kk',function($data){
            	return ucwords($data->no_kk);
            })
            ->editColumn('no_bea_cukai',function($data){
            	return ucwords($data->no_bea_cukai);
            })
            ->rawColumns(['from','to'])
            ->make(true);
        }
    }

    public function getLookup(Request $request)
    {
        $q      = strtolower(trim($request->q));
        $lists   = ConsumableAsset::select('consumable_assets.id as consumable_asset_id'
        ,'consumable_asset_details.id as consumable_asset_detail_id'
        ,'consumable_assets.model as consumable_asset_model'
        ,'consumable_assets.serial_number as consumable_asset_serial_number'
        ,'consumable_asset_details.value as consumable_asset_detail_value')
        ->where('asset_type_id',$request->custom_lookup_id)
        ->leftjoin('consumable_asset_details','consumable_asset_details.consumable_asset_id','consumable_assets.id')
        ->where(function($query) use ($q)
        {
            $query->where(db::raw('lower(consumable_assets.model)'),'LIKE',"%$q%")
            ->orWhere(db::raw('lower(consumable_assets.serial_number)'),'LIKE',"%$q%")
            ->orWhere(db::raw('lower(consumable_asset_details.value)'),'LIKE',"%$q%");
        })
        ->paginate(10);

        return view('asset._loookup_value',compact('lists'));
    }

    static function getStatus(Request $request)
    {
        $company_id         = auth::user()->company_id;
        $factory_id         = $request->factory_id;
        $sub_department_id  = $request->sub_department_id;

        $status         = Asset::select('status')
        ->where('origin_company_location_id',$company_id)
        ->where('origin_factory_location_id',$factory_id);

        if($sub_department_id != 'null') $status = $status->where('origin_sub_department_location_id',$sub_department_id);
        
        $status = $status->groupby('status')
        ->pluck('status','status')
        ->all();

        return response()->json(['status'=>$status]);
    }

    static function getCustomField(Request $request)
    {
        $asset_id               = $request->asset_id;
        $asset_type_id          = $request->asset_type_id;
        $asset                  = Asset::find($asset_id);
        $asset_custom_fields    = AssetInformationField::where([
            ['asset_type_id', $asset_type_id],
            ['is_custom', true],
        ])
        ->whereNull('delete_at')
        ->orderby('created_at','asc')
        ->get();

        $array = array();
        foreach ($asset_custom_fields as $key => $value) 
        {

            $has_coma           = strpos($value->value, ",");
            $is_need_expolde    = ($has_coma) ? true : false;
            $custom_field       = $value->name;
            $custom_lookup_id   = $value->lookup_id;
            $current_value      = ($asset ? $asset->$custom_field:null);
            $has_pipe           = strpos($current_value, "|");
            $is_need_lookup     = ($has_pipe) ? true : false;
            $options            = array();

            if($is_need_expolde)
            {
                $splits = explode(',',$value->value);
                foreach ($splits as $key_1 => $split) 
                {
                    if(strtoupper($current_value) == strtoupper($split)) $selected = true;
                    else $selected = false;

                    $obj            = new stdClass();
                    $obj->id        = $split;
                    $obj->name      = $split;
                    $obj->selected  = $selected;
                    $options []     = $obj;
                }
            }
           // dd($has_pipe);
            if($is_need_lookup)
            {
                $splits                     = explode('|',$current_value);
                $custom_lookup_id           = $splits[0];
                $lookup_id                  = $splits[1];
                $detail_lookup_id           = $splits[2];

                $consumable_asset           = ConsumableAsset::where([
                    ['asset_type_id',$custom_lookup_id],
                    ['id',$lookup_id],
                ])
                ->first();

                $consumable_asset_detail    = ConsumableAssetDetail::where([
                    ['consumable_asset_id',$lookup_id],
                    ['id',$detail_lookup_id],
                ])
                ->first();

                $model_header               = ($consumable_asset ? $consumable_asset->model : null);
                $serial_number_header       = ($consumable_asset ? $consumable_asset->serial_number : null);
                $detail_value               = ($consumable_asset_detail ? $consumable_asset_detail->value : null);
                $lookup_value               = $model_header;

                if($serial_number_header)   $lookup_value .= ':'.strtoupper($serial_number_header);
                if($detail_value)           $lookup_value .= ':'.strtoupper($detail_value);
                
                $current_value              = $lookup_value;
            }

            $consumable_asset       = ConsumableAssetMovement::where([
                ['asset_id',$asset_id],
                ['is_using_in_custom_field',true],
            ])
            ->whereIn('consumable_asset_id',function($query) use($custom_lookup_id)
            {
                $query->select('id')
                ->from('consumable_assets')
                ->where('asset_type_id',$custom_lookup_id);
            })
            ->first();

            $obj                        = new stdClass();
            $obj->id                    = $value->id;
            $obj->asset_type            = $value->assetType->name;
            $obj->name                  = $custom_field;
            $obj->default_value         = (count($options) > 0)? $options : null;
            $obj->label                 = ucwords($value->label);
            $obj->is_option             = ($value->type == 'option')? true:false;
            $obj->is_number             = ($value->type == 'number')? true:false;
            $obj->is_text               = ($value->type == 'text')? true:false;
            $obj->is_lookup             = ($value->type == 'lookup')? true:false;
            $obj->is_required           = ($value->required)? true:false;
            $obj->custom_lookup_id      = $value->lookup_id;
            $obj->lookup_id             = ($consumable_asset)? $consumable_asset->consumable_asset_id : null;
            $obj->detail_lookup_id      = ($consumable_asset)? $consumable_asset->consumable_asset_detail_id : null;
            $obj->value                 = ucwords($current_value);
            $array []                   = $obj;
        }

        return response()->json($array);
    }

    static function getAssetTypeAndManufacture(Request $request)
    {

        $flag               = $request->flag;
        $sub_department_id  = $request->sub_department_id;
        $company_id         = auth::user()->company_id;

        if($flag == 'asset_type')
        {
            $asset_type_id  = $request->asset_type_id;
            $asset_types = AssetType::whereNull('delete_at')
            ->where([
                ['sub_department_id',$sub_department_id],
                ['company_id',$company_id],
                ['is_consumable',false],
            ])
            ->pluck('name','id')
            ->all();
            
            return response()->json(['asset_types' => $asset_types,'asset_type_id' => $asset_type_id]);
        }else
        {
            $manufacture_id = $request->manufacture_id;
            $manufactures = Manufacture::whereNull('delete_at')
            ->where([
                ['sub_department_id',$sub_department_id],
                ['company_id',$company_id],
            ])
            ->pluck('name','id')
            ->all();

            return response()->json(['manufactures' => $manufactures,'manufacture_id' => $manufacture_id]);
        }
    }

    static function getBudget(Request $request)
    {

        $budget_id              = $request->budget_id;
        $sub_department_id      = $request->sub_department_id;
        $factory_id             = $request->factory_id;
        $budgets                = Budget::whereNull('deleted_at')
        ->where([
            ['sub_department_id',$sub_department_id],
            ['factory_id',$factory_id],
        ])
        ->groupby('name', 'unique_id')
        ->pluck('name', 'unique_id')
        ->all();

        return response()->json(['budgets' => $budgets,'budget_id' => $budget_id]);
    }

    static function randomCode()
    {
        $referral_code = 'A'.Carbon::now()->format('u');
        $sequence = Barcode::where('referral_code',$referral_code)->max('sequence');

        if ($sequence == null) $sequence = 1;
        else $sequence += 1;
        
        Barcode::FirstOrCreate([
            'barcode'       => $referral_code.$sequence.'-2',
            'referral_code' => $referral_code,
            'sequence'      => $sequence,
        ]);

        return $referral_code.$sequence.'-2';
    }

    public function showImage($filename)
    {
        $path = Config::get('storage.asset');
        $resp = response()->download($path . '/' . $filename);
        $resp->headers->set('Content-Disposition', 'inline');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');
        return $resp;
    }

    public function printout(Request $request,$id)
    {
        return view('asset.printout');
    }
}
