<?php namespace App\Http\Controllers;

use DB;
use Auth;
use File;
use Excel;
use Config;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Session;

use App\Models\Factory;
use App\Models\Calendar;
use App\Models\SubDepartment;

class ReportAssetOpnameController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $factories          = Factory::where('company_id',auth::user()->company_id)->whereNull('delete_at')->pluck('name', 'id')->all();
        $sub_departments    = SubDepartment::where('is_other',false)
        ->whereIn('code',['ict','elk','ga','me'])
        ->whereNull('delete_at')
        ->pluck('name', 'id')
        ->all();
        
        $factory_id         = auth::user()->factory_id;
        $sub_department_id  = auth::user()->sub_department_id;

        return view('report_asset_opname.index',compact('factories','sub_departments','factory_id','sub_department_id'));
    }

    public function export(Request $request)
    {
        $sub_department_id  = $request->_sub_department;
        $factory_id         = $request->_factory;
        $data               = DB::select(db::raw("SELECT *  FROM dashboard_detail_asset('$factory_id','$sub_department_id');" ));

        return Excel::create('report_asset_opname',function($excel) use($data,$sub_department_id,$factory_id)
        {
            $excel->sheet('active',function($sheet) use($data,$factory_id)
            {
                $sheet->setCellValue('A1','#');
                $sheet->setCellValue('B1','Asset Type Name');
                $sheet->setCellValue('C1','Total');
                
                $events = Calendar::whereNull('deleted_at')
                ->orderby('event_start','asc')
                ->get();

                $current_column = 'D';
                foreach ($events as $key => $event) 
                {
                    $event_name = $event->event_name.' ('.$event->event_start->format('d/M/Y').'-'.$event->event_end->format('d/M/Y').')';
                    $sheet->setCellValue($current_column.'1',$event_name);
                    $current_column++;
                }

                $no     = 1;
                $row    = 2;
                foreach ($data as $key => $data) 
                {
                    $sheet->setCellValue('A'.$row,$no);
                    $sheet->setCellValue('B'.$row,$data->name);
                    $sheet->setCellValue('C'.$row,$data->total_all);
                    $current_column = 'D';

                    $_events = Calendar::whereNull('deleted_at')
                    ->orderby('event_start','asc')
                    ->get();

                    foreach ($_events as $key => $_event) 
                    {
                        $scanned = DB::select(db::raw("
                            select count(distinct assets.id) as total_sto
                            From asset_movements
                            join assets on assets.id = asset_movements.asset_id
                            left join calendars on calendars.id = asset_movements.calendar_id
                            where asset_movements.status = 'sto'
                            and asset_movements.calendar_id is not null
                            and assets.asset_type_id = '$data->id'
                            and assets.origin_factory_location_id = '$factory_id'
                            and calendars.id = '$_event->id'
                        "));

                        $total_sto = 'total_scanned : '.$scanned[0]->total_sto.',variance : '.($scanned[0]->total_sto - $data->total_all);
                        $sheet->setCellValue($current_column.$row,$total_sto);
                        $current_column++;
                    }

                    $no++;
                    $row++;
                }


            });

            $events = Calendar::whereNull('deleted_at')
            ->orderby('event_start','asc')
            ->get();

            foreach ($events as $key => $event) 
            {
                $event_name = 'pending_('.$event->event_start->format('d-m-Y').'_'.$event->event_end->format('d-m-Y').')';
                
                $excel->sheet($event_name,function($sheet) use($sub_department_id,$factory_id,$event)
                {
                    $sheet->setCellValue('A1','#');
                    $sheet->setCellValue('B1','Asset Type');
                    $sheet->setCellValue('C1','Serial Number');
                    $sheet->setCellValue('D1','No Asset');
                    $sheet->setCellValue('E1','No Inventory');
                    $sheet->setCellValue('F1','No Inventory Manual');
                    $sheet->setCellValue('G1','Last Location');
                    
                    $oustandings = DB::select(db::raw("
                        select asset_v.asset_type,
                        asset_v.serial_number,
                        asset_v.erp_no_asset as no_asset,
                        asset_v.no_inventory,
                        asset_v.no_inventory_manual,
                        asset_v.last_company_name,
                        asset_v.last_factory_name,
                        asset_v.last_department_name,
                        asset_v.last_used_by,
                        asset_v.last_area_location,
                        dtl.calendar_id
                        from asset_v
                        left join (
                                select asset_id,calendar_id
                                from asset_movements
                                where status = 'sto'
                                and exists (
                                        select 1 from assets
                                        where assets.id = asset_movements.asset_id
                                        and origin_factory_location_id = '$factory_id'
                                        and origin_sub_department_location_id = '$sub_department_id' 
                                )
                                and asset_movements.calendar_id = '$event->id'
                                GROUP BY asset_id,calendar_id
                        )dtl on dtl.asset_id = asset_v.id
                        where origin_factory_location_id = '$factory_id'
                        and origin_sub_department_location_id = '$sub_department_id'
                        and dtl.calendar_id is null
                    "));;

                    $no     = 1;
                    $row    = 2;
                    foreach ($oustandings as $key => $oustanding) 
                    {
                        $sheet->setCellValue('A'.$row,$no);
                        $sheet->setCellValue('B'.$row,$oustanding->asset_type);
                        $sheet->setCellValue('C'.$row,$oustanding->serial_number);
                        $sheet->setCellValue('D'.$row,$oustanding->no_asset);
                        $sheet->setCellValue('E'.$row,$oustanding->no_inventory);
                        $sheet->setCellValue('F'.$row,$oustanding->no_inventory_manual);
                        $sheet->setCellValue('G'.$row,$oustanding->last_company_name.','.$oustanding->last_factory_name.','.$oustanding->last_department_name.','.$oustanding->last_used_by.','.$oustanding->last_area_location);

                        $no++;
                        $row++;
                    }
                    
    
    
                });
            }
        })
        ->export('xlsx');
        
    }

    
}
