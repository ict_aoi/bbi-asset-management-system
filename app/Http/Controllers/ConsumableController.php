<?php namespace App\Http\Controllers;

use DB;
use Auth;
use File;
use Excel;
use Config;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Session;
use Intervention\Image\ImageManagerStatic as Image;

use App\Models\Area;
use App\Models\User;
use App\Models\Asset;
use App\Models\Company;
use App\Models\Factory;
use App\Models\Barcode;
use App\Models\AssetType;
use App\Models\Department;
use App\Models\Manufacture;
use App\Models\SubDepartment;
use App\Models\ConsumableAsset;
use App\Models\ConsumableAssetDetail;
use App\Models\AssetInformationField;
use App\Models\ConsumableAssetMovement;


class ConsumableAssetController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $msg       = $request->session()->get('message');
        if(auth::user()->is_super_admin)
        {
            $company_id             = auth::user()->company_id;
            $department_id          = null;
            $factory_id             = null;
            $sub_department_id      = null;
        }else
        {
            $company_id             = auth::user()->company_id;
            $department_id          = auth::user()->department_id;
            $factory_id             = auth::user()->factory_id;
            $sub_department_id      = auth::user()->sub_department_id;
        } 

        
        $companies          = Company::where('id',$company_id)->whereNull('delete_at')->pluck('name', 'id')->all();
        $sub_departments    = SubDepartment::where('is_other',false)
        ->whereIn('code',['ict','elk','ga','me'])
        ->whereNull('delete_at')
        ->pluck('name', 'id')
        ->all();

        return view('consumable.index',compact('msg','companies','sub_departments','company_id','department_id','factory_id','sub_department_id'));
    }

    
    public function data(Request $request)
    {
        if(request()->ajax()) 
        {
            $company_id             = $request->company;
            $sub_department_id      = $request->sub_department;
            $asset_type_id          = $request->asset_type;

            $data = DB::table('consumable_v')
            ->where(function($query) use($company_id)
            {
                $query = $query->Where('company_id','LIKE',"%$company_id%");
            })
            ->where(function($query) use($sub_department_id)
            {
                $query = $query->Where('sub_department_id','LIKE',"%$sub_department_id%");
            })
            ->where(function($query) use($asset_type_id)
            {
                $query = $query->Where('asset_type_id','LIKE',"%$asset_type_id%");
            });

            return datatables()->of($data)
            ->addColumn('action', function($data) {
                return view('consumable._action', [
                    'model'    => $data,
                    'checkin' => route('consumable.checkin',$data->id),
                    'checkout' => route('consumable.checkout',$data->id),
                    'edit'     => route('consumable.edit',$data->id),
                    'delete'   => route('consumable.destroy',$data->id),
                    'history'  => route('consumable.history',$data->id),
                ]);
            })
            ->editColumn('company',function($data){
            	return ucwords($data->company);
            })
            ->editColumn('asset_type',function($data){
            	return ucwords($data->asset_type);
            })
            ->editColumn('manufacture',function($data){
            	return ucwords($data->manufacture);
            })
            ->editColumn('sub_department_name',function($data){
            	return ucwords($data->sub_department_name);
            })
            ->editColumn('no_asset',function($data){
            	return strtoupper($data->no_asset);
            })
            ->editColumn('model',function($data){
            	return ucwords($data->model);
            })
            ->make(true);
        }
    }

    public function create(Request $request)
    {
        if($request->session()->has('message')) $request->session()->forget('message');
        
        if(auth::user()->is_super_admin)
        {
            $company_id             = auth::user()->company_id;
            $department_id          = null;
            $factory_id             = null;
            $sub_department_id      = null;
        }else
        {
            $company_id             = auth::user()->company_id;
            $department_id          = auth::user()->department_id;
            $factory_id             = auth::user()->factory_id;
            $sub_department_id      = auth::user()->sub_department_id;
        } 

        
        $companies          = Company::where('id',$company_id)->whereNull('delete_at')->pluck('name', 'id')->all();
        $sub_departments    = SubDepartment::where('is_other',false)
        ->whereIn('code',['ict','elk','ga','me'])
        ->whereNull('delete_at')
        ->pluck('name', 'id')
        ->all();

        return view('consumable.create',compact('companies','sub_departments','company_id','department_id','factory_id','sub_department_id'));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
    		'company'=>'required',
    		'asset_type'=>'required',
    	]);
 
        $is_exists  = ConsumableAsset::where([
            ['asset_type_id',$request->asset_type],
            ['company_id',$request->company],
            ['sub_department_id',$request->sub_department],
            ['manufacture_id',$request->manufacture],
            [db::raw('lower(model)'),strtolower($request->model)]
        ])
        ->whereNull('delete_at');

        if($request->serial_number) $is_exists = $is_exists->where(db::raw('lower(serial_number)'),strtolower($request->serial_number));
        
        $is_exists = $is_exists->exists();

        if($is_exists) return response()->json(['message' => 'data already exists'],422);
        
        try 
        {
            DB::beginTransaction();
            $consumable_asset = ConsumableAsset::firstorCreate([
                'asset_type_id'                     => $request->asset_type,
                'company_id'                        => $request->company,
                'sub_department_id'                 => $request->sub_department,
				'manufacture_id'                    => $request->manufacture,
				'serial_number'                     => $request->serial_number,
                'model'                             => $request->model,
                'no_asset'                          => $request->no_asset,
                'model'                             => strtolower($request->model),
                'total'                             => $request->total_all,
                'qty_used'                          => 0,
                'qty_available'                     => $request->total_all,
                'create_user_id'                    => Auth::user()->id,
                'delete_at'                         => null,
            ]);

            $custom_fields = json_decode($request->custom_fields);
            foreach ($custom_fields as $key => $custom_field) 
            {
                $custom_field_name                      = $custom_field->name;
                $value                                  = $custom_field->value;
                $consumable_asset->$custom_field_name   = strtolower($value);
            }

            $consumable_asset->save();

            $details = json_decode($request->details);
            foreach ($details as $key => $detail) 
            {
                ConsumableAssetDetail::firstorCreate([
                    'consumable_asset_id'               => $consumable_asset->id,
                    'value'                             => $detail->detail_value,
                    'no_asset'                          => $detail->detail_no_asset,
                ]);
            }
            DB::commit();

            $request->session()->flash('message', 'success');
            return response()->json('success',200);
            
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
        
       
        return response()->json(200);
    }

    public function storeDetail(Request $request,$consumable_asset_id)
    {
        try 
        {
            DB::beginTransaction();
            ConsumableAssetDetail::firstorCreate([
                'consumable_asset_id'               => $consumable_asset_id,
                'value'                             => $request->value,
                'no_asset'                          => $request->no_asset,
            ]);
            
            $consumable_asset   = ConsumableAsset::find($consumable_asset_id);
            $consumable_details = $consumable_asset->consumableAssetDetail()->get();
            $details            = array();

            foreach ($consumable_details as $key => $consumable_detail) 
            {
                $obj                    = new stdClass;
                $obj->id                = $consumable_detail->id;
                $obj->detail_value      = $consumable_detail->value;
                $obj->detail_no_asset   = $consumable_detail->no_asset;
                $details []             = $obj;
            }
            
            DB::commit();

            return response()->json(['message' => 'success_1','details' => $details],200);
            
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
        
       
        return response()->json(200);
    }

    public function edit(Request $request,$id)
    {
        if($request->session()->has('message')) $request->session()->forget('message');
        
        $consumable_asset       = ConsumableAsset::find($id);
        
        $companies          = Company::where('id',$consumable_asset->company_id)->whereNull('delete_at')->pluck('name', 'id')->all();
        $sub_departments    = SubDepartment::where('is_other',false)
        ->whereIn('code',['ict','elk','ga','me'])
        ->whereNull('delete_at')
        ->pluck('name', 'id')
        ->all();

             
        $consumable_details = $consumable_asset->consumableAssetDetail()->get();
        $details            = array();

        foreach ($consumable_details as $key => $consumable_detail) 
        {
            $obj                    = new stdClass;
            $obj->detail_value      = $consumable_detail->value;
            $obj->detail_no_asset   = $consumable_detail->no_asset;
            $details []             = $obj;
        }
        return view('consumable.edit',compact('sub_departments','consumable_asset','companies','details'));
    }

    public function dataDetailConsumable(Request $request,$id)
    {
        if(request()->ajax()) 
        {
            $data = db::select(db::raw("select consumable_asset_details.id as id
            ,consumable_asset_details.value
            ,consumable_asset_details.no_asset
            from consumable_asset_details 
            where consumable_asset_details.consumable_asset_id = '".$id."'
            and consumable_asset_details.delete_at is null
            order by created_at desc
            "));

            return datatables()->of($data)
            ->addColumn('action', function($data)use($id){
                return view('consumable._action_modal', [
                    'model' => $data,
                    'update' => route('consumable.editDetail',[$id,($data)?$data->id : null]),
                    'delete' => route('consumable.destroyDetail',[$id,($data)?$data->id : null]),
                ]);

               
            })
            ->make(true);
        }
        
    }

    public function editDetail(Request $request,$consumable_asset_id,$consumable_asset_detail_id)
    {
        $consumable_asset_detail = ConsumableAssetDetail::find($consumable_asset_detail_id);
        return response()->json([
            'consumable_asset_detail'   => $consumable_asset_detail, 
            'consumable_asset_id'       => $consumable_asset_id,
            'url_update_detail'         => route('consumable.updateDetail',[$consumable_asset_id,$consumable_asset_detail_id])
        ],200);
    }

    public function update(Request $request, $id)
    {
        $is_exists  = ConsumableAsset::where([
            ['id','!=',$id],
            ['asset_type_id',$request->asset_type_id],
            ['company_id',$request->company_id],
            ['manufacture_id',$request->manufacture],
            ['sub_department_id',$request->sub_department_id],
            [db::raw('lower(model)'),strtolower($request->model)]
        ])
        ->whereNull('delete_at');

        if($request->serial_number) $is_exists = $is_exists->where(db::raw('lower(serial_number)'),strtolower($request->serial_number));
        
        $is_exists = $is_exists->exists();

        if($is_exists)
        {
            return response()->json(['message' => 'data sudah ada'],422);
        }

        try 
        {
            DB::beginTransaction();
            $consumable_asset                   = ConsumableAsset::find($id);
            $total_old                          = $consumable_asset->total;
            $qty_used                           = $consumable_asset->qty_used;
            $new_total                          = $request->total_all;
            $new_qty_available                  = $new_total - $qty_used;
            
            $consumable_asset->manufacture_id   = $request->manufacture;
            $consumable_asset->no_asset         = $request->no_asset;
            $consumable_asset->model            = strtolower($request->model);
            $consumable_asset->serial_number    = $request->serial_number;
            $consumable_asset->total            = $new_total;
            $consumable_asset->qty_available    = $new_qty_available;

            

            $custom_fields = json_decode($request->custom_fields);
            foreach ($custom_fields as $key => $custom_field) 
            {
                $custom_field_name                      = $custom_field->name;
                $value                                  = $custom_field->value;
                $consumable_asset->$custom_field_name   = strtolower($value);
            }

            $consumable_asset->save();
            DB::commit();

            $request->session()->flash('message', 'success_2');
            return response()->json('success',200);
            
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
        
       
        return response()->json(200);
    }

    public function updateDetail(Request $request, $consumable_asset_id,$consumable_asset_detail_id)
    {
        $consumable_asset_detail = ConsumableAssetDetail::find($consumable_asset_detail_id);
      
        try 
        {
            DB::beginTransaction();
            $consumable_asset_detail->value      = $request->value;
            $consumable_asset_detail->no_asset   = $request->no_asset;
            $consumable_asset_detail->save();
            
            $consumable_asset   = ConsumableAsset::find($consumable_asset_id);
            $consumable_details = $consumable_asset->consumableAssetDetail()->get();
            $details            = array();

            foreach ($consumable_details as $key => $consumable_detail) 
            {
                $obj                    = new stdClass;
                $obj->id                = $consumable_detail->id;
                $obj->detail_value      = $consumable_detail->value;
                $obj->detail_no_asset   = $consumable_detail->no_asset;
                $details []             = $obj;
            }
            
            DB::commit();

            return response()->json(['message' => 'success_2','details' => $details],200);
            
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    public function destroy($id)
    {
        $consumable_asset                   = ConsumableAsset::find($id);
        $consumable_asset->delete_at        = carbon::now();
        $consumable_asset->delete_user_id   = auth::user()->id;
        $consumable_asset->save();
    }

    public function destroyDetail($consumable_asset_id,$consumable_asset_detail_id)
    {
        $consumable_asset_detail                    = ConsumableAssetDetail::find($consumable_asset_detail_id);
        $consumable_asset_detail->delete_at         = carbon::now();
        $consumable_asset_detail->delete_user_id    = auth::user()->id;
        $consumable_asset_detail->save();

        $consumable_asset   = ConsumableAsset::find($consumable_asset_id);
        $consumable_details = $consumable_asset->consumableAssetDetail()->get();
        $details            = array();

        foreach ($consumable_details as $key => $consumable_detail) 
        {
            $obj                    = new stdClass;
            $obj->detail_value      = $consumable_detail->value;
            $obj->detail_no_asset   = $consumable_detail->no_asset;
            $details []             = $obj;
        }

        return response()->json($details,200);
    }

    public function import(Request $request)
    {
        if($request->session()->has('message')) $request->session()->forget('message');
        
        $companies          = Company::where('id',auth::user()->company_id)->whereNull('delete_at')->pluck('name', 'id')->all();
        $factories          = Factory::where('company_id',auth::user()->company_id)->whereNull('delete_at')->pluck('name', 'id')->all();
        $sub_departments    = SubDepartment::where('is_other',false)
        ->whereIn('code',['ict','elk','ga','me'])
        ->whereNull('delete_at')
        ->pluck('name', 'id')
        ->all();

        return view('consumable.import',compact('companies','factories','sub_departments'));
    }

    public function exportFormImport(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company' => 'required',
            'factory' => 'required',
            'department' => 'required',
            'asset_type' => 'required',
        ]);

        if($request->asset_type == '') return redirect()->back()->with(['message' => 'silahkan pilih tipe asset terlebih dahulu'],'422');

        $company_id = $request->company;
        $factory_id = $request->factory;
        $department = $request->department;
        $asset_type_id = $request->asset_type;
       
        $asset_type = AssetType::find($asset_type_id);
        $asset_type_informations = $asset_type->assetInformationFields($asset_type->id,$asset_type->is_consumable);

        $alphabets = array(
            0 => 'A',
            1 => 'B',
            2 => 'C',
            3 => 'D',
            4 => 'E',
            5 => 'F',
            6 => 'G',
            7 => 'H',
            8 => 'I',
            9 => 'J',
            10 => 'K',
            11 => 'L',
            12 => 'M',
            13 => 'N',
            14 => 'O',
            15 => 'P',
            16 => 'Q',
            17 => 'R',
            18 => 'S',
            19 => 'T',
            20 => 'U',
            21 => 'V',
            22 => 'W',
            23 => 'X',
            24 => 'Y',
            25 => 'Z',
            26 => 'AA',
            27 => 'AB',
            28 => 'AC',
            29 => 'AD',
            30 => 'AE',
            31 => 'AF',
            32 => 'AG',
            33 => 'AH',
            34 => 'AI',
            35 => 'AJ',
            36 => 'AK',
            37 => 'AL',
            38 => 'AM',
            39 => 'AN',
            40 => 'AO',
            41 => 'AP',
            42 => 'AQ',
            43 => 'AR',
            44 => 'AS',
            45 => 'AT',
            46 => 'AU',
            47 => 'AV',
            48 => 'AW',
            49 => 'AX',
            50 => 'AY',
            51 => 'AZ',
        );

        //return response()->json($array);

        return Excel::create('upload_asset',function ($excel) use($company_id,$factory_id,$department,$asset_type_id,$asset_type_informations,$alphabets) 
        {
            $excel->sheet('active', function($sheet) use($company_id,$factory_id,$department,$asset_type_id,$asset_type_informations,$alphabets) 
            {
                foreach ($asset_type_informations as $key => $asset_type_information) 
                {
                    if($asset_type_information->is_custom) $column_name = $asset_type_information->name.'_'.str_slug($asset_type_information->label,'_');
                    else $column_name = $asset_type_information->name;

                    $sheet->setCellValue($alphabets[$key].'1',$column_name);
                    $j = 2;

                    if(in_array(strtoupper($asset_type_information->name),['COMPANY_ID','ASSET_TYPE_ID','FACTORY_ID','DEPARTMENT_NAME','MANUFACTURE_ID'])
                        || ($asset_type_information->is_custom && $asset_type_information->type == 'option'))
                    {
                        if(strtoupper($asset_type_information->name) == 'COMPANY_ID') $value = $company_id;
                        elseif(strtoupper($asset_type_information->name) == 'FACTORY_ID') $value = $factory_id;
                        elseif(strtoupper($asset_type_information->name) == 'ASSET_TYPE_ID') $value = $asset_type_id;
                        elseif(strtoupper($asset_type_information->name) == 'DEPARTMENT_NAME') $value = $department;

                        if(in_array(strtoupper($asset_type_information->name),['COMPANY_ID','ASSET_TYPE_ID','FACTORY_ID','DEPARTMENT_NAME']))
                        {
                            $sheet->cell($alphabets[$key].'1', function($cell) 
                            {
                                $cell->setBackground('#000000');
                                $cell->setFontColor('#ffffff');
                            });
                        }
                        

                        for ($i=0; $i < 100; $i++) 
                        { 
                            if(in_array(strtoupper($asset_type_information->name),['COMPANY_ID','ASSET_TYPE_ID','FACTORY_ID','DEPARTMENT_NAME']))
                            {
                                $sheet->setCellValue($alphabets[$key].$j,$value);
                                $sheet->cell($alphabets[$key].$j, function($cell) {
                                    $cell->setBackground('#000000');
                                    $cell->setFontColor('#ffffff');
                                });
                            } 
                            else 
                            {
                                if(in_array(strtoupper($asset_type_information->name),['MODEL','SERIAL_NUMBER']))
                                {
                                    $sheet->cell($alphabets[$key].$j, function($cell) {
                                        $cell->setBackground('#ff4d4d');
                                        $cell->setFontColor('#ffffff');
                                    });
                                }

                                if($asset_type_information->is_custom && $asset_type_information->type == 'option')
                                {
                                    $objValidation = $sheet->getCell($alphabets[$key].$j)->getDataValidation();
                                    $objValidation->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
                                    $objValidation->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
                                    $objValidation->setAllowBlank(false);
                                    $objValidation->setShowInputMessage(true);
                                    $objValidation->setShowErrorMessage(true);
                                    $objValidation->setShowDropDown(true);
                                    $objValidation->setErrorTitle('Input error');
                                    $objValidation->setError('Nilai tidak ada dalam list.');
                                    $objValidation->setPromptTitle('Pilih Nilai');
                                    $objValidation->setPrompt('Silahkan input sesuai dengan list.');
                                    $objValidation->setFormula1($asset_type_information->name);
                                }else
                                {
                                    $objValidation = $sheet->getCell($alphabets[$key].$j)->getDataValidation();
                                    $objValidation->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
                                    $objValidation->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
                                    $objValidation->setAllowBlank(false);
                                    $objValidation->setShowInputMessage(true);
                                    $objValidation->setShowErrorMessage(true);
                                    $objValidation->setShowDropDown(true);
                                    $objValidation->setErrorTitle('Input error');
                                    $objValidation->setError('Nilai tidak ada dalam list.');
                                    $objValidation->setPromptTitle('Pilih Pabrikan');
                                    $objValidation->setPrompt('Silahkan input sesuai dengan list.');
                                    $objValidation->setFormula1('manufacture_list');
                                }
                                
                                
                            }
                           
                            $j++;
                        }
                    }

                    $sheet->setColumnFormat(array(
                        $alphabets[$key] => '@'
                    ));
                }
            });

            $excel->sheet('list_pabrikan',function($sheet) use ($company_id,$department,$asset_type_informations,$alphabets)
            {
                $variantsSheet = $sheet->_parent->getSheet(1); // Variants Sheet , "0" is the Index of Variants Sheet

                $manufactures = Manufacture::select('name')
                ->whereNull('delete_at')
                ->where([
                    ['department_name',$department],
                    ['company_id',$company_id],
                ])
                ->groupby('name')
                ->get();
            
                $sheet->SetCellValue("A1", "LIST_PABRIKAN");
                $j = 2;
                foreach ($manufactures as $key => $manufacture) 
                {
                    $sheet->SetCellValue("A".$j, $manufacture->name);
                    $j++;
                }

                foreach ($asset_type_informations as $key => $asset_type_information) 
                {
                    if($asset_type_information->is_custom && $asset_type_information->type == 'option')
                    {
                        $column_name = $asset_type_information->name.'_'.str_slug($asset_type_information->label,'_');

                        $sheet->setCellValue($alphabets[$key].'1',$column_name);
                        $has_coma = strpos($asset_type_information->value, ",");
                        $is_need_expolde = ($has_coma) ? true : false;
                       
                        if($is_need_expolde)
                        {
                            $splits = explode(',',$asset_type_information->value);
                            $k = 2;
                            foreach ($splits as $key_1 => $split) 
                            {
                                $sheet->SetCellValue($alphabets[$key].$k, $split);
                                $k++;
                            }
                        }

                        $sheet->_parent->addNamedRange(
                            new \PHPExcel_NamedRange(
                                $asset_type_information->name, $variantsSheet, $alphabets[$key].'2:'.$alphabets[$key].($k-1) // You can also replace 'A2:A7' with $variantsSheet ->calculateWorksheetDimension()
                            )
                        );
                    }
                }

                $sheet->_parent->addNamedRange(
                    new \PHPExcel_NamedRange(
                        'manufacture_list', $variantsSheet, 'A2:A'.$j // You can also replace 'A2:A7' with $variantsSheet ->calculateWorksheetDimension()
                    )
                );

                
            });

           

            $excel->setActiveSheetIndex(0);



        })->export('xlsx');
    }
    
    public function uploadFormImport(Request $request)
    {  
        $array = array();
        if($request->hasFile('upload_file')){
            $validator = Validator::make($request->all(), [
                'upload_file' => 'required|mimes:xls'
            ]);

            $path = $request->file('upload_file')->getRealPath();
            $data = Excel::selectSheets('active')->load($path,function($render){})->get();

            if(!empty($data) && $data->count())
            {
                foreach ($data as $key => $value) 
                {
                }
                   
                return response()->json($array,200);
            }else
            {
                return response()->json(['message' => 'import gagal, silahkan cek file anda'],422);
            }


        }
    }

    public function history($id)
    {
        $consumable_asset = ConsumableAsset::find($id);
        return view('consumable.history',compact('consumable_asset'));

    }

    public function historyData(Request $request, $id)
    {
        if(request()->ajax()) 
        {
            
            $data = DB::table('consumable_history_v')->where('consumable_asset_id',$id)->orderby('movement_date','desc');

            return datatables()->of($data)
            ->editColumn('status',function($data){
            	return ucwords($data->status);
            })
            ->editColumn('from',function($data){
                 return '<b>Perusahaan</b><br/>'.ucwords($data->from_company_name).
                '<br><b>Pabik</b><br/>'.ucwords($data->from_factory_name).
                '<br><b>Sub Departemen</b><br/>'.ucwords($data->from_sub_department_name).
                '<br><b>Nik</b><br/>'.ucwords($data->from_employee_nik).
                '<br><b>Nama</b><br/>'.ucwords($data->from_employee_name);
            })
            ->editColumn('movement_date',function($data){
            	return  Carbon::createFromFormat('Y-m-d H:i:s', $data->movement_date)->format('d/M/Y H:i:s');
            })
            ->editColumn('is_return',function($data){
            	if ($data->is_return) return '<div class="checker border-info-600 text-info-800">
                    <span class="checked"><input type="checkbox" class="control-info" checked="checked"></span>
                </div>';
            	else return null;
            })
            ->editColumn('to',function($data){
                if($data->asset_id)
                {
                    $_consumable_asset_value = ucwords($data->consumable_asset_model);
                    if($data->consumable_asset_serial_number) $_consumable_asset_value = $_consumable_asset_value.'-'.ucwords($data->consumable_asset_model);
                    if($data->consumable_asset_detail_value) $_consumable_asset_value = $_consumable_asset_value.'-'.ucwords($data->consumable_asset_detail_value);

                    return '<b>Asset</b><br/>'.ucwords($data->asset_serial_number);
                }else
                {
                    $_consumable_asset_value = ucwords($data->consumable_asset_model);
                    if($data->consumable_asset_serial_number) $_consumable_asset_value = $_consumable_asset_value.'-'.ucwords($data->consumable_asset_model);
                    if($data->consumable_asset_detail_value) $_consumable_asset_value = $_consumable_asset_value.'-'.ucwords($data->consumable_asset_detail_value);

                    return '<b>Perusahaan</b><br/>'.ucwords($data->to_company_name).
                    '<br><b>Pabik</b><br/>'.ucwords($data->to_factory_name).
                    '<br><b>Sub Departemen</b><br/>'.ucwords($data->to_sub_department_name).
                    '<br><b>Consumable asset value</b><br/>'.$_consumable_asset_value.
                    '<br><b>Nik</b><br/>'.ucwords($data->to_employee_nik).
                    '<br><b>Nama</b><br/>'.ucwords($data->to_employee_name);
                }
                 
            })
            ->rawColumns(['from','to','is_return'])
            ->make(true);
        }
    }

    public function getAssetTypeAndManufacture(Request $request)
    {
        $flag               =  $request->flag;
        $sub_department_id  = $request->sub_department_id;
        $company_id         = $request->company_id;

        if($flag == 'asset_type')
        {
            $asset_type_id  = $request->asset_type_id;
            $asset_types = AssetType::whereNull('delete_at')
            ->where([
                ['sub_department_id',$sub_department_id],
                ['company_id',$company_id],
                ['is_consumable',true],
            ])
            ->pluck('name','id')
            ->all();
            
            return response()->json(['asset_types' => $asset_types,'asset_type_id' => $asset_type_id]);
        }else
        {
            $manufacture_id = $request->manufacture_id;
            $manufactures = Manufacture::whereNull('delete_at')
            ->where([
                ['sub_department_id',$sub_department_id],
                ['company_id',$company_id],
            ])
            ->pluck('name','id')
            ->all();

            return response()->json(['manufactures' => $manufactures,'manufacture_id' => $manufacture_id]);
        }
    }

    public function getCustomField(Request $request)
    {
        $consumable_asset_id    = $request->consumable_asset_id;
        $asset_type_id          = $request->asset_type_id;
        $consumable_asset       = ConsumableAsset::find($consumable_asset_id);
        $asset_custom_fields    = AssetInformationField::where([
            ['asset_type_id', $asset_type_id],
            ['is_custom', true],
        ])
        ->whereNull('delete_at')
        ->orderby('created_at','asc')
        ->get();

        $array = array();
        foreach ($asset_custom_fields as $key => $value) 
        {

            $has_coma = strpos($value->value, ",");
            $is_need_expolde = ($has_coma) ? true : false;
            $custom_field = $value->name;
            $current_value = ($consumable_asset)? $consumable_asset->$custom_field:null;
            $options = array();

            if($is_need_expolde)
            {
                $splits = explode(',',$value->value);
                foreach ($splits as $key_1 => $split) 
                {
                    if(strtoupper($current_value) == strtoupper($split)) $selected = true;
                    else $selected = false;

                    $obj = new stdClass();
                    $obj->id = $split;
                    $obj->name = $split;
                    $obj->selected = $selected;
                    $options [] = $obj;
                }
            }

            $obj = new stdClass();
            $obj->id = $value->id;
            $obj->asset_type = $value->assetType->name;
            $obj->name = $custom_field;
            $obj->default_value = (count($options) > 0)? $options : null;
            $obj->label = ucwords($value->label);
            $obj->is_option = ($value->type == 'option')? true:false;
            $obj->is_number = ($value->type == 'number')? true:false;
            $obj->is_text = ($value->type == 'text')? true:false;
            $obj->is_required = ($value->required)? true:false;
            $obj->value = ucwords($current_value);
            $array [] = $obj;
        }

        return response()->json($array);
    }
}
