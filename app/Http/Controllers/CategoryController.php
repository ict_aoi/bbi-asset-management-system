<?php namespace App\Http\Controllers;

use DB;
use Auth;
use StdClass;
use Validator;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\User;
use App\Models\Factory;
use App\Models\Company;
use App\Models\Category;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $msg = $request->session()->get('message');
        return view('category.index',compact('msg'));
    }
   
    public function data()
    {
        if(request()->ajax())
        {
            if(auth::user()->is_super_admin)
            {
                $data = db::select(db::raw("select categories.id AS id
                ,categories.name
                ,categories.department_name
                ,companies.name as company_name
                ,categories.description
                from categories 
                left join companies on categories.company_id = companies.id 
                where categories.delete_at is null
                order by companies.name desc,categories.department_name desc"));

            } 
            else
            {
                $data = db::select(db::raw("select categories.id AS id
                ,categories.name
                ,categories.department_name
                ,companies.name as company_name
                ,categories.description
                from categories 
                left join companies on categories.company_id = companies.id 
                where categories.delete_at is null
                and categories.department_name = '".strtolower(auth::user()->department_name)."'
                and categories.company_id = '".strtolower(auth::user()->company_id)."'
                order by categories.created_at desc
                "));
                
            } 

            return datatables()->of($data)
                ->editColumn('name', function ($data) {
                    return ucwords($data->name);
                })
                ->editColumn('department_name', function ($data) {
                    return ucwords($data->department_name);
                })
                ->editColumn('company_name', function ($data) {
                    return ucwords($data->company_name);
                })
                ->editColumn('description', function ($data) {
                    return ucwords($data->description);
                })
            ->addColumn('action', function($data) {
                return view('category._action', [
                    'model' => $data,
                    'edit'  => route('category.edit',$data->id),
                    'delete'=> route('category.destroy',$data->id),
                ]);
            })
            ->rawColumns(['delete_at', 'action'])
            ->make(true);
        }
    }

    public function create(Request $request)
    {
        if($request->session()->has('message')) $request->session()->forget('message');
        $companies = Company::pluck('name', 'id')->all();
        if(auth::user()->is_super_admin)
        {
            $company_id = null;
            $department_name = null;
        }else
        {
            $company_id = auth::user()->company_id;
            $department_name = auth::user()->department_name;
        } 

        return view('category.create',compact('companies','company_id','department_name'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'company' => 'required',
            'department' => 'required'
        ]);

        if (Category::where([
                ['name', strtolower($request->name)],
                ['company_id', $request->company],
                ['department_name', strtolower($request->department)],
            ])
            ->whereNull('delete_at')
            ->exists()) 
        {
            return response()->json(['message' => 'Nama sudah ada, silahkan cari nama lain'], 422);
        }

        try {
           

            DB::beginTransaction();
            Category::firstorCreate([
                'name'             => strtolower($request->name),
                'description'      => strtolower($request->description),
                'department_name'  => strtolower($request->department),
                'company_id'       => $request->company,
                'create_user_id'   => Auth::user()->id
            ]);
            DB::commit();
            $request->session()->flash('message', 'success');
            return response()->json('success', 200);
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    public function edit(Request $request,$id)
    {
        $category         = Category::find($id);
        if($request->session()->has('message')) $request->session()->forget('message');
        $companies = Company::pluck('name', 'id')->all();
        if(auth::user()->is_super_admin)
        {
            $company_id = null;
            $department_name = null;
        }else
        {
            $company_id = auth::user()->company_id;
            $department_name = auth::user()->department_name;
        } 

        return view('category.edit',compact('companies','category'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'company' => 'required',
            'department' => 'required'
        ]);

        if (Category::where([
            ['name', strtolower($request->name)],
            ['company_id', $request->company],
            ['id','!=', $id],
            ['department_name', strtolower($request->department)],
        ])
        ->whereNull('delete_at')
        ->exists()) 
        {
            return response()->json(['message' => 'Nama sudah ada, silahkan cari nama lain'], 422);
        }

        $category                    = Category::findorFail($id);
        $category->company_id        = $request->company;
        $category->name              = strtolower($request->name);
        $category->description       = strtolower($request->description);
        $category->department_name   = strtolower($request->department);
        $category->save();
        $request->session()->flash('message', 'success_2');
        return response()->json('success', 200);
    }

    public function destroy($id)
    {
        $category                 = Category::findorFail($id);
        $category->delete_at      = carbon::now();
        $category->delete_user_id = Auth::user()->id;
        $category->save();

        return response()->json('success', 200);
    }

    public function getDepartment(Request $request)
    {
        $company_id = $request->company_id;
        $department_name = strtolower($request->department_name);

        $departments = User::select('department_name')
        ->where('company_id',$company_id)
        ->groupby('department_name')
        ->pluck('department_name','department_name')
        ->all();

        return response()->json(['departments' => $departments,'department_name' => $department_name],200);

    }
}
