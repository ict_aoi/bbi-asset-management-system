<?php namespace App\Http\Controllers;

use DB;
use Auth;
use File;
use Excel;
use Config;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Session;

use App\Models\Factory;
use App\Models\BcType;
use App\Models\RentalAgreement;

use App\Models\DiasAoi\UserDev;
use App\Models\DiasAoi\UserLive;
use App\Models\DiasAoi\BcTypeDev;
use App\Models\DiasAoi\BcTypeLive;
use App\Models\DiasAoi\RentalAgreementDev;
use App\Models\DiasAoi\RentalAgreementLive;


class ReportOutstandingBCController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $factories          = Factory::where('company_id',auth::user()->company_id)->whereNull('delete_at')->pluck('name', 'id')->all();
        $bc_types           = BcType::whereNull('deleted_at')->orderby('code','asc')->pluck('code', 'id')->all();
        $factory_id         = auth::user()->factory_id;
       
        return view('report_outstanding_bc.index',compact('factories','factory_id','bc_types'));
    }

    public function data(Request $request)
    {
        if(request()->ajax()) 
        {
            $factory_id = $request->factory;
            $type       = $request->type;
            $status     = $request->status;
            
            $data = DB::table('report_outstanding_bc_v')
            ->where(function($query) use ($factory_id)
            {
                $query->where('factory_id','LIKE',"%$factory_id%")
                ->orWhere('destination_factory_id','LIKE',"%$factory_id%");
            });

            if($type == 1) $data = $data->where('rental_agreement_type','no agremeent for rent from outside');
            else if ($type == 2) $data = $data->where('rental_agreement_type','no agremeent for rent from internal');
            else if ($type == 3) $data = $data->where('rental_agreement_type','no agremeent for handover');
            else if ($type == 4) $data = $data->where('rental_agreement_type','no agremeent for obsolete');
            else if ($type == 5) $data = $data->where('rental_agreement_type','no agremeent for return');


            if($status == 1) $data = $data->where('flag',1);
            if($status == 2) $data = $data->where('flag',2);
            else if ($status == 3) $data->where('flag',3);
            
            return datatables()->of($data)
            ->addColumn('type', function($data) 
            {
                if($data->rental_agreement_type == 'no agremeent for rent from outside') return '<span class="label label-info">Rent From External</span>';
                else if($data->rental_agreement_type == 'no agremeent for rent from internal') return '<span class="label label-info">Rent From Internal</span>';
                else if($data->rental_agreement_type == 'no agremeent for handover') return '<span class="label label-success">Handover</span>';
                else if($data->rental_agreement_type == 'no agremeent for obsolete') return '<span class="label label-danger">Obsolete</span>';
                else if($data->rental_agreement_type == 'no agremeent for return') return '<span class="label label-default">Return</span>';

            })
            ->editColumn('bc_in_type',function ($data) use ($status)
            {
                if($status == '3')
                {  
                    $url = route('reportOutstandingBC.edit',$data->id);
                    if($data->bc_in_type) return '<a href="javascript:void(0)" onClick="edit(\''.$url.'\',\'1\')">'.$data->bc_in_type.'</a>';
                    else return $data->bc_in_type;
                }else return $data->bc_in_type;
                
            })
            ->editColumn('bc_out_type',function ($data) use ($status)
            {
                if($status == '3')
                {
                    $url = route('reportOutstandingBC.edit',$data->id);
                    if($data->bc_out_type) return '<a href="javascript:void(0)" onClick="edit(\''.$url.'\',\'2\')">'.$data->bc_out_type.'</a>';
                    else return $data->bc_out_type;
                }else return $data->bc_out_type;
            })
            ->editColumn('start_date',function ($data)
            {
                if($data->is_agreement_for_rental) return  Carbon::createFromFormat('Y-m-d H:i:s', $data->start_date)->format('d/M/Y');
                else return '-';
            })
            ->editColumn('end_date',function ($data)
            {
                if($data->is_agreement_for_rental) return  Carbon::createFromFormat('Y-m-d H:i:s', $data->end_date)->format('d/M/Y');
                else return '-';
            })
            ->addColumn('action', function($data) use ($status)
            {
                return view('report_outstanding_bc._action', [
                    'model'    => $data,
                    'status'   => $status,
                    'detail'   => route('reportOutstandingBC.detail',$data->id),
                    'edit'     => route('reportOutstandingBC.edit',$data->id),
                ]);
            })
            ->setRowAttr([
                'style' => function($data)
                {
                    if(!$data->parent_no_agreement) return  'background-color: #d6d6d6;';
                },
            ])
            ->rawColumns(['action','type','style','bc_in_type','bc_out_type'])
           ->make(true);
        }
    }

    public function detail($id)
    {
        $data = DB::table('rental_agreement_v')
        ->where('id',$id)
        ->first();
       
        if($data) return view('report_outstanding_bc.detail',compact('data'));
        else return redirect()->action('ReportOutstandingBCController@index');
    }

    public function detailData(Request $request,$id)
    {
        $data = RentalAgreement::find($id);
        $parent     = $data->parent_rental_agreement_id;
        $array_parents    = array();
        while($parent != null)
        {
            $rental_agreeement_parent = RentalAgreement::where('id',$parent)
            ->whereNull('deleted_at')
            ->first();

            if($rental_agreeement_parent)
            {
                $array_parents [] = $rental_agreeement_parent->id;
                $parent = $rental_agreeement_parent->parent_rental_agreement_id;
            }else $parent = null;
        }
        
        array_push($array_parents,$data->id);
        $childs = RentalAgreement::whereIn('parent_rental_agreement_id',$array_parents)
        ->whereNull('deleted_at')
        ->get();

        $array_childs = array();
        foreach ($childs as $key => $child) 
        {
            $has_child = RentalAgreement::where('parent_rental_agreement_id',$child->id)
            ->whereNull('deleted_at')
            ->exists();

            $parent_rental_agreement_id = $child->id;

            while($has_child)
            {
                $child_agreeement_parents = RentalAgreement::where('parent_rental_agreement_id',$parent_rental_agreement_id)
                ->whereNull('deleted_at')
                ->get();

                foreach ($child_agreeement_parents as $key => $child_agreeement_parent) 
                {
                    if(!$child_agreeement_parent->deleted_at && !$child_agreeement_parent->return_date)
                    {
                        $array_childs [] = $child_agreeement_parent->id;
                        $parent_rental_agreement_id = $child_agreeement_parent->id;
                        $has_child = RentalAgreement::where('parent_rental_agreement_id',$child_agreeement_parent->id)
                        ->whereNull('deleted_at')
                        ->exists();
                    }else $has_child = false;
                }
                
            }

            array_push($array_childs,$child->id);
        }

        if(request()->ajax()) 
        {
            $data = DB::table('asset_rent_v')
            ->select('asset_type','serial_number','model')
            ->whereIn('no_rent',array_merge($array_parents,$array_childs))
            ->groupby('asset_type','serial_number','model');

            return datatables()->of($data)
            ->make(true);
        }
    }

    public function edit(Request $request,$id)
    {
        $status = $request->status;
        $data   = RentalAgreement::find($id);

        if($data->is_internal)
        {
            if($status == 1) // in
            {
                if(auth::user()->factory_id == $data->destination_factory_id)
                {
                    $obj                        = new StdClass();
                    $obj->id                    = $data->id;
                    $obj->no_registration       = ($data->no_registration_in ? strtoupper($data->no_registration_in) : strtoupper($data->is_internal ? $data->no_registration : $data->no_registration_in));
                    $obj->no_aju                = ($data->no_aju_in ? strtoupper($data->no_aju_in) : strtoupper($data->is_internal ? $data->no_aju : $data->no_aju_in));
                    $obj->no_bc                 = ($data->no_bc_in ? strtoupper($data->no_bc_in) : strtoupper($data->is_internal ? $data->no_bc : $data->no_bc_in));
                    $obj->no_agreement          = strtoupper($data->no_agreement);
                    $obj->bc_type_id            = ($data->bc_type_in_id ? $data->bc_type_in_id : ($data->is_internal ? $data->bc_type_id : $data->bc_type_in_id)); 
                    $obj->date_registration     = ($data->date_registration_in ? $data->date_registration_in->format('d/m/Y') : ($data->is_internal ? ($data->date_registration ? $data->date_registration->format('d/m/Y') : null) : ($data->date_registration_in ? $data->date_registration_in->format('d/m/Y') : null)));
                    $obj->status                = '1';
                }else return response()->json(['message' => 'you dont have permission to update bc in from other factory'],422); 

            }else if ($status == 2) // out
            {
                if(auth::user()->factory_id == $data->factory_id)
                {
                    $obj                        = new StdClass();
                    $obj->id                    = $data->id;
                    $obj->no_registration       = ($data->no_registration ? strtoupper($data->no_registration) : strtoupper($data->is_internal ? $data->no_registration : $data->no_registration_in));
                    $obj->no_aju                = ($data->no_aju ? strtoupper($data->no_aju) : strtoupper($data->is_internal ? $data->no_aju : $data->no_aju_in));
                    $obj->no_bc                 = ($data->no_bc ? strtoupper($data->no_bc) : strtoupper($data->is_internal ? $data->no_bc : $data->no_bc_in));
                    $obj->no_agreement          = strtoupper($data->no_agreement);
                    $obj->bc_type_id            = ($data->bc_type_id ? $data->bc_type_id : ($data->is_internal ? $data->bc_type_id : null)); 
                    $obj->date_registration     = ($data->date_registration ? $data->date_registration->format('d/m/Y') : ($data->is_internal ? ($data->date_registration ? $data->date_registration->format('d/m/Y') : null) : ($data->date_registration_in ? $data->date_registration_in->format('d/m/Y') : null))) ;
                    $obj->status                = '2';
                }else  return response()->json(['message' => 'you dont have permission to update bc out from other factory'],422); 
            }else return response()->json(['message' => 'this kk is completed'],422); 
        }else
        {
            if($status == 1) // in
            {
                if(auth::user()->factory_id == $data->factory_id)
                {
                    $obj                        = new StdClass();
                    $obj->id                    = $data->id;
                    $obj->no_registration       = ($data->no_registration_in ? strtoupper($data->no_registration_in) : strtoupper($data->is_internal ? $data->no_registration : $data->no_registration_in));
                    $obj->no_aju                = ($data->no_aju_in ? strtoupper($data->no_aju_in) : strtoupper($data->is_internal ? $data->no_aju : $data->no_aju_in));
                    $obj->no_bc                 = ($data->no_bc_in ? strtoupper($data->no_bc_in) : strtoupper($data->is_internal ? $data->no_bc : $data->no_bc_in));
                    $obj->no_agreement          = strtoupper($data->no_agreement);
                    $obj->bc_type_id            = ($data->bc_type_in_id ? $data->bc_type_in_id : ($data->is_internal ? $data->bc_type_id : $data->bc_type_in_id)); 
                    $obj->date_registration     = ($data->date_registration_in ? $data->date_registration_in->format('d/m/Y') : ($data->is_internal ? ($data->date_registration ? $data->date_registration->format('d/m/Y') : null) : ($data->date_registration_in ? $data->date_registration_in->format('d/m/Y') : null)));
                    $obj->status                = '1';
                }else return response()->json(['message' => 'you dont have permission to update bc in from other factory'],422); 

            }else if ($status == 2) // out
            {
                $obj                        = new StdClass();
                $obj->id                    = $data->id;
                $obj->no_registration       = ($data->no_registration ? strtoupper($data->no_registration) : strtoupper($data->is_internal ? $data->no_registration : $data->no_registration_in));
                $obj->no_aju                = ($data->no_aju ? strtoupper($data->no_aju) : strtoupper($data->is_internal ? $data->no_aju : $data->no_aju_in));
                $obj->no_bc                 = ($data->no_bc ? strtoupper($data->no_bc) : strtoupper($data->is_internal ? $data->no_bc : $data->no_bc_in));
                $obj->no_agreement          = strtoupper($data->no_agreement);
                $obj->bc_type_id            = ($data->bc_type_id ? $data->bc_type_id : ($data->is_internal ? $data->bc_type_id : null)); 
                $obj->date_registration     = ($data->date_registration ? $data->date_registration->format('d/m/Y') : ($data->is_internal ? ($data->date_registration ? $data->date_registration->format('d/m/Y') : null) : ($data->date_registration_in ? $data->date_registration_in->format('d/m/Y') : null))) ;
                $obj->status                = '2';
            }else return response()->json(['message' => 'this kk is completed'],422); 
        }
       
        return response()->json($obj,200);
    }

    public function update(Request $request)
    {
        $app_env    = Config::get('app.env');
        $data       = RentalAgreement::find($request->id);

        if($data)
        {
            if($request->status == '1')
            {
                ReportOutstandingBCController::doUpdateBcIn($data,$request,'1');
                $parent     = $data->parent_rental_agreement_id;
                $array_parents    = array();
                while($parent != null)
                {
                    $rental_agreeement_parent = RentalAgreement::where('id',$parent)
                    ->whereNull('deleted_at')
                    ->first();
    
                    if($rental_agreeement_parent)
                    {
                        ReportOutstandingBCController::doUpdateBcIn($rental_agreeement_parent,$request);
                        $array_parents [] = $rental_agreeement_parent->id;
                        $parent = $rental_agreeement_parent->parent_rental_agreement_id;
                    }else $parent = null;
                }
                
                array_push($array_parents,$data->id);
                $childs = RentalAgreement::whereIn('parent_rental_agreement_id',$array_parents)
                ->whereNull('deleted_at')
                ->get();
    
                $array_childs = array();
                foreach ($childs as $key => $child) 
                {
                    $has_child = RentalAgreement::where('parent_rental_agreement_id',$child->id)
                    ->whereNull('deleted_at')
                    ->exists();

                    $parent_rental_agreement_id = $child->id;

                    ReportOutstandingBCController::doUpdateBcIn($child,$request,'1');
                    
                    while($has_child)
                    {
                        $child_agreeement_parents = RentalAgreement::where('parent_rental_agreement_id',$parent_rental_agreement_id)
                        ->whereNull('deleted_at')
                        ->get();

                        foreach ($child_agreeement_parents as $key => $child_agreeement_parent) 
                        {
                            if(!$child_agreeement_parent->deleted_at && !$child_agreeement_parent->return_date)
                            {
                                ReportOutstandingBCController::doUpdateBcIn($child_agreeement_parent,$request,'1');
                                $parent_rental_agreement_id = $child_agreeement_parent->id;
                                $has_child = RentalAgreement::where('parent_rental_agreement_id',$child_agreeement_parent->id)
                                ->whereNull('deleted_at')
                                ->exists();
                            }else $has_child = false;
                        }
                        
                    }
                }

                if($data->mapping_id)
                {
                    if($app_env == 'live') $source_no_kk = RentalAgreementLive::find($data->mapping_id);
                    else  $source_no_kk = RentalAgreementDev::find($data->mapping_id);

                    if($source_no_kk)
                    {
                        ReportOutstandingBCController::doUpdateBcIn($source_no_kk,$request,'2');
                        $source_parent          = $source_no_kk->parent_rental_agreement_id;
                        $array_source_parents   = array();

                        while($source_parent != null)
                        {
                            if($app_env == 'live') $source_rental_agreeement_parent = RentalAgreementLive::where('id',$source_parent)
                            ->whereNull('deleted_at')
                            ->first();
                            else  $source_rental_agreeement_parent = RentalAgreementDev::where('id',$source_parent)
                            ->whereNull('deleted_at')
                            ->first();

                            if($source_rental_agreeement_parent)
                            {
                                ReportOutstandingBCController::doUpdateBcIn($source_rental_agreeement_parent,$request,'2');
                                $array_source_parents [] = $source_rental_agreeement_parent->id;
                                $source_parent = $source_rental_agreeement_parent->parent_rental_agreement_id;
                            }else $source_parent = null;
                        }
                        
                        array_push($array_source_parents,$source_no_kk->id);

                        if($app_env == 'live')  $source_childs = RentalAgreementLive::whereIn('parent_rental_agreement_id',$array_source_parents)
                        ->whereNull('deleted_at')
                        ->get();
                        else   $source_childs = RentalAgreementDev::whereIn('parent_rental_agreement_id',$array_source_parents)
                        ->whereNull('deleted_at')
                        ->get();
            
                       
                        $array_source_childs = array();
                        foreach ($source_childs as $key => $source_child) 
                        {

                            if($app_env == 'live') $source_has_child = RentalAgreementLive::where('parent_rental_agreement_id',$source_child->id)
                            ->whereNull('deleted_at')
                            ->exists();
                            else  $source_has_child = RentalAgreementDev::where('parent_rental_agreement_id',$source_child->id)
                            ->whereNull('deleted_at')
                            ->exists();
                            
                            $source_parent_rental_agreement_id = $child->id;

                            ReportOutstandingBCController::doUpdateBcIn($source_child,$request,'2');
                            
                            while($source_has_child)
                            {
                                if($app_env == 'live') $source_child_agreeement_parents = RentalAgreementLive::where('parent_rental_agreement_id',$source_parent_rental_agreement_id)
                                ->whereNull('deleted_at')
                                ->get();
                                else  $source_child_agreeement_parents = RentalAgreementDev::where('parent_rental_agreement_id',$source_parent_rental_agreement_id)
                                ->whereNull('deleted_at')
                                ->get();

                                foreach ($source_child_agreeement_parents as $key => $source_child_agreeement_parent) 
                                {
                                    if(!$source_child_agreeement_parent->deleted_at && !$source_child_agreeement_parent->return_date)
                                    {
                                        ReportOutstandingBCController::doUpdateBcIn($source_child_agreeement_parent,$request,'2');
                                        $source_parent_rental_agreement_id = $source_child_agreeement_parent->id;

                                        if($app_env == 'live') $source_has_child = RentalAgreementLive::where('parent_rental_agreement_id',$source_child_agreeement_parent->id)
                                        ->whereNull('deleted_at')
                                        ->exists();
                                        else  $source_has_child = RentalAgreementDev::where('parent_rental_agreement_id',$source_child_agreeement_parent->id)
                                        ->whereNull('deleted_at')
                                        ->exists();

                                    }else $source_has_child = false;
                                }
                                
                            }
                        }
                    }
                   
                }


            }else if($request->status == '2')
            {
                ReportOutstandingBCController::doUpdateBcOut($data,$request,'1');
                $parent     = $data->parent_rental_agreement_id;
                $array_parents    = array();
                while($parent != null)
                {
                    $rental_agreeement_parent = RentalAgreement::where('id',$parent)
                    ->whereNull('deleted_at')
                    ->first();
    
                    if($rental_agreeement_parent)
                    {
                        ReportOutstandingBCController::doUpdateBcOut($rental_agreeement_parent,$request,'1');
                        $array_parents [] = $rental_agreeement_parent->id;
                        $parent = $rental_agreeement_parent->parent_rental_agreement_id;
                    }else $parent = null;
                }

                array_push($array_parents,$data->id);
                $childs = RentalAgreement::whereIn('parent_rental_agreement_id',$array_parents)
                ->whereNull('deleted_at')
                ->get();
    
                $array_childs = array();
                foreach ($childs as $key => $child) 
                {
                    if(!$child->deleted_at && !$child->return_date)
                    {
                        $has_child = RentalAgreement::where('parent_rental_agreement_id',$child->id)
                        ->whereNull('deleted_at')
                        ->exists();
    
                        $parent_rental_agreement_id = $child->id;
    
                        ReportOutstandingBCController::doUpdateBcOut($child,$request,'1');
                        
                        while($has_child)
                        {
                            $child_agreeement_parents = RentalAgreement::where('parent_rental_agreement_id',$parent_rental_agreement_id)
                            ->whereNull('deleted_at')
                            ->get();
    
                            foreach ($child_agreeement_parents as $key => $child_agreeement_parent) 
                            {
                                ReportOutstandingBCController::doUpdateBcOut($child_agreeement_parent,$request,'1');
                                $parent_rental_agreement_id = $child_agreeement_parent->id;
                                $has_child = RentalAgreement::where('parent_rental_agreement_id',$child_agreeement_parent->id)
                                ->whereNull('deleted_at')
                                ->exists();
                            }
                            
                        }
                    }
                }

                 if($data->mapping_id)
                {
                    if($app_env == 'live') $source_no_kk = RentalAgreementLive::find($data->mapping_id);
                    else  $source_no_kk = RentalAgreementDev::find($data->mapping_id);

                    if($source_no_kk)
                    {
                        ReportOutstandingBCController::doUpdateBcOut($source_no_kk,$request,'2');
                        $source_parent          = $source_no_kk->parent_rental_agreement_id;
                        $array_source_parents   = array();

                        while($source_parent != null)
                        {
                            if($app_env == 'live') $source_rental_agreeement_parent = RentalAgreementLive::where('id',$source_parent)
                            ->whereNull('deleted_at')
                            ->first();
                            else  $source_rental_agreeement_parent = RentalAgreementDev::where('id',$source_parent)
                            ->whereNull('deleted_at')
                            ->first();

                            if($source_rental_agreeement_parent)
                            {
                                ReportOutstandingBCController::doUpdateBcOut($source_rental_agreeement_parent,$request,'2');
                                $array_source_parents [] = $source_rental_agreeement_parent->id;
                                $source_parent = $source_rental_agreeement_parent->parent_rental_agreement_id;
                            }else $source_parent = null;
                        }
                        
                        array_push($array_source_parents,$source_no_kk->id);

                        if($app_env == 'live')  $source_childs = RentalAgreementLive::whereIn('parent_rental_agreement_id',$array_source_parents)
                        ->whereNull('deleted_at')
                        ->get();
                        else   $source_childs = RentalAgreementDev::whereIn('parent_rental_agreement_id',$array_source_parents)
                        ->whereNull('deleted_at')
                        ->get();
            
                       
                        $array_source_childs = array();
                        foreach ($source_childs as $key => $source_child) 
                        {

                            if($app_env == 'live') $source_has_child = RentalAgreementLive::where('parent_rental_agreement_id',$source_child->id)
                            ->whereNull('deleted_at')
                            ->exists();
                            else  $source_has_child = RentalAgreementDev::where('parent_rental_agreement_id',$source_child->id)
                            ->whereNull('deleted_at')
                            ->exists();
                            
                            $source_parent_rental_agreement_id = $child->id;

                            ReportOutstandingBCController::doUpdateBcOut($source_child,$request,'2');
                            
                            while($source_has_child)
                            {
                                if($app_env == 'live') $source_child_agreeement_parents = RentalAgreementLive::where('parent_rental_agreement_id',$source_parent_rental_agreement_id)
                                ->whereNull('deleted_at')
                                ->get();
                                else  $source_child_agreeement_parents = RentalAgreementDev::where('parent_rental_agreement_id',$source_parent_rental_agreement_id)
                                ->whereNull('deleted_at')
                                ->get();

                                foreach ($source_child_agreeement_parents as $key => $source_child_agreeement_parent) 
                                {
                                    if(!$source_child_agreeement_parent->deleted_at && !$source_child_agreeement_parent->return_date)
                                    {
                                        ReportOutstandingBCController::doUpdateBcOut($source_child_agreeement_parent,$request,'2');
                                        $source_parent_rental_agreement_id = $source_child_agreeement_parent->id;

                                        if($app_env == 'live') $source_has_child = RentalAgreementLive::where('parent_rental_agreement_id',$source_child_agreeement_parent->id)
                                        ->whereNull('deleted_at')
                                        ->exists();
                                        else  $source_has_child = RentalAgreementDev::where('parent_rental_agreement_id',$source_child_agreeement_parent->id)
                                        ->whereNull('deleted_at')
                                        ->exists();

                                    }else $source_has_child = false;
                                }
                                
                            }
                        }
                    }
                   
                }

            }
            
           
        }

        return response()->json(200);
    }
    
    public function import()
    {
        $factories          = Factory::where('company_id',auth::user()->company_id)->whereNull('delete_at')->pluck('name', 'id')->all();
        $factory_id         = auth::user()->factory_id;
       
        return view('report_outstanding_bc.import',compact('factories','factory_id'));

    }

    public function downloadFormImport(Request $request)
    {
        $factory_id = $request->has('factory') ? $request->factory : auth::user()->factory_id;
        $data       = $data = DB::table('rental_agreement_v')
        ->where('factory_id','LIKE',"%$factory_id%")
        ->whereNull('no_bc')
        ->orderby('no_agreement','asc')
        ->get();

        return Excel::create('outstanding_bc',function ($excel) use($data) 
        {

            $excel->sheet('active', function($sheet) use($data) 
            {
                $sheet->setCellValue('A1','#');
                $sheet->setCellValue('B1','id');
                $sheet->setCellValue('C1','DEPARTMENT');
                $sheet->setCellValue('D1','NO_KK');
                $sheet->setCellValue('E1','NO_REGISTRATION');
                $sheet->setCellValue('F1','DATE_REGISTRATION');
                $sheet->setCellValue('G1','NO_AJU');
                $sheet->setCellValue('H1','NO_BC');
                $sheet->setCellValue('I1','TYPE_BC');

                $no     = 1;
                $row    = 2;
                foreach ($data as $i)
                {
                    $sheet->setCellValue('A'.$row,$no);
                    $sheet->setCellValue('B'.$row,$i->id);
                    $sheet->setCellValue('C'.$row,$i->department_name);
                    $sheet->setCellValue('D'.$row,$i->no_agreement);
                    $sheet->setCellValue('E'.$row,$i->no_registration);
                    $sheet->setCellValue('F'.$row,$i->date_registration);
                    $sheet->setCellValue('G'.$row,$i->no_aju);
                    $sheet->setCellValue('H'.$row,$i->no_bc);
                    $sheet->setCellValue('I'.$row,$i->bc_type);
                    $no++;
                    $row++;
                }
            });

            $excel->setActiveSheetIndex(0);
        })
        ->export('xlsx');
    }

    public function uploadFormImport(Request $request)
    {
        $array              = array();
        
        if($request->hasFile('upload_file'))
        {
            $validator = Validator::make($request->all(), [
                'upload_file' => 'required|mimes:xls'
            ]);

            $path           = $request->file('upload_file')->getRealPath();
            $data           = Excel::selectSheets('active')->load($path,function($render){})->get();

            //return response()->json($data);
            $movement_date  = $request->current_time;

            if(!empty($data) && $data->count())
            {
                try 
                {
                    DB::beginTransaction();
                    foreach ($data as $key => $value) 
                    {
                        $id     = $value->id;
                        $no_kk  = strtolower(trim($value->no_kk));
                        $no_bc  = strtolower(trim($value->no_bc));
                        
                        if($no_bc)
                        {
                            $data = RentalAgreement::find($id);

                            if($data)
                            {
                                $data->no_bc        = $no_bc;
                                $data->updated_at   = $movement_date;
                                $data->save();

                                $obj                = new stdClass();
                                $obj->no_kk         = strtoupper($no_kk);
                                $obj->no_bc         = strtoupper($no_bc);
                                $obj->is_error      = false;
                                $obj->system_log    = null;
                                $array []           = $obj;
                            }else
                            {
                                $obj                = new stdClass();
                                $obj->no_kk         = strtoupper($no_kk);
                                $obj->no_bc         = strtoupper($no_bc);
                                $obj->is_error      = true;
                                $obj->system_log    = 'ID NOT FOUND';
                                $array []           = $obj;
                            }
                        }else
                        {
                            $obj                = new stdClass();
                            $obj->no_kk         = strtoupper($no_kk);
                            $obj->no_bc         = strtoupper($no_bc);
                            $obj->is_error      = true;
                            $obj->system_log    = 'No BC is required';
                            $array []           = $obj;
                        }

                    }  
                    DB::commit();   
                } catch (Exception $e) 
                {
                    DB::rollBack();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }

                return response()->json($array,200);
            }else
            {
                return response()->json(['message' => 'import failed, please check again your file'],422);
            }


        }
    }

    public function downloadReport(Request $request)
    {
        $data       = $data = DB::table('rental_agreement_v')
        ->orderby('no_agreement','asc')
        ->get();

        return Excel::create('report_no_agreement_bc',function ($excel) use($data) 
        {

            $excel->sheet('active', function($sheet) use($data) 
            {
                $sheet->setCellValue('A1','#');
                $sheet->setCellValue('B1','FACTORY');
                $sheet->setCellValue('C1','DEPARTMENT');
                $sheet->setCellValue('D1','NO_KK');
                $sheet->setCellValue('E1','NO_REGISTRATION');
                $sheet->setCellValue('F1','DATE_REGISTRATION');
                $sheet->setCellValue('G1','NO_AJU');
                $sheet->setCellValue('H1','NO_BC');
                $sheet->setCellValue('I1','BC_TYPE');
                $sheet->setCellValue('J1','TYPE_AGREEMENT');
                $sheet->setCellValue('K1','START_AGREEMENT');
                $sheet->setCellValue('L1','END_AGREEMENT');

                $no     = 1;
                $row    = 2;
                foreach ($data as $i)
                {
                    $sheet->setCellValue('A'.$row,$no);
                    $sheet->setCellValue('B'.$row,$i->factory_name);
                    $sheet->setCellValue('C'.$row,$i->department_name);
                    $sheet->setCellValue('D'.$row,$i->no_agreement);
                    $sheet->setCellValue('E'.$row,$i->no_registration);
                    $sheet->setCellValue('F'.$row,$i->date_registration);
                    $sheet->setCellValue('G'.$row,$i->no_aju);
                    $sheet->setCellValue('H'.$row,$i->no_bc);
                    $sheet->setCellValue('I'.$row,$i->bc_type);
                    $sheet->setCellValue('J'.$row,($i->is_agreement_for_rental ? 'KK_RENT' : 'KK_HANDOVER'));
                    $sheet->setCellValue('K'.$row,($i->is_agreement_for_rental ? $i->start_date : null));
                    $sheet->setCellValue('L'.$row,($i->is_agreement_for_rental ? $i->end_date : null));
                    $no++;
                    $row++;
                }
            });

            $excel->setActiveSheetIndex(0);
        })
        ->export('xlsx');
    }

    static function doUpdateBcIn($data,$request,$flag)
    {
        $app_env    = Config::get('app.env');

        if($flag == '2')
        {
            $bbi_bc_type = BcType::find($request->bc_type);
            if($app_env == 'live')
            {
                $bc_type = BcTypeLive::where('code',strtolower($bbi_bc_type->code))->first();
                if($bc_type)
                {
                    if(!$bc_type->mapping_id)
                    {
                        $bc_type->update([
                            'mapping_id' => $bbi_bc_type->id
                        ]);
                    }

                    $bc_type_in_id = $bc_type->id;
                }else response()->json(['message' => 'Bc type '.$bbi_bc_type->code.' not found di this db'],422);
                
                $new_user = UserLive::where('nik','bbi-'.auth::user()->nik)->first();
                if(!$new_user)
                {
                    $new_user = UserLive::firstOrCreate([
                        'nik' => 'bbi-'.auth::user()->nik,
                        'name' => auth::user()->name,
                        'sex'   => 'unknown',
                        'email'   => 'unknown',
                        'password'   => bcrypt('unknown'),
                    ]);
                }

                $updated_user_exim_in_id    = $new_user->id;
            }else if($app_env == 'dev')
            {
                $bc_type = BcTypeDev::where('code',strtolower($bbi_bc_type->code))->first();
                if($bc_type)
                {
                    if(!$bc_type->mapping_id)
                    {
                        $bc_type->update([
                            'mapping_id' => $bbi_bc_type->id
                        ]);
                    }

                    $bc_type_in_id = $bc_type->id;
                }else response()->json(['message' => 'Bc type '.$bbi_bc_type->code.' not found di this db'],422);
                
                $new_user = UserDev::where('nik','bbi-'.auth::user()->nik)->first();
                if(!$new_user)
                {
                    $new_user = UserDev::firstOrCreate([
                        'nik' => 'bbi-'.auth::user()->nik,
                        'name' => auth::user()->name,
                        'sex'   => 'unknown',
                        'email'   => 'unknown',
                        'password'   => bcrypt('unknown'),
                    ]);
                }

                $updated_user_exim_in_id    = $new_user->id;
            }

            $receive_date = carbon::now();
        }else
        {
            $bc_type_in_id              = $request->bc_type;
            $updated_user_exim_in_id    = auth::user()->id;
            $receive_date               = null;
        } 

        //echo $flag.' '.$bc_type_in_id.'</br>';
        $data->no_aju_in                    = strtolower(trim($request->no_aju));
        $data->no_registration_in           = strtolower(trim($request->no_registration));
        $data->date_registration_in         = Carbon::createFromFormat('d/m/Y', $request->date_registration)->format('Y-m-d');
        $data->bc_type_in_id                = $bc_type_in_id;
        $data->no_bc_in                     = strtolower(trim($request->no_bc));
        $data->updated_bc_in_date           = $request->current_time ? $request->current_time : carbon::now();
        $data->updated_at                   = $request->current_time ? $request->current_time : carbon::now();
        $data->updated_user_exim_in_id      = $updated_user_exim_in_id;
        $data->receive_date                 = $receive_date;
        $data->save();

        if((strtolower($data->description) == 'no agremeent for dikembalikan' 
        && $data->is_internal == true) || strtolower($data->description) == 'no agremeent for dipindah tangan' )
        {
            Asset::whereNotNull('no_rent')
            ->where('no_rent',$data->id)
            ->update([
                'no_rent' => null
            ]);
        }
    }

    static function doUpdateBcOut($data,$request,$flag)
    {
        $app_env    = Config::get('app.env');
        if($flag == '2')
        {
            $bbi_bc_type = BcType::find($request->bc_type);
            if($app_env == 'live')
            {
                $bc_type = BcTypeLive::where('code',strtolower($bbi_bc_type->code))->first();
                if($bc_type)
                {
                    if(!$bc_type->mapping_id)
                    {
                        $bc_type->update([
                            'mapping_id' => $bbi_bc_type->id
                        ]);
                    }

                    $bc_type_id = $bc_type->id;
                }else response()->json(['message' => 'Bc type '.$bbi_bc_type->code.' not found di this db'],422);
                
                $new_user = UserLive::where('nik','bbi-'.auth::user()->nik)->first();
                if(!$new_user)
                {
                    $new_user = UserLive::firstOrCreate([
                        'nik' => 'bbi-'.auth::user()->nik,
                        'name' => auth::user()->name,
                        'sex'   => 'unknown',
                        'email'   => 'unknown',
                        'password'   => bcrypt('unknown'),
                    ]);
                }

                $updated_user_exim_id    = $new_user->id;
            }else if($app_env == 'dev')
            {
                $bc_type = BcTypeDev::where('code',strtolower($bbi_bc_type->code))->first();
                if($bc_type)
                {
                    if(!$bc_type->mapping_id)
                    {
                        $bc_type->update([
                            'mapping_id' => $bbi_bc_type->id
                        ]);
                    }

                    $bc_type_id = $bc_type->id;
                }else response()->json(['message' => 'Bc type '.$bbi_bc_type->code.' not found di this db'],422);
                
                $new_user = UserDev::where('nik','bbi-'.auth::user()->nik)->first();
                if(!$new_user)
                {
                    $new_user = UserDev::firstOrCreate([
                        'nik' => 'bbi-'.auth::user()->nik,
                        'name' => auth::user()->name,
                        'sex'   => 'unknown',
                        'email'   => 'unknown',
                        'password'   => bcrypt('unknown'),
                    ]);
                }

                $updated_user_exim_id    = $new_user->id;
            }
        }else
        {
            $bc_type_id                 = $request->bc_type;
            $updated_user_exim_id       = auth::user()->id;
        } 

        $data->no_aju               = strtolower(trim($request->no_aju));
        $data->no_registration      = strtolower(trim($request->no_registration));
        $data->date_registration    = Carbon::createFromFormat('d/m/Y', $request->date_registration)->format('Y-m-d');
        $data->bc_type_id           = $bc_type_id;
        $data->no_bc                = strtolower(trim($request->no_bc));
        $data->updated_bc_date      = $request->current_time ? $request->current_time : carbon::now();
        $data->updated_at           = $request->current_time ? $request->current_time : carbon::now();
        $data->updated_user_exim_id = $updated_user_exim_id;
        $data->save();
    }
}
