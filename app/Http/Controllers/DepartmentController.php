<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Config;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\Company;
use App\Models\Factory;
use App\Models\Department;

use App\Models\Erp\AssetGroupDev;
use App\Models\Erp\AssetGroupLive;
use App\Models\Erp\DepartmentDev;
use App\Models\Erp\DepartmentLive;

use App\Models\Absensi\DepartmentAoi;
use App\Models\Absensi\DepartmentBbi;

class DepartmentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request)
    {
        $msg = $request->session()->get('message');
        return view('department.index',compact('msg'));
    }

    public function data()
    {
    	if(request()->ajax())
        {
            $company = Company::find(auth::user()->company_id);

            if($company->code == 'aoi') $data = DepartmentAoi::orderby('department_id','asc');
            else $data = DepartmentBbi::orderby('department_id','asc');
           
            return datatables()->of($data)
            ->addColumn('code',function($data)
            {
                $department = Department::where('department_absensi_id',$data->department_id)->first();
            	return ($department ? $department->code : null);
            })
            ->addColumn('erp_oprational_cost_type',function($data)
            {
                $department = Department::where('department_absensi_id',$data->department_id)->first();
            	return ($department ? ucwords($department->erp_oprational_cost_type) : null);
            })
            ->addColumn('erp_account_locator_name',function($data)
            {
                $department = Department::where('department_absensi_id',$data->department_id)->first();
            	return ($department ? $department->erp_account_locator_name : null);
            })
            ->addColumn('department_erp_id',function($data)
            {
                $department = Department::where('department_absensi_id',$data->department_id)->first();
            	return ($department ? $department->department_erp_id : null);
            })
            ->editColumn('department_name',function($data){
                return ucwords(strtolower($data->department_name));
            })
            ->addColumn('action', function($data) {
                return view('department._action', [
                    'model' => $data,
                    'edit' => route('department.edit',$data->department_id)
                ]);
            })
            ->rawColumns(['action'])
            ->make(true);
        }
    }

    public function edit(Request $request,$id)
    {
        if($request->session()->has('message')) $request->session()->forget('message');
        $company = Company::find(auth::user()->company_id);

        if($company->code == 'aoi') $department_absensi = DepartmentAoi::where('department_id',$id)->first();
        else $department_absensi = DepartmentBbi::where('department_id',$id)->first();
        
        $app_env = Config::get('app.env');

        if($app_env == 'live')
        {
            $oprational_cost_types_erp  = AssetGroupLive::select('beban')->whereNotNull('beban')->groupby('beban')->pluck('beban','beban')->all();
            $departments_erp            = DepartmentLive::pluck('department_name','c_activity_id')->all();
        }else
        {
            $oprational_cost_types_erp  = AssetGroupDev::select('beban')->whereNotNull('beban')->groupby('beban')->pluck('beban','beban')->all();
            $departments_erp            = DepartmentDev::pluck('department_name','c_activity_id')->all();
        }
        $department                 = Department::where('department_absensi_id',$id)->first();
       
        return view('department.edit',compact('department','department_absensi','id','departments_erp','oprational_cost_types_erp'));
    }

    public function store(Request $request)
    {
        if($request->session()->has('message')) $request->session()->forget('message');
        if (Department::where('department_absensi_id',$request->department_absensi_id)
        ->whereNull('delete_at')
        ->exists())
        {
    		return response()->json(['message'=>'Kode departemen sudah ada, silahkan cari Kode departemen lain'],422);
        }

        try 
        {
            DB::beginTransaction();
            
            Department::FirstOrCreate([
                'name'                      => strtolower(trim($request->name)),
                'erp_oprational_cost_type'  => strtolower(trim($request->oprational_cost_types_erp)),
                'department_absensi_id'     => $request->department_absensi_id,
                'department_erp_id'         => $request->department_erp,
            ]);

            $request->session()->flash('message', 'success_2');
            DB::commit();
            
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }

        return response()->json('success', 200);
    }

    public function update(Request $request,$id)
    {
        if($request->session()->has('message')) $request->session()->forget('message');
        if (Department::where([
            ['id','!=',$id],
            ['department_absensi_id',$request->department_absensi_id],
        ])
        ->whereNull('delete_at')
        ->exists())
        {
    		return response()->json(['message'=>'Kode departemen sudah ada, silahkan cari Kode departemen lain'],422);
        }

        try 
        {
            DB::beginTransaction();
            
            $department                             = Department::find($id);
            $department->name                       = strtolower(trim($request->name));
            $department->department_erp_id          = $request->department_erp;
            $department->erp_oprational_cost_type   = strtolower(trim($request->oprational_cost_types_erp));
            $department->save();

            $request->session()->flash('message', 'success_2');
            DB::commit();
            
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }

        return response()->json('success', 200);

        
    }
}
