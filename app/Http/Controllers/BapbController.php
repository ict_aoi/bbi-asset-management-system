<?php namespace App\Http\Controllers;
use DB;
use Auth;
use View;
use Excel;
use StdClass;
use Validator;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\Asset;
use App\Models\Factory;
use App\Models\Company;
use App\Models\Temporary;
use App\Models\Assettype;
use App\Models\AssetMovement;
use App\Models\RentalAgreement;
use App\Models\SubDepartment;

class BapbController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $factories      = Factory::whereIn('code',['bbis'])
        ->whereNull('delete_at')
        ->orderBy('code','asc')
        ->pluck('name', 'id')
        ->all();

        $sub_departments    = SubDepartment::whereIn('code',['ict','elk','ga','me'])
        ->whereNull('delete_at')
        ->orderBy('code','asc')
        ->pluck('name', 'id')
        ->all();
        

        Temporary::where([
            ['column_1','cart bapb'],
            ['column_3',auth::user()->id],
        ])
        ->delete();

        return view('bapb.index',compact('factories','sub_departments'));
    }
    
    public function data(Request $request)
    {
        if(request()->ajax()) 
        {
            $factory_id         = $request->factory;
            $sub_department_id  = $request->subdepartment;
            $asset_type_id      = $request->asset_type;
            
            $data = DB::table('asset_v')
            ->where('status','obsolete')
            ->where(function($query) use($factory_id)
            {
                $query = $query->Where('origin_factory_location_id','LIKE',"%$factory_id%");
            })
            ->where(function($query) use($sub_department_id)
            {
                $query = $query->Where('origin_sub_department_location_id','LIKE',"%$sub_department_id%");
            })
            ->where(function($query) use($asset_type_id)
            {
                $query = $query->Where('asset_type_id','LIKE',"%$asset_type_id%");
            });

            return datatables()->of($data)
            ->addColumn('action', function($data) 
            {
                $is_exists = Temporary::where([
                    ['column_1','cart bapb'],
                    ['column_2',$data->id],
                ])
                ->first();

                if($is_exists)
                {
                    if($is_exists->column_3 == auth::user()->id)
                    {
                        return view('bapb._action', [
                            'model'     => $data,
                            'remove'    => $data->id,
                        ]);
                    }else return null;
                }else
                {
                    return view('bapb._action', [
                        'model'     => $data,
                        'cancel'    => route('bapb.cancelObsolete',$data->id),
                        'approve'   => $data->id,
                    ]);
                }
               
            })
            ->editColumn('last_company_name',function($data){
            	return ucwords($data->last_company_name);
            })
            ->editColumn('last_factory_name',function($data){
            	return ucwords($data->last_factory_name);
            })
            ->editColumn('last_department_name',function($data){
            	return ucwords($data->last_department_name);
            })
            ->editColumn('no_inventory_manual',function($data){
            	return strtoupper($data->no_inventory_manual);
            })
            ->editColumn('origin_factory_name',function($data){
            	return ucwords($data->origin_factory_name);
            })
            ->editColumn('origin_department_name',function($data){
            	return ucwords($data->origin_department_name);
            })
            ->editColumn('last_factory_name',function($data){
            	return ucwords($data->last_factory_name);
            })
            ->editColumn('model',function($data){
            	return ucwords($data->model);
            })
            ->editColumn('asset_type',function($data){
            	return ucwords($data->asset_type);
            })
            ->setRowAttr([
                'style' => function($data)
                {
                    $is_exists = Temporary::where([
                        ['column_1','cart bapb'],
                        ['column_2',$data->id],
                    ])
                    ->exists();

                    if($is_exists) return  'background-color: #d9ffde;';
                   
                },
            ])
            ->rawColumns(['action'])
            ->make(true);

           

        }
    }

    public function cancelObsolete(Request $request,$id)
    {
        $current_time   = ($request->current_time ? $request->current_time: carbon::now()->toDateTimeString() );
        $asset          = Asset::find($id);

        if($asset)
        {
            $asset_movement = AssetMovement::find($asset->last_asset_movement_id);

            if($asset_movement)
            {
                $asset_movement->cancel_obsolete_date       = $current_time;
                $asset_movement->cancel_obsolete_user_id    = auth::user()->id;
                $asset_movement->cancel_obsolete_note       = trim(strtolower($request->cancel_reason));
                $asset_movement->save();
    
                $new_asset_movement = AssetMovement::firstorCreate([
                    'asset_id'                          => $asset_movement->asset_id,
                    'status'                            => 'siap digunakan',
                    'from_company_location'             => $asset_movement->asset->origin_company_location_id,
                    'from_factory_location'             => $asset_movement->asset->origin_factory_location_id,
                    'from_department_location'          => $asset_movement->asset->origin_department_location_id,
                    'from_sub_department_location'      => $asset_movement->asset->origin_sub_department_location_id,
                    'from_employee_nik'                 => null,
                    'from_employee_name'                => null,
                    'to_company_location'               => $asset_movement->asset->origin_company_location_id,
                    'to_factory_location'               => $asset_movement->asset->origin_factory_location_id,
                    'to_department_location'            => $asset_movement->asset->origin_department_location_id,
                    'to_sub_department_location'        => $asset_movement->asset->origin_sub_department_location_id,
                    'to_employee_nik'                   => null,
                    'to_employee_name'                  => null,
                    'movement_date'                     => $current_time,
                    'note'                              => null,
                    'is_handover_asset'                 => false,
                    'rental_agreement_id'               => null,
                    'note'                              => 'movement obsolete sebelum nya di batalkan',
                ]);
    
                $asset->last_asset_movement_id          = $new_asset_movement->id;
                $asset->status                          = 'siap digunakan';
                $asset->last_used_by                    = null;
                $asset->last_movement_date              = $current_time;
                $asset->last_area_location              = null;
                $asset->last_user_movement_id           = auth::user()->id;
                $asset->no_rent                         = null;
                $asset->save();
            }

        }
    }

    public function addToCart(Request $request)
    {
        $id     = $request->id;
        $status = $request->status;

        if($status == 'add')
        {
            $is_exists = Temporary::where([
                ['column_1','cart bapb'],
                ['column_2',$id],
            ])
            ->exists();

            if($is_exists) return response()->json(['message' => 'Asset already selected'], 422);

            Temporary::Create([
                'column_1' => 'cart bapb',
                'column_2' => $id,
                'column_3' => auth::user()->id,
            ]);

            $asset = Asset::find($id);

            $data                           = new stdClass();
            $data->id                       = $asset->id;
            $data->barcode                  = $asset->barcode;
            $data->no_inventory             = $asset->no_inventory;
            $data->model                    = $asset->model;
            $data->serial_number            = $asset->serial_number;
            $data->last_factory_name        = $asset->lastFactory->name;
            $data->last_sub_department_name = $asset->lastSubDepartment->name;
            $data->last_factory_id          = $asset->last_factory_location_id;
            $data->last_sub_department_id   = $asset->last_sub_department_location_id;

            return response()->json($data, 200);


        }else if ($status == 'remove')
        {
            Temporary::where([
                ['column_1','cart bapb'],
                ['column_2',$id],
                ['column_3',auth::user()->id],
            ])
            ->delete();
        }
    }
    
    public function store(Request $request)
    {
        $current_time       = ($request->current_time ? $request->current_time: carbon::now()->toDateTimeString() );
        $factory_id         = $request->bapb_factory;
        $sub_department_id  = $request->bapb_subdepartment;
        $status             = $request->status;
        $buyer_name         = ($request->buyer_name ? strtolower(trim($request->buyer_name)) : null);
        $new_no_ba          = ($request->new_no_ba ? strtolower(trim($request->new_no_ba)) : null);
        $cost_of_goods      = ($request->cost_of_goods ? strtolower(trim($request->cost_of_goods)) : 0);
        $list_bapbs         = json_decode($request->list_selected_data);

        try
        {
            DB::beginTransaction();

            $new_rental = RentalAgreement::FirstOrCreate([
                'factory_id'                    => $factory_id,
                'destination_factory_id'        => $factory_id,
                'sub_department_id'             => $sub_department_id,
                'no_agreement'                  => $new_no_ba,
                'price'                         => 0,
                'start_date'                    => carbon::now(),
                'end_date'                      => carbon::now(),
                'duration_in_day'               => 0,
                'description'                   => 'no agremeent for obsolete',
                'created_at'                    => $current_time,
                'updated_at'                    => $current_time,
                'created_user_id'               => auth::user()->id,
                'updated_user_id'               => auth::user()->id,
                'deleted_user_id'               => null,
                'is_internal'                   => true,
                'destination_other'             => null,
                'is_agreement_for_rental'       => false,
                'status_obsolete'               => $status,
                'buyer_name'                    => $buyer_name,
                'cost_of_goods'                 => $cost_of_goods,
                'accounting_confirmation_date'  => $current_time,
                'accounting_user_id'            => auth::user()->id,
            ]);
            
            if($new_rental)
            {
                foreach ($list_bapbs as $key => $list_bapb) 
                {
                    if($list_bapb->last_factory_id != $factory_id) return response()->json(['message' => 'Factory not same'], 422);
                    if($list_bapb->last_sub_department_id != $sub_department_id) return response()->json(['message' => 'Subdepartment not same'], 422);

                    $asset = Asset::find($list_bapb->id);

                    if($asset)
                    {
                        if($asset->status != 'bapb')
                        {
                            $asset_movement = AssetMovement::firstorcreate([
                                'asset_id'                      => $asset->id,
                                'status'                        => 'bapb',
                                'from_company_location'         => $asset->last_company_location_id,
                                'from_factory_location'         => $asset->last_factory_location_id,
                                'from_department_location'      => $asset->last_department_location_id,
                                'from_sub_department_location'  => $asset->last_sub_department_location_id,
                                'from_employee_nik'             => auth::user()->nik,
                                'from_employee_name'            => auth::user()->name,
                                'to_company_location'           => $asset->last_company_location_id,
                                'to_factory_location'           => $asset->last_factory_location_id,
                                'to_department_location'        => $asset->last_department_location_id,
                                'to_sub_department_location'    => $asset->last_sub_department_location_id,
                                'to_area_location'              => 'bapb',
                                'movement_date'                 => $current_time,
                                'rental_agreement_id'           => $new_rental->id,
                                'note'                          => ($status == 'dijual' ? 'proses bapb disetujuan oleh '.auth::user()->name.', pada waktu '.$current_time.'dengan status asset dijual ke '.$buyer_name.', dengan harga '.$cost_of_goods : 'proses bapb disetujuan oleh '.auth::user()->name.', pada waktu '.$current_time.'dengan status asset dimusnahkan (scrap)'),
                            ]);
        
                            $asset->status                   = 'bapb';
                            $asset->last_used_by             = null;
                            $asset->last_movement_date       = $current_time;
                            $asset->last_area_location       = null;
                            $asset->last_asset_movement_id   = $asset_movement->id;
                            $asset->last_user_movement_id    = auth::user()->id;
                            $asset->delete_at                = $current_time;
                            $asset->delete_user_id           = Auth::user()->id;
                            $asset->save();
                        }
                    }
                   
                }
            }

            DB::commit();
            return response()->json('success',200);

        } catch (Exception $e)
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    public function getAssetType(Request $request)
    {
        $sub_department_id  = $request->sub_department_id;
        $factory_id         = $request->factory_id;
        $master_factory     = Factory::find($factory_id);

        $asset_types = AssetType::whereNull('delete_at')
        ->where([
            ['sub_department_id',$sub_department_id],
            ['company_id',($master_factory ? $master_factory->company_id : null)],
            ['is_consumable',false],
        ])
        ->pluck('name','id')
        ->all();
        
        return response()->json(['asset_types' => $asset_types]);

    }
}
