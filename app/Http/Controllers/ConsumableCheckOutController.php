<?php namespace App\Http\Controllers;

use DB;
use Auth;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Models\Asset; 
use App\Models\Company;
use App\Models\Factory;
use App\Models\AssetType;
use App\Models\SubDepartment;
use App\Models\ConsumableAsset;
use App\Models\ConsumableAssetDetail;
use App\Models\AssetInformationField;
use App\Models\ConsumableAssetMovement;

use App\Models\Absensi\AbsensiBbi;

class ConsumableCheckOutController extends Controller
{
    public function index(Request $request,$id)
    {
        
        $consumable_asset = ConsumableAsset::find($id);

        $consumable_asset_detail = ($consumable_asset->consumableAssetDetail()->exists()=='true'?1:0);
        
        $asset_custom_fields    = AssetInformationField::where([
            ['asset_type_id', $consumable_asset->asset_type_id],
        ])
        ->whereNull('delete_at')
        ->orderby('created_at','asc')
        ->get();

        $array = array();
        foreach ($asset_custom_fields as $key => $value) 
        {
            $column_name        = $value->name;
            $curr_value         = ($consumable_asset)? $consumable_asset->$column_name:null;

            if($column_name == 'asset_type_id') $curr_value                 = $consumable_asset->assetType->name;
            if($column_name == 'company_id') $curr_value                    = $consumable_asset->company->name;
            if($column_name == 'manufacture_id') $curr_value                = ($consumable_asset->manufacture_id)?$consumable_asset->manufacture->name : null;
            
            if($consumable_asset->assetType->is_consumable == false)
            {
                if($column_name == 'total') $curr_value = 1;
            }
            
            $obj                = new stdClass();
            $obj->column_name   = $column_name;
            $obj->label         = ucwords(str_replace('_',' ',$value->label));
            $obj->value         = ucwords($curr_value);
            $array []           = $obj;
        }
        
        return view('consumable.checkout',compact('consumable_asset','array','consumable_asset_detail'));
    }

    public function store(Request $request,$id)
    {
        $this->validate($request, [
            'company_id'        => 'required',
            'sub_department'    => 'required',
        ]);

        try
        {
            DB::beginTransaction();
            
            $checkout_date          = ($request->checkout_date ? Carbon::createFromFormat('d/m/Y', $request->checkout_date)->format('Y-m-d H:i:s') : carbon::now()->toDateTimeString());
            $detail_id              = ($request->detail_id=='' ? null:$request->detail_id);
            $consumable_asset_id    = $request->consumable_asset_id;
            $consumable_asset       = ConsumableAsset::find($consumable_asset_id);
            $from_factory           = Factory::where('company_id'  ,$consumable_asset->company_id)->first();

            if($request->checkout_to_status == 1)// checkout ke user
            {
                $asset_id              = null;
                $sub_department         = SubDepartment::where('is_other',false)
                ->where('name',strtolower(trim($request->employee_sub_department)))
                ->first();
                
                if(!$sub_department)
                {
                    return response()->json(['message' => 'Sub Department is not exists yet, please do sync first'], 422);
                }
                
                $to_company_location    = $consumable_asset->company_id;
                $to_factory_location    = $from_factory->id;
                $to_department_location = $sub_department->id;
                $to_employee_nik        = strtolower($request->employee_nik);
                $to_employee_name       = strtolower($request->employee_name);

            }else if($request->checkout_to_status == 2)// checkout ke asset
            {
                $asset                  = Asset::find($request->asset_id);
                $asset_id               = $asset->id;
                
                $to_company_location    = null; //belum jadi
                $to_factory_location    = null; //belum jadi
                $to_department_location = null; //belum jadi
                $to_employee_nik        = null;
                $to_employee_name       = null;
                
            }

           ConsumableAssetMovement::firstorcreate([
                'consumable_asset_id'        => $consumable_asset_id,
                'asset_id'                   => $asset_id,
                'status'                     => 'digunakan',
                'consumable_asset_detail_id' => $detail_id,
                'from_company_location'      => $consumable_asset->company_id,
                'from_factory_location'      => $from_factory->id,
                'from_department_location'   => $consumable_asset->sub_department_id,
                'from_employee_nik'          => auth::user()->nik,
                'from_employee_name'         => auth::user()->name,
                'to_company_location'        => $to_company_location,
                'to_factory_location'        => $to_factory_location,
                'to_department_location'     => $to_department_location,
                'to_employee_nik'            => $to_employee_nik,
                'to_employee_name'           => $to_employee_name,
                'movement_date'              => $checkout_date
            ]);

            $qty_used                        = $consumable_asset->qty_used;
            $qty_available                   = $consumable_asset->qty_available;
            $consumable_asset->qty_used      = $qty_used+1;
            $consumable_asset->qty_available = $qty_available-1;
            

            $consumable_asset->save();
            
            DB::commit();
            return response()->json('success',200);

        } catch (Exception $e)
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    public function getAbsence(Request $request)
    {
        $absences = AbsensiBbi::select('nik','name','department_name','subdept_name','factory')->get();
            
        $getAbsen = [];
        foreach ($absences as $key => $value) {
            $getAbsen [] = $value->nik.':'.$value->name.':'.$value->subdept_name.':'.$value->factory;
        }
        
        return response()->json($getAbsen,200);
        
    }

    public function getAsset(Request $request)
    {
        $company_id             = $request->company_id;
        $sub_department_id      = $request->sub_department_id;
        $barcode                = $request->barcode;

        $assets                  = Asset::where([
            ['barcode',$barcode],
            ['company_id',$company_id],
            ['origin_sub_department_location_id',$sub_department_id],
        ])
        
        ->whereNull('delete_at')
        ->first();

        return response()->json([
            'assets'    => $assets
        ]);
    }

    public function listOfAsset(Request $request)
    {
        $q = strtolower(trim($request->q));
        $lists = Asset::where([
            ['company_id',$request->company_id],
            ['origin_sub_department_location_id',$request->sub_department_id]
        ])
        ->whereNull('delete_at')
        ->where(function($query) use($q){
            $query->where('no_inventory','like',"%$q%")
            ->orWhere('serial_number','like',"%$q%")
            ->orWhere('no_inventory_manual','like',"%$q%")
            ->orWhere('erp_no_asset','like',"%$q%");
        })
        ->paginate(10);

        return view('consumable._assets_list',compact('lists'));
    } 
}
