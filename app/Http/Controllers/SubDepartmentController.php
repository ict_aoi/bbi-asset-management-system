<?php namespace App\Http\Controllers;

use DB;
use Auth;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\Department;
use App\Models\SubDepartment;

use App\Models\Absensi\SubDepartmentBbi;


class SubDepartmentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request)
    {
        $msg = $request->session()->get('message');
        return view('sub_department.index',compact('msg'));
    }

    public function data()
    {
    	if(request()->ajax())
        {
            $data = SubDepartmentBbi::orderby('id_department','asc');
           
            return datatables()->of($data)
            ->addColumn('code',function($data)
            {
                $sub_department = SubDepartment::where('sub_department_absensi_id',$data->id)->first();
            	return ($sub_department ? $sub_department->code : null);
            })
            ->editColumn('department_name',function($data)
            {
                $sub_department = SubDepartment::where('sub_department_absensi_id',$data->id)->first();
                return ($sub_department ? ucwords(strtolower($sub_department->department->name)) : null);
            })
            ->editColumn('subdept_name',function($data){
                return ucwords(strtolower($data->subdept_name));
            })
            ->addColumn('action', function($data) {
                return view('sub_department._action', [
                    'model' => $data,
                    'edit' => route('subDepartment.edit',$data->id)
                ]);
            })
            ->rawColumns(['action'])
            ->make(true);
        }
    }

    public function edit(Request $request,$id)
    {
        if($request->session()->has('message')) $request->session()->forget('message');
        
        $sub_department_absensi     = SubDepartmentBbi::where('id',$id)->first();
        $departments                = Department::pluck('name','id')->all();
        $sub_department             = SubDepartment::where('sub_department_absensi_id',$id)->first();
       
        return view('sub_department.edit',compact('departments','sub_department_absensi','id','sub_department'));
    }

    public function store(Request $request)
    {
        if($request->session()->has('message')) $request->session()->forget('message');
        if (SubDepartment::where([
            ['code',strtolower($request->code)],
            ['sub_department_absensi_id',$request->sub_department_absensi_id],
        ])
        ->whereNull('delete_at')
        ->exists())
        {
    		return response()->json(['message'=>'Kode departemen sudah ada, silahkan cari Kode departemen lain'],422);
        }

        try 
        {
            DB::beginTransaction();
            
            SubDepartment::FirstOrCreate([
                'code'                      => ($request->code ? strtolower(trim($request->code)) : null),
                'name'                      => strtolower(trim($request->name)),
                'sub_department_absensi_id' => $request->sub_department_absensi_id,
                'department_id'             => $request->department,
            ]);

            $request->session()->flash('message', 'success_2');
            DB::commit();
            
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }

        return response()->json('success', 200);
    }

    public function update(Request $request,$id)
    {
        if($request->session()->has('message')) $request->session()->forget('message');
        if (SubDepartment::where([
            ['code',strtolower($request->code)],
            ['id','!=',$id],
            ['sub_department_absensi_id',$request->department_absensi_id],
        ])
        ->whereNull('delete_at')
        ->exists())
        {
    		return response()->json(['message'=>'Kode departemen sudah ada, silahkan cari Kode departemen lain'],422);
        }

        try 
        {
            DB::beginTransaction();
            
            $sub_department                             = SubDepartment::find($id);
            $sub_department->code                       = ($request->code ?strtolower(trim($request->code))  : null);
            $sub_department->name                       = strtolower(trim($request->name));
            $sub_department->sub_department_absensi_id  = $request->sub_department_absensi_id;
            $sub_department->department_id              = $request->department;
            $sub_department->save();

            $request->session()->flash('message', 'success_2');
            DB::commit();
            
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }

        return response()->json('success', 200);

        
    }
}
