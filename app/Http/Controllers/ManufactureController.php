<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Excel;
use StdClass;
use Validator;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\User;
use App\Models\Asset;
use App\Models\Company;
use App\Models\SubDepartment;
use App\Models\Manufacture;


class ManufactureController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $msg            = $request->session()->get('message');

        $sub_departments    = SubDepartment::where('is_other',false)
        ->whereIn('code',['ict','elk','ga','me'])
        ->whereNull('delete_at')
        ->pluck('name', 'id')->all();

        return view('manufacture.index',compact('msg','sub_departments'));
    }

    public function data(Request $request)
    {
        if (request()->ajax()) 
        {
            $sub_department = $request->sub_department;
            if(auth::user()->is_super_admin)
            {
                $data = DB::table('manufacture_v')
                ->where([
                    ['company_id',auth::user()->company_id],
                    ['sub_department_id','like',"%$sub_department%"],
                ]);
            } 
            else
            {
                $data = DB::table('manufacture_v')
                ->where([
                    ['company_id',auth::user()->company_id],
                    ['sub_department_id','like',"%$sub_department%"],
                ]);
            } 

            return datatables()->of($data)
            ->editColumn('company_name', function ($data) 
            {
                return ucwords($data->company_name);
            })
            ->editColumn('department_name', function ($data) 
            {
                return ucwords($data->department_name);
            })
            ->editColumn('name', function ($data) 
            {
                return ucwords($data->name);
            })
            ->editColumn('description', function ($data) 
            {
                return ucwords($data->description);
            })
            ->addColumn('action', function($data) 
            {
                return view('manufacture._action', [
                    'model'     => $data,
                    'edit'      => route('manufacture.edit',$data->id),
                    'delete'    => route('manufacture.destroy',$data->id),
                ]);
            })
            ->rawColumns(['action'])
            ->make(true);
        }
    }

    public function create(Request $request)
    {
        if($request->session()->has('message')) $request->session()->forget('message');
        $sub_departments = SubDepartment::where('is_other',false)
        ->whereNull('delete_at')
        ->whereIn('code',['ict','elk','ga','me'])
        ->pluck('name', 'id')
        ->all();
       
        return view('manufacture.create',compact('companies','company_id','sub_departments'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name'          => 'required',
            'sub_department'=> 'required'
        ]);

        $sub_department = SubDepartment::find($request->sub_department);

        if(!$sub_department)
        {
            return response()->json(['message' => 'Sub Departemen belum ada, silahkan sync terlebih dahulu'], 422);
        }

        if (Manufacture::where([
            ['name', strtolower($request->name)],
            ['company_id', auth::user()->company_id],
            ['department_id', $sub_department->department_id],
            ['sub_department_id', $sub_department->id],
        ])
        ->whereNull('delete_at')
        ->exists()) 
        {
            return response()->json(['message' => 'Nama sudah ada, silahkan cari nama lain'], 422);
        }

        try 
        {
            DB::beginTransaction();

            Manufacture::firstorCreate([
                'name'              => strtolower(trim($request->name)),
                'description'       => strtolower(trim($request->description)),
                'department_id'     => $sub_department->department_id,
                'sub_department_id' => $sub_department->id,
                'company_id'        => auth::user()->company_id,
                'create_user_id'    => Auth::user()->id,
                'created_at'        => $request->current_time,
                'updated_at'        => $request->current_time,
            ]);
            DB::commit();
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }

        $request->session()->flash('message', 'success');
        return response()->json('success', 200);
    }

    public function edit(Request $request,$id)
    {
        if($request->session()->has('message')) $request->session()->forget('message');
       
        $manufacture        = Manufacture::find($id);
       
        return view('manufacture.edit',compact('manufacture'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'          => 'required',
            'sub_department'=> 'required'
        ]);

        $manufacture    = Manufacture::findorFail($id);

        if (Manufacture::where([
            ['name', strtolower($request->name)],
            ['company_id', $manufacture->company_id],
            ['id','!=', $id],
            ['department_id', $manufacture->department_id],
            ['sub_department_id', $manufacture->sub_department_id],
        ])
        ->whereNull('delete_at')
        ->exists()) 
        {
            return response()->json(['message' => 'Nama sudah ada, silahkan cari nama lain'], 422);
        }

        try 
        {
            DB::beginTransaction();
            
            $manufacture->updated_at        = $request->current_time;
            $manufacture->name              = strtolower($request->name);
            $manufacture->description       = strtolower($request->description);
            $manufacture->save();

            DB::commit();
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }


       

        $request->session()->flash('message', 'success_2');
        return response()->json('success', 200);
    }
    
    public function destroy($id)
    {
        $count = Asset::where('manufacture_id',$id)->count();
        if($count == 0)
        {
            $manufacture                 = Manufacture::findorFail($id);
            $manufacture->delete();
        }else
        {
            $manufacture                 = Manufacture::findorFail($id);
            $manufacture->delete_at      = carbon::now();
            $manufacture->delete_user_id = Auth::user()->id;
            $manufacture->save();
        }
       
        return response()->json('success', 200);
    }


    public function import(Request $request)
    {
        if($request->session()->has('message')) $request->session()->forget('message');
        
        $companies      = Company::where('id',auth::user()->company_id)->whereNull('delete_at')->pluck('name', 'id')->all();
        
        if(auth::user()->is_super_admin)
        {
            $company_id         = null;
            $factory_id         = null;
            $sub_department_id  = null;
            $sub_departments    = SubDepartment::where('is_other',false)
            ->whereIn('code',['ict','elk','ga','me'])
            ->whereNull('delete_at')
            ->pluck('name', 'id')
            ->all();
       
        }else
        {
            $company_id         = auth::user()->company_id;
            $factory_id         = auth::user()->factory_id;
            $sub_department_id  = auth::user()->sub_department_id;
            $sub_departments    = SubDepartment::where('is_other',false)
            ->whereIn('code',['ict','elk','ga','me'])
            ->where('id',$sub_department_id)
            ->whereNull('delete_at')->pluck('name', 'id')
            ->all();
       
        } 


        return view('manufacture.import',compact('companies','sub_departments','company_id','factory_id','sub_department_id'));
    }

    public function exportFormImport(Request $request)
    {
       
        $company_id     = $request->company;
        $sub_department = $request->sub_department;
       
        return Excel::create('upload_manufacture',function ($excel) use($company_id,$sub_department) 
        {
            $excel->sheet('active', function($sheet) use($company_id,$sub_department) 
            {
                $sheet->setCellValue('A1','company_id');
                $sheet->setCellValue('B1','sub_department_id');
                $sheet->setCellValue('C1','nama_pabrikan');
                $sheet->setCellValue('D1','deskripsi');

                $sheet->cell('A1', function($cell) 
                {
                    $cell->setBackground('#000000');
                    $cell->setFontColor('#ffffff');
                });

                $sheet->cell('B1', function($cell) 
                {
                    $cell->setBackground('#000000');
                    $cell->setFontColor('#ffffff');
                });

                $sheet->cell('C1', function($cell) 
                {
                    $cell->setBackground('#ff4d4d');
                    $cell->setFontColor('#ffffff');
                });
                
                $j = 2;
                for ($i=0; $i < 100; $i++) 
                { 
                    $sheet->setCellValue('A'.$j,$company_id);
                    $sheet->setCellValue('B'.$j,$sub_department);

                    $sheet->cell('A'.$j, function($cell) 
                    {
                        $cell->setBackground('#000000');
                        $cell->setFontColor('#ffffff');
                    });

                    $sheet->cell('B'.$j, function($cell) 
                    {
                        $cell->setBackground('#000000');
                        $cell->setFontColor('#ffffff');
                    });

                    $sheet->cell('C'.$j, function($cell) 
                    {
                        $cell->setBackground('#ff4d4d');
                        $cell->setFontColor('#ffffff');
                    });
                    
                    $j++;
                }

                $sheet->setColumnFormat(array(
                    'C' => '@'
                ));
            });

        })->export('xlsx');

    }
    
    public function uploadFormImport(Request $request)
    {  
        $array = array();
        if($request->hasFile('upload_file')){
            $validator = Validator::make($request->all(), [
                'upload_file' => 'required|mimes:xls'
            ]);

            $current_time   = $request->current_time;
            $path           = $request->file('upload_file')->getRealPath();
            $data           = Excel::selectSheets('active')->load($path,function($render){})->get();

            if(!empty($data) && $data->count())
            {
                foreach ($data as $key => $value) 
                {
                    $company_id             = $value->company_id;
                    $sub_department_id      = strtolower($value->sub_department_id);
                    $nama_pabrikan          = strtolower($value->nama_pabrikan);
                    $description            = strtolower($value->deskripsi);
                    $_company               = Company::find($company_id);
                    $_sub_department        = SubDepartment::find($sub_department_id);
                    
                    if($company_id && $sub_department_id && $nama_pabrikan && $_company)
                    {
                        if (Manufacture::where([
                            [db::raw('lower(name)'), $nama_pabrikan],
                            ['company_id', $company_id],
                            ['department_id', $_sub_department->department_id],
                            ['sub_department_id', $sub_department_id],
                        ])
                        ->whereNull('delete_at')
                        ->exists()) 
                        {
                            $obj                        = new stdClass();
                            $obj->company_name          = $_company->name;
                            $obj->department_id         = $_sub_department->name;
                            $obj->name                  = $nama_pabrikan;
                            $obj->description           = $description;
                            $obj->is_error              = true;
                            $obj->system_log            = 'GAGAL, NAMA PABRIKAN SUDAH ADA';
                            $array []                   = $obj;
                        }else
                        {
                            try 
                            {
                                DB::beginTransaction();
                                Manufacture::firstorCreate([
                                    'name'              => $nama_pabrikan,
                                    'description'       => $description,
                                    'department_id'     => $_sub_department->department_id,
                                    'sub_department_id' => $_sub_department->id,
                                    'company_id'        => $company_id,
                                    'create_user_id'    => Auth::user()->id,
                                    'created_at'        => $current_time,
                                    'updated_at'        => $current_time,
                                ]);
                                DB::commit();

                                $obj                        = new stdClass();
                                $obj->company_name          = $_company->name;
                                $obj->department_id         = $_sub_department->name;
                                $obj->name                  = $nama_pabrikan;
                                $obj->description           = $description;
                                $obj->is_error              = false;
                                $obj->system_log            = 'BERHASIL, DATA SUDAH MASUK KEDALAM DATABASE';
                                $array []                   = $obj;
                            } catch (Exception $e) 
                            {
                                DB::rollBack();
                                $message = $e->getMessage();
                                ErrorHandler::db($message);
                            }
                        }
                    }
                 }   
                return response()->json($array,200);
            }else
            {
                return response()->json(['message' => 'import gagal, silahkan cek file anda'],422);
            }


        }
    }
}
