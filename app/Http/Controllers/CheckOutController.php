<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Config;
use DateTime;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Models\User;
use App\Models\Area;
use App\Models\Asset;
use App\Models\Company;
use App\Models\Factory;
use App\Models\Category;
use App\Models\Temporary;
use App\Models\AssetType;
use App\Models\Department;
use App\Models\DesignTable;
use App\Models\AssetMovement;
use App\Models\SubDepartment;
use App\Models\RentalAgreement;
use App\Models\ConsumableAsset;
use App\Models\ConsumableAssetDetail;
use App\Models\AssetInformationField;

use App\Models\Absensi\AbsensiBbi;


use App\Models\DiasAoi\AssetDev;
use App\Models\DiasAoi\AssetLive;
use App\Models\DiasAoi\RentalAgreementDev;
use App\Models\DiasAoi\RentalAgreementLive;
use App\Models\DiasAoi\CompanyDev as CompanyAoiDev;
use App\Models\DiasAoi\CompanyLive as CompanyAoiLive;
use App\Models\DiasAoi\FactoryDev as FactoryAoiDev;
use App\Models\DiasAoi\FactoryLive as FactoryAoiLive;
use App\Models\DiasAoi\DepartmentDev as DepartmentAoiDev;
use App\Models\DiasAoi\DepartmentLive as DepartmentAoiLive;


class CheckOutController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $sub_department_id  = auth::user()->sub_department_id;
        $department_id      = auth::user()->department_id;
        $factory_id         = auth::user()->factory_id; 

        if(Factory::where([
                ['id',$factory_id],
                ['code','oth'],
            ])
            ->exists()
        )
        {
            $filters = ['ict','elk','ga','me','oth'];
        }else
        {
            $filters = ['ict','elk','ga','me'];
        }
    
   
        $factories          = Factory::where('company_id',auth::user()->company_id)
        ->whereNull('delete_at')
        ->pluck('name', 'id')
        ->all();

        $factories_out      = Factory::whereNull('delete_at')
        ->pluck('name', 'id')
        ->all();

        $sub_departments    = SubDepartment::whereIn('code',$filters)
        ->whereNull('delete_at')
        ->pluck('name', 'id')
        ->all();

        $no_kks             = RentalAgreement::whereNull('deleted_at')->where([
            ['is_agreement_for_rental',false],
            ['sub_department_id',auth::user()->sub_department_id],
        ])->pluck('no_agreement', 'id')
        ->all();

        

        return view('checkout.index',compact('factories','department_id','sub_department_id','factory_id','sub_departments','factories_out','no_kks'));
    }

    public function store(Request $request)
    {
        try
        {
            DB::beginTransaction();
            $app_env                            = Config::get('app.env');
            $last_factory                       = $request->selected_last_factory_id;
            $origin_subdepartment               = $request->selected_origin_subdepartment_id;
            $destination_factory                = $request->destination_factory;
            $destination_subdepartment          = $request->destination_sub_department;
            $destination_area                   = ($request->destination_area ? trim(strtolower($request->destination_area)): null);
            $employee_name                      = ($request->employee_name ? trim(strtolower($request->employee_name)): null);
            $employee_department                = ($request->employee_department ? trim(strtolower($request->employee_department)): null);
            $employee_subdepartment             = ($request->employee_sub_department ? trim(strtolower($request->employee_sub_department)): null);
            $employee_nik                       = ($request->nik ? trim(strtolower($request->nik)): null);
            $new_oth_destination                = ($request->new_oth_destination ? trim(strtolower($request->new_oth_destination)): null);
            $checkout_to_status                 = $request->checkout_to_status;
            $status                             = $request->status;
            $new_no_kk                          = ($request->new_no_kk ? strtolower(trim($request->new_no_kk)) : null);
            $start_rent_date                    = ($request->start_rent_date ? Carbon::createFromFormat('d/m/Y H:i:s', $request->start_rent_date.'00:00:00')->format('Y-m-d H:i:s') : null);
            $end_rent_date                      = ($request->end_rent_date ? Carbon::createFromFormat('d/m/Y H:i:s', $request->end_rent_date.'23:59:59')->format('Y-m-d H:i:s') : null);
            $checkout_date                      = ($request->current_time ? $request->current_time : carbon::now()->toDateTimeString());
            $assets                             = json_decode($request->assets);
            $no_kk                              = $request->no_kk;
            $master_destinaton_factory          = Factory::find($destination_factory);
            $master_destinaton_subdepartment    = SubDepartment::find($destination_subdepartment);

            if($start_rent_date && $end_rent_date) $duration    = date_diff(new DateTime($start_rent_date),new DateTime($end_rent_date));
            else $duration  = null;
            
            if($checkout_to_status == '1')
            {
                // check out ke user
                foreach ($assets as $key => $value) 
                {
                    $asset              = Asset::find($value->id);
                    
                    $sub_department = SubDepartment::where('name',$employee_subdepartment)->first();
                    if(!$sub_department) return response()->json(['message' => 'Subdepartment is not exists, please do sync first.'], 422);

                    $asset_movement = AssetMovement::firstorcreate([
                        'asset_id'                          => $asset->id,
                        'status'                            => 'digunakan',
                        'from_company_location'             => $asset->last_company_location_id,
                        'from_factory_location'             => $asset->last_factory_location_id,
                        'from_department_location'          => $asset->last_department_location_id,
                        'from_sub_department_location'      => $asset->last_sub_department_location_id,
                        'from_employee_nik'                 => auth::user()->nik,
                        'from_employee_name'                => strtolower(auth::user()->name),
                        'to_company_location'               => $asset->last_company_location_id,
                        'to_factory_location'               => $asset->last_factory_location_id,
                        'to_department_location'            => $sub_department->department_id,
                        'to_sub_department_location'        => $sub_department->id,
                        'to_area_location'                  => null,
                        'to_employee_nik'                   => $employee_nik,
                        'to_employee_name'                  => $employee_name,
                        'rental_agreement_id'               => null,
                        'movement_date'                     => $checkout_date,
                        'note'                              => 'checkout to user',
                        'create_user_id'                    => auth::user()->id
                    ]);
                    
                    $asset->last_asset_movement_id          = $asset_movement->id;
                    $asset->status                          = 'digunakan';
                    $asset->last_used_by                    = $employee_nik.'-'.$employee_name;
                    $asset->last_movement_date              = $checkout_date;
                    $asset->last_company_location_id        = $asset->last_company_location_id;
                    $asset->last_factory_location_id        = $asset->last_factory_location_id;
                    $asset->last_department_location_id     = $sub_department->department_id;
                    $asset->last_sub_department_location_id = $sub_department->id;
                    $asset->last_area_location              = null;
                    $asset->last_user_movement_id           = auth::user()->id;
                    $asset->save();
    
                    Temporary::firstorCreate([
                        'column_1'  => $asset->origin_department_location_id,
                        'column_2'  => 'sync_export_asset_per_department',
                    ]);
                }
                
            }else
            {
                if($status == 'digunakan')
                {
                    foreach ($assets as $key => $value) 
                    {
                        $asset              = Asset::find($value->id);
                        
                        $asset_movement = AssetMovement::firstorcreate([
                            'asset_id'                          => $asset->id,
                            'status'                            => 'digunakan',
                            'from_company_location'             => $asset->last_company_location_id,
                            'from_factory_location'             => $asset->last_factory_location_id,
                            'from_department_location'          => $asset->last_department_location_id,
                            'from_sub_department_location'      => $asset->last_sub_department_location_id,
                            'from_employee_nik'                 => auth::user()->nik,
                            'from_employee_name'                => strtolower(auth::user()->name),
                            'to_company_location'               => $asset->last_company_location_id,
                            'to_factory_location'               => $asset->last_factory_location_id,
                            'to_department_location'            => $master_destinaton_subdepartment->department_id,
                            'to_sub_department_location'        => $master_destinaton_subdepartment->id,
                            'to_area_location'                  => $destination_area,
                            'to_employee_nik'                   => null,
                            'to_employee_name'                  => null,
                            'rental_agreement_id'               => null,
                            'movement_date'                     => $checkout_date,
                            'note'                              => 'checkout to area',
                            'create_user_id'                    => auth::user()->id
                        ]);
        
                        $asset->last_asset_movement_id          = $asset_movement->id;
                        $asset->status                          = 'digunakan';
                        $asset->last_used_by                    = null;
                        $asset->last_movement_date              = $checkout_date;
                        $asset->last_company_location_id        = $asset->last_company_location_id;
                        $asset->last_factory_location_id        = $asset->last_factory_location_id;
                        $asset->last_department_location_id     = $master_destinaton_subdepartment->department_id;
                        $asset->last_sub_department_location_id = $master_destinaton_subdepartment->id;
                        $asset->last_area_location              = $destination_area;
                        $asset->last_user_movement_id           = auth::user()->id;
                        $asset->save();
        
                        Temporary::firstorCreate([
                            'column_1'  => $asset->origin_department_location_id,
                            'column_2'  => 'sync_export_asset_per_department',
                        ]);
                    }
                }else if ($status == 'obsolete')
                {
                    foreach ($assets as $key => $value) 
                    {
                        $asset              = Asset::find($value->id);
                        
                        if($asset->no_rent) return response()->json(['message' => 'Asset rent cannot move to obsolete.'], 422);
                        if($asset->last_factory_location_id != $asset->origin_factory_location_id) return response()->json(['message' => 'last factory location is different with factory destination.'], 422);
                        
                        $asset_movement = AssetMovement::firstorcreate([
                            'asset_id'                          => $asset->id,
                            'status'                            => 'obsolete',
                            'from_company_location'             => $asset->last_company_location_id,
                            'from_factory_location'             => $asset->last_factory_location_id,
                            'from_department_location'          => $asset->last_department_location_id,
                            'from_sub_department_location'      => $asset->last_sub_department_location_id,
                            'from_employee_nik'                 => auth::user()->nik,
                            'from_employee_name'                => strtolower(auth::user()->name),
                            'to_company_location'               => $asset->last_company_location_id,
                            'to_factory_location'               => $asset->last_factory_location_id,
                            'to_department_location'            => $master_destinaton_subdepartment->department_id,
                            'to_sub_department_location'        => $master_destinaton_subdepartment->id,
                            'to_area_location'                  => null,
                            'to_employee_nik'                   => null,
                            'to_employee_name'                  => null,
                            'rental_agreement_id'               => null,
                            'movement_date'                     => $checkout_date,
                            'create_user_id'                    => auth::user()->id
                        ]);
        
                        $asset->last_asset_movement_id          = $asset_movement->id;
                        $asset->status                          = 'obsolete';
                        $asset->last_used_by                    = null;
                        $asset->last_movement_date              = $checkout_date;
                        $asset->last_company_location_id        = $asset->last_company_location_id;
                        $asset->last_factory_location_id        = $asset->last_factory_location_id;
                        $asset->last_department_location_id     = $master_destinaton_subdepartment->department_id;
                        $asset->last_sub_department_location_id = $master_destinaton_subdepartment->id;
                        $asset->last_area_location              = null;
                        $asset->last_user_movement_id           = auth::user()->id;
                        $asset->save();
        
                        Temporary::firstorCreate([
                            'column_1'  => $asset->origin_department_location_id,
                            'column_2'  => 'sync_export_asset_per_department',
                        ]);
                    }
                }else if ($status == 'dipinjam')
                {
                    if($no_kk == -1)
                    {
                        if($new_no_kk)
                        {
                            $rental_agreement_exits = RentalAgreement::where([
                                ['factory_id',$last_factory],
                                ['destination_factory_id',$destination_factory],
                                ['no_agreement',$new_no_kk],
                                ['sub_department_id',$master_destinaton_subdepartment->id],
                                ['is_internal',true],
                                ['description','no agremeent for dipinjam'],
                                ['created_at',$checkout_date],
                            ]);

                            if($new_oth_destination) $rental_agreement_exits = $rental_agreement_exits->where('destination_other',$new_oth_destination);

                            $rental_agreement_exits = $rental_agreement_exits->first();

                            if(!$rental_agreement_exits)
                            {
                                $new_rental = RentalAgreement::FirstOrCreate([
                                    'factory_id'                    => $last_factory,
                                    'destination_factory_id'        => $destination_factory,
                                    'sub_department_id'             => $master_destinaton_subdepartment->id,
                                    'no_agreement'                  => $new_no_kk,
                                    'price'                         => 0,
                                    'start_date'                    => carbon::now(),
                                    'end_date'                      => carbon::now(),
                                    'duration_in_day'               => 0,
                                    'description'                   => 'no agremeent for dipinjam',
                                    'created_at'                    => $checkout_date,
                                    'updated_at'                    => $checkout_date,
                                    'created_user_id'               => auth::user()->id,
                                    'updated_user_id'               => auth::user()->id,
                                    'deleted_user_id'               => null,
                                    'is_internal'                   => true,
                                    'destination_other'             => $new_oth_destination,
                                    'is_agreement_for_rental'       => true,
                                ]);

                                $rental_agreement_id = $new_rental->id;
                            }else $rental_agreement_id = $rental_agreement_exits->id;
                        }
                    }else 
                    {
                        $rental_agreement_id = $no_kk;
                    }

                    foreach ($assets as $key => $value) 
                    {
                        $asset              = Asset::find($value->id);
                        $has_dash           = strpos($asset->barcode, "-");
                        $barcode_handover   = ($has_dash ? $asset->barcode : $asset->barcode.'-2');

                        if($asset->is_rent) return response()->json(['message' => $asset->no_inventory.' is rental asset cannot move into different factory'], 422);
                        if($asset->last_factory_location_id != $asset->origin_factory_location_id) return response()->json(['message' => 'last factory location is different with factory destination.'], 422);
                        
                        $asset_movement = AssetMovement::firstorcreate([
                            'asset_id'                          => $asset->id,
                            'status'                            => 'dipinjam',
                            'from_company_location'             => $asset->last_company_location_id,
                            'from_factory_location'             => $asset->last_factory_location_id,
                            'from_department_location'          => $asset->last_department_location_id,
                            'from_sub_department_location'      => $asset->last_sub_department_location_id,
                            'from_employee_nik'                 => auth::user()->nik,
                            'from_employee_name'                => strtolower(auth::user()->name),
                            'to_company_location'               => $master_destinaton_factory->company_id,
                            'to_factory_location'               => $master_destinaton_factory->id,
                            'to_department_location'            => $master_destinaton_subdepartment->department_id,
                            'to_department_location'            => $master_destinaton_subdepartment->department_id,
                            'to_sub_department_location'        => $master_destinaton_subdepartment->id,
                            'to_area_location'                  => null,
                            'to_employee_nik'                   => null,
                            'to_employee_name'                  => null,
                            'rental_agreement_id'               => $rental_agreement_id,
                            'movement_date'                     => $checkout_date,
                            'create_user_id'                    => auth::user()->id,
                            'note'                              => null,
                        ]);
        
                        $asset->last_asset_movement_id          = $asset_movement->id;
                        $asset->status                          = 'dipinjam';
                        $asset->last_used_by                    = null;
                        $asset->last_movement_date              = $checkout_date;
                        $asset->source_company_location_id      = $asset->last_company_location_id;
                        $asset->source_factory_location_id      = $asset->last_factory_location_id;
                        $asset->source_department_location_id   = $asset->last_department_location_id;
                        $asset->last_company_location_id        = $master_destinaton_factory->company_id;
                        $asset->last_factory_location_id        = $master_destinaton_factory->id;
                        $asset->last_department_location_id     = $master_destinaton_subdepartment->department_id;
                        $asset->last_sub_department_location_id = $master_destinaton_subdepartment->id;
                        $asset->is_rent                         = true;
                        $asset->no_rent                         = $rental_agreement_id;
                        $asset->last_area_location              = null;
                        $asset->last_user_movement_id           = auth::user()->id;
                        $asset->save();
        
                        Temporary::firstorCreate([
                            'column_1'  => $asset->origin_department_location_id,
                            'column_2'  => 'sync_export_asset_per_department',
                        ]);
                    }
                }else if ($status == 'dikembalikan')
                {
                    foreach ($assets as $key => $value) 
                    {
                        $asset              = Asset::find($value->id);
                        $is_return_from_aoi = ((strtolower($asset->originFactory->code) == 'aoi1' || strtolower($asset->originFactory->code) == 'aoi2') ? true : false);
                        
                        if($destination_factory != $asset->origin_factory_location_id) return response()->json(['message' => 'destination factory is different with original factory.'], 422);
                        
                        if($asset->no_rent)
                        {
                            $old_rental_agreement = RentalAgreement::find($asset->no_rent);
                            
                            if($old_rental_agreement->destination_factory_id)
                            {
                                if($old_rental_agreement->bc_type_in_id)
                                {
                                    if($old_rental_agreement->factory_id == $destination_factory)
                                    {
                                        $new_rental_agreement_return = RentalAgreement::where([
                                            ['factory_id',$asset->last_factory_location_id],
                                            ['destination_factory_id',$destination_factory],
                                            ['no_agreement',strtolower($old_rental_agreement->no_agreement)],
                                            ['sub_department_id',$asset->origin_sub_department_location_id],
                                            ['is_internal',true],
                                            ['description','no agremeent for dikembalikan'],
                                            ['created_at',$checkout_date],
                                        ])
                                        ->first();
        
                                        if(!$new_rental_agreement_return)
                                        {
                                            $new_rental_agreement_return = RentalAgreement::FirstOrCreate([
                                                'factory_id'                    => $asset->last_factory_location_id,
                                                'destination_factory_id'        => $destination_factory,
                                                'sub_department_id'             => $asset->origin_sub_department_location_id,
                                                'no_agreement'                  => strtolower($old_rental_agreement->no_agreement),
                                                'price'                         => 0,
                                                'start_date'                    => $old_rental_agreement->start_date,
                                                'end_date'                      => $old_rental_agreement->end_date,
                                                'duration_in_day'               => $old_rental_agreement->duration_in_day,
                                                'description'                   => 'no agremeent for dikembalikan',
                                                'created_at'                    => $checkout_date,
                                                'updated_at'                    => $checkout_date,
                                                'created_user_id'               => auth::user()->id,
                                                'updated_user_id'               => auth::user()->id,
                                                'deleted_user_id'               => null,
                                                'is_internal'                   => $old_rental_agreement->is_internal,
                                                'destination_other'             => $old_rental_agreement->destination_other,
                                                'is_agreement_for_rental'       => $old_rental_agreement->is_agreement_for_rental,
                                                'is_return_to_source'           => true,
                                            ]);
                                           
                                            if(strtolower($master_destinaton_factory->code) == 'aoi1' || 
                                                strtolower($master_destinaton_factory->code) == 'aoi2')
                                            {
                                                if($app_env == 'live')
                                                {
                                                    $new_source_agreement = RentalAgreementLive::FirstOrCreate([
                                                        'factory_id'                    => $asset->lastFactory->mapping_id,
                                                        'destination_factory_id'        => $master_destinaton_factory->mapping_id,
                                                        'department_id'                 => $asset->originSubDepartment->mapping_id,
                                                        'no_agreement'                  => strtolower($new_rental_agreement_return->no_agreement),
                                                        'price'                         => 0,
                                                        'start_date'                    => $new_rental_agreement_return->start_date,
                                                        'end_date'                      => $new_rental_agreement_return->end_date,
                                                        'duration_in_day'               => $new_rental_agreement_return->duration_in_day,
                                                        'description'                   => $new_rental_agreement_return->description,
                                                        'created_at'                    => $checkout_date,
                                                        'updated_at'                    => $checkout_date,
                                                        'created_user_id'               => $new_rental_agreement_return->created_user_id,
                                                        'updated_user_id'               => $new_rental_agreement_return->updated_user_id,
                                                        'deleted_user_id'               => null,
                                                        'is_internal'                   => $new_rental_agreement_return->is_internal,
                                                        'destination_other'             => $new_rental_agreement_return->destination_other,
                                                        'is_agreement_for_rental'       => $new_rental_agreement_return->is_agreement_for_rental,
                                                        'mapping_id'                    => $new_rental_agreement_return->id,
                                                        'is_return_to_source'           => true,
                                                    ]);

                                                    $new_rental_agreement_return->mapping_id = $new_source_agreement->id;
                                                    $new_rental_agreement_return->save();

                                                    $new_source_agreement_id = $new_source_agreement->id;

                                                    $source_asset       = AssetLive::find($asset->mapping_id);
                                                }else
                                                {
                                                    $new_source_agreement = RentalAgreementDev::FirstOrCreate([
                                                        'factory_id'                    => $asset->lastFactory->mapping_id,
                                                        'destination_factory_id'        => $master_destinaton_factory->mapping_id,
                                                        'department_id'                 => $asset->originSubDepartment->mapping_id,
                                                        'no_agreement'                  => strtolower($new_rental_agreement_return->no_agreement),
                                                        'price'                         => 0,
                                                        'start_date'                    => $new_rental_agreement_return->start_date,
                                                        'end_date'                      => $new_rental_agreement_return->end_date,
                                                        'duration_in_day'               => $new_rental_agreement_return->duration_in_day,
                                                        'description'                   => $new_rental_agreement_return->description,
                                                        'created_at'                    => $checkout_date,
                                                        'updated_at'                    => $checkout_date,
                                                        'created_user_id'               => $new_rental_agreement_return->created_user_id,
                                                        'updated_user_id'               => $new_rental_agreement_return->updated_user_id,
                                                        'deleted_user_id'               => null,
                                                        'is_internal'                   => $new_rental_agreement_return->is_internal,
                                                        'destination_other'             => $new_rental_agreement_return->destination_other,
                                                        'is_agreement_for_rental'       => $new_rental_agreement_return->is_agreement_for_rental,
                                                        'mapping_id'                    => $new_rental_agreement_return->id,
                                                        'is_return_to_source'           => true,
                                                    ]);

                                                    $new_rental_agreement_return->mapping_id = $new_source_agreement->id;
                                                    $new_rental_agreement_return->save();

                                                    $new_source_agreement_id    = $new_source_agreement->id;
                                                    $source_asset               = AssetDev::find($asset->mapping_id);
                                                }

                                                if($source_asset)
                                                {
                                                    $source_asset->no_rent                         = $new_source_agreement_id;
                                                    $source_asset->status                          = 'dikembalikan';
                                                    $source_asset->last_used_by                    = null;
                                                    $source_asset->last_movement_date              = $checkout_date;
                                                    $source_asset->last_company_location_id        = $master_destinaton_factory->company->mapping_id;
                                                    $source_asset->last_factory_location_id        = $master_destinaton_factory->mapping_id;
                                                    $source_asset->last_department_location_id     = $master_destinaton_subdepartment->mapping_id;
                                                    $source_asset->last_area_location              = null;
                                                    $source_asset->save();
                                                }
                                            }
                                            
                                            $rental_agreement_id    = $new_rental_agreement_return->id;
                                        }else $rental_agreement_id    = $new_rental_agreement_return->id;
                                    }else return response()->json(['message' => 'If you want to return asset  '.$asset->no_inventory.' please to '.$old_rental_agreement->factory->code.' not factory '.$destination_factory->code], 422);
                                    
                                }else return response()->json(['message' => 'If you want to return asset  '.$asset->no_inventory.' please input bc in first for no kk '.$old_rental_agreement->no_agreement], 422);
                            }else return response()->json(['message' => 'If you want to return asset  '.$asset->no_inventory.' from outside bbigroup do it from menu rental agreement'], 422);
                        }else return response()->json(['message' => 'No KK source for asset  '.$asset->no_inventory.' is not found'], 422);
                        
                        $asset_movement = AssetMovement::firstorcreate([
                            'asset_id'                          => $asset->id,
                            'status'                            => 'dikembalikan',
                            'from_company_location'             => $asset->last_company_location_id,
                            'from_factory_location'             => $asset->last_factory_location_id,
                            'from_department_location'          => $asset->last_department_location_id,
                            'from_sub_department_location'      => $asset->last_sub_department_location_id,
                            'from_employee_nik'                 => auth::user()->nik,
                            'from_employee_name'                => auth::user()->name,
                            'to_company_location'               => $master_destinaton_factory->company_id,
                            'to_factory_location'               => $master_destinaton_factory->id,
                            'to_department_location'            => $master_destinaton_subdepartment->department_id,
                            'to_sub_department_location'        => $master_destinaton_subdepartment->id,
                            'to_area_location'                  => null,
                            'to_employee_nik'                   => null,
                            'to_employee_name'                  => null,
                            'rental_agreement_id'               => $rental_agreement_id,
                            'movement_date'                     => $checkout_date,
                        ]);
        
                        $asset->last_asset_movement_id          = $asset_movement->id;
                        $asset->status                          = 'dikembalikan';
                        $asset->last_used_by                    = null;
                        $asset->last_movement_date              = $checkout_date;
                        $asset->last_company_location_id        = $master_destinaton_factory->company_id;
                        $asset->last_factory_location_id        = $master_destinaton_factory->id;
                        $asset->last_department_location_id     = $master_destinaton_subdepartment->department_id;
                        $asset->last_sub_department_location_id = $master_destinaton_subdepartment->id;
                        $asset->last_area_location              = null;
                        $asset->no_rent                         = $rental_agreement_id;
                        $asset->last_user_movement_id           = auth::user()->id;

                        if($is_return_from_aoi)
                        {
                            $asset->delete_at                       = $checkout_date;
                            $asset->delete_user_id                  = Auth::user()->id;
                        }else
                        {
    
                            $asset->source_company_location_id          = $asset->last_company_location_id;
                            $asset->source_factory_location_id          = $asset->last_factory_location_id;
                            $asset->source_department_location_id       = $asset->last_department_location_id;
                            $asset->source_sub_department_location_id   = $asset->last_sub_department_location_id;
                        }

                        $asset->save();
        
                        Temporary::firstorCreate([
                            'column_1'  => $asset->origin_department_location_id,
                            'column_2'  => 'sync_export_asset_per_department',
                        ]);
                    }
                }else if ($status == 'rusak')
                {
                    foreach ($assets as $key => $value) 
                    {
                        $asset              = Asset::find($value->id);
                        
                        $asset_movement = AssetMovement::firstorcreate([
                            'asset_id'                          => $asset->id,
                            'status'                            => 'rusak',
                            'from_company_location'             => $asset->last_company_location_id,
                            'from_factory_location'             => $asset->last_factory_location_id,
                            'from_department_location'          => $asset->last_department_location_id,
                            'from_sub_department_location'      => $asset->last_department_location_id,
                            'from_employee_nik'                 => auth::user()->nik,
                            'from_employee_name'                => strtolower(auth::user()->name),
                            'to_company_location'               => $asset->last_company_location_id,
                            'to_factory_location'               => $asset->last_factory_location_id,
                            'to_department_location'            => $master_destinaton_subdepartment->department_id,
                            'to_sub_department_location'        => $master_destinaton_subdepartment->id,
                            'to_area_location'                  => $destination_area,
                            'to_employee_nik'                   => null,
                            'to_employee_name'                  => null,
                            'rental_agreement_id'               => null,
                            'movement_date'                     => $checkout_date,
                            'note'                              => null,
                            'create_user_id'                    => auth::user()->id
                        ]);
        
                        $asset->last_asset_movement_id          = $asset_movement->id;
                        $asset->status                          = 'rusak';
                        $asset->last_used_by                    = null;
                        $asset->last_movement_date              = $checkout_date;
                        $asset->last_company_location_id        = $asset->last_company_location_id;
                        $asset->last_factory_location_id        = $asset->last_factory_location_id;
                        $asset->last_department_location_id     = $master_destinaton_subdepartment->department_id;
                        $asset->last_sub_department_location_id = $master_destinaton_subdepartment->id;
                        $asset->last_area_location              = $destination_area;
                        $asset->last_user_movement_id           = auth::user()->id;
                        $asset->save();
        
                        Temporary::firstorCreate([
                            'column_1'  => $asset->origin_department_location_id,
                            'column_2'  => 'sync_export_asset_per_department',
                        ]);
                    }
                }else if ($status == 'dipindah tangan')
                {
                    if($no_kk == -1)
                    {
                        if($new_no_kk)
                        {
                            $rental_agreement_exits = RentalAgreement::where([
                                ['factory_id',$last_factory],
                                ['destination_factory_id',$destination_factory],
                                ['no_agreement',$new_no_kk],
                                ['sub_department_id',$master_destinaton_subdepartment->id],
                                ['is_internal',true],
                                ['description','no agremeent for dipindah tangan'],
                                ['created_at',$checkout_date],
                            ]);

                            if($new_oth_destination) $rental_agreement_exits = $rental_agreement_exits->where('destination_other',$new_oth_destination);

                            $rental_agreement_exits = $rental_agreement_exits->first();

                            if(!$rental_agreement_exits)
                            {
                                $new_rental = RentalAgreement::FirstOrCreate([
                                    'factory_id'                    => $last_factory,
                                    'destination_factory_id'        => $destination_factory,
                                    'sub_department_id'             => $master_destinaton_subdepartment->id,
                                    'no_agreement'                  => $new_no_kk,
                                    'price'                         => 0,
                                    'start_date'                    => carbon::now(),
                                    'end_date'                      => carbon::now(),
                                    'duration_in_day'               => 0,
                                    'description'                   => 'no agremeent for dipindah tangan',
                                    'created_at'                    => $checkout_date,
                                    'updated_at'                    => $checkout_date,
                                    'created_user_id'               => auth::user()->id,
                                    'updated_user_id'               => auth::user()->id,
                                    'deleted_user_id'               => null,
                                    'is_internal'                   => true,
                                    'destination_other'             => $new_oth_destination,
                                    'is_agreement_for_rental'       => true,
                                ]);

                                $rental_agreement_id = $new_rental->id;
                            }else $rental_agreement_id = $rental_agreement_exits->id;
                        }
                    }else 
                    {
                        $rental_agreement_id = $no_kk;
                    }

                    foreach ($assets as $key => $value) 
                    {
                        $asset              = Asset::find($value->id);
                        $has_dash           = strpos($asset->barcode, "-");
                        $barcode_handover   = ($has_dash ? $asset->barcode : $asset->barcode.'-2');

                        if($asset->is_rent) return response()->json(['message' => $asset->no_inventory.' is rental asset cannot move into different factory'], 422);
                        if($asset->last_factory_location_id != $asset->origin_factory_location_id) return response()->json(['message' => 'last factory location is different with factory destination.'], 422);
                        
                        $asset_movement = AssetMovement::firstorcreate([
                            'asset_id'                          => $asset->id,
                            'status'                            => 'dipindah tangan',
                            'from_company_location'             => $asset->last_company_location_id,
                            'from_factory_location'             => $asset->last_factory_location_id,
                            'from_department_location'          => $asset->last_department_location_id,
                            'from_sub_department_location'      => $asset->last_sub_department_location_id,
                            'from_employee_nik'                 => auth::user()->nik,
                            'from_employee_name'                => strtolower(auth::user()->name),
                            
                            'to_company_location'               => $master_destinaton_factory->company_id,
                            'to_factory_location'               => $master_destinaton_factory->id,
                            'to_department_location'            => $master_destinaton_subdepartment->department_id,
                            'to_sub_department_location'        => $master_destinaton_subdepartment->id,
                            'to_area_location'                  => null,
                            'to_employee_nik'                   => null,
                            'to_employee_name'                  => null,
                            'rental_agreement_id'               => $rental_agreement_id,
                            'movement_date'                     => $checkout_date,
                            'create_user_id'                    => auth::user()->id,
                            'note'                              => null,
                        ]);
        
                        $asset->last_asset_movement_id          = $asset_movement->id;
                        $asset->status                          = 'dipindah tangan';
                        $asset->last_used_by                    = null;
                        $asset->last_movement_date              = $checkout_date;
                        $asset->source_company_location_id      = $asset->last_company_location_id;
                        $asset->source_factory_location_id      = $asset->last_factory_location_id;
                        $asset->source_department_location_id   = $asset->last_department_location_id;
                        $asset->last_company_location_id        = $master_destinaton_factory->company_id;
                        $asset->last_factory_location_id        = $master_destinaton_factory->id;
                        $asset->last_department_location_id     = $master_destinaton_subdepartment->department_id;
                        $asset->last_sub_department_location_id = $master_destinaton_subdepartment->id;
                        $asset->is_rent                         = false;
                        $asset->no_rent                         = $rental_agreement_id;
                        $asset->last_area_location              = null;
                        $asset->last_user_movement_id           = auth::user()->id;
                        $asset->save();
        
                        Temporary::firstorCreate([
                            'column_1'  => $asset->origin_department_location_id,
                            'column_2'  => 'sync_export_asset_per_department',
                        ]);
                    }
                }
            }
            
            DB::commit();
            return response()->json('success',200);

        } catch (Exception $e)
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    public function getDestinationSubDepartment(Request $request)
    {
        $sub_department_id  = $request->origin_subdepartment_id;
        $factory_id         = $request->factory_id;
        $status             = $request->status;
        $sub_departments    = SubDepartment::where('is_other',false);
        
        if($status != 'digunakan') $sub_departments = $sub_departments->where('id',$sub_department_id);

        $sub_departments = $sub_departments->whereNull('delete_at')
        ->pluck('name','id')
        ->all();
        
        return response()->json($sub_departments,200);

    }

    public function getDestinationArea(Request $request)
    {
        $factory_id = $request->factory_id;
        
        $areas = Area::select('name')
        ->where([
            ['factory_id',$factory_id],
            ['is_area_stock',false]
        ])
        ->whereNotnULL('name')
        ->groupby('name')
        ->pluck('name','name')
        ->all();
        
        return response()->json($areas,200);

    }

    public function getOtherFactory(Request $request)
    {
        $factory_id = $request->factory_id;
        
        $factories = Factory::whereNull('delete_at')
      	->where('id','!=',$factory_id)
		->pluck('name','id')
        ->all();
        
        return response()->json($factories,200);

    }

    public function getAbsence(Request $request)
    {
        $factory_id = $request->factory_id;
        $factory = Factory::find($factory_id);
        
        if($factory)
        {
            $absences = AbsensiBbi::select('nik','name','department_name','subdept_name')
            ->where(function($query){
                $query->whereNotNull('department_name')
                ->whereNotNull('subdept_name');
            })
            ->get(); 

            $getAbsen = [];
            foreach ($absences as $key => $value) 
            {
                $getAbsen [] = $value->nik.':'.ucwords(strtolower(trim($value->name))).':'.ucwords(strtolower(trim($value->department_name))).':'.ucwords(strtolower(trim($value->subdept_name)));
            }
            
            // return $getAbsen;
        }
        
        
        return response()->json($getAbsen,200);

    }

    public function listOfKK (Request $request)
    {
        $factory_id             = $request->factory_id;
        $destination_factory_id = $request->destination_factory_id;
        $status                 = $request->status;
        $sub_department_id      = $request->subdepartment_id;

        $no_kks        = RentalAgreement::whereNull('deleted_at')
        ->where(function($query) use ($destination_factory_id)
        {
            $query->where('destination_factory_id','LIKE',"%$destination_factory_id%");
        })
        ->where([
            ['factory_id',$factory_id],
            ['sub_department_id',$sub_department_id],
        ]);

        if($status == 'obsolete') $no_kks = $no_kks->where('description','no agremeent for obsolete');
        else if($status == 'dipindah tangan') $no_kks = $no_kks->where('description','no agremeent for dipindah tangan');
        else if($status == 'dipinjam') $no_kks = $no_kks->where('description','no agremeent for dipinjam');
        else if($status == 'dikembalikan') $no_kks = $no_kks->where('description','no agremeent for dikembalikan');

        $no_kks = $no_kks->orderby('created_at','desc')
        ->pluck('no_agreement', 'id')
        ->all();

        return response()->json($no_kks,200); 
    }   

    public function listOfAsset(Request $request)
    {
        $q = strtolower(trim($request->q));

        $lists = Asset::where([
            ['last_factory_location_id',$request->factory_id],
            ['origin_sub_department_location_id',$request->sub_department_id]
        ])
        ->where(function($query) use($q)
        {
            $query->where('status','siap digunakan')
            ->orWhere('status','dikeluarkan')
            ->orWhere('status','digunakan');
        })
        ->where(function($query) use($q){
            $query->where('no_inventory','like',"%$q%")
            ->orWhere('serial_number','like',"%$q%")
            ->orWhere('no_inventory_manual','like',"%$q%")
            ->orWhere('erp_no_asset','like',"%$q%");
        })
        ->whereNull('delete_at')
        ->paginate(10);

        return view('checkout._lov_assets',compact('lists'));
    }   

    public function getAsset(Request $request)
    {
        $factory_id             = $request->factory;
        $sub_department_id      = $request->sub_department_id;
        $barcode                = $request->barcode;
        
        $asset                  = Asset::where([
            ['barcode',$barcode],
            ['last_factory_location_id',$factory_id],
            ['origin_sub_department_location_id',$sub_department_id],
        ])
        ->where(function($query)
        {
            $query->where('status','siap digunakan')
            ->orWhere('status','digunakan');
        })
        ->whereNull('delete_at')
        ->first();

        if(!$asset)
        {
            $area = Area::where([
                ['is_area_stock',false],
                ['factory_id',$factory_id],
                ['code',strtoupper($barcode)],
            ])
            ->first();

            if(!$area) return response()->json(['message' => 'Barcode not found'],422); 

            return response()->json([
                'area'  => $area,
                'asset' => null,
            ],200);
        }else
        {
            $asset_custom_fields    = AssetInformationField::where([
                ['asset_type_id', $asset->asset_type_id],
            ])
            ->whereNotIn('name',[
                'department_name',
                'created_at',
                'updated_at',
                'referral_code',
                'sequence',
                'image',
                'delete_at',
                'create_user_id',
                'source_company_location_id',
                'source_factory_location_id',
                'source_department_location_id',
                'delete_user_id',
                'is_from_sto_date',
                'qty_available',
                'qty_used',
                'total',
                'erp_item_id',
                'erp_asset_group_id',
                
            ])
            ->whereNull('delete_at')
            ->orderby('created_at','asc')
            ->get();
    
            $array = array();
            foreach ($asset_custom_fields as $key => $value) 
            {
                $column_name        = $value->name;
                $column_type        = $value->type;
                $curr_value         = ($asset)? $asset->$column_name:null;
    
                if($column_name == 'asset_type_id') $curr_value                 = $asset->assetType->name;
                if($column_name == 'company_id') $curr_value                    = $asset->company->name;
                if($column_name == 'factory_id') $curr_value                    = $asset->factory->name;
                if($column_name == 'department_id') $curr_value                 = $asset->department->name;
                if($column_name == 'manufacture_id') $curr_value                = ($asset->manufacture_id)? $asset->manufacture->name : null;
                if($column_name == 'last_company_location_id') $curr_value      = $asset->lastCompany->name;
                if($column_name == 'last_factory_location_id') $curr_value      = $asset->lastFactory->name;
                if($column_name == 'last_department_location_id') $curr_value   = $asset->lastDepartment->name;
                if($column_name == 'last_movement_date') $curr_value            = ($asset->last_movement_date ? $asset->last_movement_date->format('d/m/Y H:i:s') : null);
                
                if($asset->assetType->is_consumable == false)
                {
                    if($column_name == 'total') $curr_value = 1;
                }
    
                if($column_type == 'lookup')
                {
                    $splits                     = explode('|',$curr_value);
                    if($curr_value)
                    {
                        $custom_lookup_id           = $splits[0];
                        $lookup_id                  = $splits[1];
                        $detail_lookup_id           = $splits[2];
    
                        $consumable_asset           = ConsumableAsset::where([
                            ['asset_type_id',$custom_lookup_id],
                            ['id',$lookup_id],
                        ])
                        ->first();
        
                        $consumable_asset_detail    = ConsumableAssetDetail::where([
                            ['consumable_asset_id',$lookup_id],
                            ['id',$detail_lookup_id],
                        ])
                        ->first();
        
                        $model_header               = ($consumable_asset ? $consumable_asset->model : null);
                        $serial_number_header       = ($consumable_asset ? $consumable_asset->serial_number : null);
                        $detail_value               = ($consumable_asset_detail) ? $consumable_asset_detail->value : null;
                        $lookup_value               = $model_header;
        
                        if($serial_number_header)   $lookup_value .= ':'.strtoupper($serial_number_header);
                        if($detail_value)           $lookup_value .= ':'.strtoupper($detail_value);
                        
                        $curr_value                 = $lookup_value;
                    }else $curr_value               = $curr_value;
                    
    
                    
                }
                
                $obj                = new stdClass();
                $obj->column_name   = $column_name;
                $obj->label         = ucwords(str_replace('_',' ',$value->label));
                $obj->value         = ucwords($curr_value);
                $array []           = $obj;
            }
    
            if($asset->no_rent)
            {
                $rental_agreement = RentalAgreement::where('is_internal',true)->find($asset->no_rent);
                if($rental_agreement)
                {
                    if($rental_agreement->is_internal)
                    {
                        if(!$rental_agreement->updated_bc_in_date) return response()->json(['message' => 'Please input bc in first for no kk '.$rental_agreement->no_agreement],422); 
                    }
                }
            }
            
            $obj                        = new stdClass();
            $obj->id                    = $asset->id;
            $obj->barcode               = $asset->barcode;
            $obj->no_inventory          = strtoupper($asset->no_inventory);
            $obj->erp_no_asset          = strtoupper($asset->erp_no_asset);
            $obj->asset_type            = ucwords($asset->assetType->name);
            $obj->serial_number         = strtoupper($asset->serial_number);
            $obj->model                 = ucwords($asset->model);
            $obj->last_location         = 'Last Factory:'.($asset->last_factory_location_id ? $asset->lastFactory->name : '-').', Last Department:'.($asset->last_department_location_id ? $asset->lastDepartment->name : '-').', Last Area Location:'.($asset->last_area_location ? $asset->last_area_location : '-');
            $obj->detail_informations   = $array;
    
            return response()->json([
                'area'  => null,
                'asset' => $obj,
            ],200);
        }

    }
}
