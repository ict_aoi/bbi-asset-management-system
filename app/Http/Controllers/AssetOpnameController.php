<?php namespace App\Http\Controllers;

use DB;
use Auth;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Models\User;
use App\Models\Area;
use App\Models\Asset;
use App\Models\Company;
use App\Models\Factory;
use App\Models\Calendar;
use App\Models\Temporary;
use App\Models\Department;
use App\Models\DesignTable;
use App\Models\AssetMovement;
use App\Models\ConsumableAsset;
use App\Models\ConsumableAssetDetail;
use App\Models\AssetInformationField;

class AssetOpnameController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $factories          = Factory::where('company_id',auth::user()->company_id)->whereNull('delete_at')->pluck('name', 'id')->all();
        $departments        = Department::whereNull('delete_at')->pluck('name', 'id')->all();
        
        $department_id      = auth::user()->department_id;
        $factory_id         = auth::user()->factory_id;
        $date_now           = carbon::now()->format('Y-m-d');

        $calendar           = Calendar::whereNull('deleted_at')
        ->whereBetween(db::raw("'$date_now'"),[db::raw("event_start"),db::raw("event_end")])
        ->first();

    	return view('asset_opname.index',compact('factories','departments','department_id','factory_id','calendar'));
    }

    public function store(Request $request)
    {
        try
        {
            DB::beginTransaction();
            $calendar_id            = $request->calendar_id;
            $opname_area            = $request->opname_area_location;
            $opname_factory         = Factory::find($request->factory_id);
            $assets                 = json_decode($request->assets);

            foreach ($assets as $key => $value) 
            {
                $status         = $value->status;
                $asset          = Asset::find($value->id);
                $old_status     = $asset->status;
                $opname_date    = $request->current_time;

                $asset_movement =  AssetMovement::firstorcreate([
                    'asset_id'                          => $asset->id,
                    'status'                            => 'sto',
                    'from_company_location'             => $asset->last_company_location_id,
                    'from_factory_location'             => $asset->last_factory_location_id,
                    'from_department_location'          => $asset->last_department_location_id,
                    'from_employee_nik'                 => auth::user()->nik,
                    'from_employee_name'                => auth::user()->name,
                    'to_company_location'               => $opname_factory->company_id,
                    'to_factory_location'               => $opname_factory->id,
                    'to_department_location'            => $asset->last_department_location_id,
                    'to_employee_nik'                   => auth::user()->nik,
                    'to_employee_name'                  => auth::user()->name,
                    'to_area_location'                  => ($opname_area ? $opname_area : $asset->last_area_location),
                    'no_kk'                             => null,
                    'no_bea_cukai'                      => null,
                    'note'                              => 'Asset berhasil discan ketika proses sto di area '.$opname_area.', pabrik '.$opname_factory->name.', di STO oleh '.auth::user()->name.' pada tanggal '.$opname_date.'. kondisi asset pada saat di STO adalah '.strtoupper($status),
                    'movement_date'                     => $opname_date,
                    'calendar_id'                       => $calendar_id,
                ]);

                if($status == 'rusak') $asset->status                  = 'rusak';

                $asset->last_asset_movement_id  = $asset_movement->id;
                $asset->last_sto_date           = $opname_date;
                $asset->sto_user_id             = auth::user()->id;
                
                if($opname_area) $asset->last_area_location      = $opname_area;
                $asset->save();

                Temporary::firstorCreate([
                    'column_1'  => $asset->origin_department_location_id,
                    'column_2'  => 'sync_export_asset_per_department',
                ]);
            }
           
            DB::commit();
            return response()->json('success',200);

        } catch (Exception $e)
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    public function listOfAsset(Request $request)
    {
        $q = strtolower(trim($request->q));

        $lists = Asset::where([
            ['company_id',auth::user()->company_id],
            ['factory_id',$request->factory_id],
        ])
        ->where(function($query)
        {
            $query->where('status','!=','dipindah tangan')
            ->orWhere('status','!=','dipinjam');
        })
        ->whereNull('delete_at')
        ->where(function($query) use($q)
        {
            $query->where(db::raw('lower(no_inventory)'),'like',"%$q%")
            ->orWhere(db::raw('lower(serial_number)'),'like',"%$q%")
            ->orWhere(db::raw('lower(no_inventory_manual)'),'like',"%$q%")
            ->orWhere(db::raw('lower(erp_no_asset)'),'like',"%$q%");
        })
        ->paginate(10);
        
        return view('checkin._lov_assets',compact('lists'));
    } 

    public function getAsset(Request $request)
    {
        $factory_id             = $request->factory;
        $barcode                = $request->barcode;
        $current_time           = ($request->current_time ? $request->current_time : carbon::now());
        
        $asset                  = Asset::where([
            ['barcode',$barcode],
            ['factory_id',$factory_id],
        ])
        ->where(function($query)
        {
            $query->where('status','!=','dipindah tangan')
            ->orWhere('status','!=','dipinjam');
        })
        ->whereNull('delete_at')
        ->first();

        if(!$asset)
        {
            $area = Area::where([
                ['is_area_stock',true],
                ['factory_id',$factory_id],
                ['code',strtoupper($barcode)],
            ])
            ->first();

            if(!$area)return response()->json(['message' => 'Barcode not found'],422); 

            return response()->json([
                'area'  => $area,
                'asset' => null,
            ],200);
        }else
        {
            $options = [
                [
                    'id'        => '',
                    'name'      => '-- Please Select --',
                    'selected'  => null
                ],
                [
                    'id'        => 'rusak',
                    'name'      => 'Broken',
                    'selected'  => null
                ],
                [
                    'id'        => 'tidak rusak',
                    'name'      => 'Not Broken',
                    'selected'  => null
                ],
            ];

            $asset_custom_fields    = AssetInformationField::where([
                ['asset_type_id', $asset->asset_type_id],
            ])
            ->whereNotIn('name',[
                'department_name',
                'created_at',
                'updated_at',
                'referral_code',
                'sequence',
                'image',
                'delete_at',
                'create_user_id',
                'source_company_location_id',
                'source_factory_location_id',
                'source_department_location_id',
                'delete_user_id',
                'is_from_sto_date',
                'qty_available',
                'qty_used',
                'total',
                'erp_item_id',
                'erp_asset_group_id',
                
            ])
            ->whereNull('delete_at')
            ->orderby('created_at','asc')
            ->get();
    
            $array = array();
            foreach ($asset_custom_fields as $key => $value) 
            {
                $column_name        = $value->name;
                $column_type        = $value->type;
                $curr_value         = ($asset)? $asset->$column_name:null;
    
                if($column_name == 'asset_type_id') $curr_value                 = $asset->assetType->name;
                if($column_name == 'company_id') $curr_value                    = $asset->company->name;
                if($column_name == 'factory_id') $curr_value                    = $asset->factory->name;
                if($column_name == 'department_id') $curr_value                 = $asset->department->name;
                if($column_name == 'manufacture_id') $curr_value                = ($asset->manufacture_id)? $asset->manufacture->name : null;
                if($column_name == 'last_company_location_id') $curr_value      = $asset->lastCompany->name;
                if($column_name == 'last_factory_location_id') $curr_value      = $asset->lastFactory->name;
                if($column_name == 'last_department_location_id') $curr_value   = $asset->lastDepartment->name;
                if($column_name == 'last_movement_date') $curr_value            = ($asset->last_movement_date ? $asset->last_movement_date->format('d/m/Y H:i:s') : null);
                
                if($asset->assetType->is_consumable == false)
                {
                    if($column_name == 'total') $curr_value = 1;
                }
    
                if($column_type == 'lookup')
                {
                    $splits                     = explode('|',$curr_value);
                    if($curr_value)
                    {
                        $custom_lookup_id           = $splits[0];
                        $lookup_id                  = $splits[1];
                        $detail_lookup_id           = $splits[2];
    
                        $consumable_asset           = ConsumableAsset::where([
                            ['asset_type_id',$custom_lookup_id],
                            ['id',$lookup_id],
                        ])
                        ->first();
        
                        $consumable_asset_detail    = ConsumableAssetDetail::where([
                            ['consumable_asset_id',$lookup_id],
                            ['id',$detail_lookup_id],
                        ])
                        ->first();
        
                        $model_header               = ($consumable_asset ? $consumable_asset->model : null);
                        $serial_number_header       = ($consumable_asset ? $consumable_asset->serial_number : null);
                        $detail_value               = ($consumable_asset_detail) ? $consumable_asset_detail->value : null;
                        $lookup_value               = $model_header;
        
                        if($serial_number_header)   $lookup_value .= ':'.strtoupper($serial_number_header);
                        if($detail_value)           $lookup_value .= ':'.strtoupper($detail_value);
                        
                        $curr_value                 = $lookup_value;
                    }else $curr_value               = $curr_value;
                    
    
                    
                }
                
                $obj                = new stdClass();
                $obj->column_name   = $column_name;
                $obj->label         = ucwords(str_replace('_',' ',$value->label));
                $obj->value         = ucwords($curr_value);
                $array []           = $obj;
            }
    
            $obj                        = new stdClass();
            $obj->id                    = $asset->id;
            $obj->barcode               = $asset->barcode;
            $obj->no_inventory          = $asset->no_inventory;
            $obj->erp_no_asset          = $asset->erp_no_asset;
            $obj->asset_type            = $asset->assetType->name;
            $obj->serial_number         = $asset->serial_number;
            $obj->model                 = $asset->model;
            $obj->status_options        = $options;
            $obj->detail_informations   = $array;
            $obj->status                = null;
            $obj->is_error              = false;
            $obj->current_time          = $current_time;
    
            return response()->json([
                'area'  => null,
                'asset' => $obj,
            ],200);
        } 
    }

    public function getArea(Request $request)
    {
        $factory_id = $request->factory_id;
        
        $areas = Area::select('name')
        ->where([
            ['factory_id',$factory_id],
            ['is_area_stock',false]
        ])
        ->whereNotnULL('name')
        ->groupby('name')
        ->pluck('name','name')
        ->all();
        
        return response()->json($areas,200);

    }
}
