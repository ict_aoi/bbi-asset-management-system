<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Excel;
use DateTime;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\Asset;
use App\Models\Budget;
use App\Models\Factory;
use App\Models\SubDepartment;
use App\Models\RentalAgreement;
use App\Models\AssetMovement;

class RentalAgreementController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request)
    {
        $msg                = $request->session()->get('message');
        $factories          = Factory::whereNull('delete_at')->pluck('name', 'id')->all();
        $sub_departments    = SubDepartment::where('is_other',false)
        ->whereIn('code',['ict','elk','ga','me'])
        ->whereNull('delete_at')
        ->pluck('name', 'id')
        ->all();
        
        return view('rental_agreement.index',compact('msg','factories','sub_departments'));
    }

    public function data(Request $request)
    {
        if(request()->ajax()) 
        {
            $factory_id         = $request->factory;
            $sub_department_id  = $request->sub_department;
            $budget_id          = $request->budget;
            $status_agreement   = $request->status_agreement;
            
            $data = DB::table('rental_agreement_v')
            ->where('is_internal',false)
            ->where(function($query) use ($factory_id,$sub_department_id,$budget_id)
            {
                $query->where('factory_id','LIKE',"%$factory_id%")
                ->orWhere('sub_department_id','LIKE',"%$sub_department_id%")
                ->orWhere('budget_id','LIKE',"%$budget_id%");
            });
           

            if($status_agreement == '1') $data = $data->where('status','active')
            ->whereNull('return_date');
            else if($status_agreement == '2') $data = $data->where('status','non-active')
            ->whereNull('return_date');
            else if($status_agreement == '3') $data = $data->whereNotNull('return_date');
            
            return datatables()->of($data)
            ->addColumn('action', function($data) {
                return view('rental_agreement._action', [
                    'model'    => $data,
                    'edit'     => route('rentalAgreement.edit',$data->id),
                    'return'   => route('rentalAgreement.returnAsset',$data->id),
                    'extend'   => route('rentalAgreement.extend',$data->id),
                    'delete'   => route('rentalAgreement.delete',$data->id),
                ]);
            })
            ->editColumn('start_date',function ($data)
            {
                return  Carbon::createFromFormat('Y-m-d H:i:s', $data->start_date)->format('d/M/Y');
            })
            ->editColumn('end_date',function ($data)
            {
                return  Carbon::createFromFormat('Y-m-d H:i:s', $data->end_date)->format('d/M/Y');
            })
            ->editColumn('price',function($data)
            {
                return number_format($data->price, 2, '.', ',');
            })
            ->editColumn('status',function($data)
            {
                if($data->status == 'active') return '<span class="label label-success">Active</span>';
                else if ($data->return_date) return '<span class="label label-default">Returned</span>';
                else return '<span class="label label-default">Not-Active</span>';
            })
            ->setRowAttr([
                'style' => function($data)
                {
                    if(!$data->parent_no_agreement) return  'background-color: #d6d6d6;';
                },
            ])
            ->rawColumns(['action','status','style'])
            ->make(true);
        }
    }

    public function create(Request $request)
    {
        if($request->session()->has('message')) $request->session()->forget('message');

        $factories          = Factory::whereNull('delete_at')->where('company_id', auth::user()->company_id)->pluck('name', 'id')->all();
        $sub_departments    = SubDepartment::where('is_other',false)
        ->whereIn('code',['ict','elk','ga','me'])
        ->whereNull('delete_at')
        ->pluck('name', 'id')
        ->all();
        
        return view('rental_agreement.create',compact('factories','sub_departments'));
    }

    public function store(Request $request)
    {
        $factory_id         = $request->factory;
        $sub_department_id  = $request->sub_department;
        $budget_id          = $request->budget;
        $current_time       = ($request->current_time ? $request->current_time :carbon::now());
        $no_agreement       = strtolower(trim($request->no_agreement));
        $price_rent         = sprintf("%0.8f",$request->price_rent);
        $start_rent_date    = Carbon::createFromFormat('d/m/Y H:i:s', $request->start_rent_date.'00:00:00')->format('Y-m-d H:i:s');
        $end_rent_date      = Carbon::createFromFormat('d/m/Y H:i:s', $request->end_rent_date.'23:59:59')->format('Y-m-d H:i:s');
        $description        = strtolower(trim($request->description));
        
        $duration           = date_diff(new DateTime($start_rent_date),new DateTime($end_rent_date));
        
        $is_exists          = RentalAgreement::where([
            ['factory_id',$factory_id],
            ['sub_department_id',$sub_department_id],
            ['budget_id',$budget_id],
            ['no_agreement',$no_agreement],
            ['start_date',$start_rent_date],
            ['end_date',$end_rent_date],
        ])
        ->whereNull('deleted_at')
        ->exists();

        if($is_exists) return response()->json(['message' => 'Data already exists'],422);
       
        try 
        {
            DB::beginTransaction();
            
            RentalAgreement::FirstOrCreate([
                'factory_id'                    => $factory_id,
                'sub_department_id'             => $sub_department_id,
                'budget_id'                     => $budget_id,
                'parent_rental_agreement_id'    => null,
                'no_agreement'                  => $no_agreement,
                'price'                         => $price_rent,
                'start_date'                    => $start_rent_date,
                'end_date'                      => $end_rent_date,
                'duration_in_day'               => $duration->format("%d"),
                'description'                   => $description,
                'created_at'                    => $current_time,
                'updated_at'                    => $current_time,
                'created_user_id'               => auth::user()->id,
                'updated_user_id'               => auth::user()->id,
                'deleted_user_id'               => null,
                'is_agreement_for_rental'       => true,
                'is_extended'                   => false,
            ]);

            DB::commit();
            $request->session()->flash('message', 'success');
            return response()->json('sucess',200);
            
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    public function edit(Request $request,$id)
    {
        if($request->session()->has('message')) $request->session()->forget('message');

        $factories          = Factory::whereNull('delete_at')->where('company_id', auth::user()->company_id)->pluck('name', 'id')->all();
        $sub_departments    = SubDepartment::where('is_other',false)
        ->whereIn('code',['ict','elk','ga','me'])
        ->whereNull('delete_at')
        ->pluck('name', 'id')
        ->all();
        
        $rental_agreement   = RentalAgreement::find($id);

        $budgets = Budget::whereNull('deleted_at')->pluck('name','id')->all();

        if(!$rental_agreement->deleted_at) return view('rental_agreement.edit',compact('factories','sub_departments','rental_agreement','budgets'));
        else return redirect()->action('RentalAgreementController@index');
    }

    public function update(Request $request, $id)
    {
        $rental_agreement   = RentalAgreement::find($id);
        if(!$rental_agreeement->deleted_at && !$rental_agreeement->return_date)
        {
            $budget_id          = $request->budget;
            $current_time       = $request->current_time;
            $no_agreement       = strtolower(trim($request->no_agreement));
            $price_rent         = sprintf("%0.8f",$request->price_rent);
            $start_rent_date    = Carbon::createFromFormat('d/m/Y H:i:s', $request->start_rent_date.'00:00:00')->format('Y-m-d H:i:s');
            $end_rent_date      = Carbon::createFromFormat('d/m/Y H:i:s', $request->end_rent_date.'23:59:59')->format('Y-m-d H:i:s');
            $description        = strtolower(trim($request->description));
            
            $duration           = date_diff(new DateTime($start_rent_date),new DateTime($end_rent_date));
            
            $is_exists          = RentalAgreement::where([
                ['id','!=',$id],
                ['factory_id',$rental_agreement->factory_id],
                ['sub_department_id',$rental_agreement->sub_department_id],
                ['budget_id',$budget_id],
                ['no_agreement',$no_agreement],
                ['start_date',$start_rent_date],
                ['end_date',$end_rent_date],
            ])
            ->whereNull('deleted_at')
            ->exists();
    
            if($is_exists) return response()->json(['message' => 'Data already exists'],422);
           
            try 
            {
                DB::beginTransaction();
                
               
                $rental_agreement->budget_id        = $budget_id;
                $rental_agreement->no_agreement     = $no_agreement;
                $rental_agreement->price            = $price_rent;
                $rental_agreement->start_date       = $start_rent_date;
                $rental_agreement->end_date         = $end_rent_date;
                $rental_agreement->duration_in_day  = $duration->format("%d");
                $rental_agreement->description      = $description;
                $rental_agreement->updated_at       = $current_time;
                $rental_agreement->updated_user_id  = auth::user()->id;
                $rental_agreement->is_extended      = false;
                $rental_agreement->save();
                    
    
                DB::commit();
                $request->session()->flash('message', 'success_2');
                return response()->json('sucess',200);
                
            } catch (Exception $e) 
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }else return response()->json(['message' => 'Data has returned or deleted'],422);
        
    }

    public function extend(Request $request, $id)
    {
        $rental_agreement   = RentalAgreement::find($id);

        if($rental_agreement)
        {
            if(!$rental_agreement->deleted_at && !$rental_agreement->return_date)
            {
                $budget_id          = $request->budget;
                $current_time       = $request->current_time;
                $no_agreement       = strtolower(trim($request->no_agreement));
                $price_rent         = sprintf("%0.8f",$request->price_rent);
                $start_rent_date    = Carbon::createFromFormat('d/m/Y H:i:s', $request->start_rent_date.'00:00:00')->format('Y-m-d H:i:s');
                $end_rent_date      = Carbon::createFromFormat('d/m/Y H:i:s', $request->end_rent_date.'23:59:59')->format('Y-m-d H:i:s');
                $description        = strtolower(trim($request->description));
                
                $duration           = date_diff(new DateTime($start_rent_date),new DateTime($end_rent_date));
                
                $is_exists          = RentalAgreement::where([
                    ['id','!=',$id],
                    ['factory_id',$rental_agreement->factory_id],
                    ['sub_department_id',$rental_agreement->sub_department_id],
                    ['budget_id',$budget_id],
                    ['no_agreement',$no_agreement],
                    ['start_date',$start_rent_date],
                    ['end_date',$end_rent_date],
                ])
                ->whereNull('deleted_at')
                ->exists();
        
                if($is_exists) return response()->json(['message' => 'Data already exists'],422);
               
                try 
                {
                    DB::beginTransaction();
                    
                    $new_rent = RentalAgreement::FirstOrCreate([
                        'factory_id'                    => $rental_agreement->factory_id,
                        'destination_factory_id'        => $rental_agreement->destination_factory_id,
                        'sub_department_id'             => $rental_agreement->sub_department_id,
                        'bc_type_in_id'                 => ($rental_agreement->bc_type_in_id ? $rental_agreement->bc_type_in_id : null),
                        'no_bc_in'                      => ($rental_agreement->no_bc_in ? $rental_agreement->no_bc_in : null),
                        'no_aju_in'                     => ($rental_agreement->no_aju_in ? $rental_agreement->no_aju_in : null),
                        'no_registration_in'            => ($rental_agreement->no_registration_in ? $rental_agreement->no_registration_in : null),
                        'date_registration_in'          => ($rental_agreement->date_registration_in ? $rental_agreement->date_registration_in : null),
                        'bc_type_id'                    => ($rental_agreement->bc_type_id ? $rental_agreement->bc_type_id : null),
                        'no_bc'                         => ($rental_agreement->no_bc ? $rental_agreement->no_bc : null),
                        'no_aju'                        => ($rental_agreement->no_aju ? $rental_agreement->no_aju : null),
                        'no_registration'               => ($rental_agreement->no_registration ? $rental_agreement->no_registration : null),
                        'date_registration'             => ($rental_agreement->date_registration ? $rental_agreement->date_registration : null),
                        'budget_id'                     => $budget_id,
                        'parent_rental_agreement_id'    => $rental_agreement->id,
                        'no_agreement'                  => $no_agreement,
                        'price'                         => $price_rent,
                        'start_date'                    => $start_rent_date,
                        'end_date'                      => $end_rent_date,
                        'duration_in_day'               => $duration->format("%d"),
                        'description'                   => $description,
                        'created_at'                    => $current_time,
                        'updated_at'                    => $current_time,
                        'created_user_id'               => auth::user()->id,
                        'updated_user_id'               => auth::user()->id,
                        'deleted_user_id'               => null,
                        'is_agreement_for_rental'       => true,
                    ]);
        
                    $asset_movements = AssetMovement::whereNotNull('rental_agreement_id')
                    ->where('rental_agreement_id',$rental_agreement->id)
                    ->get();
    
                    foreach ($asset_movements as $key => $asset_movement) 
                    {
                        $asset_movement->rental_agreement_id = $rental_agreement->id;
                        $asset_movement->save();
                    }
    
                    Asset::where([
                        ['is_rent',true],
                        ['no_rent',$rental_agreement->id],
                    ])
                    ->update([
                        'no_rent' => $new_rent->id
                    ]);
                    
                    $rental_agreement->is_extended = false;
                    $rental_agreement->save();
        
                    DB::commit();
                    $request->session()->flash('message', 'success_2');
                    return response()->json('sucess',200);
                    
                } catch (Exception $e) 
                {
                    DB::rollBack();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }
            }else return response()->json(['message' => 'Data has returned or deleted'],422); 
        }return response()->json(['message' => 'Data not found'],422);
    }

    public function returnAsset(Request $request,$id)
    {
        $current_time       = ($request->current_time ? $request->current_time : carbon::now()->todatetimestring());
        $rental_agreeement  = RentalAgreement::find($id);
        
        if($rental_agreeement->bc_type_in_id)
        {
            if(!$rental_agreeement->deleted_at && !$rental_agreeement->return_date)
            {
                RentalAgreementController::doReturnAsset($rental_agreeement->id,$current_time);
    
                $parent     = $rental_agreeement->parent_rental_agreement_id;
                $array_parents    = array();
                while($parent != null)
                {
                    $rental_agreeement_parent = RentalAgreement::where('id',$parent)
                    ->whereNull('deleted_at')
                    ->whereNull('return_date')
                    ->first();
    
                    if($rental_agreeement_parent)
                    {
                        if(!$rental_agreeement_parent->deleted_at && !$rental_agreeement_parent->return_date)
                        {
                            RentalAgreementController::doReturnAsset($rental_agreeement_parent->id,$current_time);
                            $array_parents [] = $rental_agreeement_parent->id;
                            $parent = $rental_agreeement_parent->parent_rental_agreement_id;
                        }
                    }else $parent = null;
                }
              
                array_push($array_parents,$rental_agreeement->id);
                $childs = RentalAgreement::whereIn('parent_rental_agreement_id',$array_parents)
                ->whereNull('deleted_at')
                ->whereNull('return_date')
                ->get();
    
                $array_childs = array();
                foreach ($childs as $key => $child) 
                {
                    if(!$child->deleted_at && !$child->return_date)
                    {
                        $has_child = RentalAgreement::where('parent_rental_agreement_id',$child->id)
                        ->whereNull('deleted_at')
                        ->whereNull('return_date')
                        ->exists();
    
                        $parent_rental_agreement_id = $child->id;
    
                        RentalAgreementController::doReturnAsset($child->id,$current_time);
                        
                        while($has_child)
                        {
                            $child_agreeement_parents = RentalAgreement::where('parent_rental_agreement_id',$parent_rental_agreement_id)
                            ->whereNull('deleted_at')
                            ->whereNull('return_date')
                            ->get();
    
                            foreach ($child_agreeement_parents as $key => $child_agreeement_parent) 
                            {
                                if(!$child_agreeement_parent->deleted_at && !$child_agreeement_parent->return_date)
                                {
                                    RentalAgreementController::doReturnAsset($child_agreeement_parent->id,$current_time);
                                    $parent_rental_agreement_id = $child_agreeement_parent->id;
                                    $has_child = RentalAgreement::where('parent_rental_agreement_id',$child_agreeement_parent->id)
                                    ->whereNull('deleted_at')
                                    ->whereNull('return_date')
                                    ->exists();
                                }else $has_child = false;
                            }
                            
                        }
                    }
                }
            }else return response()->json(['message' => 'return failed, this asset has returned'],422);
        }else return response()->json(['message' => 'return failed, please info exim to input bc in first'],422);
        
        
    }

    public function delete(Request $request,$id)
    {
        $current_time = $request->current_time;
        $rental_agreeement = RentalAgreement::find($id);
        if(!$rental_agreeement->deleted_at)
        {
            //kurang pengecekan asset, harus sudah di balikin semua sebelum dihapus.
            $rental_agreeement->deleted_at      = $current_time;
            $rental_agreeement->deleted_user_id = auth::user()->id;
            $rental_agreeement->save();
        }

        return response()->json(200);
    }

    static function doReturnAsset($id,$current_time)
    {
        try
        {
            $rental_agreeement = RentalAgreement::find($id);
            if(!$rental_agreeement->deleted_at && !$rental_agreeement->return_date)
            {
                    $assets = Asset::where([
                        ['no_rent',$rental_agreeement->id],
                        ['status','!=','bapb'],
                    ])
                    ->get();
    
                    foreach ($assets as $key => $asset) 
                    {
                        $asset_movement = AssetMovement::firstorcreate([
                            'asset_id'                      => $asset->id,
                            'status'                        => 'bapb',
                            'from_company_location'         => $asset->last_company_location_id,
                            'from_factory_location'         => $asset->last_factory_location_id,
                            'from_department_location'      => $asset->last_department_location_id,
                            'from_sub_department_location'  => $asset->last_sub_department_location_id,
                            'from_employee_nik'             => auth::user()->nik,
                            'from_employee_name'            => auth::user()->name,
                            'to_company_location'           => $asset->origin_company_location_id,
                            'to_factory_location'           => $asset->origin_factory_location_id,
                            'to_department_location'        => $asset->origin_department_location_id,
                            'to_sub_department_location'    => $asset->origin_sub_department_location_id,
                            'to_area_location'              => 'return asset',
                            'note'                          => 'asset sewa dikembalikan',
                            'rental_agreement_id'           => $asset->no_rent,
                            'movement_date'                 => $current_time,
                        ]);
    
                        $asset->status                          = 'bapb';
                        $asset->last_used_by                    = auth::user()->nik.'-'.auth::user()->name;
                        $asset->last_movement_date              = $current_time;
                        $asset->last_company_location_id        = $asset->origin_company_location_id;
                        $asset->last_factory_location_id        = $asset->origin_factory_location_id;
                        $asset->last_department_location_id     = $asset->origin_department_location_id;
                        $asset->last_sub_department_location_id = $asset->origin_sub_department_location_id;
                        $asset->last_area_location              = 'return asset';
                        $asset->last_asset_movement_id          = $asset_movement->id;
                        $asset->last_user_movement_id           = auth::user()->id;
                        $asset->delete_at                       = $current_time;
                        $asset->delete_user_id                  = Auth::user()->id;
    
                        $asset->save();
                    }
                    
                    $rental_agreeement->return_user_id  = auth::user()->id;
                    $rental_agreeement->return_date     = $current_time;
                    $rental_agreeement->save();
            }
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
        
    }

    static function getBudget(Request $request)
    {
        $sub_department_id = $request->sub_department_id;

        $budgets = Budget::whereNull('deleted_at')
        ->where('sub_department_id',$sub_department_id)
        ->pluck('name','id')
        ->all();

        return response()->json($budgets,200);
    }
}
