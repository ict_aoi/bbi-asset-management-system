<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Excel;
use StdClass;
use Validator;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;


use App\Models\Area;
use App\Models\Barcode;
use App\Models\Factory;

class AreaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');      
    }

    public function index(Request $request)
    {
    	$msg = $request->session()->get('message');
        return view('area.index',compact('msg'));
    }
    
    public function create(Request $request)
    {
        if($request->session()->has('message')) $request->session()->forget('message');
        $factories = Factory::where('company_id',auth::user()->company_id)->whereNull('delete_at')->pluck('name', 'id')->all();
        
        return view('area.create',compact('factories'));
    }

    public function data(Request $request)
    {
    	if(request()->ajax())
        {
            //area_v
            $area_location  = $request->area_location;
            $data =  DB::table('area_v');

            if($area_location == '1') $data = $data->where('is_area_stock',true);
            else if($area_location == '2') $data = $data->where('is_area_stock',false);
            

            return datatables()->of($data)
            ->editColumn('name',function($data){
            	return ucwords($data->name);
            })
            ->editColumn('factory_name',function($data){
            	return ucwords($data->factory_name);
            })
            ->editColumn('description',function($data){
                return ucwords($data->description);
            })
            ->editColumn('is_area_stock',function($data){
                if ($data->is_area_stock) return '<div class="checker border-info-600 text-info-800">
                    <span class="checked"><input type="checkbox" class="control-info" checked="checked"></span>
                </div>';
                else return null;
            })
            ->addColumn('action', function($data) {
                return view('area._action', [
                    'model'         => $data,
                    'edit'          => route('area.edit',$data->id),
                    'delete'        => route('area.destroy',$data->id),
                    'barcode'       => route('area.barcode',$data->id)
                ]);
            })
            ->rawColumns(['action','is_area_stock'])
            ->make(true);
        }
    }

    public function store(Request $request)
    {
    	$this->validate($request,[
            'factory'   =>'required',
    		'name'      =>'required',

        ]);
        
        if (Area::where([
            ['name',strtolower($request->code)],
            ['factory_id',$request->factory],
            ['is_area_stock',($request->has('is_area_stock'))? true : false],
        ])
        ->whereNull('delete_at')
        ->exists()) 
        {
    		return response()->json(['message'=>'Nama sudah ada, silahkan cari nama code lain'],422);
        }
        
        try 
        {
    		DB::beginTransaction();
    		Area::firstorCreate([
                'name'              => strtolower($request->name),
				'factory_id'        => $request->factory,
				'description'       => strtolower($request->description),
                'create_user_id'    => Auth::user()->id,
                'is_area_stock'     => ($request->has('is_area_stock'))? true : false,
				'created_at'        => $request->current_time,
				'updated_at'        => $request->current_time,
    		]);
            DB::commit();
            
    	} catch (Exception $e) {
    		DB::rollBack();
    		$message = $e->getMessage();
    		ErrorHandler::db($message);
        }
        
        $request->session()->flash('message', 'success');
        return response()->json('success',200);

    }

    public function edit(Request $request,$id)
    {
        if($request->session()->has('message')) $request->session()->forget('message');
        $factories = Factory::where('company_id',auth::user()->company_id)->whereNull('delete_at')->pluck('name', 'id')->all();
        $area      = Area::find($id);
        
        return view('area.edit',compact('factories','area'));
    }

    public function update(Request $request,$id)
    {
        $this->validate($request,[
            'factory'   =>'required',
    		'name'      =>'required',

        ]);
        
        if (Area::where([
            ['name',strtolower($request->code)],
            ['factory_id',$request->factory],
            ['id','!=',$id],
        ])
        ->whereNull('delete_at')
        ->exists()) 
        {
    		return response()->json(['message'=>'Nama sudah ada, silahkan cari nama code lain'],422);
        }

        try 
        {
    		DB::beginTransaction();
            
            $area                   = Area::findorFail($id);
            $area->updated_at       = $request->current_time;
            $area->name             = strtolower($request->name);
            $area->description      = strtolower($request->description);
            $area->is_area_stock    = ($request->has('is_area_stock'))? true : false;
            $area->save();
            
            DB::commit();
        } catch (Exception $e) {
    		DB::rollBack();
    		$message = $e->getMessage();
    		ErrorHandler::db($message);
        }

        $request->session()->flash('message', 'success_2');
        return response()->json('success',200);
        
    }

    public function barcode($id)
    {
        $areas = Area::where('id',$id)->get();;

        foreach ($areas as $key => $area) 
        {
            if($area)
            {
                if(!$area->code)
                {
                    $area->code = $this->randomCode();
                    $area->save();
                }
                
            }
        }
        

        return view('area.barcode',compact('areas'));
    }

    public function printBarcode(Request $request)
    {
        $list_barcodes  = json_decode($request->list_barcodes);
        $areas          = Area::whereIn('id',$list_barcodes)->get();;

        foreach ($areas as $key => $area) 
        {
            if($area)
            {
                if(!$area->code)
                {
                    $area->code = $this->randomCode();
                    $area->save();
                }
            }
        }
        

        return view('area.barcode',compact('areas'));
    }

    public function printAllBarcode()
    {
        $areas = Area::orderBy('factory_id','asc')
        ->orderby('created_at','desc')
        ->get();

        foreach ($areas as $key => $area) 
        {
            if($area)
            {
                if(!$area->code)
                {
                    $area->code = $this->randomCode();
                    $area->save();
                }
                
            }
        }
        

        return view('area.barcode',compact('areas'));
    }

    public function destroy($id)
    {
        $factory                 = Area::findorFail($id);
        $factory->delete_at      = carbon::now();
        $factory->delete_user_id = Auth::user()->id;
        $factory->save();

        return response()->json('success',200);
    }

    public function import(Request $request)
    {
        if($request->session()->has('message')) $request->session()->forget('message');
        $factories = Factory::where('company_id',auth::user()->company_id)->whereNull('delete_at')->pluck('name', 'id')->all();
        
        if(auth::user()->is_super_admin)
        {
            $company_id         = auth::user()->company_id;
            $factory_id         = null;
            $department_id      = null;
        }else
        {
            $company_id         = auth::user()->factory->company->id;
            $factory_id         = auth::user()->factory_id;
            $department_id      = auth::user()->department_id;
        } 


        return view('area.import',compact('factories','factory_id','department_name'));
    }

    public function exportFormImport(Request $request)
    {
       
        $factory_id = $request->factory;
       
        return Excel::create('upload_area',function ($excel) use($factory_id) 
        {
            $excel->sheet('active', function($sheet) use($factory_id) 
            {
                $sheet->setCellValue('A1','factory_id');
                $sheet->setCellValue('B1','nama_area');
                $sheet->setCellValue('C1','deskripsi');

                $sheet->cell('A1', function($cell) 
                {
                    $cell->setBackground('#000000');
                    $cell->setFontColor('#ffffff');
                });

               
                $sheet->cell('B1', function($cell) 
                {
                    $cell->setBackground('#ff4d4d');
                    $cell->setFontColor('#ffffff');
                });
                
                $j = 2;
                for ($i=0; $i < 100; $i++) 
                { 
                    $sheet->setCellValue('A'.$j,$factory_id);
                    
                    $sheet->cell('A'.$j, function($cell) 
                    {
                        $cell->setBackground('#000000');
                        $cell->setFontColor('#ffffff');
                    });

                    $sheet->cell('B'.$j, function($cell) 
                    {
                        $cell->setBackground('#ff4d4d');
                        $cell->setFontColor('#ffffff');
                    });
                    
                    $j++;
                }

                $sheet->setColumnFormat(array(
                    'C' => '@'
                ));
            });

        })->export('xlsx');

    }
    
    public function uploadFormImport(Request $request)
    {  
        $array = array();
        if($request->hasFile('upload_file')){
            $validator = Validator::make($request->all(), [
                'upload_file' => 'required|mimes:xls'
            ]);

            $current_time   = $request->current_time;
            $path           = $request->file('upload_file')->getRealPath();
            $data           = Excel::selectSheets('active')->load($path,function($render){})->get();

            if(!empty($data) && $data->count())
            {
                foreach ($data as $key => $value) 
                {
                    $factory_id             = $value->factory_id;
                    $nama_area              = strtolower($value->nama_area);
                    $description            = strtolower($value->deskripsi);
                    $_factory               = Factory::find($factory_id);
                    
                    if($factory_id && $nama_area && $_factory)
                    {
                        if (Area::where([
                            [db::raw('lower(name)'), $nama_area],
                            ['factory_id', $factory_id]
                        ])
                        ->whereNull('delete_at')
                        ->exists()) 
                        {
                            $obj                        = new stdClass();
                            $obj->factory_name          = $_factory->name;
                            $obj->name                  = $nama_area;
                            $obj->description           = $description;
                            $obj->is_error              = true;
                            $obj->system_log            = 'GAGAL, NAMA AREA SUDAH ADA';
                            $array []                   = $obj;
                        }else
                        {
                            try 
                            {
                                DB::beginTransaction();
                                Area::firstorCreate([
                                    'name'              => $nama_area,
                                    'description'       => $description,
                                    'factory_id'        => $factory_id,
                                    'create_user_id'    => Auth::user()->id,
                                    'created_at'        => $current_time,
                                    'updated_at'        => $current_time,
                                ]);
                                DB::commit();

                                $obj                        = new stdClass();
                                $obj->factory_name          = $_factory->name;
                                $obj->name                  = $nama_area;
                                $obj->description           = $description;
                                $obj->is_error              = false;
                                $obj->system_log            = 'BERHASIL, DATA SUDAH MASUK KEDALAM DATABASE';
                                $array []                   = $obj;
                            } catch (Exception $e) 
                            {
                                DB::rollBack();
                                $message = $e->getMessage();
                                ErrorHandler::db($message);
                            }
                        }
                    }
                 }   
                return response()->json($array,200);
            }else
            {
                return response()->json(['message' => 'import gagal, silahkan cek file anda'],422);
            }


        }
    }

    static function randomCode()
    {
        $referral_code = 'R'.Carbon::now()->format('u');
        $sequence = Barcode::where('referral_code',$referral_code)->max('sequence');

        if ($sequence == null) $sequence = 1;
        else $sequence += 1;
        
        Barcode::FirstOrCreate([
            'barcode'       => $referral_code.$sequence,
            'referral_code' => $referral_code,
            'sequence'      => $sequence,
        ]);

        return $referral_code.$sequence;
    }
}
