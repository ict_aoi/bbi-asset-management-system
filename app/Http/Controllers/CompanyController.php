<?php namespace App\Http\Controllers;

use DB;
use Auth;
use StdClass;
use Validator;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\Company;

class CompanyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $msg = $request->session()->get('message');
        return view('company.index',compact('msg'));
    }

    public function data()
    {
    	if(request()->ajax())
        {
            $data = Company::where('id',auth::user()->company_id)->whereNull('delete_at')->orderby('delete_at','desc');
            
            return datatables()->of($data)
            ->editColumn('code',function($data){
            	return strtoupper($data->code);
            })
            ->editColumn('name',function($data){
            	return ucwords($data->name);
            })
            ->editColumn('description',function($data){
                return ucwords($data->description);
            })
            ->addColumn('action', function($data) {
                if ($data->delete_at==NULL) {
                    return view('company._action', [
                        'model' => $data,
                        'edit' => route('company.edit',$data->id),
                        'delete' => route('company.destroy',$data->id),
                    ]);
                }
            })
            ->rawColumns(['delete_at','action'])
            ->make(true);
        }
    }

    public function create(Request $request)
    {
        if($request->session()->has('message')) $request->session()->forget('message');
        return view('company.create');
    }

    public function store(Request $request)
    {
    	$this->validate($request,[
    		'code'=>'required',
    		'name'=>'required'
    	]);

        if (Company::where('code',strtolower($request->code))
        ->whereNull('delete_at')
        ->exists())
        {
    		return response()->json(['message'=>'Kode perusahaan sudah ada, silahkan cari Kode perusahaan lain'],422);
        }

        if (Company::where('name',strtolower($request->name))
        ->whereNull('delete_at')
        ->exists())
        {
    		return response()->json(['message'=>'Nama perusahaan sudah ada, silahkan cari nama perusahaan lain'],422);
        }

        try
        {
    		DB::beginTransaction();
    		Company::firstorCreate([
				'code'        => strtolower($request->code),
				'name'        => strtolower($request->name),
				'description' =>strtolower($request->description),
		    ]);
            
            DB::commit();
            $request->session()->flash('message', 'success');
    		return response()->json('success',200);

        } catch (Exception $e)
        {
    		DB::rollBack();
    		$message = $e->getMessage();
    		ErrorHandler::db($message);
    	}

    }
    public function edit(Request $request,$id)
    {
        if($request->session()->has('message')) $request->session()->forget('message');
        $company = Company::findorFail($id);
        return view('company.edit',compact('company'));
    }
    public function update(Request $request,$id)
    {

        $this->validate($request,[
            'name'=>'required|min:3'
        ]);
        if (Company::where('name',strtolower($request->name))->where('id','!=',$id)->exists()) {
            return response()->json(['message'=>'Nama Company sudah ada, silahkan cari nama company lain'],422);
        }
        $company              = Company::findorFail($id);
        $company->code        = strtolower($request->code);
        $company->name        = strtolower($request->name);
        $company->description = strtolower($request->description);
        $company->save();

        $request->session()->flash('message', 'success');
        return response()->json('success',200);
    }

    public function destroy($id)
    {
        $company                 = Company::findorFail($id);
        $company->delete_at      = carbon::now();
        $company->save();

        return response()->json('success',200);
    }
}
