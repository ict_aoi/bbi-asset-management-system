<?php namespace App\Http\Controllers;

use DB;
use Auth;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\User;
use App\Models\Asset;
use App\Models\Company;
use App\Models\Factory;
use App\Models\Category;
use App\Models\AssetType;
use App\Models\SubDepartment;
use App\Models\DesignTable;
use App\Models\AssetInformationField;

use App\Models\Erp\CategoryAsset;

class AssetTypeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $msg                = $request->session()->get('message');
        $sub_departments    = SubDepartment::where('is_other',false)
        ->whereIn('code',['ict','elk','ga','me'])
        ->whereNull('delete_at')
        ->pluck('name', 'id')
        ->all();
        
        if(auth::user()->is_super_admin)
        {
            $company_id         = auth::user()->company_id;
            $factory_id         = null;
            $department_id      = null;
            $sub_department_id  = null;
        }else
        {
            $company_id         = auth::user()->company_id;
            $factory_id         = auth::user()->factory_id;
            $department_id      = auth::user()->department_id;
            $sub_department_id  = auth::user()->sub_department_id;
        } 

        return view('asset_type.index',compact('msg','company_id','factory_id','department_id','sub_department_id','sub_departments'));
    }

    public function data(Request $request)
    {
        if(request()->ajax()) 
        {
            $sub_department  = $request->sub_department;
            $is_consumable   = $request->is_consumable;
        
            if(auth::user()->is_super_admin)
            {
                $data = DB::table('asset_type_v')
                ->where([
                    ['company_id',auth::user()->company_id],
                    ['sub_department_id','like',"%$sub_department%"],
                ]);
            }else
            {
                $data = DB::table('asset_type_v')
                ->where([
                    ['company_id',auth::user()->company_id],
                    ['sub_department_id',auth::user()->sub_department_id],
                ]);
            }
            
            if($is_consumable == 1) $data = $data->where('is_consumable',true);
            elseif($is_consumable == 2) $data = $data->where('is_consumable',false);

            return datatables()->of($data)
            ->addColumn('action', function($data) {
                return view('asset_type._action', [
                    'model' => $data,
                    'edit' => route('assetType.edit',$data->id),
                    'delete' => route('assetType.destroy',$data->id),
                ]);
            })
            ->editColumn('department_name',function($data){
            	return ucwords($data->department_name);
            })
            ->editColumn('name',function($data){
            	return ucwords($data->name);
            })
            ->editColumn('description',function($data){
            	return ucfirst($data->description);
            })
            ->editColumn('is_consumable',function($data){
            	if ($data->is_consumable) return '<div class="checker border-info-600 text-info-800">
                    <span class="checked"><input type="checkbox" class="control-info" checked="checked"></span>
                </div>';
            	else return null;
            })
            ->rawColumns(['is_consumable','action'])
            ->make(true);
        }
    }

    public function create(Request $request)
    {
        if($request->session()->has('message')) $request->session()->forget('message');

        $erp_catogories = CategoryAsset::pluck('category_name', 'm_product_category_id')->all();
        $subdepartments = SubDepartment::where('is_other',false)
        ->whereIn('code',['ict','elk','ga','me'])
        ->whereNull('delete_at')
        ->pluck('name', 'id')
        ->all();

        $custom_fields  = DesignTable::where([
            ['table_name','assets'],
            ['is_column_custom',true],
        ])
        ->pluck('alias', 'column_name')
        ->all();

        $design_tables  = DesignTable::where('table_name','assets')
        ->whereNotIn('column_name',[
            'id'
            ,'referral_code'
            ,'qty_used'
            ,'sequence'
            ,'qty_used'
            ,'qty_available'
            ,'last_used_by'
            ,'created_at'
            ,'updated_at'
            ,'delete_at'
            ,'create_user_id'
            ,'delete_user_id'
        ])
        ->where('is_column_custom',false)
        ->get();

        $mappings = array();
        foreach ($design_tables as $key => $design_table) 
        {
            $obj                        = new stdClass;
            $obj->custom_field_id       = $design_table->column_name;
            $obj->custom_label          = $design_table->alias;
            $obj->custom_type_id        = $design_table->type_data;
            $obj->custom_type_name      = $design_table->type_data;
            $obj->custom_lookup_id      = null;
            $obj->custom_default_value  = null;
            $obj->is_required           = false;
            $obj->is_edited             = false;
            $obj->is_custom             = false;
            $mappings []                = $obj;
        }

        return view('asset_type.create',compact('subdepartments','erp_catogories','mappings','custom_fields','companies','company_id','department_name'));
    }
   
    public function store(Request $request)
    {
        $this->validate($request,[
    		'code'          =>'required',
    		'erp_catogories'=>'required',
    		'sub_department'=>'required',
    		'name'          =>'required',
    	]);

        $sub_department = SubDepartment::find($request->sub_department);
        if(!$sub_department) return response()->json('Sub Departemen belum ada, silahkan sync terlebih dahulu.', 422);

        if (AssetType::where([
            ['name', strtolower($request->name)],
            ['company_id', auth::user()->company_id],
            ['erp_category_id', $request->erp_catogories],
            ['department_id', $sub_department->department_id],
            ['sub_department_id', $sub_department->id],
        ])
        ->whereNull('delete_at')
        ->exists()) 
            return response()->json(['message'=>'Nama Tipe asset sudah ada, silahkan cari nama lain'],422);

        if (AssetType::where([
            ['code', strtolower($request->code)],
            ['company_id', auth::user()->company_id],
            ['erp_category_id', $request->erp_catogories],
            ['department_id', $sub_department->department_id],
            ['sub_department_id', $sub_department->id],
        ])
        ->whereNull('delete_at')
        ->exists()) 
            return response()->json(['message'=>'Kode Tipe asset sudah ada, silahkan cari kode lain'],422);
        
        try 
        {
            DB::beginTransaction();
            $erp_category   = CategoryAsset::where('m_product_category_id',$request->erp_catogories)->first();
    		$asset_type     = AssetType::firstorCreate([
				'company_id'            => auth::user()->company_id,
				'department_id'         => $sub_department->department_id,
				'sub_department_id'     => $sub_department->id,
				'code'                  => strtolower($request->code),
				'is_consumable'         => ($request->has('is_consumable'))? true : false,
				'name'                  => strtolower($request->name),
				'description'           => strtolower($request->description),
                'create_user_id'        => Auth::user()->id,
                'delete_at'             => null,
                'erp_category_id'       => $erp_category->m_product_category_id,
                'erp_category_name'     => $erp_category->category_name,
                'erp_asset_group_id'    => $erp_category->a_asset_group_id,
                'erp_asset_group_name'  => $erp_category->asset_group_name,
                'created_at'            => $request->current_time,
                'updated_at'            => $request->current_time,
            ]);
            
            $mappings = json_decode($request->mappings);
            foreach ($mappings as $key => $mapping) 
            {
                AssetInformationField::firstorCreate([
                    'asset_type_id'     => $asset_type->id,
                    'type'              => strtolower($mapping->custom_type_id),
                    'name'              => strtolower($mapping->custom_field_id),
                    'label'             => strtolower($mapping->custom_label),
                    'value'             => strtolower($mapping->custom_default_value),
                    'required'          => $mapping->is_required,
                    'is_custom'         => $mapping->is_custom,
                    'lookup_id'         => $mapping->custom_lookup_id,
                    'created_at'        => $asset_type->created_at,
                    'updated_at'        => $asset_type->updated_at,
                    'create_user_id'    => Auth::user()->id
                ]);
            }

            DB::commit();
            $request->session()->flash('message', 'success');
    		
	    	
        } catch (Exception $e) 
        {
    		DB::rollBack();
    		$message = $e->getMessage();
    		ErrorHandler::db($message);
        }
        
        return response()->json('success',200);
    }

    public function storeCustomField(Request $request)
    {
        try 
        {
    		DB::beginTransaction();
    		AssetInformationField::firstorCreate([
                'asset_type_id'     => $request->asset_type_id,
                'type'              => strtolower($request->custom_type_id),
                'name'              => strtolower($request->custom_field_id),
                'label'             => strtolower($request->custom_label),
                'value'             => strtolower($request->custom_default_value),
                'required'          => $request->is_required,
                'lookup_id'         => $request->custom_lookup_id,
                'is_custom'         => true,
                'create_user_id'    => Auth::user()->id,
                'created_at'        => carbon::now(),
                'updated_at'        => carbon::now(),
            ]);

    		DB::commit();
    		return response()->json('success',200);
	    	
        } catch (Exception $e) 
        {
    		DB::rollBack();
    		$message = $e->getMessage();
    		ErrorHandler::db($message);
    	}
    }

    public function edit(Request $request,$id)
    {
        $asset_type     = AssetType::find($id);
        if($request->session()->has('message')) $request->session()->forget('message');

        $erp_catogories     = CategoryAsset::pluck('category_name', 'm_product_category_id')->all();
        $sub_departments    = SubDepartment::where('is_other',false)
        ->whereIn('code',['ict','elk','ga','me'])
        ->whereNull('delete_at')
        ->pluck('name', 'id')
        ->all();

        $custom_fields      = DesignTable::where([
            ['table_name','assets'],
            ['is_column_custom',true],
        ])
        ->pluck('alias', 'column_name')
        ->all();

        $design_tables  = $asset_type->assetInformationFields($asset_type->id,$asset_type->is_consumable);
        $mappings = array();
        foreach ($design_tables as $key => $design_table) 
        {
            $obj                        = new stdClass;
            $obj->custom_field_id       = $design_table->name;
            $obj->custom_label          = $design_table->label;
            $obj->custom_type_id        = $design_table->type;
            $obj->custom_type_name      = $design_table->type;
            $obj->custom_default_value  = $design_table->value;
            $obj->custom_lookup_id      = $design_table->lookup_id;
            $obj->is_required           = $design_table->required;
            $obj->is_edited             = ($design_table->is_custom)? true : false;
            $obj->is_custom             = $design_table->is_custom;
            $mappings []                = $obj;
        }

        return view('asset_type.edit',compact('categories','SubDepartment','mappings','custom_fields','asset_type','erp_catogories','departments'));
    }

    public function dataCustomField(Request $request,$id)
    {
        if(request()->ajax()) 
        {
            $data = AssetInformationField::where('asset_type_id',$id)->orderBy('is_custom','desc')->orderBy('created_at','asc');
            return datatables()->of($data)
            ->editColumn('required',function($data){
            	if ($data->required) return '<div class="checker border-info-600 text-info-800">
                    <span class="checked"><input type="checkbox" class="control-info" checked="checked"></span>
                </div>';
            	else return null;
            })
            ->addColumn('action', function($data)use($id){
                if($data->is_custom)
                {
                    return view('asset_type._action_modal', [
                        'model' => $data,
                        'edit_modal' => route('assetType.editCustomField',$data->id),
                    ]);
                }
            })
            ->setRowAttr([
                'style' => function($data) {
                    if($data->is_custom == false) return  'background-color: #666666;color:#fff;';
                },
            ])
            ->rawColumns(['required','action','style'])
            ->make(true);
        }
        
    }

    public function editCustomField(Request $request, $id)
    {
        $asset_information_field = AssetInformationField::find($id);
        
        $obj                = new StdClass();
        $obj->id            = $id;
        $obj->lookup_id     = $asset_information_field->lookup_id;
        $obj->name          = $asset_information_field->name;
		$obj->label         = $asset_information_field->label;
		$obj->type          = $asset_information_field->type;
		$obj->value         = $asset_information_field->value;
		$obj->required      = $asset_information_field->required;
		$obj->url_update    = route('assetType.updateCustomField',$asset_information_field->id);
		
		return response()->json($obj,200);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
    		'code'          =>'required',
    		'sub_department'=>'required',
    		'name'          =>'required',
    	]);

        if (AssetType::where([
            ['id','!=',$id],
            ['name', strtolower($request->name)],
            ['company_id', auth::user()->company_id],
            ['erp_category_id', $request->erp_catogories],
            ['department_id', $request->department],
            ['sub_department_id', $request->sub_department],
        ])
        ->whereNull('delete_at')
        ->exists()) 
            return response()->json(['message'=>'Nama Tipe asset sudah ada, silahkan cari nama lain'],422);

        if (AssetType::where([
            ['id','!=',$id],
            ['code', strtolower($request->code)],
            ['company_id', auth::user()->company_id],
            ['erp_category_id', $request->erp_catogories],
            ['department_id', $request->department],
            ['sub_department_id', $request->sub_department],
        ])
        ->whereNull('delete_at')
        ->exists()) 
            return response()->json(['message'=>'Kode Tipe asset sudah ada, silahkan cari kode lain'],422);
        

        try 
        {
            DB::beginTransaction();
            
    		$asset_type                     = AssetType::find($id);
            $asset_type->updated_at         = $request->current_time;
            $asset_type->name               = strtolower($request->name);
            $asset_type->description        = strtolower($request->description);
            
            if(!$asset_type->erp_category_id)
            {
                $erp_category                       = CategoryAsset::where('m_product_category_id',$request->erp_catogories)->first();
                $asset_type->erp_category_id        = $erp_category->m_product_category_id;
                $asset_type->erp_category_name      = $erp_category->category_name;
                $asset_type->erp_asset_group_id     = $erp_category->a_asset_group_id;
                $asset_type->erp_asset_group_name   = $erp_category->asset_group_name;
            }

            $asset_type->save();

            DB::commit();
            $request->session()->flash('message', 'success_2');
    		return response()->json('success',200);
	    	
        } catch (Exception $e) 
        {
    		DB::rollBack();
    		$message = $e->getMessage();
    		ErrorHandler::db($message);
    	}
    }

    public function updateCustomField(Request $request, $id)
    {
        $asset_information_field            = AssetInformationField::find($id);
        $asset_information_field->name      = $request->custom_field_id;
        $asset_information_field->label     = $request->custom_label;
        $asset_information_field->type      = $request->custom_type_id;
        $asset_information_field->value     = $request->custom_default_value;
        $asset_information_field->lookup_id = $request->custom_lookup_id;
        $asset_information_field->required  = $request->is_required;
        $asset_information_field->save();
        return response()->json(200);

    }

    public function destroy($id)
    {
        $count = Asset::where('asset_type_id',$id)->count();
        if($count == 0)
        {
            $asset_type                 = AssetType::findorFail($id);
            $asset_type->delete();
        }else
        {
            $asset_type                 = AssetType::findorFail($id);
            $asset_type->delete_at      = carbon::now();
            $asset_type->delete_user_id = Auth::user()->id;
            $asset_type->save();
        }

        return response()->json('success',200);
    }

    public function getConsumbaleAssetType(Request $request)
    {
        $asset_type_id      = $request->asset_type_id;
        $company_id         = auth::user()->company_id;
        $department_id      = $request->department_id;
        $q                  = strtolower(trim($request->q));

        $lists              = AssetType::whereNull('delete_at')
        ->where([
            ['company_id',$company_id],
            ['department_id',$department_id],
            ['is_consumable',true],
            ['id','!=',$asset_type_id]
        ])
        ->where(function($query) use($q){
            $query->where('name','like',"%$q%");
        })
        ->paginate(10);
        
        return view('asset_type._lov_asset_type',compact('lists'));
    }

    public function getAvailableColumn(Request $request)
    {
        $is_consumable = $request->is_consumable;

        if($is_consumable == 'true')
        {
            $design_tables  = DesignTable::where('table_name','consumable_assets')
            ->whereNotIn('column_name',['id'
            ,'barcode'
            ,'no_inventory_manual'
            ,'no_inventory'
            ,'referral_code'
            ,'qty_used'
            ,'sequence'
            ,'qty_used'
            ,'qty_available'
            ,'created_at'
            ,'updated_at'
            ,'delete_at'
            ,'create_user_id'
            ,'delete_user_id'])
            ->where('is_column_custom',false)
            ->orderby('created_at')
            ->get();
        }else{
            $design_tables  = DesignTable::where('table_name','assets')
            ->whereNotIn('column_name',['id'
            ,'referral_code'
            ,'qty_used'
            ,'sequence'
            ,'qty_used'
            ,'qty_available'
            ,'last_used_by'
            ,'created_at'
            ,'updated_at'
            ,'delete_at'
            ,'create_user_id'
            ,'delete_user_id'])
            ->where('is_column_custom',false)
            ->orderby('created_at')
            ->get();
        }

        $mappings = array();
        foreach ($design_tables as $key => $design_table) 
        {
            $obj                    = new stdClass;
            $obj->custom_field_id       = $design_table->column_name;
            $obj->custom_label          = $design_table->alias;
            $obj->custom_type_id        = $design_table->type_data;
            $obj->custom_type_name      = $design_table->type_data;
            $obj->custom_lookup_id      = null;
            $obj->custom_default_value  = null;
            $obj->is_required           = false;
            $obj->is_edited             = false;
            $obj->is_custom             = false;
            $mappings [] = $obj;
        }

        return response()->json($mappings);
    }
   
}
