<?php namespace App\Http\Controllers;

use DB;
use Auth;
use File;
use Excel;
use Config;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Session;
use Intervention\Image\ImageManagerStatic as Image;

use App\Models\Area;
use App\Models\User;
use App\Models\Asset;
use App\Models\Status;
use App\Models\Company;
use App\Models\Factory;
use App\Models\Barcode;
use App\Models\AssetType;
use App\Models\Temporary;
use App\Models\Department;
use App\Models\RentalAgreement;
use App\Models\SubDepartment;
use App\Models\Manufacture;
use App\Models\AssetMovement;
use App\Models\ConsumableAsset;
use App\Models\ConsumableAssetDetail;
use App\Models\AssetInformationField;
use App\Models\ConsumableAssetMovement;

use App\Models\Erp\AssetGroupDev;
use App\Models\Erp\AssetGroupLive;
use App\Models\Erp\ItemIndirect;
use App\Models\Erp\AssetReceived;
use App\Models\Erp\Asset as ERPAsset;

class AssetController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $msg                = $request->session()->get('message');
        $companies          = Company::where('id',auth::user()->company_id)->whereNull('delete_at')->pluck('name', 'id')->all();
        $factories          = Factory::where('company_id',auth::user()->company_id)->whereNull('delete_at')->pluck('name', 'id')->all();
        $sub_departments    = SubDepartment::where('is_other',false)
        ->whereIn('code',['ict','elk','ga','me'])
        ->whereNull('delete_at')
        ->pluck('name', 'id')
        ->all();
        
        if(auth::user()->is_super_admin)
        {
            $company_id         = auth::user()->company_id;
            $factory_id         = null;
            $department_id      = null;
            $sub_department_id  = null;
            $asset_types        = [];
            $status             = [];
        }else
        {
            $company_id         = auth::user()->company_id;
            $factory_id         = auth::user()->factory_id;
            $department_id      = auth::user()->department_id;
            $sub_department_id  = auth::user()->sub_department_id;
            $asset_types = AssetType::whereNull('delete_at')
            ->where([
                ['sub_department_id',$sub_department_id],
                ['company_id',$company_id],
                ['is_consumable',false],
            ])
            ->pluck('name','id')
            ->all();

            $status         = Asset::select('status')
            ->where('origin_company_location_id',$company_id)
            ->where('origin_sub_department_location_id',$sub_department_id)
            ->where('origin_factory_location_id',$factory_id)
            ->groupby('status')
            ->pluck('status','status')
            ->all();
        } 

        return view('asset.index',compact('msg','companies','status','asset_types','company_id','factory_id','department_id','sub_department_id','factories','sub_departments'));
    }

    public function data(Request $request)
    {
        if(request()->ajax()) 
        {
            $company_id         = $request->company;
            $factory_id         = $request->factory;
            $sub_department_id  = $request->sub_department;
            $asset_type_id      = $request->asset_type;
            $status             = $request->status;
            $is_rent            = $request->is_rent;
            
            $data = DB::table('asset_v')
            ->where(function($query) use($company_id)
            {
                $query = $query->Where('origin_company_location_id','LIKE',"%$company_id%")
                ->OrWhere('last_company_location_id','LIKE',"%$company_id%");
            })
            ->where(function($query) use($factory_id)
            {
                $query = $query->Where('origin_factory_location_id','LIKE',"%$factory_id%")
                ->oRWhere('last_factory_location_id','LIKE',"%$factory_id%");
            })
            ->where(function($query) use($sub_department_id)
            {
                $query = $query->Where('origin_sub_department_location_id','LIKE',"%$sub_department_id%");
            })
            ->where(function($query) use($is_rent)
            {
                if($is_rent == '1') $query = $query->Where('is_rent',true);
                else if($is_rent == '2') $query = $query->Where('is_rent',false);
            })
            ->where(function($query) use($asset_type_id,$status)
            {
                $query = $query->Where('asset_type_id','LIKE',"%$asset_type_id%");
                if($status)
                {
                    $query = $query->Where('status',"$status");
                }
                
            });

            return datatables()->of($data)
            ->addColumn('action', function($data) {
                return view('asset._action', [
                    'model'    => $data,
                    'edit'     => route('asset.edit',$data->id),
                    'printout' => route('asset.printout',$data->id),
                    'delete'   => route('asset.destroy',$data->id),
                    'history'  => route('asset.history',$data->id),
                ]);
            })
            ->editColumn('last_company_name',function($data){
            	return ucwords($data->last_company_name);
            })
            ->editColumn('last_factory_name',function($data){
            	return ucwords($data->last_factory_name);
            })
            ->editColumn('last_department_name',function($data){
            	return ucwords($data->last_department_name);
            })
            ->editColumn('last_sub_department_name',function($data){
            	return ucwords($data->last_sub_department_name);
            })
            ->editColumn('no_inventory_manual',function($data){
            	return strtoupper($data->no_inventory_manual);
            })
            ->editColumn('origin_factory_name',function($data){
            	return ucwords($data->origin_factory_name);
            })
            ->editColumn('origin_department_name',function($data){
            	return ucwords($data->origin_department_name);
            })
            ->editColumn('origin_sub_department_name',function($data){
            	return ucwords($data->origin_sub_department_name);
            })
            ->editColumn('last_factory_name',function($data){
            	return ucwords($data->last_factory_name);
            })
            ->editColumn('model',function($data){
            	return ucwords($data->model);
            })
            ->editColumn('asset_type',function($data){
            	return ucwords($data->asset_type);
            })
            ->editColumn('status',function($data){
                if($data->status == 'siap digunakan') return '<span class="label label-info">'.ucwords($data->status).'</span>';
                else return '<span class="label label-default">'.ucwords($data->status).'</span>';
            })
            ->rawColumns(['action','status'])
            ->make(true);
        }
    }

    public function create(Request $request)
    {
        if($request->session()->has('message')) $request->session()->forget('message');
        
        $app_env            = Config::get('app.env');
        $companies          = Company::where('id',auth::user()->company_id)->whereNull('delete_at')->pluck('name', 'id')->all();
        $factories          = Factory::whereNull('delete_at')->where('company_id', auth::user()->company_id)->pluck('name', 'id')->all();
        $sub_departments    = SubDepartment::where('is_other',false)
        ->whereIn('code',['ict','elk','ga','me'])
        ->whereNull('delete_at')
        ->pluck('name', 'id')
        ->all();

        if($app_env == 'live')  $asset_groups       = AssetGroupLive::pluck('asset_group_name', 'a_asset_group_id')->all();
        else $asset_groups       = AssetGroupDev::pluck('asset_group_name', 'a_asset_group_id')->all();

        if(auth::user()->is_super_admin)
        {
            $company_id         = auth::user()->company_id;
            $factory_id         = null;
            $department_id      = null;
            $sub_department_id  = null;
            $asset_types        = [];
            $manufactures       = [];
        }else
        {
            $company_id         = auth::user()->company_id;
            $factory_id         = auth::user()->factory_id;
            $department_id      = auth::user()->department_id;
            $sub_department_id  = auth::user()->sub_department_id;

            $asset_types = AssetType::whereNull('delete_at')
            ->where([
                ['sub_department_id',$sub_department_id],
                ['company_id',$company_id],
                ['is_consumable',false],
            ])
            ->pluck('name','id')
            ->all();

            $manufactures = Manufacture::whereNull('delete_at')
            ->where([
                ['sub_department_id',$sub_department_id],
                ['company_id',$company_id],
            ])
            ->pluck('name','id')
            ->all();
        } 

        
        return view('asset.create',compact('companies','manufactures','asset_types','company_id','sub_department_id','factory_id','department_id','factories','sub_departments','asset_groups'));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
    		'factory'       =>'required',
    		'asset_type'    =>'required',
    		'serial_number' =>'required',
    		'sub_department'=>'required',
        ]);

        $asset_store        = Config::get('storage.asset');
        $return_barcode     = array();

        if (!File::exists($asset_store)) File::makeDirectory($asset_store, 0777, true);
        
        $image = null;
        if ($request->hasFile('photo')) 
        {
            if ($request->file('photo')->isValid()) 
            {
                $file           = $request->file('photo');
                $now            = Carbon::now()->format('u');
                $filename       = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                $image          = $filename . '_' . $now . '.' . $file->getClientOriginalExtension();

                //Resize Function
                $image_resize   = Image::make($file->getRealPath());
                $height         = $image_resize->height();
                $width          = $image_resize->width();

                $newWidth       = 200;
                $newHeight      = 200;
                $image_resize->resize($newWidth, $newHeight);
            }
        }

        $asset_type     = AssetType::find($request->asset_type);
        $factory        = Factory::find($request->factory);
        $sub_department = SubDepartment::find($request->sub_department);

        if(!$sub_department) return response()->json(['message' => 'Sub Department is not exists yet, please do sync first'], 422);
        if($asset_type->delete_at) return response()->json(['message' => 'Asset type is not active'],422);
        if($factory->delete_at) return response()->json(['message' => 'Factory is not active'],422);
        if($sub_department->delete_at) return response()->json(['message' => 'Sub Department is not active'],422);
        if(!$sub_department->code) return response()->json(['message' => 'Sub department code is empty. please contact ICT to update it'],422);
        
        $is_exists      = Asset::where([
            ['company_id',auth::user()->company_id],
            ['factory_id',$request->factory],
            ['department_id',$sub_department->department_id],
            ['sub_department_id',$sub_department->sub_department_id],
            ['origin_department_location_id',$sub_department->department_id],
            ['origin_sub_department_location_id',$sub_department->sub_department_id],
            ['asset_type_id',$request->asset_type],
        ])
        ->whereNull('delete_at');

        if($request->no_inventory_manual) $is_exists    = $is_exists->where('no_inventory_manual',strtolower($request->no_inventory_manual));
        if($request->erp_no_asset) $is_exists           = $is_exists->where('erp_no_asset',strtolower($request->erp_no_asset));
        if($request->model) $is_exists                  = $is_exists->where('model',strtolower($request->model));
        if($request->serial_number) $is_exists          = $is_exists->where('serial_number',strtolower($request->serial_number));
        if($request->manufacture) $is_exists            = $is_exists->where('manufacture_id',$request->manufacture);
        if($request->no_rent) $is_exists                = $is_exists->where('no_rent',$request->no_rent);

        $is_exists = $is_exists->exists();

        if($is_exists)
            return response()->json(['message' => 'Data already exists'],422);

        
        $month              = ($request->receiving_date)? Carbon::createFromFormat('d/m/Y', $request->receiving_date)->format('m') : Carbon::now()->format('m');
        $year               = ($request->receiving_date)? Carbon::createFromFormat('d/m/Y', $request->receiving_date)->format('y') : Carbon::now()->format('y');
        
        $asset_group_id     = $request->asset_group_id;
        $check_underscore   = strpos($request->asset_group_name, ",");

        if($check_underscore == false)
        {
            $asset_group_name = $request->asset_group_name;
        }else
        {
            $split = explode('_',$request->asset_group_name);
            $asset_group_name = $split[0];
        }

        $_asset_group_id    = ($asset_group_id != '' ? $asset_group_id : $asset_type->erp_asset_group_id);
        $_asset_group_name  = ($asset_group_name != '' ? $asset_group_name : $asset_type->erp_asset_group_name);
        $_referral_code     = $_asset_group_name.'.'.strtoupper($sub_department->code).'.'.strtoupper($factory->code).'.'.strtoupper($asset_type->code);
        $_no_inventory      = $_asset_group_name.'.'.strtoupper($sub_department->code).'.'.strtoupper($factory->code).'.'.strtoupper($asset_type->code).'.'.$month.'.'.$year;
        $sequence           = Asset::where('referral_code',$_referral_code)->max('sequence');

        if(!$_asset_group_name)
        {
            return response()->json(['message' => 'Code asset group is empthy, Please fill code first'],422);
        }

        if ($sequence == null) $sequence = 1;
        else $sequence                  += 1;

        if($sequence < 10) $sequence_format         = '00'.$sequence;
        else if($sequence < 100) $sequence_format   = '0'.$sequence;
        else $sequence_format                       = $sequence;

        $no_inventory                               = $_no_inventory.'.'.$sequence_format;
        
        try 
        {
            DB::beginTransaction();
            $asset = Asset::firstorCreate([
                'asset_type_id'                     => $request->asset_type,
                'company_id'                        => auth::user()->company_id,
				'factory_id'                        => $request->factory,
                'department_id'                     => $sub_department->department_id,
                'sub_department_id'                 => $sub_department->id,
                'origin_company_location_id'        => auth::user()->company_id,
				'origin_factory_location_id'        => $request->factory,
                'origin_department_location_id'     => $sub_department->department_id,
                'origin_sub_department_location_id' => $sub_department->id,
                'manufacture_id'                    => $request->manufacture,
				'barcode'                           => 'BELUM DIPRINT',
                'serial_number'                     => $request->serial_number,
                'no_inventory'                      => $no_inventory,
                'no_inventory_manual'               => strtolower($request->no_inventory_manual),
                'description'                       => ($request->item_desc ? strtolower(trim($request->item_desc)) : null),
                'erp_no_asset'                      => ($request->erp_no_asset ? strtolower($request->erp_no_asset) : null),
                'erp_asset_id'                      => ($request->erp_no_asset_id ? $request->erp_no_asset_id : null),
                'model'                             => strtolower($request->model),
                'purchase_number'                   => ($request->document_no ? strtolower($request->document_no) : null),
                'supplier_name'                     => ($request->supplier_name ? strtolower($request->supplier_name) : null),
				'receiving_date'                    => ($request->receiving_date ? Carbon::createFromFormat('d/m/Y', $request->receiving_date)->format('Y-m-d') : null),
                'image'                             => $image,
                'erp_asset_group_id'                => $_asset_group_id,
                'erp_asset_group_name'              => $_asset_group_name,
                'erp_item_id'                       => ($request->item_id ? $request->item_id : null),
                'erp_item_code'                     => ($request->item_code ? strtolower($request->item_code) : null),
                'referral_code'                     => $_referral_code,
				'sequence'                          => $sequence,
                'status'                            => 'siap digunakan',
                'last_company_location_id'          => auth::user()->company_id,
				'price'                             => $request->price,
				'last_factory_location_id'          => $request->factory,
				'last_department_location_id'       => $sub_department->department_id,
                'last_sub_department_location_id'   => $sub_department->id,
                'no_rent'                           => ($request->no_rent ? $request->no_rent : null),
				'is_rent'                           => ($request->no_rent ? true :false),
                'create_user_id'                    => Auth::user()->id,
                'delete_at'                         => null,
                'created_at'                        => $request->current_time,
                'updated_at'                        => $request->current_time,
            ]);

            if($asset->erp_no_asset)
            {
                Temporary::firstorCreate([
                    'column_1'  => $asset->id,
                    'column_2'  => 'sync_asset_id_erp',
                ]);
            }

            $asset_movement = AssetMovement::firstorCreate([
                'asset_id'                          => $asset->id,
                'status'                            => 'siap digunakan',
                'from_company_location'             => $asset->company_id,
                'from_factory_location'             => $asset->factory_id,
                'from_department_location'          => $sub_department->department_id,
                'from_sub_department_location'      => $sub_department->id,
                'from_employee_nik'                 => auth::user()->nik,
                'from_employee_name'                => auth::user()->name,
                'movement_date'                     => $request->current_time,
                'created_at'                        => $request->current_time,
                'updated_at'                        => $request->current_time,
                'rental_agreement_id'               => ($asset->no_rent ? $asset->no_rent : null),
                'note'                              => 'asset baru',
            ]);
            
            $asset->last_user_movement_id   = auth::user()->id;
            $asset->last_asset_movement_id  = $asset_movement->id;
            $asset->save();

            $custom_fields  = json_decode($request->custom_fields);
            $array          = array();
            foreach ($custom_fields as $key => $custom_field) 
            {
                $custom_field_name          = $custom_field->name;
                $is_lookup                  = $custom_field->is_lookup;
                $custom_lookup_id           = $custom_field->custom_lookup_id;
                $lookup_id                  = $custom_field->lookup_id;
                $detail_lookup_id           = ($custom_field->detail_lookup_id)? $custom_field->detail_lookup_id : '-1';
                $value                      = ($is_lookup)? $custom_lookup_id.'|'.$lookup_id.'|'.$detail_lookup_id : $custom_field->value;
                $asset->$custom_field_name  = strtolower($value);

                if($is_lookup)
                {
                    if($custom_field->lookup_id)
                    {  
                        $movement_date          = carbon::now();
                        ConsumableAssetMovement::firstorCreate([
                            'asset_id'                          => $asset->id,
                            'consumable_asset_id'               => $custom_field->lookup_id,
                            'consumable_asset_detail_id'        => ($custom_field->detail_lookup_id != '')? $custom_field->detail_lookup_id : null,
                            'status'                            => 'digunakan',
                            'from_company_location'             => $asset->company_id,
                            'from_factory_location'             => $asset->factory_id,
                            'from_department_location'          => $asset->department_id,
                            'from_employee_nik'                 => auth::user()->nik,
                            'from_employee_name'                => auth::user()->name,
                            'to_company_location'               => $asset->company_id,
                            'to_factory_location'               => $asset->factory_id,
                            'to_department_location'            => $asset->department_id,
                            'movement_date'                     => $movement_date,
                            'is_using_in_custom_field'          => true,
                            'system_note'                       => 'digunakan untuk asset, dengan nomor inventori '.$asset->no_inventory.'. dikeluarkan pada tanggal '.$movement_date->format('d/M/Y').' oleh '.auth::user()->name,
                        ]);
    
                        $array [] = $custom_field->lookup_id;
                    }
                   
                }
                
            }

            if(isset($array))
            {
                $detail_consumable_assets = ConsumableAssetMovement::select('consumable_asset_id',db::raw('count(0) as total_qty_used'))
                ->whereIn('consumable_asset_id',$array)
                ->where([
                    ['status','digunakan'],
                    ['is_return',false]
                ])
                ->groupby('consumable_asset_id')
                ->get();

                foreach ($detail_consumable_assets as $key => $detail_consumable_asset) 
                {
                    $consumable_asset                   = ConsumableAsset::find($detail_consumable_asset->consumable_asset_id);
                    $total                              = $consumable_asset->total;
                    $new_available_qty                  = $total - $detail_consumable_asset->total_qty_used;

                    $consumable_asset->qty_used         = $detail_consumable_asset->total_qty_used;
                    $consumable_asset->qty_available    = $new_available_qty;
                    $consumable_asset->save();
                }
            }
            

            if($asset->save())
            {
                if ($request->hasFile('photo')) $image_resize->save($asset_store.'/'.$image);
            } 

            Temporary::firstorCreate([
                'column_1'  => $asset->origin_sub_department_location_id,
                'column_2'  => 'sync_export_asset_per_department',
            ]);

            DB::commit();

            $request->session()->flash('message', 'success');
            $return_barcode [] = $asset->id;
            return response()->json($return_barcode,200);
            
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
        
       
        return response()->json(200);
    }

    public function edit(Request $request,$id)
    {
        if($request->session()->has('message')) $request->session()->forget('message');
        $app_env        = Config::get('app.env');
        $asset          = Asset::find($id);
        $companies      = Company::where('id',$asset->origin_company_location_id)
        ->whereNull('delete_at')
        ->pluck('name', 'id')
        ->all();

        $factories      = Factory::whereNull('delete_at')
        ->where('company_id', $asset->origin_company_location_id)
        ->pluck('name', 'id')
        ->all();

        $sub_departments= SubDepartment::where('is_other',false)
        ->whereIn('code',['ict','elk','ga','me'])
        ->whereNull('delete_at')
        ->pluck('name', 'id')
        ->all();
       
        if($app_env == 'live') $asset_groups   = AssetGroupLive::pluck('asset_group_name', 'a_asset_group_id')->all();
        else $asset_groups   = AssetGroupDev::pluck('asset_group_name', 'a_asset_group_id')->all();
       
       
        $manufactures   = Manufacture::where('department_id',$asset->origin_department_location_id)->pluck('name', 'id')->all();
        $asset_types    = AssetType::where([
            ['department_id',$asset->origin_department_location_id],
            ['company_id',$asset->origin_company_location_id],
        ])->pluck('name', 'id')
        ->all();

        return view('asset.edit',compact('asset','companies','factories','sub_departments','manufactures','asset_types','asset_groups'));
    }

    public function printBarcode(Request $request)
    {
        if($request->list_barcodes)
        {
            $list_barcodes  = json_decode($request->list_barcodes);
            $assets         = Asset::whereIn('id',$list_barcodes)
            ->orderby('sequence','asc')
            ->whereNull('delete_at')
            ->get();
    
            foreach ($assets as $key => $asset) 
            {
                if($asset->barcode == 'BELUM DIPRINT')
                {
                    $asset->barcode = $this->randomCode();
                    $asset->save();
                }
            }
        }else
        {
            $assets         = Asset::where([
                ['origin_factory_location_id',$request->factory],
                ['asset_type_id',$request->asset_type],
            ])
            ->orderby('sequence','asc')
            ->whereNull('delete_at')
            ->get();
    
            foreach ($assets as $key => $asset) 
            {
                if($asset->barcode == 'BELUM DIPRINT')
                {
                    $asset->barcode = $this->randomCode();
                    $asset->save();
                }
            }
        }
        

        return view('asset.barcode',compact('assets'));
        
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
    		'serial_number'=>'required',
    	]);

        $asset_store = Config::get('storage.asset');
        if (!File::exists($asset_store)) File::makeDirectory($asset_store, 0777, true);
        
        $asset_type     = AssetType::find($request->asset_type_id);
        $factory        = Factory::find($request->factory_id);
        $sub_department = SubDepartment::find($request->sub_department_id);

        if(!$sub_department) return response()->json(['message' => 'Sub Department is not exists yet, please do sync first'], 422);
        if($asset_type->delete_at) return response()->json(['message' => 'Asset type is not active'],422);
        if($factory->delete_at) return response()->json(['message' => 'Factory is not active'],422);
        if($sub_department->delete_at) return response()->json(['message' => 'Sub Department is not active'],422);
        if(!$sub_department->code) return response()->json(['message' => 'Sub department code is empty. please contact ICT to update it'],422);
        

        $is_exists      = Asset::where([
            ['id','!=',$id],
            ['company_id',auth::user()->company_id],
            ['factory_id',$request->factory],
            ['department_id',$sub_department->department_id],
            ['sub_department_id',$sub_department->sub_department_id],
            ['origin_department_location_id',$sub_department->department_id],
            ['origin_sub_department_location_id',$sub_department->sub_department_id],
            ['asset_type_id',$asset_type->id],
        ])
        ->whereNull('delete_at');

        if($request->no_inventory_manual) $is_exists    = $is_exists->where('no_inventory_manual',strtolower($request->no_inventory_manual));
        if($request->erp_no_asset) $is_exists           = $is_exists->where('erp_no_asset',strtolower($request->erp_no_asset));
        if($request->model) $is_exists                  = $is_exists->where('model',strtolower($request->model));
        if($request->serial_number) $is_exists          = $is_exists->where('serial_number',strtolower($request->serial_number));
        if($request->manufacture) $is_exists            = $is_exists->where('manufacture_id',$request->manufacture);
        if($request->no_rent) $is_exists                = $is_exists->where('no_rent',$request->no_rent);

        $is_exists = $is_exists->exists();

        if($is_exists) return response()->json(['message' => 'data ini sudah ada'],422);

        $asset      = Asset::find($id);
        $factory    = Factory::find($asset->factory_id);
        $image      = $asset->image;

        if ($request->hasFile('photo')) 
        {
            if ($request->file('photo')->isValid()) 
            {
                $file           = $request->file('photo');
                $now            = Carbon::now()->format('u');
                $filename       = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                $image          = $filename . '_' . $now . '.' . $file->getClientOriginalExtension();

                //Resize Function
                $image_resize   = Image::make($file->getRealPath());
                $height         = $image_resize->height();
                $width          = $image_resize->width();

                $newWidth       = 200;
                $newHeight      = 200;
                $image_resize->resize($newWidth, $newHeight);

                $old_file = $asset_store . '/' . $asset->image;
                if(File::exists($old_file)) File::delete($old_file);
            }
        }

        try 
        {
            DB::beginTransaction();
            /*$month              = ($asset->receiving_date)? $asset->receiving_date->format('m') : Carbon::createFromFormat('Y-m-d H:i:s', $asset->created_at)->format('m');
            $year               = ($asset->receiving_date)? $asset->receiving_date->format('y') : Carbon::createFromFormat('Y-m-d H:i:s', $asset->created_at)->format('y');
            $sequence           = $asset->sequence;
        
          
            $check_underscore   = strpos($request->asset_group_name, ",");

            if($check_underscore == false)
            {
                $asset_group_name = $request->asset_group_name;
            }else
            {
                $split = explode('_',$request->asset_group_name);
                $asset_group_name = $split[0];
            }
            
            $_referral_code     = $asset_group_name.'.'.strtoupper($asset->originDepartment->code).'.'.strtoupper($asset->originFactory->code).'.'.strtoupper($asset->assetType->code);
            $_no_inventory      = $asset_group_name.'.'.strtoupper($asset->originDepartment->code).'.'.strtoupper($asset->originFactory->code).'.'.strtoupper($asset->assetType->code).'.'.$month.'.'.$year;
          
            if($sequence < 10) $sequence_format         = '00'.$sequence;
            else if($sequence < 100) $sequence_format   = '0'.$sequence;
            else $sequence_format                       = $sequence;

            $no_inventory                               = $_no_inventory.'.'.$sequence_format;*/
            
            $asset->updated_at          = $request->current_time;
            $asset->manufacture_id      = $request->manufacture;
            $asset->serial_number       = $request->serial_number;
            $asset->price               = $request->price;
            $asset->no_inventory_manual = strtolower($request->no_inventory_manual);
            $asset->model               = strtolower($request->model);
            //$asset->no_inventory        = $no_inventory;
            //$asset->referral_code       = $_referral_code;
            $asset->image               = $image;
            $asset->description         = ($request->item_desc ? strtolower(trim($request->item_desc)) : null);
            $asset->purchase_number     = ($request->document_no ? strtolower($request->document_no) : null);
            $asset->supplier_name       = ($request->supplier_name ? strtolower($request->supplier_name) : null);
            $asset->erp_no_asset        = ($request->erp_no_asset ? strtolower($request->no_asset) : null);
            $asset->erp_asset_id        = ($request->erp_asset_id ? $request->erp_asset_id : null);
            $asset->receiving_date      = ($request->receiving_date ? Carbon::createFromFormat('d/m/Y', $request->receiving_date)->format('Y-m-d') : null);
            $asset->erp_asset_group_id  = ($request->asset_group_id ? $request->asset_group_id : null);
            $asset->erp_asset_group_name= ($request->asset_group_name ? $request->asset_group_name : null);
            $asset->erp_item_id         = ($request->item_id ? $request->item_id : null);
            $asset->erp_item_code       = ($request->item_code ? $request->item_code : null);
            $asset->no_rent             = ($request->no_rent ? $request->no_rent : null);
            $asset->is_rent             = ($request->no_rent ? true : false);
           
            $custom_fields = json_decode($request->custom_fields);
            foreach ($custom_fields as $key => $custom_field) 
            {
                $custom_field_name          = $custom_field->name;
                $is_lookup                  = $custom_field->is_lookup;
                $custom_lookup_id           = $custom_field->custom_lookup_id;
                $lookup_id                  = $custom_field->lookup_id;
                $detail_lookup_id           = ($custom_field->detail_lookup_id)? $custom_field->detail_lookup_id : '-1';
                $value                      = ($is_lookup)? $custom_lookup_id.'|'.$lookup_id.'|'.$detail_lookup_id : $custom_field->value;
                $asset->$custom_field_name  = strtolower($value);

                if($is_lookup)
                {
                    $movement_date          = carbon::now();
                    $is_exists              = ConsumableAssetMovement::where([
                        ['asset_id',$asset->id],
                        ['is_using_in_custom_field',true],
                    ])
                    ->whereIn('consumable_asset_id',function($query) use($custom_lookup_id)
                    {
                        $query->select('id')
                        ->from('consumable_assets')
                        ->where('asset_type_id',$custom_lookup_id);
                    })
                    ->first();

                    if($is_exists)
                    {
                        if($is_exists->consumable_asset_detail_id)
                        {
                            if($is_exists->consumable_asset_id != $custom_field->lookup_id
                            || $is_exists->consumable_asset_detail_id != $custom_field->detail_lookup_id)
                            {
                                ConsumableAssetMovement::firstorCreate([
                                    'asset_id'                          => $asset->id,
                                    'consumable_asset_id'               => $is_exists->consumable_asset_id,
                                    'consumable_asset_detail_id'        => $is_exists->consumable_asset_detail_id,
                                    'status'                            => 'dikembalikan',
                                    'from_company_location'             => strtolower($is_exists->from_company_location),
                                    'from_factory_location'             => strtolower($is_exists->from_factory_location),
                                    'from_department_location'          => strtolower($is_exists->from_department_location),
                                    'from_employee_nik'                 => strtolower($is_exists->from_employee_nik),
                                    'from_employee_name'                => strtolower($is_exists->from_employee_name),
                                    'movement_date'                     => $movement_date,
                                    'is_return'                         => true,
                                    'is_using_in_custom_field'          => true,
                                    'system_note'                       => 'dikembalikan dari asset, dengan nomor inventori '.$asset->no_inventory.'. dikembalikan pada tanggal '.$movement_date->format('d/M/Y').' oleh '.auth::user()->name,
                                ]);

                                ConsumableAssetMovement::firstorCreate([
                                    'asset_id'                          => $asset->id,
                                    'consumable_asset_id'               => $custom_field->lookup_id,
                                    'consumable_asset_detail_id'        => ($custom_field->detail_lookup_id != '')? $custom_field->detail_lookup_id : null,
                                    'status'                            => 'digunakan',
                                    'from_company_location'             => strtolower($factory->company->name),
                                    'from_factory_location'             => strtolower($factory->name),
                                    'from_department_location'          => strtolower( $asset->department_name),
                                    'from_employee_nik'                 => strtolower(auth::user()->nik),
                                    'from_employee_name'                => strtolower(auth::user()->name),
                                    'movement_date'                     => $movement_date,
                                    'is_using_in_custom_field'          => true,
                                    'system_note'                       => 'digunakan untuk asset, dengan nomor inventori '.$asset->no_inventory.'. dikeluarkan pada tanggal '.$movement_date->format('d/M/Y').' oleh '.auth::user()->name,
                                ]);
                            }
                        }else
                        {
                            if($is_exists->consumable_asset_id != $custom_field->lookup_id)
                            {
                                ConsumableAssetMovement::firstorCreate([
                                    'asset_id'                          => $asset->id,
                                    'consumable_asset_id'               => $is_exists->consumable_asset_id,
                                    'consumable_asset_detail_id'        => $is_exists->consumable_asset_detail_id,
                                    'status'                            => 'dikembalikan',
                                    'from_company_location'             => strtolower($is_exists->from_company_location),
                                    'from_factory_location'             => strtolower($is_exists->from_factory_location),
                                    'from_department_location'          => strtolower($is_exists->from_department_location),
                                    'from_employee_nik'                 => strtolower($is_exists->from_employee_nik),
                                    'from_employee_name'                => strtolower($is_exists->from_employee_name),
                                    'to_company_location'               => strtolower($is_exists->from_company_location),
                                    'to_factory_location'               => strtolower($is_exists->from_factory_location),
                                    'to_department_location'            => strtolower($is_exists->from_department_location),
                                    'movement_date'                     => $movement_date,
                                    'is_using_in_custom_field'          => true,
                                    'system_note'                       => 'dikembalikan dari asset, dengan nomor inventori '.$asset->no_inventory.'. dikembalikan pada tanggal '.$movement_date->format('d/M/Y').' oleh '.auth::user()->name,
                                ]);

                                ConsumableAssetMovement::firstorCreate([
                                    'asset_id'                          => $asset->id,
                                    'consumable_asset_id'               => $custom_field->lookup_id,
                                    'consumable_asset_detail_id'        => ($custom_field->detail_lookup_id != '')? $custom_field->detail_lookup_id : null,
                                    'status'                            => 'digunakan',
                                    'from_company_location'             => strtolower($factory->company->name),
                                    'from_factory_location'             => strtolower($factory->name),
                                    'from_department_location'          => strtolower($asset->department_name),
                                    'from_employee_nik'                 => strtolower(auth::user()->nik),
                                    'from_employee_name'                => strtolower(auth::user()->name),
                                    'to_company_location'               => strtolower($is_exists->from_company_location),
                                    'to_factory_location'               => strtolower($is_exists->from_factory_location),
                                    'to_department_location'            => strtolower($is_exists->from_department_location),
                                    'movement_date'                     => $movement_date,
                                    'is_using_in_custom_field'          => true,
                                    'system_note'                       => 'digunakan untuk asset, dengan nomor inventori '.$asset->no_inventory.'. dikeluarkan pada tanggal '.$movement_date->format('d/M/Y').' oleh '.auth::user()->name,
                                ]);
                            }
                        }
                        
                        
                    }

                    $array [] = $custom_field->lookup_id;
                }
            }

            if(isset($array))
            {
                $detail_consumable_assets = ConsumableAssetMovement::select('consumable_asset_id',db::raw('count(0) as total_qty_used'))
                ->whereIn('consumable_asset_id',$array)
                ->where([
                    ['status','digunakan'],
                    ['is_return',false]
                ])
                ->groupby('consumable_asset_id')
                ->get();

                foreach ($detail_consumable_assets as $key => $detail_consumable_asset) 
                {
                    $consumable_asset                   = ConsumableAsset::find($detail_consumable_asset->consumable_asset_id);
                    $total                              = $consumable_asset->total;
                    $new_available_qty                  = $total - $detail_consumable_asset->total_qty_used;

                    $consumable_asset->qty_used         = $detail_consumable_asset->total_qty_used;
                    $consumable_asset->qty_available    = $new_available_qty;
                    $consumable_asset->save();
                }
            }
            

            if($asset->save())
            {
                if($request->hasFile('photo')) $image_resize->save($asset_store.'/'.$image);
            } 

            Temporary::firstorCreate([
                'column_1'  => $asset->origin_sub_department_location_id,
                'column_2'  => 'sync_export_asset_per_department',
            ]);
            
            DB::commit();

            $request->session()->flash('message', 'success_2');
            return response()->json('success',200);
            
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    public function erpAssetPicklist(Request $request)
    {
        $receiving_date = ($request->receiving_date) ? Carbon::createFromFormat('d/m/Y', $request->receiving_date)->format('Y-m-d') : Carbon::now()->format('Y-m-d');
        $q              = trim(strtolower($request->q));

        $erp_asset_ids  = Asset::select('erp_asset_id')
        ->groupby('erp_asset_id')
        ->whereNotNull('erp_asset_id')
        ->get()
        ->toArray();

        $lists          = AssetReceived::whereBetween('mr_date', [$receiving_date, $receiving_date])
        ->whereNotIn('a_asset_id',$erp_asset_ids)
        ->where(function($query) use($q)
        {
            $query->where(db::raw('lower(inventoryno)'),'like',"%$q%")
            ->orwhere(db::raw('lower(item_code)'),$q,'like',"%$q%")
            ->orwhere(db::raw('lower(item_desc)'),$q,'like',"%$q%")
            ->orwhere(db::raw('lower(po_supplier)'),$q,'like',"%$q%");
        })
        ->paginate(20);

        return view('asset._erp_picklist',compact('lists'));
    }

    public function destroy($id)
    {
        $asset = Asset::find($id);
        $asset->delete_at = carbon::now();
        $asset->delete_user_id = auth::user()->id;
        $asset->save();
    }

    public function import(Request $request)
    {
        if($request->session()->has('message')) $request->session()->forget('message');
        
        $companies          = Company::where('id',auth::user()->company_id)->whereNull('delete_at')->pluck('name', 'id')->all();
        $factories          = Factory::where('company_id',auth::user()->company_id)->whereNull('delete_at')->pluck('name', 'id')->all();
        $sub_departments    = SubDepartment::where('is_other',false)
        ->whereIn('code',['ict','elk','ga','me'])
        ->whereNull('delete_at')
        ->pluck('name', 'id')
        ->all();

        if(auth::user()->is_super_admin)
        {
            $company_id         = null;
            $factory_id         = null;
            $department_id      = null;
            $sub_department_id  = null;
            $asset_types        = [];
        }else
        {
            $company_id         = auth::user()->factory->company->id;
            $factory_id         = auth::user()->factory_id;
            $department_id      = auth::user()->department_id;
            $sub_department_id  = auth::user()->sub_department_id;

            $asset_types = AssetType::whereNull('delete_at')
            ->where([
                ['sub_department_id',$sub_department_id],
                ['company_id',$company_id],
                ['is_consumable',false],
            ])
            ->pluck('name','id')
            ->all();
        } 


        return view('asset.import',compact('companies','company_id','asset_types','sub_department_id','factory_id','department_id','factories','sub_departments'));
    }

    public function exportFormImport(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company'       => 'required',
            'factory'       => 'required',
            'sub_department'=> 'required',
            'asset_type'    => 'required',
        ]);

        $company_id         = $request->company;
        $factory_id         = $request->factory;
        $_sub_department    = $request->sub_department;
        $asset_type_id      = $request->asset_type;
       
        $sub_department = SubDepartment::find($_sub_department);
        $asset_type     = AssetType::find($asset_type_id);
        $asset_type_informations = $asset_type->assetInformationFields($asset_type->id,$asset_type->is_consumable);

        $alphabets = array(
            0 => 'A',
            1 => 'B',
            2 => 'C',
            3 => 'D',
            4 => 'E',
            5 => 'F',
            6 => 'G',
            7 => 'H',
            8 => 'I',
            9 => 'J',
            10 => 'K',
            11 => 'L',
            12 => 'M',
            13 => 'N',
            14 => 'O',
            15 => 'P',
            16 => 'Q',
            17 => 'R',
            18 => 'S',
            19 => 'T',
            20 => 'U',
            21 => 'V',
            22 => 'W',
            23 => 'X',
            24 => 'Y',
            25 => 'Z',
            26 => 'AA',
            27 => 'AB',
            28 => 'AC',
            29 => 'AD',
            30 => 'AE',
            31 => 'AF',
            32 => 'AG',
            33 => 'AH',
            34 => 'AI',
            35 => 'AJ',
            36 => 'AK',
            37 => 'AL',
            38 => 'AM',
            39 => 'AN',
            40 => 'AO',
            41 => 'AP',
            42 => 'AQ',
            43 => 'AR',
            44 => 'AS',
            45 => 'AT',
            46 => 'AU',
            47 => 'AV',
            48 => 'AW',
            49 => 'AX',
            50 => 'AY',
            51 => 'AZ',
            52 => 'BA',
            53 => 'BB',
            54 => 'BC',
            55 => 'BD',
            56 => 'BE',
            57 => 'BF',
            58 => 'BG',
            59 => 'BH',
            60 => 'BI',
            61 => 'BJ',
            62 => 'BK',
            63 => 'BL',
            64 => 'BM',
            65 => 'BN',
            66 => 'BO',
            67 => 'BP',
            68 => 'BQ',
            69 => 'BR',
            70 => 'BS',
            71 => 'BT',
            72 => 'BU',
            73 => 'BV',
            74 => 'BW',
            75 => 'BX',
            76 => 'BY',
            77 => 'BZ',
        );

       
        return Excel::create('upload_asset_'.str_slug($asset_type->name),function ($excel) use($company_id,$factory_id,$sub_department,$asset_type_id,$asset_type_informations,$alphabets) 
        {
            $excel->sheet('active', function($sheet) use($company_id,$factory_id,$sub_department,$asset_type_id,$asset_type_informations,$alphabets) 
            {
                foreach ($asset_type_informations as $key => $asset_type_information) 
                {
                    if($asset_type_information->is_custom) $column_name = $asset_type_information->name.'_'.str_slug($asset_type_information->label,'_');
                    else $column_name = $asset_type_information->name;

                    $sheet->setCellValue($alphabets[$key].'1',$column_name);
                    $j = 2;

                    if(in_array(strtoupper($asset_type_information->name),['COMPANY_ID','ASSET_TYPE_ID','FACTORY_ID','DEPARTMENT_ID','SUB_DEPARTMENT_ID','MANUFACTURE_ID','ERP_ASSET_GROUP_NAME','SERIAL_NUMBER','MODEL','NO_RENT'])
                        || ($asset_type_information->is_custom && $asset_type_information->type == 'option')
                        || ($asset_type_information->is_custom && $asset_type_information->type == 'lookup'))
                    {
                        if(strtoupper($asset_type_information->name) == 'COMPANY_ID') $value = $company_id;
                        elseif(strtoupper($asset_type_information->name) == 'FACTORY_ID') $value = $factory_id;
                        elseif(strtoupper($asset_type_information->name) == 'ASSET_TYPE_ID') $value = $asset_type_id;
                        elseif(strtoupper($asset_type_information->name) == 'DEPARTMENT_ID') $value = $sub_department->department_id;
                        elseif(strtoupper($asset_type_information->name) == 'SUB_DEPARTMENT_ID') $value = $sub_department->id;

                        if(in_array(strtoupper($asset_type_information->name),['COMPANY_ID','ASSET_TYPE_ID','FACTORY_ID','DEPARTMENT_ID','SUB_DEPARTMENT_ID']))
                        {
                            $sheet->cell($alphabets[$key].'1', function($cell) 
                            {
                                $cell->setBackground('#000000');
                                $cell->setFontColor('#ffffff');
                            });
                        }else if(in_array(strtoupper($asset_type_information->name),['SERIAL_NUMBER','MODEL']))
                        {
                            $sheet->cell($alphabets[$key].'1', function($cell) 
                            {
                                $cell->setBackground('#ff4d4d');
                                $cell->setFontColor('#ffffff');
                            });
                        }
                        

                        for ($i=0; $i < 100; $i++) 
                        { 
                            if(in_array(strtoupper($asset_type_information->name),['COMPANY_ID','ASSET_TYPE_ID','FACTORY_ID','DEPARTMENT_ID','SUB_DEPARTMENT_ID']))
                            {
                                $sheet->setCellValue($alphabets[$key].$j,$value);
                                $sheet->cell($alphabets[$key].$j, function($cell) 
                                {
                                    $cell->setBackground('#000000');
                                    $cell->setFontColor('#ffffff');
                                });
                            }else if(in_array(strtoupper($asset_type_information->name),['SERIAL_NUMBER','MODEL']))
                            {
                                $sheet->cell($alphabets[$key].$j, function($cell) 
                                {
                                    $cell->setBackground('#ff4d4d');
                                    $cell->setFontColor('#ffffff');
                                });
                            }
                            else 
                            {
                                if(in_array(strtoupper($asset_type_information->name),['MODEL','SERIAL_NUMBER']))
                                {
                                    $sheet->cell($alphabets[$key].$j, function($cell) 
                                    {
                                        $cell->setBackground('#ff4d4d');
                                        $cell->setFontColor('#ffffff');
                                    });
                                }

                                if($asset_type_information->is_custom && ($asset_type_information->type == 'option' || $asset_type_information->type == 'lookup'))
                                {
                                    $objValidation = $sheet->getCell($alphabets[$key].$j)->getDataValidation();
                                    $objValidation->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
                                    $objValidation->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
                                    $objValidation->setAllowBlank(false);
                                    $objValidation->setShowInputMessage(true);
                                    $objValidation->setShowErrorMessage(true);
                                    $objValidation->setShowDropDown(true);
                                    $objValidation->setErrorTitle('Input error');
                                    $objValidation->setError('Nilai tidak ada dalam list.');
                                    $objValidation->setPromptTitle('Pilih Nilai');
                                    $objValidation->setPrompt('Silahkan input sesuai dengan list.');
                                    $objValidation->setFormula1($asset_type_information->name);
                                }else
                                {

                                    if(strtoupper($asset_type_information->name) == 'MANUFACTURE_ID')
                                    {
                                        $objValidation = $sheet->getCell($alphabets[$key].$j)->getDataValidation();
                                        $objValidation->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
                                        $objValidation->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
                                        $objValidation->setAllowBlank(false);
                                        $objValidation->setShowInputMessage(true);
                                        $objValidation->setShowErrorMessage(true);
                                        $objValidation->setShowDropDown(true);
                                        $objValidation->setErrorTitle('Input error');
                                        $objValidation->setError('Nilai tidak ada dalam list.');
                                        $objValidation->setPromptTitle('Pilih Pabrikan');
                                        $objValidation->setPrompt('Silahkan input sesuai dengan list.');
                                        $objValidation->setFormula1('manufacture_list');
                                    }

                                    if(strtoupper($asset_type_information->name) == 'NO_RENT')
                                    {
                                        $objValidation = $sheet->getCell($alphabets[$key].$j)->getDataValidation();
                                        $objValidation->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
                                        $objValidation->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
                                        $objValidation->setAllowBlank(false);
                                        $objValidation->setShowInputMessage(true);
                                        $objValidation->setShowErrorMessage(true);
                                        $objValidation->setShowDropDown(true);
                                        $objValidation->setErrorTitle('Input error');
                                        $objValidation->setError('Nilai tidak ada dalam list.');
                                        $objValidation->setPromptTitle('Pilih No KK Rent');
                                        $objValidation->setPrompt('Silahkan input sesuai dengan list.');
                                        $objValidation->setFormula1('no_kk_list');
                                    }
                                    
                                    if(strtoupper($asset_type_information->name) == 'ERP_ASSET_GROUP_NAME')
                                    {
                                        $objValidation = $sheet->getCell($alphabets[$key].$j)->getDataValidation();
                                        $objValidation->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
                                        $objValidation->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
                                        $objValidation->setAllowBlank(false);
                                        $objValidation->setShowInputMessage(true);
                                        $objValidation->setShowErrorMessage(true);
                                        $objValidation->setShowDropDown(true);
                                        $objValidation->setErrorTitle('Input error');
                                        $objValidation->setError('Nilai tidak ada dalam list.');
                                        $objValidation->setPromptTitle('Pilih Kelompok Asset');
                                        $objValidation->setPrompt('Silahkan input sesuai dengan list.');
                                        $objValidation->setFormula1('erp_asset_group_list');
                                    }
                                    
                                    
                                }
                                
                                
                            }
                           
                            $j++;
                        }
                    }

                    $sheet->setColumnFormat(array(
                        $alphabets[$key] => '@'
                    ));
                }
            });

            $excel->sheet('list_pabrikan',function($sheet) use ($company_id,$factory_id,$sub_department,$asset_type_informations,$alphabets)
            {
                $variantsSheet = $sheet->_parent->getSheet(1); // Variants Sheet , "0" is the Index of Variants Sheet

                $manufactures = Manufacture::select('name')
                ->whereNull('delete_at')
                ->where([
                    ['sub_department_id',$sub_department->id],
                    ['company_id',$company_id],
                ])
                ->groupby('name')
                ->get();
                $total_manufacture = count($manufactures)+1;
                $sheet->SetCellValue("A1", "LIST_PABRIKAN");
                $j = 2;
                foreach ($manufactures as $key => $manufacture) 
                {
                    $sheet->SetCellValue("A".$j, $manufacture->name);
                    $j++;
                }

                $app_env            = Config::get('app.env');

                if($app_env == 'live')
                {
                    $asset_groups       = AssetGroupLive::select('a_asset_group_id','asset_group_name')
                    ->whereNotNull('asset_group_name')
                    ->whereNotNull('a_asset_group_id')
                    ->get();
                    $total_asset_group = count($asset_groups)+1;
                }else
                {
                    $asset_groups       = AssetGroupDev::select('a_asset_group_id','asset_group_name')
                    ->whereNotNull('asset_group_name')
                    ->whereNotNull('a_asset_group_id')
                    ->get();
                    $total_asset_group = count($asset_groups)+1;
                }
                
            
                $sheet->SetCellValue("B1", "LIST_GROUP_NAME");
                $l = 2;
                foreach ($asset_groups as $key => $asset_group) 
                {
                    $sheet->SetCellValue("B".$l, $asset_group->a_asset_group_id.'_'.$asset_group->asset_group_name);
                    $l++;
                }

                $no_rental_agreements = RentalAgreement::whereNull('deleted_at')
                ->where([
                    ['is_internal',false],
                    ['sub_department_id',$sub_department->id],
                    ['factory_id',$factory_id],
                ])
                ->where(function($query)
                {
                    $query->where(db::raw("to_char(start_date,'YYYYMMDD')"),'>=',carbon::now()->format('Ymd'))
                    ->orWhere(db::raw("to_char(end_date,'YYYYMMDD')"),'>=',carbon::now()->format('Ymd'));
                })
                ->get();

                $total_no_rental_agreements = count($no_rental_agreements)+1;
                $sheet->SetCellValue("C1", "LIST_NO_KK_RENT");
                $m = 2;
                foreach ($no_rental_agreements as $key => $no_rental_agreement) 
                {
                    $sheet->SetCellValue("C".$m, $no_rental_agreement->id.'_'.$no_rental_agreement->no_agreement);
                    $m++;
                }

                foreach ($asset_type_informations as $key => $asset_type_information) 
                {
                    if($asset_type_information->is_custom && $asset_type_information->type == 'option')
                    {
                        $column_name = $asset_type_information->name.'_'.str_slug($asset_type_information->label,'_');

                        $sheet->setCellValue($alphabets[$key].'1',$column_name);
                        $has_coma = strpos($asset_type_information->value, ",");
                        $is_need_expolde = ($has_coma) ? true : false;
                        
                        if($is_need_expolde)
                        {
                            $splits = explode(',',$asset_type_information->value);
                            
                            $k = 2;

                            foreach ($splits as $key_1 => $split) 
                            {
                                $sheet->SetCellValue($alphabets[$key].$k, $split);
                                $k++;
                            }

                            $sheet->_parent->addNamedRange(
                                new \PHPExcel_NamedRange(
                                    $asset_type_information->name, $variantsSheet, $alphabets[$key].'2:'.$alphabets[$key].($k-1) // You can also replace 'A2:A7' with $variantsSheet ->calculateWorksheetDimension()
                                )
                            );
                        }

                       
                    }else if($asset_type_information->is_custom && $asset_type_information->type == 'lookup')
                    {
                        $column_name        = $asset_type_information->name.'_'.str_slug($asset_type_information->label,'_');
                        $custom_lookup_id   = $asset_type_information->lookup_id;
                        $lists              = ConsumableAsset::select('consumable_assets.id as consumable_asset_id'
                        ,'consumable_asset_details.id as consumable_asset_detail_id'
                        ,'consumable_assets.model as consumable_asset_model'
                        ,'consumable_assets.serial_number as consumable_asset_serial_number'
                        ,'consumable_asset_details.value as consumable_asset_detail_value')
                        ->where('asset_type_id',$custom_lookup_id)
                        ->leftjoin('consumable_asset_details','consumable_asset_details.consumable_asset_id','consumable_assets.id')
                        ->get();
                        
                        $sheet->setCellValue($alphabets[$key].'1',$column_name);
                        
                        if(count($lists) > 0)
                        {
                            $k = 2;
                            foreach ($lists as $key_1 => $list) 
                            {
                                $lookup_id                  = $list->consumable_asset_id;
                                $detail_lookup_id           = ($list->consumable_asset_detail_id)? $list->consumable_asset_detail_id : '-1';
                                $model_header               = $list->consumable_asset_model;
                                $serial_number_header       = $list->consumable_asset_serial_number;
                                $detail_value               = ($list->consumable_asset_detail_value) ? $list->consumable_asset_detail_value : null;
                                $lookup_value               = $custom_lookup_id.'|'.$lookup_id.'|'.$detail_lookup_id.';'.$model_header;

                                if($serial_number_header)   $lookup_value .= ':'.strtoupper($serial_number_header);
                                if($detail_value)           $lookup_value .= ':'.strtoupper($detail_value);
                                
                                $sheet->SetCellValue($alphabets[$key].$k, $lookup_value);
                                $k++;
                            }

                            $sheet->_parent->addNamedRange(
                                new \PHPExcel_NamedRange(
                                    $asset_type_information->name, $variantsSheet, $alphabets[$key].'2:'.$alphabets[$key].($k-1) // You can also replace 'A2:A7' with $variantsSheet ->calculateWorksheetDimension()
                                )
                            );
                        }

                       
                    }

                }

                $sheet->_parent->addNamedRange(
                    new \PHPExcel_NamedRange(
                        'manufacture_list', $variantsSheet, 'A2:A'.$total_manufacture // You can also replace 'A2:A7' with $variantsSheet ->calculateWorksheetDimension()
                    )
                );

                $sheet->_parent->addNamedRange(
                    new \PHPExcel_NamedRange(
                        'no_kk_list', $variantsSheet, 'C2:C'.$total_no_rental_agreements // You can also replace 'A2:A7' with $variantsSheet ->calculateWorksheetDimension()
                    )
                );

                $sheet->_parent->addNamedRange(
                    new \PHPExcel_NamedRange(
                        'erp_asset_group_list', $variantsSheet, 'B2:B'.$total_asset_group // You can also replace 'A2:A7' with $variantsSheet ->calculateWorksheetDimension()
                    )
                );

                
            });

           

            $excel->setActiveSheetIndex(0);



        })->export('xlsx');
    }
    
    public function uploadFormImport(Request $request)
    {  
        $return_barcode     = array();
        $array              = array();
        $update_consumables = array();

        if($request->hasFile('upload_file'))
        {
            $validator = Validator::make($request->all(), [
                'upload_file' => 'required|mimes:xls'
            ]);

            $path           = $request->file('upload_file')->getRealPath();
            $data           = Excel::selectSheets('active')->load($path,function($render){})
            ->setDateFormat('Y-m-d')
            ->get();

            //return response()->json($data);
            
            $movement_date  = $request->current_time;

            if(!empty($data) && $data->count())
            {
                try 
                {
                    DB::beginTransaction();
                    foreach ($data as $key => $value) 
                    {
                        $asset_type_id          = $value->asset_type_id;
                        $company_id             = $value->company_id;
                        $factory_id             = $value->factory_id;
                        $department_id          = $value->department_id;
                        $sub_department_id      = $value->sub_department_id;
                        $manufacture_name       = strtolower($value->manufacture_id);
                        $serial_number          = strtolower(trim($value->serial_number));
                        $no_inventory_manual    = strtolower(trim($value->no_inventory_manual));
                        $model                  = strtolower(trim($value->model));
                        $purchase_number        = strtolower(trim($value->purchase_number));
                        $supplier_name          = strtolower(trim($value->supplier_name));
                        $erp_no_asset           = strtolower(trim($value->erp_no_asset));
                        $no_bea_cukai           = strtolower(trim($value->no_bea_cukai));
                        $erp_item_code          = strtolower(trim($value->erp_item_code));
                        $price                  = $value->price;
                        $_erp_asset_group       = $value->erp_asset_group_name;
                        $_no_rent               = $value->no_rent;
                        $erp_asset_group_id     = ($_erp_asset_group ? explode('_',$_erp_asset_group)[0] : null );
                        $erp_asset_group_name   = ($_erp_asset_group ? explode('_',$_erp_asset_group)[1] : null );
                        $no_rent                = ($_no_rent ? explode('_',$_no_rent)[0] : null );
                        $description            = strtolower(trim($value->description));
                        $_is_from_sto_date      = strtolower(trim($value->is_from_sto_date));
                        $receiving_date         = ($value->receiving_date) ? Carbon::createFromFormat('Y-m-d', $value->receiving_date)->format('Y-m-d') : null;
                        
                        if($_is_from_sto_date == 'true' || $_is_from_sto_date == 't') $is_from_sto_date = true;
                        else $is_from_sto_date = false;
                        
                        if($asset_type_id && $company_id && $factory_id && $department_id && $serial_number && $model)
                        {
                            $company                    = Company::find($company_id);
                            $asset_type                 = AssetType::find($asset_type_id);
                            $factory                    = Factory::find($factory_id);
                            $erp_item                   = ItemIndirect::where(db::raw('lower(product_code)'),$erp_item_code)->first();
                            $asset_information_fields   = AssetInformationField::whereNull('delete_at')
                            ->where([
                                ['asset_type_id',$asset_type_id],
                                ['is_custom',true],
                            ])
                            ->orderby('created_at','asc')
                            ->get();

                            $_department                = Department::find($department_id);

                            $_manufacture               = Manufacture::whereNull('delete_at')
                            ->where([
                                [db::raw('lower(name)'),$manufacture_name],
                                ['company_id',$company_id],
                                ['sub_department_id',$sub_department_id],
                            ])
                            ->first();

                            if(!$company)
                            {
                                $obj                            = new stdClass();
                                $obj->asset_type_name           = ($asset_type ? $asset_type->name : null);
                                $obj->department_name           = ($_department ? $_department->name : null);
                                $obj->manufacture_name          = ($_manufacture) ? $_manufacture->name : null;
                                $obj->supplier_name             = $supplier_name;
                                $obj->serial_number             = $serial_number;
                                $obj->no_inventory              = null;
                                $obj->no_inventory_manual       = $no_inventory_manual;
                                $obj->no_asset                  = $erp_no_asset;
                                $obj->model                     = $model;
                                $obj->purchase_number           = $purchase_number;
                                $obj->receiving_date            = $receiving_date;
                                $obj->is_error                  = true;
                                $obj->erp_asset_id              = '-';
                                $obj->erp_asset_group_name      = '-';

                                foreach ($asset_information_fields as $key_2 => $asset_information_field) 
                                {

                                    $custom_field_name           = $asset_information_field->name;
                                    $_custom_field_name          = $asset_information_field->name.'_'.str_slug($asset_information_field->label,'_');
                                    $obj->$custom_field_name     = strtolower($value->$_custom_field_name);
                                }
                            
                                $obj->system_log                = 'Perusahaan tidak ditemukan';
                                $array []   = $obj;
                            }else
                            {
                                if(!$factory)
                                {
                                    $obj                            = new stdClass();
                                    $obj->asset_type_name           = ($asset_type ? $asset_type->name : null);
                                    $obj->department_name           = ($_department ? $_department->name : null);
                                    $obj->manufacture_name          = ($_manufacture) ? $_manufacture->name : null;
                                    $obj->supplier_name             = $supplier_name;
                                    $obj->serial_number             = $serial_number;
                                    $obj->no_inventory              = null;
                                    $obj->no_inventory_manual       = $no_inventory_manual;
                                    $obj->no_asset                  = $erp_no_asset;
                                    $obj->model                     = $model;
                                    $obj->purchase_number           = $purchase_number;
                                    $obj->receiving_date            = $receiving_date;
                                    $obj->is_error                  = true;
                                    $obj->erp_asset_id              = '-';
                                    $obj->erp_asset_group_name      = '-';

                                    foreach ($asset_information_fields as $key_2 => $asset_information_field) 
                                    {

                                        $custom_field_name           = $asset_information_field->name;
                                        $_custom_field_name          = $asset_information_field->name.'_'.str_slug($asset_information_field->label,'_');
                                        $obj->$custom_field_name     = strtolower($value->$_custom_field_name);
                                    }
                                
                                    $obj->system_log                = 'Pabrik tidak ditemukan';
                                    $array []   = $obj;
                                }else
                                {
                                    if(!$asset_type)
                                    {
                                        $obj                            = new stdClass();
                                        $obj->asset_type_name           = ($asset_type ? $asset_type->name : null);
                                        $obj->department_name           = ($_department ? $_department->name : null);
                                        $obj->manufacture_name          = ($_manufacture) ? $_manufacture->name : null; 
                                        $obj->supplier_name             = $supplier_name;
                                        $obj->serial_number             = $serial_number;
                                        $obj->no_inventory              = null;
                                        $obj->no_inventory_manual       = $no_inventory_manual;
                                        $obj->no_asset                  = $erp_no_asset;
                                        $obj->model                     = $model;
                                        $obj->purchase_number           = $purchase_number;
                                        $obj->receiving_date            = $receiving_date;
                                        $obj->is_error                  = true;
                                        $obj->erp_asset_id              = '-';
                                        $obj->erp_asset_group_name      = '-';

                                        foreach ($asset_information_fields as $key_2 => $asset_information_field) 
                                        {

                                            $custom_field_name           = $asset_information_field->name;
                                            $_custom_field_name          = $asset_information_field->name.'_'.str_slug($asset_information_field->label,'_');
                                            $obj->$custom_field_name     = strtolower($value->$_custom_field_name);
                                        }
                                    
                                        $obj->system_log                = 'Tipe asset tidak ditemukan';
                                        $array []   = $obj;
                                    }else
                                    {
                                        if(!$_department)
                                        {
                                            $obj                            = new stdClass();
                                            $obj->asset_type_name           = ($asset_type ? $asset_type->name : null);
                                            $obj->department_name           = ($_department ? $_department->name : null);
                                            $obj->manufacture_name          = ($_manufacture) ? $_manufacture->name : null; 
                                            $obj->supplier_name             = $supplier_name;
                                            $obj->serial_number             = $serial_number;
                                            $obj->no_inventory              = null;
                                            $obj->no_inventory_manual       = $no_inventory_manual;
                                            $obj->no_asset                  = $erp_no_asset;
                                            $obj->model                     = $model;
                                            $obj->purchase_number           = $purchase_number;
                                            $obj->receiving_date            = $receiving_date;
                                            $obj->is_error                  = true;
                                            $obj->erp_asset_id              = '-';
                                            $obj->erp_asset_group_name      = '-';

                                            foreach ($asset_information_fields as $key_2 => $asset_information_field) 
                                            {

                                                $custom_field_name           = $asset_information_field->name;
                                                $_custom_field_name          = $asset_information_field->name.'_'.str_slug($asset_information_field->label,'_');
                                                $obj->$custom_field_name     = strtolower($value->$_custom_field_name);
                                            }
                                        
                                            $obj->system_log                = 'Kode departemen masih belum diisi. Silahkan hubungi admin / ict untuk di update terlebih dahulu';
                                            $array []   = $obj;
                                        }else
                                        {
                                            if($asset_type->delete_at)
                                            {
                                                $obj                            = new stdClass();
                                                $obj->asset_type_name           = ($asset_type ? $asset_type->name : null);
                                                $obj->department_name           = ($_department ? $_department->name : null);
                                                $obj->manufacture_name          = ($_manufacture) ? $_manufacture->name : null; 
                                                $obj->supplier_name             = $supplier_name;
                                                $obj->serial_number             = $serial_number;
                                                $obj->no_inventory              = null;
                                                $obj->no_inventory_manual       = $no_inventory_manual;
                                                $obj->no_asset                  = $erp_no_asset;
                                                $obj->model                     = $model;
                                                $obj->purchase_number           = $purchase_number;
                                                $obj->receiving_date            = $receiving_date;
                                                $obj->is_error                  = true;
                                                $obj->erp_asset_id              = '-';
                                                $obj->erp_asset_group_name      = '-';
        
                                                foreach ($asset_information_fields as $key_2 => $asset_information_field) 
                                                {
        
                                                    $custom_field_name           = $asset_information_field->name;
                                                    $_custom_field_name          = $asset_information_field->name.'_'.str_slug($asset_information_field->label,'_');
                                                    $obj->$custom_field_name     = strtolower($value->$_custom_field_name);
                                                }
                                            
                                                $obj->system_log                = 'Tipe asset sudah di hapus';
                                                $array []   = $obj;
                                            }else
                                            {
                                                $is_exists      = Asset::where([
                                                    ['company_id',$company_id],
                                                    ['factory_id',$factory_id],
                                                    ['department_id',$department_id],
                                                    ['sub_department_id',$sub_department_id],
                                                    ['origin_company_location_id',$company_id],
                                                    ['origin_factory_location_id',$factory_id],
                                                    ['origin_department_location_id',$department_id],
                                                    ['origin_sub_department_location_id',$sub_department_id],
                                                    ['asset_type_id',$asset_type_id],
                                                ])
                                                ->whereNull('delete_at');
                                        
                                                if($no_rent) $is_exists                 = $is_exists->where('no_rent',$no_rent);
                                                if($no_inventory_manual) $is_exists     = $is_exists->where('no_inventory_manual',$no_inventory_manual);
                                                if($erp_no_asset) $is_exists            = $is_exists->where('erp_no_asset',$erp_no_asset);
                                                if($model) $is_exists                   = $is_exists->where('model',$model);
                                                if($serial_number) $is_exists           = $is_exists->where('serial_number',$serial_number);
                                                if($_manufacture) $is_exists            = $is_exists->where('manufacture_id',($_manufacture)? $_manufacture->id : null);
                                                if($erp_asset_group_id) $is_exists      = $is_exists->where('erp_asset_group_id',$erp_asset_group_id);
                                        
                                                $is_exists = $is_exists->exists();
    
                                                if($is_exists)
                                                {
                                                    $obj                            = new stdClass();
                                                    $obj->asset_type_name           = ($asset_type ? $asset_type->name : null);
                                                    $obj->department_name           = ($_department ? $_department->name : null);
                                                    $obj->manufacture_name          = ($_manufacture) ? $_manufacture->name : null;
                                                    $obj->supplier_name             = $supplier_name;
                                                    $obj->serial_number             = $serial_number;
                                                    $obj->no_inventory              = null;
                                                    $obj->no_inventory_manual       = $no_inventory_manual;
                                                    $obj->no_asset                  = $erp_no_asset;
                                                    $obj->model                     = $model;
                                                    $obj->purchase_number           = $purchase_number;
                                                    $obj->receiving_date            = $receiving_date;
                                                    $obj->erp_asset_id              = '-';
                                                    $obj->erp_asset_group_name      = '-';
                                                    $obj->is_error                  = true;
    
                                                    foreach ($asset_information_fields as $key_2 => $asset_information_field) 
                                                    {
    
                                                        $custom_field_name           = $asset_information_field->name;
                                                        $_custom_field_name          = $asset_information_field->name.'_'.str_slug($asset_information_field->label,'_');
                                                        $obj->$custom_field_name     = strtolower($value->$_custom_field_name);
                                                    }
                                                
                                                    $obj->system_log                = 'data sudah ada';
                                                    $array []   = $obj;
                                                }else
                                                {
                                                        $barcode                        = 'BELUM DIPRINT';
                                                        $month                          = ($value->receiving_date)? Carbon::createFromFormat('Y-m-d', $value->receiving_date)->format('m') : Carbon::now()->format('m');
                                                        $year                           = ($value->receiving_date)? Carbon::createFromFormat('Y-m-d', $value->receiving_date)->format('y') : Carbon::now()->format('y');
                                                        $_erp_asset_group_name          = ($erp_asset_group_name ? $erp_asset_group_name : $asset_type->erp_asset_group_name);
                                                        $_erp_asset_group_id            = ($erp_asset_group_id ? $erp_asset_group_id : $asset_type->erp_asset_group_id);
                                                        $_referral_code                 = $_erp_asset_group_name.'.'.strtoupper($_department->code).'.'.strtoupper($factory->code).'.'.strtoupper($asset_type->code);
                                                        $_no_inventory                  = $_erp_asset_group_name.'.'.strtoupper($_department->code).'.'.strtoupper($factory->code).'.'.strtoupper($asset_type->code).'.'.$month.'.'.$year;
                                                        $sequence                       = Asset::where('referral_code',$_referral_code)->max('sequence');
    
                                                        if ($sequence == null) $sequence = 1;
                                                        else $sequence                  += 1;
    
                                                        if($sequence < 10) $sequence_format = '00'.$sequence;
                                                        else if($sequence < 100) $sequence_format = '0'.$sequence;
                                                        else $sequence_format = $sequence;
    
                                                        $no_inventory                   = $_no_inventory.'.'.$sequence_format;
    
                                                    
                                                        $asset = Asset::firstorCreate([
                                                            'asset_type_id'                     => $asset_type_id,
                                                            'company_id'                        => $company_id,
                                                            'factory_id'                        => $factory_id,
                                                            'department_id'                     => $_department->id,
                                                            'sub_department_id'                 => $sub_department_id,
                                                            'origin_company_location_id'        => $company_id,
                                                            'origin_factory_location_id'        => $factory_id,
                                                            'origin_department_location_id'     => $_department->id,
                                                            'origin_sub_department_location_id' => $sub_department_id,
                                                            'manufacture_id'                    => ($_manufacture ? $_manufacture->id : null),
                                                            'price'                             => ($price ? $price : 0),
                                                            'barcode'                           => $barcode,
                                                            'serial_number'                     => $serial_number,
                                                            'no_inventory'                      => $no_inventory,
                                                            'no_inventory_manual'               => $no_inventory_manual,
                                                            'model'                             => $model,
                                                            'purchase_number'                   => $purchase_number,
                                                            'supplier_name'                     => $supplier_name,
                                                            'receiving_date'                    => $receiving_date,
                                                            'referral_code'                     => $_referral_code,
                                                            'sequence'                          => $sequence,
                                                            'erp_item_id'                       => ($erp_item ? $erp_item->m_product_id : null),
                                                            'erp_item_code'                     => ($erp_item ? $erp_item->product_code : null),
                                                            'erp_no_asset'                      => ($erp_no_asset ? $erp_no_asset : null),
                                                            'erp_asset_group_id'                => $_erp_asset_group_id,
                                                            'erp_asset_group_name'              => $_erp_asset_group_name,
                                                            'status'                            => 'siap digunakan',
                                                            'no_rent'                           => $no_rent,
                                                            'is_rent'                           => ($no_rent ? true : false),
                                                            'last_company_location_id'          => $company_id,
                                                            'last_factory_location_id'          => $factory_id,
                                                            'last_department_location_id'       => $_department->id,
                                                            'last_sub_department_location_id'   => $sub_department_id,
                                                            'created_at'                        => $movement_date,
                                                            'updated_at'                        => $movement_date,
                                                            'create_user_id'                    => Auth::user()->id
                                                        ]);
    
                                                        if($asset->erp_no_asset)
                                                        {
                                                            Temporary::firstorCreate([
                                                                'column_1'  => $asset->id,
                                                                'column_2'  => 'sync_asset_id_erp',
                                                            ]);
                                                        }
                                            
                                                        $asset_movement = AssetMovement::firstorCreate([
                                                            'asset_id'                          => $asset->id,
                                                            'status'                            => 'siap digunakan',
                                                            'from_company_location'             => $asset->company_id,
                                                            'from_factory_location'             => $asset->factory_id,
                                                            'from_department_location'          => $asset->department_id,
                                                            'from_sub_department_location'      => $asset->sub_department_id,
                                                            'from_employee_nik'                 => auth::user()->nik,
                                                            'from_employee_name'                => auth::user()->name,
                                                            'movement_date'                     => $movement_date,
                                                            'created_at'                        => $movement_date,
                                                            'updated_at'                        => $movement_date,
                                                            'note'                              => 'asset baru',
                                                            'rental_agreement_id'               => ($asset->no_rent ? $asset->no_rent : null),
                                                        ]);
                                                        
                                                        $asset->last_user_movement_id   = auth::user()->id;
                                                        $asset->last_asset_movement_id     = $asset_movement->id;
                                                        $asset->save();

                                                        foreach ($asset_information_fields as $key_2 => $asset_information_field) 
                                                        {
    
                                                            $custom_field_name           = $asset_information_field->name;
                                                            $is_lookup                   = ($asset_information_field->type == 'lookup') ? true :false;
                                                            $_custom_field_name          = $asset_information_field->name.'_'.str_slug($asset_information_field->label,'_');
                                                            
                                                            if($is_lookup)
                                                            {
                                                                $lookup_value            = $value->$_custom_field_name;
                                                                $has_simicolon           = strpos($lookup_value, ";");
                                                                $is_need_lookup          = ($has_simicolon) ? true : false;
    
                                                                if($is_need_lookup)
                                                                {
                                                                    $split_simicolons           = explode(';',$lookup_value);
                                                                    $custom_lookup_value        = $split_simicolons[0];
                                                                    $has_pipe                   = strpos($custom_lookup_value, "|");
                                                                    $_is_need_lookup            = ($has_pipe) ? true : false;
                                                                    
                                                                    if($_is_need_lookup)
                                                                    {
                                                                        $split_pipelines                = explode('|',$custom_lookup_value);
                                                                        $custom_lookup_id               = $split_pipelines[0];
                                                                        $lookup_id                      = $split_pipelines[1];
                                                                        $lookup_detail_id               = $split_pipelines[2];
    
                                                                        $is_consumable_header_exists    = ConsumableAsset::where('id',$lookup_id)->exists();
                                                                        
                                                                        if($is_consumable_header_exists)
                                                                        {
                                                                            ConsumableAssetMovement::firstorCreate([
                                                                                'asset_id'                          => $asset->id,
                                                                                'consumable_asset_id'               => $lookup_id,
                                                                                'consumable_asset_detail_id'        => ($lookup_detail_id != '-1'? $lookup_detail_id : null),
                                                                                'status'                            => 'digunakan',
                                                                                'from_company_location'             => $asset->company_id,
                                                                                'from_factory_location'             => $asset->factory_id,
                                                                                'from_department_location'          => $asset->deparment_id,
                                                                                'from_sub_department_location'      => $asset->sub_deparment_id,
                                                                                'from_employee_nik'                 => auth::user()->nik,
                                                                                'from_employee_name'                => auth::user()->name,
                                                                                'to_company_location'               => $asset->company_id,
                                                                                'to_factory_location'               => $asset->factory_id,
                                                                                'to_department_location'            => $asset->deparment_id,
                                                                                'to_sub_department_location'        => $asset->sub_deparment_id,
                                                                                'movement_date'                     => $movement_date,
                                                                                'is_using_in_custom_field'          => true,
                                                                                'system_note'                       => 'digunakan untuk asset, dengan nomor inventori '.$asset->no_inventory.'. dikeluarkan pada tanggal '.$movement_date->format('d/M/Y').' oleh '.auth::user()->name,
                                                                            ]);
                                                        
                                                                            $update_consumables [] = $lookup_id;
                                                                        }
                                                                    }
                                                                    
                                                                    $asset->$custom_field_name   = $custom_lookup_value;
                                                                }
                                                            }else
                                                            {
                                                                $asset->$custom_field_name   = strtolower($value->$_custom_field_name);
                                                            }
                                                            $asset->save();
                                                        }
    
                                                        $obj                            = new stdClass();
                                                        $obj->asset_type_name           = ($asset_type ? $asset_type->name : null);
                                                        $obj->department_name           = ($_department ? $_department->name : null);
                                                        $obj->manufacture_name          = ($_manufacture) ? $_manufacture->name : null;
                                                        $obj->serial_number             = $serial_number;
                                                        $obj->no_inventory              = $no_inventory;
                                                        $obj->supplier_name             = $supplier_name;
                                                        $obj->no_inventory_manual       = $no_inventory_manual;
                                                        $obj->no_asset                  = $erp_no_asset;
                                                        $obj->model                     = $model;
                                                        $obj->purchase_number           = $purchase_number;
                                                        $obj->receiving_date            = $receiving_date;
                                                        $obj->is_error                  = false;
                                                        $obj->erp_asset_id              = $asset->erp_asset_id;
                                                        $obj->erp_asset_group_name      = $asset->erp_asset_group_name;
    
                                                        foreach ($asset_information_fields as $key_2 => $asset_information_field) 
                                                        {
    
                                                            $custom_field_name           = $asset_information_field->name;
                                                            $_custom_field_name          = $asset_information_field->name.'_'.str_slug($asset_information_field->label,'_');
                                                            $obj->$custom_field_name     = strtolower($value->$_custom_field_name);
                                                        }
                                                    
                                                        Temporary::firstorCreate([
                                                            'column_1'  => $asset->origin_sub_department_location_id,
                                                            'column_2'  => 'sync_export_asset_per_department',
                                                        ]);

                                                        $obj->system_log                = 'success';
                                                        $return_barcode []              = $asset->id;
                                                        $array []                       = $obj;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                    }  

                    $detail_consumable_assets = ConsumableAssetMovement::select('consumable_asset_id',db::raw('count(0) as total_qty_used'))
                    ->whereIn('consumable_asset_id',$update_consumables)
                    ->where([
                        ['status','digunakan'],
                        ['is_return',false]
                    ])
                    ->groupby('consumable_asset_id')
                    ->get();

                    foreach ($detail_consumable_assets as $key => $detail_consumable_asset) 
                    {
                        $consumable_asset                   = ConsumableAsset::find($detail_consumable_asset->consumable_asset_id);
                        $total                              = $consumable_asset->total;
                        $new_available_qty                  = $total - $detail_consumable_asset->total_qty_used;

                        $consumable_asset->qty_used         = $detail_consumable_asset->total_qty_used;
                        $consumable_asset->qty_available    = $new_available_qty;
                        $consumable_asset->save();
                    }

                    DB::commit();   
                } catch (Exception $e) 
                {
                    DB::rollBack();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }

                return response()->json([
                    'data'      => $array,
                    'barcodes'  => $return_barcode,
                ],200);

            }else
            {
                return response()->json(['message' => 'import gagal, silahkan cek file anda'],422);
            }


        }
    }

    public function history($id)
    {
        $asset = Asset::find($id);
        return view('asset.history',compact('asset'));

    }

    public function historyData(Request $request, $id)
    {
        if(request()->ajax()) 
        {
            $data = db::select(db::raw("select status
                ,movement_date
                ,from_company.name as from_company_name
                ,from_factory.name as from_factory_name
                ,from_department.name from_department_location
                ,from_sub_department.name from_sub_department_location
                ,from_employee_nik
                ,from_employee_name
                ,to_company.name as to_company_name
                ,to_factory.name as to_factory_name
                ,to_department.name as to_department_location
                ,to_sub_department.name as to_sub_department_location
                ,to_area_location
                ,to_employee_nik
                ,to_employee_name
                ,technician_name
                ,rental_agreements.no_agreement as no_kk
                ,rental_agreements.no_bc as no_bea_cukai
                ,note
                ,is_handover_asset
                ,cancel_obsolete_date
                ,cancelObsolete.name as cancel_obsolete_name
                ,cancel_obsolete_note
                from asset_movements 
                left join companies as from_company on asset_movements.from_company_location = from_company.id 
                left join factories as from_factory on asset_movements.from_factory_location = from_factory.id
                left join departments as from_department on asset_movements.from_department_location = from_department.id
                left join sub_departments as from_sub_department on asset_movements.from_sub_department_location = from_sub_department.id 
                left join companies as to_company on asset_movements.to_company_location = to_company.id 
                left join factories as to_factory on asset_movements.to_factory_location = to_factory.id 
                left join departments as to_department on asset_movements.to_department_location = to_department.id 
                left join sub_departments as to_sub_department on asset_movements.to_sub_department_location = to_sub_department.id
                left join rental_agreements on asset_movements.rental_agreement_id = rental_agreements.id 
                left join users as cancelObsolete on asset_movements.cancel_obsolete_user_id = cancelObsolete.id
                where asset_movements.asset_id = '".$id."'
                order by movement_date desc"));

            
                return datatables()->of($data)
            ->editColumn('movement_date',function($data)
            {
                return  Carbon::createFromFormat('Y-m-d H:i:s', $data->movement_date)->format('d/M/Y H:i:s');
            })
            ->editColumn('status',function($data)
            {
            	if($data->is_handover_asset) return 'Penerimaan asset dari pindah tangan';
                else return ucwords($data->status);

            })
            ->editColumn('from',function($data)
            {
                if($data->status != 'bapb')
                {
                    $msg = '';
                    if($data->from_company_name) $msg .= '<b>Perusahaan</b><br/>'.ucwords($data->from_company_name).'<br/>';
                    if($data->from_factory_name) $msg .= '<b>Pabik</b><br/>'.ucwords($data->from_factory_name).'<br/>';
                    if($data->from_department_location) $msg .= '<b>Departemen</b><br/>'.ucwords($data->from_department_location).'<br/>';
                    if($data->from_sub_department_location) $msg .= '<b>Subdepartemen</b><br/>'.ucwords($data->from_sub_department_location).'<br/>';
                    if($data->from_employee_nik) $msg .= '<b>Nik</b><br/>'.ucwords($data->from_employee_nik).'<br><b>Nama</b><br/>'.ucwords($data->from_employee_name).'<br/>';
                    if($data->technician_name) $msg .= '<b>Nama Teknisi</b><br/>'.ucwords($data->technician_name);
                    return $msg;
                }else
                {
                    return 'ASSET SUDAH DI BAPB';
                }
                
                 
            })
            ->editColumn('to',function($data)
            {
                if($data->status != 'bapb')
                {
                    if($data->note != 'asset baru')
                    {
                        if($data->status == 'dikeluarkan' || $data->status == 'dipindah tangan' )
                        {
                            return '<b>Perusahaan</b><br/>'.ucwords($data->to_company_name).
                            '<br><b>Pabik</b><br/>'.ucwords($data->to_factory_name).
                            '<br><b>Departemen</b><br/>'.ucwords($data->to_department_location).
                            '<br><b>Sub Departemen</b><br/>'.ucwords($data->to_sub_department_location).
                            '<br><b>Area</b><br/>'.ucwords($data->to_area_location);
                        }
                        else if($data->status =='dipinjam' || $data->status == 'dipindah tangan')
                        {
        
                            return '<b>Perusahaan</b><br/>'.ucwords($data->to_company_name).
                            '<br><b>Pabik</b><br/>'.ucwords($data->to_factory_name).
                            '<br><b>Departemen</b><br/>'.ucwords($data->to_department_location).
                            '<br><b>Sub Departemen</b><br/>'.ucwords($data->to_sub_department_location).
                            ($data->to_area_location ? '<br><b>Area</b><br/>'.ucwords($data->to_area_location) : null);
        
                        }else if($data->status == 'diperbaiki')
                        {
                            return '<br><b>Nama Teknisi</b><br/>'.ucwords($data->to_employee_name);
                        }else if ($data->status == 'obsolete') 
                        {
                            return '<b>Siap di BAPB</b><br/>';
                        }else
                        {
                            return '<b>Perusahaan</b><br/>'.ucwords($data->to_company_name).
                            '<br><b>Pabik</b><br/>'.ucwords($data->to_factory_name).
                            '<br><b>Departemen</b><br/>'.ucwords($data->to_department_location).
                            '<br><b>Sub Departemen</b><br/>'.ucwords($data->to_sub_department_location).
                            '<br><b>Nik</b><br/>'.ucwords($data->to_employee_nik).
                            '<br><b>Nama</b><br/>'.ucwords($data->to_employee_name);
                        }
                    }
                }else
                {
                    return 'ASSET SUDAH DI BAPB';
                }
                

            })
            ->editColumn('no_kk',function($data){
            	return ucwords($data->no_kk);
            })
            ->editColumn('no_bea_cukai',function($data){
            	return ucwords($data->no_bea_cukai);
            })
            ->editColumn('note',function($data)
            {
                if($data->status == 'rusak') return 'root cause : '.$data->note;
            	else return ucwords($data->note.($data->cancel_obsolete_date ? '['.$data->cancel_obsolete_date.'] movement dibatalkan oleh '.$data->cancel_obsolete_name.', dengan alasan '.$data->cancel_obsolete_note : null));
            })
            ->rawColumns(['from','to'])
            ->make(true);
        }
    }

    public function exportReport(Request $request)
    {
        $department = SubDepartment::find($request->department);
        $file = Config::get('storage.excel_asset') . '/' . 'asset_'.str_slug($department->name).'.xlsx';

        if(!file_exists($file)) return 'file yang anda cari tidak ditemukan';

        $resp = response()->download($file);
        $resp->headers->set('Pragma', 'no-cache');
        $resp->headers->set('Cache-Control', 'nocache, no-store, max-age=0, must-revalidate');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');
        return $resp;
    }

    public function getStatus(Request $request)
    {
        $company_id         = auth::user()->company_id;
        $factory_id         = $request->factory_id;
        $sub_department_id  = $request->sub_department_id;

        $status         = Asset::select('status')
        ->where('origin_company_location_id',$company_id)
        ->where('origin_factory_location_id',$factory_id);

        if($sub_department_id != 'null') $status = $status->where('origin_sub_department_location_id',$sub_department_id);
        
        $status = $status->groupby('status')
        ->pluck('status','status')
        ->all();

        return response()->json(['status'=>$status]);
    }

    public function getCustomField(Request $request)
    {
        $asset_id               = $request->asset_id;
        $asset_type_id          = $request->asset_type_id;
        $asset                  = Asset::find($asset_id);
        $asset_custom_fields    = AssetInformationField::where([
            ['asset_type_id', $asset_type_id],
            ['is_custom', true],
        ])
        ->whereNull('delete_at')
        ->orderby('created_at','asc')
        ->get();

        $array = array();
        foreach ($asset_custom_fields as $key => $value) 
        {

            $has_coma           = strpos($value->value, ",");
            $is_need_expolde    = ($has_coma) ? true : false;
            $custom_field       = $value->name;
            $custom_lookup_id   = $value->lookup_id;
            $current_value      = ($asset ? $asset->$custom_field:null);
            $has_pipe           = strpos($current_value, "|");
            $is_need_lookup     = ($has_pipe) ? true : false;
            $options            = array();

            if($is_need_expolde)
            {
                $splits = explode(',',$value->value);
                foreach ($splits as $key_1 => $split) 
                {
                    if(strtoupper($current_value) == strtoupper($split)) $selected = true;
                    else $selected = false;

                    $obj            = new stdClass();
                    $obj->id        = $split;
                    $obj->name      = $split;
                    $obj->selected  = $selected;
                    $options []     = $obj;
                }
            }
           // dd($has_pipe);
            if($is_need_lookup)
            {
                $splits                     = explode('|',$current_value);
                $custom_lookup_id           = $splits[0];
                $lookup_id                  = $splits[1];
                $detail_lookup_id           = $splits[2];

                $consumable_asset           = ConsumableAsset::where([
                    ['asset_type_id',$custom_lookup_id],
                    ['id',$lookup_id],
                ])
                ->first();

                $consumable_asset_detail    = ConsumableAssetDetail::where([
                    ['consumable_asset_id',$lookup_id],
                    ['id',$detail_lookup_id],
                ])
                ->first();

                $model_header               = ($consumable_asset ? $consumable_asset->model : null);
                $serial_number_header       = ($consumable_asset ? $consumable_asset->serial_number : null);
                $detail_value               = ($consumable_asset_detail ? $consumable_asset_detail->value : null);
                $lookup_value               = $model_header;

                if($serial_number_header)   $lookup_value .= ':'.strtoupper($serial_number_header);
                if($detail_value)           $lookup_value .= ':'.strtoupper($detail_value);
                
                $current_value              = $lookup_value;
            }

            $consumable_asset       = ConsumableAssetMovement::where([
                ['asset_id',$asset_id],
                ['is_using_in_custom_field',true],
            ])
            ->whereIn('consumable_asset_id',function($query) use($custom_lookup_id)
            {
                $query->select('id')
                ->from('consumable_assets')
                ->where('asset_type_id',$custom_lookup_id);
            })
            ->first();

            $obj                        = new stdClass();
            $obj->id                    = $value->id;
            $obj->asset_type            = $value->assetType->name;
            $obj->name                  = $custom_field;
            $obj->default_value         = (count($options) > 0)? $options : null;
            $obj->label                 = ucwords($value->label);
            $obj->is_option             = ($value->type == 'option')? true:false;
            $obj->is_number             = ($value->type == 'number')? true:false;
            $obj->is_text               = ($value->type == 'text')? true:false;
            $obj->is_lookup             = ($value->type == 'lookup')? true:false;
            $obj->is_required           = ($value->required)? true:false;
            $obj->custom_lookup_id      = $value->lookup_id;
            $obj->lookup_id             = ($consumable_asset)? $consumable_asset->consumable_asset_id : null;
            $obj->detail_lookup_id      = ($consumable_asset)? $consumable_asset->consumable_asset_detail_id : null;
            $obj->value                 = ucwords($current_value);
            $array []                   = $obj;
        }

        return response()->json($array);
    }

    public function getRentAgreement(Request $request)
    {
        $factory_id         = $request->factory_id;
        $sub_department_id  = $request->sub_department_id;

        $rent_agreement = RentalAgreement::whereNull('deleted_at')
        ->where([
            ['is_internal',false],
            ['is_return_to_source',false],
            ['sub_department_id',$sub_department_id],
        ])
        ->where(function($query) use ($factory_id)
        {
            $query->where('factory_id',$factory_id)
            ->orWhere('destination_factory_id',$factory_id);
        })
        ->where(function($query)
        {
            $query->where(db::raw("to_char(start_date,'YYYYMMDD')"),'>=',carbon::now()->format('Ymd'))
            ->orWhere(db::raw("to_char(end_date,'YYYYMMDD')"),'>=',carbon::now()->format('Ymd'));
        })
        ->pluck('no_agreement','id')
        ->all();

        return response()->json(['rent_agreement'=>$rent_agreement]);
    }

    public function getAssetTypeAndManufacture(Request $request)
    {

        $flag               = $request->flag;
        $sub_department_id  = $request->sub_department_id;
        $company_id         = auth::user()->company_id;

        if($flag == 'asset_type')
        {
            $asset_type_id  = $request->asset_type_id;
            $asset_types = AssetType::whereNull('delete_at')
            ->where([
                ['sub_department_id',$sub_department_id],
                ['company_id',$company_id],
                ['is_consumable',false],
            ])
            ->pluck('name','id')
            ->all();
            
            return response()->json(['asset_types' => $asset_types,'asset_type_id' => $asset_type_id]);
        }else
        {
            $manufacture_id = $request->manufacture_id;
            $manufactures = Manufacture::whereNull('delete_at')
            ->where([
                ['sub_department_id',$sub_department_id],
                ['company_id',$company_id],
            ])
            ->pluck('name','id')
            ->all();

            return response()->json(['manufactures' => $manufactures,'manufacture_id' => $manufacture_id]);
        }
    }

    public function getLookup(Request $request)
    {
        $q      = strtolower(trim($request->q));
        $lists   = ConsumableAsset::select('consumable_assets.id as consumable_asset_id'
        ,'consumable_asset_details.id as consumable_asset_detail_id'
        ,'consumable_assets.model as consumable_asset_model'
        ,'consumable_assets.serial_number as consumable_asset_serial_number'
        ,'consumable_asset_details.value as consumable_asset_detail_value')
        ->where('asset_type_id',$request->custom_lookup_id)
        ->leftjoin('consumable_asset_details','consumable_asset_details.consumable_asset_id','consumable_assets.id')
        ->where(function($query) use ($q)
        {
            $query->where(db::raw('lower(consumable_assets.model)'),'LIKE',"%$q%")
            ->orWhere(db::raw('lower(consumable_assets.serial_number)'),'LIKE',"%$q%")
            ->orWhere(db::raw('lower(consumable_asset_details.value)'),'LIKE',"%$q%");
        })
        ->paginate(10);

        return view('asset._loookup_value',compact('lists'));
    }

    static function randomCode()
    {
        $referral_code = 'A'.Carbon::now()->format('u');
        $sequence = Barcode::where('referral_code',$referral_code)->max('sequence');

        if ($sequence == null) $sequence = 1;
        else $sequence += 1;
        
        Barcode::FirstOrCreate([
            'barcode'       => $referral_code.$sequence.'-2',
            'referral_code' => $referral_code,
            'sequence'      => $sequence,
        ]);

        return $referral_code.$sequence.'-2';
    }

    public function showImage($filename)
    {
        $path = Config::get('storage.asset');
        $resp = response()->download($path . '/' . $filename);
        $resp->headers->set('Content-Disposition', 'inline');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');
        return $resp;
    }

    public function printout(Request $request,$id)
    {
        $assets         = Asset::where('id',$id)
        ->orderby('sequence','asc')
        ->whereNull('delete_at')
        ->get();

        foreach ($assets as $key => $asset) 
        {
            if($asset->barcode == 'BELUM DIPRINT')
            {
                $asset->barcode = $this->randomCode();
                $asset->save();
            }
        }

        return view('asset.barcode',compact('assets'));
    }
}
