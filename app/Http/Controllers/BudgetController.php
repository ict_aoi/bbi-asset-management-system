<?php namespace App\Http\Controllers;

use DB;
use Auth;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\Budget;
use App\Models\Factory;
use App\Models\SubDepartment;
use App\Models\DetailBudget;

class BudgetController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $msg                = $request->session()->get('message');
        $factories          = Factory::whereNull('delete_at')->where('code','bbis')->pluck('name', 'id')->all();
        $sub_departments    = SubDepartment::where('is_other',false)
        ->whereIn('code',['ict','elk','ga','me'])
        ->whereNull('delete_at')
        ->pluck('name', 'id')
        ->all();
        
        $years              = array();
        $first_year         = carbon::now()->format('Y');

        while($first_year <= carbon::now()->addYear(5)->format('Y'))
        {
            $years[$first_year] = $first_year;
            $first_year++;
        }
        
        return view('budget.index',compact('msg','factories','sub_departments','years'));
    }

    public function data(Request $request)
    {
        if(request()->ajax()) 
        {
            $factory_id         = $request->factory;
            $sub_department_id  = $request->sub_department;
            
            $data = DB::table('budget_v')
            ->where('factory_id','LIKE',"%$factory_id%")
            ->orWhere('sub_department_id','LIKE',"%$sub_department_id%");
            
            return datatables()->of($data)
            ->addColumn('action', function($data) {
                return view('budget._action', [
                    'model'    => $data,
                    'edit'     => route('budget.edit',$data->id),
                    'delete'   => route('budget.destroy',$data->id),
                ]);
            })
            
            ->editColumn('total_budget',function($data){
                return number_format($data->total_budget, 4, '.', ',');
            })
            ->rawColumns(['action'])
            ->make(true);
        }
    }

    public function create(Request $request)
    {
        $factories      = Factory::whereNull('delete_at')->pluck('name', 'id')->all();
        $departments    = Department::whereNull('delete_at')->pluck('name', 'id')->all();

        return view('budget.create',compact('factories','departments'));
    }

    public function store(Request $request)
    {
        $factory_id         = $request->factory;
        $sub_department_id  = $request->sub_department;
        $year               = $request->year;
        $current_time       = ($request->current_time ? $request->current_time : carbon::now());
        $name               = trim(strtolower($request->name));
        $description        = trim(strtolower($request->description));

        $months = [
            '01' => 'january',
            '02' => 'february',
            '03' => 'march',
            '04' => 'april',
            '05' => 'may',
            '06' => 'june',
            '07' => 'july',
            '08' => 'augusts',
            '09' => 'september',
            '10' => 'october',
            '11' => 'november',
            '12' => 'december',
        ];

        if (Budget::where([
            ['name',$name],
            ['periode_year',$year],
            ['factory_id',$factory_id],
            ['sub_department_id',$sub_department_id]
        ])
        ->whereNull('deleted_at')
        ->exists())  return response()->json(['message'=>'Budget already exists'],422);
        
        try 
        {
            DB::beginTransaction();
            $budget = Budget::firstorCreate([
                'name'                  => $name,
                'factory_id'            => $factory_id,
                'sub_department_id'     => $sub_department_id,
                'description'           => $description,
                'periode_year'          => $year,
                'created_at'            => $current_time,
                'updated_at'            => $current_time,
                'created_user_id'       => Auth::user()->id,
                'updated_user_id'       => Auth::user()->id
            ]);

            foreach ($months as $key => $month) 
            {
                $first_date = $year.'-'.$key.'-01';
                $end_date   = Carbon::createFromFormat('Y-m-d', $first_date)->endOfMonth()->format('Y-m-d');
                
                DetailBudget::firstorCreate([
                    'budget_id'             => $budget->id,
                    'periode_month'         => $month,
                    'periode_by_date_start' => $first_date,
                    'periode_by_date_end'   => $end_date,
                    'total'                 => 0,
                    'created_at'            => $current_time,
                    'updated_at'            => $current_time,
                    'created_user_id'       => Auth::user()->id,
                    'updated_user_id'       => Auth::user()->id
                ]);
            }
           
            
    		
            DB::commit();
            
    	} catch (Exception $e) {
    		DB::rollBack();
    		$message = $e->getMessage();
    		ErrorHandler::db($message);
        }

        $url_redirect = route('budget.edit',$budget->id);
        return response()->json($url_redirect,200);

    }

    public function edit($id)
    {
        $budget         = Budget::find($id);
        $detail_budgets = $budget->detailBudget()->orderby('periode_by_date_start','asc')->get();

        return view('budget.edit',compact('budget','detail_budgets'));
    }

    public function update(Request $request, $id)
    {
        $current_time   = $request->current_time;
        $budgets        = json_decode($request->budgets);

        try 
        {
            DB::beginTransaction();
            
            foreach ($budgets as $key => $value) 
            {
               $detail_budget = DetailBudget::find($value->id);
               if($detail_budget)
               {
                    $detail_budget->total              = sprintf('%0.8f',$value->total);
                    $detail_budget->updated_at         = $current_time;
                    $detail_budget->updated_user_id    = auth::user()->id;
                    $detail_budget->save();
               }
            }
            
    		 DB::commit();
            
    	} catch (Exception $e) {
    		DB::rollBack();
    		$message = $e->getMessage();
    		ErrorHandler::db($message);
        }

        $request->session()->flash('message', 'success_2');
        return response()->json(200);
        
    }

    public function destroy($id)
    {
        Budget::find($id)
        ->update([
            'deleted_user_id'   => auth::user()->id,
            'deleted_at'        => carbon::now()
        ]);

        return response()->json(200);

    }
}
