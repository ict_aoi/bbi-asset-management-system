<?php namespace App\Http\Controllers;

use DB;
use Auth;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\Factory;
use App\Models\Company;


use App\Models\Erp\AccountingFactoryLocation;

class FactoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request)
    {
        $msg = $request->session()->get('message');
        return view('factory.index',compact('msg'));
    }

    public function create(Request $request)
    {
        if($request->session()->has('message')) $request->session()->forget('message');

        $erp_accounting_locations   = AccountingFactoryLocation::pluck('accounting_location_name','accounting_location_id')->all();
        $companies                  = Company::where('id',auth::user()->company_id)->whereNull('delete_at')->pluck('name','id')->all();
        
        return view('factory.create',compact('companies','erp_accounting_locations'));
    }

    public function data()
    {
    	if(request()->ajax())
        {
            $data = db::select(db::raw("select factories.id AS id
            ,factories.code
            ,factories.name
            ,companies.name as company_name
            ,factories.description
            ,factories.description
            ,factories.erp_accounting_location_id
            from factories 
            left join companies on factories.company_id = companies.id 
            where factories.delete_at is null
            and factories.company_id = '".auth::user()->company_id."'
            order by companies.name desc,factories.name desc"));
            
            return datatables()->of($data)
            ->editColumn('code',function($data){
            	return strtoupper($data->code);
            })
            ->editColumn('company_name',function($data){
            	return ucwords($data->company_name);
            })
            ->editColumn('name',function($data){
            	return ucwords($data->name);
            })
            ->editColumn('description',function($data){
                return ucwords($data->description);
            })
            ->addColumn('action', function($data) {
                return view('factory._action', [
                    'model' => $data,
                    'edit' => route('factory.edit',$data->id),
                    'delete' => route('factory.destroy',$data->id),
                ]);
            })
            ->rawColumns(['delete_at','action'])
            ->make(true);
        }
    }

    public function store(Request $request)
    {
    	$this->validate($request,[
    		'name'                          =>'required',
    		'company'                       =>'required',
    		'code'                          =>'required',
    		'erp_accounting_location'       =>'required'
    	]);

        if (Factory::where('code',strtolower($request->code))
        ->whereNull('delete_at')
        ->exists())
        {
    		return response()->json(['message'=>'Kode perusahaan sudah ada, silahkan cari Kode perusahaan lain'],422);
        }

        if (Factory::where('name',strtolower($request->name))
        ->whereNull('delete_at')
        ->exists())
        {
    		return response()->json(['message'=>'Nama perusahaan sudah ada, silahkan cari nama perusahaan lain'],422);
        }

        try 
        {
    		DB::beginTransaction();
    		Factory::firstorCreate([
				'code'                          => strtolower($request->code),
				'name'                          => strtolower($request->name),
				'company_id'                    => $request->company,
				'erp_accounting_location_id'    => $request->erp_accounting_location,
				'description'                   => strtolower($request->description),
    		]);
            DB::commit();
            $request->session()->flash('message', 'success');
    		return response()->json('success',200);

        } catch (Exception $e)
        {
    		DB::rollBack();
    		$message = $e->getMessage();
    		ErrorHandler::db($message);
    	}

    }

    public function edit(Request $request,$id)
    {
        if($request->session()->has('message')) $request->session()->forget('message');

		$factory                    = Factory::find($id);
        $companies                  = Company::whereNull('delete_at')->pluck('name','id')->all();
        $erp_accounting_locations   = AccountingFactoryLocation::pluck('accounting_location_name','accounting_location_id')->all();
        
        return view('factory.edit',compact('companies','factory','erp_accounting_locations'));
    }

    public function update(Request $request,$id)
    {
        $this->validate($request,[
            'name'      =>'required',
    		'company'   =>'required',
    		'code'      =>'required'
        ]);

        if (Factory::where([
            ['code',strtolower($request->code)],
            ['id','!=',$id],
        ])
        ->whereNull('delete_at')
        ->exists())
        {
    		return response()->json(['message'=>'Kode perusahaan sudah ada, silahkan cari Kode perusahaan lain'],422);
        }

        if (Factory::where([
            ['name',strtolower($request->code)],
            ['id','!=',$id],
        ])
        ->whereNull('delete_at')
        ->exists())
        {
    		return response()->json(['message'=>'Nama perusahaan sudah ada, silahkan cari nama perusahaan lain'],422);
        }

        try 
        {
            DB::beginTransaction();
            
    	    $factory                                = Factory::findorFail($id);
            $factory->code                          = strtolower($request->code);
            $factory->name                          = strtolower($request->name);
            $factory->company_id                    = $request->company;
            $factory->description                   = strtolower($request->description);
            if($request->exists('erp_accounting_location')) $factory->erp_accounting_location_id    = $request->erp_accounting_location;
            $factory->save();

            DB::commit();
            $request->session()->flash('message', 'success');
    		return response()->json('success',200);

        } catch (Exception $e)
        {
    		DB::rollBack();
    		$message = $e->getMessage();
    		ErrorHandler::db($message);
        }
    }

    public function destroy($id)
    {
        $factory                 = Factory::findorFail($id);
        $factory->delete_at      = carbon::now();
        $factory->save();

        return response()->json('success',200);
    }
}
