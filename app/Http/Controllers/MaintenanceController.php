<?php namespace App\Http\Controllers;
use DB;
use Auth;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Models\User;
use App\Models\Area;
use App\Models\Asset;
use App\Models\Company;
use App\Models\Factory;
use App\Models\Category;
use App\Models\AssetType;
use App\Models\Department;
use App\Models\SubDepartment;
use App\Models\DesignTable;
use App\Models\AssetMovement;
use App\Models\AssetInformationField;


class MaintenanceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $factories          = Factory::where('company_id',auth::user()->company_id)->whereNull('delete_at')->pluck('name', 'id')->all();
        $sub_departments    = SubDepartment::where('is_other',false)
        ->whereIn('code',['ict','elk','ga','me'])
        ->whereNull('delete_at')
        ->orderBy('code','asc')
        ->pluck('name', 'id')
        ->all();

        $sub_department_id  = auth::user()->sub_department_id;
        $factory_id         = auth::user()->factory_id;

    	return view('maintenance.index',compact('factories','sub_departments','sub_department_id','factory_id'));
    }

    public function data(Request $request)
    {
        if(request()->ajax()) 
        {
            $company_id         = $request->company;
            $factory_id         = $request->factory;
            $sub_department_id  = $request->sub_department;
            $asset_type_id      = $request->asset_type;
            
            $data = DB::table('asset_v')
            ->where('status','rusak')
            ->where(function($query) use($company_id)
            {
                $query = $query->Where('origin_company_location_id','LIKE',"%$company_id%");
            })
            ->where(function($query) use($factory_id)
            {
                $query = $query->Where('origin_factory_location_id','LIKE',"%$factory_id%");
            })
            ->where(function($query) use($sub_department_id)
            {
                $query = $query->Where('origin_sub_department_location_id','LIKE',"%$sub_department_id%");
            })
            ->where(function($query) use($asset_type_id)
            {
                $query = $query->Where('asset_type_id','LIKE',"%$asset_type_id%");
            });

            return datatables()->of($data)
            ->addColumn('action', function($data) {
                return view('maintenance._action', [
                    'model'         => $data,
                    'editStatus'    => $data->id,
                ]);
            })
            ->editColumn('last_company_name',function($data){
            	return ucwords($data->last_company_name);
            })
            ->editColumn('last_factory_name',function($data){
            	return ucwords($data->last_factory_name);
            })
            ->editColumn('last_department_name',function($data){
            	return ucwords($data->last_department_name);
            })
            ->editColumn('no_inventory_manual',function($data){
            	return strtoupper($data->no_inventory_manual);
            })
            ->editColumn('origin_factory_name',function($data){
            	return ucwords($data->origin_factory_name);
            })
            ->editColumn('origin_department_name',function($data){
            	return ucwords($data->origin_department_name);
            })
            ->editColumn('last_factory_name',function($data){
            	return ucwords($data->last_factory_name);
            })
            ->editColumn('model',function($data){
            	return ucwords($data->model);
            })
            ->editColumn('asset_type',function($data){
            	return ucwords($data->asset_type);
            })
            ->rawColumns(['action'])
            ->make(true);
        }
    }

    public function store(Request $request)
    {
    	$this->validate($request, [
			'factory_id'        => 'required',
			'sub_department_id' => 'required',
        ]);
        
        try
        {
            DB::beginTransaction();
            $from_factory           = Factory::find($request->factory_id);
            $from_sub_department    = SubDepartment::find($request->sub_department_id);
            $checkout_date          = $request->current_time;
            $assets                 = json_decode($request->assets);

            $asset                  = Asset::find($request->asset_id); 
            $status                 = strtolower($request->status);
            
            $old_asset_movement = AssetMovement::find($asset->last_asset_movement_id);
            if(strtolower($old_asset_movement->status) == 'rusak')
            {
                $old_asset_movement->note               = strtolower(trim($request->root_cause));
                $old_asset_movement->repaired_date      = $checkout_date;
                $old_asset_movement->technician_name    = strtolower(trim($request->teknisi));
                $old_asset_movement->save();
            }

            $asset_movement = AssetMovement::firstorcreate([
                'asset_id'                          => $asset->id,
                'status'                            => 'siap digunakan',
                'technician_name'                   => ($request->teknisi ? strtolower($request->teknisi) : null),
                'to_company_location'               => auth::user()->company_id,
                'to_factory_location'               => $from_factory->id,
                'to_department_location'            => $from_sub_department->department_id,
                'to_sub_department_location'        => $from_sub_department->id,
                'to_area_location'                  => null,
                'to_employee_nik'                   => auth::user()->nik,
                'to_employee_name'                  => auth::user()->name,
                'movement_date'                     => $checkout_date,
                'note'                              => 'asset ini sebelumnya nya rusak lalu di perbaiki',
            ]);
            
            $asset->last_asset_movement_id          = $asset_movement->id;
            
            $asset->status                          = 'siap digunakan';
            $asset->last_movement_date              = $checkout_date;
            $asset->last_area_location              = null;
            $asset->last_used_by                    = null;
           
            $asset->save();
           
            DB::commit();
            return response()->json('success',200);

        } catch (Exception $e)
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    public function getAssetType(Request $request)
    {
        $sub_department_id      = $request->sub_department_id;
        $company_id         = auth::user()->company_id;

        $asset_type_id  = $request->asset_type_id;
        $asset_types = AssetType::whereNull('delete_at')
        ->where([
            ['sub_department_id',$sub_department_id],
            ['company_id',$company_id],
            ['is_consumable',false],
        ])
        ->pluck('name','id')
        ->all();
        
        return response()->json(['asset_types' => $asset_types,'asset_type_id' => $asset_type_id]);

    }
}
