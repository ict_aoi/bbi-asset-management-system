<?php namespace App\Http\Controllers;

use DB;
use Auth;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\Calendar;


class CalendarController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $msg = $request->session()->get('message');
        return view('calendar.index',compact('msg'));
    }

    public function data()
    {
        if(request()->ajax()) 
        {
            $data = Calendar::orderby('created_at','desc');
            return datatables()->of($data)
            ->editColumn('event_start',function($data)
            {
            	return $data->event_start->format('d/M/Y');
            })
            ->editColumn('event_end',function($data)
            {
            	return $data->event_end->format('d/M/Y');
            })
            ->addColumn('action', function($data) {
                return view('calendar._action', [
                    'model'     => $data,
                    'edit'      => route('calendar.edit',$data->id),
                    'delete'    => route('calendar.destroy',$data->id),
                ]);
            })
            ->make(true);
        }
    }

    public function create(Request $request)
    {
        if($request->session()->has('message')) $request->session()->forget('message');
        return view('calendar.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        $event_name     = strtolower($request->name);
        $event_start    = ($request->event_start)? Carbon::createFromFormat('d/m/Y', $request->event_start)->format('Y-m-d') : null; // ini etd.
        $event_end      = ($request->event_end)? Carbon::createFromFormat('d/m/Y', $request->event_end)->format('Y-m-d') : null;

        if($event_start && $event_name && $event_end)
        {
            if(Calendar::whereNull('deleted_at')
            ->where([
                ['event_name',$event_name],
                ['event_start',$event_start],
                ['event_end',$event_end],
            ])->exists())
                return response()->json(['message' => 'Event already exists'], 422);
    
    
            try
            {
                DB::beginTransaction();
                Calendar::firstorCreate([
                    'event_name'        => $event_name,
                    'event_start'       => $event_start,
                    'event_end'         => $event_end,
                    'created_at'        => ($request->current_time ? $request->current_time : carbon::now()),
                    'updated_at'        => ($request->current_time ? $request->current_time : carbon::now()),
                    'created_user_id'   => auth::user()->id,
                    'updated_user_id'   => auth::user()->id,
                ]);
                
                DB::commit();
                $request->session()->flash('message', 'success');
                return response()->json('success',200);
    
            } catch (Exception $e)
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }
    }

    public function edit(Request $request,$id)
    {
        $calendar = Calendar::find($id);
        if($request->session()->has('message')) $request->session()->forget('message');
        return view('calendar.edit',compact('calendar'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        $event_name     = strtolower($request->name);
        $event_start    = ($request->event_start)? Carbon::createFromFormat('d/m/Y', $request->event_start)->format('Y-m-d') : null; // ini etd.
        $event_end      = ($request->event_end)? Carbon::createFromFormat('d/m/Y', $request->event_end)->format('Y-m-d') : null;


        if($event_start && $event_name && $event_end)
        {
            if(Calendar::whereNull('deleted_at')
            ->where([
                ['event_name',$event_name],
                ['event_start',$event_start],
                ['event_end',$event_end],
                ['id','!=',$id],
            ])->exists())
                return response()->json(['message' => 'Event already exists'], 422);
    
    
            try
            {
                DB::beginTransaction();
                $calendar                   = Calendar::find($id);
                $calendar->event_name       = $event_name;
                $calendar->event_start      = $event_start;
                $calendar->event_end        = $event_end;
                $calendar->updated_at       = ($request->current_time ? $request->current_time : carbon::now());
                $calendar->updated_user_id  = auth::user()->id;
                $calendar->save();
                
                DB::commit();
                $request->session()->flash('message', 'success_2');
                return response()->json('success',200);
    
            } catch (Exception $e)
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
