<?php namespace App\Http\Controllers;


use DB;
use Hash;
use File;
use Auth;
use Config;
use StdClass;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Intervention\Image\ImageManagerStatic as Image;

use App\Models\User;
use App\Models\Asset;
use App\Models\Company;
use App\Models\Factory;
use App\Models\AssetType;
use App\Models\SubDepartment;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')
        ->except([
            'dashboardEis',
            'dashboardDataSummary',
            'dashboardDataPie',
            'dashboardDataPieDepartmentSummary',
            'dashboardDataTableSummary',
        ]);
    }

    public function index(request $request)
    {
        $dashboard_eis = false;

        if(auth::user()->is_super_admin)
        {
            if($request->has('detail'))
            {
                $detail = $request->detail;
                if($request->has('sub_department'))
                {
                    $sub_department = $request->sub_department;
                    return view('dashboard_2',compact('detail','sub_department','dashboard_eis'));
                } 
                else return view('dashboard_1',compact('detail','dashboard_eis'));
            }else return view('home',compact('dashboard_eis'));
        }else
        {
            $is_bbis_1 = Factory::where([
                ['id',auth::user()->factory_id],
                ['code','bbis'],
            ])
            ->exists();

            $_sub_department = SubDepartment::where('id',auth::user()->sub_department_id)->first();

            if(strtolower($_sub_department->name) == 'mekanik') $sub_department = 'mechanical';
            else if(strtolower($_sub_department->name) == 'elektrik') $sub_department = 'electric';
            else if(strtolower($_sub_department->name) == 'general affair') $sub_department = 'ga';
            else if(strtolower($_sub_department->name) == 'ict') $sub_department = 'ict';
            else $sub_department = null;
            
            $detail     = ($is_bbis_1 ? 'bbis' : 'bbis');
            return view('dashboard_2',compact('detail','sub_department','dashboard_eis'));
        }
        
    }

    public function dashboardEis(request $request)
    {
        $dashboard_eis = true;
        if($request->has('detail'))
        {
            $detail = $request->detail;
            if($request->has('department'))
            {
                $department = $request->department;
                return view('dashboard_2',compact('detail','department','dashboard_eis'));
            } 
            else return view('dashboard_1',compact('detail','dashboard_eis'));
        }else return view('home',compact('dashboard_eis'));
    }

    public function dashboardDataSummary(request $request)
    {
        $has_detail         = ($request->has('detail'))? true : false;
        $has_sub_department = ($request->has('sub_department'))? true : false;

        if($has_sub_department)
        {
            $_sub_department = $request->sub_department;
            if($_sub_department == 'mechanical' || $_sub_department == 'mechanic') $sub_department     = SubDepartment::where('name','mekanik')->first();
            else if($_sub_department == 'electric') $sub_department     = SubDepartment::where('name','elektrik')->first();
            else if($_sub_department == 'ga') $sub_department     = SubDepartment::where('name','general affair')->first();
            else if($_sub_department == 'ict') $sub_department     = SubDepartment::where('name','ict')->first();

            $sub_department_id  = ($sub_department ? $sub_department->id : null);
        }else $sub_department_id = null;

        $company            = Company::where('code','bbi')->first();
        $company_id         = (auth::check() ? auth::user()->company_id : $company->id);
        $factory_id         = null;

        if($request->has('detail'))
        {
            $detail     = $request->detail;
            $factory    = Factory::where('code',$detail)->first();
            $factory_id = ($factory ? $factory->id : null);

            if($request->has('sub_department'))
            {
                $_sub_department     = $request->sub_department;
                if($_sub_department == 'mechanical' || $_sub_department == 'mechanic') $sub_department     = SubDepartment::where('name','mekanik')->first();
                else if($_sub_department == 'electric') $sub_department     = SubDepartment::where('name','elektrik')->first();
                else if($_sub_department == 'ga') $sub_department     = SubDepartment::where('name','general affair')->first();
                else if($_sub_department == 'ict') $sub_department     = SubDepartment::where('name','ict')->first();
                
                $sub_department_id = ($sub_department ? $sub_department->id : $sub_department_id);
            }
        }

        $data = DB::select(db::raw("SELECT *  FROM dashboard_summary_asset('$company_id','$factory_id','$sub_department_id');" ));
        return response()->json($data);
    }

    public function dashboardDataPie(request $request)
    {
        $has_sub_department = ($request->has('sub_department'))? true : false;
        if($has_sub_department)
        {
            $_sub_department = $request->sub_department;
            if($_sub_department == 'mechanical' || $_sub_department == 'mechanic') $sub_department     = SubDepartment::where('name','mekanik')->first();
            else if($_sub_department == 'electric') $sub_department     = SubDepartment::where('name','elektrik')->first();
            else if($_sub_department == 'ga') $sub_department     = SubDepartment::where('name','general affair')->first();
            else if($_sub_department == 'ict') $sub_department     = SubDepartment::where('name','ict')->first();

            $sub_department_id  = ($sub_department ? $sub_department->id : null);
        }else $sub_department_id = null;

        $_factory           = $request->factory;
        $factory            = Factory::where('code',$_factory)->first();
        $company            = Company::where('code','bbi')->first();

        $status             = ['Used','Stock','Broken','Obsolete'];
        $total_assets       = DB::select(db::raw("SELECT *  FROM dashboard_pie_asset('$company->id','$factory->id','$sub_department_id');" ));
        
        
        return response()->json([
            'asset_status_legend' => $status,
            'total_asset'         => $total_assets,
            'factory'             => $_factory
        ]);
    }

    public function dashboardDataPieDepartmentSummary(request $request)
    {
        $company            = Company::where('code','bbi')->first();
        $factory            = $request->factory;
        $_sub_department    = $request->sub_department;
        $company_id         = (auth::check() ? auth::user()->company_id : $company->id);
        
        if($_sub_department == 'mechanical' || $_sub_department == 'mechanic') $sub_department     = SubDepartment::where('name','mekanik')->first();
        else if($_sub_department == 'electric') $sub_department     = SubDepartment::where('name','elektrik')->first();
        else if($_sub_department == 'ga') $sub_department     = SubDepartment::where('name','general affair')->first();
        else if($_sub_department == 'ict') $sub_department     = SubDepartment::where('name','ict')->first();

        $sub_department_id  = ($sub_department ? $sub_department->id : null);
        $factory        = Factory::where('code',$factory)->first();

        $status         = ['Used','Stock','Broken','Obsolete'];
        $total_assets   = DB::select(db::raw("SELECT *  FROM dashboard_pie_asset('$company->id','$factory->id','$sub_department_id');" ));
        
        
        return response()->json([
            'asset_status_legend'   => $status,
            'total_asset'           => $total_assets,
            'sub_department'        => $_sub_department
        ]);
    }

    public function dashboardDataTableSummary(request $request)
    {
        if(request()->ajax()) 
        {
            $detail             = $request->detail;
            $_sub_department    = $request->sub_department;
            $factory            = Factory::where('code',$detail)->first();

            if($_sub_department == 'mechanical' || $_sub_department == 'mechanic') $sub_department     = SubDepartment::where('name','mekanik')->first();
            else if($_sub_department == 'electric') $sub_department  = SubDepartment::where('name','elektrik')->first();
            else if($_sub_department == 'ga') $sub_department       = SubDepartment::where('name','general affair')->first();
            else if($_sub_department == 'ict') $sub_department      = SubDepartment::where('name','ict')->first();

            
            $data       = DB::select(db::raw("SELECT *  FROM dashboard_detail_asset('$factory->id','$sub_department->id');" ));

            return datatables()->of($data)
            ->make(true);
        }
    }

    public function accountSetting()
    {
        return view('account_setting');
    }

    public function showAvatar($filename)
    {
        $path = Config::get('storage.avatar');
        $resp = response()->download($path . '/' . $filename);
        $resp->headers->set('Content-Disposition', 'inline');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');
        return $resp;
    }

    public function updateAccount(request $request,$id)
    {
        $avatar = Config::get('storage.avatar');
        if (!File::exists($avatar)) File::makeDirectory($avatar, 0777, true);

        $old_password = $request->old_password;
        $password = $request->new_password;
        $retype_password = $request->retype_password;
        $user = User::find($id);
        $image = null;
        if($password)
        {
            if (!Hash::check($old_password, $user->password)) return redirect()->back()
            ->withErrors([
                'old_password' => 'Password tidak sama dengan yang saat ini',
            ]);

            if ($password != $retype_password) return redirect()->back()
            ->withErrors([
                'retype_password' => 'Password yang anda masukan tidak sama dengan password baru',
            ]);
        }

       
        if ($request->hasFile('photo')) 
        {
            if ($request->file('photo')->isValid()) 
            {
                $old_file = $avatar . '/' . $user->photo;
                
                if(File::exists($avatar)) File::delete($old_file);
                
                $file = $request->file('photo');
                $now = Carbon::now()->format('u');
                $filename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                $image = $filename . '_' . $now . '.' . $file->getClientOriginalExtension();

                //Resize Function
                $image_resize = Image::make($file->getRealPath());
                $height = $image_resize->height();
                $width = $image_resize->width();

                $newWidth = 130;
                $newHeight = 130;
                $image_resize->resize($newWidth, $newHeight);
                $image_resize->save($avatar.'/'.$image);
            }
        }

        if ($password) $user->password = bcrypt($password);
        if ($image) $user->photo = $image;
        $user->save();

        return redirect()->route('accountSetting'); //->withSuccess();
        
        
    }
}
