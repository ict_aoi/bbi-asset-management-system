<?php namespace App\Http\Controllers;

use DB;
use Auth;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\BcType;

class BcTypeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $msg = $request->session()->get('message');
        return view('bc_type.index',compact('msg'));
    }

    public function data()
    {
        if(request()->ajax()) 
        {
            $data = BcType::whereNull('deleted_at')
            ->orderby('created_at','desc');
            return datatables()->of($data)
            ->addColumn('action', function($data) {
                return view('bc_type._action', [
                    'model'     => $data,
                    'edit'      => route('bcType.edit',$data->id),
                    'delete'    => route('bcType.destroy',$data->id),
                ]);
            })
            ->make(true);
        }
    }
    
    public function create(Request $request)
    {
        if($request->session()->has('message')) $request->session()->forget('message');
        return view('bc_type.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'code' => 'required'
        ]);

        $current_time = ($request->current_time ? $request->current_time : carbon::now());
       
        if(BcType::whereNull('deleted_at')->where('code',strtolower(trim($request->code)))->exists())
            return response()->json(['message' => 'Code already exists'], 422);


        try
        {
            DB::beginTransaction();
            BcType::firstorCreate([
                'code'              => strtolower(trim($request->code)),
                'description'       => strtolower(trim($request->description)),
                'created_user_id'   => auth::user()->id,
                'updated_user_id'   => auth::user()->id,
                'updated_at'        => $current_time,
                'created_at'        => $current_time,
                'deleted_at'        => null,
            ]);
            
            DB::commit();
            $request->session()->flash('message', 'success');
            return response()->json('success',200);

        } catch (Exception $e)
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    public function edit(Request $request,$id)
    {
        $bc_type = BcType::find($id);
        if($request->session()->has('message')) $request->session()->forget('message');
        return view('bc_type.edit',compact('bc_type'));
    }

    
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'code' => 'required'
        ]);

        $current_time = ($request->current_time ? $request->current_time : carbon::now());

        if(BcType::whereNull('deleted_at')
            ->where('code',strtolower(trim($request->code)))
            ->where('id','!=',$id)->exists())
            return response()->json(['message' => 'Code already exists'], 422);

        try
        {
            DB::beginTransaction();
            $bc_type                    = BcType::find($id);
            $bc_type->code              = strtolower(trim($request->code));
            $bc_type->description       = strtolower(trim($request->description));
            $bc_type->updated_at        = $current_time;
            $bc_type->updated_user_id   = auth::user()->id;
            $bc_type->save();
            
            DB::commit();
            $request->session()->flash('message', 'success_2');
            return response()->json('success',200);

        } catch (Exception $e)
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
        
    }

    public function destroy($id)
    {
        $current_time   = carbon::now();
        $bc_type        = BcType::findorFail($id)->update([
            'deleted_at'        => $current_time,
            'deleted_user_id'   => auth::user()->id,
        ]);
        
        return response()->json(200);
    }
}
