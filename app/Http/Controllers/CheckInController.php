<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Config;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Models\User;
use App\Models\Area;
use App\Models\Asset;
use App\Models\BcType;
use App\Models\Company;
use App\Models\Factory;
use App\Models\Temporary;
use App\Models\AssetType;
use App\Models\Department;
use App\Models\DesignTable;
use App\Models\Manufacture;
use App\Models\SubDepartment;
use App\Models\AssetMovement;
use App\Models\ConsumableAsset;
use App\Models\RentalAgreement;
use App\Models\ConsumableAssetDetail;
use App\Models\AssetInformationField;

use App\Models\DiasAoi\AssetDev;
use App\Models\DiasAoi\AssetLive;
use App\Models\DiasAoi\CompanyDev;
use App\Models\DiasAoi\CompanyLive;
use App\Models\DiasAoi\FactoryDev;
use App\Models\DiasAoi\FactoryLive;
use App\Models\DiasAoi\BcTypeLive;
use App\Models\DiasAoi\BcTypeDev;
use App\Models\DiasAoi\DepartmentDev;
use App\Models\DiasAoi\DepartmentLive;
use App\Models\DiasAoi\AssetTypeDev;
use App\Models\DiasAoi\AssetTypeLive;
use App\Models\DiasAoi\ManufactureDev;
use App\Models\DiasAoi\ManufactureLive;
use App\Models\DiasAoi\RentalAgreementLive;
use App\Models\DiasAoi\RentalAgreementDev;
use App\Models\DiasAoi\AssetInformationFieldLive;
use App\Models\DiasAoi\AssetInformationFieldDev;
use App\Models\DiasAoi\UserDev;
use App\Models\DiasAoi\UserLive;
use App\Models\DiasAoi\AssetMovementLive;
use App\Models\DiasAoi\AssetMovementDev;

class CheckInController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $sub_department_id  = auth::user()->sub_department_id;
        $factory_id         = auth::user()->factory_id;
        $factories          = Factory::where('company_id',auth::user()->company_id)->whereNull('delete_at')->pluck('name', 'id')->all();
       
       
        if(Factory::where([
                ['id',$factory_id],
                ['code','oth'],
            ])
            ->exists()
        )
        {
            $filters = ['ict','elk','ga','me','oth'];
        }else
        {
            $filters = ['ict','elk','ga','me'];
        }
        
        $sub_departments    = SubDepartment::whereIn('code',$filters)
        ->whereNull('delete_at')
        ->pluck('name', 'id')
        ->all();
        
       
        
        return view('checkin.index',compact('factories','sub_departments','sub_department_id','factory_id'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'factory_id'        => 'required',
            'sub_department_id' => 'required',
            'assets'            => 'required',
        ]);

        try
        {
            DB::beginTransaction();
            $checkin_date           = ($request->current_time ? $request->current_time : carbon::now()->toDateTimeString());
            $checkin_area_location  = $request->checkin_area_location;
            $destination_factory    = Factory::find($request->factory_id);
            $sub_department         = SubDepartment::find($request->sub_department_id);
            $assets                 = json_decode($request->assets);
            $app_env                = Config::get('app.env');
            
            foreach ($assets as $key => $value) 
            {
                if($value->flag == '1')
                {
                    if($app_env == 'dev')
                    {
                        $copy_asset                 = AssetDev::find($value->id);
                        $copy_asset_type            = AssetTypeDev::find($copy_asset->asset_type_id);
                        $copy_rental_agreement      = RentalAgreementDev::find($copy_asset->no_rent);
                        $copy_asset_informations    = AssetInformationFieldDev::where('asset_type_id',$copy_asset_type->id)->get();
                        $copy_source_company        = CompanyDev::find($copy_asset->origin_company_location_id);
                        $copy_source_factory        = FactoryDev::find($copy_asset->origin_factory_location_id);
                        $copy_source_department     = DepartmentDev::find($copy_asset->origin_department_location_id);
                        $copy_last_company          = CompanyDev::find($copy_asset->last_company_location_id);
                        $copy_last_factory          = FactoryDev::find($copy_asset->last_factory_location_id);
                        $copy_last_department       = DepartmentDev::find($copy_asset->last_department_location_id);
                        $copy_bc_type_out           = BcTypeDev::find($copy_rental_agreement->bc_type_id);
                        $copy_manufacture           = ManufactureDev::find($copy_asset->manufacture_id);
                        $copy_user                  = UserDev::find($copy_asset->last_user_movement_id);
                        $update_asset_movement      = AssetMovementDev::find($copy_asset->last_asset_movement_id);
                       
                    } else if($app_env == 'live')
                    {
                        $copy_asset                 = AssetLive::find($value->id);
                        $copy_asset_type            = AssetTypeLive::find($copy_asset->asset_type_id);
                        $copy_rental_agreement      = RentalAgreementLive::find($copy_asset->no_rent);
                        $copy_source_company        = CompanyDev::find($copy_asset->origin_company_location_id);
                        $copy_source_factory        = FactoryLive::find($copy_asset->origin_factory_location_id);
                        $copy_source_department     = DepartmentDev::find($copy_asset->origin_department_location_id);
                        $copy_last_company          = CompanyLive::find($copy_asset->last_company_location_id);
                        $copy_last_factory          = FactoryLive::find($copy_asset->last_factory_location_id);
                        $copy_last_department       = DepartmentDev::find($copy_asset->last_department_location_id);
                        $copy_asset_informations    = AssetInformationFieldLive::where('asset_type_id',$copy_asset_type->id)->get();
                        $copy_bc_type_out           = BcTypeLive::find($copy_rental_agreement->bc_type_id);
                        $copy_manufacture           = ManufactureLive::find($copy_asset->manufacture_id);
                        $copy_user                  = UserLive::find($copy_asset->last_user_movement_id);
                        $update_asset_movement      = AssetMovementLive::find($copy_asset->last_asset_movement_id);
                        
                    }

                    //copy manufacture 
                    if($copy_manufacture)
                    {
                        if(!Manufacture::where('mapping_id',$copy_manufacture->id)->exists())
                        {
                            $new_manufacture = Manufacture::firstorCreate([
                                'name'              => strtolower(trim($copy_manufacture->name)),
                                'description'       => strtolower(trim($copy_manufacture->description)),
                                'department_id'     => $copy_last_department->mapping_id,
                                'sub_department_id' => $copy_last_department->mapping_sub_department_id,
                                'created_at'        => $copy_manufacture->created_at,
                                'updated_at'        => $copy_manufacture->updated_at,
                                'company_id'        => $copy_last_company->mapping_id,
                                'mapping_id'        => $copy_manufacture->id,
                                'create_user_id'    => Auth::user()->id
                            ]);
                        }else $new_manufacture = Manufacture::where('mapping_id',$copy_manufacture->id)->first();
                    }

                    //update mapping bc type
                    $db_bbi_bc_type = BcType::where('code',$copy_bc_type_out->code)->first();
                    if($db_bbi_bc_type)
                    {
                        if(!$db_bbi_bc_type->mapping_id)
                        {
                            $db_bbi_bc_type->update([
                                'mapping_id' => $copy_bc_type_out->id
                            ]);
                        }
                    }else response()->json(['message' => 'Bc type '.$copy_bc_type_out->code.' not found di this db'],422);

                    //insert rental agreement
                    if(!RentalAgreement::where('mapping_id',$copy_rental_agreement->id)->exists())
                    {
                        $new_rental_agreement =   RentalAgreement::FirstOrCreate([
                            'factory_id'                    => $copy_source_factory->mapping_id,
                            'destination_factory_id'        => $copy_last_factory->mapping_id,
                            'sub_department_id'             => $copy_last_department->mapping_sub_department_id,
                            'bc_type_id'                    => $db_bbi_bc_type->id,
                            'no_bc'                         => ($copy_rental_agreement->no_bc ? $copy_rental_agreement->no_bc : null),
                            'no_aju'                        => ($copy_rental_agreement->no_aju ? $copy_rental_agreement->no_aju : null),
                            'no_registration'               => ($copy_rental_agreement->no_registration ? $copy_rental_agreement->no_registration : null),
                            'date_registration'             => ($copy_rental_agreement->date_registration ? $copy_rental_agreement->date_registration : null),
                            'no_agreement'                  => $copy_rental_agreement->no_agreement,
                            'price'                         => $copy_rental_agreement->price,
                            'start_date'                    => $copy_rental_agreement->start_date,
                            'end_date'                      => $copy_rental_agreement->end_date,
                            'duration_in_day'               => $copy_rental_agreement->duration_in_day,
                            'description'                   => $copy_rental_agreement->description,
                            'created_at'                    => $copy_rental_agreement->created_at,
                            'updated_at'                    => $copy_rental_agreement->updated_at,
                            'is_internal'                   => $copy_rental_agreement->is_internal,
                            'is_agreement_for_rental'       => $copy_rental_agreement->is_agreement_for_rental,
                            'mapping_id'                    => $copy_rental_agreement->id,
                            'receive_date'                  => $checkin_date,
                        ]);
                    }else
                    {
                        $new_rental_agreement = RentalAgreement::where('mapping_id',$copy_rental_agreement->id)->first();
                        $new_rental_agreement->receive_date = $checkin_date;
                        $new_rental_agreement->save();
                    }

                    if(!AssetType::whereNotNull('mapping_id')
                    ->where('mapping_id',$copy_asset_type->id)
                    ->exists())
                    {
                        $new_asset_type     = AssetType::firstorCreate([
                            'company_id'            => $copy_last_company->mapping_id,
                            'department_id'         => $copy_last_department->mapping_id,
                            'sub_department_id'     => $copy_last_department->mapping_sub_department_id,
                            'code'                  => $copy_asset_type->code,
                            'is_consumable'         => $copy_asset_type->is_consumable,
                            'name'                  => strtolower($copy_asset_type->name),
                            'description'           => strtolower($copy_asset_type->description),
                            'create_user_id'        => Auth::user()->id,
                            'delete_at'             => null,
                            'mapping_id'            => $copy_asset_type->id
                        ]);
    
                        foreach ($copy_asset_informations as $key => $copy_asset_information) 
                        {
                            AssetInformationField::firstorCreate([
                                'asset_type_id'     => $new_asset_type->id,
                                'type'              => strtolower($copy_asset_information->type),
                                'name'              => strtolower($copy_asset_information->name),
                                'label'             => strtolower($copy_asset_information->label),
                                'value'             => strtolower($copy_asset_information->value),
                                'required'          => $copy_asset_information->required,
                                'is_custom'         => $copy_asset_information->is_custom,
                                'lookup_id'         => $copy_asset_information->lookup_id,
                                'created_at'        => $copy_asset_information->created_at,
                                'updated_at'        => $copy_asset_information->updated_at,
                                'create_user_id'    => Auth::user()->id
                            ]);
                        }
                    }else
                    {
                        $new_asset_type = AssetType::whereNotNull('mapping_id')
                        ->where('mapping_id',$copy_asset_type->id)
                        ->first();
                    }


                    $is_asset_is_copied      = Asset::where([ 
                        ['mapping_id',$copy_asset->id],
                        ['origin_company_location_id',$copy_source_company->mapping_id],
                        ['origin_factory_location_id',$copy_source_factory->mapping_id],
                        ['origin_sub_department_location_id',$copy_source_department->mapping_sub_department_id],
                        ['asset_type_id',$new_asset_type->id],
                    ])
                    ->whereNull('delete_at')
                    ->exists();

                    if(!$is_asset_is_copied)
                    {
                        $new_asset = Asset::firstorCreate([
                            'asset_type_id'                     => $new_asset_type->id,
                            'company_id'                        => $copy_source_company->mapping_id,
                            'factory_id'                        => $copy_source_factory->mapping_id,
                            'department_id'                     => $copy_source_department->mapping_id,
                            'sub_department_id'                 => $copy_source_department->mapping_sub_department_id,
                            'origin_company_location_id'        => $copy_source_company->mapping_id,
                            'origin_factory_location_id'        => $copy_source_factory->mapping_id,
                            'origin_department_location_id'     => $copy_source_department->mapping_id,
                            'origin_sub_department_location_id' => $copy_source_department->mapping_sub_department_id,
                            'manufacture_id'                    => $new_manufacture->id,
                            'barcode'                           => $copy_asset->barcode,
                            'serial_number'                     => $copy_asset->serial_number,
                            'no_inventory'                      => $copy_asset->no_inventory,
                            'no_inventory_manual'               => $copy_asset->no_inventory_manual,
                            'description'                       => $copy_asset->description,
                            'erp_no_asset'                      => $copy_asset->erp_no_asset,
                            'erp_asset_id'                      => $copy_asset->erp_asset_id,
                            'model'                             => $copy_asset->model,
                            'purchase_number'                   => $copy_asset->purchase_number,
                            'supplier_name'                     => $copy_asset->supplier_name,
                            'receiving_date'                    => $copy_asset->receiving_date,
                            'image'                             => $copy_asset->image,
                            'erp_asset_group_id'                => $copy_asset->erp_asset_group_id,
                            'erp_asset_group_name'              => $copy_asset->erp_asset_group_name,
                            'erp_item_id'                       => $copy_asset->erp_item_id,
                            'erp_item_code'                     => $copy_asset->erp_item_code,
                            'referral_code'                     => $copy_asset->referral_code,
                            'sequence'                          => $copy_asset->sequence,
                            'custom_field_1'                    => $copy_asset->custom_field_1,
                            'custom_field_2'                    => $copy_asset->custom_field_2,
                            'custom_field_3'                    => $copy_asset->custom_field_3,
                            'custom_field_4'                    => $copy_asset->custom_field_4,
                            'custom_field_5'                    => $copy_asset->custom_field_5,
                            'custom_field_6'                    => $copy_asset->custom_field_6,
                            'custom_field_7'                    => $copy_asset->custom_field_7,
                            'custom_field_8'                    => $copy_asset->custom_field_8,
                            'custom_field_9'                    => $copy_asset->custom_field_9,
                            'custom_field_10'                   => $copy_asset->custom_field_10,
                            'custom_field_11'                   => $copy_asset->custom_field_11,
                            'custom_field_12'                   => $copy_asset->custom_field_12,
                            'custom_field_13'                   => $copy_asset->custom_field_13,
                            'custom_field_14'                   => $copy_asset->custom_field_14,
                            'custom_field_15'                   => $copy_asset->custom_field_15,
                            'custom_field_16'                   => $copy_asset->custom_field_16,
                            'custom_field_17'                   => $copy_asset->custom_field_17,
                            'custom_field_18'                   => $copy_asset->custom_field_18,
                            'custom_field_19'                   => $copy_asset->custom_field_19,
                            'custom_field_20'                   => $copy_asset->custom_field_20,
                            'status'                            => 'siap digunakan',
                            'last_company_location_id'          => $destination_factory->company_id,
                            'last_factory_location_id'          => $destination_factory->id,
                            'last_department_location_id'       => $sub_department->department_id,
                            'last_sub_department_location_id'   => $sub_department->id,
                            'no_rent'                           => $new_rental_agreement->id,
                            'is_rent'                           => true,
                            'last_user_movement_id'             => Auth::user()->id,
                            'create_user_id'                    => Auth::user()->id,
                            'delete_at'                         => null,
                            'created_at'                        => $copy_asset->created_at,
                            'updated_at'                        => $copy_asset->updated_at,
                            'last_movement_date'                => $checkin_date,
                            'mapping_id'                        => $copy_asset->id,
                        ]);
                    }else
                    {
                        $copy_asset = Asset::where([ 
                            ['mapping_id',$copy_asset->id],
                            ['origin_company_location_id',$copy_source_company->mapping_id],
                            ['origin_factory_location_id',$copy_source_factory->mapping_id],
                            ['origin_sub_department_location_id',$copy_source_department->mapping_sub_department_id],
                            ['asset_type_id',$new_asset_type->id],
                        ])
                        ->whereNull('delete_at')
                        ->first();
                    }

                    $asset_movement = AssetMovement::firstorCreate([
                        'asset_id'                          => $new_asset->id,
                        'status'                            => 'siap digunakan',
                        'from_company_location'             => $new_asset->origin_company_location_id,
                        'from_factory_location'             => $new_asset->origin_factory_location_id,
                        'from_department_location'          => $new_asset->origin_department_location_id,
                        'from_sub_department_location'      => $new_asset->origin_sub_department_location_id,
                        'from_employee_nik'                 => ($copy_user ? $copy_user->nik : null),
                        'from_employee_name'                => ($copy_user ? $copy_user->name : null),
                        'to_company_location'               => $destination_factory->company_id,
                        'to_factory_location'               => $destination_factory->id,
                        'to_department_location'            => $sub_department->department_id,
                        'to_sub_department_location'        => $sub_department->id,
                        'to_employee_nik'                   => auth::user()->nik,
                        'to_employee_name'                  => auth::user()->name,
                        'movement_date'                     => carbon::now(),
                        'note'                              => 'asset pinjaman dari pabrik '.$copy_source_factory->name.' ,departemen '.$copy_source_department->name,
                        'is_handover_asset'                 => true,
                        'rental_agreement_id'               => $new_asset->no_rent,
                    ]);

                    $new_asset->last_asset_movement_id = $asset_movement->id;
                    $new_asset->save();

                    $copy_asset->status = 'siap digunakan';
                    $copy_asset->save();

                    if($update_asset_movement)
                    {
                        $update_asset_movement->note = '['.$checkin_date.'] asset sudah di terima di aoi oleh '.auth::user()->name.'('.auth::user()->nik.'), '.$update_asset_movement->note;
                        $update_asset_movement->save();
                    }

                    Temporary::firstorCreate([
                        'column_1'  => $new_asset->origin_sub_department_location_id,
                        'column_2'  => 'sync_export_asset_per_department',
                    ]);
                }else
                {
                    $asset      = Asset::find($value->id);
                    $old_status = $asset->status;
    
                    if($old_status == 'dipindah tangan' || $old_status == 'dipinjam' || $old_status == 'dikembalikan')
                    {
                        if($old_status == 'dipindah tangan') $note = 'asset pindahan dari '.$asset->sourceCompany->name.' ,pabrik '.$asset->sourceFactory->name.' ,departemen '.$asset->sourceDepartment->name.' ,sub department '.$asset->sourceSubDepartment->name;
                        else if($old_status == 'dikembalikan') $note = 'pengembalian asset dari '.$asset->sourceCompany->name.' ,pabrik '.$asset->sourceFactory->name.' ,departemen '.$asset->sourceDepartment->name.' ,sub department '.$asset->sourceSubDepartment->name;
                        else $note = 'asset pinjaman dari '.$asset->sourceCompany->name.' ,pabrik '.$asset->sourceFactory->name.' ,departemen '.$asset->sourceDepartment->name.' ,sub department '.$asset->sourceSubDepartment->name;
                        
                        $from_user = User::find($asset->last_user_movement_id);
    
                        $asset_movement = AssetMovement::firstorcreate([
                            'asset_id'                          => $asset->id,
                            'status'                            => 'siap digunakan',
                            'from_company_location'             => $asset->source_company_location_id,
                            'from_factory_location'             => $asset->source_factory_location_id,
                            'from_department_location'          => $asset->source_department_location_id,
                            'from_employee_nik'                 => ($from_user ? $from_user->nik : null),
                            'from_employee_name'                => ($from_user ? $from_user->name : null),
                            'from_sub_department_location'      => $asset->source_sub_department_location_id,
                            'to_company_location'               => auth::user()->company_id,
                            'to_factory_location'               => $destination_factory->id,
                            'to_department_location'            => $sub_department->department_id,
                            'to_sub_department_location'        => $sub_department->id,
                            'to_employee_nik'                   => auth::user()->nik,
                            'to_employee_name'                  => auth::user()->name,
                            'movement_date'                     => carbon::now(),
                            'note'                              => $note,
                            'is_handover_asset'                 => true,
                            'rental_agreement_id'               => $asset->no_rent,
                        ]);
    
                        $asset->last_asset_movement_id              = $asset_movement->id;
                        $asset->source_company_location_id          = null;
                        $asset->source_factory_location_id          = null;
                        $asset->source_department_location_id       = null;
                        $asset->source_sub_department_location_id   = null;

                        $update_rental_agreement = RentalAgreement::find($asset->no_rent);
                        if($update_rental_agreement)
                        {
                            $update_rental_agreement->receive_date = carbon::now();
                            $update_rental_agreement->save();
                        }
                        
                        if($old_status == 'dipindah tangan')
                        {
                            $asset->origin_company_location_id         = auth::user()->company_id;
                            $asset->origin_factory_location_id         = $destination_factory->id;
                            $asset->origin_department_location_id      = $sub_department->department_id;
                            $asset->origin_sub_department_location_id  = $sub_department->id;
                            $asset->is_rent                             = false;   
                        }else if($old_status == 'dikembalikan')
                        {
                            $asset->is_rent = false;   
                        }
                    }else
                    {
                        $asset_movement =  AssetMovement::firstorcreate([
                            'asset_id'                          => $asset->id,
                            'status'                            => 'siap digunakan',
                            'from_company_location'             => $asset->last_company_location_id,
                            'from_factory_location'             => $asset->last_factory_location_id,
                            'from_department_location'          => $asset->last_department_location_id,
                            'from_employee_nik'                 => ($asset->last_used_by ? explode('-',strtolower($asset->last_used_by))[0] : null),
                            'from_employee_name'                => ($asset->last_used_by ? explode('-',strtolower($asset->last_used_by))[1] : null),
                            'from_sub_department_location'      => $asset->last_sub_department_location_id,
                            'to_company_location'               => auth::user()->company_id,
                            'to_factory_location'               => $destination_factory->id,
                            'to_department_location'            => $sub_department->department_id,
                            'to_sub_department_location'        => $sub_department->id,
                            'to_employee_nik'                   => auth::user()->nik,
                            'to_employee_name'                  => auth::user()->name,
                            'to_area_location'                  => $checkin_area_location,
                            'no_kk'                             => $asset->last_no_kk,
                            'no_bea_cukai'                      => $asset->last_no_bea_cukai,
                            'movement_date'                     => $checkin_date,
                        ]);
    
                        $asset->last_asset_movement_id = $asset_movement->id;
                    }
        
                    $asset->status                          = 'siap digunakan';
                    $asset->last_used_by                    = null;
                    $asset->last_movement_date              = $checkin_date;
                    $asset->last_company_location_id        = auth::user()->company_id;
                    $asset->last_factory_location_id        = $destination_factory->id;
                    $asset->last_department_location_id     = $sub_department->department_id;
                    $asset->last_sub_department_location_id = $sub_department->id;
                    $asset->last_no_kk                      = null;
                    $asset->last_no_bea_cukai               = null;
                    $asset->last_user_movement_id           = auth::user()->id;
                    $asset->save();
    
                    $asset->save();

                    Temporary::firstorCreate([
                        'column_1'  => $asset->origin_sub_department_location_id,
                        'column_2'  => 'sync_export_asset_per_department',
                    ]);
                }
                
                
                
            }
           
            DB::commit();
            return response()->json('success',200);

        } catch (Exception $e)
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    public function getAsset(Request $request)
    {
        $factory_id             = $request->factory;
        $sub_department         = $request->sub_department;
        $barcode                = $request->barcode;
        $has_dash               = strpos($barcode, "-");

        $asset                  = Asset::where([
            ['barcode',$barcode],
            ['last_factory_location_id',$factory_id],
            ['origin_sub_department_location_id',$sub_department],
        ])
        ->where(function($query){
            $query->where('status','dipindah tangan')
            ->orWhere('status','digunakan')
            ->orWhere('status','dikembalikan')
            ->orWhere('status','diperbaiki')
            ->orWhere('status','dipinjam');
        })
        ->whereNull('delete_at')
        ->first();
        
        if(!$asset)
        {
            $area = Area::where([
                ['is_area_stock',true],
                ['factory_id',$factory_id],
                ['code',strtoupper($barcode)],
            ])
            ->first();

            if(!$area) 
            {
                $app_env        = Config::get('app.env');
            
                $handover_asset =  DB::table('dias_aoi_asset_v')
                ->where([
                    ['barcode_handover',$barcode],
                    ['last_factory_mapping_id',$factory_id],
                    ['last_sub_deparment_mapping_id',$sub_department],
                ])
                ->where(function($query)
                {
                    $query->where('status','dipindah tangan')
                    ->orWhere('status','dikembalikan')
                    ->orWhere('status','dipinjam');
                })
                ->first();

                if($handover_asset)
                {
                    if($app_env == 'live')
                    {
                        $asset    = AssetLive::find($handover_asset->id);
                        $no_rent = RentalAgreementLive::find($asset->no_rent);
                    }else if($app_env == 'dev')
                    {
                        $asset    = AssetDev::find($handover_asset->id);
                        $no_rent = RentalAgreementDev::find($asset->no_rent);
                    }
                    
                    if($no_rent)
                    {
                        if(!$no_rent->bc_type_id) return response()->json(['message' => 'Please input BC out first !, please info exim bbi to input it'],422);
                    }else return response()->json(['message' => 'No KK not found'],422);

                    $array = array();
                    
                    $obj                        = new stdClass();
                    $obj->flag                  = '1';
                    $obj->id                    = $asset->id;
                    $obj->barcode               = $asset->barcode;
                    $obj->no_inventory          = strtoupper($asset->no_inventory);
                    $obj->erp_no_asset          = strtoupper($asset->erp_no_asset);
                    $obj->asset_type            = ucwords($handover_asset->asset_type);
                    $obj->serial_number         = strtoupper($handover_asset->serial_number);
                    $obj->model                 = ucwords($handover_asset->model);
                    $obj->last_location         = 'receive from aoi';
                    $obj->detail_informations   = $array;
            
                    return response()->json([
                        'area'  => null,
                        'asset' => $obj,
                    ],200);
                }else return response()->json(['message' => 'Barcode not found'],422);
            }

            return response()->json([
                'area'  => $area,
                'asset' => null,
            ],200);
        }else
        {
            if($asset->status == 'dipindah tangan' || $asset->status == 'dipinjam')
            {
                if($asset->factory_id != $asset->last_factory_location_id 
                ||$asset->company_id != $asset->last_company_location_id)
                {
                    if($asset->last_factory_location_id != $factory_id) return response()->json(['message' => 'Asset dipindah pabrik, silahkan cekin di pabrik tujuan.'],422); 
                }

                $no_rent = RentalAgreement::find($asset->no_rent);
                
                if($no_rent)
                {
                    if(!$no_rent->bc_type_id) return response()->json(['message' => 'Please input BC out first !, please info exim '.$no_rent->factory->company->code.'  to input it'],422);
                }else return response()->json(['message' => 'No KK not found'],422);
            }else if($asset->status == 'dikembalikan')
            {
                $no_rent = RentalAgreement::find($asset->no_rent);
                
                if($no_rent)
                {
                    if(!$no_rent->bc_type_id) return response()->json(['message' => 'Please input BC out first !, please info exim '.$no_rent->factory->company->code.'  to input it'],422);
                }else return response()->json(['message' => 'No KK not found'],422);
            }
                
            $asset_custom_fields    = AssetInformationField::where([
                ['asset_type_id', $asset->asset_type_id],
            ])
            ->whereNotIn('name',[
                'department_name',
                'created_at',
                'updated_at',
                'referral_code',
                'sequence',
                'image',
                'delete_at',
                'create_user_id',
                'source_company_location_id',
                'source_factory_location_id',
                'source_department_location_id',
                'delete_user_id',
                'is_from_sto_date',
                'qty_available',
                'qty_used',
                'total',
                'erp_item_id',
                'erp_asset_group_id',
                
            ])
            ->whereNull('delete_at')
            ->orderby('created_at','asc')
            ->get();
    
            $array = array();
            foreach ($asset_custom_fields as $key => $value) 
            {
                $column_name        = $value->name;
                $column_type        = $value->type;
                $curr_value         = ($asset)? $asset->$column_name:null;
    
                if($column_name == 'asset_type_id') $curr_value                 = $asset->assetType->name;
                if($column_name == 'company_id') $curr_value                    = $asset->company->name;
                if($column_name == 'factory_id') $curr_value                    = $asset->factory->name;
                if($column_name == 'department_id') $curr_value                 = $asset->department->name;
                if($column_name == 'manufacture_id') $curr_value                = ($asset->manufacture_id)? $asset->manufacture->name : null;
                if($column_name == 'last_company_location_id') $curr_value      = $asset->lastCompany->name;
                if($column_name == 'last_factory_location_id') $curr_value      = $asset->lastFactory->name;
                if($column_name == 'last_department_location_id') $curr_value   = $asset->lastDepartment->name;
                if($column_name == 'last_movement_date') $curr_value            = ($asset->last_movement_date ? $asset->last_movement_date->format('d/m/Y H:i:s') : null);
                
                if($asset->assetType->is_consumable == false)
                {
                    if($column_name == 'total') $curr_value = 1;
                }
    
                if($column_type == 'lookup')
                {
                    $splits                     = explode('|',$curr_value);
                    if($curr_value)
                    {
                        $custom_lookup_id           = $splits[0];
                        $lookup_id                  = $splits[1];
                        $detail_lookup_id           = $splits[2];
    
                        $consumable_asset           = ConsumableAsset::where([
                            ['asset_type_id',$custom_lookup_id],
                            ['id',$lookup_id],
                        ])
                        ->first();
        
                        $consumable_asset_detail    = ConsumableAssetDetail::where([
                            ['consumable_asset_id',$lookup_id],
                            ['id',$detail_lookup_id],
                        ])
                        ->first();
        
                        $model_header               = ($consumable_asset ? $consumable_asset->model : null);
                        $serial_number_header       = ($consumable_asset ? $consumable_asset->serial_number : null);
                        $detail_value               = ($consumable_asset_detail) ? $consumable_asset_detail->value : null;
                        $lookup_value               = $model_header;
        
                        if($serial_number_header)   $lookup_value .= ':'.strtoupper($serial_number_header);
                        if($detail_value)           $lookup_value .= ':'.strtoupper($detail_value);
                        
                        $curr_value                 = $lookup_value;
                    }else $curr_value               = $curr_value;
                    
    
                    
                }
                
                $obj                = new stdClass();
                $obj->column_name   = $column_name;
                $obj->label         = ucwords(str_replace('_',' ',$value->label));
                $obj->value         = ucwords($curr_value);
                $array []           = $obj;
            }
            
            $obj                        = new stdClass();
            $obj->flag                  = '2';
            $obj->id                    = $asset->id;
            $obj->barcode               = $asset->barcode;
            $obj->no_inventory          = strtoupper($asset->no_inventory);
            $obj->erp_no_asset          = strtoupper($asset->erp_no_asset);
            $obj->asset_type            = ucwords($asset->assetType->name);
            $obj->serial_number         = strtoupper($asset->serial_number);
            $obj->model                 = ucwords($asset->model);
            $obj->last_location         = 'Last Factory:'.($asset->last_factory_location_id ? $asset->lastFactory->name : '-').', Last Department:'.($asset->last_department_location_id ? $asset->lastDepartment->name : '-').', Last Area Location:'.($asset->last_area_location ? $asset->last_area_location : '-');
            $obj->detail_informations   = $array;
    
            return response()->json([
                'area'  => null,
                'asset' => $obj,
            ],200);
        } 
        
    }

    public function getAreaStock(Request $request)
    {
        $factory_id = $request->factory_id;
        
        $areas = Area::select('name')
        ->where([
            ['factory_id',$factory_id],
            ['is_area_stock',true]
        ])
        ->whereNotnULL('name')
        ->groupby('name')
        ->pluck('name','name')
        ->all();
        
        return response()->json($areas,200);

    }

    public function listOfAsset(Request $request)
    {
        $q = strtolower(trim($request->q));

        $lists = DB::table('lov_asset_in_v')
        ->where([
            ['last_factory_location_id',$request->factory_id],
            ['origin_sub_department_location_id',$request->sub_department_id]
        ])
        ->where(function($query)
        {
            $query->where('status','dipindah tangan')
            ->orWhere('status','digunakan')
            ->orWhere('status','dikembalikan')
            ->orWhere('status','diperbaiki')
            ->orWhere('status','dipinjam');
        })
        ->where(function($query) use($q)
        {
            $query->where(db::raw('lower(no_inventory)'),'like',"%$q%")
            ->orWhere(db::raw('lower(serial_number)'),'like',"%$q%")
            ->orWhere(db::raw('lower(no_inventory_manual)'),'like',"%$q%")
            ->orWhere(db::raw('lower(erp_no_asset)'),'like',"%$q%");
        })
        ->paginate(10);

        return view('checkin._lov_assets',compact('lists'));
    } 
}
