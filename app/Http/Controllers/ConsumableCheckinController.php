<?php namespace App\Http\Controllers;

use DB;
use Auth;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Session;

use App\Models\Area;
use App\Models\User;
use App\Models\Asset;
use App\Models\Company;
use App\Models\ConsumableAsset;
use App\Models\ConsumableAssetDetail;
use App\Models\AssetInformationField;
use App\Models\ConsumableAssetMovement;

class ConsumableCheckinController extends Controller
{
    public function index(Request $request,$id)
    {
    	$consumable_asset_id = $id;
        return view('consumable.checkin',compact('consumable_asset_id'));
    }

    public function data(Request $request)
    {
    	
         if(request()->ajax()) 
        {
            //consumable_history_v
            $data = DB::table('consumable_history_v')->where([
                ['consumable_asset_id',$request->consumable_asset_id],
                ['is_return',false],
                ['status','digunakan'],
            ])
            ->orderby('movement_date','desc');

            return datatables()->of($data)
            ->editColumn('movement_date',function($data){
            	return  Carbon::createFromFormat('Y-m-d H:i:s', $data->movement_date)->format('d/M/Y H:i:s');
            })
            ->editColumn('to',function($data){
                if($data->asset_id)
                {
                    $_consumable_asset_value = ucwords($data->consumable_asset_model);
                    if($data->consumable_asset_serial_number) $_consumable_asset_value = $_consumable_asset_value.'-'.ucwords($data->consumable_asset_model);
                    if($data->consumable_asset_detail_value) $_consumable_asset_value = $_consumable_asset_value.'-'.ucwords($data->consumable_asset_detail_value);

                    return '<b>Asset</b><br/>'.ucwords($data->asset_serial_number);
                }else
                {
                    $_consumable_asset_value = ucwords($data->consumable_asset_model);
                    if($data->consumable_asset_serial_number) $_consumable_asset_value = $_consumable_asset_value.'-'.ucwords($data->consumable_asset_model);
                    if($data->consumable_asset_detail_value) $_consumable_asset_value = $_consumable_asset_value.'-'.ucwords($data->consumable_asset_detail_value);

                    return '<b>Perusahaan</b><br/>'.ucwords($data->to_company_name).
                    '<br><b>Pabik</b><br/>'.ucwords($data->to_factory_name).
                    '<br><b>Sub Departemen</b><br/>'.ucwords($data->to_sub_department_name).
                    '<br><b>Consumable asset value</b><br/>'.$_consumable_asset_value.
                    '<br><b>Nik</b><br/>'.ucwords($data->to_employee_nik).
                    '<br><b>Nama</b><br/>'.ucwords($data->to_employee_name);
                }
           })
           ->addColumn('action', function($data) {
                return view('consumable._action_checkin', [
                    'backTo' => route('consumable.store-checkin',$data->consumable_asset_movement_id),
                ]);
            })
            ->rawColumns(['from','to','action'])
            ->make(true);
        }
    }
    
    public function store($id)
    {
        try 
        {
            DB::beginTransaction();
            $consumable_movement            = ConsumableAssetMovement::Find($id);
            if($consumable_movement)
            {
                $consumable_movement->is_return = 't';
                $consumable_movement->save();

                $consumable_asset                = ConsumableAsset::find($consumable_movement->consumable_asset_id);
                $qty_used                        = $consumable_asset->qty_used;
                $qty_available                   = $consumable_asset->qty_available;
                $consumable_asset->qty_used      = $qty_used-1;
                $consumable_asset->qty_available = $qty_available+1;
                $consumable_asset->save();
            }
            
           
            DB::commit();
            return response()->json('success',200);

        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }
}
