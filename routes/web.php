<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/dashboard-eis', 'HomeController@dashboardEis')->name('dashboardEis');


Route::get('/', function () {
    return redirect('/dashboard');
});

Auth::routes();
Route::get('/dashboard', 'HomeController@index')->name('home');
Route::get('/dashboard-data-summary', 'HomeController@dashboardDataSummary')->name('home.dashboardDataSummary');
Route::get('/dashboard-data-pie-summary', 'HomeController@dashboardDataPie')->name('home.dashboardDataPie');
Route::get('/dashboard-data-pie-department-summary', 'HomeController@dashboardDataPieDepartmentSummary')->name('home.dashboardDataPieDepartmentSummary');
Route::get('/dashboard-data-table-summary', 'HomeController@dashboardDataTableSummary')->name('home.dashboardDataTableSummary');


Route::get('/account-setting', 'HomeController@accountSetting')->name('accountSetting');
Route::put('/account-setting/{id}', 'HomeController@updateAccount')->name('accountSetting.updateAccount');
Route::get('/account-setting/{id}/show-avatar', 'HomeController@showAvatar')->name('accountSetting.showAvatar');

Route::prefix('/account-management/permission')->middleware(['permission:menu-permission'])->group(function()
{
    Route::get('', 'PermissionController@index')->name('permission.index');
    Route::get('baru', 'PermissionController@create')->name('permission.create');
    Route::get('data', 'PermissionController@data')->name('permission.data');
    Route::post('store', 'PermissionController@store')->name('permission.store');
    Route::get('ubah/{id}', 'PermissionController@edit')->name('permission.edit');
    Route::post('update/{id}', 'PermissionController@update')->name('permission.update');
    Route::delete('delete/{id}', 'PermissionController@destroy')->name('permission.destroy');
});

Route::prefix('/account-management/role')->middleware(['permission:menu-role'])->group(function()
{
    Route::get('', 'RoleController@index')->name('role.index');
    Route::get('baru', 'RoleController@create')->name('role.create');
    Route::get('data', 'RoleController@data')->name('role.data');
    Route::get('ubah/{id}', 'RoleController@edit')->name('role.edit');
    Route::get('edit/{id}/permission-role', 'RoleController@dataPermission')->name('role.dataPermission');
    Route::post('store', 'RoleController@store')->name('role.store');
    Route::post('store/permission', 'RoleController@storePermission')->name('role.storePermission');
    Route::post('delete/{role_id}/{permission_id}/permission-role', 'RoleController@destroyPermissionRole')->name('role.destroyPermissionRole');
    Route::post('update/{id}', 'RoleController@update')->name('role.update');
    Route::delete('delete/{id}', 'RoleController@destroy')->name('role.destroy');
});


Route::prefix('/rental-agreement')->middleware(['permission:menu-rental-agreement'])->group(function()
{
    Route::get('', 'RentalAgreementController@index')->name('rentalAgreement.index');
    Route::get('data', 'RentalAgreementController@data')->name('rentalAgreement.data');
    Route::get('create', 'RentalAgreementController@create')->name('rentalAgreement.create');
    Route::get('edit/{id}', 'RentalAgreementController@edit')->name('rentalAgreement.edit');
    Route::get('get-budget', 'RentalAgreementController@getBudget')->name('rentalAgreement.getBudget');
    Route::post('store', 'RentalAgreementController@store')->name('rentalAgreement.store');
    Route::post('return-asset/{id}', 'RentalAgreementController@returnAsset')->name('rentalAgreement.returnAsset');
    Route::post('extend/{id}', 'RentalAgreementController@extend')->name('rentalAgreement.extend');
    Route::post('update/{id}', 'RentalAgreementController@update')->name('rentalAgreement.update');
    Route::put('delete/{id}', 'RentalAgreementController@delete')->name('rentalAgreement.delete');
   
});

Route::prefix('/calendar')->middleware(['permission:menu-calendar'])->group(function()
{
    Route::get('', 'CalendarController@index')->name('calendar.index');
    Route::get('create', 'CalendarController@create')->name('calendar.create');
    Route::get('edit/{id}', 'CalendarController@edit')->name('calendar.edit');
    Route::get('data', 'CalendarController@data')->name('calendar.data');
    Route::post('store', 'CalendarController@store')->name('calendar.store');
    Route::post('update/{id}', 'CalendarController@update')->name('calendar.update');
    Route::put('delete/{id}', 'CalendarController@destroy')->name('calendar.destroy');
});

Route::prefix('/bc-type')->middleware(['permission:menu-bc-type'])->group(function()
{
    Route::get('', 'BcTypeController@index')->name('bcType.index');
    Route::get('create', 'BcTypeController@create')->name('bcType.create');
    Route::get('data', 'BcTypeController@data')->name('bcType.data');
    Route::post('store', 'BcTypeController@store')->name('bcType.store');
    Route::get('edit/{id}', 'BcTypeController@edit')->name('bcType.edit');
    Route::post('update/{id}', 'BcTypeController@update')->name('bcType.update');
    Route::delete('delete/{id}', 'BcTypeController@destroy')->name('bcType.destroy');
});

Route::prefix('/account-management/user')->middleware(['permission:menu-user'])->group(function()
{
    Route::get('', 'UserController@index')->name('user.index');
    Route::get('baru', 'UserController@create')->name('user.create');
    Route::get('data', 'UserController@data')->name('user.data');
    Route::get('get-absence', 'UserController@getAbsence')->name('user.getAbsence');
    Route::get('ubah/{id}', 'UserController@edit')->name('user.edit');
    Route::get('edit/{id}/role-user', 'UserController@dataRole')->name('user.dataRole');
    Route::post('store', 'UserController@store')->name('user.store');
    Route::post('store/role', 'UserController@storeRole')->name('user.storeRole');
    Route::post('delete/{user_id}/{role_id}/role-user', 'UserController@destroyRoleUser')->name('user.destroyRoleUser');
    Route::post('update/{id}', 'UserController@update')->name('user.update');
    Route::put('reset-password/{id}', 'UserController@resetPassword')->name('user.resetPassword');
    Route::put('delete/{id}', 'UserController@destroy')->name('user.destroy');
});

Route::prefix('/organization/company')->middleware(['permission:menu-company'])->group(function()
{
    Route::get('', 'CompanyController@index')->name('company.index');
    Route::get('create', 'CompanyController@create')->name('company.create');
    Route::get('edit/{id}', 'CompanyController@edit')->name('company.edit');
    Route::get('data', 'CompanyController@data')->name('company.data');
    Route::post('store', 'CompanyController@store')->name('company.store');
    Route::post('update/{id}', 'CompanyController@update')->name('company.update');
    Route::put('delete/{id}', 'CompanyController@destroy')->name('company.destroy');
});


Route::prefix('/organization/factory')->middleware(['permission:menu-factory'])->group(function()
{
    Route::get('', 'FactoryController@index')->name('factory.index');
    Route::get('create', 'FactoryController@create')->name('factory.create');
    Route::get('edit/{id}', 'FactoryController@edit')->name('factory.edit');
    Route::get('data', 'FactoryController@data')->name('factory.data');
    Route::post('store', 'FactoryController@store')->name('factory.store');
    Route::post('update/{id}', 'FactoryController@update')->name('factory.update');
    Route::put('delete/{id}', 'FactoryController@destroy')->name('factory.destroy');
});

Route::prefix('/organization/department')->middleware(['permission:menu-department'])->group(function()
{
    Route::get('', 'DepartmentController@index')->name('department.index');
    Route::get('create', 'DepartmentController@create')->name('department.create');
    Route::get('edit/{id}', 'DepartmentController@edit')->name('department.edit');
    Route::get('data', 'DepartmentController@data')->name('department.data');
    Route::post('update/{id}', 'DepartmentController@update')->name('department.update');
    Route::post('store', 'DepartmentController@store')->name('department.store');
});

Route::prefix('/organization/sub-departement')->middleware(['permission:menu-sub-department'])->group(function()
{
    Route::get('', 'SubDepartmentController@index')->name('subDepartment.index');
    Route::get('create', 'SubDepartmentController@create')->name('subDepartment.create');
    Route::get('edit/{id}', 'SubDepartmentController@edit')->name('subDepartment.edit');
    Route::get('data', 'SubDepartmentController@data')->name('subDepartment.data');
    Route::post('update/{id}', 'SubDepartmentController@update')->name('subDepartment.update');
    Route::post('store', 'SubDepartmentController@store')->name('subDepartment.store');
});

Route::prefix('/setting/asset-type')->middleware(['permission:menu-asset-type'])->group(function()
{
    Route::get('', 'AssetTypeController@index')->name('assetType.index');
    Route::get('data', 'AssetTypeController@data')->name('assetType.data');
    Route::get('create', 'AssetTypeController@create')->name('assetType.create');
    Route::get('edit/{id}', 'AssetTypeController@edit')->name('assetType.edit');
    Route::get('department', 'AssetTypeController@getDepartment')->name('assetType.getDepartment');
    Route::get('list-available-columns', 'AssetTypeController@getAvailableColumn')->name('assetType.getAvailableColumn');
    Route::get('list-of-consumable-asset-type', 'AssetTypeController@getConsumbaleAssetType')->name('assetType.getConsumbaleAssetType');
    Route::get('data/custom-field/{id}', 'AssetTypeController@dataCustomField')->name('assetType.dataField');
    Route::get('edit/asset-information-custom-field/{asset_informat_custom_field}', 'AssetTypeController@editCustomField')->name('assetType.editCustomField');
    Route::post('store', 'AssetTypeController@store')->name('assetType.store');
    Route::post('store/asset-information-custom-field', 'AssetTypeController@storeCustomField')->name('assetType.storeCustomField');
    Route::put('delete/{id}', 'AssetTypeController@destroy')->name('assetType.destroy');
    Route::post('update/{id}', 'AssetTypeController@update')->name('assetType.update');
    Route::put('update/asset-information-custom-field/{asset_informat_custom_field}', 'AssetTypeController@updateCustomField')->name('assetType.updateCustomField');
});

Route::prefix('/setting/manufacture')->middleware(['permission:menu-manufacture'])->group(function ()
{
    Route::get('', 'ManufactureController@index')->name('manufacture.index');
    Route::get('data', 'ManufactureController@data')->name('manufacture.data');
    Route::get('baru', 'ManufactureController@create')->name('manufacture.create');
    Route::get('ubah/{id}', 'ManufactureController@edit')->name('manufacture.edit');
    Route::get('import', 'ManufactureController@import')->name('manufacture.import');
    Route::get('import/export-form-import', 'ManufactureController@exportFormImport')->name('manufacture.exportFormImport');
    Route::post('store', 'ManufactureController@store')->name('manufacture.store');
    Route::post('update/{id}', 'ManufactureController@update')->name('manufacture.update');
    Route::put('delete/{id}', 'ManufactureController@destroy')->name('manufacture.destroy');
    Route::post('import/upload-form-import', 'ManufactureController@uploadFormImport')->name('manufacture.uploadFormImport'); 
});

Route::prefix('/area')->middleware(['permission:menu-area'])->group(function()
{
    Route::get('', 'AreaController@index')->name('area.index');
    Route::get('create', 'AreaController@create')->name('area.create');
    Route::get('barcode/{id}', 'AreaController@barcode')->name('area.barcode');
    Route::get('print-barcode', 'AreaController@printBarcode')->name('area.printBarcode');
    Route::get('print-all-barcode', 'AreaController@printAllBarcode')->name('area.printAllBarcode');
    Route::get('edit/{id}', 'AreaController@edit')->name('area.edit');
    Route::get('data', 'AreaController@data')->name('area.data');
    Route::get('import', 'AreaController@import')->name('area.import');
    Route::get('import/export-form-import', 'AreaController@exportFormImport')->name('area.exportFormImport');
    Route::post('store', 'AreaController@store')->name('area.store');
    Route::post('update/{id}', 'AreaController@update')->name('area.update');
    Route::put('delete/{id}', 'AreaController@destroy')->name('area.destroy');
    Route::post('import/upload-form-import', 'AreaController@uploadFormImport')->name('area.uploadFormImport');
});


Route::prefix('/budget')->middleware(['permission:menu-budget'])->group(function()
{
    Route::get('', 'BudgetController@index')->name('budget.index');
    Route::get('create', 'BudgetController@create')->name('budget.create');
    Route::get('edit/{id}', 'BudgetController@edit')->name('budget.edit');
    Route::get('data', 'BudgetController@data')->name('budget.data');
    Route::get('import', 'BudgetController@import')->name('budget.import');
    Route::get('import/export-form-import', 'BudgetController@exportFormImport')->name('budget.exportFormImport');
    Route::post('store', 'BudgetController@store')->name('budget.store');
    Route::post('update/{id}', 'BudgetController@update')->name('budget.update');
    Route::put('delete/{id}', 'BudgetController@destroy')->name('budget.destroy');
    Route::post('import/upload-form-import', 'BudgetController@uploadFormImport')->name('budget.uploadFormImport');
});

Route::prefix('/asset')->middleware(['permission:menu-asset'])->group(function()
{
    Route::get('', 'AssetController@index')->name('asset.index');
    Route::get('export-report', 'AssetController@exportReport')->name('asset.exportReport');
    Route::get('erp-asset-picklist', 'AssetController@erpAssetPicklist')->name('asset.erpAssetPicklist');
    Route::get('import', 'AssetController@import')->name('asset.import');
    Route::get('create', 'AssetController@create')->name('asset.create');
    Route::get('status', 'AssetController@getStatus')->name('asset.status');
    Route::get('rent-agreement', 'AssetController@getRentAgreement')->name('asset.getRentAgreement');
    Route::get('custom-field', 'AssetController@getCustomField')->name('asset.getCustomField');
    Route::get('asset-type', 'AssetController@getAssetTypeAndManufacture')->name('asset.getAssetType');
    Route::get('manufacture', 'AssetController@getAssetTypeAndManufacture')->name('asset.getManufacture');
    Route::get('data', 'AssetController@data')->name('asset.data');
    Route::get('import/export-form-import', 'AssetController@exportFormImport')->name('asset.exportFormImport');
    Route::get('edit/{id}', 'AssetController@edit')->name('asset.edit');
    Route::get('lookup', 'AssetController@getLookup')->name('asset.getLookup');
    Route::get('history/{id}', 'AssetController@history')->name('asset.history');
    Route::get('history/{id}/data', 'AssetController@historyData')->name('asset.historyData');
    Route::get('show-image/{file_name}', 'AssetController@showImage')->name('asset.showImage');
    Route::get('printout/{id}', 'AssetController@printout')->name('asset.printout');
    Route::get('barcode/{id}', 'AssetController@barcode')->name('asset.barcode');
    
    Route::post('store', 'AssetController@store')->name('asset.store');
    Route::post('import/upload-form-import', 'AssetController@uploadFormImport')->name('asset.uploadFormImport');
    Route::put('delete/{id}', 'AssetController@destroy')->name('asset.destroy');
    Route::post('update/{id}', 'AssetController@update')->name('asset.update');
    Route::post('print-barcode', 'AssetController@printBarcode')->name('asset.printBarcode');
});

Route::prefix('/asset-rent')->middleware(['permission:menu-asset-rent'])->group(function()
{
    Route::get('', 'AssetRentController@index')->name('assetRent.index');
    Route::get('erp-asset-picklist', 'AssetRentController@erpAssetPicklist')->name('assetRent.erpAssetPicklist');
    Route::get('export-report', 'AssetRentController@exportReport')->name('assetRent.exportReport');
    Route::get('import', 'AssetRentController@import')->name('assetRent.import');
    Route::get('create', 'AssetRentController@create')->name('assetRent.create');
    Route::get('list-budget', 'AssetRentController@getBudget')->name('assetRent.getBudget');
    Route::get('status', 'AssetRentController@getStatus')->name('assetRent.status');
    Route::get('custom-field', 'AssetRentController@getCustomField')->name('assetRent.getCustomField');
    Route::get('asset-type', 'AssetRentController@getAssetTypeAndManufacture')->name('assetRent.getAssetType');
    Route::get('manufacture', 'AssetRentController@getAssetTypeAndManufacture')->name('assetRent.getManufacture');
    Route::get('data', 'AssetRentController@data')->name('assetRent.data');
    Route::get('import/export-form-import', 'AssetRentController@exportFormImport')->name('assetRent.exportFormImport');
    Route::get('edit/{id}', 'AssetRentController@edit')->name('assetRent.edit');
    Route::get('lookup', 'AssetRentController@getLookup')->name('assetRent.getLookup');
    Route::get('history/{id}', 'AssetRentController@history')->name('assetRent.history');
    Route::get('history/{id}/data', 'AssetRentController@historyData')->name('assetRent.historyData');
    Route::get('show-image/{file_name}', 'AssetRentController@showImage')->name('assetRent.showImage');
    Route::get('barcode/{id}', 'AssetRentController@barcode')->name('assetRent.barcode');
    
    Route::post('store', 'AssetRentController@store')->name('assetRent.store');
    Route::post('import/upload-form-import', 'AssetRentController@uploadFormImport')->name('assetRent.uploadFormImport');
    Route::put('delete/{id}', 'AssetRentController@destroy')->name('assetRent.destroy');
    Route::post('update/{id}', 'AssetRentController@update')->name('assetRent.update');
    Route::post('print-barcode', 'AssetRentController@printBarcode')->name('assetRent.printBarcode');
});

Route::prefix('/consumable')->middleware(['permission:menu-consumable'])->group(function()
{
    Route::get('', 'ConsumableAssetController@index')->name('consumable.index');
    Route::get('import', 'ConsumableAssetController@import')->name('consumable.import');
    Route::get('create', 'ConsumableAssetController@create')->name('consumable.create');
    Route::get('sub-department', 'ConsumableAssetController@getSubDepartment')->name('consumable.getSubDepartment');
    Route::get('custom-field', 'ConsumableAssetController@getCustomField')->name('consumable.getCustomField');
    Route::get('asset-type', 'ConsumableAssetController@getAssetTypeAndManufacture')->name('consumable.getAssetType');
    Route::get('manufacture', 'ConsumableAssetController@getAssetTypeAndManufacture')->name('consumable.getManufacture');
    Route::get('data', 'ConsumableAssetController@data')->name('consumable.data');
    Route::get('import/export-form-import', 'ConsumableAssetController@exportFormImport')->name('consumable.exportFormImport');
    Route::get('edit/{id}', 'ConsumableAssetController@edit')->name('consumable.edit');
    Route::get('check-out/{id}', 'ConsumableCheckOutController@index')->name('consumable.checkout');
    Route::get('check-in/{id}', 'ConsumableCheckinController@index')->name('consumable.checkin');
    Route::get('check-in-data', 'ConsumableCheckinController@data')->name('consumable.checkin-data');
    Route::get('history/{id}', 'ConsumableAssetController@history')->name('consumable.history');
    Route::get('history/{id}/data', 'ConsumableAssetController@historyData')->name('consumable.historyData');
    Route::get('edit/{id}/data-detail-consumable-asset', 'ConsumableAssetController@dataDetailConsumable')->name('consumable.dataDetailConsumable');
    Route::get('edit/{consumable_asset_id}/{consumable_asset_detail_id}/detail-data', 'ConsumableAssetController@editDetail')->name('consumable.editDetail');
    Route::get('absence', 'ConsumableCheckOutController@getAbsence')->name('consumable.getAbsence');
    Route::post('store-checkin/{id}', 'ConsumableCheckinController@store')->name('consumable.store-checkin');
    Route::get('list-of-asset', 'ConsumableCheckOutController@listOfAsset')->name('consumable.listOfAsset');
    Route::get('list-of-detail', 'ConsumableCheckOutController@listOfAssetDetail')->name('consumable.listOfAssetDetail');
    Route::get('asset', 'ConsumableCheckOutController@getAsset')->name('consumable.getAsset');

    Route::post('store', 'ConsumableAssetController@store')->name('consumable.store');
    Route::post('check-out/{id}', 'ConsumableCheckOutController@store')->name('consumable.storeCheckout');
    Route::post('import/upload-form-import', 'ConsumableAssetController@uploadFormImport')->name('consumable.uploadFormImport');
    Route::post('edit/{consumable_asset_id}/{consumable_asset_detail_id}/update-data', 'ConsumableAssetController@updateDetail')->name('consumable.updateDetail');
    Route::post('edit/{consumable_asset_id}/store-data', 'ConsumableAssetController@storeDetail')->name('consumable.storeDetail');
    Route::post('update/{id}', 'ConsumableAssetController@update')->name('consumable.update');

    Route::put('delete/{id}', 'ConsumableAssetController@destroy')->name('consumable.destroy');
    Route::put('delete/{consumable_asset_id}/{consumable_asset_detail_id}/detail', 'ConsumableAssetController@destroyDetail')->name('consumable.destroyDetail');
});

Route::prefix('/check-in')->middleware(['permission:menu-check-in'])->group(function () 
{
    Route::get('', 'CheckInController@index')->name('checkin.index');
    Route::get('data', 'CheckInController@data')->name('checkin.data');
    Route::get('area-stock', 'CheckInController@getAreaStock')->name('checkin.getAreaStock');
    Route::get('asset', 'CheckInController@getAsset')->name('checkin.getAsset');
    Route::post('store', 'CheckInController@store')->name('checkin.store');
    Route::get('list-of-asset', 'CheckInController@listOfAsset')->name('checkin.listOfAsset');
});

Route::prefix('/check-out')->middleware(['permission:menu-check-out'])->group(function () 
{
    Route::get('', 'CheckOutController@index')->name('checkout.index');
    Route::get('department', 'CheckOutController@getDepartment')->name('checkout.getDepartment');
    Route::get('other-factory', 'CheckOutController@getOtherFactory')->name('checkout.getOtherFactory');
    Route::get('absence', 'CheckOutController@getAbsence')->name('checkout.getAbsence');
    Route::get('destination-subdepartment', 'CheckOutController@getDestinationSubDepartment')->name('checkout.getDestinationSubDepartment');
    Route::get('destination-area', 'CheckOutController@getDestinationArea')->name('checkout.getDestinationArea');
    Route::get('list-of-asset', 'CheckOutController@listOfAsset')->name('checkout.listOfAsset');
    Route::get('list-of-kk', 'CheckOutController@listOfKK')->name('checkout.listOfkk');
    Route::get('asset', 'CheckOutController@getAsset')->name('checkout.getAsset');
    Route::get('factory', 'CheckOutController@getInformationFactory')->name('checkout.getFactory');
    Route::post('store', 'CheckOutController@store')->name('checkout.store');
    
});

Route::prefix('/asset-opname')->middleware(['permission:menu-asset-opname'])->group(function () 
{
    Route::get('', 'AssetOpnameController@index')->name('assetOpname.index');
    Route::get('data', 'AssetOpnameController@data')->name('assetOpname.data');
    Route::get('asset', 'AssetOpnameController@getAsset')->name('assetOpname.getAsset');
    Route::get('area', 'AssetOpnameController@getArea')->name('assetOpname.getArea');
    Route::post('store', 'AssetOpnameController@store')->name('assetOpname.store');
    Route::get('list-of-asset', 'AssetOpnameController@listOfAsset')->name('assetOpname.listOfAsset');
});

Route::prefix('/maintenance')->middleware(['permission:menu-maintenance'])->group(function ()
{
    Route::get('', 'MaintenanceController@index')->name('maintenance.index');
    Route::get('data', 'MaintenanceController@data')->name('maintenance.data');
    Route::get('asset-type', 'MaintenanceController@getAssetType')->name('maintenance.getAssetType');
    Route::get('sub-department', 'MaintenanceController@getSubDepartment')->name('maintenance.getSubDepartment');
    Route::get('list-of-asset', 'MaintenanceController@listOfAsset')->name('maintenance.listOfAsset');
    Route::get('asset', 'MaintenanceController@getAsset')->name('maintenance.getAsset'); 
    Route::post('store', 'MaintenanceController@store')->name('maintenance.store'); 
});

Route::prefix('/bapb')->middleware(['permission:menu-bapb'])->group(function()
{
    Route::get('', 'BapbController@index')->name('bapb.index');
    Route::get('data', 'BapbController@data')->name('bapb.data');
    Route::get('get-asset-type', 'BapbController@getAssetType')->name('bapb.getAssetType');
    Route::put('cancel-obsolete/{id}', 'BapbController@cancelObsolete')->name('bapb.cancelObsolete');
    Route::post('store', 'BapbController@store')->name('bapb.store');
    Route::post('add-to-cart', 'BapbController@addToCart')->name('bapb.addToCart');
});

Route::prefix('report')->group(function()
{

    Route::prefix('asset-opname')->middleware(['permission:menu-report-asset-opname'])->group(function()
    {
        Route::get('', 'ReportAssetOpnameController@index')->name('reportAssetOpname.index');
        Route::get('data', 'ReportAssetOpnameController@data')->name('reportAssetOpname.data');
        Route::get('export', 'ReportAssetOpnameController@export')->name('reportAssetOpname.export');
    });

    Route::prefix('outsanding-bc')->middleware(['permission:menu-report-outstanding-bc'])->group(function()
    {
        Route::get('', 'ReportOutstandingBCController@index')->name('reportOutstandingBC.index');
        Route::get('import', 'ReportOutstandingBCController@import')->name('reportOutstandingBC.import');
        Route::get('download-report', 'ReportOutstandingBCController@downloadReport')->name('reportOutstandingBC.downloadReport');
        Route::get('download-form-import', 'ReportOutstandingBCController@downloadFormImport')->name('reportOutstandingBC.downloadFormImport');
        Route::get('detail/{id}', 'ReportOutstandingBCController@detail')->name('reportOutstandingBC.detail');
        Route::get('edit/{id}', 'ReportOutstandingBCController@edit')->name('reportOutstandingBC.edit');
        Route::get('data', 'ReportOutstandingBCController@data')->name('reportOutstandingBC.data');
        Route::get('detail-data/{id}', 'ReportOutstandingBCController@detailData')->name('reportOutstandingBC.detailData');
        Route::post('update', 'ReportOutstandingBCController@update')->name('reportOutstandingBC.update');
        Route::post('upload-form-import', 'ReportOutstandingBCController@uploadFormImport')->name('reportOutstandingBC.uploadFormImport');
        
    });

});