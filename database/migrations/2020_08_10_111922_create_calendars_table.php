<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalendarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       

        Schema::create('calendars', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->string('event_name');
            $table->date('event_start');
            $table->date('event_end');
            $table->biginteger('created_user_id')->nullable()->unsigned()->nullable();;
            $table->biginteger('updated_user_id')->nullable()->unsigned()->nullable();;
            $table->biginteger('deleted_user_id')->nullable()->unsigned()->nullable();;
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
            
            $table->foreign('created_user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('updated_user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('deleted_user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('asset_movements', function (Blueprint $table) 
        {
            $table->char('calendar_id',36)->nullable();
            $table->foreign('calendar_id')->references('id')->on('calendars')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calendars');
    }
}
