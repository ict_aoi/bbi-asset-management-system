<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssetMovementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asset_movements', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('asset_id',36)->unsigned();
            $table->string('status'); // digunakan, dipinjam, dipindah tangan,dikembalikan,bpbp,rusak
            $table->string('from_company_location')->nullable(); //sesuai dengan pilihan user
            $table->string('from_factory_location')->nullable(); //sesuai dengan pilihan user
            $table->string('from_department_location')->nullable(); //sesuai dengan pilihan user
            $table->string('from_employee_nik')->nullable(); // user yang mindahin / menerima
            $table->string('from_employee_name')->nullable(); // user yang mindahin / menerima
            
            $table->string('to_company_location')->nullable();
            $table->string('to_factory_location')->nullable();
            $table->string('to_department_location')->nullable();
            $table->string('to_area_location')->nullable();
            $table->string('to_employee_nik')->nullable();
            $table->string('to_employee_name')->nullable();
           
            $table->string('no_kk')->nullable();
            $table->string('no_bea_cukai')->nullable();
            $table->timestamp('movement_date')->nullable(); // barang keluar 
            $table->text('note')->nullable();
            
            $table->timestamps();
            $table->biginteger('create_user_id')->nullable()->unsigned();
            $table->biginteger('delete_user_id')->nullable()->unsigned();

            $table->foreign('asset_id')->references('id')->on('assets')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('create_user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('delete_user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asset_movements');
    }
}
