<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRentInAssetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('assets', function (Blueprint $table) {
            $table->char('last_asset_movement_id',36)->nullable()->unsigned();
            $table->foreign('last_asset_movement_id')->references('id')->on('asset_movements')->onUpdate('cascade');

            $table->char('budget_id',36)->nullable()->unsigned();
            
            $table->integer('duration_rent_in_days')->default(false);
            $table->boolean('is_rent')->default(false);
            $table->string('no_rent')->nullable();
            $table->double('price',15,8)->default(0);
            $table->timestamp('start_rent_date')->nullable();
            $table->timestamp('end_rent_date')->nullable();
            $table->timestamp('return_date')->nullable();
            $table->text('remark_rent')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('assets', function (Blueprint $table) {
            //
        });
    }
}
