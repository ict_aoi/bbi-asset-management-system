<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssetInformationFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asset_information_fields', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('asset_type_id',36)->unsigned();
            $table->integer('ordering')->default(1);
            $table->string('label');
            $table->string('name');
            $table->string('type');
            $table->text('value')->nullable();
            $table->boolean('is_custom')->default(false);
            $table->boolean('required')->default(false);
            $table->timestamps();
            $table->timestamp('delete_at')->nullable();
            $table->biginteger('create_user_id')->nullable()->unsigned();
            $table->biginteger('delete_user_id')->nullable()->unsigned();
            
            $table->foreign('asset_type_id')->references('id')->on('asset_types')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('create_user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('delete_user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asset_information_fields');
    }
}
