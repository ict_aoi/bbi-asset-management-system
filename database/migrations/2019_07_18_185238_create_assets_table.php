<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assets', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('asset_type_id',36)->unsigned();
            $table->char('company_id',36)->unsigned();
            $table->char('factory_id',36)->unsigned();
            $table->string('department_name')->nullable();
            $table->char('manufacture_id',36)->nullable()->unsigned();
            $table->string('barcode')->unique();
            $table->string('serial_number');
            $table->string('no_inventory_manual')->nullable();
            $table->string('no_inventory')->nullable();
            $table->string('erp_no_asset')->nullable();
            $table->string('model')->nullable();
            $table->text('description')->nullable();
            $table->string('no_bea_cukai')->nullable();
            $table->string('purchase_number')->nullable();
            $table->string('supplier_name')->nullable();
            $table->date('receiving_date')->nullable();
            $table->integer('total')->default(0);
            $table->integer('qty_used')->default(0);
            $table->integer('qty_available')->default(0);
            $table->text('image')->nullable();
            $table->text('custom_field_1')->nullable();
            $table->text('custom_field_2')->nullable();
            $table->text('custom_field_3')->nullable();
            $table->text('custom_field_4')->nullable();
            $table->text('custom_field_5')->nullable();
            $table->text('custom_field_6')->nullable();
            $table->text('custom_field_7')->nullable();
            $table->text('custom_field_8')->nullable();
            $table->text('custom_field_9')->nullable();
            $table->text('custom_field_10')->nullable();
            $table->text('custom_field_11')->nullable();
            $table->text('custom_field_12')->nullable();
            $table->text('custom_field_13')->nullable();
            $table->text('custom_field_14')->nullable();
            $table->text('custom_field_15')->nullable();
            $table->text('custom_field_16')->nullable();
            $table->text('custom_field_17')->nullable();
            $table->text('custom_field_18')->nullable();
            $table->text('custom_field_19')->nullable();
            $table->text('custom_field_20')->nullable();
            $table->string('referral_code')->default(0); 
            $table->integer('sequence')->default(0);
            
            // untuk movement
            $table->string('status');
            $table->timestamp('last_movement_date')->nullable();
            $table->string('last_used_by')->nullable();
            $table->char('last_company_location_id',36)->nullable()->unsigned();
            $table->char('last_factory_location_id',36)->nullable()->unsigned();
            $table->string('last_department_location')->nullable()->unsigned();
            $table->string('last_area_location')->nullable();
            $table->string('last_no_kk')->nullable();
            $table->string('last_no_bea_cukai')->nullable();
            
            //untuk pindah tangan
            $table->char('origin_company_location_id',36)->nullable()->unsigned();
            $table->char('origin_factory_location_id',36)->nullable()->unsigned();
            $table->string('origin_department_location')->nullable()->unsigned();

            $table->timestamps();
            $table->timestamp('delete_at')->nullable();
            $table->biginteger('create_user_id')->nullable()->unsigned();
            $table->biginteger('delete_user_id')->nullable()->unsigned();

            $table->foreign('manufacture_id')->references('id')->on('manufactures')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('asset_type_id')->references('id')->on('asset_types')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('factory_id')->references('id')->on('factories')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('last_company_location_id')->references('id')->on('companies')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('last_factory_location_id')->references('id')->on('factories')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('origin_company_location_id')->references('id')->on('companies')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('origin_factory_location_id')->references('id')->on('factories')->onDelete('cascade')->onUpdate('cascade');
            
            #$table->foreign('area_id')->references('id')->on('areas')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('create_user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('delete_user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assets');
    }
}
