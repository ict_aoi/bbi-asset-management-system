<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColoumnIsReturnInConsumableAssetMovementsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('consumable_asset_movements', function (Blueprint $table) {
             $table->boolean('is_return')->default(false);
             $table->text('system_note')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('consumable_asset_movements', function (Blueprint $table) {
            //
        });
    }
}
