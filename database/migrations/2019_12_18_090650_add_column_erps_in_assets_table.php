<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnErpsInAssetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('assets', function (Blueprint $table) {
            $table->string('erp_asset_id')->nullable();
            $table->string('erp_asset_group_id')->nullable();
            $table->string('erp_asset_group_name')->nullable();
            $table->string('erp_item_id')->nullable();
            $table->string('erp_item_code')->nullable();
            $table->timestamp('is_from_sto_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('assets', function (Blueprint $table) {
            //
        });
    }
}
