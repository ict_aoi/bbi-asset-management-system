<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRentalAgreementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rental_agreements', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('factory_id',36);
            $table->char('sub_department_id',36);
            $table->char('budget_id',36);
            $table->char('parent_rental_agreement_id',36)->nullable();
            $table->string('no_agreement');
            $table->integer('duration_in_day');
            $table->double('price',18,2)->default(0);
            $table->timestamp('start_date');
            $table->timestamp('end_date');
            $table->text('description')->nullable();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();

            $table->biginteger('created_user_id')->nullable()->unsigned()->nullable();;
            $table->biginteger('updated_user_id')->nullable()->unsigned()->nullable();;
            $table->biginteger('deleted_user_id')->nullable()->unsigned()->nullable();;

            $table->foreign('factory_id')->references('id')->on('factories')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('sub_department_id')->references('id')->on('sub_departments')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('budget_id')->references('id')->on('budgets')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rental_agreements');
    }
}
