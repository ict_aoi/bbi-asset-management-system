<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnCopyIdInCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('companies', function (Blueprint $table) {
            $table->char('mapping_id',36)->nullable();
        });

        Schema::table('factories', function (Blueprint $table) {
            $table->char('mapping_id',36)->nullable();
        });
        
        Schema::table('departments', function (Blueprint $table) {
            $table->char('mapping_id',36)->nullable();
        });

        Schema::table('sub_departments', function (Blueprint $table) {
            $table->char('mapping_id',36)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('company', function (Blueprint $table) {
            //
        });
    }
}
