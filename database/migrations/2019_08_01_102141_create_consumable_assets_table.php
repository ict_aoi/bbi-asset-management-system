<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsumableAssetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consumable_assets', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('company_id',36)->unsigned();
            $table->char('asset_type_id',36)->unsigned();
            $table->char('manufacture_id',36)->unsigned()->nullable();
            $table->string('department_name')->nullable();
            $table->string('barcode')->unique();
            $table->string('no_asset')->nullable();
            $table->string('no_inventory_manual')->nullable();
            $table->string('no_inventory')->nullable();
            $table->string('serial_number')->nullable();
            $table->string('model')->nullable();
            $table->string('description')->nullable();
            $table->string('purchase_number')->nullable();
            $table->string('supplier_name')->nullable();
            $table->string('receiving_date')->nullable();
            $table->double('total',15,8)->default(0)->nullable();
            $table->double('qty_used',15,8)->default(0)->nullable();
            $table->double('qty_available',15,8)->default(0)->nullable();

            $table->text('custom_field_1')->nullable();
            $table->text('custom_field_2')->nullable();
            $table->text('custom_field_3')->nullable();
            $table->text('custom_field_4')->nullable();
            $table->text('custom_field_5')->nullable();
            $table->text('custom_field_6')->nullable();
            $table->text('custom_field_7')->nullable();
            $table->text('custom_field_8')->nullable();
            $table->text('custom_field_9')->nullable();
            $table->text('custom_field_10')->nullable();
            $table->text('custom_field_11')->nullable();
            $table->text('custom_field_12')->nullable();
            $table->text('custom_field_13')->nullable();
            $table->text('custom_field_14')->nullable();
            $table->text('custom_field_15')->nullable();
            $table->text('custom_field_16')->nullable();
            $table->text('custom_field_17')->nullable();
            $table->text('custom_field_18')->nullable();
            $table->text('custom_field_19')->nullable();
            $table->text('custom_field_20')->nullable();

            $table->string('referral_code')->default(0); 
            $table->integer('sequence')->default(0);

            $table->timestamps();
            $table->timestamp('delete_at')->nullable();
            $table->biginteger('create_user_id')->nullable()->unsigned();
            $table->biginteger('delete_user_id')->nullable()->unsigned();

            $table->foreign('manufacture_id')->references('id')->on('manufactures')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('asset_type_id')->references('id')->on('asset_types')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('create_user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('delete_user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consumable_assets');
    }
}
