<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsumableAssetMovementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consumable_asset_movements', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('consumable_asset_id',36)->unsigned();
            $table->char('consumable_asset_detail_id',36)->nullable()->unsigned();
            $table->char('asset_id',36)->nullable()->unsigned();
            $table->string('status')->nullable();
            $table->string('from_company_location')->nullable();
            $table->string('from_factory_location')->nullable();
            $table->string('from_department_location')->nullable();
            $table->string('from_employee_nik')->nullable();
            $table->string('from_employee_name')->nullable();
            $table->string('to_company_location')->nullable();
            $table->string('to_factory_location')->nullable();
            $table->string('to_department_location')->nullable();
            $table->string('to_area_location')->nullable();
            $table->string('to_employee_nik')->nullable();
            $table->string('to_employee_name')->nullable();
            $table->timestamp('movement_date')->nullable();
            
            $table->timestamps();
            $table->foreign('asset_id')->references('id')->on('assets')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('consumable_asset_detail_id')->references('id')->on('consumable_asset_details')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('consumable_asset_id')->references('id')->on('consumable_assets')->onDelete('cascade')->onUpdate('cascade');
            

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consumable_asset_movements');
    }
}
