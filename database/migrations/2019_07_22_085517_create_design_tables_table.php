<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDesignTablesTable extends Migration
{
    public function up()
    {
        Schema::create('design_tables', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->string('table_name');
            $table->string('column_name');
            $table->string('alias');
            $table->string('type_data');
            $table->string('length');
            $table->boolean('not_null')->default(false);
            $table->boolean('is_primary_key')->default(false);
            $table->boolean('is_foreign_key')->default(false);
            $table->boolean('is_column_custom')->default(false);
            $table->text('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('design_tables');
    }
}
