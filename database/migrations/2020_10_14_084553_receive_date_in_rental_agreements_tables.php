<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ReceiveDateInRentalAgreementsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rental_agreements', function (Blueprint $table) 
        {
            $table->timestamp('updated_bc_date')->nullable();
            $table->string('no_bc_in')->nullable();
            $table->string('no_aju_in')->nullable();
            $table->string('no_registration_in')->nullable();
            $table->date('date_registration_in')->nullable();
            $table->integer('updated_user_exim_in_id')->nullable();
            $table->foreign('updated_user_exim_in_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamp('updated_bc_in_date')->nullable();
            $table->timestamp('receive_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rental_agreements', function (Blueprint $table) {
            //
        });
    }
}
