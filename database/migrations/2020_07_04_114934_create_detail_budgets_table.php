<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailBudgetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_budgets', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('budget_id',36);
            $table->string('periode_month');
            $table->date('periode_by_date_start');
            $table->date('periode_by_date_end');
            $table->double('total',18,2)->default(0);
            $table->timestamps();

            $table->biginteger('created_user_id')->nullable()->unsigned()->nullable();;
            $table->biginteger('updated_user_id')->nullable()->unsigned()->nullable();;
            $table->biginteger('deleted_user_id')->nullable()->unsigned()->nullable();;

            $table->foreign('budget_id')->references('id')->on('budgets')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('created_user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('updated_user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('deleted_user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_budgets');
    }
}
