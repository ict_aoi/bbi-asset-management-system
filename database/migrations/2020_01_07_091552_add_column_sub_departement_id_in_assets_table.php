<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnSubDepartementIdInAssetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('assets', function (Blueprint $table) {
            $table->char('sub_department_id',36)->after('department_id')->nullable()->unsigned();
            $table->foreign('sub_department_id')->references('id')->on('sub_departments')->onDelete('cascade')->onUpdate('cascade');

            $table->char('origin_sub_department_location_id',36)->after('origin_department_location_id')->nullable()->unsigned();
            $table->foreign('origin_sub_department_location_id')->references('id')->on('sub_departments')->onDelete('cascade')->onUpdate('cascade');

            $table->char('source_sub_department_location_id',36)->after('source_department_id')->nullable()->unsigned();
            $table->foreign('source_sub_department_location_id')->references('id')->on('sub_departments')->onDelete('cascade')->onUpdate('cascade');

            $table->string('last_sub_department_location_id',36)->after('last_department_location_id')->nullable()->unsigned();
            $table->foreign('last_sub_department_location_id')->references('id')->on('sub_departments')->onDelete('cascade')->onUpdate('cascade');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('assets', function (Blueprint $table) {
            //
        });
    }
}
