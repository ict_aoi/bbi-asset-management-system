<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBudgetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('budgets', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('factory_id',36);
            $table->char('sub_department_id',36);
            $table->string('unique_id');
            $table->string('name');
            $table->text('description');
            $table->string('periode_year');
            $table->string('periode_month');
            $table->date('periode_by_date_start');
            $table->date('periode_by_date_end');
            $table->double('total',18,2)->default(0);
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();

            $table->biginteger('created_user_id')->nullable()->unsigned()->nullable();;
            $table->biginteger('updated_user_id')->nullable()->unsigned()->nullable();;
            $table->biginteger('deleted_user_id')->nullable()->unsigned()->nullable();;

            $table->foreign('factory_id')->references('id')->on('factories')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('sub_department_id')->references('id')->on('sub_departments')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('created_user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('updated_user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('deleted_user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('budgets');
    }
}
