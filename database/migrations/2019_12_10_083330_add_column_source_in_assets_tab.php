<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnSourceInAssetsTab extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('assets', function (Blueprint $table) {
            $table->char('source_company_location_id',36)->nullable()->unsigned();
            $table->char('source_factory_location_id',36)->nullable()->unsigned();
            $table->char('source_department_location_id',36)->nullable()->unsigned();
            
            $table->foreign('source_company_location_id')->references('id')->on('companies')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('source_factory_location_id')->references('id')->on('factories')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('source_department_location_id')->references('id')->on('departments')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('assets', function (Blueprint $table) {
            //
        });
    }
}
