<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubDepartmentsTable extends Migration
{
    public function up()
    {
        Schema::create('sub_departments', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('department_id',36)->nullable()->unsigned();
            $table->string('sub_department_absensi_id')->nullable();
            $table->string('code')->nullable();
            $table->string('name');
            $table->timestamps();
            $table->datetime('delete_at')->nullable();

            $table->foreign('department_id')->references('id')->on('departments')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->char('sub_department_id',36)->after('departement_id')->nullable()->unsigned();
            $table->foreign('sub_department_id')->references('id')->on('sub_departments')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_departments');
    }
}
