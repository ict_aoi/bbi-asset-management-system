<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnIsUsingInCustomFieldInConsumableAssetMovemetns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('consumable_asset_movements', function (Blueprint $table) {
            $table->boolean('is_using_in_custom_field')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('consumable_asset_movements', function (Blueprint $table) {
            //
        });
    }
}
