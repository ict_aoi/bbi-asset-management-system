<?php use Illuminate\Database\Seeder;

use App\Models\Factory;
use App\Models\Department;

class DepartmentTableSeeder extends Seeder
{
    
    public function run()
    {
        $department_bbis = DB::connection('absence_bbi')
        ->table('get_employee') 
        ->select('department_name','factory')
        ->groupby('department_name','factory')
        ->get();

        foreach ($department_bbis as $key => $value) 
        {
            $factory_name       = $value->factory;
            $department_name    = $value->department_name;

            $factory            = Factory::where('code',strtolower($factory_name))->first();

            $is_exists = Department::where([
                ['name',strtolower($department_name)],
                ['factory_id',$factory->id],
            ])
            ->exists();

            if(!$is_exists)
            {
                Department::create([
                    'factory_id' => $factory->id,
                    'name' => strtolower($department_name),
                ]);
            }
            
        }

        $sub_department_bbis = DB::connection('absence_bbi')
        ->table('get_employee') 
        ->select('subdept_name','factory')
        ->groupby('subdept_name','factory')
        ->get();

        foreach ($sub_department_bbis as $key => $value) 
        {
            $factory_name       = $value->factory;
            $department_name    = $value->subdept_name;

            $factory            = Factory::where('code',strtolower($factory_name))->first();

            $is_exists = Department::where([
                ['name',strtolower($department_name)],
                ['factory_id',$factory->id],
            ])
            ->exists();

            if(!$is_exists)
            {
                Department::create([
                    'factory_id' => $factory->id,
                    'name' => strtolower($department_name),
                ]);
            }
        }

        $department_aoi = DB::connection('absence_aoi')
        ->table('get_employee') 
        ->select('department_name','factory')
        ->groupby('department_name','factory')
        ->get();

        foreach ($department_aoi as $key => $value) 
        {
            $factory_name       = $value->factory;
            $department_name    = $value->department_name;

            $factory            = Factory::where('code',strtolower($factory_name))->first();

            $is_exists = Department::where([
                ['name',strtolower($department_name)],
                ['factory_id',$factory->id],
            ])
            ->exists();

            if(!$is_exists)
            {
                Department::create([
                    'factory_id' => $factory->id,
                    'name' => strtolower($department_name),
                ]);
            }
        }

        $sub_department_aoi = DB::connection('absence_aoi')
        ->table('get_employee') 
        ->select('subdept_name','factory')
        ->groupby('subdept_name','factory')
        ->get();

        foreach ($sub_department_aoi as $key => $value) 
        {
            $factory_name       = $value->factory;
            $department_name    = $value->subdept_name;

            $factory            = Factory::where('code',strtolower($factory_name))->first();

            $is_exists = Department::where([
                ['name',strtolower($department_name)],
                ['factory_id',$factory->id],
            ])
            ->exists();

            if(!$is_exists)
            {
                Department::create([
                    'factory_id' => $factory->id,
                    'name' => strtolower($department_name),
                ]);
            }
        }
    }
}
