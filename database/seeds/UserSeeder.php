<?php use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Role;
use App\Models\Factory;
use Carbon\Carbon;

class UserSeeder extends Seeder
{
    public function run()
    {
        $factory_bbis                       = Factory::where('code','bbis')->first();
        $role_super_admins                  = Role::Select('id')->where('name','super-admin')->get();
        
        $super_admin                        = new User();
        $super_admin->name                  = 'SUPER ADMIN';
        $super_admin->nik                   = '11111111';
        $super_admin->sex                   = 'laki';
        $super_admin->is_super_admin        = true;
        $super_admin->department_name       = 'bbi group';
        $super_admin->email                 = 'super_admin@aoi.co.id';
        $super_admin->department_name       = 'super admin';
        $super_admin->factory_id            = $factory_bbis->id;
        $super_admin->company_id            = $factory_bbis->company_id;
        $super_admin->email_verified_at     = carbon::now();
        $super_admin->password              =  bcrypt('password1');

        if($super_admin->save()) $super_admin->attachRoles($role_super_admins);  
    }
}
