<?php use Illuminate\Database\Seeder;

use App\Models\Company;

class CompanyTableSeeder extends Seeder
{
    
    public function run()
    {
        Company::Create([
            'code'=> 'bbi',
            'name'=> 'pt. bina busana internusa',
            'description'=> 'pt. bina busana internusa',
        ]);

        Company::Create([
            'code'=> 'aoi',
            'name'=> 'pt. apparel one indonesia',
            'description'=> 'pt. apparel one indonesia',
        ]);
    }
}
