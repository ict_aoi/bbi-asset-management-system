<?php use Illuminate\Database\Seeder;

use App\Models\Asset;
use App\Models\AssetType;
use App\Models\User;
use App\Models\Company;
use App\Models\Factory;
use App\Models\Manufacture;
use Faker\Factory as Faker;


class AssetTableSeeder extends Seeder
{
   
    public function run()
    {
        $faker = Faker::create('id_ID');
        $user = User::where('nik','11111111')->first();
        $aoi = Company::where('code','aoi')->first();
        $factory_aoi_2 = Factory::where('code','aoi2')->first();
        $factory_aoi_1 = Factory::where('code','aoi1')->first();
        $asset_type_aoi = AssetType::where([
            ['company_id',$aoi->id],
            ['department_name','ict'],
        ])
        ->first();

        $bbi = Company::where('code','bbi')->first();
        $asset_type_bbi = AssetType::where([
            ['company_id',$bbi->id],
            ['department_name','ict'],
        ])
        ->first();
        $factory_bbis = Factory::where('code','bbis')->first();

        for ($i=0; $i < 5; $i++) { 
            $manufacture = Manufacture::where([
                ['company_id',$aoi->id],
                ['department_name','ict'],
            ])
            ->first();

            Asset::Create([
                'asset_type_id'=> $asset_type_aoi->id,
                'origin_company_location_id'=> $aoi->id,
                'origin_factory_location_id'=> $factory_aoi_2->id,
                'origin_department_location'=> 'ict',
                'last_company_location_id'=> $aoi->id,
                'last_factory_location_id'=> $factory_aoi_2->id,
                'last_department_location'=> 'ict',
                'company_id'=> $aoi->id,
                'factory_id'=> $factory_aoi_2->id,
                'department_name'=> 'ict',
                'manufacture_id' => $manufacture->id,
                'barcode' => 'A'.$faker->ean8,
                'serial_number' => $faker->creditCardNumber,
                'no_inventory_manual' => $faker->creditCardNumber,
                'no_inventory' => $faker->creditCardNumber,
                'no_asset' => $faker->creditCardNumber,
                'model' => $faker->creditCardNumber,
                'purchase_number' => $faker->creditCardNumber,
                'supplier_name' => $faker->name,
                'receiving_date' => $faker->dateTime,
                'status' => 'siap digunakan',
            ]);
        }
        

        for ($i=0; $i < 5; $i++) { 
            $manufacture = Manufacture::where([
                ['company_id',$bbi->id],
                ['department_name','ict'],
            ])
            ->first();

            Asset::Create([
                'asset_type_id'=> $asset_type_bbi->id,
                'company_id'=> $bbi->id,
                'factory_id'=> $factory_bbis->id,
                'department_name'=> 'ict',
                'last_company_location_id'=> $bbi->id,
                'last_factory_location_id'=> $factory_bbis->id,
                'last_department_location'=> 'ict',
                'origin_company_location_id'=> $bbi->id,
                'origin_factory_location_id'=> $factory_bbis->id,
                'origin_department_location'=> 'ict',
                'manufacture_id' => $manufacture->id,
                'barcode' => 'A'.$faker->ean8,
                'serial_number' => $faker->creditCardNumber,
                'no_inventory_manual' => $faker->creditCardNumber,
                'no_inventory' => $faker->creditCardNumber,
                'no_asset' => $faker->creditCardNumber,
                'model' => $faker->creditCardNumber,
                'purchase_number' => $faker->creditCardNumber,
                'supplier_name' => $faker->name,
                'receiving_date' => $faker->dateTime,
                'status' => 'siap digunakan',
            ]);
        }
    }
}
