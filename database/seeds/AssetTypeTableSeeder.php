<?php use Illuminate\Database\Seeder;

use App\Models\AssetType;
use App\Models\AssetInformationField;
use App\Models\DesignTable;
use App\Models\User;
use App\Models\Company;

class AssetTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::where('nik','11111111')->first();
        $aoi = Company::where('code','aoi')->first();
        $bbi = Company::where('code','bbi')->first();

        $design_tables  = DesignTable::where('table_name','assets')
        ->whereNotIn('column_name',['id'
        ,'referral_code'
        ,'qty_used'
        ,'sequence'
        ,'qty_used'
        ,'qty_available'
        ,'last_used_by'
        ,'created_at'
        ,'updated_at'
        ,'delete_at'
        ,'create_user_id'
        ,'delete_user_id'])
        ->where('is_column_custom',false)
        ->get();

        $asset_aoi = AssetType::FirstOrCreate([
            'company_id'=> $aoi->id,
            'code'=> 'pc',
            'name'=> 'komputer pc',
            'department_name'=> 'ict',
            'is_consumable'=> false,
            'create_user_id' => $user->id
        ]);

        foreach ($design_tables as $key => $value) {
            AssetInformationField::firstorCreate([
                'asset_type_id' => $asset_aoi->id,
                'type' => 'text',
                'name' => $value->column_name,
                'label' => $value->alias,
                'value' => null,
                'required' => false,
                'is_custom' => false,
                'create_user_id' => $user->id
            ]);
        }

        
        $asset_bbi = AssetType::FirstOrCreate([
            'company_id'=> $bbi->id,
            'code'=> 'pc',
            'name'=> 'komputer pc',
            'department_name'=> 'ict',
            'is_consumable'=> false,
            'create_user_id' => $user->id
        ]);

        foreach ($design_tables as $key => $value) {
            AssetInformationField::firstorCreate([
                'asset_type_id' => $asset_bbi->id,
                'type' => 'text',
                'name' => $value->column_name,
                'label' => $value->alias,
                'value' => null,
                'required' => false,
                'is_custom' => false,
                'create_user_id' => $user->id
            ]);
        }
    }
}
