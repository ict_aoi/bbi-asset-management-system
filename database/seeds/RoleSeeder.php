<?php use Illuminate\Database\Seeder;

use App\Models\Role;
use App\Models\Permission;

class RoleSeeder extends Seeder
{
    public function run()
    {
        $permissions = Permission::select('id')->get();
        $role = new Role();
        $role->name = 'super-admin';
        $role->display_name = 'super admin';
        $role->description  = 'super admin';

        
        if($role->save())
        {
            $role->attachPermissions($permissions);
        }

        $admin_ict = new Role();
        $admin_ict->name = 'admin';
        $admin_ict->display_name = 'admin';
        $admin_ict->description  = 'admin';

        $permission_admin_icts = Permission::select('id')
        ->whereIn('name',[
            ['menu-asset'],
            ['menu-status'],
            ['menu-check-in'],
            ['menu-check-out'],
            ['menu-category'],
            ['menu-asset-type'],
            ['menu-manufacture']
        ])
        ->get();

        if($admin_ict->save())
        {
            $admin_ict->attachPermissions($permission_admin_icts);
        }

        $ict = new Role();
        $ict->name = 'user-pic';
        $ict->display_name = 'user pic';
        $ict->description  = 'user pic';

        $icts = Permission::select('id')
        ->whereIn('name',[
            ['menu-asset'],
            ['menu-check-in'],
            ['menu-check-out'],
        ])
        ->get();

        if($ict->save())
        {
            $ict->attachPermissions($icts);
        }
    }
}
