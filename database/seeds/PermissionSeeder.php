<?php

use Illuminate\Database\Seeder;
use App\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            [
                'name' => 'menu-user',
                'display_name' => 'menu user',
                'description' => 'menu user'
            ],
            [
                'name' => 'menu-asset',
                'display_name' => 'menu asset',
                'description' => 'menu asset'
            ],
            [
                'name' => 'menu-consumable',
                'display_name' => 'menu consumable',
                'description' => 'menu consumable'
            ],
            [
                'name' => 'menu-maintenance',
                'display_name' => 'menu maintenance',
                'description' => 'menu maintenance'
            ],
            [
                'name' => 'menu-bapb',
                'display_name' => 'menu bapb',
                'description' => 'menu bapb'
            ],
            [
                'name' => 'menu-check-in',
                'display_name' => 'menu check in',
                'description' => 'menu check in'
            ],
            [
                'name' => 'menu-department',
                'display_name' => 'menu department',
                'description' => 'menu department'
            ],
            [
                'name' => 'menu-area',
                'display_name' => 'menu area',
                'description' => 'menu area'
            ],
            [
                'name' => 'menu-check-out',
                'display_name' => 'menu check out',
                'description' => 'menu check out'
            ],
            [
                'name' => 'menu-company',
                'display_name' => 'menu company',
                'description' => 'menu company'
            ],
            [
                'name' => 'menu-factory',
                'display_name' => 'menu factory',
                'description' => 'menu factory'
            ],
            [
                'name' => 'menu-role',
                'display_name' => 'menu role',
                'description' => 'menu role'
            ],
            [
                'name' => 'menu-permission',
                'display_name' => 'menu permission',
                'description' => 'menu permission'
            ],
            [
                'name' => 'menu-asset-type',
                'display_name' => 'menu asset type',
                'description' => 'menu asset type'
            ],
            [
                'name' => 'menu-manufacture',
                'display_name' => 'menu manufacture',
                'description' => 'menu manufacture'
            ],
            
        ];

        foreach ($permissions as $key => $permission) {
            Permission::create([
                'name' => $permission['name'],
                'display_name' => $permission['display_name'],
                'description' => $permission['description']
            ]);
        }
    }
}
