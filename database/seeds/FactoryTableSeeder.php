<?php use Illuminate\Database\Seeder;

use App\Models\Company;
use App\Models\Factory;

class FactoryTableSeeder extends Seeder
{
    public function run()
    {
        $bbi = Company::where('code','bbi')->first();
        $aoi = Company::where('code','aoi')->first();
        
        Factory::Create([
            'company_id'=> $bbi->id,
            'code'=> 'bbis',
            'name'=> 'bina busana internusa semarang',
            'description'=> 'gudang bina busana internusa semarang',
        ]);

        Factory::Create([
            'company_id'=> $bbi->id,
            'code'=> 'bbic',
            'name'=> 'bina busana internusa cakung',
            'description'=> 'gudang bina busana internusa semarang',
        ]);

        Factory::Create([
            'company_id'=> $aoi->id,
            'code'=> 'aoi1',
            'name'=> 'aoi 1',
            'description'=> 'gudang aoi 1',
        ]);

        Factory::Create([
            'company_id'=> $aoi->id,
            'code'=> 'aoi2',
            'name'=> 'aoi 2',
            'description'=> 'gudang aoi 2',
        ]);

        Factory::Create([
            'company_id'=> $aoi->id,
            'code'=> 'tc',
            'name'=> 'training center',
            'description'=> 'training center',
        ]);
    }
}
