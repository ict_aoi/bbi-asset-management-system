<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        //DB::table('users')->delete();
        //$this->call(UserSeeder::class);
        /*DB::table('permissions')->truncate();
        DB::table('roles')->truncate();
        DB::table('companies')->truncate();
        DB::table('factories')->truncate();
        DB::table('manufactures')->truncate();
        DB::table('areas')->truncate();*/
        DB::table('design_tables')->truncate();
        /*DB::table('asset_types')->truncate();
        DB::table('assets')->truncate();
        DB::table('departments')->truncate();
        DB::table('users')->truncate();*/
        
        /*$this->call(CompanyTableSeeder::class);
        $this->call(FactoryTableSeeder::class);
        $this->call(DepartmentTableSeeder::class);
        
        
        $this->call(PermissionSeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(UserSeeder::class);

        $this->call(ManufactureTableSeeder::class);
        $this->call(DesignTableSeeder::class);
        $this->call(AssetTypeTableSeeder::class);
        $this->call(AssetTableSeeder::class);
        $this->call(AreaTableSeeder::class);*/

        $this->call(DesignTableSeeder::class);
        //$this->call(DepartmentTableSeeder::class);
    }
}
