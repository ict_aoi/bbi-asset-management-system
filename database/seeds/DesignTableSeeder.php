<?php use Illuminate\Database\Seeder;

use App\Models\User;
use App\Models\DesignTable;
use App\Models\AssetType;
use App\Models\AssetInformationField;

class DesignTableSeeder extends Seeder
{
    public function run()
    {
        DesignTable::create([
            'table_name'        => 'assets',
            'column_name'       => 'id',
            'alias'             => 'asset_id',
            'type_data'         => 'char',
            'length'            => '36',
            'not_null'          => true,
            'is_column_custom'  => false,
            'is_foreign_key'    => false,
            'is_primary_key'    => true,
            'description'       => 'PRIMARY KEY DARI TABEL',
            'order'             => 1,
        ]);

        DesignTable::create([
            'table_name'        => 'assets',
            'column_name'       => 'erp_asset_id',
            'alias'             => 'erp_asset_id',
            'type_data'         => 'string',
            'length'            => '255',
            'not_null'          => true,
            'is_column_custom'  => false,
            'is_foreign_key'    => false,
            'is_primary_key'    => true,
            'description'       => 'ID ASSET ERP',
            'order'             => 2,
        ]);

        DesignTable::create([
            'table_name'        => 'assets',
            'column_name'       => 'erp_asset_group_id',
            'alias'             => 'erp_asset_group_id',
            'type_data'         => 'string',
            'length'            => '255',
            'not_null'          => true,
            'is_column_custom'  => false,
            'is_foreign_key'    => false,
            'is_primary_key'    => true,
            'description'       => 'ID GROUP ASSET ERP',
            'order'             => 3,
        ]);

        DesignTable::create([
            'table_name'        => 'assets',
            'column_name'       => 'asset_type_id',
            'alias'             => 'tipe_asset',
            'type_data'         => 'char',
            'length'            => '36',
            'not_null'          => true,
            'is_column_custom'  => false,
            'is_foreign_key'    => true,
            'is_primary_key'    => false,
            'description'       => 'FOREIGN KEY DARI KE TABEL ASSET_TYPES',
            'order'             => 4,
        ]);

        DesignTable::create([
            'table_name'        => 'assets',
            'column_name'       => 'company_id',
            'alias'             => 'perusahaan',
            'type_data'         => 'char',
            'length'            => '36',
            'not_null'          => true,
            'is_column_custom'  => false,
            'is_foreign_key'    => true,
            'is_primary_key'    => false,
            'description'       => 'FOREIGN KEY DARI KE TABEL COMPANIES',
            'order'             => 5,
        ]);

        DesignTable::create([
            'table_name'        => 'assets',
            'column_name'       => 'factory_id',
            'alias'             => 'pabrik',
            'type_data'         => 'char',
            'length'            => '36',
            'not_null'          => true,
            'is_column_custom'  => false,
            'is_foreign_key'    => true,
            'is_primary_key'    => false,
            'description'       => 'FOREIGN KEY DARI KE TABEL FACTORIES',
            'order'             => 6,
        ]);

        DesignTable::create([
            'table_name'        => 'assets',
            'column_name'       => 'department_name',
            'alias'             => 'departemen',
            'type_data'         => 'text',
            'length'            => '255',
            'not_null'          => false,
            'is_column_custom'  => false,
            'is_foreign_key'    => false,
            'is_primary_key'    => false,
            'description'       => null,
            'order'             => 7,
        ]);

        DesignTable::create([
            'table_name'        => 'assets',
            'column_name'       => 'department_id',
            'alias'             => 'departemen',
            'type_data'         => 'text',
            'length'            => '255',
            'not_null'          => false,
            'is_column_custom'  => false,
            'is_foreign_key'    => false,
            'is_primary_key'    => false,
            'description'       => null,
            'order'             => 8,
        ]);

        DesignTable::create([
            'table_name'        => 'assets',
            'column_name'       => 'sub_department_id',
            'alias'             => 'sub_departemen',
            'type_data'         => 'text',
            'length'            => '255',
            'not_null'          => false,
            'is_column_custom'  => false,
            'is_foreign_key'    => false,
            'is_primary_key'    => false,
            'description'       => null,
            'order'             => 9,
        ]);

        DesignTable::create([
            'table_name'        => 'assets',
            'column_name'       => 'manufacture_id',
            'alias'             => 'pabrikan',
            'type_data'         => 'char',
            'length'            => '36',
            'not_null'          => false,
            'is_column_custom'  => false,
            'is_foreign_key'    => true,
            'is_primary_key'    => false,
            'description'       => 'FOREIGN KEY DARI KE TABEL MANUFACTURES',
            'order'             => 10,
        ]);

        DesignTable::create([
            'table_name'        => 'assets',
            'column_name'       => 'erp_asset_group_name',
            'alias'             => 'erp_asset_group_name',
            'type_data'         => 'string',
            'length'            => '255',
            'not_null'          => true,
            'is_column_custom'  => false,
            'is_foreign_key'    => false,
            'is_primary_key'    => true,
            'description'       => 'NAMA GROUP ASSET ERP',
            'order'             => 11,
        ]);

        DesignTable::create([
            'table_name'        => 'assets',
            'column_name'       => 'barcode',
            'alias'             => 'barcode',
            'type_data'         => 'text',
            'length'            => '255',
            'not_null'          => true,
            'is_column_custom'  => false,
            'is_foreign_key'    => true,
            'is_primary_key'    => false,
            'description'       => null,
            'order'             => 12,
        ]);

        DesignTable::create([
            'table_name'        => 'assets',
            'column_name'       => 'serial_number',
            'alias'             => 'nomor_serial',
            'type_data'         => 'text',
            'length'            => '255',
            'not_null'          => true,
            'is_column_custom'  => false,
            'is_foreign_key'    => true,
            'is_primary_key'    => false,
            'description'       => null,
            'order'             => 13,
        ]);
        
        DesignTable::create([
            'table_name'        => 'assets',
            'column_name'       => 'no_inventory_manual',
            'alias'             => 'no_inventori_manual',
            'type_data'         => 'text',
            'length'            => '255',
            'not_null'          => false,
            'is_column_custom'  => false,
            'is_foreign_key'    => false,
            'is_primary_key'    => false,
            'description'       => null,
            'order'             => 14,
        ]);

        DesignTable::create([
            'table_name'        => 'assets',
            'column_name'       => 'no_inventory',
            'alias'             => 'no_inventori',
            'type_data'         => 'text',
            'length'            => '255',
            'not_null'          => false,
            'is_column_custom'  => false,
            'is_foreign_key'    => false,
            'is_primary_key'    => false,
            'description'       => null,
            'order'             => 15,
        ]);

        DesignTable::create([
            'table_name'        => 'assets',
            'column_name'       => 'erp_no_asset',
            'alias'             => 'erp_no_asset',
            'type_data'         => 'text',
            'length'            => '255',
            'not_null'          => false,
            'is_column_custom'  => false,
            'is_foreign_key'    => false,
            'is_primary_key'    => false,
            'description'       => null,
            'order'             => 16,
        ]);

        DesignTable::create([
            'table_name'        => 'assets',
            'column_name'       => 'model',
            'alias'             => 'model',
            'type_data'         => 'text',
            'length'            => '255',
            'not_null'          => false,
            'is_column_custom'  => false,
            'is_foreign_key'    => false,
            'is_primary_key'    => false,
            'description'       => null,
            'order'             => 17,
        ]);

        DesignTable::create([
            'table_name'        => 'assets',
            'column_name'       => 'description',
            'alias'             => 'deskripsi',
            'type_data'         => 'text',
            'length'            => '0',
            'not_null'          => false,
            'is_column_custom'  => false,
            'is_foreign_key'    => false,
            'is_primary_key'    => false,
            'description'       => null,
            'order'             => 18,
        ]);

        DesignTable::create([
            'table_name'        => 'assets',
            'column_name'       => 'no_bea_cukai',
            'alias'             => 'no_bea_cukai_kedatangan',
            'type_data'         => 'text',
            'length'            => '255',
            'not_null'          => false,
            'is_column_custom'  => false,
            'is_foreign_key'    => false,
            'is_primary_key'    => false,
            'description'       => null,
            'order'             => 19,
        ]);

        DesignTable::create([
            'table_name'        => 'assets',
            'column_name'       => 'purchase_number',
            'alias'             => 'nomor_pembelian',
            'type_data'         => 'text',
            'length'            => '255',
            'not_null'          => false,
            'is_column_custom'  => false,
            'is_foreign_key'    => false,
            'is_primary_key'    => false,
            'description'       => null,
            'order'             => 20,
        ]);

        DesignTable::create([
            'table_name'        => 'assets',
            'column_name'       => 'supplier_name',
            'alias'             => 'nama_supplier',
            'type_data'         => 'text',
            'length'            => '255',
            'not_null'          => false,
            'is_column_custom'  => false,
            'is_foreign_key'    => false,
            'is_primary_key'    => false,
            'description'       => null,
            'order'             => 21,
        ]);

        DesignTable::create([
            'table_name'        => 'assets',
            'column_name'       => 'erp_item_code',
            'alias'             => 'erp_item_code',
            'type_data'         => 'string',
            'length'            => '255',
            'not_null'          => true,
            'is_column_custom'  => false,
            'is_foreign_key'    => false,
            'is_primary_key'    => true,
            'description'       => 'KODE ITEM ERP',
            'order'             => 22,
        ]);

        DesignTable::create([
            'table_name'        => 'assets',
            'column_name'       => 'receiving_date',
            'alias'             => 'tanggal_penerimaan',
            'type_data'         => 'date',
            'length'            => '0',
            'not_null'          => false,
            'is_column_custom'  => false,
            'is_foreign_key'    => false,
            'is_primary_key'    => false,
            'description'       => null,
            'order'             => 23,
        ]);

        DesignTable::create([
            'table_name'        => 'assets',
            'column_name'       => 'erp_item_id',
            'alias'             => 'erp_item_id',
            'type_data'         => 'string',
            'length'            => '255',
            'not_null'          => true,
            'is_column_custom'  => false,
            'is_foreign_key'    => false,
            'is_primary_key'    => true,
            'description'       => 'ID ITEM ERP',
            'order'             => 24,
        ]);

        DesignTable::create([
            'table_name'        => 'assets',
            'column_name'       => 'total',
            'alias'             => 'total',
            'type_data'         => 'integer',
            'length'            => '0',
            'not_null'          => false,
            'is_column_custom'  => false,
            'is_foreign_key'    => false,
            'is_primary_key'    => false,
            'description'       => null,
            'order'             => 25,
        ]);

        DesignTable::create([
            'table_name'        => 'assets',
            'column_name'       => 'qty_used',
            'alias'             => 'total_terpakai',
            'type_data'         => 'integer',
            'length'            => '0',
            'not_null'          => false,
            'is_column_custom'  => false,
            'is_foreign_key'    => false,
            'is_primary_key'    => false,
            'description'       => null,
            'order'             => 26,
        ]);

        DesignTable::create([
            'table_name'        => 'assets',
            'column_name'       => 'qty_available',
            'alias'             => 'total_tersedia',
            'type_data'         => 'integer',
            'length'            => '0',
            'not_null'          => false,
            'is_column_custom'  => false,
            'is_foreign_key'    => false,
            'is_primary_key'    => false,
            'description'       => null,
            'order'             => 27,
        ]);

        DesignTable::create([
            'table_name'        => 'assets',
            'column_name'       => 'image',
            'alias'             => 'gambar',
            'type_data'         => 'text',
            'length'            => '0',
            'not_null'          => false,
            'is_column_custom'  => false,
            'is_foreign_key'    => false,
            'is_primary_key'    => false,
            'description'       => null,
            'order'             => 28,
        ]);

        $order = 28;
        for ($i=0; $i < 20; $i++) 
        { 
            DesignTable::create([
                'table_name'        => 'assets',
                'column_name'       => 'custom_field_'.($i+1),
                'alias'             => 'Custom Field '.($i+1),
                'type_data'         => 'text',
                'length'            => '0',
                'not_null'          => false,
                'is_column_custom'  => true,
                'is_foreign_key'    => false,
                'is_primary_key'    => false,
                'description'        => null,
                'order'             => $order+1,
            ]);
        }

        DesignTable::create([
            'table_name'        => 'assets',
            'column_name'       => 'referral_code',
            'alias'             => 'referral_code',
            'type_data'         => 'text',
            'length'            => '255',
            'not_null'          => true,
            'is_column_custom'  => false,
            'is_foreign_key'    => true,
            'is_primary_key'    => false,
            'description'       => null,
            'order'             => 49,
        ]);

        DesignTable::create([
            'table_name'        => 'assets',
            'column_name'       => 'sequence',
            'alias'             => 'sequence',
            'type_data'         => 'integer',
            'length'            => '0',
            'not_null'          => true,
            'is_column_custom'  => false,
            'is_foreign_key'    => true,
            'is_primary_key'    => false,
            'description'       => null,
            'order'             => 50,
        ]);
        
        DesignTable::create([
            'table_name'        => 'assets',
            'column_name'       => 'status',
            'alias'             => 'status',
            'type_data'         => 'text',
            'length'            => '255',
            'not_null'          => true,
            'is_column_custom'  => false,
            'is_foreign_key'    => true,
            'is_primary_key'    => false,
            'description'       => null,
            'order'             => 51,
        ]);
        
        DesignTable::create([
            'table_name'        => 'assets',
            'column_name'       => 'last_movement_date',
            'alias'             => 'tanggal_terakhir_pergerakan',
            'type_data'         => 'datetime',
            'length'            => '0',
            'not_null'          => false,
            'is_column_custom'  => false,
            'is_foreign_key'    => false,
            'is_primary_key'    => false,
            'description'       => null,
            'order'             => 52,
        ]);

        DesignTable::create([
            'table_name'        => 'assets',
            'column_name'       => 'last_used_by',
            'alias'             => 'terakhir_digunakan',
            'type_data'         => 'text',
            'length'            => '255',
            'not_null'          => false,
            'is_column_custom'  => false,
            'is_foreign_key'    => false,
            'is_primary_key'    => false,
            'description'       => null,
            'order'             => 53,
        ]);

        DesignTable::create([
            'table_name'        => 'assets',
            'column_name'       => 'source_company_location_id',
            'alias'             => 'asal_lokasi_perusahaan',
            'type_data'         => 'text',
            'length'            => '255',
            'not_null'          => false,
            'is_column_custom'  => false,
            'is_foreign_key'    => true,
            'is_primary_key'    => false,
            'description'       => 'FOREIGN KEY DARI KE TABEL COMAPANIES',
            'order'             => 54,
        ]);

        DesignTable::create([
            'table_name'        => 'assets',
            'column_name'       => 'source_factory_location_id',
            'alias'             => 'asal_lokasi_pabrik',
            'type_data'         => 'text',
            'length'            => '255',
            'not_null'          => false,
            'is_column_custom'  => false,
            'is_foreign_key'    => true,
            'is_primary_key'    => false,
            'description'       => 'FOREIGN KEY DARI KE TABEL FACTORIES',
            'order'             => 55,
        ]);

        DesignTable::create([
            'table_name'        => 'assets',
            'column_name'       => 'source_department_location_id',
            'alias'             => 'asal_lokasi_departemen',
            'type_data'         => 'text',
            'length'            => '255',
            'not_null'          => false,
            'is_column_custom'  => false,
            'is_foreign_key'    => false,
            'is_primary_key'    => false,
            'description'       => null,
            'order'             => 56,
        ]);

        DesignTable::create([
            'table_name'        => 'assets',
            'column_name'       => 'source_sub_department_location_id',
            'alias'             => 'asal_lokasi_sub_departemen',
            'type_data'         => 'text',
            'length'            => '255',
            'not_null'          => false,
            'is_column_custom'  => false,
            'is_foreign_key'    => false,
            'is_primary_key'    => false,
            'description'       => null,
            'order'             => 57,
        ]);

        DesignTable::create([
            'table_name'        => 'assets',
            'column_name'       => 'last_company_location_id',
            'alias'             => 'lokasi_perusahaan_terakhir',
            'type_data'         => 'text',
            'length'            => '255',
            'not_null'          => false,
            'is_column_custom'  => false,
            'is_foreign_key'    => true,
            'is_primary_key'    => false,
            'description'       => 'FOREIGN KEY DARI KE TABEL COMAPANIES',
            'order'             => 58,
        ]);

        DesignTable::create([
            'table_name'        => 'assets',
            'column_name'       => 'last_factory_location_id',
            'alias'             => 'lokasi_pabrik_terakhir',
            'type_data'         => 'text',
            'length'            => '255',
            'not_null'          => false,
            'is_column_custom'  => false,
            'is_foreign_key'    => true,
            'is_primary_key'    => false,
            'description'       => 'FOREIGN KEY DARI KE TABEL FACTORIES',
            'order'             => 59,
        ]);

        DesignTable::create([
            'table_name'        => 'assets',
            'column_name'       => 'last_department_location_id',
            'alias'             => 'lokasi_departemen_terakhir',
            'type_data'         => 'text',
            'length'            => '255',
            'not_null'          => false,
            'is_column_custom'  => false,
            'is_foreign_key'    => false,
            'is_primary_key'    => false,
            'description'       => null,
            'order'             => 60,
        ]);

        DesignTable::create([
            'table_name'        => 'assets',
            'column_name'       => 'last_sub_department_location_id',
            'alias'             => 'lokasi_sub_departemen_terakhir',
            'type_data'         => 'text',
            'length'            => '255',
            'not_null'          => false,
            'is_column_custom'  => false,
            'is_foreign_key'    => false,
            'is_primary_key'    => false,
            'description'       => null,
            'order'             => 61,
        ]);

        DesignTable::create([
            'table_name'        => 'assets',
            'column_name'       => 'last_department_location',
            'alias'             => 'lokasi_departemen_terakhir',
            'type_data'         => 'text',
            'length'            => '255',
            'not_null'          => false,
            'is_column_custom'  => false,
            'is_foreign_key'    => false,
            'is_primary_key'    => false,
            'description'       => null,
            'order'             => 62,
        ]);

        DesignTable::create([
            'table_name'        => 'assets',
            'column_name'       => 'last_area_location',
            'alias'             => 'last_area_location',
            'type_data'         => 'text',
            'length'            => '255',
            'not_null'          => false,
            'is_column_custom'  => false,
            'is_foreign_key'    => false,
            'is_primary_key'    => false,
            'description'       => null,
            'order'             => 63,
        ]);
        
        DesignTable::create([
            'table_name'        => 'assets',
            'column_name'       => 'last_no_kk',
            'alias'             => 'no_kk',
            'type_data'         => 'text',
            'length'            => '255',
            'not_null'          => false,
            'is_column_custom'  => false,
            'is_foreign_key'    => false,
            'is_primary_key'    => false,
            'description'       => null,
            'order'             => 64,
        ]);
        
        DesignTable::create([
            'table_name'        => 'assets',
            'column_name'       => 'last_no_bea_cukai',
            'alias'             => 'no_bea_cukai',
            'type_data'         => 'text',
            'length'            => '255',
            'not_null'          => false,
            'is_column_custom'  => false,
            'is_foreign_key'    => false,
            'is_primary_key'    => false,
            'description'       => null,
            'order'             => 65,
        ]);
        
        DesignTable::create([
            'table_name'        => 'assets',
            'column_name'       => 'created_at',
            'alias'             => 'tanggal_dibuat',
            'type_data'         => 'timestamp',
            'length'            => '0',
            'not_null'          => false,
            'is_column_custom'  => false,
            'is_foreign_key'    => false,
            'is_primary_key'    => false,
            'description'       => null,
            'order'             => 66,
        ]);

        DesignTable::create([
            'table_name'        => 'assets',
            'column_name'       => 'updated_at',
            'alias'             => 'tanggal_diubah',
            'type_data'         => 'timestamp',
            'length'            => '0',
            'not_null'          => false,
            'is_column_custom'  => false,
            'is_foreign_key'    => false,
            'is_primary_key'    => false,
            'description'       => null,
            'order'             => 67,
        ]);

        DesignTable::create([
            'table_name'        => 'assets',
            'column_name'       => 'delete_at',
            'alias'             => 'tanggal_dihapus',
            'type_data'         => 'timestamp',
            'length'            => '0',
            'not_null'          => false,
            'is_column_custom'  => false,
            'is_foreign_key'    => false,
            'is_primary_key'    => false,
            'description'       => null,
            'order'             => 68,
        ]);

        DesignTable::create([
            'table_name'        => 'assets',
            'column_name'       => 'create_user_id',
            'alias'             => 'dibuat_oleh',
            'type_data'         => 'integer',
            'length'            => '0',
            'not_null'          => false,
            'is_column_custom'  => false,
            'is_foreign_key'    => false,
            'is_primary_key'    => false,
            'description'       => 'FOREIGN KEY DARI KE TABEL USERS',
            'order'             => 69,
        ]);
        
        DesignTable::create([
            'table_name'        => 'assets',
            'column_name'       => 'delete_user_id',
            'alias'             => 'dihapus_oleh',
            'type_data'         => 'integer',
            'length'            => '0',
            'not_null'          => false,
            'is_column_custom'  => false,
            'is_foreign_key'    => false,
            'is_primary_key'    => false,
            'description'       => 'FOREIGN KEY DARI KE TABEL USERS',
            'order'             => 70,
        ]);

        DesignTable::create([
            'table_name'        => 'assets',
            'column_name'       => 'is_from_sto_date',
            'alias'             => 'sto_date',
            'type_data'         => 'string',
            'length'            => '255',
            'not_null'          => true,
            'is_column_custom'  => false,
            'is_foreign_key'    => false,
            'is_primary_key'    => true,
            'description'       => 'TANGGAL STO',
            'order'             => 71,
        ]);

        DesignTable::create([
            'table_name'        => 'assets',
            'column_name'       => 'last_asset_movement_id',
            'alias'             => 'last_asset_movement_id',
            'type_data'         => 'char',
            'length'            => '36',
            'not_null'          => true,
            'is_column_custom'  => false,
            'is_foreign_key'    => false,
            'is_primary_key'    => true,
            'description'       => 'LAST ASSET MOVEMENT',
            'order'             => 72,
        ]);

        DesignTable::create([
            'table_name'        => 'assets',
            'column_name'       => 'last_user_movement_id',
            'alias'             => 'last_user_movement_id',
            'type_data'         => 'char',
            'length'            => '36',
            'not_null'          => true,
            'is_column_custom'  => false,
            'is_foreign_key'    => false,
            'is_primary_key'    => true,
            'description'       => 'LAST USER ASSET MOVEMENT',
            'order'             => 73,
        ]);

        DesignTable::create([
            'table_name'        => 'assets',
            'column_name'       => 'budget_id',
            'alias'             => 'budget_id',
            'type_data'         => 'char',
            'length'            => '36',
            'not_null'          => true,
            'is_column_custom'  => false,
            'is_foreign_key'    => false,
            'is_primary_key'    => true,
            'description'       => 'BUDGET',
            'order'             => 74,
        ]);

        DesignTable::create([
            'table_name'        => 'assets',
            'column_name'       => 'is_rent',
            'alias'             => 'is_rent',
            'type_data'         => 'boolean',
            'length'            => '0',
            'not_null'          => true,
            'is_column_custom'  => false,
            'is_foreign_key'    => false,
            'is_primary_key'    => false,
            'description'       => 'STATUS RENT',
            'order'             => 75,
        ]);

        DesignTable::create([
            'table_name'        => 'assets',
            'column_name'       => 'no_rent',
            'alias'             => 'no_rent',
            'type_data'         => 'string',
            'length'            => '255',
            'not_null'          => false,
            'is_column_custom'  => false,
            'is_foreign_key'    => false,
            'is_primary_key'    => false,
            'description'       => 'NO RENT',
            'order'             => 76,
        ]);

        DesignTable::create([
            'table_name'        => 'assets',
            'column_name'       => 'price',
            'alias'             => 'price',
            'type_data'         => 'float8',
            'length'            => '53',
            'not_null'          => false,
            'is_column_custom'  => false,
            'is_foreign_key'    => false,
            'is_primary_key'    => false,
            'description'       => 'PRICE',
            'order'             => 77,
        ]);

        DesignTable::create([
            'table_name'        => 'assets',
            'column_name'       => 'start_rent_date',
            'alias'             => 'start_rent_date',
            'type_data'         => 'date',
            'length'            => '0',
            'not_null'          => false,
            'is_column_custom'  => false,
            'is_foreign_key'    => false,
            'is_primary_key'    => false,
            'description'       => 'DATE START RENT',
            'order'             => 78,
        ]);

        DesignTable::create([
            'table_name'        => 'assets',
            'column_name'       => 'end_rent_date',
            'alias'             => 'end_rent_date',
            'type_data'         => 'date',
            'length'            => '0',
            'not_null'          => false,
            'is_column_custom'  => false,
            'is_foreign_key'    => false,
            'is_primary_key'    => false,
            'description'       => 'DATE END RENT',
            'order'             => 79,
        ]);

        DesignTable::create([
            'table_name'        => 'assets',
            'column_name'       => 'return_date',
            'alias'             => 'return_date',
            'type_data'         => 'date',
            'length'            => '0',
            'not_null'          => false,
            'is_column_custom'  => false,
            'is_foreign_key'    => false,
            'is_primary_key'    => false,
            'description'       => 'DATE RETURN RENT',
            'order'             => 80,
        ]);

        DesignTable::create([
            'table_name'        => 'assets',
            'column_name'       => 'remark_rent',
            'alias'             => 'remark_rent',
            'type_data'         => 'text',
            'length'            => '0',
            'not_null'          => false,
            'is_column_custom'  => false,
            'is_foreign_key'    => false,
            'is_primary_key'    => false,
            'description'       => 'REMARK RENT',
            'order'             => 81,
        ]);

        DesignTable::create([
            'table_name'        => 'assets',
            'column_name'       => 'duration_rent_in_days',
            'alias'             => 'duration_rent_in_days',
            'type_data'         => 'integer',
            'length'            => '0',
            'not_null'          => false,
            'is_column_custom'  => false,
            'is_foreign_key'    => false,
            'is_primary_key'    => false,
            'description'       => 'DURATION RENT',
            'order'             => 82,
        ]);


        DesignTable::create([
            'table_name'        => 'assets',
            'column_name'       => 'last_sto_date',
            'alias'             => 'last_sto_date',
            'type_data'         => 'date',
            'length'            => '0',
            'not_null'          => false,
            'is_column_custom'  => false,
            'is_foreign_key'    => false,
            'is_primary_key'    => false,
            'description'       => 'LAST STO DATE',
            'order'             => 83,
        ]);

        DesignTable::create([
            'table_name'        => 'assets',
            'column_name'       => 'sto_user_id',
            'alias'             => 'sto_user_id',
            'type_data'         => 'integer',
            'length'            => '0',
            'not_null'          => false,
            'is_column_custom'  => false,
            'is_foreign_key'    => false,
            'is_primary_key'    => false,
            'description'       => 'LAST STO DATE',
            'order'             => 84,
        ]);

        //INSERT INTO EXISTING ASSET TYPE INFORMATION FOR ASSET
        $data = AssetType::where('is_consumable',false)->get();
        foreach ($data as $key => $datum) 
        {
            AssetInformationField::where([
                ['asset_type_id',$datum->id],
                ['is_custom',false],
            ])
            ->delete();

            $design_tables = DesignTable::where([
                ['table_name','assets'],
                ['is_column_custom',false],
            ])
            ->orderby('order','asc')
            ->get();

            foreach ($design_tables as $key => $design_tables) 
            {
                AssetInformationField::firstorCreate([
                    'asset_type_id'     => $datum->id,
                    'type'              => strtolower($design_tables->type_data),
                    'name'              => strtolower($design_tables->column_name),
                    'label'             => strtolower($design_tables->alias),
                    'create_user_id'    => $datum->create_user_id,
                    'created_at'        => '2019-07-31 00:00:00',
                    'updated_at'        => '2019-07-31 00:00:00',
                ]);
            }
        }


        /*
                CONSUMBALE
        */

        DesignTable::create([
            'table_name'        => 'consumable_assets',
            'column_name'       => 'id',
            'alias'             => 'consumable_asset_id',
            'type_data'         => 'char',
            'length'            => '36',
            'not_null'          => true,
            'is_column_custom'  => false,
            'is_foreign_key'    => false,
            'is_primary_key'    => true,
            'description'       => 'PRIMARY KEY DARI TABEL',
            'order'             => 1,
        ]);

        DesignTable::create([
            'table_name'        => 'consumable_assets',
            'column_name'       => 'company_id',
            'alias'             => 'perusahaan',
            'type_data'         => 'char',
            'length'            => '36',
            'not_null'          => false,
            'is_column_custom'  => false,
            'is_foreign_key'    => true,
            'is_primary_key'    => false,
            'description'       => 'FOREIGN KEY DARI KE TABEL COMPANIES',
            'order'             => 2,
        ]);

        DesignTable::create([
            'table_name'        => 'consumable_assets',
            'column_name'       => 'asset_type_id',
            'alias'             => 'tipe_asset',
            'type_data'         => 'char',
            'length'            => '36',
            'not_null'          => false,
            'is_column_custom'  => false,
            'is_foreign_key'    => true,
            'is_primary_key'    => false,
            'description'       => 'FOREIGN KEY DARI KE TABEL ASSET TYPES',
            'order'             => 3,
        ]);

        DesignTable::create([
            'table_name'        => 'consumable_assets',
            'column_name'       => 'manufacture_id',
            'alias'             => 'pabrikan',
            'type_data'         => 'char',
            'length'            => '36',
            'not_null'          => false,
            'is_column_custom'  => false,
            'is_foreign_key'    => true,
            'is_primary_key'    => false,
            'description'       => 'FOREIGN KEY DARI KE TABEL MANUFACTURES',
            'order'             => 4,
        ]);

        DesignTable::create([
            'table_name'        => 'consumable_assets',
            'column_name'       => 'department_id',
            'alias'             => 'departemen',
            'type_data'         => 'string',
            'length'            => '255',
            'not_null'          => false,
            'is_column_custom'  => false,
            'is_foreign_key'    => false,
            'is_primary_key'    => false,
            'description'       => null,
            'order'             => 5,
        ]);

        DesignTable::create([
            'table_name'        => 'consumable_assets',
            'column_name'       => 'sub_department_id',
            'alias'             => 'sub_departemen',
            'type_data'         => 'string',
            'length'            => '255',
            'not_null'          => false,
            'is_column_custom'  => false,
            'is_foreign_key'    => false,
            'is_primary_key'    => false,
            'description'       => null,
            'order'             => 6,
        ]);

        DesignTable::create([
            'table_name'        => 'consumable_assets',
            'column_name'       => 'barcode',
            'alias'             => 'barcode',
            'type_data'         => 'string',
            'length'            => '255',
            'not_null'          => false,
            'is_column_custom'  => false,
            'is_foreign_key'    => false,
            'is_primary_key'    => false,
            'description'       => null,
            'order'             => 7,
        ]);

        DesignTable::create([
            'table_name'        => 'consumable_assets',
            'column_name'       => 'no_asset',
            'alias'             => 'no_asset',
            'type_data'         => 'string',
            'length'            => '255',
            'not_null'          => false,
            'is_column_custom'  => false,
            'is_foreign_key'    => false,
            'is_primary_key'    => false,
            'description'       => null,
            'order'             => 8,
        ]);

        DesignTable::create([
            'table_name'            => 'consumable_assets',
            'column_name'           => 'no_inventory_manual',
            'alias'                 => 'no_inventory_manual',
            'type_data'             => 'string',
            'length'                => '255',
            'not_null'              => false,
            'is_column_custom'      => false,
            'is_foreign_key'        => false,
            'is_primary_key'        => false,
            'description'           => null,
            'order'                 => 9,
        ]);

        DesignTable::create([
            'table_name'            => 'consumable_assets',
            'column_name'           => 'no_inventory',
            'alias'                 => 'no_inventory',
            'type_data'             => 'string',
            'length'                => '255',
            'not_null'              => false,
            'is_column_custom'      => false,
            'is_foreign_key'        => false,
            'is_primary_key'        => false,
            'description'           => null,
            'order'                 => 10,
        ]);

        DesignTable::create([
            'table_name'            => 'consumable_assets',
            'column_name'           => 'serial_number',
            'alias'                 => 'nomor_serial',
            'type_data'             => 'string',
            'length'                => '255',
            'not_null'              => false,
            'is_column_custom'      => false,
            'is_foreign_key'        => false,
            'is_primary_key'        => false,
            'description'           => null,
            'order'                 => 11,
        ]);

        DesignTable::create([
            'table_name'            => 'consumable_assets',
            'column_name'           => 'model',
            'alias'                 => 'model',
            'type_data'             => 'string',
            'length'                => '255',
            'not_null'              => false,
            'is_column_custom'      => false,
            'is_foreign_key'        => false,
            'is_primary_key'        => false,
            'description'           => null,
            'order'                 => 12,
        ]);

        DesignTable::create([
            'table_name'            => 'consumable_assets',
            'column_name'           => 'description',
            'alias'                 => 'deskripsi',
            'type_data'             => 'string',
            'length'                => '255',
            'not_null'              => false,
            'is_column_custom'      => false,
            'is_foreign_key'        => false,
            'is_primary_key'        => false,
            'description'           => null,
            'order'                 => 13,
        ]);

        DesignTable::create([
            'table_name'            => 'consumable_assets',
            'column_name'           => 'purchase_number',
            'alias'                 => 'nomor_pembelian',
            'type_data'             => 'string',
            'length'                => '255',
            'not_null'              => false,
            'is_column_custom'      => false,
            'is_foreign_key'        => false,
            'is_primary_key'        => false,
            'description'           => null,
            'order'                 => 14,
        ]);

        DesignTable::create([
            'table_name'            => 'consumable_assets',
            'column_name'           => 'supplier_name',
            'alias'                 => 'nama_supplier',
            'type_data'             => 'string',
            'length'                => '255',
            'not_null'              => false,
            'is_column_custom'      => false,
            'is_foreign_key'        => false,
            'is_primary_key'        => false,
            'description'           => null,
            'order'                 => 15,
        ]);

        DesignTable::create([
            'table_name'            => 'consumable_assets',
            'column_name'           => 'receiving_date',
            'alias'                 => 'tanggal_kedatangan',
            'type_data'             => 'string',
            'length'                => '255',
            'not_null'              => false,
            'is_column_custom'      => false,
            'is_foreign_key'        => false,
            'is_primary_key'        => false,
            'description'           => null,
            'order'                 => 16,
        ]);

        DesignTable::create([
            'table_name'            => 'consumable_assets',
            'column_name'           => 'total',
            'alias'                 => 'total_keseluruhan',
            'type_data'             => 'decimal',
            'length'                => 0,
            'not_null'              => false,
            'is_column_custom'      => false,
            'is_foreign_key'        => false,
            'is_primary_key'        => false,
            'description'           => null,
            'order'                 => 17,
        ]);

        DesignTable::create([
            'table_name'            => 'consumable_assets',
            'column_name'           => 'qty_used',
            'alias'                 => 'total_terpakai',
            'type_data'             => 'decimal',
            'length'                => 0,
            'not_null'              => false,
            'is_column_custom'      => false,
            'is_foreign_key'        => false,
            'is_primary_key'        => false,
            'description'           => null,
            'order'                 => 18,
        ]);

        DesignTable::create([
            'table_name'            => 'consumable_assets',
            'column_name'           => 'qty_available',
            'alias'                 => 'total_tersedia',
            'type_data'             => 'decimal',
            'length'                => 0,
            'not_null'              => false,
            'is_column_custom'      => false,
            'is_foreign_key'        => false,
            'is_primary_key'        => false,
            'description'           => null,
            'order'                 => 19,
        ]);

        $order = 19;
        for ($i=0; $i < 20; $i++) 
        { 
            DesignTable::create([
                'table_name'        => 'consumable_assets',
                'column_name'       => 'custom_field_'.($i+1),
                'alias'             => 'Custom Field '.($i+1),
                'type_data'         => 'text',
                'length'            => '0',
                'not_null'          => false,
                'is_column_custom'  => true,
                'is_foreign_key'    => false,
                'is_primary_key'    => false,
                'description'       => null,
                'order'             => $order+1,
            ]);
        }

        DesignTable::create([
            'table_name'        => 'consumable_assets',
            'column_name'       => 'referral_code',
            'alias'             => 'referral_code',
            'type_data'         => 'string',
            'length'            => '255',
            'not_null'          => false,
            'is_column_custom'  => false,
            'is_foreign_key'    => false,
            'is_primary_key'    => false,
            'description'       => null,
            'order'              => 30,
        ]);

        DesignTable::create([
            'table_name'        => 'consumable_assets',
            'column_name'       => 'sequence',
            'alias'             => 'sequence',
            'type_data'         => 'integer',
            'length'            => 0,
            'not_null'          => false,
            'is_column_custom'  => false,
            'is_foreign_key'    => false,
            'is_primary_key'    => false,
            'description'       => null,
            'order'             => 31,
        ]);

        DesignTable::create([
            'table_name'        => 'consumable_assets',
            'column_name'       => 'create_user_id',
            'alias'             => 'dibuat_oleh',
            'type_data'         => 'integer',
            'length'            => '0',
            'not_null'          => false,
            'is_column_custom'  => false,
            'is_foreign_key'    => true,
            'is_primary_key'    => false,
            'description'       => 'FOREIGN KEY DARI KE TABEL USERS',
            'order'              => 32,
        ]);
        
        DesignTable::create([
            'table_name'        => 'consumable_assets',
            'column_name'       => 'delete_user_id',
            'alias'             => 'dihapus_oleh',
            'type_data'         => 'integer',
            'length'            => '0',
            'not_null'          => false,
            'is_column_custom'  => false,
            'is_foreign_key'    => true,
            'is_primary_key'    => false,
            'description'       => 'FOREIGN KEY DARI KE TABEL USERS',
            'order'              => 33,
        ]);

        //INSERT INTO EXISTING ASSET TYPE INFORMATION FOR ASSET
        $data = AssetType::where('is_consumable',true)->get();
        foreach ($data as $key => $datum) 
        {
            AssetInformationField::where([
                ['asset_type_id',$datum->id],
                ['is_custom',false],
            ])
            ->delete();

            $design_tables = DesignTable::where([
                ['table_name','consumable_assets'],
                ['is_column_custom',false],
            ])
            ->orderby('order','asc')
            ->get();

            foreach ($design_tables as $key => $design_tables) 
            {
                AssetInformationField::firstorCreate([
                    'asset_type_id'     => $datum->id,
                    'type'              => strtolower($design_tables->type_data),
                    'name'              => strtolower($design_tables->column_name),
                    'label'             => strtolower($design_tables->alias),
                    'create_user_id'    => $datum->create_user_id,
                    'created_at'        => '2019-07-31 00:00:00',
                    'updated_at'        => '2019-07-31 00:00:00',
                ]);
            }
        }
    }
}
