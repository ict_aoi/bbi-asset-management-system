<?php use Illuminate\Database\Seeder;

use App\Models\User;
use App\Models\Area;
use App\Models\Factory;

class AreaTableSeeder extends Seeder
{
    public function run()
    {
        $user = User::where('nik','11111111')->first();
        $aoi_1 = Factory::where('code','aoi1')->first();

        for ($i=0; $i < 10; $i++) { 
            Area::create([
                'factory_id' => $aoi_1->id,
                'name' => 'area '.($i+1),
                'create_user_id' => $user->id,
            ]);
        }

        $aoi_2 = Factory::where('code','aoi2')->first();
        for ($i=0; $i < 10; $i++) { 
            Area::create([
                'factory_id' => $aoi_2->id,
                'name' => 'area '.($i+1),
                'create_user_id' => $user->id,
            ]);
        }
        

        $bbis = Factory::where('code','bbis')->first();
        for ($i=0; $i < 10; $i++) { 
            Area::create([
                'factory_id' => $bbis->id,
                'name' => 'area '.($i+1),
                'create_user_id' => $user->id,
            ]);
        }
    }
}
