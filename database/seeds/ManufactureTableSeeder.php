<?php use Illuminate\Database\Seeder;

use App\Models\User;
use App\Models\Manufacture;
use App\Models\Factory;

class ManufactureTableSeeder extends Seeder
{
    
    public function run()
    {
        $user = User::where('nik','11111111')->first();
        $factory = Factory::where('code','aoi2')->first();

        Manufacture::Create([
            'name'=> 'Apple',
            'description'=> 'Apple Company',
            'department_name'=> 'ict',
            'company_id'=> $factory->company->id,
            #'factory_id' => $factory->id,
            'create_user_id' => $user->id
        ]);

        Manufacture::Create([
            'name'=> 'Acer',
            'description'=> 'Acer Company',
            'department_name'=> 'ict',
            'company_id'=> $factory->company->id,
            #'factory_id' => $factory->id,
            'create_user_id' => $user->id
        ]);

        Manufacture::Create([
            'name'=> 'Dell',
            'description'=> 'Dell Company',
            'department_name'=> 'ict',
            'company_id'=> $factory->company->id,
            #'factory_id' => $factory->id,
            'create_user_id' => $user->id
        ]);

        Manufacture::Create([
            'name'=> 'Cisco',
            'department_name'=> 'ict',
            'company_id'=> $factory->company->id,
            #'factory_id' => $factory->id,
            'description'=> 'Cisco Company',
            'create_user_id' => $user->id
        ]);

        Manufacture::Create([
            'name'=> 'Samsung',
            'department_name'=> 'ict',
            'company_id'=> $factory->company->id,
            #'factory_id' => $factory->id,
            'description'=> 'Samsung Company',
            'create_user_id' => $user->id
        ]);

        Manufacture::Create([
            'name'=> 'Brother',
            'department_name'=> 'ict',
            'company_id'=> $factory->company->id,
            #'factory_id' => $factory->id,
            'description'=> 'Brother Company',
            'create_user_id' => $user->id
        ]);

        Manufacture::Create([
            'name'=> 'Juki',
            'department_name'=> 'ict',
            'company_id'=> $factory->company->id,
            #'factory_id' => $factory->id,
            'description'=> 'Juki Company',
            'create_user_id' => $user->id
        ]);

        $factory_bbi = Factory::where('code','bbis')->first();

        Manufacture::Create([
            'name'=> 'Apple',
            'description'=> 'Apple Company',
            'department_name'=> 'ict',
            'company_id'=> $factory_bbi->company->id,
            #'factory_id' => $factory->id,
            'create_user_id' => $user->id
        ]);

        Manufacture::Create([
            'name'=> 'Acer',
            'description'=> 'Acer Company',
            'department_name'=> 'ict',
            'company_id'=> $factory_bbi->company->id,
            #'factory_id' => $factory->id,
            'create_user_id' => $user->id
        ]);

        Manufacture::Create([
            'name'=> 'Dell',
            'description'=> 'Dell Company',
            'department_name'=> 'ict',
            'company_id'=> $factory_bbi->company->id,
            #'factory_id' => $factory->id,
            'create_user_id' => $user->id
        ]);

        Manufacture::Create([
            'name'=> 'Cisco',
            'department_name'=> 'ict',
            'company_id'=> $factory_bbi->company->id,
            #'factory_id' => $factory->id,
            'description'=> 'Cisco Company',
            'create_user_id' => $user->id
        ]);

        Manufacture::Create([
            'name'=> 'Samsung',
            'department_name'=> 'ict',
            'company_id'=> $factory_bbi->company->id,
            #'factory_id' => $factory->id,
            'description'=> 'Samsung Company',
            'create_user_id' => $user->id
        ]);

        Manufacture::Create([
            'name'=> 'Brother',
            'department_name'=> 'ict',
            'company_id'=> $factory_bbi->company->id,
            #'factory_id' => $factory->id,
            'description'=> 'Brother Company',
            'create_user_id' => $user->id
        ]);

        Manufacture::Create([
            'name'=> 'Juki',
            'department_name'=> 'ict',
            'company_id'=> $factory_bbi->company->id,
            #'factory_id' => $factory->id,
            'description'=> 'Juki Company',
            'create_user_id' => $user->id
        ]);
    }
}
